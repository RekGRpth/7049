.class Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;
.super Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;
.source "ClientLogsBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/logger/ClientLogsBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LatencyProcessor"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;
    }
.end annotation


# instance fields
.field private final mBreakDownEvents:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final mFromEvents:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final mPastEvents:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final mToEvents:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;",
            ">;>;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;)V
    .locals 12
    .param p2    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;

    const v11, 0x40000009

    const v10, 0x1000000b

    const v4, 0x1000000c

    const v3, 0x50000020

    iput-object p1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;-><init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;)V

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;->mPastEvents:Landroid/util/SparseArray;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;->mFromEvents:Landroid/util/SparseArray;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;->mToEvents:Landroid/util/SparseArray;

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;->mBreakDownEvents:Ljava/util/ArrayList;

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;

    const v1, 0x10000007

    const v2, 0x10000008

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;-><init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;II)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;

    const v1, 0x40000001

    const v2, 0x10000003

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;-><init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;II)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;

    const v1, 0x10000017

    const v2, 0x1000001f

    invoke-direct {v0, p0, v1, v10, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;-><init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;III)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;

    const v1, 0x10000020

    invoke-direct {v0, p0, v10, v4, v1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;-><init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;III)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;

    const v1, 0x10000013

    const v2, 0x10000049

    invoke-direct {v0, p0, v1, v4, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;-><init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;III)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;

    const v1, 0x1000001e

    const v2, 0x10000021

    invoke-direct {v0, p0, v4, v1, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;-><init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;III)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;

    const v1, 0x40000002

    const v2, 0x10000052

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;-><init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;II)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;

    const v1, 0x40000005

    const v2, 0x10000061

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;-><init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;II)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;

    const v1, 0x40000006

    const v2, 0x10000060

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;-><init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;II)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;

    const v1, 0x1000006a

    invoke-direct {v0, p0, v11, v1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;-><init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;II)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;

    const v1, 0x1000006b

    invoke-direct {v0, p0, v11, v1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;-><init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;II)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v0, 0xd

    new-array v5, v0, [I

    fill-array-data v5, :array_0

    new-instance v0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;

    const v2, 0x40000003

    const v4, 0x10000063

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;-><init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;III[I)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v0, 0xb

    new-array v5, v0, [I

    fill-array-data v5, :array_1

    new-instance v0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;

    const v2, 0x40000004

    const v4, 0x10000064

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;-><init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;III[I)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v0, 0xa

    new-array v5, v0, [I

    fill-array-data v5, :array_2

    new-instance v0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;

    const v2, 0x40000007

    const v4, 0x10000065

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;-><init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;III[I)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x7

    new-array v5, v0, [I

    fill-array-data v5, :array_3

    new-instance v0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;

    const v2, 0x40000008

    const v4, 0x10000066

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;-><init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;III[I)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;

    iget-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;->mFromEvents:Landroid/util/SparseArray;

    iget v1, v7, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;->mFromEvent:I

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;->mToEvents:Landroid/util/SparseArray;

    iget v1, v7, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;->mToEvent:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/List;

    if-nez v9, :cond_0

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;->mToEvents:Landroid/util/SparseArray;

    iget v1, v7, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;->mToEvent:I

    invoke-virtual {v0, v1, v9}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_0
    invoke-interface {v9, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-void

    nop

    :array_0
    .array-data 4
        0x10
        0x11
        0x12
        0x13
        0x14
        0x15
        0x16
        0x17
        0x18
        0x19
        0x1a
        0x1c
        0x20
    .end array-data

    :array_1
    .array-data 4
        0x12
        0x13
        0x14
        0x15
        0x16
        0x17
        0x18
        0x19
        0x1a
        0x1c
        0x20
    .end array-data

    :array_2
    .array-data 4
        0x14
        0x15
        0x16
        0x17
        0x18
        0x19
        0x1a
        0x1b
        0x1c
        0x20
    .end array-data

    :array_3
    .array-data 4
        0x14
        0x17
        0x18
        0x19
        0x1a
        0x1c
        0x20
    .end array-data
.end method

.method private addBreakDownData(JJLcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)V
    .locals 6
    .param p1    # J
    .param p3    # J
    .param p5    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;
    .param p6    # Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    invoke-virtual {p6}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->getLatency()Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;

    move-result-object v4

    invoke-static {v4}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v4, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;->mBreakDownEvents:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->timestamp:J
    invoke-static {v1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$1000(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)J

    move-result-wide v4

    cmp-long v4, v4, p1

    if-ltz v4, :cond_0

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->timestamp:J
    invoke-static {v1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$1000(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)J

    move-result-wide v4

    cmp-long v4, v4, p3

    if-gtz v4, :cond_0

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->type:I
    invoke-static {v1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$600(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)I

    move-result v4

    # invokes: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;->hasBreakDownEvent(I)Z
    invoke-static {p5, v4}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;->access$1400(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;I)Z

    move-result v4

    if-eqz v4, :cond_0

    new-instance v0, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyBreakdownEvent;

    invoke-direct {v0}, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyBreakdownEvent;-><init>()V

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->type:I
    invoke-static {v1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$600(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyBreakdownEvent;->setEvent(I)Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyBreakdownEvent;

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->timestamp:J
    invoke-static {v1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$1000(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)J

    move-result-wide v4

    sub-long/2addr v4, p1

    long-to-int v4, v4

    invoke-virtual {v0, v4}, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyBreakdownEvent;->setOffsetMsec(I)Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyBreakdownEvent;

    invoke-virtual {p6}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->getLatency()Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->addBreakdown(Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyBreakdownEvent;)Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->mSublatencyCalculator:Lcom/google/android/voicesearch/logger/SublatencyCalculator;
    invoke-static {v4}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->access$1500(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;)Lcom/google/android/voicesearch/logger/SublatencyCalculator;

    move-result-object v4

    invoke-virtual {p6}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->getLatency()Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/voicesearch/logger/SublatencyCalculator;->addBreakDownSublatency(Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;)V

    return-void
.end method


# virtual methods
.method public internalProcess(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)Z
    .locals 16
    .param p1    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;
    .param p2    # Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->group:I
    invoke-static/range {p1 .. p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$500(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)I

    move-result v1

    const/high16 v12, 0x50000000

    if-ne v1, v12, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;->mBreakDownEvents:Ljava/util/ArrayList;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->clone()Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;

    move-result-object v12

    invoke-virtual {v1, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;->mFromEvents:Landroid/util/SparseArray;

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->group:I
    invoke-static/range {p1 .. p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$500(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)I

    move-result v12

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->type:I
    invoke-static/range {p1 .. p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$600(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)I

    move-result v13

    or-int/2addr v12, v13

    invoke-virtual {v1, v12}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;->mPastEvents:Landroid/util/SparseArray;

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->group:I
    invoke-static/range {p1 .. p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$500(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)I

    move-result v12

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->type:I
    invoke-static/range {p1 .. p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$600(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)I

    move-result v13

    or-int/2addr v12, v13

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->timestamp:J
    invoke-static/range {p1 .. p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$1000(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)J

    move-result-wide v13

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    invoke-virtual {v1, v12, v13}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;->mToEvents:Landroid/util/SparseArray;

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->group:I
    invoke-static/range {p1 .. p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$500(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)I

    move-result v12

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->type:I
    invoke-static/range {p1 .. p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$600(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)I

    move-result v13

    or-int/2addr v12, v13

    invoke-virtual {v1, v12}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_5

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;->mToEvents:Landroid/util/SparseArray;

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->group:I
    invoke-static/range {p1 .. p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$500(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)I

    move-result v12

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->type:I
    invoke-static/range {p1 .. p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$600(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)I

    move-result v13

    add-int/2addr v12, v13

    invoke-virtual {v1, v12}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_2
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;->mPastEvents:Landroid/util/SparseArray;

    iget v12, v11, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;->mFromEvent:I

    invoke-virtual {v1, v12}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2

    if-eqz v6, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;->mPastEvents:Landroid/util/SparseArray;

    iget v12, v11, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;->mFromEvent:I

    invoke-virtual {v1, v12}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;->mPastEvents:Landroid/util/SparseArray;

    iget v14, v6, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;->mFromEvent:I

    invoke-virtual {v1, v14}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    cmp-long v1, v12, v14

    if-lez v1, :cond_2

    :cond_3
    move-object v6, v11

    goto :goto_0

    :cond_4
    if-eqz v6, :cond_5

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;->mPastEvents:Landroid/util/SparseArray;

    iget v12, v6, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;->mFromEvent:I

    invoke-virtual {v1, v12}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->timestamp:J
    invoke-static/range {p1 .. p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$1000(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)J

    move-result-wide v4

    sub-long v12, v4, v2

    long-to-int v9, v12

    iget v1, v6, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;->mEventType:I

    iget v12, v6, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;->mToEvent:I

    if-ne v1, v12, :cond_6

    new-instance v1, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;

    invoke-direct {v1}, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;-><init>()V

    invoke-virtual {v1, v9}, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->setDurationMsec(I)Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->setLatency(Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    :cond_5
    :goto_1
    const/4 v1, 0x0

    return v1

    :cond_6
    new-instance v1, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;

    invoke-direct {v1}, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;-><init>()V

    invoke-virtual {v1, v9}, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->setDurationMsec(I)Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;

    move-result-object v10

    new-instance v1, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    invoke-direct {v1}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;-><init>()V

    iget v12, v6, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;->mEventType:I

    invoke-static {v12}, Lcom/google/android/voicesearch/logger/EventUtils;->getId(I)I

    move-result v12

    invoke-virtual {v1, v12}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->setEventType(I)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    move-result-object v1

    invoke-virtual {v1, v10}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->setLatency(Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    move-result-object v1

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->timestamp:J
    invoke-static/range {p1 .. p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$1000(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)J

    move-result-wide v12

    invoke-virtual {v1, v12, v13}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->setClientTimeMs(J)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    move-result-object v7

    invoke-virtual/range {p2 .. p2}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasRequestId()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-virtual/range {p2 .. p2}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->getRequestId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->setRequestId(Ljava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    :cond_7
    invoke-virtual/range {p2 .. p2}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasActionType()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-virtual/range {p2 .. p2}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->getActionType()I

    move-result v1

    invoke-virtual {v7, v1}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->setActionType(I)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    :cond_8
    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;->addBreakDownData(JJLcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor$LatencyEvent;Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    # invokes: Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->addClientEvent(Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)V
    invoke-static {v1, v7}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->access$800(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)V

    goto :goto_1
.end method
