.class Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;
.super Ljava/lang/Object;
.source "ClientLogsBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/logger/ClientLogsBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoggedEvent"
.end annotation


# instance fields
.field private data:Ljava/lang/Object;

.field private group:I

.field private source:I

.field final synthetic this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

.field private timestamp:J

.field private type:I


# direct methods
.method private constructor <init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$1;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder;
    .param p2    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder$1;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;-><init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)J
    .locals 2
    .param p0    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;

    iget-wide v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->timestamp:J

    return-wide v0
.end method

.method static synthetic access$500(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)I
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;

    iget v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->group:I

    return v0
.end method

.method static synthetic access$600(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)I
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;

    iget v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->type:I

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;

    iget-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->data:Ljava/lang/Object;

    return-object v0
.end method

.method private getSource(I)I
    .locals 1
    .param p1    # I

    sparse-switch p1, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :sswitch_0
    const/4 v0, 0x3

    goto :goto_0

    :sswitch_1
    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const/4 v0, 0x2

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1000000 -> :sswitch_1
        0x2000000 -> :sswitch_2
        0x3000000 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public clone()Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;
    .locals 3

    new-instance v0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;

    iget-object v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    invoke-direct {v0, v1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;-><init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;)V

    iget v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->group:I

    iput v1, v0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->group:I

    iget v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->source:I

    iput v1, v0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->source:I

    iget v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->type:I

    iput v1, v0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->type:I

    iget-wide v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->timestamp:J

    iput-wide v1, v0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->timestamp:J

    iget-object v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->data:Ljava/lang/Object;

    iput-object v1, v0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->data:Ljava/lang/Object;

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->clone()Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;

    move-result-object v0

    return-object v0
.end method

.method public in(Landroid/util/SparseArray;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Boolean;",
            ">;)Z"
        }
    .end annotation

    iget v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->group:I

    iget v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->type:I

    or-int/2addr v0, v1

    invoke-virtual {p1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public is(II)Z
    .locals 1
    .param p1    # I
    .param p2    # I

    iget v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->group:I

    if-ne v0, p1, :cond_0

    iget v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->type:I

    if-ne v0, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public populate(Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)V
    .locals 4
    .param p1    # Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    iget v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->type:I

    invoke-virtual {p1, v0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->setEventType(I)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->timestamp:J

    invoke-virtual {v0, v1, v2}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->setClientTimeMs(J)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    iget v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->source:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->source:I

    invoke-virtual {p1, v0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->setEventSource(I)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->data:Ljava/lang/Object;

    instance-of v0, v0, Lcom/google/android/searchcommon/util/LatencyTracker$EventList;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    iget-wide v2, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->timestamp:J

    iget-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->data:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/searchcommon/util/LatencyTracker$EventList;

    # invokes: Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->toLatencyData(JLcom/google/android/searchcommon/util/LatencyTracker$EventList;)Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;
    invoke-static {v1, v2, v3, v0}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->access$1800(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;JLcom/google/android/searchcommon/util/LatencyTracker$EventList;)Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->setLatency(Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    :cond_1
    iget-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->data:Ljava/lang/Object;

    instance-of v0, v0, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->data:Ljava/lang/Object;

    check-cast v0, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;

    invoke-virtual {p1, v0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->setLatency(Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    :cond_2
    return-void
.end method

.method public setValues(IJLjava/lang/Object;)V
    .locals 1
    .param p1    # I
    .param p2    # J
    .param p4    # Ljava/lang/Object;

    const/high16 v0, -0x10000000

    and-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->group:I

    const/high16 v0, 0xf000000

    and-int/2addr v0, p1

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->getSource(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->source:I

    const v0, 0xffffff

    and-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->type:I

    iput-wide p2, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->timestamp:J

    iput-object p4, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->data:Ljava/lang/Object;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LoggedEvent[g="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->group:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",s="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->source:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",t="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->type:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",ts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->timestamp:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
