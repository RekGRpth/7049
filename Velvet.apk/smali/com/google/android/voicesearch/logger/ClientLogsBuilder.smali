.class Lcom/google/android/voicesearch/logger/ClientLogsBuilder;
.super Ljava/lang/Object;
.source "ClientLogsBuilder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/logger/ClientLogsBuilder$1;,
        Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;,
        Lcom/google/android/voicesearch/logger/ClientLogsBuilder$AudioInputDeviceProcessor;,
        Lcom/google/android/voicesearch/logger/ClientLogsBuilder$GrammarCompilerProcessor;,
        Lcom/google/android/voicesearch/logger/ClientLogsBuilder$OnDeviceSourceProcessor;,
        Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ContactInfoProcessor;,
        Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EmbeddedRecognizerProcessor;,
        Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;,
        Lcom/google/android/voicesearch/logger/ClientLogsBuilder$FilterOutLatencyProcessor;,
        Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ScreenProcessor;,
        Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;,
        Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ActionTypeProcessor;,
        Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ApplicationNameProcessor;,
        Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventTypeProcessor;,
        Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;
    }
.end annotation


# static fields
.field private static final EMPTY_BREAKDOWN_EVENTS:[I

.field private static final EMPTY_REQUEST_DATA:[Ljava/lang/Object;


# instance fields
.field private final mBaseClientLog:Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

.field private mClientLog:Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

.field private final mClientLogs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;",
            ">;"
        }
    .end annotation
.end field

.field private final mContactSyncLookup:Lcom/google/android/speech/contacts/ContactSyncLookup;

.field private final mEventProcessor:Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;

.field private mRequestId:Ljava/lang/String;

.field private mRequestType:I

.field private final mSublatencyCalculator:Lcom/google/android/voicesearch/logger/SublatencyCalculator;

.field private mTimeDelta:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    new-array v0, v1, [Ljava/lang/Object;

    sput-object v0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->EMPTY_REQUEST_DATA:[Ljava/lang/Object;

    new-array v0, v1, [I

    sput-object v0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->EMPTY_BREAKDOWN_EVENTS:[I

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZLjava/lang/String;Lcom/google/android/speech/contacts/ContactSyncLookup;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # I
    .param p6    # I
    .param p7    # Z
    .param p8    # Ljava/lang/String;
    .param p9    # Lcom/google/android/speech/contacts/ContactSyncLookup;

    const/4 v5, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-direct/range {p0 .. p8}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->createBaseClientLog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZLjava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->mBaseClientLog:Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->mClientLogs:Ljava/util/ArrayList;

    new-instance v3, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    invoke-direct {v3}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;-><init>()V

    iput-object v3, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->mClientLog:Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    iput-object p9, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->mContactSyncLookup:Lcom/google/android/speech/contacts/ContactSyncLookup;

    :try_start_0
    iget-object v3, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->mClientLog:Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    iget-object v4, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->mBaseClientLog:Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    invoke-virtual {v4}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->toByteArray()[B

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    new-instance v3, Lcom/google/android/voicesearch/logger/SublatencyCalculator;

    invoke-direct {v3}, Lcom/google/android/voicesearch/logger/SublatencyCalculator;-><init>()V

    iput-object v3, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->mSublatencyCalculator:Lcom/google/android/voicesearch/logger/SublatencyCalculator;

    const/4 v1, 0x0

    new-instance v2, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventTypeProcessor;

    invoke-direct {v2, p0, v1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventTypeProcessor;-><init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;)V

    new-instance v1, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ApplicationNameProcessor;

    invoke-direct {v1, p0, v2, v5}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ApplicationNameProcessor;-><init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$1;)V

    new-instance v2, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$GrammarCompilerProcessor;

    invoke-direct {v2, p0, v1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$GrammarCompilerProcessor;-><init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;)V

    new-instance v1, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;

    invoke-direct {v1, p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LatencyProcessor;-><init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;)V

    new-instance v2, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$FilterOutLatencyProcessor;

    invoke-direct {v2, p0, v1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$FilterOutLatencyProcessor;-><init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;)V

    new-instance v1, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ScreenProcessor;

    invoke-direct {v1, p0, v2, v5}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ScreenProcessor;-><init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$1;)V

    new-instance v2, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;

    invoke-direct {v2, p0, v1, v5}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$RequestDataProcessor;-><init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$1;)V

    new-instance v1, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ActionTypeProcessor;

    invoke-direct {v1, p0, v2, v5}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ActionTypeProcessor;-><init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$1;)V

    new-instance v2, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EmbeddedRecognizerProcessor;

    invoke-direct {v2, p0, v1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EmbeddedRecognizerProcessor;-><init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;)V

    new-instance v1, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$AudioInputDeviceProcessor;

    invoke-direct {v1, p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$AudioInputDeviceProcessor;-><init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;)V

    new-instance v2, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ContactInfoProcessor;

    invoke-direct {v2, p0, v1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ContactInfoProcessor;-><init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;)V

    new-instance v1, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$OnDeviceSourceProcessor;

    invoke-direct {v1, p0, v2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$OnDeviceSourceProcessor;-><init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;)V

    iput-object v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->mEventProcessor:Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;

    return-void

    :catch_0
    move-exception v0

    new-instance v3, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    invoke-direct {v3}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;-><init>()V

    iput-object v3, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->mClientLog:Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    goto :goto_0
.end method

.method static synthetic access$1100()[Ljava/lang/Object;
    .locals 1

    sget-object v0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->EMPTY_REQUEST_DATA:[Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    iget-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->mRequestId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->mRequestId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;)I
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    iget v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->mRequestType:I

    return v0
.end method

.method static synthetic access$1302(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;I)I
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder;
    .param p1    # I

    iput p1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->mRequestType:I

    return p1
.end method

.method static synthetic access$1500(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;)Lcom/google/android/voicesearch/logger/SublatencyCalculator;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    iget-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->mSublatencyCalculator:Lcom/google/android/voicesearch/logger/SublatencyCalculator;

    return-object v0
.end method

.method static synthetic access$1600()[I
    .locals 1

    sget-object v0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->EMPTY_BREAKDOWN_EVENTS:[I

    return-object v0
.end method

.method static synthetic access$1700(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;)Lcom/google/android/speech/contacts/ContactSyncLookup;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    iget-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->mContactSyncLookup:Lcom/google/android/speech/contacts/ContactSyncLookup;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;JLcom/google/android/searchcommon/util/LatencyTracker$EventList;)Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder;
    .param p1    # J
    .param p3    # Lcom/google/android/searchcommon/util/LatencyTracker$EventList;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->toLatencyData(JLcom/google/android/searchcommon/util/LatencyTracker$EventList;)Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder;
    .param p1    # Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->addClientEvent(Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->updateApplicationIds(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private addClientEvent(Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)V
    .locals 1
    .param p1    # Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    iget-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->mClientLog:Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    invoke-virtual {v0, p1}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->addBundledClientEvents(Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    return-void
.end method

.method private addCurrentClientLog()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->mClientLog:Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    invoke-virtual {v1}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->getBundledClientEventsCount()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->mClientLogs:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->mClientLog:Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    new-instance v1, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    invoke-direct {v1}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;-><init>()V

    iput-object v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->mClientLog:Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    :try_start_0
    iget-object v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->mClientLog:Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    iget-object v2, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->mBaseClientLog:Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    invoke-virtual {v2}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->toByteArray()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    invoke-direct {v1}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;-><init>()V

    iput-object v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->mClientLog:Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    goto :goto_0
.end method

.method private createBaseClientLog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZLjava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # I
    .param p6    # I
    .param p7    # Z
    .param p8    # Ljava/lang/String;

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "Android"

    sget-object v2, Landroid/os/Build;->DISPLAY:Ljava/lang/String;

    new-instance v3, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    invoke-direct {v3}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;-><init>()V

    invoke-virtual {v3, p1}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->setApplicationVersion(Ljava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    invoke-virtual {v3, p2}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->setApplicationVersionName(Ljava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    invoke-virtual {v3, v0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->setDeviceModel(Ljava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    invoke-virtual {v3, p3}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->setInstallId(Ljava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->setLocale(Ljava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    invoke-virtual {v3, v1}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->setPlatformId(Ljava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    invoke-virtual {v3, v2}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->setPlatformVersion(Ljava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    invoke-virtual {v3, p4}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->setPackageId(Ljava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    invoke-virtual {v3, p5}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->setImeLangCount(I)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    invoke-virtual {v3, p6}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->setVoicesearchLangCount(I)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    invoke-virtual {v3, p7}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->setPairedBluetooth(Z)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    invoke-virtual {v3, p8}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->addExperimentId(Ljava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    return-object v3
.end method

.method private toLatencyData(JLcom/google/android/searchcommon/util/LatencyTracker$EventList;)Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;
    .locals 8
    .param p1    # J
    .param p3    # Lcom/google/android/searchcommon/util/LatencyTracker$EventList;

    invoke-static {p3}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v3, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;

    invoke-direct {v3}, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;-><init>()V

    iget-object v4, p3, Lcom/google/android/searchcommon/util/LatencyTracker$EventList;->events:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/searchcommon/util/LatencyTracker$Event;

    new-instance v0, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyBreakdownEvent;

    invoke-direct {v0}, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyBreakdownEvent;-><init>()V

    iget v4, v1, Lcom/google/android/searchcommon/util/LatencyTracker$Event;->type:I

    invoke-virtual {v0, v4}, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyBreakdownEvent;->setEvent(I)Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyBreakdownEvent;

    iget v4, v1, Lcom/google/android/searchcommon/util/LatencyTracker$Event;->latency:I

    invoke-virtual {v0, v4}, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyBreakdownEvent;->setOffsetMsec(I)Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyBreakdownEvent;

    invoke-virtual {v3, v0}, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->addBreakdown(Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyBreakdownEvent;)Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;

    iget-wide v4, p3, Lcom/google/android/searchcommon/util/LatencyTracker$EventList;->baseTimestamp:J

    iget-wide v6, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->mTimeDelta:J

    add-long/2addr v4, v6

    sub-long v4, p1, v4

    long-to-int v4, v4

    invoke-virtual {v3, v4}, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->setDurationMsec(I)Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;

    goto :goto_0

    :cond_0
    iget-object v4, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->mSublatencyCalculator:Lcom/google/android/voicesearch/logger/SublatencyCalculator;

    invoke-virtual {v4, v3}, Lcom/google/android/voicesearch/logger/SublatencyCalculator;->addBreakDownSublatency(Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;)V

    return-object v3
.end method

.method private updateApplicationIds(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->addCurrentClientLog()V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->mClientLog:Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    invoke-virtual {v0, p1}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->setApplicationId(Ljava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    :cond_0
    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->mClientLog:Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    invoke-virtual {v0, p2}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->setTriggerApplicationId(Ljava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    :cond_1
    return-void
.end method


# virtual methods
.method public build(Lcom/google/android/voicesearch/logger/store/MainEventLoggerStore$Results;)Ljava/util/ArrayList;
    .locals 8
    .param p1    # Lcom/google/android/voicesearch/logger/store/MainEventLoggerStore$Results;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/voicesearch/logger/store/MainEventLoggerStore$Results;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;",
            ">;"
        }
    .end annotation

    new-instance v2, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;-><init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$1;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    sub-long/2addr v3, v5

    iput-wide v3, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->mTimeDelta:J

    invoke-interface {p1}, Lcom/google/android/voicesearch/logger/store/MainEventLoggerStore$Results;->size()I

    move-result v3

    if-lez v3, :cond_2

    new-instance v0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    invoke-direct {v0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;-><init>()V

    const/4 v1, 0x0

    :goto_0
    invoke-interface {p1}, Lcom/google/android/voicesearch/logger/store/MainEventLoggerStore$Results;->size()I

    move-result v3

    if-ge v1, v3, :cond_1

    invoke-interface {p1, v1}, Lcom/google/android/voicesearch/logger/store/MainEventLoggerStore$Results;->getEvent(I)I

    move-result v3

    invoke-interface {p1, v1}, Lcom/google/android/voicesearch/logger/store/MainEventLoggerStore$Results;->getTime(I)J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->mTimeDelta:J

    add-long/2addr v4, v6

    invoke-interface {p1, v1}, Lcom/google/android/voicesearch/logger/store/MainEventLoggerStore$Results;->getData(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->setValues(IJLjava/lang/Object;)V

    iget-object v3, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->mEventProcessor:Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;

    invoke-virtual {v3, v2, v0}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;->process(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    invoke-direct {v0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;-><init>()V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->addCurrentClientLog()V

    :cond_2
    iget-object v3, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->mClientLogs:Ljava/util/ArrayList;

    return-object v3
.end method
