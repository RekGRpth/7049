.class Lcom/google/android/voicesearch/logger/EventLoggerService$1;
.super Ljava/lang/Object;
.source "EventLoggerService.java"

# interfaces
.implements Lcom/google/common/base/Supplier;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/logger/EventLoggerService;->onCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Supplier",
        "<",
        "Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/logger/EventLoggerService;

.field final synthetic val$app:Lcom/google/android/velvet/VelvetApplication;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/logger/EventLoggerService;Lcom/google/android/velvet/VelvetApplication;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/logger/EventLoggerService$1;->this$0:Lcom/google/android/voicesearch/logger/EventLoggerService;

    iput-object p2, p0, Lcom/google/android/voicesearch/logger/EventLoggerService$1;->val$app:Lcom/google/android/velvet/VelvetApplication;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public get()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/logger/EventLoggerService$1;->val$app:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getGogglesSettings()Lcom/google/android/goggles/GogglesSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/goggles/GogglesSettings;->getLogServerInfo()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/voicesearch/logger/EventLoggerService$1;->get()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;

    move-result-object v0

    return-object v0
.end method
