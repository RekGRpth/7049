.class public Lcom/google/android/voicesearch/logger/EventLoggerService;
.super Landroid/app/IntentService;
.source "EventLoggerService.java"


# static fields
.field public static SEND_EVENTS:Ljava/lang/String;


# instance fields
.field private mApplicationVersionCode:Ljava/lang/String;

.field private mApplicationVersionName:Ljava/lang/String;

.field private mBluetoothHeadsetQuery:Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery;

.field private mDefaultSpokenLanguageSupplier:Lcom/google/common/base/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mLogSender:Lcom/google/android/speech/logger/LogSender;

.field private mSettings:Lcom/google/android/voicesearch/settings/Settings;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "send_events"

    sput-object v0, Lcom/google/android/voicesearch/logger/EventLoggerService;->SEND_EVENTS:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const-string v0, "EventLoggerService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method private addStoredClientLogs(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;",
            ">;)V"
        }
    .end annotation

    const/4 v1, 0x0

    :try_start_0
    new-instance v2, Ljava/io/DataInputStream;

    const-string v3, "event_logger_buffer"

    invoke-virtual {p0, v3}, Lcom/google/android/voicesearch/logger/EventLoggerService;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    :try_start_1
    invoke-virtual {v2}, Ljava/io/DataInputStream;->available()I

    move-result v3

    if-lez v3, :cond_0

    const/4 v3, 0x0

    invoke-static {v2}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->newInstance(Ljava/io/InputStream;)Lcom/google/protobuf/micro/CodedInputStreamMicro;

    move-result-object v4

    invoke-static {v4}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->parseFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    :catch_0
    move-exception v3

    move-object v1, v2

    :goto_1
    invoke-static {v1}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    :goto_2
    invoke-direct {p0}, Lcom/google/android/voicesearch/logger/EventLoggerService;->deleteBufferFile()V

    return-void

    :cond_0
    invoke-static {v2}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    move-object v1, v2

    goto :goto_2

    :catch_1
    move-exception v0

    :goto_3
    :try_start_2
    const-string v3, "EventLoggerService"

    const-string v4, "Unable to read the stored client logs"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-static {v1}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_2

    :catchall_0
    move-exception v3

    :goto_4
    invoke-static {v1}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    throw v3

    :catchall_1
    move-exception v3

    move-object v1, v2

    goto :goto_4

    :catch_2
    move-exception v0

    move-object v1, v2

    goto :goto_3

    :catch_3
    move-exception v3

    goto :goto_1
.end method

.method public static cancelSendEvents(Landroid/content/Context;)V
    .locals 2
    .param p0    # Landroid/content/Context;

    const-string v1, "alarm"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    invoke-static {p0}, Lcom/google/android/voicesearch/logger/EventLoggerService;->createPendingIntent(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    return-void
.end method

.method private static createPendingIntent(Landroid/content/Context;)Landroid/app/PendingIntent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/voicesearch/logger/EventLoggerService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v1, Lcom/google/android/voicesearch/logger/EventLoggerService;->SEND_EVENTS:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const/4 v1, 0x0

    const/high16 v2, 0x10000000

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private deleteBufferFile()V
    .locals 3

    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/logger/EventLoggerService;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "event_logger_buffer"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    :cond_0
    return-void
.end method

.method private getImeLangCount()I
    .locals 4

    const-string v2, "input_method"

    invoke-virtual {p0, v2}, Lcom/google/android/voicesearch/logger/EventLoggerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->getEnabledInputMethodSubtypeList(Landroid/view/inputmethod/InputMethodInfo;Z)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    return v2
.end method

.method private getVoiceSearchLangCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mDefaultSpokenLanguageSupplier:Lcom/google/common/base/Supplier;

    invoke-interface {v0}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static scheduleSendEvents(Landroid/content/Context;)V
    .locals 7
    .param p0    # Landroid/content/Context;

    invoke-static {p0}, Lcom/google/android/voicesearch/logger/EventLoggerService;->createPendingIntent(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v1

    const-string v2, "alarm"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    const/4 v2, 0x3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    const-wide/16 v5, 0x2710

    add-long/2addr v3, v5

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    return-void
.end method

.method private sendClientLogs(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesClientLog;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/logger/EventLoggerService;->addStoredClientLogs(Ljava/util/ArrayList;)V

    :try_start_0
    invoke-static {}, Landroid/app/ActivityManager;->isUserAMonkey()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Landroid/app/ActivityManager;->isRunningInTestHarness()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mLogSender:Lcom/google/android/speech/logger/LogSender;

    invoke-interface {v1, p1, p2}, Lcom/google/android/speech/logger/LogSender;->send(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "EventLoggerService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to send logs "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/logger/EventLoggerService;->storeClientLogs(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method private sendEvents()V
    .locals 12

    iget-object v0, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mBluetoothHeadsetQuery:Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery;

    invoke-virtual {v0, p0}, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery;->getPairedHeadset(Landroid/content/Context;)Ljava/util/concurrent/Future;

    move-result-object v11

    const/4 v7, 0x0

    const-wide/16 v0, 0x2

    :try_start_0
    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v11, v0, v1, v2}, Ljava/util/concurrent/Future;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;->FOUND:Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    if-ne v0, v1, :cond_0

    const/4 v7, 0x1

    :goto_0
    new-instance v9, Lcom/google/android/speech/contacts/ContactSyncLookup;

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/logger/EventLoggerService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v9, v0, v1}, Lcom/google/android/speech/contacts/ContactSyncLookup;-><init>(Landroid/accounts/AccountManager;Landroid/content/ContentResolver;)V

    new-instance v0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    iget-object v1, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mApplicationVersionCode:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mApplicationVersionName:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v3}, Lcom/google/android/voicesearch/settings/Settings;->getInstallId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/voicesearch/logger/EventLoggerService;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0}, Lcom/google/android/voicesearch/logger/EventLoggerService;->getImeLangCount()I

    move-result v5

    invoke-direct {p0}, Lcom/google/android/voicesearch/logger/EventLoggerService;->getVoiceSearchLangCount()I

    move-result v6

    iget-object v8, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v8}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getId()Ljava/lang/String;

    move-result-object v8

    invoke-direct/range {v0 .. v9}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZLjava/lang/String;Lcom/google/android/speech/contacts/ContactSyncLookup;)V

    invoke-static {}, Lcom/google/android/voicesearch/logger/store/EventLoggerStores;->getMainEventLoggerStore()Lcom/google/android/voicesearch/logger/store/MainEventLoggerStore;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/voicesearch/logger/store/MainEventLoggerStore;->getAndClearResults()Lcom/google/android/voicesearch/logger/store/MainEventLoggerStore$Results;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder;->build(Lcom/google/android/voicesearch/logger/store/MainEventLoggerStore$Results;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {}, Lcom/google/android/goggles/TraceTracker;->getMainTraceTracker()Lcom/google/android/goggles/TraceTracker;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/goggles/TraceTracker;->buildAndClear()Ljava/util/ArrayList;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/voicesearch/logger/EventLoggerService;->sendClientLogs(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    :goto_1
    return-void

    :cond_0
    const/4 v7, 0x0

    goto :goto_0

    :catch_0
    move-exception v10

    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_0

    :catch_2
    move-exception v0

    goto :goto_0
.end method

.method private storeClientLogs(Ljava/util/ArrayList;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;",
            ">;)V"
        }
    .end annotation

    const/4 v5, 0x0

    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/logger/EventLoggerService;->getFilesDir()Ljava/io/File;

    move-result-object v6

    const-string v7, "event_logger_buffer"

    invoke-direct {v0, v6, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const-wide/16 v1, 0x0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v1

    :cond_0
    const-string v6, "event_logger_buffer"

    const v7, 0x8000

    invoke-virtual {p0, v6, v7}, Lcom/google/android/voicesearch/logger/EventLoggerService;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v5

    const/4 v4, 0x0

    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v4, v6, :cond_1

    const-wide/16 v6, 0x2800

    cmp-long v6, v1, v6

    if-gez v6, :cond_1

    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    invoke-virtual {v6}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->toByteArray()[B

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/FileOutputStream;->write([B)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->flush()V

    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v5}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    :goto_1
    return-void

    :catch_0
    move-exception v3

    :try_start_1
    const-string v6, "EventLoggerService"

    const-string v7, "Error saving logs"

    invoke-static {v6, v7, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {v5}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_1

    :catchall_0
    move-exception v6

    invoke-static {v5}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    throw v6
.end method


# virtual methods
.method public onCreate()V
    .locals 7

    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    invoke-static {p0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getVoiceSearchServices()Lcom/google/android/voicesearch/VoiceSearchServices;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/voicesearch/VoiceSearchServices;->getSettings()Lcom/google/android/voicesearch/settings/Settings;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    new-instance v3, Lcom/google/android/speech/logs/S3LogSender;

    iget-object v4, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-static {v4}, Lcom/google/android/voicesearch/settings/SettingsUtils;->getSingleHttpServerInfoSupplier(Lcom/google/android/voicesearch/settings/Settings;)Lcom/google/common/base/Supplier;

    move-result-object v4

    new-instance v5, Lcom/google/android/voicesearch/logger/EventLoggerService$1;

    invoke-direct {v5, p0, v0}, Lcom/google/android/voicesearch/logger/EventLoggerService$1;-><init>(Lcom/google/android/voicesearch/logger/EventLoggerService;Lcom/google/android/velvet/VelvetApplication;)V

    invoke-virtual {v2}, Lcom/google/android/voicesearch/VoiceSearchServices;->getConnectionFactory()Lcom/google/android/speech/network/ConnectionFactory;

    move-result-object v6

    invoke-direct {v3, v4, v5, v6}, Lcom/google/android/speech/logs/S3LogSender;-><init>(Lcom/google/common/base/Supplier;Lcom/google/common/base/Supplier;Lcom/google/android/speech/network/ConnectionFactory;)V

    iput-object v3, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mLogSender:Lcom/google/android/speech/logger/LogSender;

    invoke-static {p0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/velvet/VelvetApplication;->getVersionCodeString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mApplicationVersionCode:Ljava/lang/String;

    invoke-static {p0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/velvet/VelvetApplication;->getVersionName()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mApplicationVersionName:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-static {v3}, Lcom/google/android/voicesearch/settings/SettingsUtils;->getDefaultSpokenLanguageSupplier(Lcom/google/android/voicesearch/settings/Settings;)Lcom/google/common/base/Supplier;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mDefaultSpokenLanguageSupplier:Lcom/google/common/base/Supplier;

    const-string v3, "audio"

    invoke-virtual {p0, v3}, Lcom/google/android/voicesearch/logger/EventLoggerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    new-instance v3, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery;

    invoke-virtual {v2}, Lcom/google/android/voicesearch/VoiceSearchServices;->getScheduledExecutorService()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v4

    invoke-direct {v3, v1, v4}, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery;-><init>(Landroid/media/AudioManager;Ljava/util/concurrent/ScheduledExecutorService;)V

    iput-object v3, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mBluetoothHeadsetQuery:Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery;

    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1    # Landroid/content/Intent;

    invoke-direct {p0}, Lcom/google/android/voicesearch/logger/EventLoggerService;->sendEvents()V

    return-void
.end method
