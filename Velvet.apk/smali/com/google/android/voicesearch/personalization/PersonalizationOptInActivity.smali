.class public Lcom/google/android/voicesearch/personalization/PersonalizationOptInActivity;
.super Landroid/app/Activity;
.source "PersonalizationOptInActivity.java"


# instance fields
.field private mDialogHelper:Lcom/google/android/voicesearch/personalization/PersonalizationDialogHelper;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getVoiceSearchServices()Lcom/google/android/voicesearch/VoiceSearchServices;

    move-result-object v0

    new-instance v1, Lcom/google/android/voicesearch/personalization/PersonalizationDialogHelper;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/VoiceSearchServices;->getAccountHelper()Lcom/google/android/speech/helper/AccountHelper;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/voicesearch/VoiceSearchServices;->getSettings()Lcom/google/android/voicesearch/settings/Settings;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/voicesearch/VoiceSearchServices;->getPersonalizationPrefManager()Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;

    move-result-object v4

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/google/android/voicesearch/personalization/PersonalizationDialogHelper;-><init>(Landroid/content/Context;Lcom/google/android/speech/helper/AccountHelper;Lcom/google/android/voicesearch/settings/Settings;Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;)V

    iput-object v1, p0, Lcom/google/android/voicesearch/personalization/PersonalizationOptInActivity;->mDialogHelper:Lcom/google/android/voicesearch/personalization/PersonalizationDialogHelper;

    return-void
.end method

.method protected onResume()V
    .locals 6

    const/4 v3, 0x0

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/voicesearch/personalization/PersonalizationOptInActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "PERSONALIZATION_OPT_IN_ENABLE"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/personalization/PersonalizationOptInActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "PERSONALIZATION_OPT_IN_ENABLE"

    invoke-virtual {v4, v5, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    move v1, v3

    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/google/android/voicesearch/personalization/PersonalizationOptInActivity;->mDialogHelper:Lcom/google/android/voicesearch/personalization/PersonalizationDialogHelper;

    new-instance v4, Lcom/google/android/voicesearch/personalization/PersonalizationOptInActivity$1;

    invoke-direct {v4, p0}, Lcom/google/android/voicesearch/personalization/PersonalizationOptInActivity$1;-><init>(Lcom/google/android/voicesearch/personalization/PersonalizationOptInActivity;)V

    invoke-virtual {v3, v1, v4}, Lcom/google/android/voicesearch/personalization/PersonalizationDialogHelper;->createDialog(ILcom/google/android/voicesearch/personalization/PersonalizationDialogHelper$Callbacks;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void

    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method
