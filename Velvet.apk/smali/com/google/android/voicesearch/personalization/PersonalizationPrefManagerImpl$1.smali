.class Lcom/google/android/voicesearch/personalization/PersonalizationPrefManagerImpl$1;
.super Ljava/lang/Object;
.source "PersonalizationPrefManagerImpl.java"

# interfaces
.implements Lcom/google/common/base/Supplier;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/personalization/PersonalizationPrefManagerImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Supplier",
        "<",
        "Landroid/content/SharedPreferences;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/personalization/PersonalizationPrefManagerImpl;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/personalization/PersonalizationPrefManagerImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/personalization/PersonalizationPrefManagerImpl$1;->this$0:Lcom/google/android/voicesearch/personalization/PersonalizationPrefManagerImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public get()Landroid/content/SharedPreferences;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/personalization/PersonalizationPrefManagerImpl$1;->this$0:Lcom/google/android/voicesearch/personalization/PersonalizationPrefManagerImpl;

    # getter for: Lcom/google/android/voicesearch/personalization/PersonalizationPrefManagerImpl;->mPreferenceController:Lcom/google/android/searchcommon/GsaPreferenceController;
    invoke-static {v0}, Lcom/google/android/voicesearch/personalization/PersonalizationPrefManagerImpl;->access$000(Lcom/google/android/voicesearch/personalization/PersonalizationPrefManagerImpl;)Lcom/google/android/searchcommon/GsaPreferenceController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/GsaPreferenceController;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/voicesearch/personalization/PersonalizationPrefManagerImpl$1;->get()Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method
