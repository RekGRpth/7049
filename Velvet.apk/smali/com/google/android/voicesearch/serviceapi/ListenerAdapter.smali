.class Lcom/google/android/voicesearch/serviceapi/ListenerAdapter;
.super Lcom/google/android/speech/listeners/RecognitionEventListenerAdapter;
.source "ListenerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/serviceapi/ListenerAdapter$OnDoneListener;
    }
.end annotation


# instance fields
.field private final mCallback:Landroid/speech/RecognitionService$Callback;

.field private final mOnDoneListener:Lcom/google/android/voicesearch/serviceapi/ListenerAdapter$OnDoneListener;

.field private final mPartialsRequested:Z

.field private mRecognitionCompleteReceived:Z

.field private final mRecognizedText:Lcom/google/android/speech/utils/RecognizedText;

.field private final mThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;


# direct methods
.method public constructor <init>(Landroid/speech/RecognitionService$Callback;Lcom/google/android/voicesearch/serviceapi/ListenerAdapter$OnDoneListener;Z)V
    .locals 1
    .param p1    # Landroid/speech/RecognitionService$Callback;
    .param p2    # Lcom/google/android/voicesearch/serviceapi/ListenerAdapter$OnDoneListener;
    .param p3    # Z

    invoke-direct {p0}, Lcom/google/android/speech/listeners/RecognitionEventListenerAdapter;-><init>()V

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/speech/RecognitionService$Callback;

    iput-object v0, p0, Lcom/google/android/voicesearch/serviceapi/ListenerAdapter;->mCallback:Landroid/speech/RecognitionService$Callback;

    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/serviceapi/ListenerAdapter$OnDoneListener;

    iput-object v0, p0, Lcom/google/android/voicesearch/serviceapi/ListenerAdapter;->mOnDoneListener:Lcom/google/android/voicesearch/serviceapi/ListenerAdapter$OnDoneListener;

    iput-boolean p3, p0, Lcom/google/android/voicesearch/serviceapi/ListenerAdapter;->mPartialsRequested:Z

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->createSameThreadCheck()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/serviceapi/ListenerAdapter;->mThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    new-instance v0, Lcom/google/android/speech/utils/RecognizedText;

    invoke-direct {v0}, Lcom/google/android/speech/utils/RecognizedText;-><init>()V

    iput-object v0, p0, Lcom/google/android/voicesearch/serviceapi/ListenerAdapter;->mRecognizedText:Lcom/google/android/speech/utils/RecognizedText;

    return-void
.end method


# virtual methods
.method public onBeginningOfSpeech()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/voicesearch/serviceapi/ListenerAdapter;->mThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    :try_start_0
    iget-object v1, p0, Lcom/google/android/voicesearch/serviceapi/ListenerAdapter;->mCallback:Landroid/speech/RecognitionService$Callback;

    invoke-virtual {v1}, Landroid/speech/RecognitionService$Callback;->beginningOfSpeech()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "ListenerAdapter"

    const-string v2, "beginningOfSpeech callback failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public onDone()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/voicesearch/serviceapi/ListenerAdapter;->mThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    iget-boolean v1, p0, Lcom/google/android/voicesearch/serviceapi/ListenerAdapter;->mRecognitionCompleteReceived:Z

    if-nez v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/voicesearch/serviceapi/ListenerAdapter;->mCallback:Landroid/speech/RecognitionService$Callback;

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Landroid/speech/RecognitionService$Callback;->error(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/voicesearch/serviceapi/ListenerAdapter;->mOnDoneListener:Lcom/google/android/voicesearch/serviceapi/ListenerAdapter$OnDoneListener;

    invoke-interface {v1}, Lcom/google/android/voicesearch/serviceapi/ListenerAdapter$OnDoneListener;->onDone()V

    return-void

    :catch_0
    move-exception v0

    const-string v1, "ListenerAdapter"

    const-string v2, "#error remote callback failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public onEndOfSpeech()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/voicesearch/serviceapi/ListenerAdapter;->mThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    :try_start_0
    iget-object v1, p0, Lcom/google/android/voicesearch/serviceapi/ListenerAdapter;->mCallback:Landroid/speech/RecognitionService$Callback;

    invoke-virtual {v1}, Landroid/speech/RecognitionService$Callback;->endOfSpeech()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "ListenerAdapter"

    const-string v2, "endOfSpeech callback failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public onError(Lcom/google/android/speech/exception/RecognizeException;)V
    .locals 3
    .param p1    # Lcom/google/android/speech/exception/RecognizeException;

    iget-object v1, p0, Lcom/google/android/voicesearch/serviceapi/ListenerAdapter;->mThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "ListenerAdapter"

    const-string v2, "onError"

    invoke-static {v1, v2, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/16 v1, 0x3b

    :try_start_0
    invoke-static {v1}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    iget-object v1, p0, Lcom/google/android/voicesearch/serviceapi/ListenerAdapter;->mCallback:Landroid/speech/RecognitionService$Callback;

    invoke-static {p1}, Lcom/google/android/voicesearch/util/ErrorUtils;->getSpeechRecognizerError(Lcom/google/android/speech/exception/RecognizeException;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/speech/RecognitionService$Callback;->error(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "ListenerAdapter"

    const-string v2, "error callback failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public onReadyForSpeech(FF)V
    .locals 4
    .param p1    # F
    .param p2    # F

    iget-object v2, p0, Lcom/google/android/voicesearch/serviceapi/ListenerAdapter;->mThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v2}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    :try_start_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    iget-object v2, p0, Lcom/google/android/voicesearch/serviceapi/ListenerAdapter;->mCallback:Landroid/speech/RecognitionService$Callback;

    invoke-virtual {v2, v1}, Landroid/speech/RecognitionService$Callback;->readyForSpeech(Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v2, "ListenerAdapter"

    const-string v3, "readyForSpeech callback failed"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public onRecognitionResult(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)V
    .locals 12
    .param p1    # Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    const/4 v11, 0x1

    iget-object v9, p0, Lcom/google/android/voicesearch/serviceapi/ListenerAdapter;->mThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v9}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {p1}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->getEventType()I

    move-result v9

    if-nez v9, :cond_0

    iget-boolean v9, p0, Lcom/google/android/voicesearch/serviceapi/ListenerAdapter;->mPartialsRequested:Z

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/google/android/voicesearch/serviceapi/ListenerAdapter;->mRecognizedText:Lcom/google/android/speech/utils/RecognizedText;

    invoke-virtual {v9, p1}, Lcom/google/android/speech/utils/RecognizedText;->update(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)Landroid/util/Pair;

    move-result-object v8

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7, v11}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v9, v8, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v9, "results_recognition"

    invoke-virtual {v0, v9, v7}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    :try_start_0
    iget-object v9, p0, Lcom/google/android/voicesearch/serviceapi/ListenerAdapter;->mCallback:Landroid/speech/RecognitionService$Callback;

    invoke-virtual {v9, v0}, Landroid/speech/RecognitionService$Callback;->partialResults(Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->getEventType()I

    move-result v9

    if-ne v9, v11, :cond_2

    invoke-virtual {p1}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->hasCombinedResult()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-virtual {p1}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->getCombinedResult()Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->getHypothesisCount()I

    move-result v9

    if-lez v9, :cond_2

    iput-boolean v11, p0, Lcom/google/android/voicesearch/serviceapi/ListenerAdapter;->mRecognitionCompleteReceived:Z

    invoke-virtual {p1}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->getCombinedResult()Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->getHypothesisList()Ljava/util/List;

    move-result-object v4

    new-instance v6, Ljava/util/ArrayList;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v9

    invoke-direct {v6, v9}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v9

    new-array v1, v9, [F

    const/4 v5, 0x0

    :goto_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v9

    if-ge v5, v9, :cond_1

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;

    invoke-virtual {v3}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->getText()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v3}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->getConfidence()F

    move-result v9

    aput v9, v1, v5

    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :catch_0
    move-exception v2

    const-string v9, "ListenerAdapter"

    const-string v10, "#partialResults remote callback failed"

    invoke-static {v9, v10, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v9, "results_recognition"

    invoke-virtual {v0, v9, v6}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v9, "confidence_scores"

    invoke-virtual {v0, v9, v1}, Landroid/os/Bundle;->putFloatArray(Ljava/lang/String;[F)V

    const/16 v9, 0x3c

    invoke-static {v9}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    :try_start_1
    iget-object v9, p0, Lcom/google/android/voicesearch/serviceapi/ListenerAdapter;->mCallback:Landroid/speech/RecognitionService$Callback;

    invoke-virtual {v9, v0}, Landroid/speech/RecognitionService$Callback;->results(Landroid/os/Bundle;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_2
    :goto_2
    return-void

    :catch_1
    move-exception v2

    const-string v9, "ListenerAdapter"

    const-string v10, "#result remote callback failed"

    invoke-static {v9, v10, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2
.end method

.method public sendRmsValue(F)V
    .locals 3
    .param p1    # F

    :try_start_0
    iget-object v1, p0, Lcom/google/android/voicesearch/serviceapi/ListenerAdapter;->mCallback:Landroid/speech/RecognitionService$Callback;

    invoke-virtual {v1, p1}, Landroid/speech/RecognitionService$Callback;->rmsChanged(F)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "ListenerAdapter"

    const-string v2, "rmsChanged callback failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
