.class Lcom/google/android/voicesearch/settings/GStaticConfiguration$1;
.super Ljava/lang/Object;
.source "GStaticConfiguration.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/settings/GStaticConfiguration;-><init>(Lcom/google/android/searchcommon/GsaPreferenceController;Landroid/content/res/Resources;Ljava/util/concurrent/ExecutorService;Lcom/google/android/searchcommon/GserviceWrapper;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/settings/GStaticConfiguration;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/settings/GStaticConfiguration;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/settings/GStaticConfiguration$1;->this$0:Lcom/google/android/voicesearch/settings/GStaticConfiguration;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/voicesearch/settings/GStaticConfiguration$1;->this$0:Lcom/google/android/voicesearch/settings/GStaticConfiguration;

    # getter for: Lcom/google/android/voicesearch/settings/GStaticConfiguration;->mLoadingLock:Ljava/lang/Object;
    invoke-static {v1}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->access$000(Lcom/google/android/voicesearch/settings/GStaticConfiguration;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/google/android/voicesearch/settings/GStaticConfiguration$1;->this$0:Lcom/google/android/voicesearch/settings/GStaticConfiguration;

    # getter for: Lcom/google/android/voicesearch/settings/GStaticConfiguration;->mConfiguration:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;
    invoke-static {v1}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->access$100(Lcom/google/android/voicesearch/settings/GStaticConfiguration;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v1, p0, Lcom/google/android/voicesearch/settings/GStaticConfiguration$1;->this$0:Lcom/google/android/voicesearch/settings/GStaticConfiguration;

    # invokes: Lcom/google/android/voicesearch/settings/GStaticConfiguration;->loadConfiguration()Z
    invoke-static {v1}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->access$200(Lcom/google/android/voicesearch/settings/GStaticConfiguration;)Z

    move-result v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/voicesearch/settings/GStaticConfiguration$1;->this$0:Lcom/google/android/voicesearch/settings/GStaticConfiguration;

    # invokes: Lcom/google/android/voicesearch/settings/GStaticConfiguration;->notifyListener()V
    invoke-static {v1}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->access$300(Lcom/google/android/voicesearch/settings/GStaticConfiguration;)V

    :cond_0
    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
