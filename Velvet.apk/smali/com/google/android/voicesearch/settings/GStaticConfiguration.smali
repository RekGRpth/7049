.class Lcom/google/android/voicesearch/settings/GStaticConfiguration;
.super Ljava/lang/Object;
.source "GStaticConfiguration.java"


# instance fields
.field private volatile mConfiguration:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

.field private final mExecutorService:Ljava/util/concurrent/ExecutorService;

.field private final mGserviceWrapper:Lcom/google/android/searchcommon/GserviceWrapper;

.field private final mListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/voicesearch/settings/Settings$ConfigurationChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private volatile mLoadFuture:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation
.end field

.field private final mLoadRunnable:Ljava/lang/Runnable;

.field private final mLoadingLock:Ljava/lang/Object;

.field private final mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

.field private final mResources:Landroid/content/res/Resources;

.field private final mScheduleLoadLock:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/GsaPreferenceController;Landroid/content/res/Resources;Ljava/util/concurrent/ExecutorService;Lcom/google/android/searchcommon/GserviceWrapper;)V
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/GsaPreferenceController;
    .param p2    # Landroid/content/res/Resources;
    .param p3    # Ljava/util/concurrent/ExecutorService;
    .param p4    # Lcom/google/android/searchcommon/GserviceWrapper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->mScheduleLoadLock:Ljava/lang/Object;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->mLoadingLock:Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

    iput-object p2, p0, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->mResources:Landroid/content/res/Resources;

    iput-object p3, p0, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    iput-object p4, p0, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->mGserviceWrapper:Lcom/google/android/searchcommon/GserviceWrapper;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->mListeners:Ljava/util/ArrayList;

    new-instance v0, Lcom/google/android/voicesearch/settings/GStaticConfiguration$1;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/settings/GStaticConfiguration$1;-><init>(Lcom/google/android/voicesearch/settings/GStaticConfiguration;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->mLoadRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/settings/GStaticConfiguration;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/settings/GStaticConfiguration;

    iget-object v0, p0, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->mLoadingLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/voicesearch/settings/GStaticConfiguration;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/settings/GStaticConfiguration;

    iget-object v0, p0, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->mConfiguration:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/voicesearch/settings/GStaticConfiguration;)Z
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/settings/GStaticConfiguration;

    invoke-direct {p0}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->loadConfiguration()Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/voicesearch/settings/GStaticConfiguration;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/settings/GStaticConfiguration;

    invoke-direct {p0}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->notifyListener()V

    return-void
.end method

.method private download(Lcom/google/android/searchcommon/util/HttpHelper;Ljava/lang/String;)[B
    .locals 4
    .param p1    # Lcom/google/android/searchcommon/util/HttpHelper;
    .param p2    # Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->mLoadingLock:Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v2

    invoke-static {v2}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    :try_start_0
    new-instance v2, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;

    invoke-direct {v2, p2}, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x0

    invoke-interface {p1, v2, v3}, Lcom/google/android/searchcommon/util/HttpHelper;->rawGet(Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;I)[B
    :try_end_0
    .catch Lcom/google/android/searchcommon/util/HttpHelper$HttpException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    const-string v2, "GStaticConfiguration"

    const-string v3, "HTTPException error in updating the configuration"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v2, "GStaticConfiguration"

    const-string v3, "IOException error in updating the configuration"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private downloadFromNetwork(Lcom/google/android/searchcommon/util/HttpHelper;)Z
    .locals 11
    .param p1    # Lcom/google/android/searchcommon/util/HttpHelper;

    const/4 v6, 0x1

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->mLoadingLock:Ljava/lang/Object;

    invoke-static {v8}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v8

    invoke-static {v8}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    const/4 v4, 0x0

    iget-object v8, p0, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->mGserviceWrapper:Lcom/google/android/searchcommon/GserviceWrapper;

    const-string v9, "voice_search:gstatic_experiment_url"

    invoke-virtual {v8, v9}, Lcom/google/android/searchcommon/GserviceWrapper;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->isCurrentlyRunningExperiment()Z

    move-result v8

    if-eqz v8, :cond_0

    if-nez v3, :cond_0

    invoke-direct {p0}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->getPrefs()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v8

    invoke-interface {v8}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->edit()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v8

    const-string v9, "voice_search:gstatic_experiment_url"

    invoke-interface {v8, v9}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->remove(Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v8

    const-string v9, "gstatic_configuration_experiment_data"

    invoke-interface {v8, v9}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->remove(Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v8

    invoke-interface {v8}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->apply()V

    const/4 v4, 0x1

    :cond_0
    if-eqz v3, :cond_2

    invoke-direct {p0}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->getPrefs()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v8

    const-string v9, "gstatic_configuration_expriment_url"

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    const/4 v5, 0x0

    :goto_0
    if-nez v5, :cond_4

    if-eqz v4, :cond_3

    invoke-direct {p0}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->loadConfiguration()Z

    :goto_1
    return v6

    :cond_1
    move-object v5, v3

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->getNewConfigurationUrl()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    :cond_3
    move v6, v7

    goto :goto_1

    :cond_4
    invoke-static {v5}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1, v5}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->download(Lcom/google/android/searchcommon/util/HttpHelper;Ljava/lang/String;)[B

    move-result-object v1

    if-nez v1, :cond_5

    const-string v6, "GStaticConfiguration"

    const-string v8, "Configuration not updated - error"

    invoke-static {v6, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v6, v7

    goto :goto_1

    :cond_5
    :try_start_0
    invoke-static {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->parseFrom([B)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->overrideConfiguration(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v0

    if-eqz v3, :cond_6

    invoke-direct {p0, v1, v3}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->writeExperimentConfigurationToPrefs([BLjava/lang/String;)V

    :goto_2
    iput-object v0, p0, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->mConfiguration:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v2

    const-string v6, "GStaticConfiguration"

    const-string v8, "Downloaded Configuration cannot be parsed"

    invoke-static {v6, v8, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v6, v7

    goto :goto_1

    :cond_6
    :try_start_1
    invoke-static {v5}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->getTimestampFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v1, v8}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->writeConfigurationToPrefs([BLjava/lang/String;)V
    :try_end_1
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method private getNewConfigurationUrl()Ljava/lang/String;
    .locals 6

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->mGserviceWrapper:Lcom/google/android/searchcommon/GserviceWrapper;

    const-string v5, "voice_search:gstatic_url"

    invoke-virtual {v4, v5}, Lcom/google/android/searchcommon/GserviceWrapper;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    const/16 v4, 0x2f

    invoke-virtual {v2, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    const/4 v5, -0x1

    if-ne v4, v5, :cond_1

    :cond_0
    const-string v4, "GStaticConfiguration"

    const-string v5, "No valid gstatic url found."

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v3

    :goto_0
    return-object v2

    :cond_1
    invoke-static {v2}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->getTimestampFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v4, "GStaticConfiguration"

    const-string v5, "No valid timestamp in gstatic url."

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v3

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->getPrefs()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v4

    const-string v5, "gstatic_configuration_timestamp"

    invoke-interface {v4, v5, v3}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_3

    const-string v4, "GStaticConfiguration"

    const-string v5, "Ignore gservice update: no configuration"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v3

    goto :goto_0

    :cond_3
    invoke-virtual {v1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-ltz v4, :cond_4

    move-object v2, v3

    goto :goto_0

    :cond_4
    const-string v3, "GStaticConfiguration"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "#getNewConfigurationUrl [pref="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", gservice="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private getPrefs()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/GsaPreferenceController;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    return-object v0
.end method

.method static getTimestampFromUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0    # Ljava/lang/String;

    const/4 v4, -0x1

    const/4 v2, 0x0

    const/16 v3, 0x2f

    invoke-virtual {p0, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    if-ne v1, v4, :cond_1

    :cond_0
    :goto_0
    return-object v2

    :cond_1
    const/16 v3, 0x5f

    invoke-virtual {p0, v3, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    if-eq v1, v4, :cond_0

    add-int/lit8 v1, v1, 0x1

    :try_start_0
    const-string v3, "2013_03_06_14_31_44"

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v3, v1

    invoke-virtual {p0, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private isCurrentlyRunningExperiment()Z
    .locals 3

    invoke-direct {p0}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->getPrefs()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    const-string v1, "gstatic_configuration_expriment_url"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isPreferenceObsolete()Z
    .locals 7

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-direct {p0}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->getPrefs()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v4

    const-string v5, "gstatic_configuration_timestamp"

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    const-string v5, "2013_03_06_14_31_44"

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-ne v4, v5, :cond_0

    const-string v4, "2013_03_06_14_31_44"

    invoke-virtual {v4, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-lez v4, :cond_3

    move v1, v2

    :goto_1
    if-eqz v1, :cond_2

    const-string v4, "GStaticConfiguration"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Bundled: 2013_03_06_14_31_44, pref: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " pref obsolete"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "2013_03_06_14_31_44"

    invoke-virtual {v6, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-lez v6, :cond_4

    :goto_2
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    move v2, v1

    goto :goto_0

    :cond_3
    move v1, v3

    goto :goto_1

    :cond_4
    move v2, v3

    goto :goto_2
.end method

.method private loadBundledConfig()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;
    .locals 6

    iget-object v4, p0, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->mLoadingLock:Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v4

    invoke-static {v4}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    const/4 v3, 0x0

    :try_start_0
    iget-object v4, p0, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->mResources:Landroid/content/res/Resources;

    const/high16 v5, 0x7f080000

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v3

    invoke-static {v3}, Lcom/google/common/io/ByteStreams;->toByteArray(Ljava/io/InputStream;)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->parseFrom([B)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v1

    const-string v4, "2013_03_06_14_31_44"

    invoke-direct {p0, v0, v4}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->writeConfigurationToPrefs([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v3}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    return-object v1

    :catch_0
    move-exception v2

    :try_start_1
    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "Unable to load from asset"

    invoke-direct {v4, v5, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v4

    invoke-static {v3}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    throw v4
.end method

.method private loadConfiguration()Z
    .locals 5

    iget-object v4, p0, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->mLoadingLock:Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v4

    invoke-static {v4}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->isPreferenceObsolete()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-direct {p0}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->loadConfigurationFromSharedPreferences()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v0

    :cond_0
    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->loadBundledConfig()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v0

    const/4 v1, 0x1

    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->overrideConfiguration(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->mConfiguration:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    return v1
.end method

.method private loadConfigurationFromSharedPreferences()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;
    .locals 6

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->mLoadingLock:Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v2

    invoke-static {v2}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->getPrefs()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v2

    const-string v4, "gstatic_configuration_experiment_data"

    const/4 v5, 0x0

    invoke-interface {v2, v4, v5}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getBytes(Ljava/lang/String;[B)[B

    move-result-object v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->getPrefs()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v2

    const-string v4, "gstatic_configuration_data"

    const/4 v5, 0x0

    invoke-interface {v2, v4, v5}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getBytes(Ljava/lang/String;[B)[B

    move-result-object v0

    :cond_0
    if-nez v0, :cond_1

    const-string v2, "GStaticConfiguration"

    const-string v4, "Not found in the preferences"

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v3

    :goto_0
    return-object v2

    :cond_1
    new-instance v2, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    invoke-direct {v2}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;-><init>()V

    invoke-virtual {v2, v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v2

    check-cast v2, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    check-cast v2, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "GStaticConfiguration"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unable to read from the preference ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, v0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v2, v3

    goto :goto_0
.end method

.method private notifyListener()V
    .locals 3

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkNotMainThread()V

    iget-object v2, p0, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->mLoadingLock:Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v2, p0, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/voicesearch/settings/Settings$ConfigurationChangeListener;

    iget-object v2, p0, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->mConfiguration:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    invoke-interface {v1, v2}, Lcom/google/android/voicesearch/settings/Settings$ConfigurationChangeListener;->onChange(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;)V

    goto :goto_1

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    return-void
.end method

.method private overrideConfiguration(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;
    .locals 5
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    iget-object v2, p0, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->mLoadingLock:Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v2

    invoke-static {v2}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->getPrefs()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v2

    const-string v3, "gstatic_configuration_override_1"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getBytes(Ljava/lang/String;[B)[B

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-object p1

    :cond_0
    const-string v2, "GStaticConfiguration"

    const-string v3, "Overriding the configuration"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1, v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v2

    check-cast v2, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0

    move-object p1, v2

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "GStaticConfiguration"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unable to read override the preference ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private scheduleNotifyListener()V
    .locals 2

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v0, p0, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/google/android/voicesearch/settings/GStaticConfiguration$2;

    invoke-direct {v1, p0}, Lcom/google/android/voicesearch/settings/GStaticConfiguration$2;-><init>(Lcom/google/android/voicesearch/settings/GStaticConfiguration;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    return-void
.end method

.method private waitUntilReady()V
    .locals 7

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->mLoadFuture:Ljava/util/concurrent/Future;

    if-eqz v4, :cond_1

    :goto_0
    invoke-static {v3}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    :try_start_0
    iget-object v3, p0, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->mConfiguration:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    if-nez v3, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-static {v3}, Lcom/google/android/voicesearch/logger/EventLogger;->recordLatencyStart(I)V

    iget-object v3, p0, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->mLoadFuture:Ljava/util/concurrent/Future;

    const-wide/16 v4, 0x7d0

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v3, v4, v5, v6}, Ljava/util/concurrent/Future;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    const/4 v3, 0x3

    invoke-static {v3}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "Unable to load"

    invoke-direct {v3, v4, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method

.method private writeConfigurationToPrefs([BLjava/lang/String;)V
    .locals 2
    .param p1    # [B
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->getPrefs()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->edit()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    const-string v1, "gstatic_configuration_timestamp"

    invoke-interface {v0, v1, p2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    const-string v1, "gstatic_configuration_data"

    invoke-interface {v0, v1, p1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->putBytes(Ljava/lang/String;[B)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->apply()V

    return-void
.end method

.method private writeExperimentConfigurationToPrefs([BLjava/lang/String;)V
    .locals 2
    .param p1    # [B
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->getPrefs()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->edit()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    const-string v1, "gstatic_configuration_expriment_url"

    invoke-interface {v0, v1, p2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    const-string v1, "gstatic_configuration_experiment_data"

    invoke-interface {v0, v1, p1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->putBytes(Ljava/lang/String;[B)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->apply()V

    return-void
.end method


# virtual methods
.method public addListener(Lcom/google/android/voicesearch/settings/Settings$ConfigurationChangeListener;)V
    .locals 2
    .param p1    # Lcom/google/android/voicesearch/settings/Settings$ConfigurationChangeListener;

    iget-object v1, p0, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->mScheduleLoadLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->mLoadFuture:Ljava/util/concurrent/Future;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v1

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method asyncLoad()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->mScheduleLoadLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->mLoadFuture:Ljava/util/concurrent/Future;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    iget-object v2, p0, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->mLoadRunnable:Ljava/lang/Runnable;

    invoke-interface {v0, v2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->mLoadFuture:Ljava/util/concurrent/Future;

    :cond_0
    monitor-exit v1

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->waitUntilReady()V

    iget-object v0, p0, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->mConfiguration:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    return-object v0
.end method

.method public getOverrideConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;
    .locals 6

    invoke-direct {p0}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->getPrefs()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v3

    const-string v4, "gstatic_configuration_override_1"

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getBytes(Ljava/lang/String;[B)[B

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v3, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    invoke-direct {v3}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;-><init>()V

    const-string v4, "override"

    invoke-virtual {v3, v4}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->setId(Ljava/lang/String;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    :try_start_0
    new-instance v1, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    invoke-direct {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    const-string v3, "GStaticConfiguration"

    const-string v4, "Unable to read the override configuration"

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance v3, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    invoke-direct {v3}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;-><init>()V

    const-string v4, "override"

    invoke-virtual {v3, v4}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->setId(Ljava/lang/String;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v1

    goto :goto_0
.end method

.method public getTimestamp()Ljava/lang/CharSequence;
    .locals 3

    invoke-direct {p0}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->getPrefs()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    const-string v1, "gstatic_configuration_timestamp"

    const-string v2, "none"

    invoke-interface {v0, v1, v2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setOverrideConfiguration(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;)V
    .locals 3
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    if-nez p1, :cond_0

    invoke-direct {p0}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->getPrefs()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->edit()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    const-string v1, "gstatic_configuration_override_1"

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->remove(Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->apply()V

    :goto_0
    iget-object v1, p0, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->mLoadingLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->loadConfiguration()Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->scheduleNotifyListener()V

    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->getPrefs()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->edit()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    const-string v1, "gstatic_configuration_override_1"

    invoke-virtual {p1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->toByteArray()[B

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->putBytes(Ljava/lang/String;[B)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->apply()V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public update(Lcom/google/android/searchcommon/util/HttpHelper;)V
    .locals 3
    .param p1    # Lcom/google/android/searchcommon/util/HttpHelper;

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkNotMainThread()V

    iget-object v2, p0, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->mLoadingLock:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->downloadFromNetwork(Lcom/google/android/searchcommon/util/HttpHelper;)Z

    move-result v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/voicesearch/settings/GStaticConfiguration;->notifyListener()V

    :cond_0
    return-void

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
