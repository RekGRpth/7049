.class public Lcom/google/android/voicesearch/speechservice/ActionProcessor;
.super Ljava/lang/Object;
.source "ActionProcessor.java"


# instance fields
.field private final mActionListener:Lcom/google/android/voicesearch/speechservice/ActionListener;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/speechservice/ActionListener;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/speechservice/ActionListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->mActionListener:Lcom/google/android/voicesearch/speechservice/ActionListener;

    return-void
.end method

.method private static check(ZLjava/lang/String;)V
    .locals 1
    .param p0    # Z
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/speech/exception/ResponseRecognizeException;
        }
    .end annotation

    if-nez p0, :cond_0

    new-instance v0, Lcom/google/android/speech/exception/ResponseRecognizeException;

    invoke-direct {v0, p1}, Lcom/google/android/speech/exception/ResponseRecognizeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method private getAttribution(Lcom/google/majel/proto/PeanutProtos$Peanut;)Lcom/google/majel/proto/AttributionProtos$Attribution;
    .locals 2
    .param p1    # Lcom/google/majel/proto/PeanutProtos$Peanut;

    invoke-virtual {p1}, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasTextResponse()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getTextResponse()Lcom/google/majel/proto/PeanutProtos$Text;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/majel/proto/PeanutProtos$Text;->getAttributionCount()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Lcom/google/majel/proto/AttributionProtos$Attribution;

    invoke-direct {v0}, Lcom/google/majel/proto/AttributionProtos$Attribution;-><init>()V

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p1}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getTextResponse()Lcom/google/majel/proto/PeanutProtos$Text;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/majel/proto/PeanutProtos$Text;->getAttribution(I)Lcom/google/majel/proto/AttributionProtos$Attribution;

    move-result-object v0

    goto :goto_0
.end method

.method private getAttributionList(Lcom/google/majel/proto/PeanutProtos$Peanut;)Ljava/util/List;
    .locals 2
    .param p1    # Lcom/google/majel/proto/PeanutProtos$Peanut;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/majel/proto/PeanutProtos$Peanut;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/AttributionProtos$Attribution;",
            ">;"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasTextResponse()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getTextResponse()Lcom/google/majel/proto/PeanutProtos$Text;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/majel/proto/PeanutProtos$Text;->getAttributionCount()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v1, Lcom/google/majel/proto/AttributionProtos$Attribution;

    invoke-direct {v1}, Lcom/google/majel/proto/AttributionProtos$Attribution;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p1}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getTextResponse()Lcom/google/majel/proto/PeanutProtos$Text;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/majel/proto/PeanutProtos$Text;->getAttributionList()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method private getImage(Lcom/google/majel/proto/PeanutProtos$Peanut;)Lcom/google/android/voicesearch/speechservice/ImageParcelable;
    .locals 2
    .param p1    # Lcom/google/majel/proto/PeanutProtos$Peanut;

    invoke-virtual {p1}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getImageResponseCount()I

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/voicesearch/speechservice/ImageParcelable;->createDefaultInstance()Lcom/google/android/voicesearch/speechservice/ImageParcelable;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/voicesearch/speechservice/ImageParcelable;

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getImageResponse(I)Lcom/google/majel/proto/PeanutProtos$Image;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/voicesearch/speechservice/ImageParcelable;-><init>(Lcom/google/majel/proto/PeanutProtos$Image;)V

    goto :goto_0
.end method

.method private getImageAttribution(Lcom/google/majel/proto/PeanutProtos$Peanut;)Lcom/google/majel/proto/AttributionProtos$Attribution;
    .locals 2
    .param p1    # Lcom/google/majel/proto/PeanutProtos$Peanut;

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getImageResponseCount()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v1}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getImageResponse(I)Lcom/google/majel/proto/PeanutProtos$Image;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/majel/proto/PeanutProtos$Image;->getAttributionCount()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Lcom/google/majel/proto/AttributionProtos$Attribution;

    invoke-direct {v0}, Lcom/google/majel/proto/AttributionProtos$Attribution;-><init>()V

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p1, v1}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getImageResponse(I)Lcom/google/majel/proto/PeanutProtos$Image;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/majel/proto/PeanutProtos$Image;->getAttribution(I)Lcom/google/majel/proto/AttributionProtos$Attribution;

    move-result-object v0

    goto :goto_0
.end method

.method private processActionV2(Lcom/google/majel/proto/PeanutProtos$Peanut;Lcom/google/majel/proto/ActionV2Protos$ActionV2;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/speech/exception/ResponseRecognizeException;
        }
    .end annotation

    invoke-virtual {p2}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->hasOpenURLActionExtension()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->getOpenURLActionExtension()Lcom/google/majel/proto/ActionV2Protos$OpenURLAction;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->processOpenURLAction(Lcom/google/majel/proto/PeanutProtos$Peanut;Lcom/google/majel/proto/ActionV2Protos$OpenURLAction;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p2}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->hasPhoneActionExtension()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p2}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->processPhoneAction(Lcom/google/majel/proto/ActionV2Protos$ActionV2;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p2}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->hasSMSActionExtension()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->getSMSActionExtension()Lcom/google/majel/proto/ActionV2Protos$SMSAction;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->processSmsAction(Lcom/google/majel/proto/ActionV2Protos$SMSAction;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p2}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->hasSetAlarmActionExtension()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p2}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->getSetAlarmActionExtension()Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->processSetAlarmAction(Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p2}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->hasEmailActionExtension()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p2}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->getEmailActionExtension()Lcom/google/majel/proto/ActionV2Protos$EmailAction;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->processEmailAction(Lcom/google/majel/proto/ActionV2Protos$EmailAction;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p2}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->hasPlayMediaActionExtension()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p2}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->getPlayMediaActionExtension()Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->processPlayMediaAction(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)V

    goto :goto_0

    :cond_5
    invoke-virtual {p2}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->hasSelfNoteActionExtension()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p2}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->getSelfNoteActionExtension()Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->processSelfNoteAction(Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;)V

    goto :goto_0

    :cond_6
    invoke-virtual {p2}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->hasShowCalendarEventActionExtension()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p2}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->getShowCalendarEventActionExtension()Lcom/google/majel/proto/ActionV2Protos$ShowCalendarEventAction;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->processQueryCalendarAction(Lcom/google/majel/proto/ActionV2Protos$ShowCalendarEventAction;)V

    goto :goto_0

    :cond_7
    invoke-virtual {p2}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->hasHelpActionExtension()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p2}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->getHelpActionExtension()Lcom/google/majel/proto/ActionV2Protos$HelpAction;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->processHelpAction(Lcom/google/majel/proto/ActionV2Protos$HelpAction;)V

    goto :goto_0

    :cond_8
    invoke-virtual {p2}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->hasAddCalendarEventActionExtension()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {p2}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->getAddCalendarEventActionExtension()Lcom/google/majel/proto/ActionV2Protos$AddCalendarEventAction;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->processAddCalendarEventAction(Lcom/google/majel/proto/ActionV2Protos$AddCalendarEventAction;)V

    goto :goto_0

    :cond_9
    invoke-virtual {p2}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->hasSoundSearchActionExtension()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-virtual {p2}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->getSoundSearchActionExtension()Lcom/google/majel/proto/ActionV2Protos$SoundSearchAction;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->processSoundSearchAction(Lcom/google/majel/proto/ActionV2Protos$SoundSearchAction;)V

    goto/16 :goto_0

    :cond_a
    invoke-virtual {p2}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->hasOpenApplicationActionExtension()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-virtual {p2}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->getOpenApplicationActionExtension()Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->processOpenApplicationAction(Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction;)V

    goto/16 :goto_0

    :cond_b
    invoke-virtual {p2}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->hasAtHomePowerControlActionExtension()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-virtual {p2}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->getAtHomePowerControlActionExtension()Lcom/google/majel/proto/ActionV2Protos$AtHomePowerControlAction;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->processAtHomePowerControlAction(Lcom/google/majel/proto/ActionV2Protos$AtHomePowerControlAction;)V

    goto/16 :goto_0

    :cond_c
    invoke-virtual {p2}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->hasUpdateSocialNetworkActionExtension()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-virtual {p2}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->getUpdateSocialNetworkActionExtension()Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->processUpdateSocialNetwork(Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;)V

    goto/16 :goto_0

    :cond_d
    invoke-virtual {p2}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->hasGogglesActionExtension()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-virtual {p2}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->getGogglesActionExtension()Lcom/google/majel/proto/ActionV2Protos$GogglesAction;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->processGogglesAction(Lcom/google/majel/proto/ActionV2Protos$GogglesAction;)V

    goto/16 :goto_0

    :cond_e
    invoke-virtual {p2}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->hasShowContactInformationActionExtension()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-virtual {p2}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->getShowContactInformationActionExtension()Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->processShowContactInformation(Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;)V

    goto/16 :goto_0

    :cond_f
    invoke-virtual {p2}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->hasAddReminderActionExtension()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-virtual {p2}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->getAddReminderActionExtension()Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->processSetReminderAction(Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;)V

    goto/16 :goto_0

    :cond_10
    invoke-virtual {p2}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->hasUnsupportedActionExtension()Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-virtual {p2}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->getUnsupportedActionExtension()Lcom/google/majel/proto/ActionV2Protos$UnsupportedAction;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->processUnsupportedAction(Lcom/google/majel/proto/ActionV2Protos$UnsupportedAction;)V

    goto/16 :goto_0

    :cond_11
    new-instance v0, Lcom/google/android/speech/exception/ResponseRecognizeException;

    const-string v1, "ActionV2 receieved that we can\'t handle"

    invoke-direct {v0, v1}, Lcom/google/android/speech/exception/ResponseRecognizeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private processAddCalendarEventAction(Lcom/google/majel/proto/ActionV2Protos$AddCalendarEventAction;)V
    .locals 10
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$AddCalendarEventAction;

    const-wide/16 v4, 0x0

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$AddCalendarEventAction;->hasCalendarEvent()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$AddCalendarEventAction;->getCalendarEvent()Lcom/google/majel/proto/CalendarProtos$CalendarEvent;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->mActionListener:Lcom/google/android/voicesearch/speechservice/ActionListener;

    invoke-virtual {v8}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->getSummary()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->getLocation()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->hasStartTime()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v8}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->getStartTime()Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->getTimeMs()J

    move-result-wide v2

    :goto_0
    invoke-virtual {v8}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->hasEndTime()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v8}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->getEndTime()Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;->getTimeMs()J

    move-result-wide v4

    :cond_0
    invoke-virtual {v8}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->getAttendeeList()Ljava/util/List;

    move-result-object v6

    invoke-virtual {v8}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->getReminderList()Ljava/util/List;

    move-result-object v7

    invoke-static/range {v0 .. v7}, Lcom/google/android/voicesearch/fragments/CalendarEventController;->createData(Ljava/lang/String;Ljava/lang/String;JJLjava/util/List;Ljava/util/List;)Lcom/google/android/voicesearch/fragments/CalendarEventController$Data;

    move-result-object v0

    invoke-interface {v9, v0}, Lcom/google/android/voicesearch/speechservice/ActionListener;->onAddCalendarEventAction(Lcom/google/android/voicesearch/fragments/CalendarEventController$Data;)V

    :cond_1
    return-void

    :cond_2
    move-wide v2, v4

    goto :goto_0
.end method

.method private processAsImageResponse(Lcom/google/majel/proto/PeanutProtos$Peanut;)V
    .locals 2
    .param p1    # Lcom/google/majel/proto/PeanutProtos$Peanut;

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->mActionListener:Lcom/google/android/voicesearch/speechservice/ActionListener;

    new-instance v1, Lcom/google/android/voicesearch/speechservice/ImageResultData;

    invoke-direct {v1, p1}, Lcom/google/android/voicesearch/speechservice/ImageResultData;-><init>(Lcom/google/majel/proto/PeanutProtos$Peanut;)V

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/speechservice/ActionListener;->onImageResults(Lcom/google/android/voicesearch/speechservice/ImageResultData;)V

    return-void
.end method

.method private processAsTextResponse(Lcom/google/majel/proto/PeanutProtos$Peanut;)V
    .locals 9
    .param p1    # Lcom/google/majel/proto/PeanutProtos$Peanut;

    invoke-virtual {p1}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getTextResponse()Lcom/google/majel/proto/PeanutProtos$Text;

    move-result-object v8

    new-instance v0, Lcom/google/android/voicesearch/speechservice/AnswerData;

    invoke-virtual {v8}, Lcom/google/majel/proto/PeanutProtos$Text;->getDisplay()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8}, Lcom/google/majel/proto/PeanutProtos$Text;->getDisplayDescription()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->getAttributionList(Lcom/google/majel/proto/PeanutProtos$Peanut;)Ljava/util/List;

    move-result-object v3

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->getImage(Lcom/google/majel/proto/PeanutProtos$Peanut;)Lcom/google/android/voicesearch/speechservice/ImageParcelable;

    move-result-object v4

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->getImageAttribution(Lcom/google/majel/proto/PeanutProtos$Peanut;)Lcom/google/majel/proto/AttributionProtos$Attribution;

    move-result-object v5

    invoke-virtual {v8}, Lcom/google/majel/proto/PeanutProtos$Text;->getDisclaimerText()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8}, Lcom/google/majel/proto/PeanutProtos$Text;->getDisclaimerUrl()Ljava/lang/String;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lcom/google/android/voicesearch/speechservice/AnswerData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/google/android/voicesearch/speechservice/ImageParcelable;Lcom/google/majel/proto/AttributionProtos$Attribution;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getHighConfidenceResponse()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->processHighConfidenceAnswerData(Lcom/google/android/voicesearch/speechservice/AnswerData;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->processMediumConfidenceAnswerData(Lcom/google/android/voicesearch/speechservice/AnswerData;)V

    goto :goto_0
.end method

.method private processAtHomePowerControlAction(Lcom/google/majel/proto/ActionV2Protos$AtHomePowerControlAction;)V
    .locals 3
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$AtHomePowerControlAction;

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->mActionListener:Lcom/google/android/voicesearch/speechservice/ActionListener;

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$AtHomePowerControlAction;->getTarget()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$AtHomePowerControlAction;->getLevel()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/voicesearch/speechservice/ActionListener;->onAtHomePowerControlAction(Ljava/lang/String;I)V

    return-void
.end method

.method private processCalculatorResult(Lcom/google/majel/proto/CommonStructuredResponse$CalculatorResult;Lcom/google/majel/proto/AttributionProtos$Attribution;)Z
    .locals 8
    .param p1    # Lcom/google/majel/proto/CommonStructuredResponse$CalculatorResult;
    .param p2    # Lcom/google/majel/proto/AttributionProtos$Attribution;

    invoke-virtual {p1}, Lcom/google/majel/proto/CommonStructuredResponse$CalculatorResult;->hasLeft()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/majel/proto/CommonStructuredResponse$CalculatorResult;->hasRight()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/google/majel/proto/CommonStructuredResponse$CalculatorResult;->getLeft()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " = <b>"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/majel/proto/CommonStructuredResponse$CalculatorResult;->getRight()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "</b>"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Lcom/google/android/voicesearch/speechservice/AnswerData;

    const-string v2, ""

    invoke-static {}, Lcom/google/android/voicesearch/speechservice/ImageParcelable;->createDefaultInstance()Lcom/google/android/voicesearch/speechservice/ImageParcelable;

    move-result-object v4

    new-instance v5, Lcom/google/majel/proto/AttributionProtos$Attribution;

    invoke-direct {v5}, Lcom/google/majel/proto/AttributionProtos$Attribution;-><init>()V

    const-string v6, ""

    const-string v7, ""

    move-object v3, p2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/voicesearch/speechservice/AnswerData;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/majel/proto/AttributionProtos$Attribution;Lcom/google/android/voicesearch/speechservice/ImageParcelable;Lcom/google/majel/proto/AttributionProtos$Attribution;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->processHighConfidenceAnswerData(Lcom/google/android/voicesearch/speechservice/AnswerData;)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private processDictionaryResult(Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;)Z
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->mActionListener:Lcom/google/android/voicesearch/speechservice/ActionListener;

    invoke-interface {v0, p1}, Lcom/google/android/voicesearch/speechservice/ActionListener;->onDictionaryResults(Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;)V

    const/4 v0, 0x1

    return v0
.end method

.method private processEarsResult(Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;Z)V
    .locals 3
    .param p1    # Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;
    .param p2    # Z

    invoke-virtual {p1}, Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;->getResultList()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/ears/EarsResultParser;->getFirstEarsResultWithMusic(Ljava/util/List;)Lcom/google/audio/ears/proto/EarsService$EarsResult;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    invoke-virtual {p1}, Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;->getDetectedCountryCode()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, p2, v2}, Lcom/google/android/ears/EarsResultParser;->convertEarsToPlayMediaAction(Lcom/google/audio/ears/proto/EarsService$EarsResult;ZLjava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->mActionListener:Lcom/google/android/voicesearch/speechservice/ActionListener;

    invoke-interface {v2, v1}, Lcom/google/android/voicesearch/speechservice/ActionListener;->onPlayMusicAction(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)V

    return-void

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private processEmailAction(Lcom/google/majel/proto/ActionV2Protos$EmailAction;)V
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$EmailAction;

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->mActionListener:Lcom/google/android/voicesearch/speechservice/ActionListener;

    invoke-interface {v0, p1}, Lcom/google/android/voicesearch/speechservice/ActionListener;->onEmailAction(Lcom/google/majel/proto/ActionV2Protos$EmailAction;)V

    return-void
.end method

.method private processException(Lcom/google/android/speech/exception/RecognizeException;)V
    .locals 2
    .param p1    # Lcom/google/android/speech/exception/RecognizeException;

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->mActionListener:Lcom/google/android/voicesearch/speechservice/ActionListener;

    invoke-static {p1}, Lcom/google/android/voicesearch/util/ErrorUtils;->getErrorMessage(Lcom/google/android/speech/exception/RecognizeException;)I

    move-result v1

    invoke-static {v1}, Lcom/google/android/voicesearch/fragments/PuntController;->createData(I)Lcom/google/android/voicesearch/fragments/PuntController$Data;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/speechservice/ActionListener;->onPuntAction(Lcom/google/android/voicesearch/fragments/PuntController$Data;)V

    return-void
.end method

.method private processFlightResult(Lcom/google/majel/proto/EcoutezStructuredResponse$FlightResult;)Z
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$FlightResult;

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->mActionListener:Lcom/google/android/voicesearch/speechservice/ActionListener;

    invoke-interface {v0, p1}, Lcom/google/android/voicesearch/speechservice/ActionListener;->onFlightResults(Lcom/google/majel/proto/EcoutezStructuredResponse$FlightResult;)V

    const/4 v0, 0x1

    return v0
.end method

.method private processGogglesAction(Lcom/google/majel/proto/ActionV2Protos$GogglesAction;)V
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$GogglesAction;

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->mActionListener:Lcom/google/android/voicesearch/speechservice/ActionListener;

    invoke-interface {v0}, Lcom/google/android/voicesearch/speechservice/ActionListener;->onGogglesAction()V

    return-void
.end method

.method private processGogglesGenericResult(Ljava/lang/String;Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$GogglesGenericResult;)Z
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$GogglesGenericResult;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p2}, Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$GogglesGenericResult;->getLinkList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$GogglesGenericResult$Link;

    new-instance v4, Lcom/google/android/goggles/GogglesGenericAction;

    invoke-virtual {v3}, Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$GogglesGenericResult$Link;->getLabel()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$GogglesGenericResult$Link;->getUrl()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v3}, Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$GogglesGenericResult$Link;->getIcon()I

    move-result v7

    invoke-direct {v4, v5, v6, v7}, Lcom/google/android/goggles/GogglesGenericAction;-><init>(Ljava/lang/String;Landroid/net/Uri;I)V

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {p2}, Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$GogglesGenericResult;->getFifeImageUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2}, Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$GogglesGenericResult;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2}, Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$GogglesGenericResult;->getSubtitle()Ljava/lang/String;

    move-result-object v6

    invoke-static {p1, v4, v5, v6, v0}, Lcom/google/android/voicesearch/fragments/GogglesGenericController;->createData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Lcom/google/android/voicesearch/fragments/GogglesGenericController$Data;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->mActionListener:Lcom/google/android/voicesearch/speechservice/ActionListener;

    invoke-interface {v4, v1}, Lcom/google/android/voicesearch/speechservice/ActionListener;->onGogglesGenericResult(Lcom/google/android/voicesearch/fragments/GogglesGenericController$Data;)V

    const/4 v4, 0x1

    return v4
.end method

.method private processHelpAction(Lcom/google/majel/proto/ActionV2Protos$HelpAction;)V
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$HelpAction;

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->mActionListener:Lcom/google/android/voicesearch/speechservice/ActionListener;

    invoke-interface {v0, p1}, Lcom/google/android/voicesearch/speechservice/ActionListener;->onHelpAction(Lcom/google/majel/proto/ActionV2Protos$HelpAction;)V

    return-void
.end method

.method private processHighConfidenceAnswerData(Lcom/google/android/voicesearch/speechservice/AnswerData;)V
    .locals 1
    .param p1    # Lcom/google/android/voicesearch/speechservice/AnswerData;

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->mActionListener:Lcom/google/android/voicesearch/speechservice/ActionListener;

    invoke-interface {v0, p1}, Lcom/google/android/voicesearch/speechservice/ActionListener;->onHighConfidenceAnswerData(Lcom/google/android/voicesearch/speechservice/AnswerData;)V

    return-void
.end method

.method private processLocalResults(Lcom/google/majel/proto/PeanutProtos$Peanut;Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;)Z
    .locals 2
    .param p1    # Lcom/google/majel/proto/PeanutProtos$Peanut;
    .param p2    # Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

    const/4 v0, 0x1

    invoke-virtual {p2}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;->getLocalResultCount()I

    move-result v1

    if-le v1, v0, :cond_0

    iget-object v1, p0, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->mActionListener:Lcom/google/android/voicesearch/speechservice/ActionListener;

    invoke-interface {v1, p2}, Lcom/google/android/voicesearch/speechservice/ActionListener;->onMultipleLocalResults(Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;)V

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p2}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;->getLocalResultCount()I

    move-result v1

    if-ne v1, v0, :cond_1

    iget-object v1, p0, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->mActionListener:Lcom/google/android/voicesearch/speechservice/ActionListener;

    invoke-interface {v1, p2}, Lcom/google/android/voicesearch/speechservice/ActionListener;->onSingleLocalResult(Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private processMediumConfidenceAnswerData(Lcom/google/android/voicesearch/speechservice/AnswerData;)V
    .locals 1
    .param p1    # Lcom/google/android/voicesearch/speechservice/AnswerData;

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->mActionListener:Lcom/google/android/voicesearch/speechservice/ActionListener;

    invoke-interface {v0, p1}, Lcom/google/android/voicesearch/speechservice/ActionListener;->onMediumConfidenceAnswerData(Lcom/google/android/voicesearch/speechservice/AnswerData;)V

    return-void
.end method

.method private processOpenApplicationAction(Lcom/google/android/speech/embedded/TaggerResult;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->mActionListener:Lcom/google/android/voicesearch/speechservice/ActionListener;

    const-string v1, "AppName"

    invoke-virtual {p1, v1}, Lcom/google/android/speech/embedded/TaggerResult;->getArgument(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/speechservice/ActionListener;->onOpenApplicationAction(Ljava/lang/String;)V

    return-void
.end method

.method private processOpenApplicationAction(Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction;)V
    .locals 2
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction;

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->mActionListener:Lcom/google/android/voicesearch/speechservice/ActionListener;

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$OpenApplicationAction;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/speechservice/ActionListener;->onOpenApplicationAction(Ljava/lang/String;)V

    return-void
.end method

.method private processOpenURLAction(Lcom/google/majel/proto/PeanutProtos$Peanut;Lcom/google/majel/proto/ActionV2Protos$OpenURLAction;)V
    .locals 3
    .param p1    # Lcom/google/majel/proto/PeanutProtos$Peanut;
    .param p2    # Lcom/google/majel/proto/ActionV2Protos$OpenURLAction;

    invoke-virtual {p1}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getUrlResponseCount()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getUrlResponse(I)Lcom/google/majel/proto/PeanutProtos$Url;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->mActionListener:Lcom/google/android/voicesearch/speechservice/ActionListener;

    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/OpenUrlController;->createData(Lcom/google/majel/proto/PeanutProtos$Url;)Lcom/google/android/voicesearch/fragments/OpenUrlController$Data;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/voicesearch/speechservice/ActionListener;->onOpenURLAction(Lcom/google/android/voicesearch/fragments/OpenUrlController$Data;)V

    return-void

    :cond_0
    new-instance v1, Lcom/google/majel/proto/PeanutProtos$Url;

    invoke-direct {v1}, Lcom/google/majel/proto/PeanutProtos$Url;-><init>()V

    invoke-virtual {p2}, Lcom/google/majel/proto/ActionV2Protos$OpenURLAction;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/majel/proto/PeanutProtos$Url;->setLink(Ljava/lang/String;)Lcom/google/majel/proto/PeanutProtos$Url;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/majel/proto/ActionV2Protos$OpenURLAction;->getDisplayUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/majel/proto/PeanutProtos$Url;->setDisplayLink(Ljava/lang/String;)Lcom/google/majel/proto/PeanutProtos$Url;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/majel/proto/ActionV2Protos$OpenURLAction;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/majel/proto/PeanutProtos$Url;->setTitle(Ljava/lang/String;)Lcom/google/majel/proto/PeanutProtos$Url;

    move-result-object v0

    goto :goto_0
.end method

.method private processPeanut(Lcom/google/android/velvet/Query;Lcom/google/majel/proto/PeanutProtos$Peanut;)V
    .locals 5
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Lcom/google/majel/proto/PeanutProtos$Peanut;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/speech/exception/ResponseRecognizeException;
        }
    .end annotation

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p2}, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasStructuredResponse()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->processStructuredResponse(Lcom/google/android/velvet/Query;Lcom/google/majel/proto/PeanutProtos$Peanut;)Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getActionV2Count()I

    move-result v4

    if-lez v4, :cond_2

    move v0, v2

    :goto_1
    invoke-virtual {p2}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getPrimaryType()I

    move-result v1

    if-eqz v0, :cond_3

    invoke-virtual {p2, v3}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getActionV2(I)Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    move-result-object v2

    invoke-direct {p0, p2, v2}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->processActionV2(Lcom/google/majel/proto/PeanutProtos$Peanut;Lcom/google/majel/proto/ActionV2Protos$ActionV2;)V

    goto :goto_0

    :cond_2
    move v0, v3

    goto :goto_1

    :cond_3
    const/4 v4, 0x3

    if-ne v4, v1, :cond_4

    invoke-virtual {p2, v3}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getUrlResponse(I)Lcom/google/majel/proto/PeanutProtos$Url;

    move-result-object v2

    invoke-direct {p0, p1, v2}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->processUrlResponse(Lcom/google/android/velvet/Query;Lcom/google/majel/proto/PeanutProtos$Url;)V

    goto :goto_0

    :cond_4
    if-eq v2, v1, :cond_5

    if-nez v1, :cond_6

    invoke-virtual {p2}, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasTextResponse()Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_5
    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->shouldSuppressAnswers(Lcom/google/android/velvet/Query;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-direct {p0, p2}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->processAsTextResponse(Lcom/google/majel/proto/PeanutProtos$Peanut;)V

    goto :goto_0

    :cond_6
    const/4 v2, 0x2

    if-ne v2, v1, :cond_7

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->shouldSuppressAnswers(Lcom/google/android/velvet/Query;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-direct {p0, p2}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->processAsImageResponse(Lcom/google/majel/proto/PeanutProtos$Peanut;)V

    goto :goto_0

    :cond_7
    const-string v2, "ActionProcessor"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unhandled peanut with primary type "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private processPhoneAction(Lcom/google/majel/proto/ActionV2Protos$ActionV2;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/speech/exception/ResponseRecognizeException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->getPhoneActionExtension()Lcom/google/majel/proto/ActionV2Protos$PhoneAction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$PhoneAction;->getContactCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Phone action without any contacts."

    invoke-static {v0, v1}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->check(ZLjava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->mActionListener:Lcom/google/android/voicesearch/speechservice/ActionListener;

    invoke-static {p1}, Lcom/google/android/voicesearch/fragments/PhoneCallController;->createData(Lcom/google/majel/proto/ActionV2Protos$ActionV2;)Lcom/google/android/voicesearch/fragments/PhoneCallController$Data;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/speechservice/ActionListener;->onPhoneAction(Lcom/google/android/voicesearch/fragments/PhoneCallController$Data;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private processPlayMediaAction(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)V
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasMovieItem()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->mActionListener:Lcom/google/android/voicesearch/speechservice/ActionListener;

    invoke-interface {v0, p1}, Lcom/google/android/voicesearch/speechservice/ActionListener;->onPlayMovieAction(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasBookItem()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->mActionListener:Lcom/google/android/voicesearch/speechservice/ActionListener;

    invoke-interface {v0, p1}, Lcom/google/android/voicesearch/speechservice/ActionListener;->onOpenBookAction(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasMusicItem()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->mActionListener:Lcom/google/android/voicesearch/speechservice/ActionListener;

    invoke-interface {v0, p1}, Lcom/google/android/voicesearch/speechservice/ActionListener;->onPlayMusicAction(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasAppItem()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->mActionListener:Lcom/google/android/voicesearch/speechservice/ActionListener;

    invoke-interface {v0, p1}, Lcom/google/android/voicesearch/speechservice/ActionListener;->onOpenAppPlayMediaAction(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)V

    goto :goto_0
.end method

.method private processPumpkinTaggerResult(Lcom/google/android/speech/embedded/TaggerResult;)Z
    .locals 4
    .param p1    # Lcom/google/android/speech/embedded/TaggerResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/speech/exception/ResponseRecognizeException;
        }
    .end annotation

    const/4 v2, 0x1

    invoke-virtual {p1}, Lcom/google/android/speech/embedded/TaggerResult;->getActionName()Ljava/lang/String;

    move-result-object v1

    const-string v3, "CallContact"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "CallNumber"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    invoke-static {p1}, Lcom/google/android/voicesearch/util/PhoneActionUtils;->fromPumpkinTaggerResult(Lcom/google/android/speech/embedded/TaggerResult;)Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->processPhoneAction(Lcom/google/majel/proto/ActionV2Protos$ActionV2;)V

    :goto_0
    return v2

    :cond_1
    const-string v3, "OpenApp"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->processOpenApplicationAction(Lcom/google/android/speech/embedded/TaggerResult;)V

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private processQueryCalendarAction(Lcom/google/majel/proto/ActionV2Protos$ShowCalendarEventAction;)V
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$ShowCalendarEventAction;

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->mActionListener:Lcom/google/android/voicesearch/speechservice/ActionListener;

    invoke-interface {v0}, Lcom/google/android/voicesearch/speechservice/ActionListener;->onQueryCalendarAction()V

    return-void
.end method

.method private processRecognizedContact(Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact;)Z
    .locals 8
    .param p1    # Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact;

    invoke-virtual {p1}, Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact;->hasName()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact;->getName()Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact$Name;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact$Name;->getFull()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1}, Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact;->getCompany()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact;->getUrlList()Ljava/util/List;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact;->getEmailList()Ljava/util/List;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact;->getPhoneNumberList()Ljava/util/List;

    move-result-object v5

    invoke-virtual {p1}, Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact;->getAddressList()Ljava/util/List;

    move-result-object v6

    invoke-static/range {v0 .. v6}, Lcom/google/android/voicesearch/fragments/RecognizedContactController;->createData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;

    move-result-object v7

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->mActionListener:Lcom/google/android/voicesearch/speechservice/ActionListener;

    invoke-interface {v0, v7}, Lcom/google/android/voicesearch/speechservice/ActionListener;->onRecognizedContact(Lcom/google/android/voicesearch/fragments/RecognizedContactController$Data;)V

    const/4 v0, 0x1

    return v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private processSelfNoteAction(Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;)V
    .locals 2
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->mActionListener:Lcom/google/android/voicesearch/speechservice/ActionListener;

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$SelfNoteAction;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/speechservice/ActionListener;->onSelfNoteAction(Ljava/lang/String;)V

    return-void
.end method

.method private processSetAlarmAction(Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;)V
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->mActionListener:Lcom/google/android/voicesearch/speechservice/ActionListener;

    invoke-interface {v0, p1}, Lcom/google/android/voicesearch/speechservice/ActionListener;->onSetAlarmAction(Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;)V

    return-void
.end method

.method private processSetReminderAction(Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;)V
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->mActionListener:Lcom/google/android/voicesearch/speechservice/ActionListener;

    invoke-interface {v0, p1}, Lcom/google/android/voicesearch/speechservice/ActionListener;->onSetReminderAction(Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;)V

    return-void
.end method

.method private processShowContactInformation(Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;)V
    .locals 3
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->getShowPhone()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->mActionListener:Lcom/google/android/voicesearch/speechservice/ActionListener;

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->getContactList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Lcom/google/android/voicesearch/speechservice/ActionListener;->onShowContactInformationAction(Ljava/util/List;I)V

    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->getShowEmail()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$ShowContactInformationAction;->getShowPostalAddress()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x3

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private processSmsAction(Lcom/google/majel/proto/ActionV2Protos$SMSAction;)V
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$SMSAction;

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->mActionListener:Lcom/google/android/voicesearch/speechservice/ActionListener;

    invoke-interface {v0, p1}, Lcom/google/android/voicesearch/speechservice/ActionListener;->onSmsAction(Lcom/google/majel/proto/ActionV2Protos$SMSAction;)V

    return-void
.end method

.method private processSoundSearchAction(Lcom/google/majel/proto/ActionV2Protos$SoundSearchAction;)V
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$SoundSearchAction;

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->mActionListener:Lcom/google/android/voicesearch/speechservice/ActionListener;

    invoke-interface {v0}, Lcom/google/android/voicesearch/speechservice/ActionListener;->onSoundSearchAction()V

    return-void
.end method

.method private processSportsResult(Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;)Z
    .locals 29
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;

    const/16 v16, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->hasTeamData()Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-virtual/range {p1 .. p1}, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->getTeamData()Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->hasInProgressMatch()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-virtual/range {p1 .. p1}, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->getTeamData()Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->getInProgressMatch()Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    move-result-object v16

    :goto_0
    const-string v8, ""

    const/4 v7, 0x0

    invoke-virtual/range {v16 .. v16}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasLiveUpdateUrl()Z

    move-result v3

    if-eqz v3, :cond_b

    invoke-virtual/range {v16 .. v16}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getLiveUpdateUrl()Ljava/lang/String;

    move-result-object v8

    const v7, 0x7f0d0152

    :cond_0
    :goto_1
    invoke-virtual/range {v16 .. v16}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasFirstTeamLogo()Z

    move-result v3

    if-eqz v3, :cond_e

    invoke-virtual/range {v16 .. v16}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getFirstTeamLogo()Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;->getUrl()Ljava/lang/String;

    move-result-object v13

    :goto_2
    new-instance v4, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;

    invoke-virtual/range {v16 .. v16}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getFirstTeam()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {v16 .. v16}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getFirstTeamShortName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v16 .. v16}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getFirstScore()Ljava/lang/String;

    move-result-object v23

    invoke-static {v13}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v24

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-direct {v4, v3, v6, v0, v1}, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual/range {v16 .. v16}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasSecondTeamLogo()Z

    move-result v3

    if-eqz v3, :cond_f

    invoke-virtual/range {v16 .. v16}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getSecondTeamLogo()Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;->getUrl()Ljava/lang/String;

    move-result-object v21

    :goto_3
    new-instance v5, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;

    invoke-virtual/range {v16 .. v16}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getSecondTeam()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {v16 .. v16}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getSecondTeamShortName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v16 .. v16}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getSecondScore()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v21 .. v21}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v24

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-direct {v5, v3, v6, v0, v1}, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    new-instance v2, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;

    invoke-virtual/range {p1 .. p1}, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {v16 .. v16}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getStatus()I

    move-result v6

    invoke-direct/range {v2 .. v8}, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;-><init>(Ljava/lang/String;Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;IILjava/lang/String;)V

    invoke-virtual/range {v16 .. v16}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasStartTimestamp()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual/range {v16 .. v16}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getStartTimestamp()J

    move-result-wide v23

    const-wide/16 v25, 0x3e8

    mul-long v23, v23, v25

    move-wide/from16 v0, v23

    invoke-virtual {v2, v0, v1}, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->setStartDateTimeMillis(J)V

    :cond_1
    invoke-virtual/range {v16 .. v16}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getStatus()I

    move-result v3

    const/4 v6, 0x1

    if-ne v3, v6, :cond_11

    invoke-virtual/range {v16 .. v16}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasCurrentPeriodNum()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual/range {v16 .. v16}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getCurrentPeriodNum()I

    move-result v3

    add-int/lit8 v19, v3, -0x1

    move/from16 v0, v19

    invoke-virtual {v2, v0}, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->setCurrentPeriod(I)V

    if-ltz v19, :cond_4

    invoke-virtual/range {v16 .. v16}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getPeriodCount()I

    move-result v3

    move/from16 v0, v19

    if-ge v0, v3, :cond_4

    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getPeriod(I)Lcom/google/majel/proto/EcoutezStructuredResponse$Period;

    move-result-object v18

    const/16 v17, 0x0

    :try_start_0
    invoke-virtual/range {v18 .. v18}, Lcom/google/majel/proto/EcoutezStructuredResponse$Period;->hasMinutes()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual/range {v18 .. v18}, Lcom/google/majel/proto/EcoutezStructuredResponse$Period;->getMinutes()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v17

    :cond_2
    const/16 v22, 0x0

    invoke-virtual/range {v18 .. v18}, Lcom/google/majel/proto/EcoutezStructuredResponse$Period;->hasSeconds()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual/range {v18 .. v18}, Lcom/google/majel/proto/EcoutezStructuredResponse$Period;->getSeconds()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v22

    :cond_3
    move/from16 v0, v17

    int-to-long v0, v0

    move-wide/from16 v23, v0

    const-wide/32 v25, 0xea60

    mul-long v23, v23, v25

    move/from16 v0, v22

    int-to-long v0, v0

    move-wide/from16 v25, v0

    const-wide/16 v27, 0x3e8

    mul-long v25, v25, v27

    add-long v23, v23, v25

    move-wide/from16 v0, v23

    invoke-virtual {v2, v0, v1}, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->setElapsedTimeMillis(J)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_4
    :goto_4
    invoke-virtual/range {v16 .. v16}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasBaseball()Z

    move-result v3

    if-eqz v3, :cond_11

    invoke-virtual/range {v16 .. v16}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getPeriodCount()I

    move-result v3

    if-lez v3, :cond_11

    invoke-virtual/range {v16 .. v16}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getBaseball()Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;

    move-result-object v10

    new-instance v12, Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;

    invoke-virtual {v10}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->getFirstTeamHits()I

    move-result v3

    invoke-virtual {v10}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->getFirstTeamRuns()I

    move-result v6

    invoke-virtual {v10}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->getFirstTeamErrors()I

    move-result v23

    move/from16 v0, v23

    invoke-direct {v12, v3, v6, v0}, Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;-><init>(III)V

    new-instance v20, Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;

    invoke-virtual {v10}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->getSecondTeamHits()I

    move-result v3

    invoke-virtual {v10}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->getSecondTeamRuns()I

    move-result v6

    invoke-virtual {v10}, Lcom/google/majel/proto/EcoutezStructuredResponse$BaseballMatch;->getSecondTeamErrors()I

    move-result v23

    move-object/from16 v0, v20

    move/from16 v1, v23

    invoke-direct {v0, v3, v6, v1}, Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;-><init>(III)V

    new-instance v9, Lcom/google/android/voicesearch/speechservice/BaseballResultData;

    invoke-virtual/range {v16 .. v16}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getPeriodCount()I

    move-result v3

    move-object/from16 v0, v20

    invoke-direct {v9, v12, v0, v3}, Lcom/google/android/voicesearch/speechservice/BaseballResultData;-><init>(Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;I)V

    const/4 v15, 0x0

    invoke-virtual/range {v16 .. v16}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getPeriodList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_5
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_10

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/google/majel/proto/EcoutezStructuredResponse$Period;

    invoke-virtual/range {v18 .. v18}, Lcom/google/majel/proto/EcoutezStructuredResponse$Period;->hasFirstTeamScore()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual/range {v18 .. v18}, Lcom/google/majel/proto/EcoutezStructuredResponse$Period;->getFirstTeamScore()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v15, v3}, Lcom/google/android/voicesearch/speechservice/BaseballResultData;->setFirstTeamScore(ILjava/lang/String;)V

    :cond_5
    invoke-virtual/range {v18 .. v18}, Lcom/google/majel/proto/EcoutezStructuredResponse$Period;->hasSecondTeamScore()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-virtual/range {v18 .. v18}, Lcom/google/majel/proto/EcoutezStructuredResponse$Period;->getSecondTeamScore()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v15, v3}, Lcom/google/android/voicesearch/speechservice/BaseballResultData;->setSecondTeamScore(ILjava/lang/String;)V

    :cond_6
    add-int/lit8 v15, v15, 0x1

    goto :goto_5

    :cond_7
    invoke-virtual/range {p1 .. p1}, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->getTeamData()Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->hasLastMatch()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-virtual/range {p1 .. p1}, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->getTeamData()Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->getLastMatch()Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    move-result-object v16

    goto/16 :goto_0

    :cond_8
    invoke-virtual/range {p1 .. p1}, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->getTeamData()Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->hasNextMatch()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-virtual/range {p1 .. p1}, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;->getTeamData()Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->getNextMatch()Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    move-result-object v16

    goto/16 :goto_0

    :cond_9
    const/4 v3, 0x1

    :goto_6
    return v3

    :cond_a
    const/4 v3, 0x1

    goto :goto_6

    :cond_b
    invoke-virtual/range {v16 .. v16}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasRecapUrl()Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-virtual/range {v16 .. v16}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getRecapUrl()Ljava/lang/String;

    move-result-object v8

    const v7, 0x7f0d014c

    goto/16 :goto_1

    :cond_c
    invoke-virtual/range {v16 .. v16}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasBoxUrl()Z

    move-result v3

    if-eqz v3, :cond_d

    invoke-virtual/range {v16 .. v16}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getBoxUrl()Ljava/lang/String;

    move-result-object v8

    const v7, 0x7f0d014d

    goto/16 :goto_1

    :cond_d
    invoke-virtual/range {v16 .. v16}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->hasPreviewUrl()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual/range {v16 .. v16}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;->getPreviewUrl()Ljava/lang/String;

    move-result-object v8

    const v7, 0x7f0d014e

    goto/16 :goto_1

    :cond_e
    const-string v13, ""

    goto/16 :goto_2

    :cond_f
    const-string v21, ""

    goto/16 :goto_3

    :catch_0
    move-exception v11

    const-string v3, "ActionProcessor"

    const-string v6, "Failed to parse elapsed time"

    invoke-static {v3, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    :cond_10
    invoke-virtual {v2, v9}, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->setBaseballData(Lcom/google/android/voicesearch/speechservice/BaseballResultData;)V

    :cond_11
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->mActionListener:Lcom/google/android/voicesearch/speechservice/ActionListener;

    invoke-interface {v3, v2}, Lcom/google/android/voicesearch/speechservice/ActionListener;->onSportsResult(Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;)V

    const/4 v3, 0x1

    goto :goto_6
.end method

.method private processStructuredResponse(Lcom/google/android/velvet/Query;Lcom/google/majel/proto/PeanutProtos$Peanut;)Z
    .locals 8
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Lcom/google/majel/proto/PeanutProtos$Peanut;

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-virtual {p2}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getStructuredResponse()Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;

    move-result-object v3

    invoke-direct {p0, p2}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->getAttribution(Lcom/google/majel/proto/PeanutProtos$Peanut;)Lcom/google/majel/proto/AttributionProtos$Attribution;

    move-result-object v0

    invoke-virtual {v3}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasCalculatorResultExtension()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->shouldSuppressAnswers(Lcom/google/android/velvet/Query;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v3}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->getCalculatorResultExtension()Lcom/google/majel/proto/CommonStructuredResponse$CalculatorResult;

    move-result-object v6

    invoke-direct {p0, v6, v0}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->processCalculatorResult(Lcom/google/majel/proto/CommonStructuredResponse$CalculatorResult;Lcom/google/majel/proto/AttributionProtos$Attribution;)Z

    move-result v6

    if-eqz v6, :cond_1

    :cond_0
    move v4, v5

    :cond_1
    :goto_0
    return v4

    :cond_2
    invoke-virtual {v3}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasWeatherResultExtension()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->shouldSuppressAnswers(Lcom/google/android/velvet/Query;)Z

    move-result v6

    if-nez v6, :cond_3

    invoke-virtual {v3}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->getWeatherResultExtension()Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;

    move-result-object v6

    invoke-direct {p0, v6, v0}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->processWeatherResult(Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;Lcom/google/majel/proto/AttributionProtos$Attribution;)Z

    move-result v6

    if-eqz v6, :cond_1

    :cond_3
    move v4, v5

    goto :goto_0

    :cond_4
    invoke-virtual {v3}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasEcoutezLocalResultsExtension()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-virtual {v3}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->getEcoutezLocalResultsExtension()Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;->getActionType()I

    move-result v6

    const/4 v7, 0x5

    if-eq v6, v7, :cond_6

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->shouldSuppressAnswers(Lcom/google/android/velvet/Query;)Z

    move-result v6

    if-nez v6, :cond_5

    invoke-direct {p0, p2, v2}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->processLocalResults(Lcom/google/majel/proto/PeanutProtos$Peanut;Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;)Z

    move-result v6

    if-eqz v6, :cond_6

    :cond_5
    :goto_1
    move v4, v5

    goto :goto_0

    :cond_6
    move v5, v4

    goto :goto_1

    :cond_7
    invoke-virtual {v3}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasFlightResultExtension()Z

    move-result v6

    if-eqz v6, :cond_9

    invoke-virtual {v3}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->getFlightResultExtension()Lcom/google/majel/proto/EcoutezStructuredResponse$FlightResult;

    move-result-object v1

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->shouldSuppressAnswers(Lcom/google/android/velvet/Query;)Z

    move-result v6

    if-nez v6, :cond_8

    invoke-direct {p0, v1}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->processFlightResult(Lcom/google/majel/proto/EcoutezStructuredResponse$FlightResult;)Z

    move-result v6

    if-eqz v6, :cond_1

    :cond_8
    move v4, v5

    goto :goto_0

    :cond_9
    invoke-virtual {v3}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasDictionaryResultExtension()Z

    move-result v6

    if-eqz v6, :cond_b

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->shouldSuppressAnswers(Lcom/google/android/velvet/Query;)Z

    move-result v6

    if-nez v6, :cond_a

    invoke-virtual {v3}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->getDictionaryResultExtension()Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->processDictionaryResult(Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;)Z

    move-result v6

    if-eqz v6, :cond_1

    :cond_a
    move v4, v5

    goto :goto_0

    :cond_b
    invoke-virtual {v3}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasSportsResultExtension()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->shouldSuppressAnswers(Lcom/google/android/velvet/Query;)Z

    move-result v6

    if-nez v6, :cond_c

    invoke-virtual {v3}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->getSportsResultExtension()Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->processSportsResult(Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;)Z

    move-result v6

    if-eqz v6, :cond_1

    :cond_c
    move v4, v5

    goto/16 :goto_0
.end method

.method private processUnsupportedAction(Lcom/google/majel/proto/ActionV2Protos$UnsupportedAction;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/speech/exception/ResponseRecognizeException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$UnsupportedAction;->hasExplanation()Z

    move-result v0

    const-string v1, "Unsupported action action without explanation"

    invoke-static {v0, v1}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->check(ZLjava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->mActionListener:Lcom/google/android/voicesearch/speechservice/ActionListener;

    invoke-interface {v0, p1}, Lcom/google/android/voicesearch/speechservice/ActionListener;->onUnsupportedAction(Lcom/google/majel/proto/ActionV2Protos$UnsupportedAction;)V

    return-void
.end method

.method private processUpdateSocialNetwork(Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;)V
    .locals 3
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;

    iget-object v1, p0, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->mActionListener:Lcom/google/android/voicesearch/speechservice/ActionListener;

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;->getStatusMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;->hasSocialNetwork()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;->getSocialNetwork()I

    move-result v0

    :goto_0
    invoke-interface {v1, v2, v0}, Lcom/google/android/voicesearch/speechservice/ActionListener;->onUpdateSocialNetwork(Ljava/lang/String;I)V

    return-void

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private processUrlResponse(Lcom/google/android/velvet/Query;Lcom/google/majel/proto/PeanutProtos$Url;)V
    .locals 3
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Lcom/google/majel/proto/PeanutProtos$Url;

    invoke-virtual {p2}, Lcom/google/majel/proto/PeanutProtos$Url;->hasSearchResultsInfo()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->shouldSuppressAnswers(Lcom/google/android/velvet/Query;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->mActionListener:Lcom/google/android/voicesearch/speechservice/ActionListener;

    invoke-interface {v0, p2}, Lcom/google/android/voicesearch/speechservice/ActionListener;->onSearchResults(Lcom/google/majel/proto/PeanutProtos$Url;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2}, Lcom/google/majel/proto/PeanutProtos$Url;->hasHtml()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Lcom/google/majel/proto/PeanutProtos$Url;->hasLink()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->shouldSuppressAnswers(Lcom/google/android/velvet/Query;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->mActionListener:Lcom/google/android/voicesearch/speechservice/ActionListener;

    invoke-virtual {p2}, Lcom/google/majel/proto/PeanutProtos$Url;->getRenderedLink()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/majel/proto/PeanutProtos$Url;->getHtml()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/voicesearch/speechservice/ActionListener;->onHtmlAnswer(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->mActionListener:Lcom/google/android/voicesearch/speechservice/ActionListener;

    invoke-static {p2}, Lcom/google/android/voicesearch/fragments/OpenUrlController;->createData(Lcom/google/majel/proto/PeanutProtos$Url;)Lcom/google/android/voicesearch/fragments/OpenUrlController$Data;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/speechservice/ActionListener;->onOpenURLAction(Lcom/google/android/voicesearch/fragments/OpenUrlController$Data;)V

    goto :goto_0
.end method

.method private processWeatherResult(Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;Lcom/google/majel/proto/AttributionProtos$Attribution;)Z
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;
    .param p2    # Lcom/google/majel/proto/AttributionProtos$Attribution;

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->mActionListener:Lcom/google/android/voicesearch/speechservice/ActionListener;

    invoke-interface {v0, p1}, Lcom/google/android/voicesearch/speechservice/ActionListener;->onWeatherResults(Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;)V

    const/4 v0, 0x1

    return v0
.end method

.method private shouldSuppressAnswers(Lcom/google/android/velvet/Query;)Z
    .locals 1
    .param p1    # Lcom/google/android/velvet/Query;

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->shouldSuppressAnswers()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public process(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/Action;Z)Z
    .locals 7
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Lcom/google/android/velvet/presenter/Action;
    .param p3    # Z

    const v6, 0x665b5c

    const/4 v3, 0x1

    invoke-virtual {p2}, Lcom/google/android/velvet/presenter/Action;->getPeanut()Lcom/google/majel/proto/PeanutProtos$Peanut;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/velvet/presenter/Action;->getEarsResponse()Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {p2}, Lcom/google/android/velvet/presenter/Action;->getEarsResponse()Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;

    move-result-object v4

    invoke-direct {p0, v4, p3}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->processEarsResult(Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;Z)V

    :goto_0
    return v3

    :cond_0
    invoke-virtual {p2}, Lcom/google/android/velvet/presenter/Action;->getRecognizeException()Lcom/google/android/speech/exception/RecognizeException;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {p2}, Lcom/google/android/velvet/presenter/Action;->getRecognizeException()Lcom/google/android/speech/exception/RecognizeException;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->processException(Lcom/google/android/speech/exception/RecognizeException;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p2}, Lcom/google/android/velvet/presenter/Action;->getPumpkinTaggerResult()Lcom/google/android/speech/embedded/TaggerResult;

    move-result-object v4

    if-eqz v4, :cond_3

    :try_start_0
    invoke-virtual {p2}, Lcom/google/android/velvet/presenter/Action;->getPumpkinTaggerResult()Lcom/google/android/speech/embedded/TaggerResult;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->processPumpkinTaggerResult(Lcom/google/android/speech/embedded/TaggerResult;)Z
    :try_end_0
    .catch Lcom/google/android/speech/exception/ResponseRecognizeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v3, "ActionProcessor"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error in processing PumpkinTagger result: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/android/speech/exception/ResponseRecognizeException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v6}, Lcom/google/android/voicesearch/logger/BugLogger;->record(I)V

    :cond_2
    :goto_1
    const/4 v3, 0x0

    goto :goto_0

    :cond_3
    if-eqz v1, :cond_2

    invoke-virtual {p2}, Lcom/google/android/velvet/presenter/Action;->getGogglesResults()Lcom/google/android/goggles/ResultSet;

    move-result-object v4

    if-eqz v4, :cond_5

    invoke-virtual {v1}, Lcom/google/majel/proto/PeanutProtos$Peanut;->hasStructuredResponse()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->shouldSuppressAnswers(Lcom/google/android/velvet/Query;)Z

    move-result v4

    if-nez v4, :cond_5

    invoke-virtual {v1}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getStructuredResponse()Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasGogglesGenericResultExtension()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {p2}, Lcom/google/android/velvet/presenter/Action;->getGogglesResults()Lcom/google/android/goggles/ResultSet;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/goggles/ResultSet;->getSessionId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getStructuredResponse()Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->getGogglesGenericResultExtension()Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$GogglesGenericResult;

    move-result-object v4

    invoke-direct {p0, v2, v4}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->processGogglesGenericResult(Ljava/lang/String;Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$GogglesGenericResult;)Z

    goto :goto_0

    :cond_4
    invoke-virtual {v1}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getStructuredResponse()Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasRecognizedContactExtension()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {v1}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getStructuredResponse()Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->getRecognizedContactExtension()Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->processRecognizedContact(Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact;)Z

    goto/16 :goto_0

    :cond_5
    :try_start_1
    invoke-direct {p0, p1, v1}, Lcom/google/android/voicesearch/speechservice/ActionProcessor;->processPeanut(Lcom/google/android/velvet/Query;Lcom/google/majel/proto/PeanutProtos$Peanut;)V
    :try_end_1
    .catch Lcom/google/android/speech/exception/ResponseRecognizeException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    :catch_1
    move-exception v0

    const-string v3, "ActionProcessor"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error in processing peanut: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/android/speech/exception/ResponseRecognizeException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v6}, Lcom/google/android/voicesearch/logger/BugLogger;->record(I)V

    goto :goto_1
.end method
