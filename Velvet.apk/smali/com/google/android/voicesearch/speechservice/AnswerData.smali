.class public Lcom/google/android/voicesearch/speechservice/AnswerData;
.super Ljava/lang/Object;
.source "AnswerData.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/voicesearch/speechservice/AnswerData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mAnswer:Ljava/lang/String;

.field private final mDescription:Ljava/lang/String;

.field private final mDisclaimer:Ljava/lang/String;

.field private final mDisclaimerUrl:Ljava/lang/String;

.field private final mImage:Lcom/google/android/voicesearch/speechservice/ImageParcelable;

.field private final mImageSource:Lcom/google/majel/proto/AttributionProtos$Attribution;

.field private final mSources:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/AttributionProtos$Attribution;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/voicesearch/speechservice/AnswerData$1;

    invoke-direct {v0}, Lcom/google/android/voicesearch/speechservice/AnswerData$1;-><init>()V

    sput-object v0, Lcom/google/android/voicesearch/speechservice/AnswerData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/speechservice/AnswerData;->mAnswer:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/speechservice/AnswerData;->mDescription:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/speechservice/AnswerData;->readAttributionList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/speechservice/AnswerData;->mSources:Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/speechservice/AnswerData;->readAttribution(Landroid/os/Parcel;)Lcom/google/majel/proto/AttributionProtos$Attribution;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/speechservice/AnswerData;->mImageSource:Lcom/google/majel/proto/AttributionProtos$Attribution;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/speechservice/AnswerData;->mDisclaimer:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/speechservice/AnswerData;->mDisclaimerUrl:Ljava/lang/String;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/speechservice/ImageParcelable;

    iput-object v0, p0, Lcom/google/android/voicesearch/speechservice/AnswerData;->mImage:Lcom/google/android/voicesearch/speechservice/ImageParcelable;

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/voicesearch/speechservice/AnswerData$1;)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
    .param p2    # Lcom/google/android/voicesearch/speechservice/AnswerData$1;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/speechservice/AnswerData;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/majel/proto/AttributionProtos$Attribution;Lcom/google/android/voicesearch/speechservice/ImageParcelable;Lcom/google/majel/proto/AttributionProtos$Attribution;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/majel/proto/AttributionProtos$Attribution;
    .param p4    # Lcom/google/android/voicesearch/speechservice/ImageParcelable;
    .param p5    # Lcom/google/majel/proto/AttributionProtos$Attribution;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/voicesearch/speechservice/AnswerData;->mAnswer:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/voicesearch/speechservice/AnswerData;->mDescription:Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/voicesearch/speechservice/AnswerData;->mSources:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/AnswerData;->mSources:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iput-object p4, p0, Lcom/google/android/voicesearch/speechservice/AnswerData;->mImage:Lcom/google/android/voicesearch/speechservice/ImageParcelable;

    iput-object p5, p0, Lcom/google/android/voicesearch/speechservice/AnswerData;->mImageSource:Lcom/google/majel/proto/AttributionProtos$Attribution;

    iput-object p6, p0, Lcom/google/android/voicesearch/speechservice/AnswerData;->mDisclaimer:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/voicesearch/speechservice/AnswerData;->mDisclaimerUrl:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/google/android/voicesearch/speechservice/ImageParcelable;Lcom/google/majel/proto/AttributionProtos$Attribution;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p4    # Lcom/google/android/voicesearch/speechservice/ImageParcelable;
    .param p5    # Lcom/google/majel/proto/AttributionProtos$Attribution;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/AttributionProtos$Attribution;",
            ">;",
            "Lcom/google/android/voicesearch/speechservice/ImageParcelable;",
            "Lcom/google/majel/proto/AttributionProtos$Attribution;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/voicesearch/speechservice/AnswerData;->mAnswer:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/voicesearch/speechservice/AnswerData;->mDescription:Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/speechservice/AnswerData;->mSources:Ljava/util/List;

    iput-object p4, p0, Lcom/google/android/voicesearch/speechservice/AnswerData;->mImage:Lcom/google/android/voicesearch/speechservice/ImageParcelable;

    iput-object p5, p0, Lcom/google/android/voicesearch/speechservice/AnswerData;->mImageSource:Lcom/google/majel/proto/AttributionProtos$Attribution;

    iput-object p6, p0, Lcom/google/android/voicesearch/speechservice/AnswerData;->mDisclaimer:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/voicesearch/speechservice/AnswerData;->mDisclaimerUrl:Ljava/lang/String;

    return-void
.end method

.method private readAttribution(Landroid/os/Parcel;)Lcom/google/majel/proto/AttributionProtos$Attribution;
    .locals 5
    .param p1    # Landroid/os/Parcel;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    new-array v0, v2, [B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readByteArray([B)V

    :try_start_0
    invoke-static {v0}, Lcom/google/majel/proto/AttributionProtos$Attribution;->parseFrom([B)Lcom/google/majel/proto/AttributionProtos$Attribution;
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    :goto_0
    return-object v3

    :catch_0
    move-exception v1

    const-string v3, "AnswerData"

    const-string v4, "Error in reading the proto"

    invoke-static {v3, v4, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v3, 0x0

    goto :goto_0
.end method

.method private readAttributionList(Landroid/os/Parcel;)Ljava/util/List;
    .locals 12
    .param p1    # Landroid/os/Parcel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Parcel;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/AttributionProtos$Attribution;",
            ">;"
        }
    .end annotation

    const/4 v9, 0x0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v8

    invoke-virtual {p1, v9}, Landroid/os/Parcel;->readArray(Ljava/lang/ClassLoader;)[Ljava/lang/Object;

    move-result-object v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v8}, Ljava/util/ArrayList;-><init>(I)V

    move-object v0, v3

    array-length v7, v0

    const/4 v6, 0x0

    :goto_0
    if-ge v6, v7, :cond_0

    aget-object v2, v0, v6

    :try_start_0
    check-cast v2, [B

    check-cast v2, [B

    invoke-static {v2}, Lcom/google/majel/proto/AttributionProtos$Attribution;->parseFrom([B)Lcom/google/majel/proto/AttributionProtos$Attribution;

    move-result-object v1

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :catch_0
    move-exception v5

    const-string v10, "AnswerData"

    const-string v11, "Error in reading the proto"

    invoke-static {v10, v11, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v4, v9

    :cond_0
    return-object v4
.end method

.method private writeAttribution(Landroid/os/Parcel;Lcom/google/majel/proto/AttributionProtos$Attribution;)V
    .locals 2
    .param p1    # Landroid/os/Parcel;
    .param p2    # Lcom/google/majel/proto/AttributionProtos$Attribution;

    invoke-virtual {p2}, Lcom/google/majel/proto/AttributionProtos$Attribution;->toByteArray()[B

    move-result-object v0

    array-length v1, v0

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    return-void
.end method

.method private writeAttributionList(Landroid/os/Parcel;Ljava/util/List;)V
    .locals 3
    .param p1    # Landroid/os/Parcel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Parcel;",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/AttributionProtos$Attribution;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    new-array v0, v2, [[B

    const/4 v1, 0x0

    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/majel/proto/AttributionProtos$Attribution;

    invoke-virtual {v2}, Lcom/google/majel/proto/AttributionProtos$Attribution;->toByteArray()[B

    move-result-object v2

    aput-object v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeArray([Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getAnswer()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/AnswerData;->mAnswer:Ljava/lang/String;

    return-object v0
.end method

.method public getAnswerDescription()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/AnswerData;->mDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getDisclaimer()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/AnswerData;->mDisclaimer:Ljava/lang/String;

    return-object v0
.end method

.method public getImage()Lcom/google/majel/proto/PeanutProtos$Image;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/AnswerData;->mImage:Lcom/google/android/voicesearch/speechservice/ImageParcelable;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/speechservice/ImageParcelable;->getImage()Lcom/google/majel/proto/PeanutProtos$Image;

    move-result-object v0

    return-object v0
.end method

.method public getImageSource()Lcom/google/majel/proto/AttributionProtos$Attribution;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/AnswerData;->mImageSource:Lcom/google/majel/proto/AttributionProtos$Attribution;

    return-object v0
.end method

.method public getSource()Lcom/google/majel/proto/AttributionProtos$Attribution;
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/AnswerData;->mSources:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/majel/proto/AttributionProtos$Attribution;

    invoke-direct {v0}, Lcom/google/majel/proto/AttributionProtos$Attribution;-><init>()V

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/AnswerData;->mSources:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/AttributionProtos$Attribution;

    goto :goto_0
.end method

.method public getSourceList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/AttributionProtos$Attribution;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/AnswerData;->mSources:Ljava/util/List;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/AnswerData;->mAnswer:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/AnswerData;->mDescription:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/AnswerData;->mSources:Ljava/util/List;

    invoke-direct {p0, p1, v0}, Lcom/google/android/voicesearch/speechservice/AnswerData;->writeAttributionList(Landroid/os/Parcel;Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/AnswerData;->mImageSource:Lcom/google/majel/proto/AttributionProtos$Attribution;

    invoke-direct {p0, p1, v0}, Lcom/google/android/voicesearch/speechservice/AnswerData;->writeAttribution(Landroid/os/Parcel;Lcom/google/majel/proto/AttributionProtos$Attribution;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/AnswerData;->mDisclaimer:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/AnswerData;->mDisclaimerUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/AnswerData;->mImage:Lcom/google/android/voicesearch/speechservice/ImageParcelable;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
