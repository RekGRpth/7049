.class public Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;
.super Ljava/lang/Object;
.source "BaseballResultData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/speechservice/BaseballResultData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BaseballTeamData"
.end annotation


# instance fields
.field private mErrors:I

.field private mHits:I

.field private mRuns:I


# direct methods
.method public constructor <init>(III)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;->mHits:I

    iput p2, p0, Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;->mRuns:I

    iput p3, p0, Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;->mErrors:I

    return-void
.end method


# virtual methods
.method public getErrors()I
    .locals 1

    iget v0, p0, Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;->mErrors:I

    return v0
.end method

.method public getHits()I
    .locals 1

    iget v0, p0, Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;->mHits:I

    return v0
.end method

.method public getRuns()I
    .locals 1

    iget v0, p0, Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;->mRuns:I

    return v0
.end method
