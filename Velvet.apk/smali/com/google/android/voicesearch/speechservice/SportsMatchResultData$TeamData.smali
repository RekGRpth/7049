.class public Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;
.super Ljava/lang/Object;
.source "SportsMatchResultData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TeamData"
.end annotation


# instance fields
.field private final mLogoUri:Landroid/net/Uri;

.field private final mName:Ljava/lang/String;

.field private final mScore:Ljava/lang/String;

.field private final mShortName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Landroid/net/Uri;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p3, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;->mScore:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;->mShortName:Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;->mName:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;->mLogoUri:Landroid/net/Uri;

    return-void
.end method


# virtual methods
.method public getLogoUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;->mLogoUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getScore()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;->mScore:Ljava/lang/String;

    return-object v0
.end method

.method public getShortName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;->mShortName:Ljava/lang/String;

    return-object v0
.end method
