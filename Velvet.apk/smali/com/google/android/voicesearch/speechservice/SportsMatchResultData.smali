.class public Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;
.super Ljava/lang/Object;
.source "SportsMatchResultData.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mBaseballData:Lcom/google/android/voicesearch/speechservice/BaseballResultData;

.field private mCurrentPeriod:I

.field private mElapsedTimeMillis:J

.field private final mFirstTeam:Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;

.field private final mInfoUrl:Ljava/lang/String;

.field private final mInfoUrlTitle:I

.field private final mSecondTeam:Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;

.field private mStartDateTimeMillis:J

.field private mStatus:I

.field private final mTitle:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$1;

    invoke-direct {v0}, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$1;-><init>()V

    sput-object v0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 10
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mTitle:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    new-instance v8, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    invoke-direct {v8, v1, v3, v2, v9}, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    iput-object v8, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mFirstTeam:Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    new-instance v8, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    invoke-direct {v8, v5, v7, v6, v9}, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    iput-object v8, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mSecondTeam:Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v8

    iput v8, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mStatus:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v8

    iput v8, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mInfoUrlTitle:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mInfoUrl:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v8

    iput-wide v8, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mStartDateTimeMillis:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v8

    iput v8, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mCurrentPeriod:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v8

    iput-wide v8, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mElapsedTimeMillis:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_0

    sget-object v8, Lcom/google/android/voicesearch/speechservice/BaseballResultData;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/voicesearch/speechservice/BaseballResultData;

    iput-object v8, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mBaseballData:Lcom/google/android/voicesearch/speechservice/BaseballResultData;

    :cond_0
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$1;)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
    .param p2    # Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$1;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;IILjava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;
    .param p3    # Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;
    .param p4    # I
    .param p5    # I
    .param p6    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mTitle:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mFirstTeam:Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;

    iput-object p3, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mSecondTeam:Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;

    iput p4, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mStatus:I

    iput p5, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mInfoUrlTitle:I

    iput-object p6, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mInfoUrl:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mCurrentPeriod:I

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getBaseballData()Lcom/google/android/voicesearch/speechservice/BaseballResultData;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mBaseballData:Lcom/google/android/voicesearch/speechservice/BaseballResultData;

    return-object v0
.end method

.method public getCurrentPeriod()I
    .locals 1

    iget v0, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mCurrentPeriod:I

    return v0
.end method

.method public getElapsedTimeMillis()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mElapsedTimeMillis:J

    return-wide v0
.end method

.method public getFirstTeamData()Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mFirstTeam:Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;

    return-object v0
.end method

.method public getInfoUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mInfoUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getInfoUrlTitle()I
    .locals 1

    iget v0, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mInfoUrlTitle:I

    return v0
.end method

.method public getSecondTeamData()Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mSecondTeam:Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;

    return-object v0
.end method

.method public getStartDateTimeMillis()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mStartDateTimeMillis:J

    return-wide v0
.end method

.method public getStatus()I
    .locals 1

    iget v0, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mStatus:I

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public setBaseballData(Lcom/google/android/voicesearch/speechservice/BaseballResultData;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/speechservice/BaseballResultData;

    iput-object p1, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mBaseballData:Lcom/google/android/voicesearch/speechservice/BaseballResultData;

    return-void
.end method

.method public setCurrentPeriod(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mCurrentPeriod:I

    return-void
.end method

.method public setElapsedTimeMillis(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mElapsedTimeMillis:J

    return-void
.end method

.method public setStartDateTimeMillis(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mStartDateTimeMillis:J

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mFirstTeam:Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mFirstTeam:Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;->getShortName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mFirstTeam:Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;->getScore()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mFirstTeam:Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;->getLogoUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mSecondTeam:Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mSecondTeam:Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;->getShortName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mSecondTeam:Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;->getScore()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mSecondTeam:Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;->getLogoUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mStatus:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mInfoUrlTitle:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mInfoUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-wide v0, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mStartDateTimeMillis:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget v0, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mCurrentPeriod:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-wide v0, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mElapsedTimeMillis:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mBaseballData:Lcom/google/android/voicesearch/speechservice/BaseballResultData;

    if-nez v0, :cond_0

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->mBaseballData:Lcom/google/android/voicesearch/speechservice/BaseballResultData;

    invoke-virtual {v0, p1, v2}, Lcom/google/android/voicesearch/speechservice/BaseballResultData;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0
.end method
