.class public Lcom/google/android/voicesearch/speechservice/s3/PinholeParamsBuilder;
.super Ljava/lang/Object;
.source "PinholeParamsBuilder.java"


# instance fields
.field private final mCorpora:Lcom/google/android/velvet/Corpora;

.field private final mSearchUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

.field private final mUserAgentSupplier:Lcom/google/common/base/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/google/SearchUrlHelper;Lcom/google/android/velvet/Corpora;Lcom/google/common/base/Supplier;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/google/SearchUrlHelper;
    .param p2    # Lcom/google/android/velvet/Corpora;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/google/SearchUrlHelper;",
            "Lcom/google/android/velvet/Corpora;",
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/voicesearch/speechservice/s3/PinholeParamsBuilder;->mSearchUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    iput-object p2, p0, Lcom/google/android/voicesearch/speechservice/s3/PinholeParamsBuilder;->mCorpora:Lcom/google/android/velvet/Corpora;

    iput-object p3, p0, Lcom/google/android/voicesearch/speechservice/s3/PinholeParamsBuilder;->mUserAgentSupplier:Lcom/google/common/base/Supplier;

    return-void
.end method

.method private getSyntheticQueryForPinhole()Lcom/google/android/velvet/Query;
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/s3/PinholeParamsBuilder;->mCorpora:Lcom/google/android/velvet/Corpora;

    invoke-virtual {v0}, Lcom/google/android/velvet/Corpora;->waitForWebCorpus()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "VS.PinholeParamsBuilder"

    const-string v1, "Timed out waiting for web corpus load."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/velvet/Query;->EMPTY:Lcom/google/android/velvet/Query;

    iget-object v1, p0, Lcom/google/android/voicesearch/speechservice/s3/PinholeParamsBuilder;->mCorpora:Lcom/google/android/velvet/Corpora;

    invoke-virtual {v1}, Lcom/google/android/velvet/Corpora;->getWebCorpus()Lcom/google/android/velvet/WebCorpus;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query;->voiceSearchForPinholeWithCorpus(Lcom/google/android/velvet/WebCorpus;)Lcom/google/android/velvet/Query;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method buildParams(Lcom/google/common/base/Supplier;)Lcom/google/speech/s3/PinholeStream$PinholeParams;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/speech/s3/PinholeStream$PinholeParams;"
        }
    .end annotation

    const/4 v5, 0x0

    invoke-direct {p0}, Lcom/google/android/voicesearch/speechservice/s3/PinholeParamsBuilder;->getSyntheticQueryForPinhole()Lcom/google/android/velvet/Query;

    move-result-object v7

    if-nez v7, :cond_1

    :cond_0
    :goto_0
    return-object v5

    :cond_1
    iget-object v11, p0, Lcom/google/android/voicesearch/speechservice/s3/PinholeParamsBuilder;->mSearchUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    invoke-virtual {v11}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSingleRequestBaseUrl()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v9}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v9}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v6

    if-eqz v8, :cond_2

    if-nez v6, :cond_3

    :cond_2
    const-string v11, "VS.PinholeParamsBuilder"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Invalid request URL: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, p0, Lcom/google/android/voicesearch/speechservice/s3/PinholeParamsBuilder;->mSearchUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    invoke-virtual {v13}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSingleRequestBaseUrl()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    iget-object v12, p0, Lcom/google/android/voicesearch/speechservice/s3/PinholeParamsBuilder;->mSearchUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    invoke-interface {p1}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    invoke-virtual {v12, v9, v7, v11}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSearchUrlBuilder(Landroid/net/Uri;Lcom/google/android/velvet/Query;Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setHostHeader(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    iget-object v11, p0, Lcom/google/android/voicesearch/speechservice/s3/PinholeParamsBuilder;->mUserAgentSupplier:Lcom/google/common/base/Supplier;

    invoke-interface {v11}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    invoke-virtual {v0, v11}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setUserAgent(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    new-instance v5, Lcom/google/speech/s3/PinholeStream$PinholeParams;

    invoke-direct {v5}, Lcom/google/speech/s3/PinholeStream$PinholeParams;-><init>()V

    invoke-virtual {v5, v6}, Lcom/google/speech/s3/PinholeStream$PinholeParams;->setUrlPath(Ljava/lang/String;)Lcom/google/speech/s3/PinholeStream$PinholeParams;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->build()Lcom/google/android/searchcommon/util/UriRequest;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/searchcommon/util/UriRequest;->getUniqueParams()Ljava/util/Map;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v12

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Null parameter: "

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v12, v11}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lcom/google/speech/s3/PinholeStream$PinholeCgiParam;

    invoke-direct {v1}, Lcom/google/speech/s3/PinholeStream$PinholeCgiParam;-><init>()V

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    invoke-virtual {v1, v11}, Lcom/google/speech/s3/PinholeStream$PinholeCgiParam;->setKey(Ljava/lang/String;)Lcom/google/speech/s3/PinholeStream$PinholeCgiParam;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    invoke-virtual {v1, v11}, Lcom/google/speech/s3/PinholeStream$PinholeCgiParam;->setValue(Ljava/lang/String;)Lcom/google/speech/s3/PinholeStream$PinholeCgiParam;

    invoke-virtual {v5, v1}, Lcom/google/speech/s3/PinholeStream$PinholeParams;->addCgiParams(Lcom/google/speech/s3/PinholeStream$PinholeCgiParam;)Lcom/google/speech/s3/PinholeStream$PinholeParams;

    goto :goto_1

    :cond_4
    invoke-virtual {v10}, Lcom/google/android/searchcommon/util/UriRequest;->getHeaders()Ljava/util/Map;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v12

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Null header: "

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v12, v11}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v3, Lcom/google/speech/s3/PinholeStream$PinholeHeader;

    invoke-direct {v3}, Lcom/google/speech/s3/PinholeStream$PinholeHeader;-><init>()V

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    invoke-virtual {v3, v11}, Lcom/google/speech/s3/PinholeStream$PinholeHeader;->setKey(Ljava/lang/String;)Lcom/google/speech/s3/PinholeStream$PinholeHeader;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    invoke-virtual {v3, v11}, Lcom/google/speech/s3/PinholeStream$PinholeHeader;->setValue(Ljava/lang/String;)Lcom/google/speech/s3/PinholeStream$PinholeHeader;

    invoke-virtual {v5, v3}, Lcom/google/speech/s3/PinholeStream$PinholeParams;->addHeaders(Lcom/google/speech/s3/PinholeStream$PinholeHeader;)Lcom/google/speech/s3/PinholeStream$PinholeParams;

    goto :goto_2
.end method

.method public getPinholeParamsCallable(Lcom/google/common/base/Supplier;)Ljava/util/concurrent/Callable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/google/speech/s3/PinholeStream$PinholeParams;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/voicesearch/speechservice/s3/PinholeParamsBuilder$1;

    const-string v1, "PinholeParamsBuilderTask"

    invoke-direct {v0, p0, v1, p1}, Lcom/google/android/voicesearch/speechservice/s3/PinholeParamsBuilder$1;-><init>(Lcom/google/android/voicesearch/speechservice/s3/PinholeParamsBuilder;Ljava/lang/String;Lcom/google/common/base/Supplier;)V

    return-object v0
.end method
