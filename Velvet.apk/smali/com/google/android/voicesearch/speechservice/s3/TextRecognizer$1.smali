.class Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer$1;
.super Ljava/lang/Object;
.source "TextRecognizer.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;->start(Lcom/google/android/velvet/Query;Lcom/google/android/speech/callback/Callback;Ljava/util/concurrent/Executor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;

.field final synthetic val$callbackExecutor:Ljava/util/concurrent/Executor;

.field final synthetic val$params:Lcom/google/android/speech/params/RecognizerParams;

.field final synthetic val$query:Lcom/google/android/velvet/Query;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;Lcom/google/android/velvet/Query;Lcom/google/android/speech/params/RecognizerParams;Ljava/util/concurrent/Executor;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer$1;->this$0:Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;

    iput-object p2, p0, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer$1;->val$query:Lcom/google/android/velvet/Query;

    iput-object p3, p0, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer$1;->val$params:Lcom/google/android/speech/params/RecognizerParams;

    iput-object p4, p0, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer$1;->val$callbackExecutor:Ljava/util/concurrent/Executor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer$1;->call()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public call()Ljava/lang/Void;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer$1;->this$0:Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;

    iget-object v1, p0, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer$1;->val$query:Lcom/google/android/velvet/Query;

    iget-object v2, p0, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer$1;->val$params:Lcom/google/android/speech/params/RecognizerParams;

    iget-object v3, p0, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer$1;->val$callbackExecutor:Ljava/util/concurrent/Executor;

    # invokes: Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;->getResponseAndDispatchCallback(Lcom/google/android/velvet/Query;Lcom/google/android/speech/params/RecognizerParams;Ljava/util/concurrent/Executor;)V
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;->access$000(Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;Lcom/google/android/velvet/Query;Lcom/google/android/speech/params/RecognizerParams;Ljava/util/concurrent/Executor;)V

    const/4 v0, 0x0

    return-object v0
.end method
