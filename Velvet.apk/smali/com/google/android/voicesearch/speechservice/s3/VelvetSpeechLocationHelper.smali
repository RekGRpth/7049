.class public Lcom/google/android/voicesearch/speechservice/s3/VelvetSpeechLocationHelper;
.super Ljava/lang/Object;
.source "VelvetSpeechLocationHelper.java"

# interfaces
.implements Lcom/google/android/speech/helper/SpeechLocationHelper;


# instance fields
.field private final mOracle:Lcom/google/android/apps/sidekick/inject/LocationOracle;

.field private final mSettings:Lcom/google/android/searchcommon/google/LocationSettings;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/google/LocationSettings;Lcom/google/android/apps/sidekick/inject/LocationOracle;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/google/LocationSettings;
    .param p2    # Lcom/google/android/apps/sidekick/inject/LocationOracle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/voicesearch/speechservice/s3/VelvetSpeechLocationHelper;->mSettings:Lcom/google/android/searchcommon/google/LocationSettings;

    iput-object p2, p0, Lcom/google/android/voicesearch/speechservice/s3/VelvetSpeechLocationHelper;->mOracle:Lcom/google/android/apps/sidekick/inject/LocationOracle;

    return-void
.end method


# virtual methods
.method public getXGeoLocation()Ljava/lang/String;
    .locals 2

    iget-object v1, p0, Lcom/google/android/voicesearch/speechservice/s3/VelvetSpeechLocationHelper;->mSettings:Lcom/google/android/searchcommon/google/LocationSettings;

    invoke-interface {v1}, Lcom/google/android/searchcommon/google/LocationSettings;->canUseLocationForSearch()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/voicesearch/speechservice/s3/VelvetSpeechLocationHelper;->mOracle:Lcom/google/android/apps/sidekick/inject/LocationOracle;

    invoke-interface {v1}, Lcom/google/android/apps/sidekick/inject/LocationOracle;->getBestLocation()Landroid/location/Location;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/google/android/searchcommon/google/XGeoEncoder;->encodeLocation(Landroid/location/Location;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public shouldSendLocation()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/speechservice/s3/VelvetSpeechLocationHelper;->mSettings:Lcom/google/android/searchcommon/google/LocationSettings;

    invoke-interface {v0}, Lcom/google/android/searchcommon/google/LocationSettings;->canUseLocationForSearch()Z

    move-result v0

    return v0
.end method
