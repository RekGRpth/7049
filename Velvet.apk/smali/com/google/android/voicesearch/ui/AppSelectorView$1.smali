.class Lcom/google/android/voicesearch/ui/AppSelectorView$1;
.super Ljava/lang/Object;
.source "AppSelectorView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/ui/AppSelectorView;->setSelectorApps(Ljava/util/List;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/ui/AppSelectorView;

.field final synthetic val$apps:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/ui/AppSelectorView;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/ui/AppSelectorView$1;->this$0:Lcom/google/android/voicesearch/ui/AppSelectorView;

    iput-object p2, p0, Lcom/google/android/voicesearch/ui/AppSelectorView$1;->val$apps:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/AppSelectorView$1;->this$0:Lcom/google/android/voicesearch/ui/AppSelectorView;

    # getter for: Lcom/google/android/voicesearch/ui/AppSelectorView;->mOnAppSelectedListener:Lcom/google/android/voicesearch/ui/AppSelectorView$OnAppSelectedListener;
    invoke-static {v0}, Lcom/google/android/voicesearch/ui/AppSelectorView;->access$000(Lcom/google/android/voicesearch/ui/AppSelectorView;)Lcom/google/android/voicesearch/ui/AppSelectorView$OnAppSelectedListener;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/AppSelectorView$1;->val$apps:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/util/AppSelectionHelper$App;

    invoke-interface {v1, v0}, Lcom/google/android/voicesearch/ui/AppSelectorView$OnAppSelectedListener;->onAppSelected(Lcom/google/android/voicesearch/util/AppSelectionHelper$App;)V

    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    return-void
.end method
