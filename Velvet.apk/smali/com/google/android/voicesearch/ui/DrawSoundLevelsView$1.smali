.class Lcom/google/android/voicesearch/ui/DrawSoundLevelsView$1;
.super Ljava/lang/Object;
.source "DrawSoundLevelsView.java"

# interfaces
.implements Landroid/animation/TimeAnimator$TimeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView$1;->this$0:Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTimeUpdate(Landroid/animation/TimeAnimator;JJ)V
    .locals 3
    .param p1    # Landroid/animation/TimeAnimator;
    .param p2    # J
    .param p4    # J

    iget-object v1, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView$1;->this$0:Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;

    # getter for: Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->mLevelSource:Lcom/google/android/speech/SpeechLevelSource;
    invoke-static {v1}, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->access$000(Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;)Lcom/google/android/speech/SpeechLevelSource;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/speech/SpeechLevelSource;->getSpeechLevel()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView$1;->this$0:Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;

    # getter for: Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->mCurrentVolume:I
    invoke-static {v1}, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->access$100(Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;)I

    move-result v1

    if-le v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView$1;->this$0:Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;

    iget-object v2, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView$1;->this$0:Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;

    # getter for: Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->mCurrentVolume:I
    invoke-static {v2}, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->access$100(Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;)I

    move-result v2

    add-int/lit8 v2, v2, 0xa

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    # setter for: Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->mCurrentVolume:I
    invoke-static {v1, v2}, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->access$102(Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;I)I

    :goto_0
    iget-object v1, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView$1;->this$0:Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->invalidate()V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView$1;->this$0:Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;

    iget-object v2, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView$1;->this$0:Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;

    # getter for: Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->mCurrentVolume:I
    invoke-static {v2}, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->access$100(Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;)I

    move-result v2

    add-int/lit8 v2, v2, -0xa

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    # setter for: Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->mCurrentVolume:I
    invoke-static {v1, v2}, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->access$102(Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;I)I

    goto :goto_0
.end method
