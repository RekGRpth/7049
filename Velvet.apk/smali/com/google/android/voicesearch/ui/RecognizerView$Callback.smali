.class public interface abstract Lcom/google/android/voicesearch/ui/RecognizerView$Callback;
.super Ljava/lang/Object;
.source "RecognizerView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/ui/RecognizerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Callback"
.end annotation


# virtual methods
.method public abstract onCancelRecordingClicked()V
.end method

.method public abstract onStartRecordingClicked()V
.end method

.method public abstract onStopRecordingClicked()V
.end method
