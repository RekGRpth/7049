.class final enum Lcom/google/android/voicesearch/ui/RecognizerView$State;
.super Ljava/lang/Enum;
.source "RecognizerView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/ui/RecognizerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/voicesearch/ui/RecognizerView$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/voicesearch/ui/RecognizerView$State;

.field public static final enum LISTENING:Lcom/google/android/voicesearch/ui/RecognizerView$State;

.field public static final enum MIC_INITIALIZING:Lcom/google/android/voicesearch/ui/RecognizerView$State;

.field public static final enum NOT_LISTENING:Lcom/google/android/voicesearch/ui/RecognizerView$State;

.field public static final enum RECOGNIZING:Lcom/google/android/voicesearch/ui/RecognizerView$State;

.field public static final enum RECORDING:Lcom/google/android/voicesearch/ui/RecognizerView$State;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/voicesearch/ui/RecognizerView$State;

    const-string v1, "NOT_LISTENING"

    invoke-direct {v0, v1, v2}, Lcom/google/android/voicesearch/ui/RecognizerView$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/voicesearch/ui/RecognizerView$State;->NOT_LISTENING:Lcom/google/android/voicesearch/ui/RecognizerView$State;

    new-instance v0, Lcom/google/android/voicesearch/ui/RecognizerView$State;

    const-string v1, "MIC_INITIALIZING"

    invoke-direct {v0, v1, v3}, Lcom/google/android/voicesearch/ui/RecognizerView$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/voicesearch/ui/RecognizerView$State;->MIC_INITIALIZING:Lcom/google/android/voicesearch/ui/RecognizerView$State;

    new-instance v0, Lcom/google/android/voicesearch/ui/RecognizerView$State;

    const-string v1, "LISTENING"

    invoke-direct {v0, v1, v4}, Lcom/google/android/voicesearch/ui/RecognizerView$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/voicesearch/ui/RecognizerView$State;->LISTENING:Lcom/google/android/voicesearch/ui/RecognizerView$State;

    new-instance v0, Lcom/google/android/voicesearch/ui/RecognizerView$State;

    const-string v1, "RECORDING"

    invoke-direct {v0, v1, v5}, Lcom/google/android/voicesearch/ui/RecognizerView$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/voicesearch/ui/RecognizerView$State;->RECORDING:Lcom/google/android/voicesearch/ui/RecognizerView$State;

    new-instance v0, Lcom/google/android/voicesearch/ui/RecognizerView$State;

    const-string v1, "RECOGNIZING"

    invoke-direct {v0, v1, v6}, Lcom/google/android/voicesearch/ui/RecognizerView$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/voicesearch/ui/RecognizerView$State;->RECOGNIZING:Lcom/google/android/voicesearch/ui/RecognizerView$State;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/voicesearch/ui/RecognizerView$State;

    sget-object v1, Lcom/google/android/voicesearch/ui/RecognizerView$State;->NOT_LISTENING:Lcom/google/android/voicesearch/ui/RecognizerView$State;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/voicesearch/ui/RecognizerView$State;->MIC_INITIALIZING:Lcom/google/android/voicesearch/ui/RecognizerView$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/voicesearch/ui/RecognizerView$State;->LISTENING:Lcom/google/android/voicesearch/ui/RecognizerView$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/voicesearch/ui/RecognizerView$State;->RECORDING:Lcom/google/android/voicesearch/ui/RecognizerView$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/voicesearch/ui/RecognizerView$State;->RECOGNIZING:Lcom/google/android/voicesearch/ui/RecognizerView$State;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/voicesearch/ui/RecognizerView$State;->$VALUES:[Lcom/google/android/voicesearch/ui/RecognizerView$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/voicesearch/ui/RecognizerView$State;
    .locals 1

    const-class v0, Lcom/google/android/voicesearch/ui/RecognizerView$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/ui/RecognizerView$State;

    return-object v0
.end method

.method public static values()[Lcom/google/android/voicesearch/ui/RecognizerView$State;
    .locals 1

    sget-object v0, Lcom/google/android/voicesearch/ui/RecognizerView$State;->$VALUES:[Lcom/google/android/voicesearch/ui/RecognizerView$State;

    invoke-virtual {v0}, [Lcom/google/android/voicesearch/ui/RecognizerView$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/voicesearch/ui/RecognizerView$State;

    return-object v0
.end method
