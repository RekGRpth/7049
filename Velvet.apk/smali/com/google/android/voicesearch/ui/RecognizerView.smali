.class public Lcom/google/android/voicesearch/ui/RecognizerView;
.super Landroid/widget/RelativeLayout;
.source "RecognizerView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/ui/RecognizerView$2;,
        Lcom/google/android/voicesearch/ui/RecognizerView$SavedState;,
        Lcom/google/android/voicesearch/ui/RecognizerView$Callback;,
        Lcom/google/android/voicesearch/ui/RecognizerView$State;
    }
.end annotation


# instance fields
.field private mCallback:Lcom/google/android/voicesearch/ui/RecognizerView$Callback;

.field protected mMicButton:Landroid/widget/ImageView;

.field private mSoundLevels:Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;

.field private mState:Lcom/google/android/voicesearch/ui/RecognizerView$State;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/ui/RecognizerView;)Lcom/google/android/voicesearch/ui/RecognizerView$State;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/ui/RecognizerView;

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/RecognizerView;->mState:Lcom/google/android/voicesearch/ui/RecognizerView$State;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/voicesearch/ui/RecognizerView;)Lcom/google/android/voicesearch/ui/RecognizerView$Callback;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/ui/RecognizerView;

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/RecognizerView;->mCallback:Lcom/google/android/voicesearch/ui/RecognizerView$Callback;

    return-object v0
.end method

.method private updateState(Lcom/google/android/voicesearch/ui/RecognizerView$State;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/ui/RecognizerView$State;

    iput-object p1, p0, Lcom/google/android/voicesearch/ui/RecognizerView;->mState:Lcom/google/android/voicesearch/ui/RecognizerView$State;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/RecognizerView;->refreshUi()V

    return-void
.end method


# virtual methods
.method public onAttachedToWindow()V
    .locals 0

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onAttachedToWindow()V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/RecognizerView;->refreshUi()V

    return-void
.end method

.method public onFinishInflate()V
    .locals 2

    const v0, 0x7f1001fb

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/RecognizerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/RecognizerView;->mSoundLevels:Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;

    const v0, 0x7f100147

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/RecognizerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/RecognizerView;->mMicButton:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/RecognizerView;->mMicButton:Landroid/widget/ImageView;

    new-instance v1, Lcom/google/android/voicesearch/ui/RecognizerView$1;

    invoke-direct {v1, p0}, Lcom/google/android/voicesearch/ui/RecognizerView$1;-><init>(Lcom/google/android/voicesearch/ui/RecognizerView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget-object v0, Lcom/google/android/voicesearch/ui/RecognizerView$State;->NOT_LISTENING:Lcom/google/android/voicesearch/ui/RecognizerView$State;

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/RecognizerView;->mState:Lcom/google/android/voicesearch/ui/RecognizerView$State;

    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    const-class v0, Lcom/google/android/voicesearch/ui/RecognizerView;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2
    .param p1    # Landroid/os/Parcelable;

    instance-of v1, p1, Lcom/google/android/voicesearch/ui/RecognizerView$SavedState;

    if-nez v1, :cond_0

    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    :goto_0
    return-void

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/voicesearch/ui/RecognizerView$SavedState;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ui/RecognizerView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/widget/RelativeLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget-object v1, v0, Lcom/google/android/voicesearch/ui/RecognizerView$SavedState;->mState:Lcom/google/android/voicesearch/ui/RecognizerView$State;

    iput-object v1, p0, Lcom/google/android/voicesearch/ui/RecognizerView;->mState:Lcom/google/android/voicesearch/ui/RecognizerView$State;

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    new-instance v0, Lcom/google/android/voicesearch/ui/RecognizerView$SavedState;

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/voicesearch/ui/RecognizerView$SavedState;-><init>(Landroid/os/Parcelable;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/ui/RecognizerView;->mState:Lcom/google/android/voicesearch/ui/RecognizerView$State;

    iput-object v1, v0, Lcom/google/android/voicesearch/ui/RecognizerView$SavedState;->mState:Lcom/google/android/voicesearch/ui/RecognizerView$State;

    return-object v0
.end method

.method protected refreshUi()V
    .locals 8

    const v7, 0x7f0d0427

    const v6, 0x7f02018b

    const v5, 0x7f020189

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/RecognizerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d038d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/android/voicesearch/ui/RecognizerView$2;->$SwitchMap$com$google$android$voicesearch$ui$RecognizerView$State:[I

    iget-object v2, p0, Lcom/google/android/voicesearch/ui/RecognizerView;->mState:Lcom/google/android/voicesearch/ui/RecognizerView$State;

    invoke-virtual {v2}, Lcom/google/android/voicesearch/ui/RecognizerView$State;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :goto_0
    iget-object v1, p0, Lcom/google/android/voicesearch/ui/RecognizerView;->mMicButton:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/voicesearch/ui/RecognizerView;->mMicButton:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v1, p0, Lcom/google/android/voicesearch/ui/RecognizerView;->mSoundLevels:Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;

    invoke-virtual {v1, v3}, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->setEnabled(Z)V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/google/android/voicesearch/ui/RecognizerView;->mMicButton:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/RecognizerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/ui/RecognizerView;->mSoundLevels:Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;

    invoke-virtual {v1, v4}, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->setEnabled(Z)V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/google/android/voicesearch/ui/RecognizerView;->mMicButton:Landroid/widget/ImageView;

    const v2, 0x7f02018e

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/RecognizerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0426

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/ui/RecognizerView;->mSoundLevels:Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;

    invoke-virtual {v1, v4}, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->setEnabled(Z)V

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/google/android/voicesearch/ui/RecognizerView;->mMicButton:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/RecognizerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0425

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/ui/RecognizerView;->mSoundLevels:Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;

    invoke-virtual {v1, v3}, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->setEnabled(Z)V

    goto :goto_0

    :pswitch_4
    iget-object v1, p0, Lcom/google/android/voicesearch/ui/RecognizerView;->mMicButton:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/RecognizerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/ui/RecognizerView;->mSoundLevels:Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;

    invoke-virtual {v1, v3}, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->setEnabled(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public setCallback(Lcom/google/android/voicesearch/ui/RecognizerView$Callback;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/ui/RecognizerView$Callback;

    iput-object p1, p0, Lcom/google/android/voicesearch/ui/RecognizerView;->mCallback:Lcom/google/android/voicesearch/ui/RecognizerView$Callback;

    return-void
.end method

.method public setSpeechLevelSource(Lcom/google/android/speech/SpeechLevelSource;)V
    .locals 1
    .param p1    # Lcom/google/android/speech/SpeechLevelSource;

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/RecognizerView;->mSoundLevels:Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->setLevelSource(Lcom/google/android/speech/SpeechLevelSource;)V

    return-void
.end method

.method public showInitializingMic()V
    .locals 1

    sget-object v0, Lcom/google/android/voicesearch/ui/RecognizerView$State;->MIC_INITIALIZING:Lcom/google/android/voicesearch/ui/RecognizerView$State;

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/ui/RecognizerView;->updateState(Lcom/google/android/voicesearch/ui/RecognizerView$State;)V

    return-void
.end method

.method public showListening()V
    .locals 1

    const/4 v0, 0x5

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    sget-object v0, Lcom/google/android/voicesearch/ui/RecognizerView$State;->LISTENING:Lcom/google/android/voicesearch/ui/RecognizerView$State;

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/ui/RecognizerView;->updateState(Lcom/google/android/voicesearch/ui/RecognizerView$State;)V

    return-void
.end method

.method public showNotListening()V
    .locals 1

    sget-object v0, Lcom/google/android/voicesearch/ui/RecognizerView$State;->NOT_LISTENING:Lcom/google/android/voicesearch/ui/RecognizerView$State;

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/ui/RecognizerView;->updateState(Lcom/google/android/voicesearch/ui/RecognizerView$State;)V

    return-void
.end method

.method public showRecognizing()V
    .locals 1

    sget-object v0, Lcom/google/android/voicesearch/ui/RecognizerView$State;->RECOGNIZING:Lcom/google/android/voicesearch/ui/RecognizerView$State;

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/ui/RecognizerView;->updateState(Lcom/google/android/voicesearch/ui/RecognizerView$State;)V

    return-void
.end method

.method public showRecording()V
    .locals 1

    sget-object v0, Lcom/google/android/voicesearch/ui/RecognizerView$State;->RECORDING:Lcom/google/android/voicesearch/ui/RecognizerView$State;

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/ui/RecognizerView;->updateState(Lcom/google/android/voicesearch/ui/RecognizerView$State;)V

    return-void
.end method
