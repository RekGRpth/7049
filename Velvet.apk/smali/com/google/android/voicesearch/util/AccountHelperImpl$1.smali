.class Lcom/google/android/voicesearch/util/AccountHelperImpl$1;
.super Landroid/os/AsyncTask;
.source "AccountHelperImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/util/AccountHelperImpl;->promptForPermissions(Landroid/app/Activity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field mAccounts:[Landroid/accounts/Account;

.field final synthetic this$0:Lcom/google/android/voicesearch/util/AccountHelperImpl;

.field final synthetic val$activity:Landroid/app/Activity;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/util/AccountHelperImpl;Landroid/app/Activity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/util/AccountHelperImpl$1;->this$0:Lcom/google/android/voicesearch/util/AccountHelperImpl;

    iput-object p2, p0, Lcom/google/android/voicesearch/util/AccountHelperImpl$1;->val$activity:Landroid/app/Activity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/util/AccountHelperImpl$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 1
    .param p1    # [Ljava/lang/Void;

    iget-object v0, p0, Lcom/google/android/voicesearch/util/AccountHelperImpl$1;->this$0:Lcom/google/android/voicesearch/util/AccountHelperImpl;

    # invokes: Lcom/google/android/voicesearch/util/AccountHelperImpl;->getGoogleAccounts()[Landroid/accounts/Account;
    invoke-static {v0}, Lcom/google/android/voicesearch/util/AccountHelperImpl;->access$000(Lcom/google/android/voicesearch/util/AccountHelperImpl;)[Landroid/accounts/Account;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/util/AccountHelperImpl$1;->mAccounts:[Landroid/accounts/Account;

    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/util/AccountHelperImpl$1;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 10
    .param p1    # Ljava/lang/Void;

    const/4 v3, 0x0

    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/util/AccountHelperImpl$1;->mAccounts:[Landroid/accounts/Account;

    if-eqz v0, :cond_0

    iget-object v7, p0, Lcom/google/android/voicesearch/util/AccountHelperImpl$1;->mAccounts:[Landroid/accounts/Account;

    array-length v9, v7

    const/4 v8, 0x0

    :goto_0
    if-ge v8, v9, :cond_0

    aget-object v1, v7, v8

    iget-object v0, p0, Lcom/google/android/voicesearch/util/AccountHelperImpl$1;->this$0:Lcom/google/android/voicesearch/util/AccountHelperImpl;

    # getter for: Lcom/google/android/voicesearch/util/AccountHelperImpl;->mAccountManager:Landroid/accounts/AccountManager;
    invoke-static {v0}, Lcom/google/android/voicesearch/util/AccountHelperImpl;->access$100(Lcom/google/android/voicesearch/util/AccountHelperImpl;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v2, "speech"

    iget-object v4, p0, Lcom/google/android/voicesearch/util/AccountHelperImpl$1;->val$activity:Landroid/app/Activity;

    move-object v5, v3

    move-object v6, v3

    invoke-virtual/range {v0 .. v6}, Landroid/accounts/AccountManager;->getAuthToken(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
