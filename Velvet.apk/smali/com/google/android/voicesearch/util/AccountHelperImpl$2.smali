.class Lcom/google/android/voicesearch/util/AccountHelperImpl$2;
.super Ljava/lang/Object;
.source "AccountHelperImpl.java"

# interfaces
.implements Lcom/google/common/base/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/util/AccountHelperImpl;->getAuthTokenFuture(Ljava/util/concurrent/Future;)Ljava/util/concurrent/Future;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Function",
        "<",
        "Landroid/os/Bundle;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/util/AccountHelperImpl;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/util/AccountHelperImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/util/AccountHelperImpl$2;->this$0:Lcom/google/android/voicesearch/util/AccountHelperImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Landroid/os/Bundle;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/util/AccountHelperImpl$2;->apply(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public apply(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 3
    .param p1    # Landroid/os/Bundle;

    if-eqz p1, :cond_0

    const-string v1, "authtoken"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/util/AccountHelperImpl$2;->this$0:Lcom/google/android/voicesearch/util/AccountHelperImpl;

    # getter for: Lcom/google/android/voicesearch/util/AccountHelperImpl;->mAuthTokens:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/voicesearch/util/AccountHelperImpl;->access$200(Lcom/google/android/voicesearch/util/AccountHelperImpl;)Ljava/util/List;

    move-result-object v2

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/google/android/voicesearch/util/AccountHelperImpl$2;->this$0:Lcom/google/android/voicesearch/util/AccountHelperImpl;

    # getter for: Lcom/google/android/voicesearch/util/AccountHelperImpl;->mAuthTokens:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/voicesearch/util/AccountHelperImpl;->access$200(Lcom/google/android/voicesearch/util/AccountHelperImpl;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    monitor-exit v2

    :goto_0
    return-object v0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
