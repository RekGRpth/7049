.class public Lcom/google/android/voicesearch/util/AppSelectionHelper$App;
.super Ljava/lang/Object;
.source "AppSelectionHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/util/AppSelectionHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "App"
.end annotation


# instance fields
.field private mIcon:Landroid/graphics/drawable/Drawable;

.field private final mIntent:Landroid/content/Intent;

.field private final mLabel:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Intent;Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1    # Landroid/content/Intent;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/graphics/drawable/Drawable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/voicesearch/util/AppSelectionHelper$App;->mIntent:Landroid/content/Intent;

    iput-object p2, p0, Lcom/google/android/voicesearch/util/AppSelectionHelper$App;->mLabel:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/voicesearch/util/AppSelectionHelper$App;->mIcon:Landroid/graphics/drawable/Drawable;

    return-void
.end method


# virtual methods
.method public getIcon()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/util/AppSelectionHelper$App;->mIcon:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getLabel()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/util/AppSelectionHelper$App;->mLabel:Ljava/lang/String;

    return-object v0
.end method

.method public getLaunchIntent()Landroid/content/Intent;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/util/AppSelectionHelper$App;->mIntent:Landroid/content/Intent;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/util/AppSelectionHelper$App;->mIntent:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
