.class Lcom/google/android/voicesearch/util/CalendarHelper$1;
.super Ljava/lang/Object;
.source "CalendarHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/util/CalendarHelper;->queryEvents(JJILcom/google/android/speech/callback/SimpleCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/util/CalendarHelper;

.field final synthetic val$callback:Lcom/google/android/speech/callback/SimpleCallback;

.field final synthetic val$limit:I

.field final synthetic val$selectionArgs:[Ljava/lang/String;

.field final synthetic val$uri:Landroid/net/Uri;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/util/CalendarHelper;Landroid/net/Uri;[Ljava/lang/String;ILcom/google/android/speech/callback/SimpleCallback;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/util/CalendarHelper$1;->this$0:Lcom/google/android/voicesearch/util/CalendarHelper;

    iput-object p2, p0, Lcom/google/android/voicesearch/util/CalendarHelper$1;->val$uri:Landroid/net/Uri;

    iput-object p3, p0, Lcom/google/android/voicesearch/util/CalendarHelper$1;->val$selectionArgs:[Ljava/lang/String;

    iput p4, p0, Lcom/google/android/voicesearch/util/CalendarHelper$1;->val$limit:I

    iput-object p5, p0, Lcom/google/android/voicesearch/util/CalendarHelper$1;->val$callback:Lcom/google/android/speech/callback/SimpleCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    iget-object v0, p0, Lcom/google/android/voicesearch/util/CalendarHelper$1;->this$0:Lcom/google/android/voicesearch/util/CalendarHelper;

    # getter for: Lcom/google/android/voicesearch/util/CalendarHelper;->mContentResolver:Landroid/content/ContentResolver;
    invoke-static {v0}, Lcom/google/android/voicesearch/util/CalendarHelper;->access$300(Lcom/google/android/voicesearch/util/CalendarHelper;)Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/util/CalendarHelper$1;->val$uri:Landroid/net/Uri;

    # getter for: Lcom/google/android/voicesearch/util/CalendarHelper;->EVENT_QUERY_PROJECTION:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/voicesearch/util/CalendarHelper;->access$200()[Ljava/lang/String;

    move-result-object v2

    const-string v3, "(visible=1) AND (calendar_access_level=700) AND (allDay=0) AND (selfAttendeeStatus!=2) AND begin > ?"

    iget-object v4, p0, Lcom/google/android/voicesearch/util/CalendarHelper$1;->val$selectionArgs:[Ljava/lang/String;

    const-string v5, "begin ASC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v10

    :goto_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, Lcom/google/android/voicesearch/util/CalendarHelper$1;->val$limit:I

    if-ge v0, v1, :cond_0

    new-instance v0, Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;

    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x3

    invoke-interface {v9, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    const/4 v7, 0x4

    invoke-interface {v9, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    invoke-direct/range {v0 .. v8}, Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;-><init>(JLjava/lang/String;Ljava/lang/String;JJ)V

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/google/android/voicesearch/util/CalendarHelper$1;->this$0:Lcom/google/android/voicesearch/util/CalendarHelper;

    # getter for: Lcom/google/android/voicesearch/util/CalendarHelper;->mUiThreadExecutor:Ljava/util/concurrent/Executor;
    invoke-static {v0}, Lcom/google/android/voicesearch/util/CalendarHelper;->access$100(Lcom/google/android/voicesearch/util/CalendarHelper;)Ljava/util/concurrent/Executor;

    move-result-object v0

    new-instance v1, Lcom/google/android/voicesearch/util/CalendarHelper$1$1;

    invoke-direct {v1, p0, v10}, Lcom/google/android/voicesearch/util/CalendarHelper$1$1;-><init>(Lcom/google/android/voicesearch/util/CalendarHelper$1;Ljava/util/List;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
