.class Lcom/google/android/voicesearch/util/CalendarHelper$2;
.super Ljava/lang/Object;
.source "CalendarHelper.java"

# interfaces
.implements Lcom/google/android/speech/callback/SimpleCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/util/CalendarHelper;->addEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/util/List;Ljava/util/List;Lcom/google/android/speech/callback/SimpleCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/speech/callback/SimpleCallback",
        "<",
        "Landroid/accounts/Account;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/util/CalendarHelper;

.field final synthetic val$attendees:Ljava/util/List;

.field final synthetic val$calendar:Ljava/lang/String;

.field final synthetic val$callback:Lcom/google/android/speech/callback/SimpleCallback;

.field final synthetic val$endTimeMs:J

.field final synthetic val$location:Ljava/lang/String;

.field final synthetic val$reminders:Ljava/util/List;

.field final synthetic val$startTimeMs:J

.field final synthetic val$summary:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/util/CalendarHelper;Lcom/google/android/speech/callback/SimpleCallback;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/util/List;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/util/CalendarHelper$2;->this$0:Lcom/google/android/voicesearch/util/CalendarHelper;

    iput-object p2, p0, Lcom/google/android/voicesearch/util/CalendarHelper$2;->val$callback:Lcom/google/android/speech/callback/SimpleCallback;

    iput-object p3, p0, Lcom/google/android/voicesearch/util/CalendarHelper$2;->val$calendar:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/voicesearch/util/CalendarHelper$2;->val$summary:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/voicesearch/util/CalendarHelper$2;->val$location:Ljava/lang/String;

    iput-wide p6, p0, Lcom/google/android/voicesearch/util/CalendarHelper$2;->val$startTimeMs:J

    iput-wide p8, p0, Lcom/google/android/voicesearch/util/CalendarHelper$2;->val$endTimeMs:J

    iput-object p10, p0, Lcom/google/android/voicesearch/util/CalendarHelper$2;->val$attendees:Ljava/util/List;

    iput-object p11, p0, Lcom/google/android/voicesearch/util/CalendarHelper$2;->val$reminders:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResult(Landroid/accounts/Account;)V
    .locals 14
    .param p1    # Landroid/accounts/Account;

    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/util/CalendarHelper$2;->val$callback:Lcom/google/android/speech/callback/SimpleCallback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/util/CalendarHelper$2;->val$callback:Lcom/google/android/speech/callback/SimpleCallback;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/speech/callback/SimpleCallback;->onResult(Ljava/lang/Object;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/voicesearch/util/CalendarHelper$2;->this$0:Lcom/google/android/voicesearch/util/CalendarHelper;

    # getter for: Lcom/google/android/voicesearch/util/CalendarHelper;->mBackgroundExecutor:Ljava/util/concurrent/Executor;
    invoke-static {v0}, Lcom/google/android/voicesearch/util/CalendarHelper;->access$400(Lcom/google/android/voicesearch/util/CalendarHelper;)Ljava/util/concurrent/Executor;

    move-result-object v13

    new-instance v0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;

    iget-object v1, p0, Lcom/google/android/voicesearch/util/CalendarHelper$2;->this$0:Lcom/google/android/voicesearch/util/CalendarHelper;

    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/voicesearch/util/CalendarHelper$2;->val$calendar:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/voicesearch/util/CalendarHelper$2;->val$summary:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/voicesearch/util/CalendarHelper$2;->val$location:Ljava/lang/String;

    iget-wide v6, p0, Lcom/google/android/voicesearch/util/CalendarHelper$2;->val$startTimeMs:J

    iget-wide v8, p0, Lcom/google/android/voicesearch/util/CalendarHelper$2;->val$endTimeMs:J

    iget-object v10, p0, Lcom/google/android/voicesearch/util/CalendarHelper$2;->val$attendees:Ljava/util/List;

    iget-object v11, p0, Lcom/google/android/voicesearch/util/CalendarHelper$2;->val$reminders:Ljava/util/List;

    iget-object v12, p0, Lcom/google/android/voicesearch/util/CalendarHelper$2;->val$callback:Lcom/google/android/speech/callback/SimpleCallback;

    invoke-direct/range {v0 .. v12}, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;-><init>(Lcom/google/android/voicesearch/util/CalendarHelper;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/util/List;Ljava/util/List;Lcom/google/android/speech/callback/SimpleCallback;)V

    invoke-interface {v13, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public bridge synthetic onResult(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Landroid/accounts/Account;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/util/CalendarHelper$2;->onResult(Landroid/accounts/Account;)V

    return-void
.end method
