.class public Lcom/google/android/voicesearch/util/CalendarHelper$Attendee;
.super Ljava/lang/Object;
.source "CalendarHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/util/CalendarHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Attendee"
.end annotation


# instance fields
.field private final mEmailAddress:Ljava/lang/String;

.field private final mName:Ljava/lang/String;


# virtual methods
.method public getEmailAddress()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/util/CalendarHelper$Attendee;->mEmailAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/util/CalendarHelper$Attendee;->mName:Ljava/lang/String;

    return-object v0
.end method
