.class public Lcom/google/android/voicesearch/util/CalendarHelper;
.super Ljava/lang/Object;
.source "CalendarHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;,
        Lcom/google/android/voicesearch/util/CalendarHelper$Reminder;,
        Lcom/google/android/voicesearch/util/CalendarHelper$Attendee;,
        Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;
    }
.end annotation


# static fields
.field private static final EVENT_QUERY_PROJECTION:[Ljava/lang/String;

.field private static final PROJECTION:[Ljava/lang/String;


# instance fields
.field private final mAccountHelper:Lcom/google/android/speech/helper/AccountHelper;

.field private final mBackgroundExecutor:Ljava/util/concurrent/Executor;

.field private final mContentResolver:Landroid/content/ContentResolver;

.field private final mEnableAttendees:Z

.field private final mUiThreadExecutor:Ljava/util/concurrent/Executor;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/voicesearch/util/CalendarHelper;->PROJECTION:[Ljava/lang/String;

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "event_id"

    aput-object v1, v0, v2

    const-string v1, "title"

    aput-object v1, v0, v3

    const/4 v1, 0x2

    const-string v2, "eventLocation"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "begin"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "end"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/voicesearch/util/CalendarHelper;->EVENT_QUERY_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/speech/helper/AccountHelper;Landroid/content/ContentResolver;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Z)V
    .locals 0
    .param p1    # Lcom/google/android/speech/helper/AccountHelper;
    .param p2    # Landroid/content/ContentResolver;
    .param p3    # Ljava/util/concurrent/Executor;
    .param p4    # Ljava/util/concurrent/Executor;
    .param p5    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/voicesearch/util/CalendarHelper;->mAccountHelper:Lcom/google/android/speech/helper/AccountHelper;

    iput-object p2, p0, Lcom/google/android/voicesearch/util/CalendarHelper;->mContentResolver:Landroid/content/ContentResolver;

    iput-object p3, p0, Lcom/google/android/voicesearch/util/CalendarHelper;->mUiThreadExecutor:Ljava/util/concurrent/Executor;

    iput-object p4, p0, Lcom/google/android/voicesearch/util/CalendarHelper;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    iput-boolean p5, p0, Lcom/google/android/voicesearch/util/CalendarHelper;->mEnableAttendees:Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/util/CalendarHelper;Ljava/lang/String;Ljava/lang/String;)J
    .locals 2
    .param p0    # Lcom/google/android/voicesearch/util/CalendarHelper;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/util/CalendarHelper;->getCalendarId(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$100(Lcom/google/android/voicesearch/util/CalendarHelper;)Ljava/util/concurrent/Executor;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/util/CalendarHelper;

    iget-object v0, p0, Lcom/google/android/voicesearch/util/CalendarHelper;->mUiThreadExecutor:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method static synthetic access$200()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/voicesearch/util/CalendarHelper;->EVENT_QUERY_PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/voicesearch/util/CalendarHelper;)Landroid/content/ContentResolver;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/util/CalendarHelper;

    iget-object v0, p0, Lcom/google/android/voicesearch/util/CalendarHelper;->mContentResolver:Landroid/content/ContentResolver;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/voicesearch/util/CalendarHelper;)Ljava/util/concurrent/Executor;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/util/CalendarHelper;

    iget-object v0, p0, Lcom/google/android/voicesearch/util/CalendarHelper;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method private getCalendarId(Ljava/lang/String;Ljava/lang/String;)J
    .locals 11
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v10, 0x0

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    if-nez p2, :cond_0

    move-object p2, p1

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/util/CalendarHelper;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/voicesearch/util/CalendarHelper;->PROJECTION:[Ljava/lang/String;

    const-string v3, "((account_name = ?) AND (account_type = ?) AND (ownerAccount = ?))"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/String;

    aput-object p2, v4, v10

    const/4 v5, 0x1

    const-string v9, "com.google"

    aput-object v9, v4, v5

    const/4 v5, 0x2

    aput-object p1, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    const-wide/16 v6, -0x1

    :goto_0
    return-wide v6

    :cond_1
    invoke-interface {v8, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method private insertAttendees(JLjava/util/List;)V
    .locals 5
    .param p1    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/voicesearch/util/CalendarHelper$Attendee;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/util/CalendarHelper$Attendee;

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "attendeeName"

    invoke-virtual {v0}, Lcom/google/android/voicesearch/util/CalendarHelper$Attendee;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "attendeeEmail"

    invoke-virtual {v0}, Lcom/google/android/voicesearch/util/CalendarHelper$Attendee;->getEmailAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "attendeeRelationship"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "attendeeType"

    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "attendeeStatus"

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "event_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget-object v3, p0, Lcom/google/android/voicesearch/util/CalendarHelper;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v4, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto :goto_0

    :cond_0
    return-void
.end method

.method private insertReminders(JLjava/util/List;)V
    .locals 5
    .param p1    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/voicesearch/util/CalendarHelper$Reminder;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/voicesearch/util/CalendarHelper$Reminder;

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "event_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v3, "minutes"

    invoke-virtual {v1}, Lcom/google/android/voicesearch/util/CalendarHelper$Reminder;->getMinutesInAdvance()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "method"

    invoke-virtual {v1}, Lcom/google/android/voicesearch/util/CalendarHelper$Reminder;->getMethod()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v3, p0, Lcom/google/android/voicesearch/util/CalendarHelper;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v4, Landroid/provider/CalendarContract$Reminders;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public addEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/util/List;Ljava/util/List;Lcom/google/android/speech/callback/SimpleCallback;)V
    .locals 14
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # J
    .param p7    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "JJ",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/voicesearch/util/CalendarHelper$Attendee;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/voicesearch/util/CalendarHelper$Reminder;",
            ">;",
            "Lcom/google/android/speech/callback/SimpleCallback",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    iget-object v13, p0, Lcom/google/android/voicesearch/util/CalendarHelper;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    new-instance v0, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-wide/from16 v6, p5

    move-wide/from16 v8, p7

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    invoke-direct/range {v0 .. v12}, Lcom/google/android/voicesearch/util/CalendarHelper$EventAdder;-><init>(Lcom/google/android/voicesearch/util/CalendarHelper;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/util/List;Ljava/util/List;Lcom/google/android/speech/callback/SimpleCallback;)V

    invoke-interface {v13, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    :goto_0
    return-void

    :cond_0
    iget-object v12, p0, Lcom/google/android/voicesearch/util/CalendarHelper;->mAccountHelper:Lcom/google/android/speech/helper/AccountHelper;

    new-instance v0, Lcom/google/android/voicesearch/util/CalendarHelper$2;

    move-object v1, p0

    move-object/from16 v2, p11

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-wide/from16 v6, p5

    move-wide/from16 v8, p7

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/google/android/voicesearch/util/CalendarHelper$2;-><init>(Lcom/google/android/voicesearch/util/CalendarHelper;Lcom/google/android/speech/callback/SimpleCallback;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/util/List;Ljava/util/List;)V

    invoke-interface {v12, v0}, Lcom/google/android/speech/helper/AccountHelper;->getMainGmailAccount(Lcom/google/android/speech/callback/SimpleCallback;)V

    goto :goto_0
.end method

.method public createAddEventIntent(Ljava/lang/String;Ljava/lang/String;JJLjava/util/List;)Landroid/content/Intent;
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # J
    .param p5    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "JJ",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/voicesearch/util/CalendarHelper$Attendee;",
            ">;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    const-wide/16 v6, 0x0

    new-instance v4, Landroid/content/Intent;

    const-string v5, "android.intent.action.INSERT"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v5, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v3

    if-eqz p1, :cond_0

    const-string v4, "title"

    invoke-virtual {v3, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    if-eqz p2, :cond_1

    const-string v4, "eventLocation"

    invoke-virtual {v3, v4, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_1
    cmp-long v4, p3, v6

    if-eqz v4, :cond_2

    const-string v4, "beginTime"

    invoke-virtual {v3, v4, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    :cond_2
    cmp-long v4, p5, v6

    if-eqz v4, :cond_3

    const-string v4, "endTime"

    invoke-virtual {v3, v4, p5, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    :cond_3
    iget-boolean v4, p0, Lcom/google/android/voicesearch/util/CalendarHelper;->mEnableAttendees:Z

    if-eqz v4, :cond_6

    if-eqz p7, :cond_6

    invoke-interface {p7}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_6

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/util/CalendarHelper$Attendee;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_4

    const/16 v4, 0x2c

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_4
    invoke-virtual {v0}, Lcom/google/android/voicesearch/util/CalendarHelper$Attendee;->getEmailAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_5
    const-string v4, "android.intent.extra.EMAIL"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_6
    return-object v3
.end method

.method public createViewCalendarIntent(J)Landroid/content/Intent;
    .locals 3
    .param p1    # J

    sget-object v1, Landroid/provider/CalendarContract;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "time"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    return-object v1
.end method

.method public createViewEventIntent(J)Landroid/content/Intent;
    .locals 3
    .param p1    # J

    sget-object v1, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v1, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    return-object v1
.end method

.method public getDefaultEndTimeMs(J)J
    .locals 3
    .param p1    # J

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-nez v2, :cond_0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/32 v0, 0x36ee80

    add-long/2addr v0, p1

    goto :goto_0
.end method

.method public getDefaultStartTimeMs(J)J
    .locals 3
    .param p1    # J

    const/4 v2, 0x0

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/16 v1, 0xc

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    const/16 v1, 0xd

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    const/16 v1, 0xe

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    const/16 v1, 0xa

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    return-wide v1
.end method

.method insertEvent(JLjava/lang/String;Ljava/lang/String;JJLjava/util/List;Ljava/util/List;)V
    .locals 7
    .param p1    # J
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # J
    .param p7    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "JJ",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/voicesearch/util/CalendarHelper$Attendee;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/voicesearch/util/CalendarHelper$Reminder;",
            ">;)V"
        }
    .end annotation

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkNotMainThread()V

    invoke-static {p3}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-wide/16 v5, 0x0

    cmp-long v5, p5, v5

    if-eqz v5, :cond_2

    const/4 v5, 0x1

    :goto_0
    invoke-static {v5}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    const-wide/16 v5, 0x0

    cmp-long v5, p7, v5

    if-eqz v5, :cond_3

    const/4 v5, 0x1

    :goto_1
    invoke-static {v5}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    const-string v5, "calendar_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v5, "dtstart"

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v5, "dtend"

    invoke-static {p7, p8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v5, "title"

    invoke-virtual {v4, v5, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p4, :cond_0

    const-string v5, "eventLocation"

    invoke-virtual {v4, v5, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string v5, "eventTimezone"

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/voicesearch/util/CalendarHelper;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v6, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5, v6, v4}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    iget-boolean v5, p0, Lcom/google/android/voicesearch/util/CalendarHelper;->mEnableAttendees:Z

    if-eqz v5, :cond_1

    move-object/from16 v0, p9

    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/voicesearch/util/CalendarHelper;->insertAttendees(JLjava/util/List;)V

    :cond_1
    move-object/from16 v0, p10

    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/voicesearch/util/CalendarHelper;->insertReminders(JLjava/util/List;)V

    return-void

    :cond_2
    const/4 v5, 0x0

    goto :goto_0

    :cond_3
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public queryEvents(JJILcom/google/android/speech/callback/SimpleCallback;)V
    .locals 10
    .param p1    # J
    .param p3    # J
    .param p5    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJI",
            "Lcom/google/android/speech/callback/SimpleCallback",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;",
            ">;>;)V"
        }
    .end annotation

    sget-object v0, Landroid/provider/CalendarContract$Instances;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v8

    invoke-static {v8, p1, p2}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    invoke-static {v8, p3, p4}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    invoke-virtual {v8}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    const-string v6, "(visible=1) AND (calendar_access_level=700) AND (allDay=0) AND (selfAttendeeStatus!=2) AND begin > ?"

    const/4 v0, 0x1

    new-array v3, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v0

    const-string v7, "begin ASC"

    iget-object v9, p0, Lcom/google/android/voicesearch/util/CalendarHelper;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    new-instance v0, Lcom/google/android/voicesearch/util/CalendarHelper$1;

    move-object v1, p0

    move v4, p5

    move-object/from16 v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/voicesearch/util/CalendarHelper$1;-><init>(Lcom/google/android/voicesearch/util/CalendarHelper;Landroid/net/Uri;[Ljava/lang/String;ILcom/google/android/speech/callback/SimpleCallback;)V

    invoke-interface {v9, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
