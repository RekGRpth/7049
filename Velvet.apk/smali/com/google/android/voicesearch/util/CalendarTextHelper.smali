.class public Lcom/google/android/voicesearch/util/CalendarTextHelper;
.super Ljava/lang/Object;
.source "CalendarTextHelper.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mResources:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/util/CalendarTextHelper;->mResources:Landroid/content/res/Resources;

    iput-object p1, p0, Lcom/google/android/voicesearch/util/CalendarTextHelper;->mContext:Landroid/content/Context;

    return-void
.end method

.method private formatSymbolicTime(JLcom/google/android/voicesearch/fragments/reminders/SymbolicTime;Z)Ljava/lang/String;
    .locals 6
    .param p1    # J
    .param p3    # Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;
    .param p4    # Z

    const/16 v0, 0xa00

    const/4 v2, 0x0

    if-eqz p4, :cond_1

    or-int/lit8 v0, v0, 0x2

    :goto_0
    if-nez p3, :cond_2

    or-int/lit8 v0, v0, 0x1

    :cond_0
    :goto_1
    iget-object v3, p0, Lcom/google/android/voicesearch/util/CalendarTextHelper;->mContext:Landroid/content/Context;

    invoke-static {v3, p1, p2, v0}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v1

    if-nez v2, :cond_4

    :goto_2
    return-object v1

    :cond_1
    or-int/lit8 v0, v0, 0x10

    goto :goto_0

    :cond_2
    sget-object v3, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->TIME_UNSPECIFIED:Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    if-eq p3, v3, :cond_0

    if-eqz p4, :cond_3

    iget v2, p3, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->sameWeekResId:I

    :goto_3
    goto :goto_1

    :cond_3
    iget v2, p3, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->defaultResId:I

    goto :goto_3

    :cond_4
    iget-object v3, p0, Lcom/google/android/voicesearch/util/CalendarTextHelper;->mResources:Landroid/content/res/Resources;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-virtual {v3, v2, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2
.end method

.method private formatSymbolicTimeToday(JLcom/google/android/voicesearch/fragments/reminders/SymbolicTime;)Ljava/lang/String;
    .locals 2
    .param p1    # J
    .param p3    # Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    if-nez p3, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/util/CalendarTextHelper;->mContext:Landroid/content/Context;

    const/16 v1, 0xa01

    invoke-static {v0, p1, p2, v1}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->TIME_UNSPECIFIED:Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    if-ne p3, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/util/CalendarTextHelper;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0d03fd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/voicesearch/util/CalendarTextHelper;->mResources:Landroid/content/res/Resources;

    iget v1, p3, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->sameDayResId:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private formatSymbolicTimeTomorrow(JLcom/google/android/voicesearch/fragments/reminders/SymbolicTime;)Ljava/lang/String;
    .locals 6
    .param p1    # J
    .param p3    # Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    const v4, 0x7f0d03fe

    const/4 v2, 0x1

    const/4 v5, 0x0

    if-nez p3, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/util/CalendarTextHelper;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0d03ff

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/voicesearch/util/CalendarTextHelper;->mContext:Landroid/content/Context;

    const/16 v4, 0xa01

    invoke-static {v3, p1, p2, v4}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->TIME_UNSPECIFIED:Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    if-ne p3, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/util/CalendarTextHelper;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/voicesearch/util/CalendarTextHelper;->mResources:Landroid/content/res/Resources;

    iget v1, p3, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->sameWeekResId:I

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/voicesearch/util/CalendarTextHelper;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static isSameDay(JJ)Z
    .locals 6
    .param p0    # J
    .param p2    # J

    const/4 v5, 0x6

    const/4 v2, 0x1

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v4

    if-ne v3, v4, :cond_0

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v1, v5}, Ljava/util/Calendar;->get(I)I

    move-result v4

    if-ne v3, v4, :cond_0

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isToday(J)Z
    .locals 2
    .param p0    # J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1, p0, p1}, Lcom/google/android/voicesearch/util/CalendarTextHelper;->isSameDay(JJ)Z

    move-result v0

    return v0
.end method

.method public static isTomorrow(J)Z
    .locals 4
    .param p0    # J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/32 v2, 0x5265c00

    add-long/2addr v0, v2

    invoke-static {v0, v1, p0, p1}, Lcom/google/android/voicesearch/util/CalendarTextHelper;->isSameDay(JJ)Z

    move-result v0

    return v0
.end method

.method public static isWithinOneWeek(JJ)Z
    .locals 4
    .param p0    # J
    .param p2    # J

    const-wide/32 v2, 0x240c8400

    add-long v0, p0, v2

    cmp-long v2, p0, p2

    if-gtz v2, :cond_0

    cmp-long v2, p2, v0

    if-gtz v2, :cond_0

    invoke-static {v0, v1, p2, p3}, Lcom/google/android/voicesearch/util/CalendarTextHelper;->isSameDay(JJ)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public createCalendarQueryTts(Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;JJ)Ljava/lang/String;
    .locals 8
    .param p1    # Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;
    .param p2    # J
    .param p4    # J

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual {p1}, Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;->getSummary()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v2, v4

    :goto_0
    sub-long v0, p4, p2

    const-wide/32 v6, 0x36ee80

    cmp-long v0, v0, v6

    if-lez v0, :cond_1

    move v3, v4

    :goto_1
    if-eqz v3, :cond_2

    invoke-virtual {p1}, Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;->getStartTimeMs()J

    move-result-wide v0

    invoke-static {v0, v1}, Landroid/text/format/DateUtils;->isToday(J)Z

    move-result v0

    if-nez v0, :cond_2

    sub-long v0, p4, p2

    const-wide/32 v6, 0x5265c00

    cmp-long v0, v0, v6

    if-lez v0, :cond_2

    :goto_2
    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/voicesearch/util/CalendarTextHelper;->createCalendarQueryTts(Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;ZZZZ)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    move v2, v5

    goto :goto_0

    :cond_1
    move v3, v5

    goto :goto_1

    :cond_2
    move v4, v5

    goto :goto_2
.end method

.method createCalendarQueryTts(Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;ZZZZ)Ljava/lang/String;
    .locals 12
    .param p1    # Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;
    .param p2    # Z
    .param p3    # Z
    .param p4    # Z
    .param p5    # Z

    if-eqz p5, :cond_1

    if-nez p3, :cond_1

    if-nez p4, :cond_1

    if-nez p2, :cond_1

    invoke-virtual {p1}, Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;->getLocation()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/voicesearch/util/CalendarTextHelper;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f0d0419

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/voicesearch/util/CalendarTextHelper;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f0d0413

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v7, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    if-eqz p3, :cond_2

    if-nez p2, :cond_2

    if-nez p4, :cond_2

    if-nez p5, :cond_2

    iget-object v8, p0, Lcom/google/android/voicesearch/util/CalendarTextHelper;->mResources:Landroid/content/res/Resources;

    const v9, 0x7f0d0415

    const/4 v1, 0x1

    new-array v10, v1, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-virtual {p1}, Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;->getStartTimeMs()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;->getStartTimeMs()J

    move-result-wide v3

    const/4 v5, 0x3

    const/4 v6, 0x3

    invoke-static/range {v1 .. v6}, Landroid/text/format/DateUtils;->formatSameDayTime(JJII)Ljava/lang/CharSequence;

    move-result-object v1

    aput-object v1, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_2
    if-eqz p3, :cond_3

    if-eqz p4, :cond_3

    if-nez p2, :cond_3

    if-nez p5, :cond_3

    iget-object v1, p0, Lcom/google/android/voicesearch/util/CalendarTextHelper;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f0d0417

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/voicesearch/util/CalendarTextHelper;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;->getStartTimeMs()J

    move-result-wide v8

    const/4 v6, 0x0

    invoke-static {v5, v8, v9, v6}, Lcom/google/android/apps/sidekick/TimeUtilities;->formatDisplayTime(Landroid/content/Context;JI)Ljava/lang/CharSequence;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_3
    if-eqz p2, :cond_4

    if-nez p3, :cond_4

    if-nez p4, :cond_4

    if-nez p5, :cond_4

    iget-object v1, p0, Lcom/google/android/voicesearch/util/CalendarTextHelper;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f0d0412

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;->getSummary()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_4
    if-eqz p5, :cond_6

    if-eqz p2, :cond_6

    if-nez p3, :cond_6

    if-nez p4, :cond_6

    invoke-virtual {p1}, Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;->getLocation()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/voicesearch/util/CalendarTextHelper;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f0d041a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    :cond_5
    iget-object v1, p0, Lcom/google/android/voicesearch/util/CalendarTextHelper;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f0d0414

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;->getSummary()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v7, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    :cond_6
    if-eqz p3, :cond_7

    if-eqz p2, :cond_7

    if-nez p4, :cond_7

    if-nez p5, :cond_7

    iget-object v8, p0, Lcom/google/android/voicesearch/util/CalendarTextHelper;->mResources:Landroid/content/res/Resources;

    const v9, 0x7f0d0416

    const/4 v1, 0x2

    new-array v10, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;->getSummary()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v10, v1

    const/4 v11, 0x1

    invoke-virtual {p1}, Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;->getStartTimeMs()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;->getStartTimeMs()J

    move-result-wide v3

    const/4 v5, 0x3

    const/4 v6, 0x3

    invoke-static/range {v1 .. v6}, Landroid/text/format/DateUtils;->formatSameDayTime(JJII)Ljava/lang/CharSequence;

    move-result-object v1

    aput-object v1, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    :cond_7
    if-eqz p3, :cond_8

    if-eqz p4, :cond_8

    if-eqz p2, :cond_8

    if-nez p5, :cond_8

    iget-object v1, p0, Lcom/google/android/voicesearch/util/CalendarTextHelper;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f0d0418

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;->getSummary()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/android/voicesearch/util/CalendarTextHelper;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;->getStartTimeMs()J

    move-result-wide v8

    const/4 v6, 0x0

    invoke-static {v5, v8, v9, v6}, Lcom/google/android/apps/sidekick/TimeUtilities;->formatDisplayTime(Landroid/content/Context;JI)Ljava/lang/CharSequence;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    :cond_8
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Can\'t answer include location: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p5

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "include summary: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "include date: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p4

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "include time: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public formatDisplayTime(JJZ)Ljava/lang/String;
    .locals 8
    .param p1    # J
    .param p3    # J
    .param p5    # Z

    const/16 v5, 0xa00

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7, p1, p2}, Lcom/google/android/voicesearch/util/CalendarTextHelper;->isSameDay(JJ)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v6, v7, p3, p4}, Lcom/google/android/voicesearch/util/CalendarTextHelper;->isSameDay(JJ)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-static {v6, v7, p1, p2}, Lcom/google/android/voicesearch/util/CalendarTextHelper;->isWithinOneWeek(JJ)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {v6, v7, p3, p4}, Lcom/google/android/voicesearch/util/CalendarTextHelper;->isWithinOneWeek(JJ)Z

    move-result v0

    if-eqz v0, :cond_3

    or-int/lit8 v5, v5, 0x2

    :cond_1
    :goto_0
    if-eqz p5, :cond_2

    or-int/lit8 v5, v5, 0x1

    :cond_2
    iget-object v0, p0, Lcom/google/android/voicesearch/util/CalendarTextHelper;->mContext:Landroid/content/Context;

    move-wide v1, p1

    move-wide v3, p3

    invoke-static/range {v0 .. v5}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_3
    or-int/lit8 v5, v5, 0x10

    goto :goto_0
.end method

.method public formatSymbolicTime(JLcom/google/android/voicesearch/fragments/reminders/SymbolicTime;)Ljava/lang/String;
    .locals 4
    .param p1    # J
    .param p3    # Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    sget-object v2, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->WEEKEND:Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    if-ne p3, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/voicesearch/util/CalendarTextHelper;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f0d040c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    :goto_0
    return-object v2

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1, p1, p2}, Lcom/google/android/voicesearch/util/CalendarTextHelper;->isSameDay(JJ)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/voicesearch/util/CalendarTextHelper;->formatSymbolicTimeToday(JLcom/google/android/voicesearch/fragments/reminders/SymbolicTime;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_1
    const-wide/32 v2, 0x5265c00

    add-long/2addr v2, v0

    invoke-static {v2, v3, p1, p2}, Lcom/google/android/voicesearch/util/CalendarTextHelper;->isSameDay(JJ)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/voicesearch/util/CalendarTextHelper;->formatSymbolicTimeTomorrow(JLcom/google/android/voicesearch/fragments/reminders/SymbolicTime;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_2
    invoke-static {v0, v1, p1, p2}, Lcom/google/android/voicesearch/util/CalendarTextHelper;->isWithinOneWeek(JJ)Z

    move-result v2

    invoke-direct {p0, p1, p2, p3, v2}, Lcom/google/android/voicesearch/util/CalendarTextHelper;->formatSymbolicTime(JLcom/google/android/voicesearch/fragments/reminders/SymbolicTime;Z)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method
