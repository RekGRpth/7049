.class Lcom/google/android/voicesearch/util/EmailSender$1;
.super Ljava/lang/Object;
.source "EmailSender.java"

# interfaces
.implements Lcom/google/android/speech/callback/SimpleCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/util/EmailSender;->sendEmail(Lcom/google/android/voicesearch/util/EmailSender$Email;ZLcom/google/android/searchcommon/util/IntentStarter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/speech/callback/SimpleCallback",
        "<",
        "Landroid/accounts/Account;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/util/EmailSender;

.field final synthetic val$autoSend:Z

.field final synthetic val$email:Lcom/google/android/voicesearch/util/EmailSender$Email;

.field final synthetic val$intentStarter:Lcom/google/android/searchcommon/util/IntentStarter;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/util/EmailSender;Lcom/google/android/voicesearch/util/EmailSender$Email;ZLcom/google/android/searchcommon/util/IntentStarter;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/util/EmailSender$1;->this$0:Lcom/google/android/voicesearch/util/EmailSender;

    iput-object p2, p0, Lcom/google/android/voicesearch/util/EmailSender$1;->val$email:Lcom/google/android/voicesearch/util/EmailSender$Email;

    iput-boolean p3, p0, Lcom/google/android/voicesearch/util/EmailSender$1;->val$autoSend:Z

    iput-object p4, p0, Lcom/google/android/voicesearch/util/EmailSender$1;->val$intentStarter:Lcom/google/android/searchcommon/util/IntentStarter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResult(Landroid/accounts/Account;)V
    .locals 6
    .param p1    # Landroid/accounts/Account;

    if-nez p1, :cond_0

    const/4 v3, 0x0

    :goto_0
    iget-object v0, p0, Lcom/google/android/voicesearch/util/EmailSender$1;->this$0:Lcom/google/android/voicesearch/util/EmailSender;

    iget-object v1, p0, Lcom/google/android/voicesearch/util/EmailSender$1;->val$email:Lcom/google/android/voicesearch/util/EmailSender$Email;

    iget-boolean v2, p0, Lcom/google/android/voicesearch/util/EmailSender$1;->val$autoSend:Z

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/voicesearch/util/EmailSender$1;->val$intentStarter:Lcom/google/android/searchcommon/util/IntentStarter;

    # invokes: Lcom/google/android/voicesearch/util/EmailSender;->sendEmail(Lcom/google/android/voicesearch/util/EmailSender$Email;ZLjava/lang/String;ZLcom/google/android/searchcommon/util/IntentStarter;)Z
    invoke-static/range {v0 .. v5}, Lcom/google/android/voicesearch/util/EmailSender;->access$000(Lcom/google/android/voicesearch/util/EmailSender;Lcom/google/android/voicesearch/util/EmailSender$Email;ZLjava/lang/String;ZLcom/google/android/searchcommon/util/IntentStarter;)Z

    return-void

    :cond_0
    iget-object v3, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    goto :goto_0
.end method

.method public bridge synthetic onResult(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Landroid/accounts/Account;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/util/EmailSender$1;->onResult(Landroid/accounts/Account;)V

    return-void
.end method
