.class Lcom/google/android/voicesearch/util/LocalTtsManager$3;
.super Ljava/lang/Object;
.source "LocalTtsManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/util/LocalTtsManager;->stop()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/util/LocalTtsManager;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/util/LocalTtsManager;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$3;->this$0:Lcom/google/android/voicesearch/util/LocalTtsManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$3;->this$0:Lcom/google/android/voicesearch/util/LocalTtsManager;

    # getter for: Lcom/google/android/voicesearch/util/LocalTtsManager;->mTextToSpeech:Landroid/speech/tts/TextToSpeech;
    invoke-static {v0}, Lcom/google/android/voicesearch/util/LocalTtsManager;->access$400(Lcom/google/android/voicesearch/util/LocalTtsManager;)Landroid/speech/tts/TextToSpeech;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$3;->this$0:Lcom/google/android/voicesearch/util/LocalTtsManager;

    # getter for: Lcom/google/android/voicesearch/util/LocalTtsManager;->mTextToSpeech:Landroid/speech/tts/TextToSpeech;
    invoke-static {v0}, Lcom/google/android/voicesearch/util/LocalTtsManager;->access$400(Lcom/google/android/voicesearch/util/LocalTtsManager;)Landroid/speech/tts/TextToSpeech;

    move-result-object v0

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->stop()I

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/util/LocalTtsManager$3;->this$0:Lcom/google/android/voicesearch/util/LocalTtsManager;

    # invokes: Lcom/google/android/voicesearch/util/LocalTtsManager;->restoreBluetoothScoVolume()V
    invoke-static {v0}, Lcom/google/android/voicesearch/util/LocalTtsManager;->access$500(Lcom/google/android/voicesearch/util/LocalTtsManager;)V

    return-void
.end method
