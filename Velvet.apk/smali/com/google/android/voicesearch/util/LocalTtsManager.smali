.class public Lcom/google/android/voicesearch/util/LocalTtsManager;
.super Ljava/lang/Object;
.source "LocalTtsManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/util/LocalTtsManager$EnqueuedUtterance;
    }
.end annotation


# instance fields
.field private mAudioManager:Landroid/media/AudioManager;

.field private final mCallbackLock:Ljava/lang/Object;

.field private final mCallbacksMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/voicesearch/util/LocalTtsManager$EnqueuedUtterance;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private final mMaxBluetoothScoVolumeSupplier:Lcom/google/common/base/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSavedBluetoothVolume:I

.field private mTextToSpeech:Landroid/speech/tts/TextToSpeech;

.field private final mTtsExecutor:Ljava/util/concurrent/Executor;

.field private final mUiThread:Ljava/util/concurrent/Executor;

.field private final mUtteranceProgressListener:Landroid/speech/tts/UtteranceProgressListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/voicesearch/settings/Settings;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/util/concurrent/Executor;
    .param p3    # Ljava/util/concurrent/Executor;
    .param p4    # Lcom/google/android/voicesearch/settings/Settings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/voicesearch/util/LocalTtsManager;->mCallbackLock:Ljava/lang/Object;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/voicesearch/util/LocalTtsManager;->mSavedBluetoothVolume:I

    new-instance v0, Lcom/google/android/voicesearch/util/LocalTtsManager$1;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/util/LocalTtsManager$1;-><init>(Lcom/google/android/voicesearch/util/LocalTtsManager;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/util/LocalTtsManager;->mUtteranceProgressListener:Landroid/speech/tts/UtteranceProgressListener;

    iput-object p1, p0, Lcom/google/android/voicesearch/util/LocalTtsManager;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/voicesearch/util/LocalTtsManager;->mUiThread:Ljava/util/concurrent/Executor;

    iput-object p3, p0, Lcom/google/android/voicesearch/util/LocalTtsManager;->mTtsExecutor:Ljava/util/concurrent/Executor;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/voicesearch/util/LocalTtsManager;->mCallbacksMap:Ljava/util/HashMap;

    invoke-static {p4}, Lcom/google/android/voicesearch/settings/SettingsUtils;->getMaxBluetoothScoVolumeSupplier(Lcom/google/android/voicesearch/settings/Settings;)Lcom/google/common/base/Supplier;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/util/LocalTtsManager;->mMaxBluetoothScoVolumeSupplier:Lcom/google/common/base/Supplier;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/util/LocalTtsManager;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/util/LocalTtsManager;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/util/LocalTtsManager;->onUtteranceCompleted(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/voicesearch/util/LocalTtsManager;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/util/LocalTtsManager;

    iget-object v0, p0, Lcom/google/android/voicesearch/util/LocalTtsManager;->mCallbackLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/voicesearch/util/LocalTtsManager;)Ljava/util/HashMap;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/util/LocalTtsManager;

    iget-object v0, p0, Lcom/google/android/voicesearch/util/LocalTtsManager;->mCallbacksMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/voicesearch/util/LocalTtsManager;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Runnable;I)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/util/LocalTtsManager;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # Ljava/lang/Runnable;
    .param p5    # I

    invoke-direct/range {p0 .. p5}, Lcom/google/android/voicesearch/util/LocalTtsManager;->internalEnqueue(Ljava/lang/String;Ljava/lang/String;ILjava/lang/Runnable;I)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/voicesearch/util/LocalTtsManager;)Landroid/speech/tts/TextToSpeech;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/util/LocalTtsManager;

    iget-object v0, p0, Lcom/google/android/voicesearch/util/LocalTtsManager;->mTextToSpeech:Landroid/speech/tts/TextToSpeech;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/voicesearch/util/LocalTtsManager;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/util/LocalTtsManager;

    invoke-direct {p0}, Lcom/google/android/voicesearch/util/LocalTtsManager;->restoreBluetoothScoVolume()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/voicesearch/util/LocalTtsManager;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/util/LocalTtsManager;

    invoke-direct {p0}, Lcom/google/android/voicesearch/util/LocalTtsManager;->maybeCreateTtsAndWaitForInit()V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/voicesearch/util/LocalTtsManager;)Ljava/util/concurrent/Executor;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/util/LocalTtsManager;

    iget-object v0, p0, Lcom/google/android/voicesearch/util/LocalTtsManager;->mUiThread:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method private adjustBluetoothScoVolume()V
    .locals 7

    const/4 v6, 0x6

    iget-object v4, p0, Lcom/google/android/voicesearch/util/LocalTtsManager;->mMaxBluetoothScoVolumeSupplier:Lcom/google/common/base/Supplier;

    invoke-interface {v4}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-gtz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v4, p0, Lcom/google/android/voicesearch/util/LocalTtsManager;->mAudioManager:Landroid/media/AudioManager;

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/google/android/voicesearch/util/LocalTtsManager;->mContext:Landroid/content/Context;

    const-string v5, "audio"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/media/AudioManager;

    iput-object v4, p0, Lcom/google/android/voicesearch/util/LocalTtsManager;->mAudioManager:Landroid/media/AudioManager;

    :cond_2
    iget-object v4, p0, Lcom/google/android/voicesearch/util/LocalTtsManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v4, v6}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    iget-object v4, p0, Lcom/google/android/voicesearch/util/LocalTtsManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v4, v6}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v2

    const/16 v4, 0x32

    div-int/2addr v4, v2

    div-int v4, v1, v4

    add-int/lit8 v4, v4, 0x1

    sub-int v3, v2, v4

    if-le v0, v3, :cond_3

    iget-object v4, p0, Lcom/google/android/voicesearch/util/LocalTtsManager;->mAudioManager:Landroid/media/AudioManager;

    const/4 v5, 0x0

    invoke-virtual {v4, v6, v3, v5}, Landroid/media/AudioManager;->setStreamVolume(III)V

    :cond_3
    iget v4, p0, Lcom/google/android/voicesearch/util/LocalTtsManager;->mSavedBluetoothVolume:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_0

    iput v0, p0, Lcom/google/android/voicesearch/util/LocalTtsManager;->mSavedBluetoothVolume:I

    goto :goto_0
.end method

.method private static createParamsWithUtteranceId(Ljava/lang/String;II)Ljava/util/HashMap;
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II)",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "utteranceId"

    invoke-virtual {v0, v1, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "streamType"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v1, 0x1

    if-eq p2, v1, :cond_0

    const/4 v1, 0x2

    if-ne p2, v1, :cond_1

    :cond_0
    const-string v1, "networkTts"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method private internalEnqueue(Ljava/lang/String;Ljava/lang/String;ILjava/lang/Runnable;I)V
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # Ljava/lang/Runnable;
    .param p5    # I

    invoke-direct {p0}, Lcom/google/android/voicesearch/util/LocalTtsManager;->maybeCreateTtsAndWaitForInit()V

    iget-object v0, p0, Lcom/google/android/voicesearch/util/LocalTtsManager;->mTextToSpeech:Landroid/speech/tts/TextToSpeech;

    if-nez v0, :cond_1

    invoke-direct {p0, p2}, Lcom/google/android/voicesearch/util/LocalTtsManager;->onUtteranceCompleted(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v7, p0, Lcom/google/android/voicesearch/util/LocalTtsManager;->mCallbackLock:Ljava/lang/Object;

    monitor-enter v7

    :try_start_0
    iget-object v8, p0, Lcom/google/android/voicesearch/util/LocalTtsManager;->mCallbacksMap:Ljava/util/HashMap;

    new-instance v0, Lcom/google/android/voicesearch/util/LocalTtsManager$EnqueuedUtterance;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/voicesearch/util/LocalTtsManager$EnqueuedUtterance;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/Runnable;I)V

    invoke-virtual {v8, p2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0}, Lcom/google/android/voicesearch/util/LocalTtsManager;->adjustBluetoothScoVolume()V

    iget-object v0, p0, Lcom/google/android/voicesearch/util/LocalTtsManager;->mTextToSpeech:Landroid/speech/tts/TextToSpeech;

    const/4 v1, 0x0

    invoke-static {p2, p3, p5}, Lcom/google/android/voicesearch/util/LocalTtsManager;->createParamsWithUtteranceId(Ljava/lang/String;II)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, Landroid/speech/tts/TextToSpeech;->speak(Ljava/lang/String;ILjava/util/HashMap;)I

    move-result v6

    if-eqz v6, :cond_0

    invoke-direct {p0, p2}, Lcom/google/android/voicesearch/util/LocalTtsManager;->onUtteranceCompleted(Ljava/lang/String;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private maybeCreateTtsAndWaitForInit()V
    .locals 5

    iget-object v3, p0, Lcom/google/android/voicesearch/util/LocalTtsManager;->mTextToSpeech:Landroid/speech/tts/TextToSpeech;

    if-eqz v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    new-instance v2, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    new-instance v1, Landroid/speech/tts/TextToSpeech;

    iget-object v3, p0, Lcom/google/android/voicesearch/util/LocalTtsManager;->mContext:Landroid/content/Context;

    new-instance v4, Lcom/google/android/voicesearch/util/LocalTtsManager$6;

    invoke-direct {v4, p0, v2, v0}, Lcom/google/android/voicesearch/util/LocalTtsManager$6;-><init>(Lcom/google/android/voicesearch/util/LocalTtsManager;Ljava/util/concurrent/atomic/AtomicBoolean;Landroid/os/ConditionVariable;)V

    invoke-direct {v1, v3, v4}, Landroid/speech/tts/TextToSpeech;-><init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;)V

    iget-object v3, p0, Lcom/google/android/voicesearch/util/LocalTtsManager;->mUtteranceProgressListener:Landroid/speech/tts/UtteranceProgressListener;

    invoke-virtual {v1, v3}, Landroid/speech/tts/TextToSpeech;->setOnUtteranceProgressListener(Landroid/speech/tts/UtteranceProgressListener;)I

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v3

    if-eqz v3, :cond_0

    iput-object v1, p0, Lcom/google/android/voicesearch/util/LocalTtsManager;->mTextToSpeech:Landroid/speech/tts/TextToSpeech;

    goto :goto_0
.end method

.method private onUtteranceCompleted(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/voicesearch/util/LocalTtsManager;->mTtsExecutor:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/google/android/voicesearch/util/LocalTtsManager$4;

    invoke-direct {v2, p0}, Lcom/google/android/voicesearch/util/LocalTtsManager$4;-><init>(Lcom/google/android/voicesearch/util/LocalTtsManager;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    iget-object v2, p0, Lcom/google/android/voicesearch/util/LocalTtsManager;->mCallbackLock:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/google/android/voicesearch/util/LocalTtsManager;->mCallbacksMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/util/LocalTtsManager$EnqueuedUtterance;

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/android/voicesearch/util/LocalTtsManager$EnqueuedUtterance;->completionCallback:Ljava/lang/Runnable;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/voicesearch/util/LocalTtsManager;->mUiThread:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/google/android/voicesearch/util/LocalTtsManager$5;

    invoke-direct {v2, p0, p1, v0}, Lcom/google/android/voicesearch/util/LocalTtsManager$5;-><init>(Lcom/google/android/voicesearch/util/LocalTtsManager;Ljava/lang/String;Lcom/google/android/voicesearch/util/LocalTtsManager$EnqueuedUtterance;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    :cond_0
    return-void

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private restoreBluetoothScoVolume()V
    .locals 5

    const/4 v4, -0x1

    iget v0, p0, Lcom/google/android/voicesearch/util/LocalTtsManager;->mSavedBluetoothVolume:I

    if-eq v0, v4, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/util/LocalTtsManager;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x6

    iget v2, p0, Lcom/google/android/voicesearch/util/LocalTtsManager;->mSavedBluetoothVolume:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    iput v4, p0, Lcom/google/android/voicesearch/util/LocalTtsManager;->mSavedBluetoothVolume:I

    :cond_0
    return-void
.end method


# virtual methods
.method public checkLocaleMatches(Ljava/util/Locale;ZLcom/google/android/speech/callback/SimpleCallback;)V
    .locals 2
    .param p1    # Ljava/util/Locale;
    .param p2    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Locale;",
            "Z",
            "Lcom/google/android/speech/callback/SimpleCallback",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/voicesearch/util/LocalTtsManager;->mTtsExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/voicesearch/util/LocalTtsManager$7;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/voicesearch/util/LocalTtsManager$7;-><init>(Lcom/google/android/voicesearch/util/LocalTtsManager;Ljava/util/Locale;ZLcom/google/android/speech/callback/SimpleCallback;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public enqueue(ILjava/lang/String;ILjava/lang/Runnable;)V
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/google/android/voicesearch/util/LocalTtsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3, p4}, Lcom/google/android/voicesearch/util/LocalTtsManager;->enqueue(Ljava/lang/String;Ljava/lang/String;ILjava/lang/Runnable;)V

    return-void
.end method

.method public enqueue(Ljava/lang/String;Ljava/lang/String;ILjava/lang/Runnable;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # Ljava/lang/Runnable;

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/voicesearch/util/LocalTtsManager;->enqueue(Ljava/lang/String;Ljava/lang/String;ILjava/lang/Runnable;I)V

    return-void
.end method

.method public enqueue(Ljava/lang/String;Ljava/lang/String;ILjava/lang/Runnable;I)V
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # Ljava/lang/Runnable;
    .param p5    # I

    if-nez p2, :cond_0

    if-nez p4, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    iget-object v7, p0, Lcom/google/android/voicesearch/util/LocalTtsManager;->mTtsExecutor:Ljava/util/concurrent/Executor;

    new-instance v0, Lcom/google/android/voicesearch/util/LocalTtsManager$2;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/voicesearch/util/LocalTtsManager$2;-><init>(Lcom/google/android/voicesearch/util/LocalTtsManager;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Runnable;I)V

    invoke-interface {v7, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method setTextToSpeechForTesting(Landroid/speech/tts/TextToSpeech;)V
    .locals 0
    .param p1    # Landroid/speech/tts/TextToSpeech;

    iput-object p1, p0, Lcom/google/android/voicesearch/util/LocalTtsManager;->mTextToSpeech:Landroid/speech/tts/TextToSpeech;

    return-void
.end method

.method public stop()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/voicesearch/util/LocalTtsManager;->mCallbackLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/voicesearch/util/LocalTtsManager;->mCallbacksMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/voicesearch/util/LocalTtsManager;->mTtsExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/voicesearch/util/LocalTtsManager$3;

    invoke-direct {v1, p0}, Lcom/google/android/voicesearch/util/LocalTtsManager$3;-><init>(Lcom/google/android/voicesearch/util/LocalTtsManager;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
