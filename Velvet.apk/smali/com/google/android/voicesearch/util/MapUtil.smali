.class public Lcom/google/android/voicesearch/util/MapUtil;
.super Ljava/lang/Object;
.source "MapUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static createMapsUriBuilder(ILcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;I)Landroid/net/Uri$Builder;
    .locals 8
    .param p0    # I
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;
    .param p2    # Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;
    .param p3    # I

    const/4 v7, 0x3

    new-instance v1, Landroid/net/Uri$Builder;

    invoke-direct {v1}, Landroid/net/Uri$Builder;-><init>()V

    const-string v2, "http"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "maps.google.com"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "sll"

    invoke-virtual {p2}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getLatDegrees()D

    move-result-wide v3

    invoke-virtual {p2}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getLngDegrees()D

    move-result-wide v5

    invoke-static {v3, v4, v5, v6}, Lcom/google/android/voicesearch/util/MapUtil;->getLatLngParameter(DD)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "q"

    invoke-virtual {p2}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "near"

    invoke-virtual {p2}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasClusterId()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "cid"

    invoke-virtual {p2}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getClusterId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_0
    const/4 v1, 0x2

    if-eq v1, p0, :cond_1

    if-ne v7, p0, :cond_3

    :cond_1
    if-eqz p1, :cond_2

    const-string v1, "saddr"

    invoke-virtual {p1}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getLatDegrees()D

    move-result-wide v2

    invoke-virtual {p1}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getLngDegrees()D

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/voicesearch/util/MapUtil;->getLatLngParameter(DD)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_2
    const-string v1, "daddr"

    invoke-virtual {p2}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string v1, "dirflg"

    invoke-static {p3}, Lcom/google/android/voicesearch/util/MapUtil;->dirflagFromTransportationMethod(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    if-ne v7, p0, :cond_3

    const-string v1, "nav"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_3
    return-object v0
.end method

.method private static dirflagFromTransportationMethod(I)Ljava/lang/String;
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    const-string v0, "d"

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "b"

    goto :goto_0

    :pswitch_1
    const-string v0, "w"

    goto :goto_0

    :pswitch_2
    const-string v0, "r"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static getIntentComponentName(I)Landroid/content/ComponentName;
    .locals 1
    .param p0    # I

    const/4 v0, 0x3

    if-ne v0, p0, :cond_0

    const-string v0, "com.google.android.apps.maps/com.google.android.maps.driveabout.app.NavigationActivity"

    invoke-static {v0}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "com.google.android.apps.maps/com.google.android.maps.MapsActivity"

    invoke-static {v0}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    goto :goto_0
.end method

.method private static getLatLngParameter(DD)Ljava/lang/String;
    .locals 2
    .param p0    # D
    .param p2    # D

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0, p1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getMapsIntent(ILcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;I)Landroid/content/Intent;
    .locals 3
    .param p0    # I
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;
    .param p2    # Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;
    .param p3    # I

    invoke-static {p0, p2, p3}, Lcom/google/android/voicesearch/util/MapUtil;->getUrlForAction(ILcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;I)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    :goto_0
    invoke-static {p0, v0}, Lcom/google/android/voicesearch/util/MapUtil;->getMapsIntentHelper(ILandroid/net/Uri$Builder;)Landroid/content/Intent;

    move-result-object v2

    return-object v2

    :cond_0
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/voicesearch/util/MapUtil;->createMapsUriBuilder(ILcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;I)Landroid/net/Uri$Builder;

    move-result-object v0

    goto :goto_0
.end method

.method public static getMapsIntent(Ljava/lang/String;)Landroid/content/Intent;
    .locals 2
    .param p0    # Ljava/lang/String;

    const/4 v0, 0x1

    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/voicesearch/util/MapUtil;->getMapsIntentHelper(ILandroid/net/Uri$Builder;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private static getMapsIntentHelper(ILandroid/net/Uri$Builder;)Landroid/content/Intent;
    .locals 2
    .param p0    # I
    .param p1    # Landroid/net/Uri$Builder;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/google/android/voicesearch/util/MapUtil;->getIntentComponentName(I)Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    return-object v0
.end method

.method public static getMapsSearchIntent(Ljava/lang/String;)Landroid/content/Intent;
    .locals 3
    .param p0    # Ljava/lang/String;

    const-string v1, "http://maps.google.com/"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "q"

    invoke-virtual {v1, v2, p0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "com.google.android.apps.maps/com.google.android.maps.MapsActivity"

    invoke-static {v2}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    return-object v1
.end method

.method private static getUrlForAction(ILcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;I)Ljava/lang/String;
    .locals 1
    .param p0    # I
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;
    .param p2    # I

    const/4 v0, 0x4

    if-ne v0, p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    packed-switch p2, :pswitch_data_0

    invoke-virtual {p1}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getActionDrivingUrl()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_0
    invoke-virtual {p1}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getActionTransitUrl()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-virtual {p1}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getActionBikingUrl()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    invoke-virtual {p1}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getActionWalkingUrl()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
