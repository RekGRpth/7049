.class public final Lcom/google/audio/ears/proto/EarsService$EarsResult;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "EarsService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/audio/ears/proto/EarsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EarsResult"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/audio/ears/proto/EarsService$EarsResult$MatchRange;
    }
.end annotation


# instance fields
.field private cachedSize:I

.field private confidence_:F

.field private country_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private debug_:Ljava/lang/String;

.field private elapsedMs_:J

.field private famousSpeechResult_:Lcom/google/audio/ears/proto/EarsService$FamousSpeechResult;

.field private hasConfidence:Z

.field private hasDebug:Z

.field private hasElapsedMs:Z

.field private hasFamousSpeechResult:Z

.field private hasMusicResult:Z

.field private hasProbeRange:Z

.field private hasRefRange:Z

.field private hasReferenceId:Z

.field private hasTtsResponse:Z

.field private hasTvResult:Z

.field private musicResult_:Lcom/google/audio/ears/proto/EarsService$MusicResult;

.field private probeRange_:Lcom/google/audio/ears/proto/EarsService$EarsResult$MatchRange;

.field private refRange_:Lcom/google/audio/ears/proto/EarsService$EarsResult$MatchRange;

.field private referenceId_:J

.field private ttsResponse_:Ljava/lang/String;

.field private tvResult_:Lcom/google/audio/ears/proto/EarsService$TvResult;


# direct methods
.method public constructor <init>()V
    .locals 4

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->confidence_:F

    const-string v0, ""

    iput-object v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->debug_:Ljava/lang/String;

    iput-wide v2, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->elapsedMs_:J

    iput-wide v2, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->referenceId_:J

    iput-object v1, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->probeRange_:Lcom/google/audio/ears/proto/EarsService$EarsResult$MatchRange;

    iput-object v1, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->refRange_:Lcom/google/audio/ears/proto/EarsService$EarsResult$MatchRange;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->country_:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->ttsResponse_:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->musicResult_:Lcom/google/audio/ears/proto/EarsService$MusicResult;

    iput-object v1, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->tvResult_:Lcom/google/audio/ears/proto/EarsService$TvResult;

    iput-object v1, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->famousSpeechResult_:Lcom/google/audio/ears/proto/EarsService$FamousSpeechResult;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addCountry(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$EarsResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->country_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->country_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->country_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->cachedSize:I

    return v0
.end method

.method public getConfidence()F
    .locals 1

    iget v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->confidence_:F

    return v0
.end method

.method public getCountryList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->country_:Ljava/util/List;

    return-object v0
.end method

.method public getDebug()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->debug_:Ljava/lang/String;

    return-object v0
.end method

.method public getElapsedMs()J
    .locals 2

    iget-wide v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->elapsedMs_:J

    return-wide v0
.end method

.method public getFamousSpeechResult()Lcom/google/audio/ears/proto/EarsService$FamousSpeechResult;
    .locals 1

    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->famousSpeechResult_:Lcom/google/audio/ears/proto/EarsService$FamousSpeechResult;

    return-object v0
.end method

.method public getMusicResult()Lcom/google/audio/ears/proto/EarsService$MusicResult;
    .locals 1

    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->musicResult_:Lcom/google/audio/ears/proto/EarsService$MusicResult;

    return-object v0
.end method

.method public getProbeRange()Lcom/google/audio/ears/proto/EarsService$EarsResult$MatchRange;
    .locals 1

    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->probeRange_:Lcom/google/audio/ears/proto/EarsService$EarsResult$MatchRange;

    return-object v0
.end method

.method public getRefRange()Lcom/google/audio/ears/proto/EarsService$EarsResult$MatchRange;
    .locals 1

    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->refRange_:Lcom/google/audio/ears/proto/EarsService$EarsResult$MatchRange;

    return-object v0
.end method

.method public getReferenceId()J
    .locals 2

    iget-wide v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->referenceId_:J

    return-wide v0
.end method

.method public getSerializedSize()I
    .locals 7

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->hasConfidence()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->getConfidence()F

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v4

    add-int/2addr v3, v4

    :cond_0
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->hasDebug()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->getDebug()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_1
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->hasMusicResult()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x3

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->getMusicResult()Lcom/google/audio/ears/proto/EarsService$MusicResult;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_2
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->hasTvResult()Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x4

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->getTvResult()Lcom/google/audio/ears/proto/EarsService$TvResult;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_3
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->hasElapsedMs()Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v4, 0x5

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->getElapsedMs()J

    move-result-wide v5

    invoke-static {v4, v5, v6}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_4
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->hasReferenceId()Z

    move-result v4

    if-eqz v4, :cond_5

    const/4 v4, 0x6

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->getReferenceId()J

    move-result-wide v5

    invoke-static {v4, v5, v6}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeUInt64Size(IJ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_5
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->hasProbeRange()Z

    move-result v4

    if-eqz v4, :cond_6

    const/4 v4, 0x7

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->getProbeRange()Lcom/google/audio/ears/proto/EarsService$EarsResult$MatchRange;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_6
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->hasRefRange()Z

    move-result v4

    if-eqz v4, :cond_7

    const/16 v4, 0x8

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->getRefRange()Lcom/google/audio/ears/proto/EarsService$EarsResult$MatchRange;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_7
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->hasFamousSpeechResult()Z

    move-result v4

    if-eqz v4, :cond_8

    const/16 v4, 0x9

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->getFamousSpeechResult()Lcom/google/audio/ears/proto/EarsService$FamousSpeechResult;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_8
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->getCountryList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_0

    :cond_9
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->getCountryList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->hasTtsResponse()Z

    move-result v4

    if-eqz v4, :cond_a

    const/16 v4, 0xb

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->getTtsResponse()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_a
    iput v3, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->cachedSize:I

    return v3
.end method

.method public getTtsResponse()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->ttsResponse_:Ljava/lang/String;

    return-object v0
.end method

.method public getTvResult()Lcom/google/audio/ears/proto/EarsService$TvResult;
    .locals 1

    iget-object v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->tvResult_:Lcom/google/audio/ears/proto/EarsService$TvResult;

    return-object v0
.end method

.method public hasConfidence()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->hasConfidence:Z

    return v0
.end method

.method public hasDebug()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->hasDebug:Z

    return v0
.end method

.method public hasElapsedMs()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->hasElapsedMs:Z

    return v0
.end method

.method public hasFamousSpeechResult()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->hasFamousSpeechResult:Z

    return v0
.end method

.method public hasMusicResult()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->hasMusicResult:Z

    return v0
.end method

.method public hasProbeRange()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->hasProbeRange:Z

    return v0
.end method

.method public hasRefRange()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->hasRefRange:Z

    return v0
.end method

.method public hasReferenceId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->hasReferenceId:Z

    return v0
.end method

.method public hasTtsResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->hasTtsResponse:Z

    return v0
.end method

.method public hasTvResult()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->hasTvResult:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/audio/ears/proto/EarsService$EarsResult;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->setConfidence(F)Lcom/google/audio/ears/proto/EarsService$EarsResult;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->setDebug(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$EarsResult;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/audio/ears/proto/EarsService$MusicResult;

    invoke-direct {v1}, Lcom/google/audio/ears/proto/EarsService$MusicResult;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->setMusicResult(Lcom/google/audio/ears/proto/EarsService$MusicResult;)Lcom/google/audio/ears/proto/EarsService$EarsResult;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/audio/ears/proto/EarsService$TvResult;

    invoke-direct {v1}, Lcom/google/audio/ears/proto/EarsService$TvResult;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->setTvResult(Lcom/google/audio/ears/proto/EarsService$TvResult;)Lcom/google/audio/ears/proto/EarsService$EarsResult;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->setElapsedMs(J)Lcom/google/audio/ears/proto/EarsService$EarsResult;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readUInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->setReferenceId(J)Lcom/google/audio/ears/proto/EarsService$EarsResult;

    goto :goto_0

    :sswitch_7
    new-instance v1, Lcom/google/audio/ears/proto/EarsService$EarsResult$MatchRange;

    invoke-direct {v1}, Lcom/google/audio/ears/proto/EarsService$EarsResult$MatchRange;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->setProbeRange(Lcom/google/audio/ears/proto/EarsService$EarsResult$MatchRange;)Lcom/google/audio/ears/proto/EarsService$EarsResult;

    goto :goto_0

    :sswitch_8
    new-instance v1, Lcom/google/audio/ears/proto/EarsService$EarsResult$MatchRange;

    invoke-direct {v1}, Lcom/google/audio/ears/proto/EarsService$EarsResult$MatchRange;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->setRefRange(Lcom/google/audio/ears/proto/EarsService$EarsResult$MatchRange;)Lcom/google/audio/ears/proto/EarsService$EarsResult;

    goto :goto_0

    :sswitch_9
    new-instance v1, Lcom/google/audio/ears/proto/EarsService$FamousSpeechResult;

    invoke-direct {v1}, Lcom/google/audio/ears/proto/EarsService$FamousSpeechResult;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->setFamousSpeechResult(Lcom/google/audio/ears/proto/EarsService$FamousSpeechResult;)Lcom/google/audio/ears/proto/EarsService$EarsResult;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->addCountry(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$EarsResult;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->setTtsResponse(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$EarsResult;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/audio/ears/proto/EarsService$EarsResult;

    move-result-object v0

    return-object v0
.end method

.method public setConfidence(F)Lcom/google/audio/ears/proto/EarsService$EarsResult;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->hasConfidence:Z

    iput p1, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->confidence_:F

    return-object p0
.end method

.method public setDebug(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$EarsResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->hasDebug:Z

    iput-object p1, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->debug_:Ljava/lang/String;

    return-object p0
.end method

.method public setElapsedMs(J)Lcom/google/audio/ears/proto/EarsService$EarsResult;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->hasElapsedMs:Z

    iput-wide p1, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->elapsedMs_:J

    return-object p0
.end method

.method public setFamousSpeechResult(Lcom/google/audio/ears/proto/EarsService$FamousSpeechResult;)Lcom/google/audio/ears/proto/EarsService$EarsResult;
    .locals 1
    .param p1    # Lcom/google/audio/ears/proto/EarsService$FamousSpeechResult;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->hasFamousSpeechResult:Z

    iput-object p1, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->famousSpeechResult_:Lcom/google/audio/ears/proto/EarsService$FamousSpeechResult;

    return-object p0
.end method

.method public setMusicResult(Lcom/google/audio/ears/proto/EarsService$MusicResult;)Lcom/google/audio/ears/proto/EarsService$EarsResult;
    .locals 1
    .param p1    # Lcom/google/audio/ears/proto/EarsService$MusicResult;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->hasMusicResult:Z

    iput-object p1, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->musicResult_:Lcom/google/audio/ears/proto/EarsService$MusicResult;

    return-object p0
.end method

.method public setProbeRange(Lcom/google/audio/ears/proto/EarsService$EarsResult$MatchRange;)Lcom/google/audio/ears/proto/EarsService$EarsResult;
    .locals 1
    .param p1    # Lcom/google/audio/ears/proto/EarsService$EarsResult$MatchRange;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->hasProbeRange:Z

    iput-object p1, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->probeRange_:Lcom/google/audio/ears/proto/EarsService$EarsResult$MatchRange;

    return-object p0
.end method

.method public setRefRange(Lcom/google/audio/ears/proto/EarsService$EarsResult$MatchRange;)Lcom/google/audio/ears/proto/EarsService$EarsResult;
    .locals 1
    .param p1    # Lcom/google/audio/ears/proto/EarsService$EarsResult$MatchRange;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->hasRefRange:Z

    iput-object p1, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->refRange_:Lcom/google/audio/ears/proto/EarsService$EarsResult$MatchRange;

    return-object p0
.end method

.method public setReferenceId(J)Lcom/google/audio/ears/proto/EarsService$EarsResult;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->hasReferenceId:Z

    iput-wide p1, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->referenceId_:J

    return-object p0
.end method

.method public setTtsResponse(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$EarsResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->hasTtsResponse:Z

    iput-object p1, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->ttsResponse_:Ljava/lang/String;

    return-object p0
.end method

.method public setTvResult(Lcom/google/audio/ears/proto/EarsService$TvResult;)Lcom/google/audio/ears/proto/EarsService$EarsResult;
    .locals 1
    .param p1    # Lcom/google/audio/ears/proto/EarsService$TvResult;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->hasTvResult:Z

    iput-object p1, p0, Lcom/google/audio/ears/proto/EarsService$EarsResult;->tvResult_:Lcom/google/audio/ears/proto/EarsService$TvResult;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 5
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->hasConfidence()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->getConfidence()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->hasDebug()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->getDebug()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->hasMusicResult()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->getMusicResult()Lcom/google/audio/ears/proto/EarsService$MusicResult;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->hasTvResult()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->getTvResult()Lcom/google/audio/ears/proto/EarsService$TvResult;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->hasElapsedMs()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->getElapsedMs()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->hasReferenceId()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->getReferenceId()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeUInt64(IJ)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->hasProbeRange()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->getProbeRange()Lcom/google/audio/ears/proto/EarsService$EarsResult$MatchRange;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->hasRefRange()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->getRefRange()Lcom/google/audio/ears/proto/EarsService$EarsResult$MatchRange;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->hasFamousSpeechResult()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->getFamousSpeechResult()Lcom/google/audio/ears/proto/EarsService$FamousSpeechResult;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->getCountryList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/16 v2, 0xa

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    goto :goto_0

    :cond_9
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->hasTtsResponse()Z

    move-result v2

    if-eqz v2, :cond_a

    const/16 v2, 0xb

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->getTtsResponse()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_a
    return-void
.end method
