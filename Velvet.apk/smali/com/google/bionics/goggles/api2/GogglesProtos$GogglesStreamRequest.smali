.class public final Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "GogglesProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/bionics/goggles/api2/GogglesProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GogglesStreamRequest"
.end annotation


# instance fields
.field private cachedSize:I

.field private hasImage:Z

.field private hasImportantPayload:Z

.field private hasPose:Z

.field private hasSequenceNumber:Z

.field private hasSessionOptions:Z

.field private hasText:Z

.field private image_:Lcom/google/bionics/goggles/api2/GogglesProtos$Image;

.field private importantPayload_:Z

.field private pose_:Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;

.field private sequenceNumber_:I

.field private sessionOptions_:Lcom/google/bionics/goggles/api2/GogglesProtos$SessionOptions;

.field private text_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput v1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->sequenceNumber_:I

    iput-object v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->sessionOptions_:Lcom/google/bionics/goggles/api2/GogglesProtos$SessionOptions;

    iput-object v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->image_:Lcom/google/bionics/goggles/api2/GogglesProtos$Image;

    iput-object v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->pose_:Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->text_:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->importantPayload_:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->cachedSize:I

    return v0
.end method

.method public getImage()Lcom/google/bionics/goggles/api2/GogglesProtos$Image;
    .locals 1

    iget-object v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->image_:Lcom/google/bionics/goggles/api2/GogglesProtos$Image;

    return-object v0
.end method

.method public getImportantPayload()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->importantPayload_:Z

    return v0
.end method

.method public getPose()Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;
    .locals 1

    iget-object v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->pose_:Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;

    return-object v0
.end method

.method public getSequenceNumber()I
    .locals 1

    iget v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->sequenceNumber_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->hasSequenceNumber()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->getSequenceNumber()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->hasSessionOptions()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->getSessionOptions()Lcom/google/bionics/goggles/api2/GogglesProtos$SessionOptions;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->hasImage()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->getImage()Lcom/google/bionics/goggles/api2/GogglesProtos$Image;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->hasPose()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->getPose()Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->hasText()Z

    move-result v1

    if-eqz v1, :cond_4

    const/16 v1, 0x10

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->hasImportantPayload()Z

    move-result v1

    if-eqz v1, :cond_5

    const/16 v1, 0x11

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->getImportantPayload()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iput v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->cachedSize:I

    return v0
.end method

.method public getSessionOptions()Lcom/google/bionics/goggles/api2/GogglesProtos$SessionOptions;
    .locals 1

    iget-object v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->sessionOptions_:Lcom/google/bionics/goggles/api2/GogglesProtos$SessionOptions;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->text_:Ljava/lang/String;

    return-object v0
.end method

.method public hasImage()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->hasImage:Z

    return v0
.end method

.method public hasImportantPayload()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->hasImportantPayload:Z

    return v0
.end method

.method public hasPose()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->hasPose:Z

    return v0
.end method

.method public hasSequenceNumber()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->hasSequenceNumber:Z

    return v0
.end method

.method public hasSessionOptions()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->hasSessionOptions:Z

    return v0
.end method

.method public hasText()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->hasText:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->setSequenceNumber(I)Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/bionics/goggles/api2/GogglesProtos$SessionOptions;

    invoke-direct {v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$SessionOptions;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->setSessionOptions(Lcom/google/bionics/goggles/api2/GogglesProtos$SessionOptions;)Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/bionics/goggles/api2/GogglesProtos$Image;

    invoke-direct {v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$Image;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->setImage(Lcom/google/bionics/goggles/api2/GogglesProtos$Image;)Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;

    invoke-direct {v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->setPose(Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;)Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->setText(Ljava/lang/String;)Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->setImportantPayload(Z)Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x82 -> :sswitch_5
        0x88 -> :sswitch_6
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;

    move-result-object v0

    return-object v0
.end method

.method public setImage(Lcom/google/bionics/goggles/api2/GogglesProtos$Image;)Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;
    .locals 1
    .param p1    # Lcom/google/bionics/goggles/api2/GogglesProtos$Image;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->hasImage:Z

    iput-object p1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->image_:Lcom/google/bionics/goggles/api2/GogglesProtos$Image;

    return-object p0
.end method

.method public setImportantPayload(Z)Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->hasImportantPayload:Z

    iput-boolean p1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->importantPayload_:Z

    return-object p0
.end method

.method public setPose(Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;)Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;
    .locals 1
    .param p1    # Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->hasPose:Z

    iput-object p1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->pose_:Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;

    return-object p0
.end method

.method public setSequenceNumber(I)Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->hasSequenceNumber:Z

    iput p1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->sequenceNumber_:I

    return-object p0
.end method

.method public setSessionOptions(Lcom/google/bionics/goggles/api2/GogglesProtos$SessionOptions;)Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;
    .locals 1
    .param p1    # Lcom/google/bionics/goggles/api2/GogglesProtos$SessionOptions;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->hasSessionOptions:Z

    iput-object p1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->sessionOptions_:Lcom/google/bionics/goggles/api2/GogglesProtos$SessionOptions;

    return-object p0
.end method

.method public setText(Ljava/lang/String;)Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->hasText:Z

    iput-object p1, p0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->text_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->hasSequenceNumber()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->getSequenceNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->hasSessionOptions()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->getSessionOptions()Lcom/google/bionics/goggles/api2/GogglesProtos$SessionOptions;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->hasImage()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->getImage()Lcom/google/bionics/goggles/api2/GogglesProtos$Image;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->hasPose()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->getPose()Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->hasText()Z

    move-result v0

    if-eqz v0, :cond_4

    const/16 v0, 0x10

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->hasImportantPayload()Z

    move-result v0

    if-eqz v0, :cond_5

    const/16 v0, 0x11

    invoke-virtual {p0}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->getImportantPayload()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_5
    return-void
.end method
