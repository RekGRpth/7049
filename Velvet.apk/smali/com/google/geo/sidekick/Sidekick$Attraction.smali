.class public final Lcom/google/geo/sidekick/Sidekick$Attraction;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Attraction"
.end annotation


# instance fields
.field private businessData_:Lcom/google/geo/sidekick/Sidekick$BusinessData;

.field private cachedSize:I

.field private hasBusinessData:Z

.field private hasRoute:Z

.field private route_:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Attraction;->businessData_:Lcom/google/geo/sidekick/Sidekick$BusinessData;

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Attraction;->route_:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$Attraction;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getBusinessData()Lcom/google/geo/sidekick/Sidekick$BusinessData;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Attraction;->businessData_:Lcom/google/geo/sidekick/Sidekick$BusinessData;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$Attraction;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Attraction;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$Attraction;->cachedSize:I

    return v0
.end method

.method public getRoute()Lcom/google/geo/sidekick/Sidekick$CommuteSummary;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Attraction;->route_:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Attraction;->hasBusinessData()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Attraction;->getBusinessData()Lcom/google/geo/sidekick/Sidekick$BusinessData;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Attraction;->hasRoute()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Attraction;->getRoute()Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$Attraction;->cachedSize:I

    return v0
.end method

.method public hasBusinessData()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Attraction;->hasBusinessData:Z

    return v0
.end method

.method public hasRoute()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Attraction;->hasRoute:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$Attraction;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$Attraction;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$BusinessData;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$BusinessData;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Attraction;->setBusinessData(Lcom/google/geo/sidekick/Sidekick$BusinessData;)Lcom/google/geo/sidekick/Sidekick$Attraction;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Attraction;->setRoute(Lcom/google/geo/sidekick/Sidekick$CommuteSummary;)Lcom/google/geo/sidekick/Sidekick$Attraction;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$Attraction;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$Attraction;

    move-result-object v0

    return-object v0
.end method

.method public setBusinessData(Lcom/google/geo/sidekick/Sidekick$BusinessData;)Lcom/google/geo/sidekick/Sidekick$Attraction;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$BusinessData;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Attraction;->hasBusinessData:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Attraction;->businessData_:Lcom/google/geo/sidekick/Sidekick$BusinessData;

    return-object p0
.end method

.method public setRoute(Lcom/google/geo/sidekick/Sidekick$CommuteSummary;)Lcom/google/geo/sidekick/Sidekick$Attraction;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Attraction;->hasRoute:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Attraction;->route_:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Attraction;->hasBusinessData()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Attraction;->getBusinessData()Lcom/google/geo/sidekick/Sidekick$BusinessData;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Attraction;->hasRoute()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Attraction;->getRoute()Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    return-void
.end method
