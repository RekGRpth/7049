.class public final Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FlightBoardingPass"
.end annotation


# instance fields
.field private additionalTicketText_:Ljava/lang/String;

.field private airlineCode_:Ljava/lang/String;

.field private airlineName_:Ljava/lang/String;

.field private airportCode_:Ljava/lang/String;

.field private boardingTime_:Ljava/lang/String;

.field private cachedSize:I

.field private flightNumber_:Ljava/lang/String;

.field private gate_:Ljava/lang/String;

.field private gmailReference_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$GmailReference;",
            ">;"
        }
    .end annotation
.end field

.field private group_:Ljava/lang/String;

.field private hasAdditionalTicketText:Z

.field private hasAirlineCode:Z

.field private hasAirlineName:Z

.field private hasAirportCode:Z

.field private hasBoardingTime:Z

.field private hasFlightNumber:Z

.field private hasGate:Z

.field private hasGroup:Z

.field private hasManageFlightUrl:Z

.field private hasPassengerName:Z

.field private hasSeat:Z

.field private hasTerminal:Z

.field private manageFlightUrl_:Ljava/lang/String;

.field private passengerName_:Ljava/lang/String;

.field private seat_:Ljava/lang/String;

.field private terminal_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->airlineName_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->airlineCode_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->flightNumber_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->airportCode_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->passengerName_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->terminal_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->gate_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->seat_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->group_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->boardingTime_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->gmailReference_:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->manageFlightUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->additionalTicketText_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addGmailReference(Lcom/google/geo/sidekick/Sidekick$GmailReference;)Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$GmailReference;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->gmailReference_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->gmailReference_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->gmailReference_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getAdditionalTicketText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->additionalTicketText_:Ljava/lang/String;

    return-object v0
.end method

.method public getAirlineCode()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->airlineCode_:Ljava/lang/String;

    return-object v0
.end method

.method public getAirlineName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->airlineName_:Ljava/lang/String;

    return-object v0
.end method

.method public getAirportCode()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->airportCode_:Ljava/lang/String;

    return-object v0
.end method

.method public getBoardingTime()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->boardingTime_:Ljava/lang/String;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->cachedSize:I

    return v0
.end method

.method public getFlightNumber()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->flightNumber_:Ljava/lang/String;

    return-object v0
.end method

.method public getGate()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->gate_:Ljava/lang/String;

    return-object v0
.end method

.method public getGmailReferenceList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$GmailReference;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->gmailReference_:Ljava/util/List;

    return-object v0
.end method

.method public getGroup()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->group_:Ljava/lang/String;

    return-object v0
.end method

.method public getManageFlightUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->manageFlightUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getPassengerName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->passengerName_:Ljava/lang/String;

    return-object v0
.end method

.method public getSeat()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->seat_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasAirlineName()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->getAirlineName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasFlightNumber()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->getFlightNumber()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasAirportCode()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->getAirportCode()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasPassengerName()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->getPassengerName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasTerminal()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->getTerminal()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasGate()Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->getGate()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasSeat()Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v3, 0x7

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->getSeat()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_6
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasGroup()Z

    move-result v3

    if-eqz v3, :cond_7

    const/16 v3, 0x8

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->getGroup()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_7
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasBoardingTime()Z

    move-result v3

    if-eqz v3, :cond_8

    const/16 v3, 0x9

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->getBoardingTime()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_8
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->getGmailReferenceList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$GmailReference;

    const/16 v3, 0xa

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_9
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasManageFlightUrl()Z

    move-result v3

    if-eqz v3, :cond_a

    const/16 v3, 0xb

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->getManageFlightUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_a
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasAirlineCode()Z

    move-result v3

    if-eqz v3, :cond_b

    const/16 v3, 0xc

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->getAirlineCode()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_b
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasAdditionalTicketText()Z

    move-result v3

    if-eqz v3, :cond_c

    const/16 v3, 0xd

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->getAdditionalTicketText()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_c
    iput v2, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->cachedSize:I

    return v2
.end method

.method public getTerminal()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->terminal_:Ljava/lang/String;

    return-object v0
.end method

.method public hasAdditionalTicketText()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasAdditionalTicketText:Z

    return v0
.end method

.method public hasAirlineCode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasAirlineCode:Z

    return v0
.end method

.method public hasAirlineName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasAirlineName:Z

    return v0
.end method

.method public hasAirportCode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasAirportCode:Z

    return v0
.end method

.method public hasBoardingTime()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasBoardingTime:Z

    return v0
.end method

.method public hasFlightNumber()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasFlightNumber:Z

    return v0
.end method

.method public hasGate()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasGate:Z

    return v0
.end method

.method public hasGroup()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasGroup:Z

    return v0
.end method

.method public hasManageFlightUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasManageFlightUrl:Z

    return v0
.end method

.method public hasPassengerName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasPassengerName:Z

    return v0
.end method

.method public hasSeat()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasSeat:Z

    return v0
.end method

.method public hasTerminal()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasTerminal:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->setAirlineName(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->setFlightNumber(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->setAirportCode(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->setPassengerName(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->setTerminal(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->setGate(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->setSeat(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->setGroup(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->setBoardingTime(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;

    goto :goto_0

    :sswitch_a
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$GmailReference;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$GmailReference;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->addGmailReference(Lcom/google/geo/sidekick/Sidekick$GmailReference;)Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->setManageFlightUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->setAirlineCode(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->setAdditionalTicketText(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;

    move-result-object v0

    return-object v0
.end method

.method public setAdditionalTicketText(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasAdditionalTicketText:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->additionalTicketText_:Ljava/lang/String;

    return-object p0
.end method

.method public setAirlineCode(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasAirlineCode:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->airlineCode_:Ljava/lang/String;

    return-object p0
.end method

.method public setAirlineName(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasAirlineName:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->airlineName_:Ljava/lang/String;

    return-object p0
.end method

.method public setAirportCode(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasAirportCode:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->airportCode_:Ljava/lang/String;

    return-object p0
.end method

.method public setBoardingTime(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasBoardingTime:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->boardingTime_:Ljava/lang/String;

    return-object p0
.end method

.method public setFlightNumber(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasFlightNumber:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->flightNumber_:Ljava/lang/String;

    return-object p0
.end method

.method public setGate(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasGate:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->gate_:Ljava/lang/String;

    return-object p0
.end method

.method public setGroup(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasGroup:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->group_:Ljava/lang/String;

    return-object p0
.end method

.method public setManageFlightUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasManageFlightUrl:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->manageFlightUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setPassengerName(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasPassengerName:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->passengerName_:Ljava/lang/String;

    return-object p0
.end method

.method public setSeat(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasSeat:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->seat_:Ljava/lang/String;

    return-object p0
.end method

.method public setTerminal(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasTerminal:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->terminal_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasAirlineName()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->getAirlineName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasFlightNumber()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->getFlightNumber()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasAirportCode()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->getAirportCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasPassengerName()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->getPassengerName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasTerminal()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->getTerminal()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasGate()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->getGate()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasSeat()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->getSeat()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasGroup()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->getGroup()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasBoardingTime()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->getBoardingTime()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->getGmailReferenceList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$GmailReference;

    const/16 v2, 0xa

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_9
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasManageFlightUrl()Z

    move-result v2

    if-eqz v2, :cond_a

    const/16 v2, 0xb

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->getManageFlightUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasAirlineCode()Z

    move-result v2

    if-eqz v2, :cond_b

    const/16 v2, 0xc

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->getAirlineCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_b
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->hasAdditionalTicketText()Z

    move-result v2

    if-eqz v2, :cond_c

    const/16 v2, 0xd

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;->getAdditionalTicketText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_c
    return-void
.end method
