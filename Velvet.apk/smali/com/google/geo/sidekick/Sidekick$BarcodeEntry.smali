.class public final Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BarcodeEntry"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;
    }
.end annotation


# instance fields
.field private actionUrl_:Ljava/lang/String;

.field private barcode_:Lcom/google/geo/sidekick/Sidekick$Photo;

.field private cachedSize:I

.field private flightBoardingPass_:Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;

.field private hasActionUrl:Z

.field private hasBarcode:Z

.field private hasFlightBoardingPass:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;->barcode_:Lcom/google/geo/sidekick/Sidekick$Photo;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;->actionUrl_:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;->flightBoardingPass_:Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getActionUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;->actionUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getBarcode()Lcom/google/geo/sidekick/Sidekick$Photo;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;->barcode_:Lcom/google/geo/sidekick/Sidekick$Photo;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;->cachedSize:I

    return v0
.end method

.method public getFlightBoardingPass()Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;->flightBoardingPass_:Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;->hasBarcode()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;->getBarcode()Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;->hasActionUrl()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;->getActionUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;->hasFlightBoardingPass()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;->getFlightBoardingPass()Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;->cachedSize:I

    return v0
.end method

.method public hasActionUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;->hasActionUrl:Z

    return v0
.end method

.method public hasBarcode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;->hasBarcode:Z

    return v0
.end method

.method public hasFlightBoardingPass()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;->hasFlightBoardingPass:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$Photo;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$Photo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;->setBarcode(Lcom/google/geo/sidekick/Sidekick$Photo;)Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;->setActionUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;->setFlightBoardingPass(Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;)Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;

    move-result-object v0

    return-object v0
.end method

.method public setActionUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;->hasActionUrl:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;->actionUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setBarcode(Lcom/google/geo/sidekick/Sidekick$Photo;)Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Photo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;->hasBarcode:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;->barcode_:Lcom/google/geo/sidekick/Sidekick$Photo;

    return-object p0
.end method

.method public setFlightBoardingPass(Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;)Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;->hasFlightBoardingPass:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;->flightBoardingPass_:Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;->hasBarcode()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;->getBarcode()Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;->hasActionUrl()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;->getActionUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;->hasFlightBoardingPass()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;->getFlightBoardingPass()Lcom/google/geo/sidekick/Sidekick$BarcodeEntry$FlightBoardingPass;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    return-void
.end method
