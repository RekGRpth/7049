.class public final Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BirthdayCardEntry"
.end annotation


# instance fields
.field private birthdayDoodle_:Lcom/google/geo/sidekick/Sidekick$Photo;

.field private cachedSize:I

.field private email_:Ljava/lang/String;

.field private hasBirthdayDoodle:Z

.field private hasEmail:Z

.field private hasName:Z

.field private hasOwnBirthday:Z

.field private hasOwnBirthdaySeconds:Z

.field private hasPhone:Z

.field private hasPhoto:Z

.field private hasPlusUrl:Z

.field private hasRecipientFocusId:Z

.field private hasSenderFocusId:Z

.field private name_:Ljava/lang/String;

.field private ownBirthdaySeconds_:J

.field private ownBirthday_:Z

.field private phone_:Ljava/lang/String;

.field private photo_:Lcom/google/geo/sidekick/Sidekick$Photo;

.field private plusUrl_:Ljava/lang/String;

.field private recipientFocusId_:Ljava/lang/String;

.field private senderFocusId_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->ownBirthday_:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->name_:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->photo_:Lcom/google/geo/sidekick/Sidekick$Photo;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->email_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->phone_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->plusUrl_:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->ownBirthdaySeconds_:J

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->birthdayDoodle_:Lcom/google/geo/sidekick/Sidekick$Photo;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->senderFocusId_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->recipientFocusId_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getBirthdayDoodle()Lcom/google/geo/sidekick/Sidekick$Photo;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->birthdayDoodle_:Lcom/google/geo/sidekick/Sidekick$Photo;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->cachedSize:I

    return v0
.end method

.method public getEmail()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->email_:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getOwnBirthday()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->ownBirthday_:Z

    return v0
.end method

.method public getOwnBirthdaySeconds()J
    .locals 2

    iget-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->ownBirthdaySeconds_:J

    return-wide v0
.end method

.method public getPhone()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->phone_:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoto()Lcom/google/geo/sidekick/Sidekick$Photo;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->photo_:Lcom/google/geo/sidekick/Sidekick$Photo;

    return-object v0
.end method

.method public getPlusUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->plusUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getRecipientFocusId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->recipientFocusId_:Ljava/lang/String;

    return-object v0
.end method

.method public getSenderFocusId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->senderFocusId_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->hasOwnBirthday()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->getOwnBirthday()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->hasName()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->hasPhoto()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->getPhoto()Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->hasEmail()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->getEmail()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->hasPhone()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->getPhone()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->hasPlusUrl()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->getPlusUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->hasOwnBirthdaySeconds()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->getOwnBirthdaySeconds()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->hasBirthdayDoodle()Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->getBirthdayDoodle()Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->hasSenderFocusId()Z

    move-result v1

    if-eqz v1, :cond_8

    const/16 v1, 0x9

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->getSenderFocusId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->hasRecipientFocusId()Z

    move-result v1

    if-eqz v1, :cond_9

    const/16 v1, 0xa

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->getRecipientFocusId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->cachedSize:I

    return v0
.end method

.method public hasBirthdayDoodle()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->hasBirthdayDoodle:Z

    return v0
.end method

.method public hasEmail()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->hasEmail:Z

    return v0
.end method

.method public hasName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->hasName:Z

    return v0
.end method

.method public hasOwnBirthday()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->hasOwnBirthday:Z

    return v0
.end method

.method public hasOwnBirthdaySeconds()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->hasOwnBirthdaySeconds:Z

    return v0
.end method

.method public hasPhone()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->hasPhone:Z

    return v0
.end method

.method public hasPhoto()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->hasPhoto:Z

    return v0
.end method

.method public hasPlusUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->hasPlusUrl:Z

    return v0
.end method

.method public hasRecipientFocusId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->hasRecipientFocusId:Z

    return v0
.end method

.method public hasSenderFocusId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->hasSenderFocusId:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->setOwnBirthday(Z)Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->setName(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$Photo;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$Photo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->setPhoto(Lcom/google/geo/sidekick/Sidekick$Photo;)Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->setEmail(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->setPhone(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->setPlusUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->setOwnBirthdaySeconds(J)Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;

    goto :goto_0

    :sswitch_8
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$Photo;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$Photo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->setBirthdayDoodle(Lcom/google/geo/sidekick/Sidekick$Photo;)Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->setSenderFocusId(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->setRecipientFocusId(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;

    move-result-object v0

    return-object v0
.end method

.method public setBirthdayDoodle(Lcom/google/geo/sidekick/Sidekick$Photo;)Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Photo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->hasBirthdayDoodle:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->birthdayDoodle_:Lcom/google/geo/sidekick/Sidekick$Photo;

    return-object p0
.end method

.method public setEmail(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->hasEmail:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->email_:Ljava/lang/String;

    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->hasName:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->name_:Ljava/lang/String;

    return-object p0
.end method

.method public setOwnBirthday(Z)Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->hasOwnBirthday:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->ownBirthday_:Z

    return-object p0
.end method

.method public setOwnBirthdaySeconds(J)Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->hasOwnBirthdaySeconds:Z

    iput-wide p1, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->ownBirthdaySeconds_:J

    return-object p0
.end method

.method public setPhone(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->hasPhone:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->phone_:Ljava/lang/String;

    return-object p0
.end method

.method public setPhoto(Lcom/google/geo/sidekick/Sidekick$Photo;)Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Photo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->hasPhoto:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->photo_:Lcom/google/geo/sidekick/Sidekick$Photo;

    return-object p0
.end method

.method public setPlusUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->hasPlusUrl:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->plusUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setRecipientFocusId(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->hasRecipientFocusId:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->recipientFocusId_:Ljava/lang/String;

    return-object p0
.end method

.method public setSenderFocusId(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->hasSenderFocusId:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->senderFocusId_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->hasOwnBirthday()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->getOwnBirthday()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->hasName()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->hasPhoto()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->getPhoto()Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->hasEmail()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->getEmail()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->hasPhone()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->getPhone()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->hasPlusUrl()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->getPlusUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->hasOwnBirthdaySeconds()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->getOwnBirthdaySeconds()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->hasBirthdayDoodle()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->getBirthdayDoodle()Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->hasSenderFocusId()Z

    move-result v0

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->getSenderFocusId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->hasRecipientFocusId()Z

    move-result v0

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->getRecipientFocusId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_9
    return-void
.end method
