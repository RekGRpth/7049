.class public final Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick$CommuteSummary;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TransitDetails"
.end annotation


# instance fields
.field private cachedSize:I

.field private departureTimeSeconds_:J

.field private hasDepartureTimeSeconds:Z

.field private hasStationLocation:Z

.field private hasTransitLineBackgroundColor:Z

.field private hasTransitLineForegroundColor:Z

.field private hasTransitLineName:Z

.field private hasWalkingTimeMinutes:Z

.field private stationLocation_:Lcom/google/geo/sidekick/Sidekick$Location;

.field private transitLineBackgroundColor_:I

.field private transitLineForegroundColor_:I

.field private transitLineName_:Ljava/lang/String;

.field private walkingTimeMinutes_:I


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->stationLocation_:Lcom/google/geo/sidekick/Sidekick$Location;

    iput v2, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->walkingTimeMinutes_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->departureTimeSeconds_:J

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->transitLineName_:Ljava/lang/String;

    iput v2, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->transitLineForegroundColor_:I

    iput v2, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->transitLineBackgroundColor_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->cachedSize:I

    return v0
.end method

.method public getDepartureTimeSeconds()J
    .locals 2

    iget-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->departureTimeSeconds_:J

    return-wide v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->hasStationLocation()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->getStationLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->hasWalkingTimeMinutes()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->getWalkingTimeMinutes()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->hasDepartureTimeSeconds()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->getDepartureTimeSeconds()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->hasTransitLineName()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->getTransitLineName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->hasTransitLineForegroundColor()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->getTransitLineForegroundColor()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFixed32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->hasTransitLineBackgroundColor()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->getTransitLineBackgroundColor()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFixed32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->cachedSize:I

    return v0
.end method

.method public getStationLocation()Lcom/google/geo/sidekick/Sidekick$Location;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->stationLocation_:Lcom/google/geo/sidekick/Sidekick$Location;

    return-object v0
.end method

.method public getTransitLineBackgroundColor()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->transitLineBackgroundColor_:I

    return v0
.end method

.method public getTransitLineForegroundColor()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->transitLineForegroundColor_:I

    return v0
.end method

.method public getTransitLineName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->transitLineName_:Ljava/lang/String;

    return-object v0
.end method

.method public getWalkingTimeMinutes()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->walkingTimeMinutes_:I

    return v0
.end method

.method public hasDepartureTimeSeconds()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->hasDepartureTimeSeconds:Z

    return v0
.end method

.method public hasStationLocation()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->hasStationLocation:Z

    return v0
.end method

.method public hasTransitLineBackgroundColor()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->hasTransitLineBackgroundColor:Z

    return v0
.end method

.method public hasTransitLineForegroundColor()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->hasTransitLineForegroundColor:Z

    return v0
.end method

.method public hasTransitLineName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->hasTransitLineName:Z

    return v0
.end method

.method public hasWalkingTimeMinutes()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->hasWalkingTimeMinutes:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$Location;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->setStationLocation(Lcom/google/geo/sidekick/Sidekick$Location;)Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->setWalkingTimeMinutes(I)Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->setDepartureTimeSeconds(J)Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->setTransitLineName(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFixed32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->setTransitLineForegroundColor(I)Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFixed32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->setTransitLineBackgroundColor(I)Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2d -> :sswitch_5
        0x35 -> :sswitch_6
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;

    move-result-object v0

    return-object v0
.end method

.method public setDepartureTimeSeconds(J)Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->hasDepartureTimeSeconds:Z

    iput-wide p1, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->departureTimeSeconds_:J

    return-object p0
.end method

.method public setStationLocation(Lcom/google/geo/sidekick/Sidekick$Location;)Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Location;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->hasStationLocation:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->stationLocation_:Lcom/google/geo/sidekick/Sidekick$Location;

    return-object p0
.end method

.method public setTransitLineBackgroundColor(I)Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->hasTransitLineBackgroundColor:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->transitLineBackgroundColor_:I

    return-object p0
.end method

.method public setTransitLineForegroundColor(I)Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->hasTransitLineForegroundColor:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->transitLineForegroundColor_:I

    return-object p0
.end method

.method public setTransitLineName(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->hasTransitLineName:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->transitLineName_:Ljava/lang/String;

    return-object p0
.end method

.method public setWalkingTimeMinutes(I)Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->hasWalkingTimeMinutes:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->walkingTimeMinutes_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->hasStationLocation()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->getStationLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->hasWalkingTimeMinutes()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->getWalkingTimeMinutes()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->hasDepartureTimeSeconds()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->getDepartureTimeSeconds()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->hasTransitLineName()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->getTransitLineName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->hasTransitLineForegroundColor()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->getTransitLineForegroundColor()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFixed32(II)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->hasTransitLineBackgroundColor()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->getTransitLineBackgroundColor()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFixed32(II)V

    :cond_5
    return-void
.end method
