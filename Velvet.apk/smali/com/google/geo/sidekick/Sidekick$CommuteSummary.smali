.class public final Lcom/google/geo/sidekick/Sidekick$CommuteSummary;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CommuteSummary"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;
    }
.end annotation


# instance fields
.field private cachedSize:I

.field private distanceInMeters_:I

.field private hasDistanceInMeters:Z

.field private hasHistoricalTrafficDelayInMinutes:Z

.field private hasRouteSummary:Z

.field private hasShowNavigation:Z

.field private hasTrafficDelayInMinutes:Z

.field private hasTrafficStatus:Z

.field private hasTransitDetails:Z

.field private hasTravelMode:Z

.field private hasTravelModeSetting:Z

.field private hasTravelTimeWithoutDelayInMinutes:Z

.field private historicalTrafficDelayInMinutes_:I

.field private pathfinderWaypoint_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Location;",
            ">;"
        }
    .end annotation
.end field

.field private routeSummary_:Ljava/lang/String;

.field private showNavigation_:Z

.field private trafficDelayInMinutes_:I

.field private trafficStatus_:I

.field private transitDetails_:Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;

.field private travelModeSetting_:I

.field private travelMode_:I

.field private travelTimeWithoutDelayInMinutes_:I

.field private waypoints_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Location;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->routeSummary_:Ljava/lang/String;

    iput v1, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->trafficDelayInMinutes_:I

    iput v1, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->historicalTrafficDelayInMinutes_:I

    iput v1, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->travelTimeWithoutDelayInMinutes_:I

    iput v1, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->distanceInMeters_:I

    const/4 v0, 0x4

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->trafficStatus_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->waypoints_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->pathfinderWaypoint_:Ljava/util/List;

    iput v1, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->travelMode_:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->transitDetails_:Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;

    iput-boolean v2, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->showNavigation_:Z

    iput v2, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->travelModeSetting_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addPathfinderWaypoint(Lcom/google/geo/sidekick/Sidekick$Location;)Lcom/google/geo/sidekick/Sidekick$CommuteSummary;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Location;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->pathfinderWaypoint_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->pathfinderWaypoint_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->pathfinderWaypoint_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addWaypoints(Lcom/google/geo/sidekick/Sidekick$Location;)Lcom/google/geo/sidekick/Sidekick$CommuteSummary;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Location;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->waypoints_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->waypoints_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->waypoints_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public clearWaypoints()Lcom/google/geo/sidekick/Sidekick$CommuteSummary;
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->waypoints_:Ljava/util/List;

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->cachedSize:I

    return v0
.end method

.method public getDistanceInMeters()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->distanceInMeters_:I

    return v0
.end method

.method public getHistoricalTrafficDelayInMinutes()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->historicalTrafficDelayInMinutes_:I

    return v0
.end method

.method public getPathfinderWaypointCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->pathfinderWaypoint_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getPathfinderWaypointList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Location;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->pathfinderWaypoint_:Ljava/util/List;

    return-object v0
.end method

.method public getRouteSummary()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->routeSummary_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasRouteSummary()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getRouteSummary()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasTrafficDelayInMinutes()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getTrafficDelayInMinutes()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasTravelTimeWithoutDelayInMinutes()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getTravelTimeWithoutDelayInMinutes()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasDistanceInMeters()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getDistanceInMeters()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasTrafficStatus()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getTrafficStatus()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getWaypointsList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Location;

    const/4 v3, 0x6

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasTravelMode()Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v3, 0x7

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getTravelMode()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_6
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasTransitDetails()Z

    move-result v3

    if-eqz v3, :cond_7

    const/16 v3, 0x8

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getTransitDetails()Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_7
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getPathfinderWaypointList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Location;

    const/16 v3, 0x9

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_1

    :cond_8
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasShowNavigation()Z

    move-result v3

    if-eqz v3, :cond_9

    const/16 v3, 0xa

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getShowNavigation()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_9
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasHistoricalTrafficDelayInMinutes()Z

    move-result v3

    if-eqz v3, :cond_a

    const/16 v3, 0xb

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getHistoricalTrafficDelayInMinutes()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_a
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasTravelModeSetting()Z

    move-result v3

    if-eqz v3, :cond_b

    const/16 v3, 0xc

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getTravelModeSetting()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_b
    iput v2, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->cachedSize:I

    return v2
.end method

.method public getShowNavigation()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->showNavigation_:Z

    return v0
.end method

.method public getTrafficDelayInMinutes()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->trafficDelayInMinutes_:I

    return v0
.end method

.method public getTrafficStatus()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->trafficStatus_:I

    return v0
.end method

.method public getTransitDetails()Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->transitDetails_:Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;

    return-object v0
.end method

.method public getTravelMode()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->travelMode_:I

    return v0
.end method

.method public getTravelModeSetting()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->travelModeSetting_:I

    return v0
.end method

.method public getTravelTimeWithoutDelayInMinutes()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->travelTimeWithoutDelayInMinutes_:I

    return v0
.end method

.method public getWaypoints(I)Lcom/google/geo/sidekick/Sidekick$Location;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->waypoints_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Location;

    return-object v0
.end method

.method public getWaypointsCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->waypoints_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getWaypointsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Location;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->waypoints_:Ljava/util/List;

    return-object v0
.end method

.method public hasDistanceInMeters()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasDistanceInMeters:Z

    return v0
.end method

.method public hasHistoricalTrafficDelayInMinutes()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasHistoricalTrafficDelayInMinutes:Z

    return v0
.end method

.method public hasRouteSummary()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasRouteSummary:Z

    return v0
.end method

.method public hasShowNavigation()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasShowNavigation:Z

    return v0
.end method

.method public hasTrafficDelayInMinutes()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasTrafficDelayInMinutes:Z

    return v0
.end method

.method public hasTrafficStatus()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasTrafficStatus:Z

    return v0
.end method

.method public hasTransitDetails()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasTransitDetails:Z

    return v0
.end method

.method public hasTravelMode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasTravelMode:Z

    return v0
.end method

.method public hasTravelModeSetting()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasTravelModeSetting:Z

    return v0
.end method

.method public hasTravelTimeWithoutDelayInMinutes()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasTravelTimeWithoutDelayInMinutes:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$CommuteSummary;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->setRouteSummary(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->setTrafficDelayInMinutes(I)Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->setTravelTimeWithoutDelayInMinutes(I)Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->setDistanceInMeters(I)Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->setTrafficStatus(I)Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    goto :goto_0

    :sswitch_6
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$Location;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->addWaypoints(Lcom/google/geo/sidekick/Sidekick$Location;)Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->setTravelMode(I)Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    goto :goto_0

    :sswitch_8
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->setTransitDetails(Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;)Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    goto :goto_0

    :sswitch_9
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$Location;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->addPathfinderWaypoint(Lcom/google/geo/sidekick/Sidekick$Location;)Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->setShowNavigation(Z)Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->setHistoricalTrafficDelayInMinutes(I)Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->setTravelModeSetting(I)Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    move-result-object v0

    return-object v0
.end method

.method public setDistanceInMeters(I)Lcom/google/geo/sidekick/Sidekick$CommuteSummary;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasDistanceInMeters:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->distanceInMeters_:I

    return-object p0
.end method

.method public setHistoricalTrafficDelayInMinutes(I)Lcom/google/geo/sidekick/Sidekick$CommuteSummary;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasHistoricalTrafficDelayInMinutes:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->historicalTrafficDelayInMinutes_:I

    return-object p0
.end method

.method public setRouteSummary(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$CommuteSummary;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasRouteSummary:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->routeSummary_:Ljava/lang/String;

    return-object p0
.end method

.method public setShowNavigation(Z)Lcom/google/geo/sidekick/Sidekick$CommuteSummary;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasShowNavigation:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->showNavigation_:Z

    return-object p0
.end method

.method public setTrafficDelayInMinutes(I)Lcom/google/geo/sidekick/Sidekick$CommuteSummary;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasTrafficDelayInMinutes:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->trafficDelayInMinutes_:I

    return-object p0
.end method

.method public setTrafficStatus(I)Lcom/google/geo/sidekick/Sidekick$CommuteSummary;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasTrafficStatus:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->trafficStatus_:I

    return-object p0
.end method

.method public setTransitDetails(Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;)Lcom/google/geo/sidekick/Sidekick$CommuteSummary;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasTransitDetails:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->transitDetails_:Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;

    return-object p0
.end method

.method public setTravelMode(I)Lcom/google/geo/sidekick/Sidekick$CommuteSummary;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasTravelMode:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->travelMode_:I

    return-object p0
.end method

.method public setTravelModeSetting(I)Lcom/google/geo/sidekick/Sidekick$CommuteSummary;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasTravelModeSetting:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->travelModeSetting_:I

    return-object p0
.end method

.method public setTravelTimeWithoutDelayInMinutes(I)Lcom/google/geo/sidekick/Sidekick$CommuteSummary;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasTravelTimeWithoutDelayInMinutes:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->travelTimeWithoutDelayInMinutes_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasRouteSummary()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getRouteSummary()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasTrafficDelayInMinutes()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getTrafficDelayInMinutes()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasTravelTimeWithoutDelayInMinutes()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getTravelTimeWithoutDelayInMinutes()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasDistanceInMeters()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getDistanceInMeters()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasTrafficStatus()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getTrafficStatus()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getWaypointsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Location;

    const/4 v2, 0x6

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasTravelMode()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getTravelMode()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasTransitDetails()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getTransitDetails()Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getPathfinderWaypointList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Location;

    const/16 v2, 0x9

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_1

    :cond_8
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasShowNavigation()Z

    move-result v2

    if-eqz v2, :cond_9

    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getShowNavigation()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasHistoricalTrafficDelayInMinutes()Z

    move-result v2

    if-eqz v2, :cond_a

    const/16 v2, 0xb

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getHistoricalTrafficDelayInMinutes()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasTravelModeSetting()Z

    move-result v2

    if-eqz v2, :cond_b

    const/16 v2, 0xc

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getTravelModeSetting()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_b
    return-void
.end method
