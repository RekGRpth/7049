.class public final Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CurrencyExchangeEntry"
.end annotation


# instance fields
.field private cachedSize:I

.field private defaultLocalExchangeAmount_:I

.field private hasDefaultLocalExchangeAmount:Z

.field private hasHomeCurrency:Z

.field private hasLocalCurrency:Z

.field private hasLocalToHomeRate:Z

.field private hasTitle:Z

.field private homeCurrency_:Ljava/lang/String;

.field private localCurrency_:Ljava/lang/String;

.field private localToHomeRate_:F

.field private title_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->title_:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->localToHomeRate_:F

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->localCurrency_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->homeCurrency_:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->defaultLocalExchangeAmount_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->cachedSize:I

    return v0
.end method

.method public getDefaultLocalExchangeAmount()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->defaultLocalExchangeAmount_:I

    return v0
.end method

.method public getHomeCurrency()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->homeCurrency_:Ljava/lang/String;

    return-object v0
.end method

.method public getLocalCurrency()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->localCurrency_:Ljava/lang/String;

    return-object v0
.end method

.method public getLocalToHomeRate()F
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->localToHomeRate_:F

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->hasTitle()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->hasLocalToHomeRate()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->getLocalToHomeRate()F

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->hasLocalCurrency()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->getLocalCurrency()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->hasHomeCurrency()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->getHomeCurrency()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->hasDefaultLocalExchangeAmount()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->getDefaultLocalExchangeAmount()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->cachedSize:I

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->title_:Ljava/lang/String;

    return-object v0
.end method

.method public hasDefaultLocalExchangeAmount()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->hasDefaultLocalExchangeAmount:Z

    return v0
.end method

.method public hasHomeCurrency()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->hasHomeCurrency:Z

    return v0
.end method

.method public hasLocalCurrency()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->hasLocalCurrency:Z

    return v0
.end method

.method public hasLocalToHomeRate()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->hasLocalToHomeRate:Z

    return v0
.end method

.method public hasTitle()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->hasTitle:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->setTitle(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->setLocalToHomeRate(F)Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->setLocalCurrency(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->setHomeCurrency(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->setDefaultLocalExchangeAmount(I)Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x15 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultLocalExchangeAmount(I)Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->hasDefaultLocalExchangeAmount:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->defaultLocalExchangeAmount_:I

    return-object p0
.end method

.method public setHomeCurrency(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->hasHomeCurrency:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->homeCurrency_:Ljava/lang/String;

    return-object p0
.end method

.method public setLocalCurrency(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->hasLocalCurrency:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->localCurrency_:Ljava/lang/String;

    return-object p0
.end method

.method public setLocalToHomeRate(F)Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->hasLocalToHomeRate:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->localToHomeRate_:F

    return-object p0
.end method

.method public setTitle(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->hasTitle:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->title_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->hasTitle()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->hasLocalToHomeRate()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->getLocalToHomeRate()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->hasLocalCurrency()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->getLocalCurrency()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->hasHomeCurrency()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->getHomeCurrency()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->hasDefaultLocalExchangeAmount()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->getDefaultLocalExchangeAmount()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_4
    return-void
.end method
