.class public final Lcom/google/geo/sidekick/Sidekick$Entry;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Entry"
.end annotation


# instance fields
.field private attractionListEntry_:Lcom/google/geo/sidekick/Sidekick$AttractionListEntry;

.field private barcodeEntry_:Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;

.field private birthdayCardEntry_:Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;

.field private cachedSize:I

.field private calendarEntry_:Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

.field private clockEntry_:Lcom/google/geo/sidekick/Sidekick$ClockEntry;

.field private currencyExchangeEntry_:Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;

.field private encodedServerPayload_:Lcom/google/protobuf/micro/ByteStringMicro;

.field private entryAction_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Action;",
            ">;"
        }
    .end annotation
.end field

.field private eventEntry_:Lcom/google/geo/sidekick/Sidekick$EventEntry;

.field private eventListEntry_:Lcom/google/geo/sidekick/Sidekick$EventListEntry;

.field private flightStatusEntry_:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;

.field private frequentPlaceEntry_:Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

.field private genericCardEntry_:Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

.field private gmailEntry_:Lcom/google/geo/sidekick/Sidekick$GmailEntry;

.field private hasAttractionListEntry:Z

.field private hasBarcodeEntry:Z

.field private hasBirthdayCardEntry:Z

.field private hasCalendarEntry:Z

.field private hasClockEntry:Z

.field private hasCurrencyExchangeEntry:Z

.field private hasEncodedServerPayload:Z

.field private hasEventEntry:Z

.field private hasEventListEntry:Z

.field private hasFlightStatusEntry:Z

.field private hasFrequentPlaceEntry:Z

.field private hasGenericCardEntry:Z

.field private hasGmailEntry:Z

.field private hasIsExample:Z

.field private hasLocationHistoryReminderEntry:Z

.field private hasMapsHistoryEntry:Z

.field private hasMovieEntry:Z

.field private hasMovieListEntry:Z

.field private hasMovieTicketEntry:Z

.field private hasNearbyPlaceEntry:Z

.field private hasNearbyPlacesListEntry:Z

.field private hasNewsEntry:Z

.field private hasNotification:Z

.field private hasPackageTrackingEntry:Z

.field private hasPhotoSpotEntry:Z

.field private hasPublicAlertEntry:Z

.field private hasRealEstateEntry:Z

.field private hasReason:Z

.field private hasReminderEntry:Z

.field private hasResearchPageEntry:Z

.field private hasResearchTopicEntry:Z

.field private hasSportScoreEntry:Z

.field private hasStockQuoteListEntry:Z

.field private hasTransitStationEntry:Z

.field private hasTranslateEntry:Z

.field private hasType:Z

.field private hasUserPrompt:Z

.field private hasVisualSearchEntry:Z

.field private hasVisualSearchListEntry:Z

.field private hasWeatherEntry:Z

.field private hasWebsiteUpdateEntry:Z

.field private hasWebsiteUpdateListEntry:Z

.field private isExample_:Z

.field private locationHistoryReminderEntry_:Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry;

.field private mapsHistoryEntry_:Lcom/google/geo/sidekick/Sidekick$MapsHistoryEntry;

.field private movieEntry_:Lcom/google/geo/sidekick/Sidekick$MovieEntry;

.field private movieListEntry_:Lcom/google/geo/sidekick/Sidekick$MovieListEntry;

.field private movieTicketEntry_:Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;

.field private nearbyPlaceEntry_:Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

.field private nearbyPlacesListEntry_:Lcom/google/geo/sidekick/Sidekick$NearbyPlacesListEntry;

.field private newsEntry_:Lcom/google/geo/sidekick/Sidekick$NewsEntry;

.field private notification_:Lcom/google/geo/sidekick/Sidekick$Notification;

.field private packageTrackingEntry_:Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

.field private photoSpotEntry_:Lcom/google/geo/sidekick/Sidekick$PhotoSpotEntry;

.field private precacheDirective_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$PrecacheDirective;",
            ">;"
        }
    .end annotation
.end field

.field private publicAlertEntry_:Lcom/google/geo/sidekick/Sidekick$PublicAlertEntry;

.field private realEstateEntry_:Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;

.field private reason_:Ljava/lang/String;

.field private reminderEntry_:Lcom/google/geo/sidekick/Sidekick$ReminderEntry;

.field private researchPageEntry_:Lcom/google/geo/sidekick/Sidekick$ResearchPageEntry;

.field private researchTopicEntry_:Lcom/google/geo/sidekick/Sidekick$ResearchTopicEntry;

.field private sportScoreEntry_:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

.field private stockQuoteListEntry_:Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;

.field private transitStationEntry_:Lcom/google/geo/sidekick/Sidekick$TransitStationEntry;

.field private translateEntry_:Lcom/google/geo/sidekick/Sidekick$TranslateEntry;

.field private type_:I

.field private userPrompt_:Ljava/lang/String;

.field private visualSearchEntry_:Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

.field private visualSearchListEntry_:Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

.field private weatherEntry_:Lcom/google/geo/sidekick/Sidekick$WeatherEntry;

.field private websiteUpdateEntry_:Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateEntry;

.field private websiteUpdateListEntry_:Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateListEntry;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->type_:I

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->frequentPlaceEntry_:Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->mapsHistoryEntry_:Lcom/google/geo/sidekick/Sidekick$MapsHistoryEntry;

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->weatherEntry_:Lcom/google/geo/sidekick/Sidekick$WeatherEntry;

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->calendarEntry_:Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->transitStationEntry_:Lcom/google/geo/sidekick/Sidekick$TransitStationEntry;

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->genericCardEntry_:Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->flightStatusEntry_:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->sportScoreEntry_:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->translateEntry_:Lcom/google/geo/sidekick/Sidekick$TranslateEntry;

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->currencyExchangeEntry_:Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->clockEntry_:Lcom/google/geo/sidekick/Sidekick$ClockEntry;

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->nearbyPlaceEntry_:Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->publicAlertEntry_:Lcom/google/geo/sidekick/Sidekick$PublicAlertEntry;

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->movieListEntry_:Lcom/google/geo/sidekick/Sidekick$MovieListEntry;

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->stockQuoteListEntry_:Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->attractionListEntry_:Lcom/google/geo/sidekick/Sidekick$AttractionListEntry;

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->packageTrackingEntry_:Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->newsEntry_:Lcom/google/geo/sidekick/Sidekick$NewsEntry;

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->locationHistoryReminderEntry_:Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry;

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->photoSpotEntry_:Lcom/google/geo/sidekick/Sidekick$PhotoSpotEntry;

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->birthdayCardEntry_:Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->movieEntry_:Lcom/google/geo/sidekick/Sidekick$MovieEntry;

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->movieTicketEntry_:Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->eventEntry_:Lcom/google/geo/sidekick/Sidekick$EventEntry;

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->eventListEntry_:Lcom/google/geo/sidekick/Sidekick$EventListEntry;

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->researchTopicEntry_:Lcom/google/geo/sidekick/Sidekick$ResearchTopicEntry;

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->researchPageEntry_:Lcom/google/geo/sidekick/Sidekick$ResearchPageEntry;

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->barcodeEntry_:Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->gmailEntry_:Lcom/google/geo/sidekick/Sidekick$GmailEntry;

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->visualSearchListEntry_:Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->visualSearchEntry_:Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->nearbyPlacesListEntry_:Lcom/google/geo/sidekick/Sidekick$NearbyPlacesListEntry;

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->realEstateEntry_:Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->reminderEntry_:Lcom/google/geo/sidekick/Sidekick$ReminderEntry;

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->websiteUpdateEntry_:Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateEntry;

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->websiteUpdateListEntry_:Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateListEntry;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->reason_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->entryAction_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->precacheDirective_:Ljava/util/List;

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->notification_:Lcom/google/geo/sidekick/Sidekick$Notification;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->isExample_:Z

    sget-object v0, Lcom/google/protobuf/micro/ByteStringMicro;->EMPTY:Lcom/google/protobuf/micro/ByteStringMicro;

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->encodedServerPayload_:Lcom/google/protobuf/micro/ByteStringMicro;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->userPrompt_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->cachedSize:I

    return-void
.end method

.method public static parseFrom([B)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p0    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;
        }
    .end annotation

    new-instance v0, Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-direct {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Entry;

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Entry;

    return-object v0
.end method


# virtual methods
.method public addEntryAction(Lcom/google/geo/sidekick/Sidekick$Action;)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Action;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->entryAction_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->entryAction_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->entryAction_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addPrecacheDirective(Lcom/google/geo/sidekick/Sidekick$PrecacheDirective;)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$PrecacheDirective;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->precacheDirective_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->precacheDirective_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->precacheDirective_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getAttractionListEntry()Lcom/google/geo/sidekick/Sidekick$AttractionListEntry;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->attractionListEntry_:Lcom/google/geo/sidekick/Sidekick$AttractionListEntry;

    return-object v0
.end method

.method public getBarcodeEntry()Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->barcodeEntry_:Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;

    return-object v0
.end method

.method public getBirthdayCardEntry()Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->birthdayCardEntry_:Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->cachedSize:I

    return v0
.end method

.method public getCalendarEntry()Lcom/google/geo/sidekick/Sidekick$CalendarEntry;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->calendarEntry_:Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

    return-object v0
.end method

.method public getClockEntry()Lcom/google/geo/sidekick/Sidekick$ClockEntry;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->clockEntry_:Lcom/google/geo/sidekick/Sidekick$ClockEntry;

    return-object v0
.end method

.method public getCurrencyExchangeEntry()Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->currencyExchangeEntry_:Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;

    return-object v0
.end method

.method public getEncodedServerPayload()Lcom/google/protobuf/micro/ByteStringMicro;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->encodedServerPayload_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object v0
.end method

.method public getEntryActionCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->entryAction_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getEntryActionList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Action;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->entryAction_:Ljava/util/List;

    return-object v0
.end method

.method public getEventEntry()Lcom/google/geo/sidekick/Sidekick$EventEntry;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->eventEntry_:Lcom/google/geo/sidekick/Sidekick$EventEntry;

    return-object v0
.end method

.method public getEventListEntry()Lcom/google/geo/sidekick/Sidekick$EventListEntry;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->eventListEntry_:Lcom/google/geo/sidekick/Sidekick$EventListEntry;

    return-object v0
.end method

.method public getFlightStatusEntry()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->flightStatusEntry_:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;

    return-object v0
.end method

.method public getFrequentPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->frequentPlaceEntry_:Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    return-object v0
.end method

.method public getGenericCardEntry()Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->genericCardEntry_:Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

    return-object v0
.end method

.method public getGmailEntry()Lcom/google/geo/sidekick/Sidekick$GmailEntry;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->gmailEntry_:Lcom/google/geo/sidekick/Sidekick$GmailEntry;

    return-object v0
.end method

.method public getIsExample()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->isExample_:Z

    return v0
.end method

.method public getLocationHistoryReminderEntry()Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->locationHistoryReminderEntry_:Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry;

    return-object v0
.end method

.method public getMapsHistoryEntry()Lcom/google/geo/sidekick/Sidekick$MapsHistoryEntry;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->mapsHistoryEntry_:Lcom/google/geo/sidekick/Sidekick$MapsHistoryEntry;

    return-object v0
.end method

.method public getMovieEntry()Lcom/google/geo/sidekick/Sidekick$MovieEntry;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->movieEntry_:Lcom/google/geo/sidekick/Sidekick$MovieEntry;

    return-object v0
.end method

.method public getMovieListEntry()Lcom/google/geo/sidekick/Sidekick$MovieListEntry;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->movieListEntry_:Lcom/google/geo/sidekick/Sidekick$MovieListEntry;

    return-object v0
.end method

.method public getMovieTicketEntry()Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->movieTicketEntry_:Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;

    return-object v0
.end method

.method public getNearbyPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->nearbyPlaceEntry_:Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    return-object v0
.end method

.method public getNearbyPlacesListEntry()Lcom/google/geo/sidekick/Sidekick$NearbyPlacesListEntry;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->nearbyPlacesListEntry_:Lcom/google/geo/sidekick/Sidekick$NearbyPlacesListEntry;

    return-object v0
.end method

.method public getNewsEntry()Lcom/google/geo/sidekick/Sidekick$NewsEntry;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->newsEntry_:Lcom/google/geo/sidekick/Sidekick$NewsEntry;

    return-object v0
.end method

.method public getNotification()Lcom/google/geo/sidekick/Sidekick$Notification;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->notification_:Lcom/google/geo/sidekick/Sidekick$Notification;

    return-object v0
.end method

.method public getPackageTrackingEntry()Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->packageTrackingEntry_:Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    return-object v0
.end method

.method public getPhotoSpotEntry()Lcom/google/geo/sidekick/Sidekick$PhotoSpotEntry;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->photoSpotEntry_:Lcom/google/geo/sidekick/Sidekick$PhotoSpotEntry;

    return-object v0
.end method

.method public getPrecacheDirectiveList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$PrecacheDirective;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->precacheDirective_:Ljava/util/List;

    return-object v0
.end method

.method public getPublicAlertEntry()Lcom/google/geo/sidekick/Sidekick$PublicAlertEntry;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->publicAlertEntry_:Lcom/google/geo/sidekick/Sidekick$PublicAlertEntry;

    return-object v0
.end method

.method public getRealEstateEntry()Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->realEstateEntry_:Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;

    return-object v0
.end method

.method public getReason()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->reason_:Ljava/lang/String;

    return-object v0
.end method

.method public getReminderEntry()Lcom/google/geo/sidekick/Sidekick$ReminderEntry;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->reminderEntry_:Lcom/google/geo/sidekick/Sidekick$ReminderEntry;

    return-object v0
.end method

.method public getResearchPageEntry()Lcom/google/geo/sidekick/Sidekick$ResearchPageEntry;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->researchPageEntry_:Lcom/google/geo/sidekick/Sidekick$ResearchPageEntry;

    return-object v0
.end method

.method public getResearchTopicEntry()Lcom/google/geo/sidekick/Sidekick$ResearchTopicEntry;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->researchTopicEntry_:Lcom/google/geo/sidekick/Sidekick$ResearchTopicEntry;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasFrequentPlaceEntry()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getFrequentPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasMapsHistoryEntry()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getMapsHistoryEntry()Lcom/google/geo/sidekick/Sidekick$MapsHistoryEntry;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasReason()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getReason()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getEntryActionList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Action;

    const/4 v3, 0x6

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasNotification()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x7

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getNotification()Lcom/google/geo/sidekick/Sidekick$Notification;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasType()Z

    move-result v3

    if-eqz v3, :cond_5

    const/16 v3, 0x8

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getType()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasWeatherEntry()Z

    move-result v3

    if-eqz v3, :cond_6

    const/16 v3, 0xb

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getWeatherEntry()Lcom/google/geo/sidekick/Sidekick$WeatherEntry;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_6
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasCalendarEntry()Z

    move-result v3

    if-eqz v3, :cond_7

    const/16 v3, 0xc

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getCalendarEntry()Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_7
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasTransitStationEntry()Z

    move-result v3

    if-eqz v3, :cond_8

    const/16 v3, 0xe

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getTransitStationEntry()Lcom/google/geo/sidekick/Sidekick$TransitStationEntry;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_8
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasGenericCardEntry()Z

    move-result v3

    if-eqz v3, :cond_9

    const/16 v3, 0x10

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getGenericCardEntry()Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_9
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasFlightStatusEntry()Z

    move-result v3

    if-eqz v3, :cond_a

    const/16 v3, 0x11

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getFlightStatusEntry()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_a
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasSportScoreEntry()Z

    move-result v3

    if-eqz v3, :cond_b

    const/16 v3, 0x12

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getSportScoreEntry()Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_b
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasTranslateEntry()Z

    move-result v3

    if-eqz v3, :cond_c

    const/16 v3, 0x13

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getTranslateEntry()Lcom/google/geo/sidekick/Sidekick$TranslateEntry;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_c
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasClockEntry()Z

    move-result v3

    if-eqz v3, :cond_d

    const/16 v3, 0x14

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getClockEntry()Lcom/google/geo/sidekick/Sidekick$ClockEntry;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_d
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasCurrencyExchangeEntry()Z

    move-result v3

    if-eqz v3, :cond_e

    const/16 v3, 0x15

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getCurrencyExchangeEntry()Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_e
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasIsExample()Z

    move-result v3

    if-eqz v3, :cond_f

    const/16 v3, 0x16

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getIsExample()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_f
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasNearbyPlaceEntry()Z

    move-result v3

    if-eqz v3, :cond_10

    const/16 v3, 0x17

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getNearbyPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_10
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasEncodedServerPayload()Z

    move-result v3

    if-eqz v3, :cond_11

    const/16 v3, 0x18

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getEncodedServerPayload()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBytesSize(ILcom/google/protobuf/micro/ByteStringMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_11
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasPublicAlertEntry()Z

    move-result v3

    if-eqz v3, :cond_12

    const/16 v3, 0x19

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getPublicAlertEntry()Lcom/google/geo/sidekick/Sidekick$PublicAlertEntry;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_12
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasMovieListEntry()Z

    move-result v3

    if-eqz v3, :cond_13

    const/16 v3, 0x1a

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getMovieListEntry()Lcom/google/geo/sidekick/Sidekick$MovieListEntry;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_13
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasStockQuoteListEntry()Z

    move-result v3

    if-eqz v3, :cond_14

    const/16 v3, 0x1b

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getStockQuoteListEntry()Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_14
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasAttractionListEntry()Z

    move-result v3

    if-eqz v3, :cond_15

    const/16 v3, 0x1c

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getAttractionListEntry()Lcom/google/geo/sidekick/Sidekick$AttractionListEntry;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_15
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasPackageTrackingEntry()Z

    move-result v3

    if-eqz v3, :cond_16

    const/16 v3, 0x1d

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getPackageTrackingEntry()Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_16
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasNewsEntry()Z

    move-result v3

    if-eqz v3, :cond_17

    const/16 v3, 0x1e

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getNewsEntry()Lcom/google/geo/sidekick/Sidekick$NewsEntry;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_17
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasLocationHistoryReminderEntry()Z

    move-result v3

    if-eqz v3, :cond_18

    const/16 v3, 0x1f

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getLocationHistoryReminderEntry()Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_18
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasPhotoSpotEntry()Z

    move-result v3

    if-eqz v3, :cond_19

    const/16 v3, 0x20

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getPhotoSpotEntry()Lcom/google/geo/sidekick/Sidekick$PhotoSpotEntry;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_19
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getPrecacheDirectiveList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$PrecacheDirective;

    const/16 v3, 0x21

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_1

    :cond_1a
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasBirthdayCardEntry()Z

    move-result v3

    if-eqz v3, :cond_1b

    const/16 v3, 0x22

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getBirthdayCardEntry()Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1b
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasMovieEntry()Z

    move-result v3

    if-eqz v3, :cond_1c

    const/16 v3, 0x23

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getMovieEntry()Lcom/google/geo/sidekick/Sidekick$MovieEntry;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1c
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasEventEntry()Z

    move-result v3

    if-eqz v3, :cond_1d

    const/16 v3, 0x24

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getEventEntry()Lcom/google/geo/sidekick/Sidekick$EventEntry;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1d
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasResearchTopicEntry()Z

    move-result v3

    if-eqz v3, :cond_1e

    const/16 v3, 0x25

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getResearchTopicEntry()Lcom/google/geo/sidekick/Sidekick$ResearchTopicEntry;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1e
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasResearchPageEntry()Z

    move-result v3

    if-eqz v3, :cond_1f

    const/16 v3, 0x26

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getResearchPageEntry()Lcom/google/geo/sidekick/Sidekick$ResearchPageEntry;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1f
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasBarcodeEntry()Z

    move-result v3

    if-eqz v3, :cond_20

    const/16 v3, 0x27

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getBarcodeEntry()Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_20
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasGmailEntry()Z

    move-result v3

    if-eqz v3, :cond_21

    const/16 v3, 0x28

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getGmailEntry()Lcom/google/geo/sidekick/Sidekick$GmailEntry;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_21
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasVisualSearchListEntry()Z

    move-result v3

    if-eqz v3, :cond_22

    const/16 v3, 0x29

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getVisualSearchListEntry()Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_22
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasVisualSearchEntry()Z

    move-result v3

    if-eqz v3, :cond_23

    const/16 v3, 0x2a

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getVisualSearchEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_23
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasNearbyPlacesListEntry()Z

    move-result v3

    if-eqz v3, :cond_24

    const/16 v3, 0x2b

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getNearbyPlacesListEntry()Lcom/google/geo/sidekick/Sidekick$NearbyPlacesListEntry;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_24
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasEventListEntry()Z

    move-result v3

    if-eqz v3, :cond_25

    const/16 v3, 0x2c

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getEventListEntry()Lcom/google/geo/sidekick/Sidekick$EventListEntry;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_25
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasUserPrompt()Z

    move-result v3

    if-eqz v3, :cond_26

    const/16 v3, 0x2d

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getUserPrompt()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_26
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasMovieTicketEntry()Z

    move-result v3

    if-eqz v3, :cond_27

    const/16 v3, 0x2e

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getMovieTicketEntry()Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_27
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasRealEstateEntry()Z

    move-result v3

    if-eqz v3, :cond_28

    const/16 v3, 0x2f

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getRealEstateEntry()Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_28
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasReminderEntry()Z

    move-result v3

    if-eqz v3, :cond_29

    const/16 v3, 0x30

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getReminderEntry()Lcom/google/geo/sidekick/Sidekick$ReminderEntry;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_29
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasWebsiteUpdateEntry()Z

    move-result v3

    if-eqz v3, :cond_2a

    const/16 v3, 0x31

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getWebsiteUpdateEntry()Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateEntry;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2a
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasWebsiteUpdateListEntry()Z

    move-result v3

    if-eqz v3, :cond_2b

    const/16 v3, 0x32

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getWebsiteUpdateListEntry()Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateListEntry;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2b
    iput v2, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->cachedSize:I

    return v2
.end method

.method public getSportScoreEntry()Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->sportScoreEntry_:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    return-object v0
.end method

.method public getStockQuoteListEntry()Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->stockQuoteListEntry_:Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;

    return-object v0
.end method

.method public getTransitStationEntry()Lcom/google/geo/sidekick/Sidekick$TransitStationEntry;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->transitStationEntry_:Lcom/google/geo/sidekick/Sidekick$TransitStationEntry;

    return-object v0
.end method

.method public getTranslateEntry()Lcom/google/geo/sidekick/Sidekick$TranslateEntry;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->translateEntry_:Lcom/google/geo/sidekick/Sidekick$TranslateEntry;

    return-object v0
.end method

.method public getType()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->type_:I

    return v0
.end method

.method public getUserPrompt()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->userPrompt_:Ljava/lang/String;

    return-object v0
.end method

.method public getVisualSearchEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->visualSearchEntry_:Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    return-object v0
.end method

.method public getVisualSearchListEntry()Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->visualSearchListEntry_:Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

    return-object v0
.end method

.method public getWeatherEntry()Lcom/google/geo/sidekick/Sidekick$WeatherEntry;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->weatherEntry_:Lcom/google/geo/sidekick/Sidekick$WeatherEntry;

    return-object v0
.end method

.method public getWebsiteUpdateEntry()Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateEntry;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->websiteUpdateEntry_:Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateEntry;

    return-object v0
.end method

.method public getWebsiteUpdateListEntry()Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateListEntry;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->websiteUpdateListEntry_:Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateListEntry;

    return-object v0
.end method

.method public hasAttractionListEntry()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasAttractionListEntry:Z

    return v0
.end method

.method public hasBarcodeEntry()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasBarcodeEntry:Z

    return v0
.end method

.method public hasBirthdayCardEntry()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasBirthdayCardEntry:Z

    return v0
.end method

.method public hasCalendarEntry()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasCalendarEntry:Z

    return v0
.end method

.method public hasClockEntry()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasClockEntry:Z

    return v0
.end method

.method public hasCurrencyExchangeEntry()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasCurrencyExchangeEntry:Z

    return v0
.end method

.method public hasEncodedServerPayload()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasEncodedServerPayload:Z

    return v0
.end method

.method public hasEventEntry()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasEventEntry:Z

    return v0
.end method

.method public hasEventListEntry()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasEventListEntry:Z

    return v0
.end method

.method public hasFlightStatusEntry()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasFlightStatusEntry:Z

    return v0
.end method

.method public hasFrequentPlaceEntry()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasFrequentPlaceEntry:Z

    return v0
.end method

.method public hasGenericCardEntry()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasGenericCardEntry:Z

    return v0
.end method

.method public hasGmailEntry()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasGmailEntry:Z

    return v0
.end method

.method public hasIsExample()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasIsExample:Z

    return v0
.end method

.method public hasLocationHistoryReminderEntry()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasLocationHistoryReminderEntry:Z

    return v0
.end method

.method public hasMapsHistoryEntry()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasMapsHistoryEntry:Z

    return v0
.end method

.method public hasMovieEntry()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasMovieEntry:Z

    return v0
.end method

.method public hasMovieListEntry()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasMovieListEntry:Z

    return v0
.end method

.method public hasMovieTicketEntry()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasMovieTicketEntry:Z

    return v0
.end method

.method public hasNearbyPlaceEntry()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasNearbyPlaceEntry:Z

    return v0
.end method

.method public hasNearbyPlacesListEntry()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasNearbyPlacesListEntry:Z

    return v0
.end method

.method public hasNewsEntry()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasNewsEntry:Z

    return v0
.end method

.method public hasNotification()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasNotification:Z

    return v0
.end method

.method public hasPackageTrackingEntry()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasPackageTrackingEntry:Z

    return v0
.end method

.method public hasPhotoSpotEntry()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasPhotoSpotEntry:Z

    return v0
.end method

.method public hasPublicAlertEntry()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasPublicAlertEntry:Z

    return v0
.end method

.method public hasRealEstateEntry()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasRealEstateEntry:Z

    return v0
.end method

.method public hasReason()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasReason:Z

    return v0
.end method

.method public hasReminderEntry()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasReminderEntry:Z

    return v0
.end method

.method public hasResearchPageEntry()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasResearchPageEntry:Z

    return v0
.end method

.method public hasResearchTopicEntry()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasResearchTopicEntry:Z

    return v0
.end method

.method public hasSportScoreEntry()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasSportScoreEntry:Z

    return v0
.end method

.method public hasStockQuoteListEntry()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasStockQuoteListEntry:Z

    return v0
.end method

.method public hasTransitStationEntry()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasTransitStationEntry:Z

    return v0
.end method

.method public hasTranslateEntry()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasTranslateEntry:Z

    return v0
.end method

.method public hasType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasType:Z

    return v0
.end method

.method public hasUserPrompt()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasUserPrompt:Z

    return v0
.end method

.method public hasVisualSearchEntry()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasVisualSearchEntry:Z

    return v0
.end method

.method public hasVisualSearchListEntry()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasVisualSearchListEntry:Z

    return v0
.end method

.method public hasWeatherEntry()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasWeatherEntry:Z

    return v0
.end method

.method public hasWebsiteUpdateEntry()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasWebsiteUpdateEntry:Z

    return v0
.end method

.method public hasWebsiteUpdateListEntry()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasWebsiteUpdateListEntry:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->setFrequentPlaceEntry(Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$MapsHistoryEntry;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$MapsHistoryEntry;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->setMapsHistoryEntry(Lcom/google/geo/sidekick/Sidekick$MapsHistoryEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$Entry;->setReason(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$Entry;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$Action;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$Action;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->addEntryAction(Lcom/google/geo/sidekick/Sidekick$Action;)Lcom/google/geo/sidekick/Sidekick$Entry;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$Notification;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$Notification;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->setNotification(Lcom/google/geo/sidekick/Sidekick$Notification;)Lcom/google/geo/sidekick/Sidekick$Entry;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$Entry;->setType(I)Lcom/google/geo/sidekick/Sidekick$Entry;

    goto :goto_0

    :sswitch_7
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->setWeatherEntry(Lcom/google/geo/sidekick/Sidekick$WeatherEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;

    goto :goto_0

    :sswitch_8
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->setCalendarEntry(Lcom/google/geo/sidekick/Sidekick$CalendarEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;

    goto :goto_0

    :sswitch_9
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->setTransitStationEntry(Lcom/google/geo/sidekick/Sidekick$TransitStationEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;

    goto :goto_0

    :sswitch_a
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->setGenericCardEntry(Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;

    goto :goto_0

    :sswitch_b
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->setFlightStatusEntry(Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;

    goto/16 :goto_0

    :sswitch_c
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->setSportScoreEntry(Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;

    goto/16 :goto_0

    :sswitch_d
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$TranslateEntry;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$TranslateEntry;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->setTranslateEntry(Lcom/google/geo/sidekick/Sidekick$TranslateEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;

    goto/16 :goto_0

    :sswitch_e
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$ClockEntry;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$ClockEntry;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->setClockEntry(Lcom/google/geo/sidekick/Sidekick$ClockEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;

    goto/16 :goto_0

    :sswitch_f
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->setCurrencyExchangeEntry(Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$Entry;->setIsExample(Z)Lcom/google/geo/sidekick/Sidekick$Entry;

    goto/16 :goto_0

    :sswitch_11
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->setNearbyPlaceEntry(Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBytes()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$Entry;->setEncodedServerPayload(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/geo/sidekick/Sidekick$Entry;

    goto/16 :goto_0

    :sswitch_13
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$PublicAlertEntry;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$PublicAlertEntry;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->setPublicAlertEntry(Lcom/google/geo/sidekick/Sidekick$PublicAlertEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;

    goto/16 :goto_0

    :sswitch_14
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$MovieListEntry;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$MovieListEntry;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->setMovieListEntry(Lcom/google/geo/sidekick/Sidekick$MovieListEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;

    goto/16 :goto_0

    :sswitch_15
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->setStockQuoteListEntry(Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;

    goto/16 :goto_0

    :sswitch_16
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$AttractionListEntry;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$AttractionListEntry;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->setAttractionListEntry(Lcom/google/geo/sidekick/Sidekick$AttractionListEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;

    goto/16 :goto_0

    :sswitch_17
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->setPackageTrackingEntry(Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;

    goto/16 :goto_0

    :sswitch_18
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$NewsEntry;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$NewsEntry;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->setNewsEntry(Lcom/google/geo/sidekick/Sidekick$NewsEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;

    goto/16 :goto_0

    :sswitch_19
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->setLocationHistoryReminderEntry(Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;

    goto/16 :goto_0

    :sswitch_1a
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$PhotoSpotEntry;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$PhotoSpotEntry;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->setPhotoSpotEntry(Lcom/google/geo/sidekick/Sidekick$PhotoSpotEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;

    goto/16 :goto_0

    :sswitch_1b
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$PrecacheDirective;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$PrecacheDirective;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->addPrecacheDirective(Lcom/google/geo/sidekick/Sidekick$PrecacheDirective;)Lcom/google/geo/sidekick/Sidekick$Entry;

    goto/16 :goto_0

    :sswitch_1c
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->setBirthdayCardEntry(Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;

    goto/16 :goto_0

    :sswitch_1d
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$MovieEntry;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$MovieEntry;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->setMovieEntry(Lcom/google/geo/sidekick/Sidekick$MovieEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;

    goto/16 :goto_0

    :sswitch_1e
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$EventEntry;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$EventEntry;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->setEventEntry(Lcom/google/geo/sidekick/Sidekick$EventEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;

    goto/16 :goto_0

    :sswitch_1f
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$ResearchTopicEntry;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$ResearchTopicEntry;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->setResearchTopicEntry(Lcom/google/geo/sidekick/Sidekick$ResearchTopicEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;

    goto/16 :goto_0

    :sswitch_20
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$ResearchPageEntry;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$ResearchPageEntry;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->setResearchPageEntry(Lcom/google/geo/sidekick/Sidekick$ResearchPageEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;

    goto/16 :goto_0

    :sswitch_21
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->setBarcodeEntry(Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;

    goto/16 :goto_0

    :sswitch_22
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$GmailEntry;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$GmailEntry;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->setGmailEntry(Lcom/google/geo/sidekick/Sidekick$GmailEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;

    goto/16 :goto_0

    :sswitch_23
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->setVisualSearchListEntry(Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;

    goto/16 :goto_0

    :sswitch_24
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->setVisualSearchEntry(Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;

    goto/16 :goto_0

    :sswitch_25
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$NearbyPlacesListEntry;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$NearbyPlacesListEntry;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->setNearbyPlacesListEntry(Lcom/google/geo/sidekick/Sidekick$NearbyPlacesListEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;

    goto/16 :goto_0

    :sswitch_26
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$EventListEntry;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$EventListEntry;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->setEventListEntry(Lcom/google/geo/sidekick/Sidekick$EventListEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;

    goto/16 :goto_0

    :sswitch_27
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$Entry;->setUserPrompt(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$Entry;

    goto/16 :goto_0

    :sswitch_28
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->setMovieTicketEntry(Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;

    goto/16 :goto_0

    :sswitch_29
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->setRealEstateEntry(Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;

    goto/16 :goto_0

    :sswitch_2a
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$ReminderEntry;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->setReminderEntry(Lcom/google/geo/sidekick/Sidekick$ReminderEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;

    goto/16 :goto_0

    :sswitch_2b
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateEntry;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateEntry;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->setWebsiteUpdateEntry(Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;

    goto/16 :goto_0

    :sswitch_2c
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateListEntry;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateListEntry;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->setWebsiteUpdateListEntry(Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateListEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x2a -> :sswitch_3
        0x32 -> :sswitch_4
        0x3a -> :sswitch_5
        0x40 -> :sswitch_6
        0x5a -> :sswitch_7
        0x62 -> :sswitch_8
        0x72 -> :sswitch_9
        0x82 -> :sswitch_a
        0x8a -> :sswitch_b
        0x92 -> :sswitch_c
        0x9a -> :sswitch_d
        0xa2 -> :sswitch_e
        0xaa -> :sswitch_f
        0xb0 -> :sswitch_10
        0xba -> :sswitch_11
        0xc2 -> :sswitch_12
        0xca -> :sswitch_13
        0xd2 -> :sswitch_14
        0xda -> :sswitch_15
        0xe2 -> :sswitch_16
        0xea -> :sswitch_17
        0xf2 -> :sswitch_18
        0xfa -> :sswitch_19
        0x102 -> :sswitch_1a
        0x10a -> :sswitch_1b
        0x112 -> :sswitch_1c
        0x11a -> :sswitch_1d
        0x122 -> :sswitch_1e
        0x12a -> :sswitch_1f
        0x132 -> :sswitch_20
        0x13a -> :sswitch_21
        0x142 -> :sswitch_22
        0x14a -> :sswitch_23
        0x152 -> :sswitch_24
        0x15a -> :sswitch_25
        0x162 -> :sswitch_26
        0x16a -> :sswitch_27
        0x172 -> :sswitch_28
        0x17a -> :sswitch_29
        0x182 -> :sswitch_2a
        0x18a -> :sswitch_2b
        0x192 -> :sswitch_2c
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v0

    return-object v0
.end method

.method public setAttractionListEntry(Lcom/google/geo/sidekick/Sidekick$AttractionListEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$AttractionListEntry;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasAttractionListEntry:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->attractionListEntry_:Lcom/google/geo/sidekick/Sidekick$AttractionListEntry;

    return-object p0
.end method

.method public setBarcodeEntry(Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasBarcodeEntry:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->barcodeEntry_:Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;

    return-object p0
.end method

.method public setBirthdayCardEntry(Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasBirthdayCardEntry:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->birthdayCardEntry_:Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;

    return-object p0
.end method

.method public setCalendarEntry(Lcom/google/geo/sidekick/Sidekick$CalendarEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasCalendarEntry:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->calendarEntry_:Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

    return-object p0
.end method

.method public setClockEntry(Lcom/google/geo/sidekick/Sidekick$ClockEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$ClockEntry;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasClockEntry:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->clockEntry_:Lcom/google/geo/sidekick/Sidekick$ClockEntry;

    return-object p0
.end method

.method public setCurrencyExchangeEntry(Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasCurrencyExchangeEntry:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->currencyExchangeEntry_:Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;

    return-object p0
.end method

.method public setEncodedServerPayload(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/ByteStringMicro;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasEncodedServerPayload:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->encodedServerPayload_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object p0
.end method

.method public setEventEntry(Lcom/google/geo/sidekick/Sidekick$EventEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$EventEntry;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasEventEntry:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->eventEntry_:Lcom/google/geo/sidekick/Sidekick$EventEntry;

    return-object p0
.end method

.method public setEventListEntry(Lcom/google/geo/sidekick/Sidekick$EventListEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$EventListEntry;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasEventListEntry:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->eventListEntry_:Lcom/google/geo/sidekick/Sidekick$EventListEntry;

    return-object p0
.end method

.method public setFlightStatusEntry(Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasFlightStatusEntry:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->flightStatusEntry_:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;

    return-object p0
.end method

.method public setFrequentPlaceEntry(Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasFrequentPlaceEntry:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->frequentPlaceEntry_:Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    return-object p0
.end method

.method public setGenericCardEntry(Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasGenericCardEntry:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->genericCardEntry_:Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

    return-object p0
.end method

.method public setGmailEntry(Lcom/google/geo/sidekick/Sidekick$GmailEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$GmailEntry;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasGmailEntry:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->gmailEntry_:Lcom/google/geo/sidekick/Sidekick$GmailEntry;

    return-object p0
.end method

.method public setIsExample(Z)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasIsExample:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->isExample_:Z

    return-object p0
.end method

.method public setLocationHistoryReminderEntry(Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasLocationHistoryReminderEntry:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->locationHistoryReminderEntry_:Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry;

    return-object p0
.end method

.method public setMapsHistoryEntry(Lcom/google/geo/sidekick/Sidekick$MapsHistoryEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$MapsHistoryEntry;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasMapsHistoryEntry:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->mapsHistoryEntry_:Lcom/google/geo/sidekick/Sidekick$MapsHistoryEntry;

    return-object p0
.end method

.method public setMovieEntry(Lcom/google/geo/sidekick/Sidekick$MovieEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$MovieEntry;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasMovieEntry:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->movieEntry_:Lcom/google/geo/sidekick/Sidekick$MovieEntry;

    return-object p0
.end method

.method public setMovieListEntry(Lcom/google/geo/sidekick/Sidekick$MovieListEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$MovieListEntry;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasMovieListEntry:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->movieListEntry_:Lcom/google/geo/sidekick/Sidekick$MovieListEntry;

    return-object p0
.end method

.method public setMovieTicketEntry(Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasMovieTicketEntry:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->movieTicketEntry_:Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;

    return-object p0
.end method

.method public setNearbyPlaceEntry(Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasNearbyPlaceEntry:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->nearbyPlaceEntry_:Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    return-object p0
.end method

.method public setNearbyPlacesListEntry(Lcom/google/geo/sidekick/Sidekick$NearbyPlacesListEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$NearbyPlacesListEntry;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasNearbyPlacesListEntry:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->nearbyPlacesListEntry_:Lcom/google/geo/sidekick/Sidekick$NearbyPlacesListEntry;

    return-object p0
.end method

.method public setNewsEntry(Lcom/google/geo/sidekick/Sidekick$NewsEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$NewsEntry;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasNewsEntry:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->newsEntry_:Lcom/google/geo/sidekick/Sidekick$NewsEntry;

    return-object p0
.end method

.method public setNotification(Lcom/google/geo/sidekick/Sidekick$Notification;)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Notification;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasNotification:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->notification_:Lcom/google/geo/sidekick/Sidekick$Notification;

    return-object p0
.end method

.method public setPackageTrackingEntry(Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasPackageTrackingEntry:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->packageTrackingEntry_:Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    return-object p0
.end method

.method public setPhotoSpotEntry(Lcom/google/geo/sidekick/Sidekick$PhotoSpotEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$PhotoSpotEntry;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasPhotoSpotEntry:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->photoSpotEntry_:Lcom/google/geo/sidekick/Sidekick$PhotoSpotEntry;

    return-object p0
.end method

.method public setPublicAlertEntry(Lcom/google/geo/sidekick/Sidekick$PublicAlertEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$PublicAlertEntry;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasPublicAlertEntry:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->publicAlertEntry_:Lcom/google/geo/sidekick/Sidekick$PublicAlertEntry;

    return-object p0
.end method

.method public setRealEstateEntry(Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasRealEstateEntry:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->realEstateEntry_:Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;

    return-object p0
.end method

.method public setReason(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasReason:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->reason_:Ljava/lang/String;

    return-object p0
.end method

.method public setReminderEntry(Lcom/google/geo/sidekick/Sidekick$ReminderEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$ReminderEntry;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasReminderEntry:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->reminderEntry_:Lcom/google/geo/sidekick/Sidekick$ReminderEntry;

    return-object p0
.end method

.method public setResearchPageEntry(Lcom/google/geo/sidekick/Sidekick$ResearchPageEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$ResearchPageEntry;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasResearchPageEntry:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->researchPageEntry_:Lcom/google/geo/sidekick/Sidekick$ResearchPageEntry;

    return-object p0
.end method

.method public setResearchTopicEntry(Lcom/google/geo/sidekick/Sidekick$ResearchTopicEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$ResearchTopicEntry;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasResearchTopicEntry:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->researchTopicEntry_:Lcom/google/geo/sidekick/Sidekick$ResearchTopicEntry;

    return-object p0
.end method

.method public setSportScoreEntry(Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasSportScoreEntry:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->sportScoreEntry_:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    return-object p0
.end method

.method public setStockQuoteListEntry(Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasStockQuoteListEntry:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->stockQuoteListEntry_:Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;

    return-object p0
.end method

.method public setTransitStationEntry(Lcom/google/geo/sidekick/Sidekick$TransitStationEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$TransitStationEntry;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasTransitStationEntry:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->transitStationEntry_:Lcom/google/geo/sidekick/Sidekick$TransitStationEntry;

    return-object p0
.end method

.method public setTranslateEntry(Lcom/google/geo/sidekick/Sidekick$TranslateEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$TranslateEntry;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasTranslateEntry:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->translateEntry_:Lcom/google/geo/sidekick/Sidekick$TranslateEntry;

    return-object p0
.end method

.method public setType(I)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasType:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->type_:I

    return-object p0
.end method

.method public setUserPrompt(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasUserPrompt:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->userPrompt_:Ljava/lang/String;

    return-object p0
.end method

.method public setVisualSearchEntry(Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasVisualSearchEntry:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->visualSearchEntry_:Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    return-object p0
.end method

.method public setVisualSearchListEntry(Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasVisualSearchListEntry:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->visualSearchListEntry_:Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

    return-object p0
.end method

.method public setWeatherEntry(Lcom/google/geo/sidekick/Sidekick$WeatherEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$WeatherEntry;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasWeatherEntry:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->weatherEntry_:Lcom/google/geo/sidekick/Sidekick$WeatherEntry;

    return-object p0
.end method

.method public setWebsiteUpdateEntry(Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateEntry;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasWebsiteUpdateEntry:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->websiteUpdateEntry_:Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateEntry;

    return-object p0
.end method

.method public setWebsiteUpdateListEntry(Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateListEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateListEntry;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->hasWebsiteUpdateListEntry:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Entry;->websiteUpdateListEntry_:Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateListEntry;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasFrequentPlaceEntry()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getFrequentPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasMapsHistoryEntry()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getMapsHistoryEntry()Lcom/google/geo/sidekick/Sidekick$MapsHistoryEntry;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasReason()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getReason()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getEntryActionList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Action;

    const/4 v2, 0x6

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasNotification()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getNotification()Lcom/google/geo/sidekick/Sidekick$Notification;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasType()Z

    move-result v2

    if-eqz v2, :cond_5

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getType()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasWeatherEntry()Z

    move-result v2

    if-eqz v2, :cond_6

    const/16 v2, 0xb

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getWeatherEntry()Lcom/google/geo/sidekick/Sidekick$WeatherEntry;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasCalendarEntry()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0xc

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getCalendarEntry()Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasTransitStationEntry()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0xe

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getTransitStationEntry()Lcom/google/geo/sidekick/Sidekick$TransitStationEntry;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasGenericCardEntry()Z

    move-result v2

    if-eqz v2, :cond_9

    const/16 v2, 0x10

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getGenericCardEntry()Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasFlightStatusEntry()Z

    move-result v2

    if-eqz v2, :cond_a

    const/16 v2, 0x11

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getFlightStatusEntry()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasSportScoreEntry()Z

    move-result v2

    if-eqz v2, :cond_b

    const/16 v2, 0x12

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getSportScoreEntry()Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_b
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasTranslateEntry()Z

    move-result v2

    if-eqz v2, :cond_c

    const/16 v2, 0x13

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getTranslateEntry()Lcom/google/geo/sidekick/Sidekick$TranslateEntry;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_c
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasClockEntry()Z

    move-result v2

    if-eqz v2, :cond_d

    const/16 v2, 0x14

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getClockEntry()Lcom/google/geo/sidekick/Sidekick$ClockEntry;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_d
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasCurrencyExchangeEntry()Z

    move-result v2

    if-eqz v2, :cond_e

    const/16 v2, 0x15

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getCurrencyExchangeEntry()Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_e
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasIsExample()Z

    move-result v2

    if-eqz v2, :cond_f

    const/16 v2, 0x16

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getIsExample()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_f
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasNearbyPlaceEntry()Z

    move-result v2

    if-eqz v2, :cond_10

    const/16 v2, 0x17

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getNearbyPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_10
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasEncodedServerPayload()Z

    move-result v2

    if-eqz v2, :cond_11

    const/16 v2, 0x18

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getEncodedServerPayload()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBytes(ILcom/google/protobuf/micro/ByteStringMicro;)V

    :cond_11
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasPublicAlertEntry()Z

    move-result v2

    if-eqz v2, :cond_12

    const/16 v2, 0x19

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getPublicAlertEntry()Lcom/google/geo/sidekick/Sidekick$PublicAlertEntry;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_12
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasMovieListEntry()Z

    move-result v2

    if-eqz v2, :cond_13

    const/16 v2, 0x1a

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getMovieListEntry()Lcom/google/geo/sidekick/Sidekick$MovieListEntry;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_13
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasStockQuoteListEntry()Z

    move-result v2

    if-eqz v2, :cond_14

    const/16 v2, 0x1b

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getStockQuoteListEntry()Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_14
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasAttractionListEntry()Z

    move-result v2

    if-eqz v2, :cond_15

    const/16 v2, 0x1c

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getAttractionListEntry()Lcom/google/geo/sidekick/Sidekick$AttractionListEntry;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_15
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasPackageTrackingEntry()Z

    move-result v2

    if-eqz v2, :cond_16

    const/16 v2, 0x1d

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getPackageTrackingEntry()Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_16
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasNewsEntry()Z

    move-result v2

    if-eqz v2, :cond_17

    const/16 v2, 0x1e

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getNewsEntry()Lcom/google/geo/sidekick/Sidekick$NewsEntry;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_17
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasLocationHistoryReminderEntry()Z

    move-result v2

    if-eqz v2, :cond_18

    const/16 v2, 0x1f

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getLocationHistoryReminderEntry()Lcom/google/geo/sidekick/Sidekick$LocationHistoryReminderEntry;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_18
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasPhotoSpotEntry()Z

    move-result v2

    if-eqz v2, :cond_19

    const/16 v2, 0x20

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getPhotoSpotEntry()Lcom/google/geo/sidekick/Sidekick$PhotoSpotEntry;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_19
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getPrecacheDirectiveList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$PrecacheDirective;

    const/16 v2, 0x21

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_1

    :cond_1a
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasBirthdayCardEntry()Z

    move-result v2

    if-eqz v2, :cond_1b

    const/16 v2, 0x22

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getBirthdayCardEntry()Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1b
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasMovieEntry()Z

    move-result v2

    if-eqz v2, :cond_1c

    const/16 v2, 0x23

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getMovieEntry()Lcom/google/geo/sidekick/Sidekick$MovieEntry;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1c
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasEventEntry()Z

    move-result v2

    if-eqz v2, :cond_1d

    const/16 v2, 0x24

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getEventEntry()Lcom/google/geo/sidekick/Sidekick$EventEntry;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1d
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasResearchTopicEntry()Z

    move-result v2

    if-eqz v2, :cond_1e

    const/16 v2, 0x25

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getResearchTopicEntry()Lcom/google/geo/sidekick/Sidekick$ResearchTopicEntry;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1e
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasResearchPageEntry()Z

    move-result v2

    if-eqz v2, :cond_1f

    const/16 v2, 0x26

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getResearchPageEntry()Lcom/google/geo/sidekick/Sidekick$ResearchPageEntry;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1f
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasBarcodeEntry()Z

    move-result v2

    if-eqz v2, :cond_20

    const/16 v2, 0x27

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getBarcodeEntry()Lcom/google/geo/sidekick/Sidekick$BarcodeEntry;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_20
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasGmailEntry()Z

    move-result v2

    if-eqz v2, :cond_21

    const/16 v2, 0x28

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getGmailEntry()Lcom/google/geo/sidekick/Sidekick$GmailEntry;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_21
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasVisualSearchListEntry()Z

    move-result v2

    if-eqz v2, :cond_22

    const/16 v2, 0x29

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getVisualSearchListEntry()Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_22
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasVisualSearchEntry()Z

    move-result v2

    if-eqz v2, :cond_23

    const/16 v2, 0x2a

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getVisualSearchEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_23
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasNearbyPlacesListEntry()Z

    move-result v2

    if-eqz v2, :cond_24

    const/16 v2, 0x2b

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getNearbyPlacesListEntry()Lcom/google/geo/sidekick/Sidekick$NearbyPlacesListEntry;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_24
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasEventListEntry()Z

    move-result v2

    if-eqz v2, :cond_25

    const/16 v2, 0x2c

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getEventListEntry()Lcom/google/geo/sidekick/Sidekick$EventListEntry;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_25
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasUserPrompt()Z

    move-result v2

    if-eqz v2, :cond_26

    const/16 v2, 0x2d

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getUserPrompt()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_26
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasMovieTicketEntry()Z

    move-result v2

    if-eqz v2, :cond_27

    const/16 v2, 0x2e

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getMovieTicketEntry()Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_27
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasRealEstateEntry()Z

    move-result v2

    if-eqz v2, :cond_28

    const/16 v2, 0x2f

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getRealEstateEntry()Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_28
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasReminderEntry()Z

    move-result v2

    if-eqz v2, :cond_29

    const/16 v2, 0x30

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getReminderEntry()Lcom/google/geo/sidekick/Sidekick$ReminderEntry;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_29
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasWebsiteUpdateEntry()Z

    move-result v2

    if-eqz v2, :cond_2a

    const/16 v2, 0x31

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getWebsiteUpdateEntry()Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateEntry;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2a
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasWebsiteUpdateListEntry()Z

    move-result v2

    if-eqz v2, :cond_2b

    const/16 v2, 0x32

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getWebsiteUpdateListEntry()Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateListEntry;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2b
    return-void
.end method
