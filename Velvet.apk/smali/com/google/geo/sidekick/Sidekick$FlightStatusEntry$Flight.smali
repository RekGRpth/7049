.class public final Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Flight"
.end annotation


# instance fields
.field private airlineCode_:Ljava/lang/String;

.field private airlineName_:Ljava/lang/String;

.field private arrivalAirport_:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;

.field private arrivalGate_:Ljava/lang/String;

.field private arrivalTerminal_:Ljava/lang/String;

.field private arrivalTime_:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;

.field private cachedSize:I

.field private departureAirport_:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;

.field private departureGate_:Ljava/lang/String;

.field private departureTerminal_:Ljava/lang/String;

.field private departureTime_:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;

.field private detailsUrl_:Ljava/lang/String;

.field private diversionAirport_:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;

.field private diversionGate_:Ljava/lang/String;

.field private diversionTerminal_:Ljava/lang/String;

.field private flightNumber_:Ljava/lang/String;

.field private gmailReference_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$GmailReference;",
            ">;"
        }
    .end annotation
.end field

.field private hasAirlineCode:Z

.field private hasAirlineName:Z

.field private hasArrivalAirport:Z

.field private hasArrivalGate:Z

.field private hasArrivalTerminal:Z

.field private hasArrivalTime:Z

.field private hasDepartureAirport:Z

.field private hasDepartureGate:Z

.field private hasDepartureTerminal:Z

.field private hasDepartureTime:Z

.field private hasDetailsUrl:Z

.field private hasDiversionAirport:Z

.field private hasDiversionGate:Z

.field private hasDiversionTerminal:Z

.field private hasFlightNumber:Z

.field private hasLastUpdatedSecondsSinceEpoch:Z

.field private hasNotificationDetails:Z

.field private hasStatus:Z

.field private hasStatusCode:Z

.field private lastUpdatedSecondsSinceEpoch_:J

.field private notificationDetails_:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$NotificationDetails;

.field private statusCode_:I

.field private status_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->statusCode_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->status_:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->lastUpdatedSecondsSinceEpoch_:J

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->airlineCode_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->airlineName_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->flightNumber_:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->departureAirport_:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->departureTime_:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->departureTerminal_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->departureGate_:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->arrivalAirport_:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->arrivalTime_:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->arrivalTerminal_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->arrivalGate_:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->diversionAirport_:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->diversionTerminal_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->diversionGate_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->detailsUrl_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->gmailReference_:Ljava/util/List;

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->notificationDetails_:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$NotificationDetails;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addGmailReference(Lcom/google/geo/sidekick/Sidekick$GmailReference;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$GmailReference;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->gmailReference_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->gmailReference_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->gmailReference_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getAirlineCode()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->airlineCode_:Ljava/lang/String;

    return-object v0
.end method

.method public getAirlineName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->airlineName_:Ljava/lang/String;

    return-object v0
.end method

.method public getArrivalAirport()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->arrivalAirport_:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;

    return-object v0
.end method

.method public getArrivalGate()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->arrivalGate_:Ljava/lang/String;

    return-object v0
.end method

.method public getArrivalTerminal()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->arrivalTerminal_:Ljava/lang/String;

    return-object v0
.end method

.method public getArrivalTime()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->arrivalTime_:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->cachedSize:I

    return v0
.end method

.method public getDepartureAirport()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->departureAirport_:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;

    return-object v0
.end method

.method public getDepartureGate()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->departureGate_:Ljava/lang/String;

    return-object v0
.end method

.method public getDepartureTerminal()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->departureTerminal_:Ljava/lang/String;

    return-object v0
.end method

.method public getDepartureTime()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->departureTime_:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;

    return-object v0
.end method

.method public getDetailsUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->detailsUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getDiversionAirport()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->diversionAirport_:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;

    return-object v0
.end method

.method public getDiversionGate()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->diversionGate_:Ljava/lang/String;

    return-object v0
.end method

.method public getDiversionTerminal()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->diversionTerminal_:Ljava/lang/String;

    return-object v0
.end method

.method public getFlightNumber()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->flightNumber_:Ljava/lang/String;

    return-object v0
.end method

.method public getGmailReferenceCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->gmailReference_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getGmailReferenceList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$GmailReference;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->gmailReference_:Ljava/util/List;

    return-object v0
.end method

.method public getLastUpdatedSecondsSinceEpoch()J
    .locals 2

    iget-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->lastUpdatedSecondsSinceEpoch_:J

    return-wide v0
.end method

.method public getNotificationDetails()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$NotificationDetails;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->notificationDetails_:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$NotificationDetails;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasStatusCode()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getStatusCode()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasStatus()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getStatus()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasLastUpdatedSecondsSinceEpoch()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getLastUpdatedSecondsSinceEpoch()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasAirlineCode()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getAirlineCode()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasAirlineName()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getAirlineName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasDepartureAirport()Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getDepartureAirport()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasDepartureTime()Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v3, 0x7

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getDepartureTime()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_6
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasDepartureTerminal()Z

    move-result v3

    if-eqz v3, :cond_7

    const/16 v3, 0x8

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getDepartureTerminal()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_7
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasDepartureGate()Z

    move-result v3

    if-eqz v3, :cond_8

    const/16 v3, 0x9

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getDepartureGate()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_8
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasArrivalAirport()Z

    move-result v3

    if-eqz v3, :cond_9

    const/16 v3, 0xa

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getArrivalAirport()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_9
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasArrivalTime()Z

    move-result v3

    if-eqz v3, :cond_a

    const/16 v3, 0xb

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getArrivalTime()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_a
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasArrivalTerminal()Z

    move-result v3

    if-eqz v3, :cond_b

    const/16 v3, 0xc

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getArrivalTerminal()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_b
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasArrivalGate()Z

    move-result v3

    if-eqz v3, :cond_c

    const/16 v3, 0xd

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getArrivalGate()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_c
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasDiversionAirport()Z

    move-result v3

    if-eqz v3, :cond_d

    const/16 v3, 0xe

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getDiversionAirport()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_d
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasDiversionTerminal()Z

    move-result v3

    if-eqz v3, :cond_e

    const/16 v3, 0xf

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getDiversionTerminal()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_e
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasDiversionGate()Z

    move-result v3

    if-eqz v3, :cond_f

    const/16 v3, 0x10

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getDiversionGate()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_f
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasFlightNumber()Z

    move-result v3

    if-eqz v3, :cond_10

    const/16 v3, 0x11

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getFlightNumber()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_10
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasDetailsUrl()Z

    move-result v3

    if-eqz v3, :cond_11

    const/16 v3, 0x12

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getDetailsUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_11
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getGmailReferenceList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_12

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$GmailReference;

    const/16 v3, 0x13

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_12
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasNotificationDetails()Z

    move-result v3

    if-eqz v3, :cond_13

    const/16 v3, 0x14

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getNotificationDetails()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$NotificationDetails;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_13
    iput v2, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->cachedSize:I

    return v2
.end method

.method public getStatus()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->status_:Ljava/lang/String;

    return-object v0
.end method

.method public getStatusCode()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->statusCode_:I

    return v0
.end method

.method public hasAirlineCode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasAirlineCode:Z

    return v0
.end method

.method public hasAirlineName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasAirlineName:Z

    return v0
.end method

.method public hasArrivalAirport()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasArrivalAirport:Z

    return v0
.end method

.method public hasArrivalGate()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasArrivalGate:Z

    return v0
.end method

.method public hasArrivalTerminal()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasArrivalTerminal:Z

    return v0
.end method

.method public hasArrivalTime()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasArrivalTime:Z

    return v0
.end method

.method public hasDepartureAirport()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasDepartureAirport:Z

    return v0
.end method

.method public hasDepartureGate()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasDepartureGate:Z

    return v0
.end method

.method public hasDepartureTerminal()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasDepartureTerminal:Z

    return v0
.end method

.method public hasDepartureTime()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasDepartureTime:Z

    return v0
.end method

.method public hasDetailsUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasDetailsUrl:Z

    return v0
.end method

.method public hasDiversionAirport()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasDiversionAirport:Z

    return v0
.end method

.method public hasDiversionGate()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasDiversionGate:Z

    return v0
.end method

.method public hasDiversionTerminal()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasDiversionTerminal:Z

    return v0
.end method

.method public hasFlightNumber()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasFlightNumber:Z

    return v0
.end method

.method public hasLastUpdatedSecondsSinceEpoch()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasLastUpdatedSecondsSinceEpoch:Z

    return v0
.end method

.method public hasNotificationDetails()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasNotificationDetails:Z

    return v0
.end method

.method public hasStatus()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasStatus:Z

    return v0
.end method

.method public hasStatusCode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasStatusCode:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->setStatusCode(I)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->setStatus(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->setLastUpdatedSecondsSinceEpoch(J)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->setAirlineCode(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->setAirlineName(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    goto :goto_0

    :sswitch_6
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->setDepartureAirport(Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    goto :goto_0

    :sswitch_7
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->setDepartureTime(Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->setDepartureTerminal(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->setDepartureGate(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    goto :goto_0

    :sswitch_a
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->setArrivalAirport(Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    goto :goto_0

    :sswitch_b
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->setArrivalTime(Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->setArrivalTerminal(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->setArrivalGate(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    goto/16 :goto_0

    :sswitch_e
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->setDiversionAirport(Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->setDiversionTerminal(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->setDiversionGate(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->setFlightNumber(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->setDetailsUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    goto/16 :goto_0

    :sswitch_13
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$GmailReference;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$GmailReference;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->addGmailReference(Lcom/google/geo/sidekick/Sidekick$GmailReference;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    goto/16 :goto_0

    :sswitch_14
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$NotificationDetails;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$NotificationDetails;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->setNotificationDetails(Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$NotificationDetails;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
        0x8a -> :sswitch_11
        0x92 -> :sswitch_12
        0x9a -> :sswitch_13
        0xa2 -> :sswitch_14
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    move-result-object v0

    return-object v0
.end method

.method public setAirlineCode(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasAirlineCode:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->airlineCode_:Ljava/lang/String;

    return-object p0
.end method

.method public setAirlineName(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasAirlineName:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->airlineName_:Ljava/lang/String;

    return-object p0
.end method

.method public setArrivalAirport(Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasArrivalAirport:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->arrivalAirport_:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;

    return-object p0
.end method

.method public setArrivalGate(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasArrivalGate:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->arrivalGate_:Ljava/lang/String;

    return-object p0
.end method

.method public setArrivalTerminal(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasArrivalTerminal:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->arrivalTerminal_:Ljava/lang/String;

    return-object p0
.end method

.method public setArrivalTime(Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasArrivalTime:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->arrivalTime_:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;

    return-object p0
.end method

.method public setDepartureAirport(Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasDepartureAirport:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->departureAirport_:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;

    return-object p0
.end method

.method public setDepartureGate(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasDepartureGate:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->departureGate_:Ljava/lang/String;

    return-object p0
.end method

.method public setDepartureTerminal(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasDepartureTerminal:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->departureTerminal_:Ljava/lang/String;

    return-object p0
.end method

.method public setDepartureTime(Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasDepartureTime:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->departureTime_:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;

    return-object p0
.end method

.method public setDetailsUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasDetailsUrl:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->detailsUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setDiversionAirport(Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasDiversionAirport:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->diversionAirport_:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;

    return-object p0
.end method

.method public setDiversionGate(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasDiversionGate:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->diversionGate_:Ljava/lang/String;

    return-object p0
.end method

.method public setDiversionTerminal(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasDiversionTerminal:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->diversionTerminal_:Ljava/lang/String;

    return-object p0
.end method

.method public setFlightNumber(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasFlightNumber:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->flightNumber_:Ljava/lang/String;

    return-object p0
.end method

.method public setLastUpdatedSecondsSinceEpoch(J)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasLastUpdatedSecondsSinceEpoch:Z

    iput-wide p1, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->lastUpdatedSecondsSinceEpoch_:J

    return-object p0
.end method

.method public setNotificationDetails(Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$NotificationDetails;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$NotificationDetails;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasNotificationDetails:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->notificationDetails_:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$NotificationDetails;

    return-object p0
.end method

.method public setStatus(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasStatus:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->status_:Ljava/lang/String;

    return-object p0
.end method

.method public setStatusCode(I)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasStatusCode:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->statusCode_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 5
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasStatusCode()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getStatusCode()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasStatus()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getStatus()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasLastUpdatedSecondsSinceEpoch()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getLastUpdatedSecondsSinceEpoch()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasAirlineCode()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getAirlineCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasAirlineName()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getAirlineName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasDepartureAirport()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getDepartureAirport()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasDepartureTime()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getDepartureTime()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasDepartureTerminal()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getDepartureTerminal()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasDepartureGate()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getDepartureGate()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasArrivalAirport()Z

    move-result v2

    if-eqz v2, :cond_9

    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getArrivalAirport()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasArrivalTime()Z

    move-result v2

    if-eqz v2, :cond_a

    const/16 v2, 0xb

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getArrivalTime()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasArrivalTerminal()Z

    move-result v2

    if-eqz v2, :cond_b

    const/16 v2, 0xc

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getArrivalTerminal()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_b
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasArrivalGate()Z

    move-result v2

    if-eqz v2, :cond_c

    const/16 v2, 0xd

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getArrivalGate()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_c
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasDiversionAirport()Z

    move-result v2

    if-eqz v2, :cond_d

    const/16 v2, 0xe

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getDiversionAirport()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_d
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasDiversionTerminal()Z

    move-result v2

    if-eqz v2, :cond_e

    const/16 v2, 0xf

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getDiversionTerminal()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_e
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasDiversionGate()Z

    move-result v2

    if-eqz v2, :cond_f

    const/16 v2, 0x10

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getDiversionGate()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_f
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasFlightNumber()Z

    move-result v2

    if-eqz v2, :cond_10

    const/16 v2, 0x11

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getFlightNumber()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_10
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasDetailsUrl()Z

    move-result v2

    if-eqz v2, :cond_11

    const/16 v2, 0x12

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getDetailsUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_11
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getGmailReferenceList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_12

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$GmailReference;

    const/16 v2, 0x13

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_12
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasNotificationDetails()Z

    move-result v2

    if-eqz v2, :cond_13

    const/16 v2, 0x14

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getNotificationDetails()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$NotificationDetails;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_13
    return-void
.end method
