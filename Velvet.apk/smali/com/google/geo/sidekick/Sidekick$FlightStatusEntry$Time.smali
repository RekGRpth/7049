.class public final Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Time"
.end annotation


# instance fields
.field private actualTimeSecondsSinceEpoch_:J

.field private cachedSize:I

.field private hasActualTimeSecondsSinceEpoch:Z

.field private hasScheduledTimeSecondsSinceEpoch:Z

.field private hasTimeZoneId:Z

.field private hasTimeZoneOffsetSeconds:Z

.field private scheduledTimeSecondsSinceEpoch_:J

.field private timeZoneId_:Ljava/lang/String;

.field private timeZoneOffsetSeconds_:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const-wide/16 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->scheduledTimeSecondsSinceEpoch_:J

    iput-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->actualTimeSecondsSinceEpoch_:J

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->timeZoneOffsetSeconds_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->timeZoneId_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getActualTimeSecondsSinceEpoch()J
    .locals 2

    iget-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->actualTimeSecondsSinceEpoch_:J

    return-wide v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->cachedSize:I

    return v0
.end method

.method public getScheduledTimeSecondsSinceEpoch()J
    .locals 2

    iget-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->scheduledTimeSecondsSinceEpoch_:J

    return-wide v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->hasScheduledTimeSecondsSinceEpoch()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->getScheduledTimeSecondsSinceEpoch()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->hasActualTimeSecondsSinceEpoch()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->getActualTimeSecondsSinceEpoch()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->hasTimeZoneOffsetSeconds()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->getTimeZoneOffsetSeconds()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->hasTimeZoneId()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->getTimeZoneId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->cachedSize:I

    return v0
.end method

.method public getTimeZoneId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->timeZoneId_:Ljava/lang/String;

    return-object v0
.end method

.method public getTimeZoneOffsetSeconds()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->timeZoneOffsetSeconds_:I

    return v0
.end method

.method public hasActualTimeSecondsSinceEpoch()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->hasActualTimeSecondsSinceEpoch:Z

    return v0
.end method

.method public hasScheduledTimeSecondsSinceEpoch()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->hasScheduledTimeSecondsSinceEpoch:Z

    return v0
.end method

.method public hasTimeZoneId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->hasTimeZoneId:Z

    return v0
.end method

.method public hasTimeZoneOffsetSeconds()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->hasTimeZoneOffsetSeconds:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->setScheduledTimeSecondsSinceEpoch(J)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->setActualTimeSecondsSinceEpoch(J)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->setTimeZoneOffsetSeconds(I)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->setTimeZoneId(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;

    move-result-object v0

    return-object v0
.end method

.method public setActualTimeSecondsSinceEpoch(J)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->hasActualTimeSecondsSinceEpoch:Z

    iput-wide p1, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->actualTimeSecondsSinceEpoch_:J

    return-object p0
.end method

.method public setScheduledTimeSecondsSinceEpoch(J)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->hasScheduledTimeSecondsSinceEpoch:Z

    iput-wide p1, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->scheduledTimeSecondsSinceEpoch_:J

    return-object p0
.end method

.method public setTimeZoneId(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->hasTimeZoneId:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->timeZoneId_:Ljava/lang/String;

    return-object p0
.end method

.method public setTimeZoneOffsetSeconds(I)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->hasTimeZoneOffsetSeconds:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->timeZoneOffsetSeconds_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->hasScheduledTimeSecondsSinceEpoch()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->getScheduledTimeSecondsSinceEpoch()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->hasActualTimeSecondsSinceEpoch()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->getActualTimeSecondsSinceEpoch()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->hasTimeZoneOffsetSeconds()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->getTimeZoneOffsetSeconds()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->hasTimeZoneId()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->getTimeZoneId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    return-void
.end method
