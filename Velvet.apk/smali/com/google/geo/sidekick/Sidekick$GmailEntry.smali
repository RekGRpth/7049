.class public final Lcom/google/geo/sidekick/Sidekick$GmailEntry;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GmailEntry"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;,
        Lcom/google/geo/sidekick/Sidekick$GmailEntry$Contact;
    }
.end annotation


# instance fields
.field private cachedSize:I

.field private gmailUser_:Lcom/google/geo/sidekick/Sidekick$GmailEntry$Contact;

.field private hasGmailUser:Z

.field private hasNumUnreadEmails:Z

.field private hasProfilePhoto:Z

.field private hasUserTimezoneId:Z

.field private messages_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;",
            ">;"
        }
    .end annotation
.end field

.field private numUnreadEmails_:J

.field private profilePhoto_:Lcom/google/geo/sidekick/Sidekick$Photo;

.field private userTimezoneId_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->profilePhoto_:Lcom/google/geo/sidekick/Sidekick$Photo;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->numUnreadEmails_:J

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->gmailUser_:Lcom/google/geo/sidekick/Sidekick$GmailEntry$Contact;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->userTimezoneId_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->messages_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addMessages(Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;)Lcom/google/geo/sidekick/Sidekick$GmailEntry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->messages_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->messages_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->messages_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->cachedSize:I

    return v0
.end method

.method public getGmailUser()Lcom/google/geo/sidekick/Sidekick$GmailEntry$Contact;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->gmailUser_:Lcom/google/geo/sidekick/Sidekick$GmailEntry$Contact;

    return-object v0
.end method

.method public getMessagesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->messages_:Ljava/util/List;

    return-object v0
.end method

.method public getNumUnreadEmails()J
    .locals 2

    iget-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->numUnreadEmails_:J

    return-wide v0
.end method

.method public getProfilePhoto()Lcom/google/geo/sidekick/Sidekick$Photo;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->profilePhoto_:Lcom/google/geo/sidekick/Sidekick$Photo;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->hasProfilePhoto()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->getProfilePhoto()Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->hasNumUnreadEmails()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->getNumUnreadEmails()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->getMessagesList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;

    const/4 v3, 0x3

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->hasGmailUser()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->getGmailUser()Lcom/google/geo/sidekick/Sidekick$GmailEntry$Contact;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->hasUserTimezoneId()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->getUserTimezoneId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_4
    iput v2, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->cachedSize:I

    return v2
.end method

.method public getUserTimezoneId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->userTimezoneId_:Ljava/lang/String;

    return-object v0
.end method

.method public hasGmailUser()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->hasGmailUser:Z

    return v0
.end method

.method public hasNumUnreadEmails()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->hasNumUnreadEmails:Z

    return v0
.end method

.method public hasProfilePhoto()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->hasProfilePhoto:Z

    return v0
.end method

.method public hasUserTimezoneId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->hasUserTimezoneId:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$GmailEntry;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$Photo;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$Photo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->setProfilePhoto(Lcom/google/geo/sidekick/Sidekick$Photo;)Lcom/google/geo/sidekick/Sidekick$GmailEntry;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->setNumUnreadEmails(J)Lcom/google/geo/sidekick/Sidekick$GmailEntry;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->addMessages(Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;)Lcom/google/geo/sidekick/Sidekick$GmailEntry;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$GmailEntry$Contact;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$GmailEntry$Contact;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->setGmailUser(Lcom/google/geo/sidekick/Sidekick$GmailEntry$Contact;)Lcom/google/geo/sidekick/Sidekick$GmailEntry;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->setUserTimezoneId(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$GmailEntry;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$GmailEntry;

    move-result-object v0

    return-object v0
.end method

.method public setGmailUser(Lcom/google/geo/sidekick/Sidekick$GmailEntry$Contact;)Lcom/google/geo/sidekick/Sidekick$GmailEntry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$GmailEntry$Contact;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->hasGmailUser:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->gmailUser_:Lcom/google/geo/sidekick/Sidekick$GmailEntry$Contact;

    return-object p0
.end method

.method public setNumUnreadEmails(J)Lcom/google/geo/sidekick/Sidekick$GmailEntry;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->hasNumUnreadEmails:Z

    iput-wide p1, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->numUnreadEmails_:J

    return-object p0
.end method

.method public setProfilePhoto(Lcom/google/geo/sidekick/Sidekick$Photo;)Lcom/google/geo/sidekick/Sidekick$GmailEntry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Photo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->hasProfilePhoto:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->profilePhoto_:Lcom/google/geo/sidekick/Sidekick$Photo;

    return-object p0
.end method

.method public setUserTimezoneId(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$GmailEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->hasUserTimezoneId:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->userTimezoneId_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 5
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->hasProfilePhoto()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->getProfilePhoto()Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->hasNumUnreadEmails()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->getNumUnreadEmails()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->getMessagesList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$GmailEntry$GmailMessageDescription;

    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->hasGmailUser()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->getGmailUser()Lcom/google/geo/sidekick/Sidekick$GmailEntry$Contact;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->hasUserTimezoneId()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$GmailEntry;->getUserTimezoneId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    return-void
.end method
