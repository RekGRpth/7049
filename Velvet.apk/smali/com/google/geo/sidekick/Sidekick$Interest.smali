.class public final Lcom/google/geo/sidekick/Sidekick$Interest;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Interest"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/geo/sidekick/Sidekick$Interest$TimeSpaceRegion;
    }
.end annotation


# instance fields
.field private cachedSize:I

.field private constraintLevel_:I

.field private encodedServerPayload_:Lcom/google/protobuf/micro/ByteStringMicro;

.field private entryTypeRestrict_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private exampleData_:Z

.field private hasConstraintLevel:Z

.field private hasEncodedServerPayload:Z

.field private hasExampleData:Z

.field private hasIncludeStricterConstraintResponses:Z

.field private hasMaxEntriesTotalToProduce:Z

.field private hasNumItemsToFetch:Z

.field private hasOffset:Z

.field private hasTargetDisplay:Z

.field private includeStricterConstraintResponses_:Z

.field private maxEntriesTotalToProduce_:I

.field private numItemsToFetch_:I

.field private offset_:I

.field private targetDisplay_:I

.field private timeSpaceRegion_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Interest$TimeSpaceRegion;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->targetDisplay_:I

    iput v1, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->maxEntriesTotalToProduce_:I

    iput v1, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->numItemsToFetch_:I

    iput v1, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->offset_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->timeSpaceRegion_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->entryTypeRestrict_:Ljava/util/List;

    iput-boolean v1, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->exampleData_:Z

    iput v1, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->constraintLevel_:I

    iput-boolean v1, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->includeStricterConstraintResponses_:Z

    sget-object v0, Lcom/google/protobuf/micro/ByteStringMicro;->EMPTY:Lcom/google/protobuf/micro/ByteStringMicro;

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->encodedServerPayload_:Lcom/google/protobuf/micro/ByteStringMicro;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->cachedSize:I

    return-void
.end method

.method public static parseFrom([B)Lcom/google/geo/sidekick/Sidekick$Interest;
    .locals 1
    .param p0    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;
        }
    .end annotation

    new-instance v0, Lcom/google/geo/sidekick/Sidekick$Interest;

    invoke-direct {v0}, Lcom/google/geo/sidekick/Sidekick$Interest;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Interest;

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Interest;

    return-object v0
.end method


# virtual methods
.method public addEntryTypeRestrict(I)Lcom/google/geo/sidekick/Sidekick$Interest;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->entryTypeRestrict_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->entryTypeRestrict_:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->entryTypeRestrict_:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addTimeSpaceRegion(Lcom/google/geo/sidekick/Sidekick$Interest$TimeSpaceRegion;)Lcom/google/geo/sidekick/Sidekick$Interest;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Interest$TimeSpaceRegion;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->timeSpaceRegion_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->timeSpaceRegion_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->timeSpaceRegion_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final clear()Lcom/google/geo/sidekick/Sidekick$Interest;
    .locals 1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->clearTargetDisplay()Lcom/google/geo/sidekick/Sidekick$Interest;

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->clearMaxEntriesTotalToProduce()Lcom/google/geo/sidekick/Sidekick$Interest;

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->clearNumItemsToFetch()Lcom/google/geo/sidekick/Sidekick$Interest;

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->clearOffset()Lcom/google/geo/sidekick/Sidekick$Interest;

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->clearTimeSpaceRegion()Lcom/google/geo/sidekick/Sidekick$Interest;

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->clearEntryTypeRestrict()Lcom/google/geo/sidekick/Sidekick$Interest;

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->clearExampleData()Lcom/google/geo/sidekick/Sidekick$Interest;

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->clearConstraintLevel()Lcom/google/geo/sidekick/Sidekick$Interest;

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->clearIncludeStricterConstraintResponses()Lcom/google/geo/sidekick/Sidekick$Interest;

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->clearEncodedServerPayload()Lcom/google/geo/sidekick/Sidekick$Interest;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->cachedSize:I

    return-object p0
.end method

.method public clearConstraintLevel()Lcom/google/geo/sidekick/Sidekick$Interest;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->hasConstraintLevel:Z

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->constraintLevel_:I

    return-object p0
.end method

.method public clearEncodedServerPayload()Lcom/google/geo/sidekick/Sidekick$Interest;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->hasEncodedServerPayload:Z

    sget-object v0, Lcom/google/protobuf/micro/ByteStringMicro;->EMPTY:Lcom/google/protobuf/micro/ByteStringMicro;

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->encodedServerPayload_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object p0
.end method

.method public clearEntryTypeRestrict()Lcom/google/geo/sidekick/Sidekick$Interest;
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->entryTypeRestrict_:Ljava/util/List;

    return-object p0
.end method

.method public clearExampleData()Lcom/google/geo/sidekick/Sidekick$Interest;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->hasExampleData:Z

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->exampleData_:Z

    return-object p0
.end method

.method public clearIncludeStricterConstraintResponses()Lcom/google/geo/sidekick/Sidekick$Interest;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->hasIncludeStricterConstraintResponses:Z

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->includeStricterConstraintResponses_:Z

    return-object p0
.end method

.method public clearMaxEntriesTotalToProduce()Lcom/google/geo/sidekick/Sidekick$Interest;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->hasMaxEntriesTotalToProduce:Z

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->maxEntriesTotalToProduce_:I

    return-object p0
.end method

.method public clearNumItemsToFetch()Lcom/google/geo/sidekick/Sidekick$Interest;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->hasNumItemsToFetch:Z

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->numItemsToFetch_:I

    return-object p0
.end method

.method public clearOffset()Lcom/google/geo/sidekick/Sidekick$Interest;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->hasOffset:Z

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->offset_:I

    return-object p0
.end method

.method public clearTargetDisplay()Lcom/google/geo/sidekick/Sidekick$Interest;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->hasTargetDisplay:Z

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->targetDisplay_:I

    return-object p0
.end method

.method public clearTimeSpaceRegion()Lcom/google/geo/sidekick/Sidekick$Interest;
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->timeSpaceRegion_:Ljava/util/List;

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->cachedSize:I

    return v0
.end method

.method public getConstraintLevel()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->constraintLevel_:I

    return v0
.end method

.method public getEncodedServerPayload()Lcom/google/protobuf/micro/ByteStringMicro;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->encodedServerPayload_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object v0
.end method

.method public getEntryTypeRestrictList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->entryTypeRestrict_:Ljava/util/List;

    return-object v0
.end method

.method public getExampleData()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->exampleData_:Z

    return v0
.end method

.method public getIncludeStricterConstraintResponses()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->includeStricterConstraintResponses_:Z

    return v0
.end method

.method public getMaxEntriesTotalToProduce()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->maxEntriesTotalToProduce_:I

    return v0
.end method

.method public getNumItemsToFetch()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->numItemsToFetch_:I

    return v0
.end method

.method public getOffset()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->offset_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 6

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->hasTargetDisplay()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->getTargetDisplay()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->hasMaxEntriesTotalToProduce()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->getMaxEntriesTotalToProduce()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->hasNumItemsToFetch()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->getNumItemsToFetch()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->hasOffset()Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->getOffset()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->getTimeSpaceRegionList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/geo/sidekick/Sidekick$Interest$TimeSpaceRegion;

    const/4 v4, 0x5

    invoke-static {v4, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->getEntryTypeRestrictList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32SizeNoTag(I)I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_1

    :cond_5
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->getEntryTypeRestrictList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->hasConstraintLevel()Z

    move-result v4

    if-eqz v4, :cond_6

    const/4 v4, 0x7

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->getConstraintLevel()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_6
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->hasExampleData()Z

    move-result v4

    if-eqz v4, :cond_7

    const/16 v4, 0x8

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->getExampleData()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_7
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->hasIncludeStricterConstraintResponses()Z

    move-result v4

    if-eqz v4, :cond_8

    const/16 v4, 0x9

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->getIncludeStricterConstraintResponses()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_8
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->hasEncodedServerPayload()Z

    move-result v4

    if-eqz v4, :cond_9

    const/16 v4, 0xa

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->getEncodedServerPayload()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBytesSize(ILcom/google/protobuf/micro/ByteStringMicro;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_9
    iput v3, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->cachedSize:I

    return v3
.end method

.method public getTargetDisplay()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->targetDisplay_:I

    return v0
.end method

.method public getTimeSpaceRegionList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Interest$TimeSpaceRegion;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->timeSpaceRegion_:Ljava/util/List;

    return-object v0
.end method

.method public hasConstraintLevel()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->hasConstraintLevel:Z

    return v0
.end method

.method public hasEncodedServerPayload()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->hasEncodedServerPayload:Z

    return v0
.end method

.method public hasExampleData()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->hasExampleData:Z

    return v0
.end method

.method public hasIncludeStricterConstraintResponses()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->hasIncludeStricterConstraintResponses:Z

    return v0
.end method

.method public hasMaxEntriesTotalToProduce()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->hasMaxEntriesTotalToProduce:Z

    return v0
.end method

.method public hasNumItemsToFetch()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->hasNumItemsToFetch:Z

    return v0
.end method

.method public hasOffset()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->hasOffset:Z

    return v0
.end method

.method public hasTargetDisplay()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->hasTargetDisplay:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$Interest;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$Interest;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$Interest;->setTargetDisplay(I)Lcom/google/geo/sidekick/Sidekick$Interest;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$Interest;->setMaxEntriesTotalToProduce(I)Lcom/google/geo/sidekick/Sidekick$Interest;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$Interest;->setNumItemsToFetch(I)Lcom/google/geo/sidekick/Sidekick$Interest;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$Interest;->setOffset(I)Lcom/google/geo/sidekick/Sidekick$Interest;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$Interest$TimeSpaceRegion;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$Interest$TimeSpaceRegion;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Interest;->addTimeSpaceRegion(Lcom/google/geo/sidekick/Sidekick$Interest$TimeSpaceRegion;)Lcom/google/geo/sidekick/Sidekick$Interest;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$Interest;->addEntryTypeRestrict(I)Lcom/google/geo/sidekick/Sidekick$Interest;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$Interest;->setConstraintLevel(I)Lcom/google/geo/sidekick/Sidekick$Interest;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$Interest;->setExampleData(Z)Lcom/google/geo/sidekick/Sidekick$Interest;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$Interest;->setIncludeStricterConstraintResponses(Z)Lcom/google/geo/sidekick/Sidekick$Interest;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBytes()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$Interest;->setEncodedServerPayload(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/geo/sidekick/Sidekick$Interest;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$Interest;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$Interest;

    move-result-object v0

    return-object v0
.end method

.method public setConstraintLevel(I)Lcom/google/geo/sidekick/Sidekick$Interest;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->hasConstraintLevel:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->constraintLevel_:I

    return-object p0
.end method

.method public setEncodedServerPayload(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/geo/sidekick/Sidekick$Interest;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/ByteStringMicro;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->hasEncodedServerPayload:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->encodedServerPayload_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object p0
.end method

.method public setExampleData(Z)Lcom/google/geo/sidekick/Sidekick$Interest;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->hasExampleData:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->exampleData_:Z

    return-object p0
.end method

.method public setIncludeStricterConstraintResponses(Z)Lcom/google/geo/sidekick/Sidekick$Interest;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->hasIncludeStricterConstraintResponses:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->includeStricterConstraintResponses_:Z

    return-object p0
.end method

.method public setMaxEntriesTotalToProduce(I)Lcom/google/geo/sidekick/Sidekick$Interest;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->hasMaxEntriesTotalToProduce:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->maxEntriesTotalToProduce_:I

    return-object p0
.end method

.method public setNumItemsToFetch(I)Lcom/google/geo/sidekick/Sidekick$Interest;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->hasNumItemsToFetch:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->numItemsToFetch_:I

    return-object p0
.end method

.method public setOffset(I)Lcom/google/geo/sidekick/Sidekick$Interest;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->hasOffset:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->offset_:I

    return-object p0
.end method

.method public setTargetDisplay(I)Lcom/google/geo/sidekick/Sidekick$Interest;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->hasTargetDisplay:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$Interest;->targetDisplay_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->hasTargetDisplay()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->getTargetDisplay()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->hasMaxEntriesTotalToProduce()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->getMaxEntriesTotalToProduce()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->hasNumItemsToFetch()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->getNumItemsToFetch()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->hasOffset()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->getOffset()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->getTimeSpaceRegionList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Interest$TimeSpaceRegion;

    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->getEntryTypeRestrictList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    const/4 v2, 0x6

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    goto :goto_1

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->hasConstraintLevel()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->getConstraintLevel()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->hasExampleData()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->getExampleData()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->hasIncludeStricterConstraintResponses()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->getIncludeStricterConstraintResponses()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->hasEncodedServerPayload()Z

    move-result v2

    if-eqz v2, :cond_9

    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Interest;->getEncodedServerPayload()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBytes(ILcom/google/protobuf/micro/ByteStringMicro;)V

    :cond_9
    return-void
.end method
