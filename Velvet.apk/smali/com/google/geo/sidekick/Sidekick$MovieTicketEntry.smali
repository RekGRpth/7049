.class public final Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MovieTicketEntry"
.end annotation


# instance fields
.field private barcode_:Lcom/google/geo/sidekick/Sidekick$Photo;

.field private cachedSize:I

.field private confirmationNumber_:Ljava/lang/String;

.field private gmailReference_:Lcom/google/geo/sidekick/Sidekick$GmailReference;

.field private hasBarcode:Z

.field private hasConfirmationNumber:Z

.field private hasGmailReference:Z

.field private hasMovie:Z

.field private hasNumberOfTickets:Z

.field private hasRoute:Z

.field private hasShowtimeSeconds:Z

.field private hasTheater:Z

.field private movie_:Lcom/google/geo/sidekick/Sidekick$Movie;

.field private numberOfTickets_:I

.field private route_:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

.field private showtimeSeconds_:J

.field private theater_:Lcom/google/geo/sidekick/Sidekick$Location;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->movie_:Lcom/google/geo/sidekick/Sidekick$Movie;

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->theater_:Lcom/google/geo/sidekick/Sidekick$Location;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->showtimeSeconds_:J

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->route_:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->gmailReference_:Lcom/google/geo/sidekick/Sidekick$GmailReference;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->numberOfTickets_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->confirmationNumber_:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->barcode_:Lcom/google/geo/sidekick/Sidekick$Photo;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getBarcode()Lcom/google/geo/sidekick/Sidekick$Photo;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->barcode_:Lcom/google/geo/sidekick/Sidekick$Photo;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->cachedSize:I

    return v0
.end method

.method public getConfirmationNumber()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->confirmationNumber_:Ljava/lang/String;

    return-object v0
.end method

.method public getGmailReference()Lcom/google/geo/sidekick/Sidekick$GmailReference;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->gmailReference_:Lcom/google/geo/sidekick/Sidekick$GmailReference;

    return-object v0
.end method

.method public getMovie()Lcom/google/geo/sidekick/Sidekick$Movie;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->movie_:Lcom/google/geo/sidekick/Sidekick$Movie;

    return-object v0
.end method

.method public getNumberOfTickets()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->numberOfTickets_:I

    return v0
.end method

.method public getRoute()Lcom/google/geo/sidekick/Sidekick$CommuteSummary;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->route_:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->hasMovie()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->getMovie()Lcom/google/geo/sidekick/Sidekick$Movie;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->hasTheater()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->getTheater()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->hasShowtimeSeconds()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->getShowtimeSeconds()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->hasRoute()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->getRoute()Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->hasGmailReference()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->getGmailReference()Lcom/google/geo/sidekick/Sidekick$GmailReference;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->hasNumberOfTickets()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->getNumberOfTickets()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->hasConfirmationNumber()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->getConfirmationNumber()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->hasBarcode()Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->getBarcode()Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->cachedSize:I

    return v0
.end method

.method public getShowtimeSeconds()J
    .locals 2

    iget-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->showtimeSeconds_:J

    return-wide v0
.end method

.method public getTheater()Lcom/google/geo/sidekick/Sidekick$Location;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->theater_:Lcom/google/geo/sidekick/Sidekick$Location;

    return-object v0
.end method

.method public hasBarcode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->hasBarcode:Z

    return v0
.end method

.method public hasConfirmationNumber()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->hasConfirmationNumber:Z

    return v0
.end method

.method public hasGmailReference()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->hasGmailReference:Z

    return v0
.end method

.method public hasMovie()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->hasMovie:Z

    return v0
.end method

.method public hasNumberOfTickets()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->hasNumberOfTickets:Z

    return v0
.end method

.method public hasRoute()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->hasRoute:Z

    return v0
.end method

.method public hasShowtimeSeconds()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->hasShowtimeSeconds:Z

    return v0
.end method

.method public hasTheater()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->hasTheater:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$Movie;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$Movie;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->setMovie(Lcom/google/geo/sidekick/Sidekick$Movie;)Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$Location;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->setTheater(Lcom/google/geo/sidekick/Sidekick$Location;)Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->setShowtimeSeconds(J)Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->setRoute(Lcom/google/geo/sidekick/Sidekick$CommuteSummary;)Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$GmailReference;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$GmailReference;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->setGmailReference(Lcom/google/geo/sidekick/Sidekick$GmailReference;)Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->setNumberOfTickets(I)Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->setConfirmationNumber(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;

    goto :goto_0

    :sswitch_8
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$Photo;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$Photo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->setBarcode(Lcom/google/geo/sidekick/Sidekick$Photo;)Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;

    move-result-object v0

    return-object v0
.end method

.method public setBarcode(Lcom/google/geo/sidekick/Sidekick$Photo;)Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Photo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->hasBarcode:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->barcode_:Lcom/google/geo/sidekick/Sidekick$Photo;

    return-object p0
.end method

.method public setConfirmationNumber(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->hasConfirmationNumber:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->confirmationNumber_:Ljava/lang/String;

    return-object p0
.end method

.method public setGmailReference(Lcom/google/geo/sidekick/Sidekick$GmailReference;)Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$GmailReference;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->hasGmailReference:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->gmailReference_:Lcom/google/geo/sidekick/Sidekick$GmailReference;

    return-object p0
.end method

.method public setMovie(Lcom/google/geo/sidekick/Sidekick$Movie;)Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Movie;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->hasMovie:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->movie_:Lcom/google/geo/sidekick/Sidekick$Movie;

    return-object p0
.end method

.method public setNumberOfTickets(I)Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->hasNumberOfTickets:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->numberOfTickets_:I

    return-object p0
.end method

.method public setRoute(Lcom/google/geo/sidekick/Sidekick$CommuteSummary;)Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->hasRoute:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->route_:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    return-object p0
.end method

.method public setShowtimeSeconds(J)Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->hasShowtimeSeconds:Z

    iput-wide p1, p0, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->showtimeSeconds_:J

    return-object p0
.end method

.method public setTheater(Lcom/google/geo/sidekick/Sidekick$Location;)Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Location;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->hasTheater:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->theater_:Lcom/google/geo/sidekick/Sidekick$Location;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->hasMovie()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->getMovie()Lcom/google/geo/sidekick/Sidekick$Movie;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->hasTheater()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->getTheater()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->hasShowtimeSeconds()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->getShowtimeSeconds()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->hasRoute()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->getRoute()Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->hasGmailReference()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->getGmailReference()Lcom/google/geo/sidekick/Sidekick$GmailReference;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->hasNumberOfTickets()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->getNumberOfTickets()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->hasConfirmationNumber()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->getConfirmationNumber()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->hasBarcode()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$MovieTicketEntry;->getBarcode()Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_7
    return-void
.end method
