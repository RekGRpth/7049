.class public final Lcom/google/geo/sidekick/Sidekick$Notification;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Notification"
.end annotation


# instance fields
.field private cachedSize:I

.field private expirationTimestampSeconds_:J

.field private hasExpirationTimestampSeconds:Z

.field private hasLastDeliveredTimestampSeconds:Z

.field private hasNotificationBarText:Z

.field private hasTriggerCondition:Z

.field private hasType:Z

.field private lastDeliveredTimestampSeconds_:J

.field private notificationBarText_:Ljava/lang/String;

.field private triggerCondition_:Lcom/google/geo/sidekick/Sidekick$TriggerCondition;

.field private type_:I


# direct methods
.method public constructor <init>()V
    .locals 3

    const-wide/16 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$Notification;->type_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Notification;->notificationBarText_:Ljava/lang/String;

    iput-wide v1, p0, Lcom/google/geo/sidekick/Sidekick$Notification;->lastDeliveredTimestampSeconds_:J

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Notification;->triggerCondition_:Lcom/google/geo/sidekick/Sidekick$TriggerCondition;

    iput-wide v1, p0, Lcom/google/geo/sidekick/Sidekick$Notification;->expirationTimestampSeconds_:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$Notification;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$Notification;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Notification;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$Notification;->cachedSize:I

    return v0
.end method

.method public getExpirationTimestampSeconds()J
    .locals 2

    iget-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$Notification;->expirationTimestampSeconds_:J

    return-wide v0
.end method

.method public getLastDeliveredTimestampSeconds()J
    .locals 2

    iget-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$Notification;->lastDeliveredTimestampSeconds_:J

    return-wide v0
.end method

.method public getNotificationBarText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Notification;->notificationBarText_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Notification;->hasType()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Notification;->getType()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Notification;->hasNotificationBarText()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Notification;->getNotificationBarText()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Notification;->hasLastDeliveredTimestampSeconds()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Notification;->getLastDeliveredTimestampSeconds()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Notification;->hasTriggerCondition()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Notification;->getTriggerCondition()Lcom/google/geo/sidekick/Sidekick$TriggerCondition;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Notification;->hasExpirationTimestampSeconds()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Notification;->getExpirationTimestampSeconds()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$Notification;->cachedSize:I

    return v0
.end method

.method public getTriggerCondition()Lcom/google/geo/sidekick/Sidekick$TriggerCondition;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$Notification;->triggerCondition_:Lcom/google/geo/sidekick/Sidekick$TriggerCondition;

    return-object v0
.end method

.method public getType()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$Notification;->type_:I

    return v0
.end method

.method public hasExpirationTimestampSeconds()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Notification;->hasExpirationTimestampSeconds:Z

    return v0
.end method

.method public hasLastDeliveredTimestampSeconds()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Notification;->hasLastDeliveredTimestampSeconds:Z

    return v0
.end method

.method public hasNotificationBarText()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Notification;->hasNotificationBarText:Z

    return v0
.end method

.method public hasTriggerCondition()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Notification;->hasTriggerCondition:Z

    return v0
.end method

.method public hasType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Notification;->hasType:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$Notification;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$Notification;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$Notification;->setType(I)Lcom/google/geo/sidekick/Sidekick$Notification;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$Notification;->setNotificationBarText(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$Notification;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/geo/sidekick/Sidekick$Notification;->setLastDeliveredTimestampSeconds(J)Lcom/google/geo/sidekick/Sidekick$Notification;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$TriggerCondition;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$TriggerCondition;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$Notification;->setTriggerCondition(Lcom/google/geo/sidekick/Sidekick$TriggerCondition;)Lcom/google/geo/sidekick/Sidekick$Notification;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/geo/sidekick/Sidekick$Notification;->setExpirationTimestampSeconds(J)Lcom/google/geo/sidekick/Sidekick$Notification;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$Notification;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$Notification;

    move-result-object v0

    return-object v0
.end method

.method public setExpirationTimestampSeconds(J)Lcom/google/geo/sidekick/Sidekick$Notification;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Notification;->hasExpirationTimestampSeconds:Z

    iput-wide p1, p0, Lcom/google/geo/sidekick/Sidekick$Notification;->expirationTimestampSeconds_:J

    return-object p0
.end method

.method public setLastDeliveredTimestampSeconds(J)Lcom/google/geo/sidekick/Sidekick$Notification;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Notification;->hasLastDeliveredTimestampSeconds:Z

    iput-wide p1, p0, Lcom/google/geo/sidekick/Sidekick$Notification;->lastDeliveredTimestampSeconds_:J

    return-object p0
.end method

.method public setNotificationBarText(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$Notification;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Notification;->hasNotificationBarText:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Notification;->notificationBarText_:Ljava/lang/String;

    return-object p0
.end method

.method public setTriggerCondition(Lcom/google/geo/sidekick/Sidekick$TriggerCondition;)Lcom/google/geo/sidekick/Sidekick$Notification;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$TriggerCondition;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Notification;->hasTriggerCondition:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$Notification;->triggerCondition_:Lcom/google/geo/sidekick/Sidekick$TriggerCondition;

    return-object p0
.end method

.method public setType(I)Lcom/google/geo/sidekick/Sidekick$Notification;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$Notification;->hasType:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$Notification;->type_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Notification;->hasType()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Notification;->getType()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Notification;->hasNotificationBarText()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Notification;->getNotificationBarText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Notification;->hasLastDeliveredTimestampSeconds()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Notification;->getLastDeliveredTimestampSeconds()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Notification;->hasTriggerCondition()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Notification;->getTriggerCondition()Lcom/google/geo/sidekick/Sidekick$TriggerCondition;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Notification;->hasExpirationTimestampSeconds()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Notification;->getExpirationTimestampSeconds()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_4
    return-void
.end method
