.class public final Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PackageTrackingEntry"
.end annotation


# instance fields
.field private cachedSize:I

.field private estimatedDeliverySecs_:J

.field private fromAddress_:Ljava/lang/String;

.field private gmailReference_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$GmailReference;",
            ">;"
        }
    .end annotation
.end field

.field private hasEstimatedDeliverySecs:Z

.field private hasFromAddress:Z

.field private hasLastUpdateTimeSecs:Z

.field private hasOrderId:Z

.field private hasPackageStatusUpdatesEnabled:Z

.field private hasSecondaryPageUrl:Z

.field private hasSecondaryPageUrlRequiresGaiaLogin:Z

.field private hasSecondaryPageUrlTitle:Z

.field private hasShippingCompanyLogoUrl:Z

.field private hasShippingCompanyName:Z

.field private hasShippingService:Z

.field private hasStatus:Z

.field private hasStatusCode:Z

.field private hasToAddress:Z

.field private hasTrackingButtonUrlRequiresGaiaLogin:Z

.field private hasTrackingNumber:Z

.field private hasTrackingUrl:Z

.field private items_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$PackageItem;",
            ">;"
        }
    .end annotation
.end field

.field private lastUpdateTimeSecs_:J

.field private orderId_:Ljava/lang/String;

.field private packageStatusUpdatesEnabled_:Z

.field private secondaryPageUrlRequiresGaiaLogin_:Z

.field private secondaryPageUrlTitle_:Ljava/lang/String;

.field private secondaryPageUrl_:Ljava/lang/String;

.field private shippingCompanyLogoUrl_:Ljava/lang/String;

.field private shippingCompanyName_:Ljava/lang/String;

.field private shippingService_:Ljava/lang/String;

.field private statusCode_:I

.field private status_:Ljava/lang/String;

.field private toAddress_:Ljava/lang/String;

.field private trackingButtonUrlRequiresGaiaLogin_:Z

.field private trackingNumber_:Ljava/lang/String;

.field private trackingUrl_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 4

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->status_:Ljava/lang/String;

    iput v1, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->statusCode_:I

    iput-wide v2, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->lastUpdateTimeSecs_:J

    iput-wide v2, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->estimatedDeliverySecs_:J

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->shippingCompanyName_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->shippingCompanyLogoUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->shippingService_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->fromAddress_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->toAddress_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->trackingNumber_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->trackingUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->orderId_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->gmailReference_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->items_:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->secondaryPageUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->secondaryPageUrlTitle_:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->secondaryPageUrlRequiresGaiaLogin_:Z

    iput-boolean v1, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->trackingButtonUrlRequiresGaiaLogin_:Z

    iput-boolean v1, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->packageStatusUpdatesEnabled_:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addGmailReference(Lcom/google/geo/sidekick/Sidekick$GmailReference;)Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$GmailReference;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->gmailReference_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->gmailReference_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->gmailReference_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addItems(Lcom/google/geo/sidekick/Sidekick$PackageItem;)Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$PackageItem;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->items_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->items_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->items_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->cachedSize:I

    return v0
.end method

.method public getEstimatedDeliverySecs()J
    .locals 2

    iget-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->estimatedDeliverySecs_:J

    return-wide v0
.end method

.method public getFromAddress()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->fromAddress_:Ljava/lang/String;

    return-object v0
.end method

.method public getGmailReferenceList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$GmailReference;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->gmailReference_:Ljava/util/List;

    return-object v0
.end method

.method public getItems(I)Lcom/google/geo/sidekick/Sidekick$PackageItem;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->items_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$PackageItem;

    return-object v0
.end method

.method public getItemsCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->items_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$PackageItem;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->items_:Ljava/util/List;

    return-object v0
.end method

.method public getLastUpdateTimeSecs()J
    .locals 2

    iget-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->lastUpdateTimeSecs_:J

    return-wide v0
.end method

.method public getOrderId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->orderId_:Ljava/lang/String;

    return-object v0
.end method

.method public getPackageStatusUpdatesEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->packageStatusUpdatesEnabled_:Z

    return v0
.end method

.method public getSecondaryPageUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->secondaryPageUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getSecondaryPageUrlRequiresGaiaLogin()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->secondaryPageUrlRequiresGaiaLogin_:Z

    return v0
.end method

.method public getSecondaryPageUrlTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->secondaryPageUrlTitle_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasStatus()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getStatus()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasStatusCode()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getStatusCode()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasLastUpdateTimeSecs()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getLastUpdateTimeSecs()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasShippingCompanyName()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getShippingCompanyName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasShippingCompanyLogoUrl()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getShippingCompanyLogoUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasShippingService()Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getShippingService()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasFromAddress()Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v3, 0x7

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getFromAddress()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_6
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasToAddress()Z

    move-result v3

    if-eqz v3, :cond_7

    const/16 v3, 0x8

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getToAddress()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_7
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasTrackingNumber()Z

    move-result v3

    if-eqz v3, :cond_8

    const/16 v3, 0x9

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getTrackingNumber()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_8
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasTrackingUrl()Z

    move-result v3

    if-eqz v3, :cond_9

    const/16 v3, 0xa

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getTrackingUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_9
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getGmailReferenceList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$GmailReference;

    const/16 v3, 0xb

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_a
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getItemsList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$PackageItem;

    const/16 v3, 0xc

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_1

    :cond_b
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasEstimatedDeliverySecs()Z

    move-result v3

    if-eqz v3, :cond_c

    const/16 v3, 0xd

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getEstimatedDeliverySecs()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_c
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasSecondaryPageUrl()Z

    move-result v3

    if-eqz v3, :cond_d

    const/16 v3, 0xe

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getSecondaryPageUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_d
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasSecondaryPageUrlTitle()Z

    move-result v3

    if-eqz v3, :cond_e

    const/16 v3, 0xf

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getSecondaryPageUrlTitle()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_e
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasOrderId()Z

    move-result v3

    if-eqz v3, :cond_f

    const/16 v3, 0x10

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getOrderId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_f
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasSecondaryPageUrlRequiresGaiaLogin()Z

    move-result v3

    if-eqz v3, :cond_10

    const/16 v3, 0x11

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getSecondaryPageUrlRequiresGaiaLogin()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_10
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasTrackingButtonUrlRequiresGaiaLogin()Z

    move-result v3

    if-eqz v3, :cond_11

    const/16 v3, 0x12

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getTrackingButtonUrlRequiresGaiaLogin()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_11
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasPackageStatusUpdatesEnabled()Z

    move-result v3

    if-eqz v3, :cond_12

    const/16 v3, 0x13

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getPackageStatusUpdatesEnabled()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_12
    iput v2, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->cachedSize:I

    return v2
.end method

.method public getShippingCompanyLogoUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->shippingCompanyLogoUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getShippingCompanyName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->shippingCompanyName_:Ljava/lang/String;

    return-object v0
.end method

.method public getShippingService()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->shippingService_:Ljava/lang/String;

    return-object v0
.end method

.method public getStatus()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->status_:Ljava/lang/String;

    return-object v0
.end method

.method public getStatusCode()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->statusCode_:I

    return v0
.end method

.method public getToAddress()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->toAddress_:Ljava/lang/String;

    return-object v0
.end method

.method public getTrackingButtonUrlRequiresGaiaLogin()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->trackingButtonUrlRequiresGaiaLogin_:Z

    return v0
.end method

.method public getTrackingNumber()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->trackingNumber_:Ljava/lang/String;

    return-object v0
.end method

.method public getTrackingUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->trackingUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public hasEstimatedDeliverySecs()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasEstimatedDeliverySecs:Z

    return v0
.end method

.method public hasFromAddress()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasFromAddress:Z

    return v0
.end method

.method public hasLastUpdateTimeSecs()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasLastUpdateTimeSecs:Z

    return v0
.end method

.method public hasOrderId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasOrderId:Z

    return v0
.end method

.method public hasPackageStatusUpdatesEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasPackageStatusUpdatesEnabled:Z

    return v0
.end method

.method public hasSecondaryPageUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasSecondaryPageUrl:Z

    return v0
.end method

.method public hasSecondaryPageUrlRequiresGaiaLogin()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasSecondaryPageUrlRequiresGaiaLogin:Z

    return v0
.end method

.method public hasSecondaryPageUrlTitle()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasSecondaryPageUrlTitle:Z

    return v0
.end method

.method public hasShippingCompanyLogoUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasShippingCompanyLogoUrl:Z

    return v0
.end method

.method public hasShippingCompanyName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasShippingCompanyName:Z

    return v0
.end method

.method public hasShippingService()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasShippingService:Z

    return v0
.end method

.method public hasStatus()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasStatus:Z

    return v0
.end method

.method public hasStatusCode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasStatusCode:Z

    return v0
.end method

.method public hasToAddress()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasToAddress:Z

    return v0
.end method

.method public hasTrackingButtonUrlRequiresGaiaLogin()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasTrackingButtonUrlRequiresGaiaLogin:Z

    return v0
.end method

.method public hasTrackingNumber()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasTrackingNumber:Z

    return v0
.end method

.method public hasTrackingUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasTrackingUrl:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->setStatus(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->setStatusCode(I)Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->setLastUpdateTimeSecs(J)Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->setShippingCompanyName(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->setShippingCompanyLogoUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->setShippingService(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->setFromAddress(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->setToAddress(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->setTrackingNumber(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->setTrackingUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    goto :goto_0

    :sswitch_b
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$GmailReference;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$GmailReference;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->addGmailReference(Lcom/google/geo/sidekick/Sidekick$GmailReference;)Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    goto :goto_0

    :sswitch_c
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$PackageItem;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$PackageItem;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->addItems(Lcom/google/geo/sidekick/Sidekick$PackageItem;)Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->setEstimatedDeliverySecs(J)Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    goto :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->setSecondaryPageUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->setSecondaryPageUrlTitle(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->setOrderId(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->setSecondaryPageUrlRequiresGaiaLogin(Z)Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->setTrackingButtonUrlRequiresGaiaLogin(Z)Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    goto/16 :goto_0

    :sswitch_13
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->setPackageStatusUpdatesEnabled(Z)Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x68 -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
        0x88 -> :sswitch_11
        0x90 -> :sswitch_12
        0x98 -> :sswitch_13
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    move-result-object v0

    return-object v0
.end method

.method public setEstimatedDeliverySecs(J)Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasEstimatedDeliverySecs:Z

    iput-wide p1, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->estimatedDeliverySecs_:J

    return-object p0
.end method

.method public setFromAddress(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasFromAddress:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->fromAddress_:Ljava/lang/String;

    return-object p0
.end method

.method public setLastUpdateTimeSecs(J)Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasLastUpdateTimeSecs:Z

    iput-wide p1, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->lastUpdateTimeSecs_:J

    return-object p0
.end method

.method public setOrderId(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasOrderId:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->orderId_:Ljava/lang/String;

    return-object p0
.end method

.method public setPackageStatusUpdatesEnabled(Z)Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasPackageStatusUpdatesEnabled:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->packageStatusUpdatesEnabled_:Z

    return-object p0
.end method

.method public setSecondaryPageUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasSecondaryPageUrl:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->secondaryPageUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setSecondaryPageUrlRequiresGaiaLogin(Z)Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasSecondaryPageUrlRequiresGaiaLogin:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->secondaryPageUrlRequiresGaiaLogin_:Z

    return-object p0
.end method

.method public setSecondaryPageUrlTitle(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasSecondaryPageUrlTitle:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->secondaryPageUrlTitle_:Ljava/lang/String;

    return-object p0
.end method

.method public setShippingCompanyLogoUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasShippingCompanyLogoUrl:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->shippingCompanyLogoUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setShippingCompanyName(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasShippingCompanyName:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->shippingCompanyName_:Ljava/lang/String;

    return-object p0
.end method

.method public setShippingService(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasShippingService:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->shippingService_:Ljava/lang/String;

    return-object p0
.end method

.method public setStatus(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasStatus:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->status_:Ljava/lang/String;

    return-object p0
.end method

.method public setStatusCode(I)Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasStatusCode:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->statusCode_:I

    return-object p0
.end method

.method public setToAddress(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasToAddress:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->toAddress_:Ljava/lang/String;

    return-object p0
.end method

.method public setTrackingButtonUrlRequiresGaiaLogin(Z)Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasTrackingButtonUrlRequiresGaiaLogin:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->trackingButtonUrlRequiresGaiaLogin_:Z

    return-object p0
.end method

.method public setTrackingNumber(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasTrackingNumber:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->trackingNumber_:Ljava/lang/String;

    return-object p0
.end method

.method public setTrackingUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasTrackingUrl:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->trackingUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 5
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasStatus()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getStatus()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasStatusCode()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getStatusCode()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasLastUpdateTimeSecs()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getLastUpdateTimeSecs()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasShippingCompanyName()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getShippingCompanyName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasShippingCompanyLogoUrl()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getShippingCompanyLogoUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasShippingService()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getShippingService()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasFromAddress()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getFromAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasToAddress()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getToAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasTrackingNumber()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getTrackingNumber()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasTrackingUrl()Z

    move-result v2

    if-eqz v2, :cond_9

    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getTrackingUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getGmailReferenceList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$GmailReference;

    const/16 v2, 0xb

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_a
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getItemsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$PackageItem;

    const/16 v2, 0xc

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_1

    :cond_b
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasEstimatedDeliverySecs()Z

    move-result v2

    if-eqz v2, :cond_c

    const/16 v2, 0xd

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getEstimatedDeliverySecs()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_c
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasSecondaryPageUrl()Z

    move-result v2

    if-eqz v2, :cond_d

    const/16 v2, 0xe

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getSecondaryPageUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_d
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasSecondaryPageUrlTitle()Z

    move-result v2

    if-eqz v2, :cond_e

    const/16 v2, 0xf

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getSecondaryPageUrlTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_e
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasOrderId()Z

    move-result v2

    if-eqz v2, :cond_f

    const/16 v2, 0x10

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getOrderId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_f
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasSecondaryPageUrlRequiresGaiaLogin()Z

    move-result v2

    if-eqz v2, :cond_10

    const/16 v2, 0x11

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getSecondaryPageUrlRequiresGaiaLogin()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_10
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasTrackingButtonUrlRequiresGaiaLogin()Z

    move-result v2

    if-eqz v2, :cond_11

    const/16 v2, 0x12

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getTrackingButtonUrlRequiresGaiaLogin()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_11
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasPackageStatusUpdatesEnabled()Z

    move-result v2

    if-eqz v2, :cond_12

    const/16 v2, 0x13

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getPackageStatusUpdatesEnabled()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_12
    return-void
.end method
