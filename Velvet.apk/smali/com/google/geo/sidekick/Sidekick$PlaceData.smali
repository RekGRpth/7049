.class public final Lcom/google/geo/sidekick/Sidekick$PlaceData;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PlaceData"
.end annotation


# instance fields
.field private businessData_:Lcom/google/geo/sidekick/Sidekick$BusinessData;

.field private cachedSize:I

.field private contactData_:Lcom/google/geo/sidekick/Sidekick$ContactData;

.field private displayName_:Ljava/lang/String;

.field private hasBusinessData:Z

.field private hasContactData:Z

.field private hasDisplayName:Z

.field private hasPlaceClusterData:Z

.field private placeClusterData_:Lcom/google/geo/sidekick/Sidekick$PlaceClusterData;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$PlaceData;->displayName_:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$PlaceData;->contactData_:Lcom/google/geo/sidekick/Sidekick$ContactData;

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$PlaceData;->businessData_:Lcom/google/geo/sidekick/Sidekick$BusinessData;

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$PlaceData;->placeClusterData_:Lcom/google/geo/sidekick/Sidekick$PlaceClusterData;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$PlaceData;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getBusinessData()Lcom/google/geo/sidekick/Sidekick$BusinessData;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$PlaceData;->businessData_:Lcom/google/geo/sidekick/Sidekick$BusinessData;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$PlaceData;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$PlaceData;->cachedSize:I

    return v0
.end method

.method public getContactData()Lcom/google/geo/sidekick/Sidekick$ContactData;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$PlaceData;->contactData_:Lcom/google/geo/sidekick/Sidekick$ContactData;

    return-object v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$PlaceData;->displayName_:Ljava/lang/String;

    return-object v0
.end method

.method public getPlaceClusterData()Lcom/google/geo/sidekick/Sidekick$PlaceClusterData;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$PlaceData;->placeClusterData_:Lcom/google/geo/sidekick/Sidekick$PlaceClusterData;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->hasDisplayName()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->hasContactData()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->getContactData()Lcom/google/geo/sidekick/Sidekick$ContactData;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->hasBusinessData()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->getBusinessData()Lcom/google/geo/sidekick/Sidekick$BusinessData;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->hasPlaceClusterData()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->getPlaceClusterData()Lcom/google/geo/sidekick/Sidekick$PlaceClusterData;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$PlaceData;->cachedSize:I

    return v0
.end method

.method public hasBusinessData()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$PlaceData;->hasBusinessData:Z

    return v0
.end method

.method public hasContactData()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$PlaceData;->hasContactData:Z

    return v0
.end method

.method public hasDisplayName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$PlaceData;->hasDisplayName:Z

    return v0
.end method

.method public hasPlaceClusterData()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$PlaceData;->hasPlaceClusterData:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$PlaceData;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->setDisplayName(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$PlaceData;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$ContactData;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$ContactData;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->setContactData(Lcom/google/geo/sidekick/Sidekick$ContactData;)Lcom/google/geo/sidekick/Sidekick$PlaceData;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$BusinessData;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$BusinessData;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->setBusinessData(Lcom/google/geo/sidekick/Sidekick$BusinessData;)Lcom/google/geo/sidekick/Sidekick$PlaceData;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$PlaceClusterData;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$PlaceClusterData;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->setPlaceClusterData(Lcom/google/geo/sidekick/Sidekick$PlaceClusterData;)Lcom/google/geo/sidekick/Sidekick$PlaceData;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$PlaceData;

    move-result-object v0

    return-object v0
.end method

.method public setBusinessData(Lcom/google/geo/sidekick/Sidekick$BusinessData;)Lcom/google/geo/sidekick/Sidekick$PlaceData;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$BusinessData;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$PlaceData;->hasBusinessData:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$PlaceData;->businessData_:Lcom/google/geo/sidekick/Sidekick$BusinessData;

    return-object p0
.end method

.method public setContactData(Lcom/google/geo/sidekick/Sidekick$ContactData;)Lcom/google/geo/sidekick/Sidekick$PlaceData;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$ContactData;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$PlaceData;->hasContactData:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$PlaceData;->contactData_:Lcom/google/geo/sidekick/Sidekick$ContactData;

    return-object p0
.end method

.method public setDisplayName(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$PlaceData;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$PlaceData;->hasDisplayName:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$PlaceData;->displayName_:Ljava/lang/String;

    return-object p0
.end method

.method public setPlaceClusterData(Lcom/google/geo/sidekick/Sidekick$PlaceClusterData;)Lcom/google/geo/sidekick/Sidekick$PlaceData;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$PlaceClusterData;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$PlaceData;->hasPlaceClusterData:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$PlaceData;->placeClusterData_:Lcom/google/geo/sidekick/Sidekick$PlaceClusterData;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->hasDisplayName()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->hasContactData()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->getContactData()Lcom/google/geo/sidekick/Sidekick$ContactData;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->hasBusinessData()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->getBusinessData()Lcom/google/geo/sidekick/Sidekick$BusinessData;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->hasPlaceClusterData()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->getPlaceClusterData()Lcom/google/geo/sidekick/Sidekick$PlaceClusterData;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_3
    return-void
.end method
