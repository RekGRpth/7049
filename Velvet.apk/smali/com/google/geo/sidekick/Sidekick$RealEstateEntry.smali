.class public final Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RealEstateEntry"
.end annotation


# instance fields
.field private address_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private cachedSize:I

.field private detailsUrl_:Ljava/lang/String;

.field private forSaleSimilarListingsUrl_:Ljava/lang/String;

.field private hasDetailsUrl:Z

.field private hasForSaleSimilarListingsUrl:Z

.field private hasListingTimestamp:Z

.field private hasLivingArea:Z

.field private hasLotSize:Z

.field private hasNextOpenHouseTimestamp:Z

.field private hasNumBedroomsAndBathrooms:Z

.field private hasPrice:Z

.field private hasRecentlySoldSimilarListingsUrl:Z

.field private hasSubtype:Z

.field private hasType:Z

.field private hasYearBuilt:Z

.field private listingTimestamp_:J

.field private livingArea_:Ljava/lang/String;

.field private lotSize_:Ljava/lang/String;

.field private nextOpenHouseTimestamp_:J

.field private numBedroomsAndBathrooms_:Ljava/lang/String;

.field private photo_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Photo;",
            ">;"
        }
    .end annotation
.end field

.field private price_:Ljava/lang/String;

.field private recentlySoldSimilarListingsUrl_:Ljava/lang/String;

.field private subtype_:Ljava/lang/String;

.field private type_:Ljava/lang/String;

.field private yearBuilt_:I


# direct methods
.method public constructor <init>()V
    .locals 3

    const-wide/16 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->address_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->photo_:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->type_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->subtype_:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->yearBuilt_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->livingArea_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->lotSize_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->numBedroomsAndBathrooms_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->detailsUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->price_:Ljava/lang/String;

    iput-wide v1, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->listingTimestamp_:J

    iput-wide v1, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->nextOpenHouseTimestamp_:J

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->forSaleSimilarListingsUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->recentlySoldSimilarListingsUrl_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addAddress(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->address_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->address_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->address_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addPhoto(Lcom/google/geo/sidekick/Sidekick$Photo;)Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Photo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->photo_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->photo_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->photo_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getAddress(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->address_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getAddressCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->address_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getAddressList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->address_:Ljava/util/List;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->cachedSize:I

    return v0
.end method

.method public getDetailsUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->detailsUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getForSaleSimilarListingsUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->forSaleSimilarListingsUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getListingTimestamp()J
    .locals 2

    iget-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->listingTimestamp_:J

    return-wide v0
.end method

.method public getLivingArea()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->livingArea_:Ljava/lang/String;

    return-object v0
.end method

.method public getLotSize()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->lotSize_:Ljava/lang/String;

    return-object v0
.end method

.method public getNextOpenHouseTimestamp()J
    .locals 2

    iget-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->nextOpenHouseTimestamp_:J

    return-wide v0
.end method

.method public getNumBedroomsAndBathrooms()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->numBedroomsAndBathrooms_:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoto(I)Lcom/google/geo/sidekick/Sidekick$Photo;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->photo_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Photo;

    return-object v0
.end method

.method public getPhotoCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->photo_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getPhotoList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Photo;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->photo_:Ljava/util/List;

    return-object v0
.end method

.method public getPrice()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->price_:Ljava/lang/String;

    return-object v0
.end method

.method public getRecentlySoldSimilarListingsUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->recentlySoldSimilarListingsUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 7

    const/4 v3, 0x0

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getAddressList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_0

    :cond_0
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getAddressList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getPhotoList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/geo/sidekick/Sidekick$Photo;

    const/4 v4, 0x2

    invoke-static {v4, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasType()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getType()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasSubtype()Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getSubtype()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasYearBuilt()Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v4, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getYearBuilt()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasLivingArea()Z

    move-result v4

    if-eqz v4, :cond_5

    const/4 v4, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getLivingArea()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasLotSize()Z

    move-result v4

    if-eqz v4, :cond_6

    const/4 v4, 0x7

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getLotSize()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_6
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasNumBedroomsAndBathrooms()Z

    move-result v4

    if-eqz v4, :cond_7

    const/16 v4, 0x8

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getNumBedroomsAndBathrooms()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_7
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasDetailsUrl()Z

    move-result v4

    if-eqz v4, :cond_8

    const/16 v4, 0x9

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getDetailsUrl()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_8
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasPrice()Z

    move-result v4

    if-eqz v4, :cond_9

    const/16 v4, 0xa

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getPrice()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_9
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasListingTimestamp()Z

    move-result v4

    if-eqz v4, :cond_a

    const/16 v4, 0xb

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getListingTimestamp()J

    move-result-wide v5

    invoke-static {v4, v5, v6}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_a
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasNextOpenHouseTimestamp()Z

    move-result v4

    if-eqz v4, :cond_b

    const/16 v4, 0xc

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getNextOpenHouseTimestamp()J

    move-result-wide v5

    invoke-static {v4, v5, v6}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_b
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasForSaleSimilarListingsUrl()Z

    move-result v4

    if-eqz v4, :cond_c

    const/16 v4, 0xd

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getForSaleSimilarListingsUrl()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_c
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasRecentlySoldSimilarListingsUrl()Z

    move-result v4

    if-eqz v4, :cond_d

    const/16 v4, 0xe

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getRecentlySoldSimilarListingsUrl()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_d
    iput v3, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->cachedSize:I

    return v3
.end method

.method public getSubtype()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->subtype_:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->type_:Ljava/lang/String;

    return-object v0
.end method

.method public getYearBuilt()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->yearBuilt_:I

    return v0
.end method

.method public hasDetailsUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasDetailsUrl:Z

    return v0
.end method

.method public hasForSaleSimilarListingsUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasForSaleSimilarListingsUrl:Z

    return v0
.end method

.method public hasListingTimestamp()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasListingTimestamp:Z

    return v0
.end method

.method public hasLivingArea()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasLivingArea:Z

    return v0
.end method

.method public hasLotSize()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasLotSize:Z

    return v0
.end method

.method public hasNextOpenHouseTimestamp()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasNextOpenHouseTimestamp:Z

    return v0
.end method

.method public hasNumBedroomsAndBathrooms()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasNumBedroomsAndBathrooms:Z

    return v0
.end method

.method public hasPrice()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasPrice:Z

    return v0
.end method

.method public hasRecentlySoldSimilarListingsUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasRecentlySoldSimilarListingsUrl:Z

    return v0
.end method

.method public hasSubtype()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasSubtype:Z

    return v0
.end method

.method public hasType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasType:Z

    return v0
.end method

.method public hasYearBuilt()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasYearBuilt:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->addAddress(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$Photo;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$Photo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->addPhoto(Lcom/google/geo/sidekick/Sidekick$Photo;)Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->setType(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->setSubtype(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->setYearBuilt(I)Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->setLivingArea(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->setLotSize(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->setNumBedroomsAndBathrooms(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->setDetailsUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->setPrice(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->setListingTimestamp(J)Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->setNextOpenHouseTimestamp(J)Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->setForSaleSimilarListingsUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;

    goto :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->setRecentlySoldSimilarListingsUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;

    move-result-object v0

    return-object v0
.end method

.method public setDetailsUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasDetailsUrl:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->detailsUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setForSaleSimilarListingsUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasForSaleSimilarListingsUrl:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->forSaleSimilarListingsUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setListingTimestamp(J)Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasListingTimestamp:Z

    iput-wide p1, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->listingTimestamp_:J

    return-object p0
.end method

.method public setLivingArea(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasLivingArea:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->livingArea_:Ljava/lang/String;

    return-object p0
.end method

.method public setLotSize(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasLotSize:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->lotSize_:Ljava/lang/String;

    return-object p0
.end method

.method public setNextOpenHouseTimestamp(J)Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasNextOpenHouseTimestamp:Z

    iput-wide p1, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->nextOpenHouseTimestamp_:J

    return-object p0
.end method

.method public setNumBedroomsAndBathrooms(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasNumBedroomsAndBathrooms:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->numBedroomsAndBathrooms_:Ljava/lang/String;

    return-object p0
.end method

.method public setPrice(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasPrice:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->price_:Ljava/lang/String;

    return-object p0
.end method

.method public setRecentlySoldSimilarListingsUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasRecentlySoldSimilarListingsUrl:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->recentlySoldSimilarListingsUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setSubtype(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasSubtype:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->subtype_:Ljava/lang/String;

    return-object p0
.end method

.method public setType(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasType:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->type_:Ljava/lang/String;

    return-object p0
.end method

.method public setYearBuilt(I)Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasYearBuilt:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->yearBuilt_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 5
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getAddressList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getPhotoList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Photo;

    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasType()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasSubtype()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getSubtype()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasYearBuilt()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getYearBuilt()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasLivingArea()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getLivingArea()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasLotSize()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getLotSize()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasNumBedroomsAndBathrooms()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getNumBedroomsAndBathrooms()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasDetailsUrl()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getDetailsUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasPrice()Z

    move-result v2

    if-eqz v2, :cond_9

    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getPrice()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasListingTimestamp()Z

    move-result v2

    if-eqz v2, :cond_a

    const/16 v2, 0xb

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getListingTimestamp()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasNextOpenHouseTimestamp()Z

    move-result v2

    if-eqz v2, :cond_b

    const/16 v2, 0xc

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getNextOpenHouseTimestamp()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_b
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasForSaleSimilarListingsUrl()Z

    move-result v2

    if-eqz v2, :cond_c

    const/16 v2, 0xd

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getForSaleSimilarListingsUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_c
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->hasRecentlySoldSimilarListingsUrl()Z

    move-result v2

    if-eqz v2, :cond_d

    const/16 v2, 0xe

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RealEstateEntry;->getRecentlySoldSimilarListingsUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_d
    return-void
.end method
