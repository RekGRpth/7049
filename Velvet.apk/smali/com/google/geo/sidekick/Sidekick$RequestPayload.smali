.class public final Lcom/google/geo/sidekick/Sidekick$RequestPayload;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RequestPayload"
.end annotation


# instance fields
.field private actionsQuery_:Lcom/google/geo/sidekick/Sidekick$ActionsQuery;

.field private cachedSize:I

.field private configurationQuery_:Lcom/google/geo/sidekick/Sidekick$Configuration;

.field private entryQuery_:Lcom/google/geo/sidekick/Sidekick$EntryQuery;

.field private experimentOverrides_:Lcom/google/geo/sidekick/Sidekick$ExperimentOverrides;

.field private fetchConfigurationQuery_:Lcom/google/geo/sidekick/Sidekick$FetchConfigurationQuery;

.field private fetchStaticEntitiesQuery_:Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesQuery;

.field private hasActionsQuery:Z

.field private hasConfigurationQuery:Z

.field private hasEntryQuery:Z

.field private hasExperimentOverrides:Z

.field private hasFetchConfigurationQuery:Z

.field private hasFetchStaticEntitiesQuery:Z

.field private hasSensorSignals:Z

.field private hasStateChangeQuery:Z

.field private hasStaticMapQuery:Z

.field private sensorSignals_:Lcom/google/geo/sidekick/Sidekick$SensorSignals;

.field private stateChangeQuery_:Lcom/google/geo/sidekick/Sidekick$StateChanges;

.field private staticMapQuery_:Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->configurationQuery_:Lcom/google/geo/sidekick/Sidekick$Configuration;

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->entryQuery_:Lcom/google/geo/sidekick/Sidekick$EntryQuery;

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->actionsQuery_:Lcom/google/geo/sidekick/Sidekick$ActionsQuery;

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->staticMapQuery_:Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->fetchConfigurationQuery_:Lcom/google/geo/sidekick/Sidekick$FetchConfigurationQuery;

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->sensorSignals_:Lcom/google/geo/sidekick/Sidekick$SensorSignals;

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->fetchStaticEntitiesQuery_:Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesQuery;

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->stateChangeQuery_:Lcom/google/geo/sidekick/Sidekick$StateChanges;

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->experimentOverrides_:Lcom/google/geo/sidekick/Sidekick$ExperimentOverrides;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getActionsQuery()Lcom/google/geo/sidekick/Sidekick$ActionsQuery;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->actionsQuery_:Lcom/google/geo/sidekick/Sidekick$ActionsQuery;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->cachedSize:I

    return v0
.end method

.method public getConfigurationQuery()Lcom/google/geo/sidekick/Sidekick$Configuration;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->configurationQuery_:Lcom/google/geo/sidekick/Sidekick$Configuration;

    return-object v0
.end method

.method public getEntryQuery()Lcom/google/geo/sidekick/Sidekick$EntryQuery;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->entryQuery_:Lcom/google/geo/sidekick/Sidekick$EntryQuery;

    return-object v0
.end method

.method public getExperimentOverrides()Lcom/google/geo/sidekick/Sidekick$ExperimentOverrides;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->experimentOverrides_:Lcom/google/geo/sidekick/Sidekick$ExperimentOverrides;

    return-object v0
.end method

.method public getFetchConfigurationQuery()Lcom/google/geo/sidekick/Sidekick$FetchConfigurationQuery;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->fetchConfigurationQuery_:Lcom/google/geo/sidekick/Sidekick$FetchConfigurationQuery;

    return-object v0
.end method

.method public getFetchStaticEntitiesQuery()Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesQuery;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->fetchStaticEntitiesQuery_:Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesQuery;

    return-object v0
.end method

.method public getSensorSignals()Lcom/google/geo/sidekick/Sidekick$SensorSignals;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->sensorSignals_:Lcom/google/geo/sidekick/Sidekick$SensorSignals;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->hasConfigurationQuery()Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x9

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->getConfigurationQuery()Lcom/google/geo/sidekick/Sidekick$Configuration;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->hasEntryQuery()Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v1, 0xa

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->getEntryQuery()Lcom/google/geo/sidekick/Sidekick$EntryQuery;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->hasActionsQuery()Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v1, 0xb

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->getActionsQuery()Lcom/google/geo/sidekick/Sidekick$ActionsQuery;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->hasStaticMapQuery()Z

    move-result v1

    if-eqz v1, :cond_3

    const/16 v1, 0xd

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->getStaticMapQuery()Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->hasFetchConfigurationQuery()Z

    move-result v1

    if-eqz v1, :cond_4

    const/16 v1, 0xe

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->getFetchConfigurationQuery()Lcom/google/geo/sidekick/Sidekick$FetchConfigurationQuery;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->hasSensorSignals()Z

    move-result v1

    if-eqz v1, :cond_5

    const/16 v1, 0xf

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->getSensorSignals()Lcom/google/geo/sidekick/Sidekick$SensorSignals;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->hasFetchStaticEntitiesQuery()Z

    move-result v1

    if-eqz v1, :cond_6

    const/16 v1, 0x10

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->getFetchStaticEntitiesQuery()Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesQuery;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->hasStateChangeQuery()Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0x11

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->getStateChangeQuery()Lcom/google/geo/sidekick/Sidekick$StateChanges;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->hasExperimentOverrides()Z

    move-result v1

    if-eqz v1, :cond_8

    const/16 v1, 0x12

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->getExperimentOverrides()Lcom/google/geo/sidekick/Sidekick$ExperimentOverrides;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->cachedSize:I

    return v0
.end method

.method public getStateChangeQuery()Lcom/google/geo/sidekick/Sidekick$StateChanges;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->stateChangeQuery_:Lcom/google/geo/sidekick/Sidekick$StateChanges;

    return-object v0
.end method

.method public getStaticMapQuery()Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->staticMapQuery_:Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;

    return-object v0
.end method

.method public hasActionsQuery()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->hasActionsQuery:Z

    return v0
.end method

.method public hasConfigurationQuery()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->hasConfigurationQuery:Z

    return v0
.end method

.method public hasEntryQuery()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->hasEntryQuery:Z

    return v0
.end method

.method public hasExperimentOverrides()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->hasExperimentOverrides:Z

    return v0
.end method

.method public hasFetchConfigurationQuery()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->hasFetchConfigurationQuery:Z

    return v0
.end method

.method public hasFetchStaticEntitiesQuery()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->hasFetchStaticEntitiesQuery:Z

    return v0
.end method

.method public hasSensorSignals()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->hasSensorSignals:Z

    return v0
.end method

.method public hasStateChangeQuery()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->hasStateChangeQuery:Z

    return v0
.end method

.method public hasStaticMapQuery()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->hasStaticMapQuery:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$RequestPayload;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$Configuration;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$Configuration;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->setConfigurationQuery(Lcom/google/geo/sidekick/Sidekick$Configuration;)Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$EntryQuery;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$EntryQuery;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->setEntryQuery(Lcom/google/geo/sidekick/Sidekick$EntryQuery;)Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$ActionsQuery;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$ActionsQuery;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->setActionsQuery(Lcom/google/geo/sidekick/Sidekick$ActionsQuery;)Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->setStaticMapQuery(Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;)Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$FetchConfigurationQuery;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$FetchConfigurationQuery;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->setFetchConfigurationQuery(Lcom/google/geo/sidekick/Sidekick$FetchConfigurationQuery;)Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    goto :goto_0

    :sswitch_6
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$SensorSignals;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->setSensorSignals(Lcom/google/geo/sidekick/Sidekick$SensorSignals;)Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    goto :goto_0

    :sswitch_7
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesQuery;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesQuery;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->setFetchStaticEntitiesQuery(Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesQuery;)Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    goto :goto_0

    :sswitch_8
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$StateChanges;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$StateChanges;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->setStateChangeQuery(Lcom/google/geo/sidekick/Sidekick$StateChanges;)Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    goto :goto_0

    :sswitch_9
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$ExperimentOverrides;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$ExperimentOverrides;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->setExperimentOverrides(Lcom/google/geo/sidekick/Sidekick$ExperimentOverrides;)Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x4a -> :sswitch_1
        0x52 -> :sswitch_2
        0x5a -> :sswitch_3
        0x6a -> :sswitch_4
        0x72 -> :sswitch_5
        0x7a -> :sswitch_6
        0x82 -> :sswitch_7
        0x8a -> :sswitch_8
        0x92 -> :sswitch_9
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public setActionsQuery(Lcom/google/geo/sidekick/Sidekick$ActionsQuery;)Lcom/google/geo/sidekick/Sidekick$RequestPayload;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$ActionsQuery;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->hasActionsQuery:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->actionsQuery_:Lcom/google/geo/sidekick/Sidekick$ActionsQuery;

    return-object p0
.end method

.method public setConfigurationQuery(Lcom/google/geo/sidekick/Sidekick$Configuration;)Lcom/google/geo/sidekick/Sidekick$RequestPayload;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Configuration;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->hasConfigurationQuery:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->configurationQuery_:Lcom/google/geo/sidekick/Sidekick$Configuration;

    return-object p0
.end method

.method public setEntryQuery(Lcom/google/geo/sidekick/Sidekick$EntryQuery;)Lcom/google/geo/sidekick/Sidekick$RequestPayload;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$EntryQuery;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->hasEntryQuery:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->entryQuery_:Lcom/google/geo/sidekick/Sidekick$EntryQuery;

    return-object p0
.end method

.method public setExperimentOverrides(Lcom/google/geo/sidekick/Sidekick$ExperimentOverrides;)Lcom/google/geo/sidekick/Sidekick$RequestPayload;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$ExperimentOverrides;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->hasExperimentOverrides:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->experimentOverrides_:Lcom/google/geo/sidekick/Sidekick$ExperimentOverrides;

    return-object p0
.end method

.method public setFetchConfigurationQuery(Lcom/google/geo/sidekick/Sidekick$FetchConfigurationQuery;)Lcom/google/geo/sidekick/Sidekick$RequestPayload;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$FetchConfigurationQuery;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->hasFetchConfigurationQuery:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->fetchConfigurationQuery_:Lcom/google/geo/sidekick/Sidekick$FetchConfigurationQuery;

    return-object p0
.end method

.method public setFetchStaticEntitiesQuery(Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesQuery;)Lcom/google/geo/sidekick/Sidekick$RequestPayload;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesQuery;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->hasFetchStaticEntitiesQuery:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->fetchStaticEntitiesQuery_:Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesQuery;

    return-object p0
.end method

.method public setSensorSignals(Lcom/google/geo/sidekick/Sidekick$SensorSignals;)Lcom/google/geo/sidekick/Sidekick$RequestPayload;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SensorSignals;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->hasSensorSignals:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->sensorSignals_:Lcom/google/geo/sidekick/Sidekick$SensorSignals;

    return-object p0
.end method

.method public setStateChangeQuery(Lcom/google/geo/sidekick/Sidekick$StateChanges;)Lcom/google/geo/sidekick/Sidekick$RequestPayload;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$StateChanges;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->hasStateChangeQuery:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->stateChangeQuery_:Lcom/google/geo/sidekick/Sidekick$StateChanges;

    return-object p0
.end method

.method public setStaticMapQuery(Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;)Lcom/google/geo/sidekick/Sidekick$RequestPayload;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->hasStaticMapQuery:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->staticMapQuery_:Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->hasConfigurationQuery()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x9

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->getConfigurationQuery()Lcom/google/geo/sidekick/Sidekick$Configuration;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->hasEntryQuery()Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0xa

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->getEntryQuery()Lcom/google/geo/sidekick/Sidekick$EntryQuery;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->hasActionsQuery()Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0xb

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->getActionsQuery()Lcom/google/geo/sidekick/Sidekick$ActionsQuery;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->hasStaticMapQuery()Z

    move-result v0

    if-eqz v0, :cond_3

    const/16 v0, 0xd

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->getStaticMapQuery()Lcom/google/geo/sidekick/Sidekick$StaticMapQuery;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->hasFetchConfigurationQuery()Z

    move-result v0

    if-eqz v0, :cond_4

    const/16 v0, 0xe

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->getFetchConfigurationQuery()Lcom/google/geo/sidekick/Sidekick$FetchConfigurationQuery;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->hasSensorSignals()Z

    move-result v0

    if-eqz v0, :cond_5

    const/16 v0, 0xf

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->getSensorSignals()Lcom/google/geo/sidekick/Sidekick$SensorSignals;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->hasFetchStaticEntitiesQuery()Z

    move-result v0

    if-eqz v0, :cond_6

    const/16 v0, 0x10

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->getFetchStaticEntitiesQuery()Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesQuery;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->hasStateChangeQuery()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x11

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->getStateChangeQuery()Lcom/google/geo/sidekick/Sidekick$StateChanges;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->hasExperimentOverrides()Z

    move-result v0

    if-eqz v0, :cond_8

    const/16 v0, 0x12

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->getExperimentOverrides()Lcom/google/geo/sidekick/Sidekick$ExperimentOverrides;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_8
    return-void
.end method
