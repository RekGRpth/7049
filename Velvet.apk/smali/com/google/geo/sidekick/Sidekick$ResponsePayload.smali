.class public final Lcom/google/geo/sidekick/Sidekick$ResponsePayload;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ResponsePayload"
.end annotation


# instance fields
.field private actionsResponse_:Lcom/google/geo/sidekick/Sidekick$ActionsResponse;

.field private cachedSize:I

.field private entryResponse_:Lcom/google/geo/sidekick/Sidekick$EntryResponse;

.field private fetchConfigurationResponse_:Lcom/google/geo/sidekick/Sidekick$FetchConfigurationResponse;

.field private fetchStaticEntitiesResponse_:Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesResponse;

.field private hasActionsResponse:Z

.field private hasEntryResponse:Z

.field private hasFetchConfigurationResponse:Z

.field private hasFetchStaticEntitiesResponse:Z

.field private hasStaticMapResponse:Z

.field private staticMapResponse_:Lcom/google/geo/sidekick/Sidekick$StaticMapResponse;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->entryResponse_:Lcom/google/geo/sidekick/Sidekick$EntryResponse;

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->actionsResponse_:Lcom/google/geo/sidekick/Sidekick$ActionsResponse;

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->staticMapResponse_:Lcom/google/geo/sidekick/Sidekick$StaticMapResponse;

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->fetchConfigurationResponse_:Lcom/google/geo/sidekick/Sidekick$FetchConfigurationResponse;

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->fetchStaticEntitiesResponse_:Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesResponse;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getActionsResponse()Lcom/google/geo/sidekick/Sidekick$ActionsResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->actionsResponse_:Lcom/google/geo/sidekick/Sidekick$ActionsResponse;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->cachedSize:I

    return v0
.end method

.method public getEntryResponse()Lcom/google/geo/sidekick/Sidekick$EntryResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->entryResponse_:Lcom/google/geo/sidekick/Sidekick$EntryResponse;

    return-object v0
.end method

.method public getFetchConfigurationResponse()Lcom/google/geo/sidekick/Sidekick$FetchConfigurationResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->fetchConfigurationResponse_:Lcom/google/geo/sidekick/Sidekick$FetchConfigurationResponse;

    return-object v0
.end method

.method public getFetchStaticEntitiesResponse()Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->fetchStaticEntitiesResponse_:Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesResponse;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->hasEntryResponse()Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->getEntryResponse()Lcom/google/geo/sidekick/Sidekick$EntryResponse;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->hasStaticMapResponse()Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v1, 0x9

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->getStaticMapResponse()Lcom/google/geo/sidekick/Sidekick$StaticMapResponse;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->hasFetchConfigurationResponse()Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v1, 0xa

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->getFetchConfigurationResponse()Lcom/google/geo/sidekick/Sidekick$FetchConfigurationResponse;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->hasActionsResponse()Z

    move-result v1

    if-eqz v1, :cond_3

    const/16 v1, 0xb

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->getActionsResponse()Lcom/google/geo/sidekick/Sidekick$ActionsResponse;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->hasFetchStaticEntitiesResponse()Z

    move-result v1

    if-eqz v1, :cond_4

    const/16 v1, 0xc

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->getFetchStaticEntitiesResponse()Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesResponse;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->cachedSize:I

    return v0
.end method

.method public getStaticMapResponse()Lcom/google/geo/sidekick/Sidekick$StaticMapResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->staticMapResponse_:Lcom/google/geo/sidekick/Sidekick$StaticMapResponse;

    return-object v0
.end method

.method public hasActionsResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->hasActionsResponse:Z

    return v0
.end method

.method public hasEntryResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->hasEntryResponse:Z

    return v0
.end method

.method public hasFetchConfigurationResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->hasFetchConfigurationResponse:Z

    return v0
.end method

.method public hasFetchStaticEntitiesResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->hasFetchStaticEntitiesResponse:Z

    return v0
.end method

.method public hasStaticMapResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->hasStaticMapResponse:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$ResponsePayload;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$EntryResponse;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->setEntryResponse(Lcom/google/geo/sidekick/Sidekick$EntryResponse;)Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$StaticMapResponse;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$StaticMapResponse;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->setStaticMapResponse(Lcom/google/geo/sidekick/Sidekick$StaticMapResponse;)Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$FetchConfigurationResponse;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$FetchConfigurationResponse;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->setFetchConfigurationResponse(Lcom/google/geo/sidekick/Sidekick$FetchConfigurationResponse;)Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$ActionsResponse;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$ActionsResponse;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->setActionsResponse(Lcom/google/geo/sidekick/Sidekick$ActionsResponse;)Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesResponse;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesResponse;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->setFetchStaticEntitiesResponse(Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesResponse;)Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x42 -> :sswitch_1
        0x4a -> :sswitch_2
        0x52 -> :sswitch_3
        0x5a -> :sswitch_4
        0x62 -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    move-result-object v0

    return-object v0
.end method

.method public setActionsResponse(Lcom/google/geo/sidekick/Sidekick$ActionsResponse;)Lcom/google/geo/sidekick/Sidekick$ResponsePayload;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$ActionsResponse;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->hasActionsResponse:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->actionsResponse_:Lcom/google/geo/sidekick/Sidekick$ActionsResponse;

    return-object p0
.end method

.method public setEntryResponse(Lcom/google/geo/sidekick/Sidekick$EntryResponse;)Lcom/google/geo/sidekick/Sidekick$ResponsePayload;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$EntryResponse;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->hasEntryResponse:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->entryResponse_:Lcom/google/geo/sidekick/Sidekick$EntryResponse;

    return-object p0
.end method

.method public setFetchConfigurationResponse(Lcom/google/geo/sidekick/Sidekick$FetchConfigurationResponse;)Lcom/google/geo/sidekick/Sidekick$ResponsePayload;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$FetchConfigurationResponse;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->hasFetchConfigurationResponse:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->fetchConfigurationResponse_:Lcom/google/geo/sidekick/Sidekick$FetchConfigurationResponse;

    return-object p0
.end method

.method public setFetchStaticEntitiesResponse(Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesResponse;)Lcom/google/geo/sidekick/Sidekick$ResponsePayload;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesResponse;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->hasFetchStaticEntitiesResponse:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->fetchStaticEntitiesResponse_:Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesResponse;

    return-object p0
.end method

.method public setStaticMapResponse(Lcom/google/geo/sidekick/Sidekick$StaticMapResponse;)Lcom/google/geo/sidekick/Sidekick$ResponsePayload;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$StaticMapResponse;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->hasStaticMapResponse:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->staticMapResponse_:Lcom/google/geo/sidekick/Sidekick$StaticMapResponse;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->hasEntryResponse()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->getEntryResponse()Lcom/google/geo/sidekick/Sidekick$EntryResponse;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->hasStaticMapResponse()Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x9

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->getStaticMapResponse()Lcom/google/geo/sidekick/Sidekick$StaticMapResponse;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->hasFetchConfigurationResponse()Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0xa

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->getFetchConfigurationResponse()Lcom/google/geo/sidekick/Sidekick$FetchConfigurationResponse;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->hasActionsResponse()Z

    move-result v0

    if-eqz v0, :cond_3

    const/16 v0, 0xb

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->getActionsResponse()Lcom/google/geo/sidekick/Sidekick$ActionsResponse;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->hasFetchStaticEntitiesResponse()Z

    move-result v0

    if-eqz v0, :cond_4

    const/16 v0, 0xc

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->getFetchStaticEntitiesResponse()Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesResponse;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_4
    return-void
.end method
