.class public final Lcom/google/geo/sidekick/Sidekick$SensorSignals;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SensorSignals"
.end annotation


# instance fields
.field private cachedSize:I

.field private hasHasRearFacingCamera:Z

.field private hasLayoutInfo:Z

.field private hasLocale:Z

.field private hasNumberWidgetsInstalled:Z

.field private hasRearFacingCamera_:Z

.field private hasStationaryTimeSeconds:Z

.field private hasTimestampSeconds:Z

.field private hasTimezoneOffsetSeconds:Z

.field private hasWifiEnabled:Z

.field private layoutInfo_:Lcom/google/geo/sidekick/Sidekick$LayoutInfo;

.field private locale_:Ljava/lang/String;

.field private numberWidgetsInstalled_:I

.field private stationaryTimeSeconds_:I

.field private timestampSeconds_:J

.field private timestampedLocation_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;",
            ">;"
        }
    .end annotation
.end field

.field private timezoneOffsetSeconds_:I

.field private wifiEnabled_:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->timestampedLocation_:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->locale_:Ljava/lang/String;

    iput v2, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->stationaryTimeSeconds_:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->wifiEnabled_:Z

    iput-boolean v2, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->hasRearFacingCamera_:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->layoutInfo_:Lcom/google/geo/sidekick/Sidekick$LayoutInfo;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->timestampSeconds_:J

    iput v2, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->timezoneOffsetSeconds_:I

    iput v2, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->numberWidgetsInstalled_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addTimestampedLocation(Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;)Lcom/google/geo/sidekick/Sidekick$SensorSignals;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->timestampedLocation_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->timestampedLocation_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->timestampedLocation_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->cachedSize:I

    return v0
.end method

.method public getHasRearFacingCamera()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->hasRearFacingCamera_:Z

    return v0
.end method

.method public getLayoutInfo()Lcom/google/geo/sidekick/Sidekick$LayoutInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->layoutInfo_:Lcom/google/geo/sidekick/Sidekick$LayoutInfo;

    return-object v0
.end method

.method public getLocale()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->locale_:Ljava/lang/String;

    return-object v0
.end method

.method public getNumberWidgetsInstalled()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->numberWidgetsInstalled_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 6

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->getTimestampedLocationList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;

    const/4 v3, 0x1

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->hasLocale()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->getLocale()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->hasStationaryTimeSeconds()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->getStationaryTimeSeconds()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->hasWifiEnabled()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->getWifiEnabled()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->hasLayoutInfo()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->getLayoutInfo()Lcom/google/geo/sidekick/Sidekick$LayoutInfo;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->hasTimestampSeconds()Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->getTimestampSeconds()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->hasTimezoneOffsetSeconds()Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v3, 0x7

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->getTimezoneOffsetSeconds()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_6
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->hasHasRearFacingCamera()Z

    move-result v3

    if-eqz v3, :cond_7

    const/16 v3, 0x8

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->getHasRearFacingCamera()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_7
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->hasNumberWidgetsInstalled()Z

    move-result v3

    if-eqz v3, :cond_8

    const/16 v3, 0x9

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->getNumberWidgetsInstalled()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_8
    iput v2, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->cachedSize:I

    return v2
.end method

.method public getStationaryTimeSeconds()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->stationaryTimeSeconds_:I

    return v0
.end method

.method public getTimestampSeconds()J
    .locals 2

    iget-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->timestampSeconds_:J

    return-wide v0
.end method

.method public getTimestampedLocationList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->timestampedLocation_:Ljava/util/List;

    return-object v0
.end method

.method public getTimezoneOffsetSeconds()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->timezoneOffsetSeconds_:I

    return v0
.end method

.method public getWifiEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->wifiEnabled_:Z

    return v0
.end method

.method public hasHasRearFacingCamera()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->hasHasRearFacingCamera:Z

    return v0
.end method

.method public hasLayoutInfo()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->hasLayoutInfo:Z

    return v0
.end method

.method public hasLocale()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->hasLocale:Z

    return v0
.end method

.method public hasNumberWidgetsInstalled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->hasNumberWidgetsInstalled:Z

    return v0
.end method

.method public hasStationaryTimeSeconds()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->hasStationaryTimeSeconds:Z

    return v0
.end method

.method public hasTimestampSeconds()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->hasTimestampSeconds:Z

    return v0
.end method

.method public hasTimezoneOffsetSeconds()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->hasTimezoneOffsetSeconds:Z

    return v0
.end method

.method public hasWifiEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->hasWifiEnabled:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$SensorSignals;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->addTimestampedLocation(Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;)Lcom/google/geo/sidekick/Sidekick$SensorSignals;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->setLocale(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SensorSignals;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->setStationaryTimeSeconds(I)Lcom/google/geo/sidekick/Sidekick$SensorSignals;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->setWifiEnabled(Z)Lcom/google/geo/sidekick/Sidekick$SensorSignals;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$LayoutInfo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->setLayoutInfo(Lcom/google/geo/sidekick/Sidekick$LayoutInfo;)Lcom/google/geo/sidekick/Sidekick$SensorSignals;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->setTimestampSeconds(J)Lcom/google/geo/sidekick/Sidekick$SensorSignals;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->setTimezoneOffsetSeconds(I)Lcom/google/geo/sidekick/Sidekick$SensorSignals;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->setHasRearFacingCamera(Z)Lcom/google/geo/sidekick/Sidekick$SensorSignals;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->setNumberWidgetsInstalled(I)Lcom/google/geo/sidekick/Sidekick$SensorSignals;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$SensorSignals;

    move-result-object v0

    return-object v0
.end method

.method public setHasRearFacingCamera(Z)Lcom/google/geo/sidekick/Sidekick$SensorSignals;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->hasHasRearFacingCamera:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->hasRearFacingCamera_:Z

    return-object p0
.end method

.method public setLayoutInfo(Lcom/google/geo/sidekick/Sidekick$LayoutInfo;)Lcom/google/geo/sidekick/Sidekick$SensorSignals;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$LayoutInfo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->hasLayoutInfo:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->layoutInfo_:Lcom/google/geo/sidekick/Sidekick$LayoutInfo;

    return-object p0
.end method

.method public setLocale(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SensorSignals;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->hasLocale:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->locale_:Ljava/lang/String;

    return-object p0
.end method

.method public setNumberWidgetsInstalled(I)Lcom/google/geo/sidekick/Sidekick$SensorSignals;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->hasNumberWidgetsInstalled:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->numberWidgetsInstalled_:I

    return-object p0
.end method

.method public setStationaryTimeSeconds(I)Lcom/google/geo/sidekick/Sidekick$SensorSignals;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->hasStationaryTimeSeconds:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->stationaryTimeSeconds_:I

    return-object p0
.end method

.method public setTimestampSeconds(J)Lcom/google/geo/sidekick/Sidekick$SensorSignals;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->hasTimestampSeconds:Z

    iput-wide p1, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->timestampSeconds_:J

    return-object p0
.end method

.method public setTimezoneOffsetSeconds(I)Lcom/google/geo/sidekick/Sidekick$SensorSignals;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->hasTimezoneOffsetSeconds:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->timezoneOffsetSeconds_:I

    return-object p0
.end method

.method public setWifiEnabled(Z)Lcom/google/geo/sidekick/Sidekick$SensorSignals;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->hasWifiEnabled:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->wifiEnabled_:Z

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 5
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->getTimestampedLocationList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;

    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->hasLocale()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->getLocale()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->hasStationaryTimeSeconds()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->getStationaryTimeSeconds()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->hasWifiEnabled()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->getWifiEnabled()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->hasLayoutInfo()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->getLayoutInfo()Lcom/google/geo/sidekick/Sidekick$LayoutInfo;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->hasTimestampSeconds()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->getTimestampSeconds()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->hasTimezoneOffsetSeconds()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->getTimezoneOffsetSeconds()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->hasHasRearFacingCamera()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->getHasRearFacingCamera()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->hasNumberWidgetsInstalled()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SensorSignals;->getNumberWidgetsInstalled()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_8
    return-void
.end method
