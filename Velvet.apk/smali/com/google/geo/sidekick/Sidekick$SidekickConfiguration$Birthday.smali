.class public final Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Birthday;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Birthday"
.end annotation


# instance fields
.field private cachedSize:I

.field private hasShowFriend:Z

.field private hasShowOwn:Z

.field private showFriend_:Z

.field private showOwn_:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Birthday;->showFriend_:Z

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Birthday;->showOwn_:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Birthday;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Birthday;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Birthday;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Birthday;->cachedSize:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Birthday;->hasShowFriend()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Birthday;->getShowFriend()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Birthday;->hasShowOwn()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Birthday;->getShowOwn()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Birthday;->cachedSize:I

    return v0
.end method

.method public getShowFriend()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Birthday;->showFriend_:Z

    return v0
.end method

.method public getShowOwn()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Birthday;->showOwn_:Z

    return v0
.end method

.method public hasShowFriend()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Birthday;->hasShowFriend:Z

    return v0
.end method

.method public hasShowOwn()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Birthday;->hasShowOwn:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Birthday;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Birthday;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Birthday;->setShowFriend(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Birthday;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Birthday;->setShowOwn(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Birthday;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Birthday;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Birthday;

    move-result-object v0

    return-object v0
.end method

.method public setShowFriend(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Birthday;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Birthday;->hasShowFriend:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Birthday;->showFriend_:Z

    return-object p0
.end method

.method public setShowOwn(Z)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Birthday;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Birthday;->hasShowOwn:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Birthday;->showOwn_:Z

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Birthday;->hasShowFriend()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Birthday;->getShowFriend()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Birthday;->hasShowOwn()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Birthday;->getShowOwn()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_1
    return-void
.end method
