.class public final Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SidekickConfigurationChanges"
.end annotation


# instance fields
.field private adds_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

.field private cachedSize:I

.field private deletes_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

.field private hasAdds:Z

.field private hasDeletes:Z

.field private hasUpdates:Z

.field private updates_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;->updates_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;->adds_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;->deletes_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getAdds()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;->adds_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;->cachedSize:I

    return v0
.end method

.method public getDeletes()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;->deletes_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;->hasUpdates()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;->getUpdates()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;->hasAdds()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;->getAdds()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;->hasDeletes()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;->getDeletes()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;->cachedSize:I

    return v0
.end method

.method public getUpdates()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;->updates_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    return-object v0
.end method

.method public hasAdds()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;->hasAdds:Z

    return v0
.end method

.method public hasDeletes()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;->hasDeletes:Z

    return v0
.end method

.method public hasUpdates()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;->hasUpdates:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;->setUpdates(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;)Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;->setAdds(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;)Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;->setDeletes(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;)Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;

    move-result-object v0

    return-object v0
.end method

.method public setAdds(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;)Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;->hasAdds:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;->adds_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    return-object p0
.end method

.method public setDeletes(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;)Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;->hasDeletes:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;->deletes_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    return-object p0
.end method

.method public setUpdates(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;)Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;->hasUpdates:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;->updates_:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;->hasUpdates()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;->getUpdates()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;->hasAdds()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;->getAdds()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;->hasDeletes()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;->getDeletes()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    return-void
.end method
