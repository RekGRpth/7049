.class public final Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SportEntity"
.end annotation


# instance fields
.field private baseballScore_:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$BaseballScore;

.field private cachedSize:I

.field private hasBaseballScore:Z

.field private hasIsUserInterest:Z

.field private hasIsWinner:Z

.field private hasLogoUrl:Z

.field private hasName:Z

.field private hasPrimaryKey:Z

.field private hasScore:Z

.field private hasSpriteOffsetFromLeft:Z

.field private hasSpriteOffsetFromTop:Z

.field private isUserInterest_:Z

.field private isWinner_:Z

.field private logoUrl_:Ljava/lang/String;

.field private name_:Ljava/lang/String;

.field private primaryKey_:Ljava/lang/String;

.field private score_:Ljava/lang/String;

.field private spriteOffsetFromLeft_:I

.field private spriteOffsetFromTop_:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->name_:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->isWinner_:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->logoUrl_:Ljava/lang/String;

    iput v1, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->spriteOffsetFromTop_:I

    iput v1, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->spriteOffsetFromLeft_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->score_:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->baseballScore_:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$BaseballScore;

    iput-boolean v1, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->isUserInterest_:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->primaryKey_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getBaseballScore()Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$BaseballScore;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->baseballScore_:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$BaseballScore;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->cachedSize:I

    return v0
.end method

.method public getIsUserInterest()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->isUserInterest_:Z

    return v0
.end method

.method public getIsWinner()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->isWinner_:Z

    return v0
.end method

.method public getLogoUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->logoUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getPrimaryKey()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->primaryKey_:Ljava/lang/String;

    return-object v0
.end method

.method public getScore()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->score_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->hasName()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->hasIsWinner()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getIsWinner()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->hasSpriteOffsetFromTop()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getSpriteOffsetFromTop()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->hasSpriteOffsetFromLeft()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getSpriteOffsetFromLeft()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->hasScore()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getScore()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->hasBaseballScore()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getBaseballScore()Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$BaseballScore;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->hasIsUserInterest()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getIsUserInterest()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->hasLogoUrl()Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getLogoUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->hasPrimaryKey()Z

    move-result v1

    if-eqz v1, :cond_8

    const/16 v1, 0x9

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getPrimaryKey()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->cachedSize:I

    return v0
.end method

.method public getSpriteOffsetFromLeft()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->spriteOffsetFromLeft_:I

    return v0
.end method

.method public getSpriteOffsetFromTop()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->spriteOffsetFromTop_:I

    return v0
.end method

.method public hasBaseballScore()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->hasBaseballScore:Z

    return v0
.end method

.method public hasIsUserInterest()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->hasIsUserInterest:Z

    return v0
.end method

.method public hasIsWinner()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->hasIsWinner:Z

    return v0
.end method

.method public hasLogoUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->hasLogoUrl:Z

    return v0
.end method

.method public hasName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->hasName:Z

    return v0
.end method

.method public hasPrimaryKey()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->hasPrimaryKey:Z

    return v0
.end method

.method public hasScore()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->hasScore:Z

    return v0
.end method

.method public hasSpriteOffsetFromLeft()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->hasSpriteOffsetFromLeft:Z

    return v0
.end method

.method public hasSpriteOffsetFromTop()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->hasSpriteOffsetFromTop:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->setName(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->setIsWinner(Z)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->setSpriteOffsetFromTop(I)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->setSpriteOffsetFromLeft(I)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->setScore(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;

    goto :goto_0

    :sswitch_6
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$BaseballScore;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$BaseballScore;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->setBaseballScore(Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$BaseballScore;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->setIsUserInterest(Z)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->setLogoUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->setPrimaryKey(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;

    move-result-object v0

    return-object v0
.end method

.method public setBaseballScore(Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$BaseballScore;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$BaseballScore;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->hasBaseballScore:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->baseballScore_:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$BaseballScore;

    return-object p0
.end method

.method public setIsUserInterest(Z)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->hasIsUserInterest:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->isUserInterest_:Z

    return-object p0
.end method

.method public setIsWinner(Z)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->hasIsWinner:Z

    iput-boolean p1, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->isWinner_:Z

    return-object p0
.end method

.method public setLogoUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->hasLogoUrl:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->logoUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->hasName:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->name_:Ljava/lang/String;

    return-object p0
.end method

.method public setPrimaryKey(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->hasPrimaryKey:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->primaryKey_:Ljava/lang/String;

    return-object p0
.end method

.method public setScore(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->hasScore:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->score_:Ljava/lang/String;

    return-object p0
.end method

.method public setSpriteOffsetFromLeft(I)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->hasSpriteOffsetFromLeft:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->spriteOffsetFromLeft_:I

    return-object p0
.end method

.method public setSpriteOffsetFromTop(I)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->hasSpriteOffsetFromTop:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->spriteOffsetFromTop_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->hasName()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->hasIsWinner()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getIsWinner()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->hasSpriteOffsetFromTop()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getSpriteOffsetFromTop()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->hasSpriteOffsetFromLeft()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getSpriteOffsetFromLeft()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->hasScore()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getScore()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->hasBaseballScore()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getBaseballScore()Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$BaseballScore;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->hasIsUserInterest()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getIsUserInterest()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->hasLogoUrl()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getLogoUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->hasPrimaryKey()Z

    move-result v0

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getPrimaryKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_8
    return-void
.end method
