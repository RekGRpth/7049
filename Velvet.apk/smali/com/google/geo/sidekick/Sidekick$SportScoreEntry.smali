.class public final Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SportScoreEntry"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;,
        Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$BaseballScore;,
        Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;
    }
.end annotation


# instance fields
.field private boxScoresUrl_:Ljava/lang/String;

.field private cachedSize:I

.field private hasBoxScoresUrl:Z

.field private hasLiveStreamUrl:Z

.field private hasLiveUpdateUrl:Z

.field private hasLocation:Z

.field private hasNumRemainingPeriods:Z

.field private hasOneboxData:Z

.field private hasPreviewUrl:Z

.field private hasRecapUrl:Z

.field private hasSource:Z

.field private hasSport:Z

.field private hasSpriteUrl:Z

.field private hasStartTimeSeconds:Z

.field private hasStartTimeZone:Z

.field private hasStatus:Z

.field private hasStatusCode:Z

.field private hasTicketsUrl:Z

.field private hasWebSearchQuery:Z

.field private liveStreamUrl_:Ljava/lang/String;

.field private liveUpdateUrl_:Ljava/lang/String;

.field private location_:Ljava/lang/String;

.field private numRemainingPeriods_:I

.field private oneboxData_:Lcom/google/geo/sidekick/Sidekick$AuxiliaryMessage;

.field private period_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;",
            ">;"
        }
    .end annotation
.end field

.field private previewUrl_:Ljava/lang/String;

.field private recapUrl_:Ljava/lang/String;

.field private source_:I

.field private sportEntity_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;",
            ">;"
        }
    .end annotation
.end field

.field private sport_:I

.field private spriteUrl_:Ljava/lang/String;

.field private startTimeSeconds_:J

.field private startTimeZone_:Ljava/lang/String;

.field private statusCode_:I

.field private status_:Ljava/lang/String;

.field private ticketsUrl_:Ljava/lang/String;

.field private webSearchQuery_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput v2, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->sport_:I

    iput v2, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->statusCode_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->status_:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->startTimeSeconds_:J

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->startTimeZone_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->location_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->period_:Ljava/util/List;

    iput v2, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->numRemainingPeriods_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->sportEntity_:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->spriteUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->recapUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->boxScoresUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->liveUpdateUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->liveStreamUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->previewUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->ticketsUrl_:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->oneboxData_:Lcom/google/geo/sidekick/Sidekick$AuxiliaryMessage;

    iput v2, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->source_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->webSearchQuery_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addPeriod(Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->period_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->period_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->period_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addSportEntity(Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->sportEntity_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->sportEntity_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->sportEntity_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getBoxScoresUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->boxScoresUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->cachedSize:I

    return v0
.end method

.method public getLiveStreamUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->liveStreamUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getLiveUpdateUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->liveUpdateUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getLocation()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->location_:Ljava/lang/String;

    return-object v0
.end method

.method public getNumRemainingPeriods()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->numRemainingPeriods_:I

    return v0
.end method

.method public getOneboxData()Lcom/google/geo/sidekick/Sidekick$AuxiliaryMessage;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->oneboxData_:Lcom/google/geo/sidekick/Sidekick$AuxiliaryMessage;

    return-object v0
.end method

.method public getPeriod(I)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->period_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;

    return-object v0
.end method

.method public getPeriodCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->period_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getPeriodList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->period_:Ljava/util/List;

    return-object v0
.end method

.method public getPreviewUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->previewUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getRecapUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->recapUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasSport()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getSport()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasStatusCode()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getStatusCode()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasStatus()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getStatus()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasStartTimeSeconds()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getStartTimeSeconds()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasStartTimeZone()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getStartTimeZone()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasLocation()Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getLocation()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getPeriodList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;

    const/4 v3, 0x7

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_6
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasNumRemainingPeriods()Z

    move-result v3

    if-eqz v3, :cond_7

    const/16 v3, 0x8

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getNumRemainingPeriods()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_7
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getSportEntityList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;

    const/16 v3, 0x9

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_1

    :cond_8
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasSpriteUrl()Z

    move-result v3

    if-eqz v3, :cond_9

    const/16 v3, 0xa

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getSpriteUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_9
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasRecapUrl()Z

    move-result v3

    if-eqz v3, :cond_a

    const/16 v3, 0xb

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getRecapUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_a
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasBoxScoresUrl()Z

    move-result v3

    if-eqz v3, :cond_b

    const/16 v3, 0xc

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getBoxScoresUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_b
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasLiveUpdateUrl()Z

    move-result v3

    if-eqz v3, :cond_c

    const/16 v3, 0xd

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getLiveUpdateUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_c
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasLiveStreamUrl()Z

    move-result v3

    if-eqz v3, :cond_d

    const/16 v3, 0xe

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getLiveStreamUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_d
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasPreviewUrl()Z

    move-result v3

    if-eqz v3, :cond_e

    const/16 v3, 0xf

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getPreviewUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_e
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasTicketsUrl()Z

    move-result v3

    if-eqz v3, :cond_f

    const/16 v3, 0x10

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getTicketsUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_f
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasOneboxData()Z

    move-result v3

    if-eqz v3, :cond_10

    const/16 v3, 0x11

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getOneboxData()Lcom/google/geo/sidekick/Sidekick$AuxiliaryMessage;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_10
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasSource()Z

    move-result v3

    if-eqz v3, :cond_11

    const/16 v3, 0x12

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getSource()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_11
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasWebSearchQuery()Z

    move-result v3

    if-eqz v3, :cond_12

    const/16 v3, 0x13

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getWebSearchQuery()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_12
    iput v2, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->cachedSize:I

    return v2
.end method

.method public getSource()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->source_:I

    return v0
.end method

.method public getSport()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->sport_:I

    return v0
.end method

.method public getSportEntity(I)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->sportEntity_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;

    return-object v0
.end method

.method public getSportEntityCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->sportEntity_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getSportEntityList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->sportEntity_:Ljava/util/List;

    return-object v0
.end method

.method public getSpriteUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->spriteUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getStartTimeSeconds()J
    .locals 2

    iget-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->startTimeSeconds_:J

    return-wide v0
.end method

.method public getStartTimeZone()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->startTimeZone_:Ljava/lang/String;

    return-object v0
.end method

.method public getStatus()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->status_:Ljava/lang/String;

    return-object v0
.end method

.method public getStatusCode()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->statusCode_:I

    return v0
.end method

.method public getTicketsUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->ticketsUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getWebSearchQuery()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->webSearchQuery_:Ljava/lang/String;

    return-object v0
.end method

.method public hasBoxScoresUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasBoxScoresUrl:Z

    return v0
.end method

.method public hasLiveStreamUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasLiveStreamUrl:Z

    return v0
.end method

.method public hasLiveUpdateUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasLiveUpdateUrl:Z

    return v0
.end method

.method public hasLocation()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasLocation:Z

    return v0
.end method

.method public hasNumRemainingPeriods()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasNumRemainingPeriods:Z

    return v0
.end method

.method public hasOneboxData()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasOneboxData:Z

    return v0
.end method

.method public hasPreviewUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasPreviewUrl:Z

    return v0
.end method

.method public hasRecapUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasRecapUrl:Z

    return v0
.end method

.method public hasSource()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasSource:Z

    return v0
.end method

.method public hasSport()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasSport:Z

    return v0
.end method

.method public hasSpriteUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasSpriteUrl:Z

    return v0
.end method

.method public hasStartTimeSeconds()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasStartTimeSeconds:Z

    return v0
.end method

.method public hasStartTimeZone()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasStartTimeZone:Z

    return v0
.end method

.method public hasStatus()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasStatus:Z

    return v0
.end method

.method public hasStatusCode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasStatusCode:Z

    return v0
.end method

.method public hasTicketsUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasTicketsUrl:Z

    return v0
.end method

.method public hasWebSearchQuery()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasWebSearchQuery:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->setSport(I)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->setStatusCode(I)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->setStatus(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->setStartTimeSeconds(J)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->setStartTimeZone(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->setLocation(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    goto :goto_0

    :sswitch_7
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->addPeriod(Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->setNumRemainingPeriods(I)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    goto :goto_0

    :sswitch_9
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->addSportEntity(Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->setSpriteUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->setRecapUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->setBoxScoresUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->setLiveUpdateUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    goto :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->setLiveStreamUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->setPreviewUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->setTicketsUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    goto/16 :goto_0

    :sswitch_11
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$AuxiliaryMessage;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$AuxiliaryMessage;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->setOneboxData(Lcom/google/geo/sidekick/Sidekick$AuxiliaryMessage;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->setSource(I)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    goto/16 :goto_0

    :sswitch_13
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->setWebSearchQuery(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
        0x8a -> :sswitch_11
        0x90 -> :sswitch_12
        0x9a -> :sswitch_13
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    move-result-object v0

    return-object v0
.end method

.method public setBoxScoresUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasBoxScoresUrl:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->boxScoresUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setLiveStreamUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasLiveStreamUrl:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->liveStreamUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setLiveUpdateUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasLiveUpdateUrl:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->liveUpdateUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setLocation(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasLocation:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->location_:Ljava/lang/String;

    return-object p0
.end method

.method public setNumRemainingPeriods(I)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasNumRemainingPeriods:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->numRemainingPeriods_:I

    return-object p0
.end method

.method public setOneboxData(Lcom/google/geo/sidekick/Sidekick$AuxiliaryMessage;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$AuxiliaryMessage;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasOneboxData:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->oneboxData_:Lcom/google/geo/sidekick/Sidekick$AuxiliaryMessage;

    return-object p0
.end method

.method public setPreviewUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasPreviewUrl:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->previewUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setRecapUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasRecapUrl:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->recapUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setSource(I)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasSource:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->source_:I

    return-object p0
.end method

.method public setSport(I)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasSport:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->sport_:I

    return-object p0
.end method

.method public setSpriteUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasSpriteUrl:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->spriteUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setStartTimeSeconds(J)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasStartTimeSeconds:Z

    iput-wide p1, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->startTimeSeconds_:J

    return-object p0
.end method

.method public setStartTimeZone(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasStartTimeZone:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->startTimeZone_:Ljava/lang/String;

    return-object p0
.end method

.method public setStatus(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasStatus:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->status_:Ljava/lang/String;

    return-object p0
.end method

.method public setStatusCode(I)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasStatusCode:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->statusCode_:I

    return-object p0
.end method

.method public setTicketsUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasTicketsUrl:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->ticketsUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setWebSearchQuery(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasWebSearchQuery:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->webSearchQuery_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 5
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasSport()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getSport()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasStatusCode()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getStatusCode()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasStatus()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getStatus()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasStartTimeSeconds()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getStartTimeSeconds()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasStartTimeZone()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getStartTimeZone()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasLocation()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getLocation()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getPeriodList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$Period;

    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_6
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasNumRemainingPeriods()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getNumRemainingPeriods()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getSportEntityList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;

    const/16 v2, 0x9

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_1

    :cond_8
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasSpriteUrl()Z

    move-result v2

    if-eqz v2, :cond_9

    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getSpriteUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasRecapUrl()Z

    move-result v2

    if-eqz v2, :cond_a

    const/16 v2, 0xb

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getRecapUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasBoxScoresUrl()Z

    move-result v2

    if-eqz v2, :cond_b

    const/16 v2, 0xc

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getBoxScoresUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_b
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasLiveUpdateUrl()Z

    move-result v2

    if-eqz v2, :cond_c

    const/16 v2, 0xd

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getLiveUpdateUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_c
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasLiveStreamUrl()Z

    move-result v2

    if-eqz v2, :cond_d

    const/16 v2, 0xe

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getLiveStreamUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_d
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasPreviewUrl()Z

    move-result v2

    if-eqz v2, :cond_e

    const/16 v2, 0xf

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getPreviewUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_e
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasTicketsUrl()Z

    move-result v2

    if-eqz v2, :cond_f

    const/16 v2, 0x10

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getTicketsUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_f
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasOneboxData()Z

    move-result v2

    if-eqz v2, :cond_10

    const/16 v2, 0x11

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getOneboxData()Lcom/google/geo/sidekick/Sidekick$AuxiliaryMessage;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_10
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasSource()Z

    move-result v2

    if-eqz v2, :cond_11

    const/16 v2, 0x12

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getSource()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_11
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->hasWebSearchQuery()Z

    move-result v2

    if-eqz v2, :cond_12

    const/16 v2, 0x13

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getWebSearchQuery()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_12
    return-void
.end method
