.class public final Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TimestampedLocation"
.end annotation


# instance fields
.field private accuracyMeters_:I

.field private cachedSize:I

.field private dEPRECATEDTimezoneOffsetSeconds_:I

.field private hasAccuracyMeters:Z

.field private hasDEPRECATEDTimezoneOffsetSeconds:Z

.field private hasLocation:Z

.field private hasNetworkLocationType:Z

.field private hasProvider:Z

.field private hasSource:Z

.field private hasTimestampSeconds:Z

.field private location_:Lcom/google/geo/sidekick/Sidekick$Location;

.field private networkLocationType_:Ljava/lang/String;

.field private provider_:Ljava/lang/String;

.field private source_:I

.field private timestampSeconds_:J


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->location_:Lcom/google/geo/sidekick/Sidekick$Location;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->timestampSeconds_:J

    iput v2, p0, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->accuracyMeters_:I

    iput v2, p0, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->dEPRECATEDTimezoneOffsetSeconds_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->provider_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->networkLocationType_:Ljava/lang/String;

    iput v2, p0, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->source_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getAccuracyMeters()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->accuracyMeters_:I

    return v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->cachedSize:I

    return v0
.end method

.method public getDEPRECATEDTimezoneOffsetSeconds()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->dEPRECATEDTimezoneOffsetSeconds_:I

    return v0
.end method

.method public getLocation()Lcom/google/geo/sidekick/Sidekick$Location;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->location_:Lcom/google/geo/sidekick/Sidekick$Location;

    return-object v0
.end method

.method public getNetworkLocationType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->networkLocationType_:Ljava/lang/String;

    return-object v0
.end method

.method public getProvider()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->provider_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->hasLocation()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->hasTimestampSeconds()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->getTimestampSeconds()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->hasAccuracyMeters()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->getAccuracyMeters()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->hasDEPRECATEDTimezoneOffsetSeconds()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->getDEPRECATEDTimezoneOffsetSeconds()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->hasProvider()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->getProvider()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->hasNetworkLocationType()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->getNetworkLocationType()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->hasSource()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->getSource()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->cachedSize:I

    return v0
.end method

.method public getSource()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->source_:I

    return v0
.end method

.method public getTimestampSeconds()J
    .locals 2

    iget-wide v0, p0, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->timestampSeconds_:J

    return-wide v0
.end method

.method public hasAccuracyMeters()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->hasAccuracyMeters:Z

    return v0
.end method

.method public hasDEPRECATEDTimezoneOffsetSeconds()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->hasDEPRECATEDTimezoneOffsetSeconds:Z

    return v0
.end method

.method public hasLocation()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->hasLocation:Z

    return v0
.end method

.method public hasNetworkLocationType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->hasNetworkLocationType:Z

    return v0
.end method

.method public hasProvider()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->hasProvider:Z

    return v0
.end method

.method public hasSource()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->hasSource:Z

    return v0
.end method

.method public hasTimestampSeconds()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->hasTimestampSeconds:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$Location;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->setLocation(Lcom/google/geo/sidekick/Sidekick$Location;)Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->setTimestampSeconds(J)Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->setAccuracyMeters(I)Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->setDEPRECATEDTimezoneOffsetSeconds(I)Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->setProvider(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->setNetworkLocationType(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->setSource(I)Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;

    move-result-object v0

    return-object v0
.end method

.method public setAccuracyMeters(I)Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->hasAccuracyMeters:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->accuracyMeters_:I

    return-object p0
.end method

.method public setDEPRECATEDTimezoneOffsetSeconds(I)Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->hasDEPRECATEDTimezoneOffsetSeconds:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->dEPRECATEDTimezoneOffsetSeconds_:I

    return-object p0
.end method

.method public setLocation(Lcom/google/geo/sidekick/Sidekick$Location;)Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Location;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->hasLocation:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->location_:Lcom/google/geo/sidekick/Sidekick$Location;

    return-object p0
.end method

.method public setNetworkLocationType(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->hasNetworkLocationType:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->networkLocationType_:Ljava/lang/String;

    return-object p0
.end method

.method public setProvider(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->hasProvider:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->provider_:Ljava/lang/String;

    return-object p0
.end method

.method public setSource(I)Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->hasSource:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->source_:I

    return-object p0
.end method

.method public setTimestampSeconds(J)Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->hasTimestampSeconds:Z

    iput-wide p1, p0, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->timestampSeconds_:J

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->hasLocation()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->hasTimestampSeconds()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->getTimestampSeconds()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->hasAccuracyMeters()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->getAccuracyMeters()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->hasDEPRECATEDTimezoneOffsetSeconds()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->getDEPRECATEDTimezoneOffsetSeconds()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->hasProvider()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->getProvider()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->hasNetworkLocationType()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->getNetworkLocationType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->hasSource()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->getSource()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_6
    return-void
.end method
