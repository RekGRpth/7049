.class public final Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup$Departure;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Departure"
.end annotation


# instance fields
.field private cachedSize:I

.field private hasRelativeArrivalTimeSeconds:Z

.field private hasRelativeDepartureTimeSeconds:Z

.field private relativeArrivalTimeSeconds_:I

.field private relativeDepartureTimeSeconds_:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup$Departure;->relativeArrivalTimeSeconds_:I

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup$Departure;->relativeDepartureTimeSeconds_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup$Departure;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup$Departure;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup$Departure;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup$Departure;->cachedSize:I

    return v0
.end method

.method public getRelativeArrivalTimeSeconds()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup$Departure;->relativeArrivalTimeSeconds_:I

    return v0
.end method

.method public getRelativeDepartureTimeSeconds()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup$Departure;->relativeDepartureTimeSeconds_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup$Departure;->hasRelativeArrivalTimeSeconds()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup$Departure;->getRelativeArrivalTimeSeconds()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup$Departure;->hasRelativeDepartureTimeSeconds()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup$Departure;->getRelativeDepartureTimeSeconds()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup$Departure;->cachedSize:I

    return v0
.end method

.method public hasRelativeArrivalTimeSeconds()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup$Departure;->hasRelativeArrivalTimeSeconds:Z

    return v0
.end method

.method public hasRelativeDepartureTimeSeconds()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup$Departure;->hasRelativeDepartureTimeSeconds:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup$Departure;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup$Departure;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup$Departure;->setRelativeArrivalTimeSeconds(I)Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup$Departure;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup$Departure;->setRelativeDepartureTimeSeconds(I)Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup$Departure;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup$Departure;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup$Departure;

    move-result-object v0

    return-object v0
.end method

.method public setRelativeArrivalTimeSeconds(I)Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup$Departure;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup$Departure;->hasRelativeArrivalTimeSeconds:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup$Departure;->relativeArrivalTimeSeconds_:I

    return-object p0
.end method

.method public setRelativeDepartureTimeSeconds(I)Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup$Departure;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup$Departure;->hasRelativeDepartureTimeSeconds:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup$Departure;->relativeDepartureTimeSeconds_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup$Departure;->hasRelativeArrivalTimeSeconds()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup$Departure;->getRelativeArrivalTimeSeconds()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup$Departure;->hasRelativeDepartureTimeSeconds()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$TransitStationEntry$Line$DepartureGroup$Departure;->getRelativeDepartureTimeSeconds()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    return-void
.end method
