.class public final Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick$WeatherEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "WeatherPoint"
.end annotation


# instance fields
.field private cachedSize:I

.field private chanceOfPrecipitation_:I

.field private description_:Ljava/lang/String;

.field private hasChanceOfPrecipitation:Z

.field private hasDescription:Z

.field private hasHighTemperature:Z

.field private hasImageUrl:Z

.field private hasLabel:Z

.field private hasLowTemperature:Z

.field private hasTemperatureUnit:Z

.field private hasWind:Z

.field private hasWindSpeed:Z

.field private hasWindUnit:Z

.field private highTemperature_:I

.field private imageUrl_:Ljava/lang/String;

.field private label_:Ljava/lang/String;

.field private lowTemperature_:I

.field private temperatureUnit_:I

.field private windSpeed_:I

.field private windUnit_:I

.field private wind_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->label_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->description_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->imageUrl_:Ljava/lang/String;

    iput v1, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->highTemperature_:I

    iput v1, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->lowTemperature_:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->temperatureUnit_:I

    iput v1, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->chanceOfPrecipitation_:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->windUnit_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->wind_:Ljava/lang/String;

    iput v1, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->windSpeed_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->cachedSize:I

    return v0
.end method

.method public getChanceOfPrecipitation()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->chanceOfPrecipitation_:I

    return v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->description_:Ljava/lang/String;

    return-object v0
.end method

.method public getHighTemperature()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->highTemperature_:I

    return v0
.end method

.method public getImageUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->imageUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getLabel()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->label_:Ljava/lang/String;

    return-object v0
.end method

.method public getLowTemperature()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->lowTemperature_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasLabel()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getLabel()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasDescription()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getDescription()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasImageUrl()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getImageUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasHighTemperature()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getHighTemperature()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasLowTemperature()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getLowTemperature()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasTemperatureUnit()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getTemperatureUnit()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasChanceOfPrecipitation()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getChanceOfPrecipitation()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasWindUnit()Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getWindUnit()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasWind()Z

    move-result v1

    if-eqz v1, :cond_8

    const/16 v1, 0x9

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getWind()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasWindSpeed()Z

    move-result v1

    if-eqz v1, :cond_9

    const/16 v1, 0xa

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getWindSpeed()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->cachedSize:I

    return v0
.end method

.method public getTemperatureUnit()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->temperatureUnit_:I

    return v0
.end method

.method public getWind()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->wind_:Ljava/lang/String;

    return-object v0
.end method

.method public getWindSpeed()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->windSpeed_:I

    return v0
.end method

.method public getWindUnit()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->windUnit_:I

    return v0
.end method

.method public hasChanceOfPrecipitation()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasChanceOfPrecipitation:Z

    return v0
.end method

.method public hasDescription()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasDescription:Z

    return v0
.end method

.method public hasHighTemperature()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasHighTemperature:Z

    return v0
.end method

.method public hasImageUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasImageUrl:Z

    return v0
.end method

.method public hasLabel()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasLabel:Z

    return v0
.end method

.method public hasLowTemperature()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasLowTemperature:Z

    return v0
.end method

.method public hasTemperatureUnit()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasTemperatureUnit:Z

    return v0
.end method

.method public hasWind()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasWind:Z

    return v0
.end method

.method public hasWindSpeed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasWindSpeed:Z

    return v0
.end method

.method public hasWindUnit()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasWindUnit:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->setLabel(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->setDescription(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->setImageUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->setHighTemperature(I)Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->setLowTemperature(I)Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->setTemperatureUnit(I)Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->setChanceOfPrecipitation(I)Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->setWindUnit(I)Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->setWind(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->setWindSpeed(I)Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;

    move-result-object v0

    return-object v0
.end method

.method public setChanceOfPrecipitation(I)Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasChanceOfPrecipitation:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->chanceOfPrecipitation_:I

    return-object p0
.end method

.method public setDescription(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasDescription:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->description_:Ljava/lang/String;

    return-object p0
.end method

.method public setHighTemperature(I)Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasHighTemperature:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->highTemperature_:I

    return-object p0
.end method

.method public setImageUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasImageUrl:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->imageUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setLabel(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasLabel:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->label_:Ljava/lang/String;

    return-object p0
.end method

.method public setLowTemperature(I)Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasLowTemperature:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->lowTemperature_:I

    return-object p0
.end method

.method public setTemperatureUnit(I)Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasTemperatureUnit:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->temperatureUnit_:I

    return-object p0
.end method

.method public setWind(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasWind:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->wind_:Ljava/lang/String;

    return-object p0
.end method

.method public setWindSpeed(I)Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasWindSpeed:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->windSpeed_:I

    return-object p0
.end method

.method public setWindUnit(I)Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasWindUnit:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->windUnit_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasLabel()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasDescription()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasImageUrl()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getImageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasHighTemperature()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getHighTemperature()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasLowTemperature()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getLowTemperature()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasTemperatureUnit()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getTemperatureUnit()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasChanceOfPrecipitation()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getChanceOfPrecipitation()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasWindUnit()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getWindUnit()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasWind()Z

    move-result v0

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getWind()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasWindSpeed()Z

    move-result v0

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getWindSpeed()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_9
    return-void
.end method
