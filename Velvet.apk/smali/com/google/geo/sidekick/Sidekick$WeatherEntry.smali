.class public final Lcom/google/geo/sidekick/Sidekick$WeatherEntry;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Sidekick.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/geo/sidekick/Sidekick;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "WeatherEntry"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;
    }
.end annotation


# instance fields
.field private cachedSize:I

.field private changeDescriptions_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;",
            ">;"
        }
    .end annotation
.end field

.field private hasLocation:Z

.field private hasLocationType:Z

.field private hasLongCityName:Z

.field private hasOneboxData:Z

.field private locationType_:I

.field private location_:Lcom/google/geo/sidekick/Sidekick$Location;

.field private longCityName_:Ljava/lang/String;

.field private oneboxData_:Lcom/google/geo/sidekick/Sidekick$AuxiliaryMessage;

.field private weatherPoint_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->locationType_:I

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->location_:Lcom/google/geo/sidekick/Sidekick$Location;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->longCityName_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->weatherPoint_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->changeDescriptions_:Ljava/util/List;

    iput-object v1, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->oneboxData_:Lcom/google/geo/sidekick/Sidekick$AuxiliaryMessage;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addChangeDescriptions(Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;)Lcom/google/geo/sidekick/Sidekick$WeatherEntry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->changeDescriptions_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->changeDescriptions_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->changeDescriptions_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addWeatherPoint(Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;)Lcom/google/geo/sidekick/Sidekick$WeatherEntry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->weatherPoint_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->weatherPoint_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->weatherPoint_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->cachedSize:I

    return v0
.end method

.method public getChangeDescriptionsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->changeDescriptions_:Ljava/util/List;

    return-object v0
.end method

.method public getLocation()Lcom/google/geo/sidekick/Sidekick$Location;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->location_:Lcom/google/geo/sidekick/Sidekick$Location;

    return-object v0
.end method

.method public getLocationType()I
    .locals 1

    iget v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->locationType_:I

    return v0
.end method

.method public getLongCityName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->longCityName_:Ljava/lang/String;

    return-object v0
.end method

.method public getOneboxData()Lcom/google/geo/sidekick/Sidekick$AuxiliaryMessage;
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->oneboxData_:Lcom/google/geo/sidekick/Sidekick$AuxiliaryMessage;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->hasLocation()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->getWeatherPointList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;

    const/4 v3, 0x2

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->hasLocationType()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->getLocationType()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->hasLongCityName()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->getLongCityName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->hasOneboxData()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->getOneboxData()Lcom/google/geo/sidekick/Sidekick$AuxiliaryMessage;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->getChangeDescriptionsList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;

    const/4 v3, 0x6

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_1

    :cond_5
    iput v2, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->cachedSize:I

    return v2
.end method

.method public getWeatherPoint(I)Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->weatherPoint_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;

    return-object v0
.end method

.method public getWeatherPointCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->weatherPoint_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getWeatherPointList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->weatherPoint_:Ljava/util/List;

    return-object v0
.end method

.method public hasLocation()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->hasLocation:Z

    return v0
.end method

.method public hasLocationType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->hasLocationType:Z

    return v0
.end method

.method public hasLongCityName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->hasLongCityName:Z

    return v0
.end method

.method public hasOneboxData()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->hasOneboxData:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$WeatherEntry;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$Location;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->setLocation(Lcom/google/geo/sidekick/Sidekick$Location;)Lcom/google/geo/sidekick/Sidekick$WeatherEntry;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->addWeatherPoint(Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;)Lcom/google/geo/sidekick/Sidekick$WeatherEntry;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->setLocationType(I)Lcom/google/geo/sidekick/Sidekick$WeatherEntry;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->setLongCityName(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$WeatherEntry;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$AuxiliaryMessage;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$AuxiliaryMessage;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->setOneboxData(Lcom/google/geo/sidekick/Sidekick$AuxiliaryMessage;)Lcom/google/geo/sidekick/Sidekick$WeatherEntry;

    goto :goto_0

    :sswitch_6
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->addChangeDescriptions(Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;)Lcom/google/geo/sidekick/Sidekick$WeatherEntry;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$WeatherEntry;

    move-result-object v0

    return-object v0
.end method

.method public setLocation(Lcom/google/geo/sidekick/Sidekick$Location;)Lcom/google/geo/sidekick/Sidekick$WeatherEntry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Location;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->hasLocation:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->location_:Lcom/google/geo/sidekick/Sidekick$Location;

    return-object p0
.end method

.method public setLocationType(I)Lcom/google/geo/sidekick/Sidekick$WeatherEntry;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->hasLocationType:Z

    iput p1, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->locationType_:I

    return-object p0
.end method

.method public setLongCityName(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$WeatherEntry;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->hasLongCityName:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->longCityName_:Ljava/lang/String;

    return-object p0
.end method

.method public setOneboxData(Lcom/google/geo/sidekick/Sidekick$AuxiliaryMessage;)Lcom/google/geo/sidekick/Sidekick$WeatherEntry;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$AuxiliaryMessage;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->hasOneboxData:Z

    iput-object p1, p0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->oneboxData_:Lcom/google/geo/sidekick/Sidekick$AuxiliaryMessage;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->hasLocation()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->getWeatherPointList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;

    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->hasLocationType()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->getLocationType()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->hasLongCityName()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->getLongCityName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->hasOneboxData()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->getOneboxData()Lcom/google/geo/sidekick/Sidekick$AuxiliaryMessage;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->getChangeDescriptionsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;

    const/4 v2, 0x6

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_1

    :cond_5
    return-void
.end method
