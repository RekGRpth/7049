.class public final Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "ActionV2Protos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/majel/proto/ActionV2Protos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AddReminderAction"
.end annotation


# instance fields
.field private absoluteTimeTrigger_:Lcom/google/majel/proto/ActionV2Protos$AbsoluteTimeTrigger;

.field private cachedSize:I

.field private confirmationUrlPath_:Ljava/lang/String;

.field private hasAbsoluteTimeTrigger:Z

.field private hasConfirmationUrlPath:Z

.field private hasLabel:Z

.field private hasLabelSpan:Z

.field private hasLocationTrigger:Z

.field private labelSpan_:Lcom/google/majel/proto/SpanProtos$Span;

.field private label_:Ljava/lang/String;

.field private locationTrigger_:Lcom/google/majel/proto/ActionV2Protos$LocationTrigger;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->label_:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->labelSpan_:Lcom/google/majel/proto/SpanProtos$Span;

    iput-object v1, p0, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->absoluteTimeTrigger_:Lcom/google/majel/proto/ActionV2Protos$AbsoluteTimeTrigger;

    iput-object v1, p0, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->locationTrigger_:Lcom/google/majel/proto/ActionV2Protos$LocationTrigger;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->confirmationUrlPath_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->cachedSize:I

    return-void
.end method

.method public static parseFrom([B)Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;
    .locals 1
    .param p0    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;
        }
    .end annotation

    new-instance v0, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;

    invoke-direct {v0}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;

    check-cast v0, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;

    return-object v0
.end method


# virtual methods
.method public getAbsoluteTimeTrigger()Lcom/google/majel/proto/ActionV2Protos$AbsoluteTimeTrigger;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->absoluteTimeTrigger_:Lcom/google/majel/proto/ActionV2Protos$AbsoluteTimeTrigger;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->cachedSize:I

    return v0
.end method

.method public getConfirmationUrlPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->confirmationUrlPath_:Ljava/lang/String;

    return-object v0
.end method

.method public getLabel()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->label_:Ljava/lang/String;

    return-object v0
.end method

.method public getLabelSpan()Lcom/google/majel/proto/SpanProtos$Span;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->labelSpan_:Lcom/google/majel/proto/SpanProtos$Span;

    return-object v0
.end method

.method public getLocationTrigger()Lcom/google/majel/proto/ActionV2Protos$LocationTrigger;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->locationTrigger_:Lcom/google/majel/proto/ActionV2Protos$LocationTrigger;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->hasLabel()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->getLabel()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->hasLabelSpan()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->getLabelSpan()Lcom/google/majel/proto/SpanProtos$Span;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->hasAbsoluteTimeTrigger()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->getAbsoluteTimeTrigger()Lcom/google/majel/proto/ActionV2Protos$AbsoluteTimeTrigger;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->hasLocationTrigger()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->getLocationTrigger()Lcom/google/majel/proto/ActionV2Protos$LocationTrigger;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->hasConfirmationUrlPath()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->getConfirmationUrlPath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iput v0, p0, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->cachedSize:I

    return v0
.end method

.method public hasAbsoluteTimeTrigger()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->hasAbsoluteTimeTrigger:Z

    return v0
.end method

.method public hasConfirmationUrlPath()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->hasConfirmationUrlPath:Z

    return v0
.end method

.method public hasLabel()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->hasLabel:Z

    return v0
.end method

.method public hasLabelSpan()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->hasLabelSpan:Z

    return v0
.end method

.method public hasLocationTrigger()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->hasLocationTrigger:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->setLabel(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/majel/proto/SpanProtos$Span;

    invoke-direct {v1}, Lcom/google/majel/proto/SpanProtos$Span;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->setLabelSpan(Lcom/google/majel/proto/SpanProtos$Span;)Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/majel/proto/ActionV2Protos$AbsoluteTimeTrigger;

    invoke-direct {v1}, Lcom/google/majel/proto/ActionV2Protos$AbsoluteTimeTrigger;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->setAbsoluteTimeTrigger(Lcom/google/majel/proto/ActionV2Protos$AbsoluteTimeTrigger;)Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/majel/proto/ActionV2Protos$LocationTrigger;

    invoke-direct {v1}, Lcom/google/majel/proto/ActionV2Protos$LocationTrigger;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->setLocationTrigger(Lcom/google/majel/proto/ActionV2Protos$LocationTrigger;)Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->setConfirmationUrlPath(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;

    move-result-object v0

    return-object v0
.end method

.method public setAbsoluteTimeTrigger(Lcom/google/majel/proto/ActionV2Protos$AbsoluteTimeTrigger;)Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$AbsoluteTimeTrigger;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->hasAbsoluteTimeTrigger:Z

    iput-object p1, p0, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->absoluteTimeTrigger_:Lcom/google/majel/proto/ActionV2Protos$AbsoluteTimeTrigger;

    return-object p0
.end method

.method public setConfirmationUrlPath(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->hasConfirmationUrlPath:Z

    iput-object p1, p0, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->confirmationUrlPath_:Ljava/lang/String;

    return-object p0
.end method

.method public setLabel(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->hasLabel:Z

    iput-object p1, p0, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->label_:Ljava/lang/String;

    return-object p0
.end method

.method public setLabelSpan(Lcom/google/majel/proto/SpanProtos$Span;)Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;
    .locals 1
    .param p1    # Lcom/google/majel/proto/SpanProtos$Span;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->hasLabelSpan:Z

    iput-object p1, p0, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->labelSpan_:Lcom/google/majel/proto/SpanProtos$Span;

    return-object p0
.end method

.method public setLocationTrigger(Lcom/google/majel/proto/ActionV2Protos$LocationTrigger;)Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$LocationTrigger;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->hasLocationTrigger:Z

    iput-object p1, p0, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->locationTrigger_:Lcom/google/majel/proto/ActionV2Protos$LocationTrigger;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->hasLabel()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->getLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->hasLabelSpan()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->getLabelSpan()Lcom/google/majel/proto/SpanProtos$Span;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->hasAbsoluteTimeTrigger()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->getAbsoluteTimeTrigger()Lcom/google/majel/proto/ActionV2Protos$AbsoluteTimeTrigger;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->hasLocationTrigger()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->getLocationTrigger()Lcom/google/majel/proto/ActionV2Protos$LocationTrigger;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->hasConfirmationUrlPath()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->getConfirmationUrlPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    return-void
.end method
