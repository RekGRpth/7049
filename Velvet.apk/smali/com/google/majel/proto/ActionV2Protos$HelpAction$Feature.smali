.class public final Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "ActionV2Protos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/majel/proto/ActionV2Protos$HelpAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Feature"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;
    }
.end annotation


# instance fields
.field private cachedSize:I

.field private example_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;",
            ">;"
        }
    .end annotation
.end field

.field private hasHeadline:Z

.field private headline_:Lcom/google/majel/proto/ActionV2Protos$TranslationConsoleString;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;->headline_:Lcom/google/majel/proto/ActionV2Protos$TranslationConsoleString;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;->example_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addExample(Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;->example_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;->example_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;->example_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;->cachedSize:I

    return v0
.end method

.method public getExampleCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;->example_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getExampleList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;->example_:Ljava/util/List;

    return-object v0
.end method

.method public getHeadline()Lcom/google/majel/proto/ActionV2Protos$TranslationConsoleString;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;->headline_:Lcom/google/majel/proto/ActionV2Protos$TranslationConsoleString;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;->hasHeadline()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;->getHeadline()Lcom/google/majel/proto/ActionV2Protos$TranslationConsoleString;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;->getExampleList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    const/4 v3, 0x2

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_1
    iput v2, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;->cachedSize:I

    return v2
.end method

.method public hasHeadline()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;->hasHeadline:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/majel/proto/ActionV2Protos$TranslationConsoleString;

    invoke-direct {v1}, Lcom/google/majel/proto/ActionV2Protos$TranslationConsoleString;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;->setHeadline(Lcom/google/majel/proto/ActionV2Protos$TranslationConsoleString;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    invoke-direct {v1}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;->addExample(Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;

    move-result-object v0

    return-object v0
.end method

.method public parseFrom([B)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;
    .locals 1
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;
        }
    .end annotation

    new-instance v0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;

    invoke-direct {v0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;-><init>()V

    invoke-virtual {v0, p1}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;

    check-cast v0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;

    return-object v0
.end method

.method public setHeadline(Lcom/google/majel/proto/ActionV2Protos$TranslationConsoleString;)Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$TranslationConsoleString;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;->hasHeadline:Z

    iput-object p1, p0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;->headline_:Lcom/google/majel/proto/ActionV2Protos$TranslationConsoleString;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;->hasHeadline()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;->getHeadline()Lcom/google/majel/proto/ActionV2Protos$TranslationConsoleString;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature;->getExampleList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/ActionV2Protos$HelpAction$Feature$Example;

    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_1
    return-void
.end method
