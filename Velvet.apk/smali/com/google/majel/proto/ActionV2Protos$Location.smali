.class public final Lcom/google/majel/proto/ActionV2Protos$Location;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "ActionV2Protos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/majel/proto/ActionV2Protos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Location"
.end annotation


# instance fields
.field private address_:Ljava/lang/String;

.field private alias_:I

.field private cachedSize:I

.field private clusterId_:Ljava/lang/String;

.field private description_:Ljava/lang/String;

.field private hasAddress:Z

.field private hasAlias:Z

.field private hasClusterId:Z

.field private hasDescription:Z

.field private hasLatDegrees:Z

.field private hasLatSpanDegrees:Z

.field private hasLngDegrees:Z

.field private hasLngSpanDegrees:Z

.field private hasMapsUrl:Z

.field private hasOriginalDescription:Z

.field private latDegrees_:D

.field private latSpanDegrees_:D

.field private lngDegrees_:D

.field private lngSpanDegrees_:D

.field private mapsUrl_:Ljava/lang/String;

.field private originalDescription_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const-wide/16 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-wide v0, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->latDegrees_:D

    iput-wide v0, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->lngDegrees_:D

    iput-wide v0, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->latSpanDegrees_:D

    iput-wide v0, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->lngSpanDegrees_:D

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->description_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->originalDescription_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->mapsUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->address_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->clusterId_:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->alias_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getAddress()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->address_:Ljava/lang/String;

    return-object v0
.end method

.method public getAlias()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->alias_:I

    return v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$Location;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->cachedSize:I

    return v0
.end method

.method public getClusterId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->clusterId_:Ljava/lang/String;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->description_:Ljava/lang/String;

    return-object v0
.end method

.method public getLatDegrees()D
    .locals 2

    iget-wide v0, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->latDegrees_:D

    return-wide v0
.end method

.method public getLatSpanDegrees()D
    .locals 2

    iget-wide v0, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->latSpanDegrees_:D

    return-wide v0
.end method

.method public getLngDegrees()D
    .locals 2

    iget-wide v0, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->lngDegrees_:D

    return-wide v0
.end method

.method public getLngSpanDegrees()D
    .locals 2

    iget-wide v0, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->lngSpanDegrees_:D

    return-wide v0
.end method

.method public getMapsUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->mapsUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getOriginalDescription()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->originalDescription_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$Location;->hasLatDegrees()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$Location;->getLatDegrees()D

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeDoubleSize(ID)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$Location;->hasLngDegrees()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$Location;->getLngDegrees()D

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeDoubleSize(ID)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$Location;->hasDescription()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$Location;->getDescription()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$Location;->hasMapsUrl()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$Location;->getMapsUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$Location;->hasAddress()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$Location;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$Location;->hasOriginalDescription()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$Location;->getOriginalDescription()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$Location;->hasClusterId()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$Location;->getClusterId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$Location;->hasLatSpanDegrees()Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$Location;->getLatSpanDegrees()D

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeDoubleSize(ID)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$Location;->hasLngSpanDegrees()Z

    move-result v1

    if-eqz v1, :cond_8

    const/16 v1, 0x9

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$Location;->getLngSpanDegrees()D

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeDoubleSize(ID)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$Location;->hasAlias()Z

    move-result v1

    if-eqz v1, :cond_9

    const/16 v1, 0xa

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$Location;->getAlias()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iput v0, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->cachedSize:I

    return v0
.end method

.method public hasAddress()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->hasAddress:Z

    return v0
.end method

.method public hasAlias()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->hasAlias:Z

    return v0
.end method

.method public hasClusterId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->hasClusterId:Z

    return v0
.end method

.method public hasDescription()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->hasDescription:Z

    return v0
.end method

.method public hasLatDegrees()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->hasLatDegrees:Z

    return v0
.end method

.method public hasLatSpanDegrees()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->hasLatSpanDegrees:Z

    return v0
.end method

.method public hasLngDegrees()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->hasLngDegrees:Z

    return v0
.end method

.method public hasLngSpanDegrees()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->hasLngSpanDegrees:Z

    return v0
.end method

.method public hasMapsUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->hasMapsUrl:Z

    return v0
.end method

.method public hasOriginalDescription()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->hasOriginalDescription:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/ActionV2Protos$Location;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/majel/proto/ActionV2Protos$Location;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readDouble()D

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/google/majel/proto/ActionV2Protos$Location;->setLatDegrees(D)Lcom/google/majel/proto/ActionV2Protos$Location;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readDouble()D

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/google/majel/proto/ActionV2Protos$Location;->setLngDegrees(D)Lcom/google/majel/proto/ActionV2Protos$Location;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ActionV2Protos$Location;->setDescription(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$Location;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ActionV2Protos$Location;->setMapsUrl(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$Location;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ActionV2Protos$Location;->setAddress(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$Location;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ActionV2Protos$Location;->setOriginalDescription(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$Location;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ActionV2Protos$Location;->setClusterId(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$Location;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readDouble()D

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/google/majel/proto/ActionV2Protos$Location;->setLatSpanDegrees(D)Lcom/google/majel/proto/ActionV2Protos$Location;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readDouble()D

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/google/majel/proto/ActionV2Protos$Location;->setLngSpanDegrees(D)Lcom/google/majel/proto/ActionV2Protos$Location;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ActionV2Protos$Location;->setAlias(I)Lcom/google/majel/proto/ActionV2Protos$Location;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x11 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x41 -> :sswitch_8
        0x49 -> :sswitch_9
        0x50 -> :sswitch_a
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/majel/proto/ActionV2Protos$Location;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/ActionV2Protos$Location;

    move-result-object v0

    return-object v0
.end method

.method public setAddress(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$Location;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->hasAddress:Z

    iput-object p1, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->address_:Ljava/lang/String;

    return-object p0
.end method

.method public setAlias(I)Lcom/google/majel/proto/ActionV2Protos$Location;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->hasAlias:Z

    iput p1, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->alias_:I

    return-object p0
.end method

.method public setClusterId(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$Location;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->hasClusterId:Z

    iput-object p1, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->clusterId_:Ljava/lang/String;

    return-object p0
.end method

.method public setDescription(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$Location;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->hasDescription:Z

    iput-object p1, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->description_:Ljava/lang/String;

    return-object p0
.end method

.method public setLatDegrees(D)Lcom/google/majel/proto/ActionV2Protos$Location;
    .locals 1
    .param p1    # D

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->hasLatDegrees:Z

    iput-wide p1, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->latDegrees_:D

    return-object p0
.end method

.method public setLatSpanDegrees(D)Lcom/google/majel/proto/ActionV2Protos$Location;
    .locals 1
    .param p1    # D

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->hasLatSpanDegrees:Z

    iput-wide p1, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->latSpanDegrees_:D

    return-object p0
.end method

.method public setLngDegrees(D)Lcom/google/majel/proto/ActionV2Protos$Location;
    .locals 1
    .param p1    # D

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->hasLngDegrees:Z

    iput-wide p1, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->lngDegrees_:D

    return-object p0
.end method

.method public setLngSpanDegrees(D)Lcom/google/majel/proto/ActionV2Protos$Location;
    .locals 1
    .param p1    # D

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->hasLngSpanDegrees:Z

    iput-wide p1, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->lngSpanDegrees_:D

    return-object p0
.end method

.method public setMapsUrl(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$Location;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->hasMapsUrl:Z

    iput-object p1, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->mapsUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setOriginalDescription(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$Location;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->hasOriginalDescription:Z

    iput-object p1, p0, Lcom/google/majel/proto/ActionV2Protos$Location;->originalDescription_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$Location;->hasLatDegrees()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$Location;->getLatDegrees()D

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeDouble(ID)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$Location;->hasLngDegrees()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$Location;->getLngDegrees()D

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeDouble(ID)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$Location;->hasDescription()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$Location;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$Location;->hasMapsUrl()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$Location;->getMapsUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$Location;->hasAddress()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$Location;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$Location;->hasOriginalDescription()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$Location;->getOriginalDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$Location;->hasClusterId()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$Location;->getClusterId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$Location;->hasLatSpanDegrees()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$Location;->getLatSpanDegrees()D

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeDouble(ID)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$Location;->hasLngSpanDegrees()Z

    move-result v0

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$Location;->getLngSpanDegrees()D

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeDouble(ID)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$Location;->hasAlias()Z

    move-result v0

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$Location;->getAlias()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_9
    return-void
.end method
