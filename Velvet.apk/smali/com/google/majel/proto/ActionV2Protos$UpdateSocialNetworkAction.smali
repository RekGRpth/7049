.class public final Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "ActionV2Protos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/majel/proto/ActionV2Protos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UpdateSocialNetworkAction"
.end annotation


# instance fields
.field private cachedSize:I

.field private hasSocialNetwork:Z

.field private hasStatusMessage:Z

.field private hasStatusMessageSpan:Z

.field private socialNetwork_:I

.field private statusMessageSpan_:Lcom/google/majel/proto/SpanProtos$Span;

.field private statusMessage_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;->statusMessage_:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;->statusMessageSpan_:Lcom/google/majel/proto/SpanProtos$Span;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;->socialNetwork_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;->cachedSize:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;->hasStatusMessage()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;->getStatusMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;->hasStatusMessageSpan()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;->getStatusMessageSpan()Lcom/google/majel/proto/SpanProtos$Span;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;->hasSocialNetwork()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;->getSocialNetwork()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;->cachedSize:I

    return v0
.end method

.method public getSocialNetwork()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;->socialNetwork_:I

    return v0
.end method

.method public getStatusMessage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;->statusMessage_:Ljava/lang/String;

    return-object v0
.end method

.method public getStatusMessageSpan()Lcom/google/majel/proto/SpanProtos$Span;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;->statusMessageSpan_:Lcom/google/majel/proto/SpanProtos$Span;

    return-object v0
.end method

.method public hasSocialNetwork()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;->hasSocialNetwork:Z

    return v0
.end method

.method public hasStatusMessage()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;->hasStatusMessage:Z

    return v0
.end method

.method public hasStatusMessageSpan()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;->hasStatusMessageSpan:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;->setStatusMessage(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/majel/proto/SpanProtos$Span;

    invoke-direct {v1}, Lcom/google/majel/proto/SpanProtos$Span;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;->setStatusMessageSpan(Lcom/google/majel/proto/SpanProtos$Span;)Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;->setSocialNetwork(I)Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;

    move-result-object v0

    return-object v0
.end method

.method public setSocialNetwork(I)Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;->hasSocialNetwork:Z

    iput p1, p0, Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;->socialNetwork_:I

    return-object p0
.end method

.method public setStatusMessage(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;->hasStatusMessage:Z

    iput-object p1, p0, Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;->statusMessage_:Ljava/lang/String;

    return-object p0
.end method

.method public setStatusMessageSpan(Lcom/google/majel/proto/SpanProtos$Span;)Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;
    .locals 1
    .param p1    # Lcom/google/majel/proto/SpanProtos$Span;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;->hasStatusMessageSpan:Z

    iput-object p1, p0, Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;->statusMessageSpan_:Lcom/google/majel/proto/SpanProtos$Span;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;->hasStatusMessage()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;->getStatusMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;->hasStatusMessageSpan()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;->getStatusMessageSpan()Lcom/google/majel/proto/SpanProtos$Span;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;->hasSocialNetwork()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$UpdateSocialNetworkAction;->getSocialNetwork()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_2
    return-void
.end method
