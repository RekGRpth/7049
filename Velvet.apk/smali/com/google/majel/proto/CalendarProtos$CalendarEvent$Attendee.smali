.class public final Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "CalendarProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/majel/proto/CalendarProtos$CalendarEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Attendee"
.end annotation


# instance fields
.field private cachedSize:I

.field private displayName_:Ljava/lang/String;

.field private email_:Ljava/lang/String;

.field private hasDisplayName:Z

.field private hasEmail:Z

.field private hasOptionalAttendee:Z

.field private hasResource:Z

.field private hasResponseComment:Z

.field private hasResponseStatus:Z

.field private optionalAttendee_:Z

.field private resource_:Z

.field private responseComment_:Ljava/lang/String;

.field private responseStatus_:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->email_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->displayName_:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->resource_:Z

    iput-boolean v1, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->optionalAttendee_:Z

    iput v1, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->responseStatus_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->responseComment_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->cachedSize:I

    return v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->displayName_:Ljava/lang/String;

    return-object v0
.end method

.method public getEmail()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->email_:Ljava/lang/String;

    return-object v0
.end method

.method public getOptionalAttendee()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->optionalAttendee_:Z

    return v0
.end method

.method public getResource()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->resource_:Z

    return v0
.end method

.method public getResponseComment()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->responseComment_:Ljava/lang/String;

    return-object v0
.end method

.method public getResponseStatus()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->responseStatus_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->hasEmail()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->getEmail()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->hasDisplayName()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->hasResource()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->getResource()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->hasOptionalAttendee()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->getOptionalAttendee()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->hasResponseStatus()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->getResponseStatus()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->hasResponseComment()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->getResponseComment()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iput v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->cachedSize:I

    return v0
.end method

.method public hasDisplayName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->hasDisplayName:Z

    return v0
.end method

.method public hasEmail()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->hasEmail:Z

    return v0
.end method

.method public hasOptionalAttendee()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->hasOptionalAttendee:Z

    return v0
.end method

.method public hasResource()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->hasResource:Z

    return v0
.end method

.method public hasResponseComment()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->hasResponseComment:Z

    return v0
.end method

.method public hasResponseStatus()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->hasResponseStatus:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->setEmail(Ljava/lang/String;)Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->setDisplayName(Ljava/lang/String;)Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->setResource(Z)Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->setOptionalAttendee(Z)Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->setResponseStatus(I)Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->setResponseComment(Ljava/lang/String;)Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;

    move-result-object v0

    return-object v0
.end method

.method public setDisplayName(Ljava/lang/String;)Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->hasDisplayName:Z

    iput-object p1, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->displayName_:Ljava/lang/String;

    return-object p0
.end method

.method public setEmail(Ljava/lang/String;)Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->hasEmail:Z

    iput-object p1, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->email_:Ljava/lang/String;

    return-object p0
.end method

.method public setOptionalAttendee(Z)Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->hasOptionalAttendee:Z

    iput-boolean p1, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->optionalAttendee_:Z

    return-object p0
.end method

.method public setResource(Z)Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->hasResource:Z

    iput-boolean p1, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->resource_:Z

    return-object p0
.end method

.method public setResponseComment(Ljava/lang/String;)Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->hasResponseComment:Z

    iput-object p1, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->responseComment_:Ljava/lang/String;

    return-object p0
.end method

.method public setResponseStatus(I)Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->hasResponseStatus:Z

    iput p1, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->responseStatus_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->hasEmail()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->getEmail()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->hasDisplayName()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->hasResource()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->getResource()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->hasOptionalAttendee()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->getOptionalAttendee()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->hasResponseStatus()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->getResponseStatus()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->hasResponseComment()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->getResponseComment()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_5
    return-void
.end method
