.class public final Lcom/google/majel/proto/CalendarProtos$CalendarEvent;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "CalendarProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/majel/proto/CalendarProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CalendarEvent"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;,
        Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;
    }
.end annotation


# instance fields
.field private attendee_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;",
            ">;"
        }
    .end annotation
.end field

.field private cachedSize:I

.field private description_:Ljava/lang/String;

.field private endTime_:Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;

.field private hasDescription:Z

.field private hasEndTime:Z

.field private hasHtmlLink:Z

.field private hasIsAllDay:Z

.field private hasLocation:Z

.field private hasOtherAttendeesExcluded:Z

.field private hasStartTime:Z

.field private hasSummary:Z

.field private htmlLink_:Ljava/lang/String;

.field private isAllDay_:Z

.field private location_:Ljava/lang/String;

.field private otherAttendeesExcluded_:Z

.field private reminder_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;",
            ">;"
        }
    .end annotation
.end field

.field private startTime_:Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;

.field private summary_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->htmlLink_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->summary_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->description_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->location_:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->startTime_:Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;

    iput-object v2, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->endTime_:Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;

    iput-boolean v1, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->isAllDay_:Z

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->attendee_:Ljava/util/List;

    iput-boolean v1, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->otherAttendeesExcluded_:Z

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->reminder_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addAttendee(Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;)Lcom/google/majel/proto/CalendarProtos$CalendarEvent;
    .locals 1
    .param p1    # Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->attendee_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->attendee_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->attendee_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addReminder(Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;)Lcom/google/majel/proto/CalendarProtos$CalendarEvent;
    .locals 1
    .param p1    # Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->reminder_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->reminder_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->reminder_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getAttendeeList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->attendee_:Ljava/util/List;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->cachedSize:I

    return v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->description_:Ljava/lang/String;

    return-object v0
.end method

.method public getEndTime()Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->endTime_:Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;

    return-object v0
.end method

.method public getHtmlLink()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->htmlLink_:Ljava/lang/String;

    return-object v0
.end method

.method public getIsAllDay()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->isAllDay_:Z

    return v0
.end method

.method public getLocation()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->location_:Ljava/lang/String;

    return-object v0
.end method

.method public getOtherAttendeesExcluded()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->otherAttendeesExcluded_:Z

    return v0
.end method

.method public getReminderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->reminder_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->hasHtmlLink()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->getHtmlLink()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->hasSummary()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->getSummary()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->hasDescription()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->getDescription()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->hasLocation()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->getLocation()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->hasStartTime()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->getStartTime()Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->hasEndTime()Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x6

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->getEndTime()Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_5
    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->getAttendeeList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;

    const/4 v3, 0x7

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_6
    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->hasOtherAttendeesExcluded()Z

    move-result v3

    if-eqz v3, :cond_7

    const/16 v3, 0x8

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->getOtherAttendeesExcluded()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_7
    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->hasIsAllDay()Z

    move-result v3

    if-eqz v3, :cond_8

    const/16 v3, 0x9

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->getIsAllDay()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_8
    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->getReminderList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;

    const/16 v3, 0xa

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_1

    :cond_9
    iput v2, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->cachedSize:I

    return v2
.end method

.method public getStartTime()Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->startTime_:Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;

    return-object v0
.end method

.method public getSummary()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->summary_:Ljava/lang/String;

    return-object v0
.end method

.method public hasDescription()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->hasDescription:Z

    return v0
.end method

.method public hasEndTime()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->hasEndTime:Z

    return v0
.end method

.method public hasHtmlLink()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->hasHtmlLink:Z

    return v0
.end method

.method public hasIsAllDay()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->hasIsAllDay:Z

    return v0
.end method

.method public hasLocation()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->hasLocation:Z

    return v0
.end method

.method public hasOtherAttendeesExcluded()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->hasOtherAttendeesExcluded:Z

    return v0
.end method

.method public hasStartTime()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->hasStartTime:Z

    return v0
.end method

.method public hasSummary()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->hasSummary:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/CalendarProtos$CalendarEvent;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->setHtmlLink(Ljava/lang/String;)Lcom/google/majel/proto/CalendarProtos$CalendarEvent;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->setSummary(Ljava/lang/String;)Lcom/google/majel/proto/CalendarProtos$CalendarEvent;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->setDescription(Ljava/lang/String;)Lcom/google/majel/proto/CalendarProtos$CalendarEvent;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->setLocation(Ljava/lang/String;)Lcom/google/majel/proto/CalendarProtos$CalendarEvent;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;

    invoke-direct {v1}, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->setStartTime(Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;)Lcom/google/majel/proto/CalendarProtos$CalendarEvent;

    goto :goto_0

    :sswitch_6
    new-instance v1, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;

    invoke-direct {v1}, Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->setEndTime(Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;)Lcom/google/majel/proto/CalendarProtos$CalendarEvent;

    goto :goto_0

    :sswitch_7
    new-instance v1, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;

    invoke-direct {v1}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->addAttendee(Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;)Lcom/google/majel/proto/CalendarProtos$CalendarEvent;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->setOtherAttendeesExcluded(Z)Lcom/google/majel/proto/CalendarProtos$CalendarEvent;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->setIsAllDay(Z)Lcom/google/majel/proto/CalendarProtos$CalendarEvent;

    goto :goto_0

    :sswitch_a
    new-instance v1, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;

    invoke-direct {v1}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->addReminder(Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;)Lcom/google/majel/proto/CalendarProtos$CalendarEvent;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/CalendarProtos$CalendarEvent;

    move-result-object v0

    return-object v0
.end method

.method public setDescription(Ljava/lang/String;)Lcom/google/majel/proto/CalendarProtos$CalendarEvent;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->hasDescription:Z

    iput-object p1, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->description_:Ljava/lang/String;

    return-object p0
.end method

.method public setEndTime(Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;)Lcom/google/majel/proto/CalendarProtos$CalendarEvent;
    .locals 1
    .param p1    # Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->hasEndTime:Z

    iput-object p1, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->endTime_:Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;

    return-object p0
.end method

.method public setHtmlLink(Ljava/lang/String;)Lcom/google/majel/proto/CalendarProtos$CalendarEvent;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->hasHtmlLink:Z

    iput-object p1, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->htmlLink_:Ljava/lang/String;

    return-object p0
.end method

.method public setIsAllDay(Z)Lcom/google/majel/proto/CalendarProtos$CalendarEvent;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->hasIsAllDay:Z

    iput-boolean p1, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->isAllDay_:Z

    return-object p0
.end method

.method public setLocation(Ljava/lang/String;)Lcom/google/majel/proto/CalendarProtos$CalendarEvent;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->hasLocation:Z

    iput-object p1, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->location_:Ljava/lang/String;

    return-object p0
.end method

.method public setOtherAttendeesExcluded(Z)Lcom/google/majel/proto/CalendarProtos$CalendarEvent;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->hasOtherAttendeesExcluded:Z

    iput-boolean p1, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->otherAttendeesExcluded_:Z

    return-object p0
.end method

.method public setStartTime(Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;)Lcom/google/majel/proto/CalendarProtos$CalendarEvent;
    .locals 1
    .param p1    # Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->hasStartTime:Z

    iput-object p1, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->startTime_:Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;

    return-object p0
.end method

.method public setSummary(Ljava/lang/String;)Lcom/google/majel/proto/CalendarProtos$CalendarEvent;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->hasSummary:Z

    iput-object p1, p0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->summary_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->hasHtmlLink()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->getHtmlLink()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->hasSummary()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->getSummary()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->hasDescription()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->getDescription()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->hasLocation()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->getLocation()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->hasStartTime()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->getStartTime()Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->hasEndTime()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->getEndTime()Lcom/google/majel/proto/CalendarProtos$CalendarDateTime;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->getAttendeeList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;

    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_6
    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->hasOtherAttendeesExcluded()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->getOtherAttendeesExcluded()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->hasIsAllDay()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->getIsAllDay()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent;->getReminderList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;

    const/16 v2, 0xa

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_1

    :cond_9
    return-void
.end method
