.class public final Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "CommonStructuredResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/majel/proto/CommonStructuredResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StructuredResponse"
.end annotation


# instance fields
.field private cachedSize:I

.field private calculatorResultExtension_:Lcom/google/majel/proto/CommonStructuredResponse$CalculatorResult;

.field private currencyConversionResultExtension_:Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;

.field private dictionaryResultExtension_:Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;

.field private ecoutezLocalResultsExtension_:Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

.field private financeResultExtension_:Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;

.field private flightResultExtension_:Lcom/google/majel/proto/EcoutezStructuredResponse$FlightResult;

.field private gogglesGenericResultExtension_:Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$GogglesGenericResult;

.field private hasCalculatorResultExtension:Z

.field private hasCurrencyConversionResultExtension:Z

.field private hasDictionaryResultExtension:Z

.field private hasEcoutezLocalResultsExtension:Z

.field private hasFinanceResultExtension:Z

.field private hasFlightResultExtension:Z

.field private hasGogglesGenericResultExtension:Z

.field private hasKnowledgeResultExtension:Z

.field private hasPublicDataResultExtension:Z

.field private hasRecognizedContactExtension:Z

.field private hasRelatedSearchResultsExtension:Z

.field private hasReplacesType:Z

.field private hasSnippetResultsExtension:Z

.field private hasSportsResultExtension:Z

.field private hasTranslationResultExtension:Z

.field private hasWeatherResultExtension:Z

.field private knowledgeResultExtension_:Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;

.field private publicDataResultExtension_:Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;

.field private recognizedContactExtension_:Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact;

.field private relatedSearchResultsExtension_:Lcom/google/majel/proto/EcoutezStructuredResponse$RelatedSearchResults;

.field private replacesType_:I

.field private snippetResultsExtension_:Lcom/google/majel/proto/EcoutezStructuredResponse$SnippetResults;

.field private sportsResultExtension_:Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;

.field private translationResultExtension_:Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;

.field private weatherResultExtension_:Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->replacesType_:I

    iput-object v1, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->translationResultExtension_:Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;

    iput-object v1, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->currencyConversionResultExtension_:Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;

    iput-object v1, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->calculatorResultExtension_:Lcom/google/majel/proto/CommonStructuredResponse$CalculatorResult;

    iput-object v1, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->weatherResultExtension_:Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;

    iput-object v1, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->financeResultExtension_:Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;

    iput-object v1, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->flightResultExtension_:Lcom/google/majel/proto/EcoutezStructuredResponse$FlightResult;

    iput-object v1, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->ecoutezLocalResultsExtension_:Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

    iput-object v1, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->dictionaryResultExtension_:Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;

    iput-object v1, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->sportsResultExtension_:Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;

    iput-object v1, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->knowledgeResultExtension_:Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;

    iput-object v1, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->snippetResultsExtension_:Lcom/google/majel/proto/EcoutezStructuredResponse$SnippetResults;

    iput-object v1, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->relatedSearchResultsExtension_:Lcom/google/majel/proto/EcoutezStructuredResponse$RelatedSearchResults;

    iput-object v1, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->gogglesGenericResultExtension_:Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$GogglesGenericResult;

    iput-object v1, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->publicDataResultExtension_:Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;

    iput-object v1, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->recognizedContactExtension_:Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->cachedSize:I

    return v0
.end method

.method public getCalculatorResultExtension()Lcom/google/majel/proto/CommonStructuredResponse$CalculatorResult;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->calculatorResultExtension_:Lcom/google/majel/proto/CommonStructuredResponse$CalculatorResult;

    return-object v0
.end method

.method public getCurrencyConversionResultExtension()Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->currencyConversionResultExtension_:Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;

    return-object v0
.end method

.method public getDictionaryResultExtension()Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->dictionaryResultExtension_:Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;

    return-object v0
.end method

.method public getEcoutezLocalResultsExtension()Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->ecoutezLocalResultsExtension_:Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

    return-object v0
.end method

.method public getFinanceResultExtension()Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->financeResultExtension_:Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;

    return-object v0
.end method

.method public getFlightResultExtension()Lcom/google/majel/proto/EcoutezStructuredResponse$FlightResult;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->flightResultExtension_:Lcom/google/majel/proto/EcoutezStructuredResponse$FlightResult;

    return-object v0
.end method

.method public getGogglesGenericResultExtension()Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$GogglesGenericResult;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->gogglesGenericResultExtension_:Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$GogglesGenericResult;

    return-object v0
.end method

.method public getKnowledgeResultExtension()Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->knowledgeResultExtension_:Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;

    return-object v0
.end method

.method public getPublicDataResultExtension()Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->publicDataResultExtension_:Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;

    return-object v0
.end method

.method public getRecognizedContactExtension()Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->recognizedContactExtension_:Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact;

    return-object v0
.end method

.method public getRelatedSearchResultsExtension()Lcom/google/majel/proto/EcoutezStructuredResponse$RelatedSearchResults;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->relatedSearchResultsExtension_:Lcom/google/majel/proto/EcoutezStructuredResponse$RelatedSearchResults;

    return-object v0
.end method

.method public getReplacesType()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->replacesType_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasReplacesType()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->getReplacesType()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasTranslationResultExtension()Z

    move-result v1

    if-eqz v1, :cond_1

    const v1, 0x1a07149

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->getTranslationResultExtension()Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasCurrencyConversionResultExtension()Z

    move-result v1

    if-eqz v1, :cond_2

    const v1, 0x1bd1793

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->getCurrencyConversionResultExtension()Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasCalculatorResultExtension()Z

    move-result v1

    if-eqz v1, :cond_3

    const v1, 0x1bd4b0a

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->getCalculatorResultExtension()Lcom/google/majel/proto/CommonStructuredResponse$CalculatorResult;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasWeatherResultExtension()Z

    move-result v1

    if-eqz v1, :cond_4

    const v1, 0x1be0d85

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->getWeatherResultExtension()Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasFinanceResultExtension()Z

    move-result v1

    if-eqz v1, :cond_5

    const v1, 0x1c7f302

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->getFinanceResultExtension()Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasFlightResultExtension()Z

    move-result v1

    if-eqz v1, :cond_6

    const v1, 0x1c879ea

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->getFlightResultExtension()Lcom/google/majel/proto/EcoutezStructuredResponse$FlightResult;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasEcoutezLocalResultsExtension()Z

    move-result v1

    if-eqz v1, :cond_7

    const v1, 0x1ca484a

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->getEcoutezLocalResultsExtension()Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasDictionaryResultExtension()Z

    move-result v1

    if-eqz v1, :cond_8

    const v1, 0x1cb332a

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->getDictionaryResultExtension()Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasSportsResultExtension()Z

    move-result v1

    if-eqz v1, :cond_9

    const v1, 0x1cce67c

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->getSportsResultExtension()Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasKnowledgeResultExtension()Z

    move-result v1

    if-eqz v1, :cond_a

    const v1, 0x1fe4d3a

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->getKnowledgeResultExtension()Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasSnippetResultsExtension()Z

    move-result v1

    if-eqz v1, :cond_b

    const v1, 0x207765c

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->getSnippetResultsExtension()Lcom/google/majel/proto/EcoutezStructuredResponse$SnippetResults;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasRelatedSearchResultsExtension()Z

    move-result v1

    if-eqz v1, :cond_c

    const v1, 0x21a00dd

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->getRelatedSearchResultsExtension()Lcom/google/majel/proto/EcoutezStructuredResponse$RelatedSearchResults;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_c
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasGogglesGenericResultExtension()Z

    move-result v1

    if-eqz v1, :cond_d

    const v1, 0x2466b15

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->getGogglesGenericResultExtension()Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$GogglesGenericResult;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_d
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasPublicDataResultExtension()Z

    move-result v1

    if-eqz v1, :cond_e

    const v1, 0x2588b73

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->getPublicDataResultExtension()Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_e
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasRecognizedContactExtension()Z

    move-result v1

    if-eqz v1, :cond_f

    const v1, 0x293d11b

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->getRecognizedContactExtension()Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_f
    iput v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->cachedSize:I

    return v0
.end method

.method public getSnippetResultsExtension()Lcom/google/majel/proto/EcoutezStructuredResponse$SnippetResults;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->snippetResultsExtension_:Lcom/google/majel/proto/EcoutezStructuredResponse$SnippetResults;

    return-object v0
.end method

.method public getSportsResultExtension()Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->sportsResultExtension_:Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;

    return-object v0
.end method

.method public getTranslationResultExtension()Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->translationResultExtension_:Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;

    return-object v0
.end method

.method public getWeatherResultExtension()Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->weatherResultExtension_:Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;

    return-object v0
.end method

.method public hasCalculatorResultExtension()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasCalculatorResultExtension:Z

    return v0
.end method

.method public hasCurrencyConversionResultExtension()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasCurrencyConversionResultExtension:Z

    return v0
.end method

.method public hasDictionaryResultExtension()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasDictionaryResultExtension:Z

    return v0
.end method

.method public hasEcoutezLocalResultsExtension()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasEcoutezLocalResultsExtension:Z

    return v0
.end method

.method public hasFinanceResultExtension()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasFinanceResultExtension:Z

    return v0
.end method

.method public hasFlightResultExtension()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasFlightResultExtension:Z

    return v0
.end method

.method public hasGogglesGenericResultExtension()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasGogglesGenericResultExtension:Z

    return v0
.end method

.method public hasKnowledgeResultExtension()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasKnowledgeResultExtension:Z

    return v0
.end method

.method public hasPublicDataResultExtension()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasPublicDataResultExtension:Z

    return v0
.end method

.method public hasRecognizedContactExtension()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasRecognizedContactExtension:Z

    return v0
.end method

.method public hasRelatedSearchResultsExtension()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasRelatedSearchResultsExtension:Z

    return v0
.end method

.method public hasReplacesType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasReplacesType:Z

    return v0
.end method

.method public hasSnippetResultsExtension()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasSnippetResultsExtension:Z

    return v0
.end method

.method public hasSportsResultExtension()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasSportsResultExtension:Z

    return v0
.end method

.method public hasTranslationResultExtension()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasTranslationResultExtension:Z

    return v0
.end method

.method public hasWeatherResultExtension()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasWeatherResultExtension:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->setReplacesType(I)Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;

    invoke-direct {v1}, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->setTranslationResultExtension(Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;)Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;

    invoke-direct {v1}, Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->setCurrencyConversionResultExtension(Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;)Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/majel/proto/CommonStructuredResponse$CalculatorResult;

    invoke-direct {v1}, Lcom/google/majel/proto/CommonStructuredResponse$CalculatorResult;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->setCalculatorResultExtension(Lcom/google/majel/proto/CommonStructuredResponse$CalculatorResult;)Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;

    invoke-direct {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->setWeatherResultExtension(Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;)Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;

    goto :goto_0

    :sswitch_6
    new-instance v1, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;

    invoke-direct {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->setFinanceResultExtension(Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;)Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;

    goto :goto_0

    :sswitch_7
    new-instance v1, Lcom/google/majel/proto/EcoutezStructuredResponse$FlightResult;

    invoke-direct {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$FlightResult;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->setFlightResultExtension(Lcom/google/majel/proto/EcoutezStructuredResponse$FlightResult;)Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;

    goto :goto_0

    :sswitch_8
    new-instance v1, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

    invoke-direct {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->setEcoutezLocalResultsExtension(Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;)Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;

    goto :goto_0

    :sswitch_9
    new-instance v1, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;

    invoke-direct {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->setDictionaryResultExtension(Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;)Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;

    goto :goto_0

    :sswitch_a
    new-instance v1, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;

    invoke-direct {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->setSportsResultExtension(Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;)Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;

    goto/16 :goto_0

    :sswitch_b
    new-instance v1, Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;

    invoke-direct {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->setKnowledgeResultExtension(Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;)Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;

    goto/16 :goto_0

    :sswitch_c
    new-instance v1, Lcom/google/majel/proto/EcoutezStructuredResponse$SnippetResults;

    invoke-direct {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$SnippetResults;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->setSnippetResultsExtension(Lcom/google/majel/proto/EcoutezStructuredResponse$SnippetResults;)Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;

    goto/16 :goto_0

    :sswitch_d
    new-instance v1, Lcom/google/majel/proto/EcoutezStructuredResponse$RelatedSearchResults;

    invoke-direct {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$RelatedSearchResults;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->setRelatedSearchResultsExtension(Lcom/google/majel/proto/EcoutezStructuredResponse$RelatedSearchResults;)Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;

    goto/16 :goto_0

    :sswitch_e
    new-instance v1, Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$GogglesGenericResult;

    invoke-direct {v1}, Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$GogglesGenericResult;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->setGogglesGenericResultExtension(Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$GogglesGenericResult;)Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;

    goto/16 :goto_0

    :sswitch_f
    new-instance v1, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;

    invoke-direct {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->setPublicDataResultExtension(Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;)Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;

    goto/16 :goto_0

    :sswitch_10
    new-instance v1, Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact;

    invoke-direct {v1}, Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->setRecognizedContactExtension(Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact;)Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0xd038a4a -> :sswitch_2
        0xde8bc9a -> :sswitch_3
        0xdea5852 -> :sswitch_4
        0xdf06c2a -> :sswitch_5
        0xe3f9812 -> :sswitch_6
        0xe43cf52 -> :sswitch_7
        0xe524252 -> :sswitch_8
        0xe599952 -> :sswitch_9
        0xe6733e2 -> :sswitch_a
        0xff269d2 -> :sswitch_b
        0x103bb2e2 -> :sswitch_c
        0x10d006ea -> :sswitch_d
        0x123358aa -> :sswitch_e
        0x12c45b9a -> :sswitch_f
        0x149e88da -> :sswitch_10
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;

    move-result-object v0

    return-object v0
.end method

.method public setCalculatorResultExtension(Lcom/google/majel/proto/CommonStructuredResponse$CalculatorResult;)Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;
    .locals 1
    .param p1    # Lcom/google/majel/proto/CommonStructuredResponse$CalculatorResult;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasCalculatorResultExtension:Z

    iput-object p1, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->calculatorResultExtension_:Lcom/google/majel/proto/CommonStructuredResponse$CalculatorResult;

    return-object p0
.end method

.method public setCurrencyConversionResultExtension(Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;)Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;
    .locals 1
    .param p1    # Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasCurrencyConversionResultExtension:Z

    iput-object p1, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->currencyConversionResultExtension_:Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;

    return-object p0
.end method

.method public setDictionaryResultExtension(Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;)Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasDictionaryResultExtension:Z

    iput-object p1, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->dictionaryResultExtension_:Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;

    return-object p0
.end method

.method public setEcoutezLocalResultsExtension(Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;)Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasEcoutezLocalResultsExtension:Z

    iput-object p1, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->ecoutezLocalResultsExtension_:Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

    return-object p0
.end method

.method public setFinanceResultExtension(Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;)Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasFinanceResultExtension:Z

    iput-object p1, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->financeResultExtension_:Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;

    return-object p0
.end method

.method public setFlightResultExtension(Lcom/google/majel/proto/EcoutezStructuredResponse$FlightResult;)Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$FlightResult;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasFlightResultExtension:Z

    iput-object p1, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->flightResultExtension_:Lcom/google/majel/proto/EcoutezStructuredResponse$FlightResult;

    return-object p0
.end method

.method public setGogglesGenericResultExtension(Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$GogglesGenericResult;)Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;
    .locals 1
    .param p1    # Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$GogglesGenericResult;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasGogglesGenericResultExtension:Z

    iput-object p1, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->gogglesGenericResultExtension_:Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$GogglesGenericResult;

    return-object p0
.end method

.method public setKnowledgeResultExtension(Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;)Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasKnowledgeResultExtension:Z

    iput-object p1, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->knowledgeResultExtension_:Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;

    return-object p0
.end method

.method public setPublicDataResultExtension(Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;)Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasPublicDataResultExtension:Z

    iput-object p1, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->publicDataResultExtension_:Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;

    return-object p0
.end method

.method public setRecognizedContactExtension(Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact;)Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;
    .locals 1
    .param p1    # Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasRecognizedContactExtension:Z

    iput-object p1, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->recognizedContactExtension_:Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact;

    return-object p0
.end method

.method public setRelatedSearchResultsExtension(Lcom/google/majel/proto/EcoutezStructuredResponse$RelatedSearchResults;)Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$RelatedSearchResults;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasRelatedSearchResultsExtension:Z

    iput-object p1, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->relatedSearchResultsExtension_:Lcom/google/majel/proto/EcoutezStructuredResponse$RelatedSearchResults;

    return-object p0
.end method

.method public setReplacesType(I)Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasReplacesType:Z

    iput p1, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->replacesType_:I

    return-object p0
.end method

.method public setSnippetResultsExtension(Lcom/google/majel/proto/EcoutezStructuredResponse$SnippetResults;)Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$SnippetResults;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasSnippetResultsExtension:Z

    iput-object p1, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->snippetResultsExtension_:Lcom/google/majel/proto/EcoutezStructuredResponse$SnippetResults;

    return-object p0
.end method

.method public setSportsResultExtension(Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;)Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasSportsResultExtension:Z

    iput-object p1, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->sportsResultExtension_:Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;

    return-object p0
.end method

.method public setTranslationResultExtension(Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;)Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;
    .locals 1
    .param p1    # Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasTranslationResultExtension:Z

    iput-object p1, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->translationResultExtension_:Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;

    return-object p0
.end method

.method public setWeatherResultExtension(Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;)Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasWeatherResultExtension:Z

    iput-object p1, p0, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->weatherResultExtension_:Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasReplacesType()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->getReplacesType()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasTranslationResultExtension()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x1a07149

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->getTranslationResultExtension()Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasCurrencyConversionResultExtension()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x1bd1793

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->getCurrencyConversionResultExtension()Lcom/google/majel/proto/CommonStructuredResponse$CurrencyConversionResult;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasCalculatorResultExtension()Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x1bd4b0a

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->getCalculatorResultExtension()Lcom/google/majel/proto/CommonStructuredResponse$CalculatorResult;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasWeatherResultExtension()Z

    move-result v0

    if-eqz v0, :cond_4

    const v0, 0x1be0d85

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->getWeatherResultExtension()Lcom/google/majel/proto/EcoutezStructuredResponse$WeatherResult;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasFinanceResultExtension()Z

    move-result v0

    if-eqz v0, :cond_5

    const v0, 0x1c7f302

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->getFinanceResultExtension()Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasFlightResultExtension()Z

    move-result v0

    if-eqz v0, :cond_6

    const v0, 0x1c879ea

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->getFlightResultExtension()Lcom/google/majel/proto/EcoutezStructuredResponse$FlightResult;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasEcoutezLocalResultsExtension()Z

    move-result v0

    if-eqz v0, :cond_7

    const v0, 0x1ca484a

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->getEcoutezLocalResultsExtension()Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasDictionaryResultExtension()Z

    move-result v0

    if-eqz v0, :cond_8

    const v0, 0x1cb332a

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->getDictionaryResultExtension()Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryResult;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasSportsResultExtension()Z

    move-result v0

    if-eqz v0, :cond_9

    const v0, 0x1cce67c

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->getSportsResultExtension()Lcom/google/majel/proto/EcoutezStructuredResponse$SportsResult;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasKnowledgeResultExtension()Z

    move-result v0

    if-eqz v0, :cond_a

    const v0, 0x1fe4d3a

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->getKnowledgeResultExtension()Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasSnippetResultsExtension()Z

    move-result v0

    if-eqz v0, :cond_b

    const v0, 0x207765c

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->getSnippetResultsExtension()Lcom/google/majel/proto/EcoutezStructuredResponse$SnippetResults;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_b
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasRelatedSearchResultsExtension()Z

    move-result v0

    if-eqz v0, :cond_c

    const v0, 0x21a00dd

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->getRelatedSearchResultsExtension()Lcom/google/majel/proto/EcoutezStructuredResponse$RelatedSearchResults;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_c
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasGogglesGenericResultExtension()Z

    move-result v0

    if-eqz v0, :cond_d

    const v0, 0x2466b15

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->getGogglesGenericResultExtension()Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$GogglesGenericResult;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_d
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasPublicDataResultExtension()Z

    move-result v0

    if-eqz v0, :cond_e

    const v0, 0x2588b73

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->getPublicDataResultExtension()Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_e
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->hasRecognizedContactExtension()Z

    move-result v0

    if-eqz v0, :cond_f

    const v0, 0x293d11b

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$StructuredResponse;->getRecognizedContactExtension()Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_f
    return-void
.end method
