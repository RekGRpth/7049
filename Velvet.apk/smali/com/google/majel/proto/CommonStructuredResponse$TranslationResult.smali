.class public final Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "CommonStructuredResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/majel/proto/CommonStructuredResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TranslationResult"
.end annotation


# instance fields
.field private cachedSize:I

.field private hasTextToTranslate:Z

.field private hasTextToTranslateLanguage:Z

.field private hasTextToTranslateLanguageDisplay:Z

.field private hasTextToTranslateTransliteration:Z

.field private hasTranslatedText:Z

.field private hasTranslatedTextLanguage:Z

.field private hasTranslatedTextLanguageDisplay:Z

.field private hasTranslatedTextTransliteration:Z

.field private textToTranslateLanguageDisplay_:Ljava/lang/String;

.field private textToTranslateLanguage_:Ljava/lang/String;

.field private textToTranslateTransliteration_:Ljava/lang/String;

.field private textToTranslate_:Ljava/lang/String;

.field private translatedTextLanguageDisplay_:Ljava/lang/String;

.field private translatedTextLanguage_:Ljava/lang/String;

.field private translatedTextTransliteration_:Ljava/lang/String;

.field private translatedText_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->textToTranslate_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->textToTranslateLanguage_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->textToTranslateLanguageDisplay_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->textToTranslateTransliteration_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->translatedText_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->translatedTextLanguage_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->translatedTextLanguageDisplay_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->translatedTextTransliteration_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->cachedSize:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->hasTextToTranslate()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->getTextToTranslate()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->hasTextToTranslateLanguage()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->getTextToTranslateLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->hasTranslatedText()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->getTranslatedText()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->hasTranslatedTextLanguage()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->getTranslatedTextLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->hasTextToTranslateLanguageDisplay()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->getTextToTranslateLanguageDisplay()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->hasTextToTranslateTransliteration()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->getTextToTranslateTransliteration()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->hasTranslatedTextLanguageDisplay()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->getTranslatedTextLanguageDisplay()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->hasTranslatedTextTransliteration()Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->getTranslatedTextTransliteration()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iput v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->cachedSize:I

    return v0
.end method

.method public getTextToTranslate()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->textToTranslate_:Ljava/lang/String;

    return-object v0
.end method

.method public getTextToTranslateLanguage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->textToTranslateLanguage_:Ljava/lang/String;

    return-object v0
.end method

.method public getTextToTranslateLanguageDisplay()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->textToTranslateLanguageDisplay_:Ljava/lang/String;

    return-object v0
.end method

.method public getTextToTranslateTransliteration()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->textToTranslateTransliteration_:Ljava/lang/String;

    return-object v0
.end method

.method public getTranslatedText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->translatedText_:Ljava/lang/String;

    return-object v0
.end method

.method public getTranslatedTextLanguage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->translatedTextLanguage_:Ljava/lang/String;

    return-object v0
.end method

.method public getTranslatedTextLanguageDisplay()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->translatedTextLanguageDisplay_:Ljava/lang/String;

    return-object v0
.end method

.method public getTranslatedTextTransliteration()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->translatedTextTransliteration_:Ljava/lang/String;

    return-object v0
.end method

.method public hasTextToTranslate()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->hasTextToTranslate:Z

    return v0
.end method

.method public hasTextToTranslateLanguage()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->hasTextToTranslateLanguage:Z

    return v0
.end method

.method public hasTextToTranslateLanguageDisplay()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->hasTextToTranslateLanguageDisplay:Z

    return v0
.end method

.method public hasTextToTranslateTransliteration()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->hasTextToTranslateTransliteration:Z

    return v0
.end method

.method public hasTranslatedText()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->hasTranslatedText:Z

    return v0
.end method

.method public hasTranslatedTextLanguage()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->hasTranslatedTextLanguage:Z

    return v0
.end method

.method public hasTranslatedTextLanguageDisplay()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->hasTranslatedTextLanguageDisplay:Z

    return v0
.end method

.method public hasTranslatedTextTransliteration()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->hasTranslatedTextTransliteration:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->setTextToTranslate(Ljava/lang/String;)Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->setTextToTranslateLanguage(Ljava/lang/String;)Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->setTranslatedText(Ljava/lang/String;)Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->setTranslatedTextLanguage(Ljava/lang/String;)Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->setTextToTranslateLanguageDisplay(Ljava/lang/String;)Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->setTextToTranslateTransliteration(Ljava/lang/String;)Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->setTranslatedTextLanguageDisplay(Ljava/lang/String;)Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->setTranslatedTextTransliteration(Ljava/lang/String;)Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;

    move-result-object v0

    return-object v0
.end method

.method public setTextToTranslate(Ljava/lang/String;)Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->hasTextToTranslate:Z

    iput-object p1, p0, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->textToTranslate_:Ljava/lang/String;

    return-object p0
.end method

.method public setTextToTranslateLanguage(Ljava/lang/String;)Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->hasTextToTranslateLanguage:Z

    iput-object p1, p0, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->textToTranslateLanguage_:Ljava/lang/String;

    return-object p0
.end method

.method public setTextToTranslateLanguageDisplay(Ljava/lang/String;)Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->hasTextToTranslateLanguageDisplay:Z

    iput-object p1, p0, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->textToTranslateLanguageDisplay_:Ljava/lang/String;

    return-object p0
.end method

.method public setTextToTranslateTransliteration(Ljava/lang/String;)Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->hasTextToTranslateTransliteration:Z

    iput-object p1, p0, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->textToTranslateTransliteration_:Ljava/lang/String;

    return-object p0
.end method

.method public setTranslatedText(Ljava/lang/String;)Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->hasTranslatedText:Z

    iput-object p1, p0, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->translatedText_:Ljava/lang/String;

    return-object p0
.end method

.method public setTranslatedTextLanguage(Ljava/lang/String;)Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->hasTranslatedTextLanguage:Z

    iput-object p1, p0, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->translatedTextLanguage_:Ljava/lang/String;

    return-object p0
.end method

.method public setTranslatedTextLanguageDisplay(Ljava/lang/String;)Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->hasTranslatedTextLanguageDisplay:Z

    iput-object p1, p0, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->translatedTextLanguageDisplay_:Ljava/lang/String;

    return-object p0
.end method

.method public setTranslatedTextTransliteration(Ljava/lang/String;)Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->hasTranslatedTextTransliteration:Z

    iput-object p1, p0, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->translatedTextTransliteration_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->hasTextToTranslate()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->getTextToTranslate()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->hasTextToTranslateLanguage()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->getTextToTranslateLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->hasTranslatedText()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->getTranslatedText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->hasTranslatedTextLanguage()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->getTranslatedTextLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->hasTextToTranslateLanguageDisplay()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->getTextToTranslateLanguageDisplay()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->hasTextToTranslateTransliteration()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->getTextToTranslateTransliteration()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->hasTranslatedTextLanguageDisplay()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->getTranslatedTextLanguageDisplay()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->hasTranslatedTextTransliteration()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    invoke-virtual {p0}, Lcom/google/majel/proto/CommonStructuredResponse$TranslationResult;->getTranslatedTextTransliteration()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_7
    return-void
.end method
