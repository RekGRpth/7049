.class public final Lcom/google/majel/proto/ContextProtos$Context;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "ContextProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/majel/proto/ContextProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Context"
.end annotation


# instance fields
.field private actionContext_:Lcom/google/majel/proto/ContextProtos$ActionContext;

.field private cachedSize:I

.field private hasActionContext:Z

.field private hasTextualContext:Z

.field private textualContext_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/ContextProtos$Context;->textualContext_:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/majel/proto/ContextProtos$Context;->actionContext_:Lcom/google/majel/proto/ContextProtos$ActionContext;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/majel/proto/ContextProtos$Context;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getActionContext()Lcom/google/majel/proto/ContextProtos$ActionContext;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ContextProtos$Context;->actionContext_:Lcom/google/majel/proto/ContextProtos$ActionContext;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/ContextProtos$Context;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/ContextProtos$Context;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/majel/proto/ContextProtos$Context;->cachedSize:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/ContextProtos$Context;->hasTextualContext()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/ContextProtos$Context;->getTextualContext()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/ContextProtos$Context;->hasActionContext()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/ContextProtos$Context;->getActionContext()Lcom/google/majel/proto/ContextProtos$ActionContext;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iput v0, p0, Lcom/google/majel/proto/ContextProtos$Context;->cachedSize:I

    return v0
.end method

.method public getTextualContext()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/ContextProtos$Context;->textualContext_:Ljava/lang/String;

    return-object v0
.end method

.method public hasActionContext()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ContextProtos$Context;->hasActionContext:Z

    return v0
.end method

.method public hasTextualContext()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/ContextProtos$Context;->hasTextualContext:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/ContextProtos$Context;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/majel/proto/ContextProtos$Context;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/ContextProtos$Context;->setTextualContext(Ljava/lang/String;)Lcom/google/majel/proto/ContextProtos$Context;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/majel/proto/ContextProtos$ActionContext;

    invoke-direct {v1}, Lcom/google/majel/proto/ContextProtos$ActionContext;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/ContextProtos$Context;->setActionContext(Lcom/google/majel/proto/ContextProtos$ActionContext;)Lcom/google/majel/proto/ContextProtos$Context;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/majel/proto/ContextProtos$Context;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/ContextProtos$Context;

    move-result-object v0

    return-object v0
.end method

.method public setActionContext(Lcom/google/majel/proto/ContextProtos$ActionContext;)Lcom/google/majel/proto/ContextProtos$Context;
    .locals 1
    .param p1    # Lcom/google/majel/proto/ContextProtos$ActionContext;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ContextProtos$Context;->hasActionContext:Z

    iput-object p1, p0, Lcom/google/majel/proto/ContextProtos$Context;->actionContext_:Lcom/google/majel/proto/ContextProtos$ActionContext;

    return-object p0
.end method

.method public setTextualContext(Ljava/lang/String;)Lcom/google/majel/proto/ContextProtos$Context;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/ContextProtos$Context;->hasTextualContext:Z

    iput-object p1, p0, Lcom/google/majel/proto/ContextProtos$Context;->textualContext_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/majel/proto/ContextProtos$Context;->hasTextualContext()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/ContextProtos$Context;->getTextualContext()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/ContextProtos$Context;->hasActionContext()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/ContextProtos$Context;->getActionContext()Lcom/google/majel/proto/ContextProtos$ActionContext;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    return-void
.end method
