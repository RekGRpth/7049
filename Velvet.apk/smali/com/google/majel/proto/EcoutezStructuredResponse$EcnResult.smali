.class public final Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "EcoutezStructuredResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/majel/proto/EcoutezStructuredResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EcnResult"
.end annotation


# instance fields
.field private anchor_:Ljava/lang/String;

.field private cachedSize:I

.field private hasAnchor:Z

.field private hasLastChangeTime:Z

.field private hasLastPrice:Z

.field private hasPriceChange:Z

.field private hasPricePercentChange:Z

.field private lastChangeTime_:Ljava/lang/String;

.field private lastPrice_:F

.field private priceChange_:F

.field private pricePercentChange_:F


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->anchor_:Ljava/lang/String;

    iput v1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->lastPrice_:F

    iput v1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->priceChange_:F

    iput v1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->pricePercentChange_:F

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->lastChangeTime_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getAnchor()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->anchor_:Ljava/lang/String;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->cachedSize:I

    return v0
.end method

.method public getLastChangeTime()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->lastChangeTime_:Ljava/lang/String;

    return-object v0
.end method

.method public getLastPrice()F
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->lastPrice_:F

    return v0
.end method

.method public getPriceChange()F
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->priceChange_:F

    return v0
.end method

.method public getPricePercentChange()F
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->pricePercentChange_:F

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->hasAnchor()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->getAnchor()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->hasLastPrice()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->getLastPrice()F

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->hasPriceChange()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->getPriceChange()F

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->hasPricePercentChange()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->getPricePercentChange()F

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->hasLastChangeTime()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->getLastChangeTime()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iput v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->cachedSize:I

    return v0
.end method

.method public hasAnchor()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->hasAnchor:Z

    return v0
.end method

.method public hasLastChangeTime()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->hasLastChangeTime:Z

    return v0
.end method

.method public hasLastPrice()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->hasLastPrice:Z

    return v0
.end method

.method public hasPriceChange()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->hasPriceChange:Z

    return v0
.end method

.method public hasPricePercentChange()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->hasPricePercentChange:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->setAnchor(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->setLastPrice(F)Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->setPriceChange(F)Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->setPricePercentChange(F)Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->setLastChangeTime(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x15 -> :sswitch_2
        0x1d -> :sswitch_3
        0x25 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;

    move-result-object v0

    return-object v0
.end method

.method public setAnchor(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->hasAnchor:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->anchor_:Ljava/lang/String;

    return-object p0
.end method

.method public setLastChangeTime(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->hasLastChangeTime:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->lastChangeTime_:Ljava/lang/String;

    return-object p0
.end method

.method public setLastPrice(F)Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->hasLastPrice:Z

    iput p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->lastPrice_:F

    return-object p0
.end method

.method public setPriceChange(F)Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->hasPriceChange:Z

    iput p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->priceChange_:F

    return-object p0
.end method

.method public setPricePercentChange(F)Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->hasPricePercentChange:Z

    iput p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->pricePercentChange_:F

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->hasAnchor()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->getAnchor()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->hasLastPrice()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->getLastPrice()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->hasPriceChange()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->getPriceChange()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->hasPricePercentChange()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->getPricePercentChange()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->hasLastChangeTime()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;->getLastChangeTime()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    return-void
.end method
