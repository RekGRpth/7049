.class public final Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "EcoutezStructuredResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/majel/proto/EcoutezStructuredResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EcoutezLocalResult"
.end annotation


# instance fields
.field private actionBikingUrl_:Ljava/lang/String;

.field private actionDrivingUrl_:Ljava/lang/String;

.field private actionTransitUrl_:Ljava/lang/String;

.field private actionWalkingUrl_:Ljava/lang/String;

.field private address1_:Ljava/lang/String;

.field private address2_:Ljava/lang/String;

.field private address_:Ljava/lang/String;

.field private authority_:Ljava/lang/String;

.field private businessDomain_:Ljava/lang/String;

.field private businessUrl_:Ljava/lang/String;

.field private cachedSize:I

.field private clusterId_:Ljava/lang/String;

.field private deprecatedHours_:Ljava/lang/String;

.field private hasActionBikingUrl:Z

.field private hasActionDrivingUrl:Z

.field private hasActionTransitUrl:Z

.field private hasActionWalkingUrl:Z

.field private hasAddress:Z

.field private hasAddress1:Z

.field private hasAddress2:Z

.field private hasAuthority:Z

.field private hasBusinessDomain:Z

.field private hasBusinessUrl:Z

.field private hasClusterId:Z

.field private hasDeprecatedHours:Z

.field private hasHours:Z

.field private hasLatDegrees:Z

.field private hasLatSpanDegrees:Z

.field private hasLngDegrees:Z

.field private hasLngSpanDegrees:Z

.field private hasMapsUrl:Z

.field private hasNearLocation:Z

.field private hasNumHalfStars:Z

.field private hasNumReviews:Z

.field private hasPhoneNumber:Z

.field private hasPlacePageUrl:Z

.field private hasQuery:Z

.field private hasReviewSnippet:Z

.field private hasReviewsText:Z

.field private hasTitle:Z

.field private hasTransitStationName:Z

.field private hasTransitStationText:Z

.field private hasTransitStationType:Z

.field private hours_:Lcom/google/majel/proto/EcoutezStructuredResponse$Hours;

.field private latDegrees_:D

.field private latSpanDegrees_:D

.field private lngDegrees_:D

.field private lngSpanDegrees_:D

.field private mapsUrl_:Ljava/lang/String;

.field private nearLocation_:Ljava/lang/String;

.field private numHalfStars_:I

.field private numReviews_:I

.field private phoneNumber_:Ljava/lang/String;

.field private placePageUrl_:Ljava/lang/String;

.field private query_:Ljava/lang/String;

.field private reviewSite_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/EcoutezStructuredResponse$ReviewSite;",
            ">;"
        }
    .end annotation
.end field

.field private reviewSnippet_:Ljava/lang/String;

.field private reviewsText_:Ljava/lang/String;

.field private title_:Ljava/lang/String;

.field private transitStationName_:Ljava/lang/String;

.field private transitStationText_:Ljava/lang/String;

.field private transitStationType_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x0

    const-wide/16 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->title_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->businessUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->businessDomain_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->placePageUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->mapsUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->actionDrivingUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->actionWalkingUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->actionTransitUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->actionBikingUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->clusterId_:Ljava/lang/String;

    iput-wide v1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->latDegrees_:D

    iput-wide v1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->lngDegrees_:D

    iput-wide v1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->latSpanDegrees_:D

    iput-wide v1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->lngSpanDegrees_:D

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->nearLocation_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->query_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->address_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->address1_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->address2_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->phoneNumber_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->deprecatedHours_:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hours_:Lcom/google/majel/proto/EcoutezStructuredResponse$Hours;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->authority_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->transitStationText_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->transitStationType_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->transitStationName_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->reviewSnippet_:Ljava/lang/String;

    iput v3, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->numHalfStars_:I

    iput v3, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->numReviews_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->reviewsText_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->reviewSite_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addReviewSite(Lcom/google/majel/proto/EcoutezStructuredResponse$ReviewSite;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$ReviewSite;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->reviewSite_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->reviewSite_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->reviewSite_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getActionBikingUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->actionBikingUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getActionDrivingUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->actionDrivingUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getActionTransitUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->actionTransitUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getActionWalkingUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->actionWalkingUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getAddress()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->address_:Ljava/lang/String;

    return-object v0
.end method

.method public getAddress1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->address1_:Ljava/lang/String;

    return-object v0
.end method

.method public getAddress2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->address2_:Ljava/lang/String;

    return-object v0
.end method

.method public getAuthority()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->authority_:Ljava/lang/String;

    return-object v0
.end method

.method public getBusinessDomain()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->businessDomain_:Ljava/lang/String;

    return-object v0
.end method

.method public getBusinessUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->businessUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->cachedSize:I

    return v0
.end method

.method public getClusterId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->clusterId_:Ljava/lang/String;

    return-object v0
.end method

.method public getDeprecatedHours()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->deprecatedHours_:Ljava/lang/String;

    return-object v0
.end method

.method public getHours()Lcom/google/majel/proto/EcoutezStructuredResponse$Hours;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hours_:Lcom/google/majel/proto/EcoutezStructuredResponse$Hours;

    return-object v0
.end method

.method public getLatDegrees()D
    .locals 2

    iget-wide v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->latDegrees_:D

    return-wide v0
.end method

.method public getLatSpanDegrees()D
    .locals 2

    iget-wide v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->latSpanDegrees_:D

    return-wide v0
.end method

.method public getLngDegrees()D
    .locals 2

    iget-wide v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->lngDegrees_:D

    return-wide v0
.end method

.method public getLngSpanDegrees()D
    .locals 2

    iget-wide v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->lngSpanDegrees_:D

    return-wide v0
.end method

.method public getMapsUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->mapsUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getNearLocation()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->nearLocation_:Ljava/lang/String;

    return-object v0
.end method

.method public getNumHalfStars()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->numHalfStars_:I

    return v0
.end method

.method public getNumReviews()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->numReviews_:I

    return v0
.end method

.method public getPhoneNumber()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->phoneNumber_:Ljava/lang/String;

    return-object v0
.end method

.method public getPlacePageUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->placePageUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getQuery()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->query_:Ljava/lang/String;

    return-object v0
.end method

.method public getReviewSiteList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/EcoutezStructuredResponse$ReviewSite;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->reviewSite_:Ljava/util/List;

    return-object v0
.end method

.method public getReviewSnippet()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->reviewSnippet_:Ljava/lang/String;

    return-object v0
.end method

.method public getReviewsText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->reviewsText_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasTitle()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasBusinessUrl()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getBusinessUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasBusinessDomain()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getBusinessDomain()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasPlacePageUrl()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getPlacePageUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasLatDegrees()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getLatDegrees()D

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeDoubleSize(ID)I

    move-result v3

    add-int/2addr v2, v3

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasLngDegrees()Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x6

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getLngDegrees()D

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeDoubleSize(ID)I

    move-result v3

    add-int/2addr v2, v3

    :cond_5
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasNearLocation()Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v3, 0x7

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getNearLocation()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_6
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasQuery()Z

    move-result v3

    if-eqz v3, :cond_7

    const/16 v3, 0x8

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getQuery()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_7
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasAddress()Z

    move-result v3

    if-eqz v3, :cond_8

    const/16 v3, 0x9

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_8
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasAddress1()Z

    move-result v3

    if-eqz v3, :cond_9

    const/16 v3, 0xa

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getAddress1()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_9
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasAddress2()Z

    move-result v3

    if-eqz v3, :cond_a

    const/16 v3, 0xb

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getAddress2()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_a
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasPhoneNumber()Z

    move-result v3

    if-eqz v3, :cond_b

    const/16 v3, 0xc

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getPhoneNumber()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_b
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasDeprecatedHours()Z

    move-result v3

    if-eqz v3, :cond_c

    const/16 v3, 0xd

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getDeprecatedHours()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_c
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasAuthority()Z

    move-result v3

    if-eqz v3, :cond_d

    const/16 v3, 0xe

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getAuthority()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_d
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasTransitStationText()Z

    move-result v3

    if-eqz v3, :cond_e

    const/16 v3, 0xf

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getTransitStationText()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_e
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasTransitStationType()Z

    move-result v3

    if-eqz v3, :cond_f

    const/16 v3, 0x10

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getTransitStationType()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_f
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasTransitStationName()Z

    move-result v3

    if-eqz v3, :cond_10

    const/16 v3, 0x11

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getTransitStationName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_10
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasReviewSnippet()Z

    move-result v3

    if-eqz v3, :cond_11

    const/16 v3, 0x12

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getReviewSnippet()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_11
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasNumHalfStars()Z

    move-result v3

    if-eqz v3, :cond_12

    const/16 v3, 0x13

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getNumHalfStars()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_12
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasNumReviews()Z

    move-result v3

    if-eqz v3, :cond_13

    const/16 v3, 0x14

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getNumReviews()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_13
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasReviewsText()Z

    move-result v3

    if-eqz v3, :cond_14

    const/16 v3, 0x15

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getReviewsText()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_14
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getReviewSiteList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_15

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/EcoutezStructuredResponse$ReviewSite;

    const/16 v3, 0x16

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_15
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasClusterId()Z

    move-result v3

    if-eqz v3, :cond_16

    const/16 v3, 0x17

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getClusterId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_16
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasMapsUrl()Z

    move-result v3

    if-eqz v3, :cond_17

    const/16 v3, 0x18

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getMapsUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_17
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasActionDrivingUrl()Z

    move-result v3

    if-eqz v3, :cond_18

    const/16 v3, 0x19

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getActionDrivingUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_18
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasActionWalkingUrl()Z

    move-result v3

    if-eqz v3, :cond_19

    const/16 v3, 0x1a

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getActionWalkingUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_19
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasActionTransitUrl()Z

    move-result v3

    if-eqz v3, :cond_1a

    const/16 v3, 0x1b

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getActionTransitUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1a
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasActionBikingUrl()Z

    move-result v3

    if-eqz v3, :cond_1b

    const/16 v3, 0x1c

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getActionBikingUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1b
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasLatSpanDegrees()Z

    move-result v3

    if-eqz v3, :cond_1c

    const/16 v3, 0x1d

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getLatSpanDegrees()D

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeDoubleSize(ID)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1c
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasLngSpanDegrees()Z

    move-result v3

    if-eqz v3, :cond_1d

    const/16 v3, 0x1e

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getLngSpanDegrees()D

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeDoubleSize(ID)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1d
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasHours()Z

    move-result v3

    if-eqz v3, :cond_1e

    const/16 v3, 0x1f

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getHours()Lcom/google/majel/proto/EcoutezStructuredResponse$Hours;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1e
    iput v2, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->cachedSize:I

    return v2
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->title_:Ljava/lang/String;

    return-object v0
.end method

.method public getTransitStationName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->transitStationName_:Ljava/lang/String;

    return-object v0
.end method

.method public getTransitStationText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->transitStationText_:Ljava/lang/String;

    return-object v0
.end method

.method public getTransitStationType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->transitStationType_:Ljava/lang/String;

    return-object v0
.end method

.method public hasActionBikingUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasActionBikingUrl:Z

    return v0
.end method

.method public hasActionDrivingUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasActionDrivingUrl:Z

    return v0
.end method

.method public hasActionTransitUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasActionTransitUrl:Z

    return v0
.end method

.method public hasActionWalkingUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasActionWalkingUrl:Z

    return v0
.end method

.method public hasAddress()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasAddress:Z

    return v0
.end method

.method public hasAddress1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasAddress1:Z

    return v0
.end method

.method public hasAddress2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasAddress2:Z

    return v0
.end method

.method public hasAuthority()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasAuthority:Z

    return v0
.end method

.method public hasBusinessDomain()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasBusinessDomain:Z

    return v0
.end method

.method public hasBusinessUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasBusinessUrl:Z

    return v0
.end method

.method public hasClusterId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasClusterId:Z

    return v0
.end method

.method public hasDeprecatedHours()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasDeprecatedHours:Z

    return v0
.end method

.method public hasHours()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasHours:Z

    return v0
.end method

.method public hasLatDegrees()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasLatDegrees:Z

    return v0
.end method

.method public hasLatSpanDegrees()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasLatSpanDegrees:Z

    return v0
.end method

.method public hasLngDegrees()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasLngDegrees:Z

    return v0
.end method

.method public hasLngSpanDegrees()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasLngSpanDegrees:Z

    return v0
.end method

.method public hasMapsUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasMapsUrl:Z

    return v0
.end method

.method public hasNearLocation()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasNearLocation:Z

    return v0
.end method

.method public hasNumHalfStars()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasNumHalfStars:Z

    return v0
.end method

.method public hasNumReviews()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasNumReviews:Z

    return v0
.end method

.method public hasPhoneNumber()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasPhoneNumber:Z

    return v0
.end method

.method public hasPlacePageUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasPlacePageUrl:Z

    return v0
.end method

.method public hasQuery()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasQuery:Z

    return v0
.end method

.method public hasReviewSnippet()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasReviewSnippet:Z

    return v0
.end method

.method public hasReviewsText()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasReviewsText:Z

    return v0
.end method

.method public hasTitle()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasTitle:Z

    return v0
.end method

.method public hasTransitStationName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasTransitStationName:Z

    return v0
.end method

.method public hasTransitStationText()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasTransitStationText:Z

    return v0
.end method

.method public hasTransitStationType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasTransitStationType:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->setTitle(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->setBusinessUrl(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->setBusinessDomain(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->setPlacePageUrl(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readDouble()D

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->setLatDegrees(D)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readDouble()D

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->setLngDegrees(D)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->setNearLocation(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->setQuery(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->setAddress(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->setAddress1(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->setAddress2(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->setPhoneNumber(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->setDeprecatedHours(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;

    goto :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->setAuthority(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;

    goto :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->setTransitStationText(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->setTransitStationType(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->setTransitStationName(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->setReviewSnippet(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;

    goto/16 :goto_0

    :sswitch_13
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->setNumHalfStars(I)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;

    goto/16 :goto_0

    :sswitch_14
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->setNumReviews(I)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;

    goto/16 :goto_0

    :sswitch_15
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->setReviewsText(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;

    goto/16 :goto_0

    :sswitch_16
    new-instance v1, Lcom/google/majel/proto/EcoutezStructuredResponse$ReviewSite;

    invoke-direct {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$ReviewSite;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->addReviewSite(Lcom/google/majel/proto/EcoutezStructuredResponse$ReviewSite;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;

    goto/16 :goto_0

    :sswitch_17
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->setClusterId(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;

    goto/16 :goto_0

    :sswitch_18
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->setMapsUrl(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;

    goto/16 :goto_0

    :sswitch_19
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->setActionDrivingUrl(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;

    goto/16 :goto_0

    :sswitch_1a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->setActionWalkingUrl(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;

    goto/16 :goto_0

    :sswitch_1b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->setActionTransitUrl(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;

    goto/16 :goto_0

    :sswitch_1c
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->setActionBikingUrl(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;

    goto/16 :goto_0

    :sswitch_1d
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readDouble()D

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->setLatSpanDegrees(D)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;

    goto/16 :goto_0

    :sswitch_1e
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readDouble()D

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->setLngSpanDegrees(D)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;

    goto/16 :goto_0

    :sswitch_1f
    new-instance v1, Lcom/google/majel/proto/EcoutezStructuredResponse$Hours;

    invoke-direct {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$Hours;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->setHours(Lcom/google/majel/proto/EcoutezStructuredResponse$Hours;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x29 -> :sswitch_5
        0x31 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
        0x8a -> :sswitch_11
        0x92 -> :sswitch_12
        0x98 -> :sswitch_13
        0xa0 -> :sswitch_14
        0xaa -> :sswitch_15
        0xb2 -> :sswitch_16
        0xba -> :sswitch_17
        0xc2 -> :sswitch_18
        0xca -> :sswitch_19
        0xd2 -> :sswitch_1a
        0xda -> :sswitch_1b
        0xe2 -> :sswitch_1c
        0xe9 -> :sswitch_1d
        0xf1 -> :sswitch_1e
        0xfa -> :sswitch_1f
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;

    move-result-object v0

    return-object v0
.end method

.method public setActionBikingUrl(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasActionBikingUrl:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->actionBikingUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setActionDrivingUrl(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasActionDrivingUrl:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->actionDrivingUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setActionTransitUrl(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasActionTransitUrl:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->actionTransitUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setActionWalkingUrl(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasActionWalkingUrl:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->actionWalkingUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setAddress(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasAddress:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->address_:Ljava/lang/String;

    return-object p0
.end method

.method public setAddress1(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasAddress1:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->address1_:Ljava/lang/String;

    return-object p0
.end method

.method public setAddress2(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasAddress2:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->address2_:Ljava/lang/String;

    return-object p0
.end method

.method public setAuthority(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasAuthority:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->authority_:Ljava/lang/String;

    return-object p0
.end method

.method public setBusinessDomain(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasBusinessDomain:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->businessDomain_:Ljava/lang/String;

    return-object p0
.end method

.method public setBusinessUrl(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasBusinessUrl:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->businessUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setClusterId(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasClusterId:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->clusterId_:Ljava/lang/String;

    return-object p0
.end method

.method public setDeprecatedHours(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasDeprecatedHours:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->deprecatedHours_:Ljava/lang/String;

    return-object p0
.end method

.method public setHours(Lcom/google/majel/proto/EcoutezStructuredResponse$Hours;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$Hours;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasHours:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hours_:Lcom/google/majel/proto/EcoutezStructuredResponse$Hours;

    return-object p0
.end method

.method public setLatDegrees(D)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;
    .locals 1
    .param p1    # D

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasLatDegrees:Z

    iput-wide p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->latDegrees_:D

    return-object p0
.end method

.method public setLatSpanDegrees(D)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;
    .locals 1
    .param p1    # D

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasLatSpanDegrees:Z

    iput-wide p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->latSpanDegrees_:D

    return-object p0
.end method

.method public setLngDegrees(D)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;
    .locals 1
    .param p1    # D

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasLngDegrees:Z

    iput-wide p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->lngDegrees_:D

    return-object p0
.end method

.method public setLngSpanDegrees(D)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;
    .locals 1
    .param p1    # D

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasLngSpanDegrees:Z

    iput-wide p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->lngSpanDegrees_:D

    return-object p0
.end method

.method public setMapsUrl(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasMapsUrl:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->mapsUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setNearLocation(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasNearLocation:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->nearLocation_:Ljava/lang/String;

    return-object p0
.end method

.method public setNumHalfStars(I)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasNumHalfStars:Z

    iput p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->numHalfStars_:I

    return-object p0
.end method

.method public setNumReviews(I)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasNumReviews:Z

    iput p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->numReviews_:I

    return-object p0
.end method

.method public setPhoneNumber(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasPhoneNumber:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->phoneNumber_:Ljava/lang/String;

    return-object p0
.end method

.method public setPlacePageUrl(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasPlacePageUrl:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->placePageUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setQuery(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasQuery:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->query_:Ljava/lang/String;

    return-object p0
.end method

.method public setReviewSnippet(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasReviewSnippet:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->reviewSnippet_:Ljava/lang/String;

    return-object p0
.end method

.method public setReviewsText(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasReviewsText:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->reviewsText_:Ljava/lang/String;

    return-object p0
.end method

.method public setTitle(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasTitle:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->title_:Ljava/lang/String;

    return-object p0
.end method

.method public setTransitStationName(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasTransitStationName:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->transitStationName_:Ljava/lang/String;

    return-object p0
.end method

.method public setTransitStationText(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasTransitStationText:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->transitStationText_:Ljava/lang/String;

    return-object p0
.end method

.method public setTransitStationType(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasTransitStationType:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->transitStationType_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 5
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasTitle()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasBusinessUrl()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getBusinessUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasBusinessDomain()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getBusinessDomain()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasPlacePageUrl()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getPlacePageUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasLatDegrees()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getLatDegrees()D

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeDouble(ID)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasLngDegrees()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getLngDegrees()D

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeDouble(ID)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasNearLocation()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getNearLocation()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasQuery()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getQuery()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasAddress()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasAddress1()Z

    move-result v2

    if-eqz v2, :cond_9

    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getAddress1()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasAddress2()Z

    move-result v2

    if-eqz v2, :cond_a

    const/16 v2, 0xb

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getAddress2()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasPhoneNumber()Z

    move-result v2

    if-eqz v2, :cond_b

    const/16 v2, 0xc

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getPhoneNumber()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_b
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasDeprecatedHours()Z

    move-result v2

    if-eqz v2, :cond_c

    const/16 v2, 0xd

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getDeprecatedHours()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_c
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasAuthority()Z

    move-result v2

    if-eqz v2, :cond_d

    const/16 v2, 0xe

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getAuthority()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_d
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasTransitStationText()Z

    move-result v2

    if-eqz v2, :cond_e

    const/16 v2, 0xf

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getTransitStationText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_e
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasTransitStationType()Z

    move-result v2

    if-eqz v2, :cond_f

    const/16 v2, 0x10

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getTransitStationType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_f
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasTransitStationName()Z

    move-result v2

    if-eqz v2, :cond_10

    const/16 v2, 0x11

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getTransitStationName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_10
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasReviewSnippet()Z

    move-result v2

    if-eqz v2, :cond_11

    const/16 v2, 0x12

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getReviewSnippet()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_11
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasNumHalfStars()Z

    move-result v2

    if-eqz v2, :cond_12

    const/16 v2, 0x13

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getNumHalfStars()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_12
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasNumReviews()Z

    move-result v2

    if-eqz v2, :cond_13

    const/16 v2, 0x14

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getNumReviews()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_13
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasReviewsText()Z

    move-result v2

    if-eqz v2, :cond_14

    const/16 v2, 0x15

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getReviewsText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_14
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getReviewSiteList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_15

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/EcoutezStructuredResponse$ReviewSite;

    const/16 v2, 0x16

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_15
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasClusterId()Z

    move-result v2

    if-eqz v2, :cond_16

    const/16 v2, 0x17

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getClusterId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_16
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasMapsUrl()Z

    move-result v2

    if-eqz v2, :cond_17

    const/16 v2, 0x18

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getMapsUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_17
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasActionDrivingUrl()Z

    move-result v2

    if-eqz v2, :cond_18

    const/16 v2, 0x19

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getActionDrivingUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_18
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasActionWalkingUrl()Z

    move-result v2

    if-eqz v2, :cond_19

    const/16 v2, 0x1a

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getActionWalkingUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_19
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasActionTransitUrl()Z

    move-result v2

    if-eqz v2, :cond_1a

    const/16 v2, 0x1b

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getActionTransitUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1a
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasActionBikingUrl()Z

    move-result v2

    if-eqz v2, :cond_1b

    const/16 v2, 0x1c

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getActionBikingUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1b
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasLatSpanDegrees()Z

    move-result v2

    if-eqz v2, :cond_1c

    const/16 v2, 0x1d

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getLatSpanDegrees()D

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeDouble(ID)V

    :cond_1c
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasLngSpanDegrees()Z

    move-result v2

    if-eqz v2, :cond_1d

    const/16 v2, 0x1e

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getLngSpanDegrees()D

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeDouble(ID)V

    :cond_1d
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->hasHours()Z

    move-result v2

    if-eqz v2, :cond_1e

    const/16 v2, 0x1f

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getHours()Lcom/google/majel/proto/EcoutezStructuredResponse$Hours;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1e
    return-void
.end method
