.class public final Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "EcoutezStructuredResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/majel/proto/EcoutezStructuredResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FinanceResult"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult$TopLink;
    }
.end annotation


# instance fields
.field private cachedSize:I

.field private chartLink_:Ljava/lang/String;

.field private chartUrl_:Ljava/lang/String;

.field private company_:Ljava/lang/String;

.field private ecnResult_:Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;

.field private exchangeCode_:Ljava/lang/String;

.field private exchange_:Ljava/lang/String;

.field private hasChartLink:Z

.field private hasChartUrl:Z

.field private hasCompany:Z

.field private hasEcnResult:Z

.field private hasExchange:Z

.field private hasExchangeCode:Z

.field private hasStockResult:Z

.field private hasSymbol:Z

.field private hasSymbolUrl:Z

.field private stockResult_:Lcom/google/majel/proto/EcoutezStructuredResponse$StockResult;

.field private symbolUrl_:Ljava/lang/String;

.field private symbol_:Ljava/lang/String;

.field private topLink_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult$TopLink;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->symbol_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->symbolUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->company_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->exchange_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->exchangeCode_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->topLink_:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->chartUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->chartLink_:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->stockResult_:Lcom/google/majel/proto/EcoutezStructuredResponse$StockResult;

    iput-object v1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->ecnResult_:Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addTopLink(Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult$TopLink;)Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult$TopLink;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->topLink_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->topLink_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->topLink_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->cachedSize:I

    return v0
.end method

.method public getChartLink()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->chartLink_:Ljava/lang/String;

    return-object v0
.end method

.method public getChartUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->chartUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getCompany()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->company_:Ljava/lang/String;

    return-object v0
.end method

.method public getEcnResult()Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->ecnResult_:Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;

    return-object v0
.end method

.method public getExchange()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->exchange_:Ljava/lang/String;

    return-object v0
.end method

.method public getExchangeCode()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->exchangeCode_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->hasSymbol()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->getSymbol()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->hasSymbolUrl()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->getSymbolUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->hasCompany()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->getCompany()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->hasExchange()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->getExchange()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->hasExchangeCode()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->getExchangeCode()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->getTopLinkList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult$TopLink;

    const/4 v3, 0x6

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->hasChartUrl()Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v3, 0x7

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->getChartUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_6
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->hasChartLink()Z

    move-result v3

    if-eqz v3, :cond_7

    const/16 v3, 0x8

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->getChartLink()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_7
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->hasStockResult()Z

    move-result v3

    if-eqz v3, :cond_8

    const/16 v3, 0x9

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->getStockResult()Lcom/google/majel/proto/EcoutezStructuredResponse$StockResult;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_8
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->hasEcnResult()Z

    move-result v3

    if-eqz v3, :cond_9

    const/16 v3, 0xa

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->getEcnResult()Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_9
    iput v2, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->cachedSize:I

    return v2
.end method

.method public getStockResult()Lcom/google/majel/proto/EcoutezStructuredResponse$StockResult;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->stockResult_:Lcom/google/majel/proto/EcoutezStructuredResponse$StockResult;

    return-object v0
.end method

.method public getSymbol()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->symbol_:Ljava/lang/String;

    return-object v0
.end method

.method public getSymbolUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->symbolUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getTopLinkList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult$TopLink;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->topLink_:Ljava/util/List;

    return-object v0
.end method

.method public hasChartLink()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->hasChartLink:Z

    return v0
.end method

.method public hasChartUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->hasChartUrl:Z

    return v0
.end method

.method public hasCompany()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->hasCompany:Z

    return v0
.end method

.method public hasEcnResult()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->hasEcnResult:Z

    return v0
.end method

.method public hasExchange()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->hasExchange:Z

    return v0
.end method

.method public hasExchangeCode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->hasExchangeCode:Z

    return v0
.end method

.method public hasStockResult()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->hasStockResult:Z

    return v0
.end method

.method public hasSymbol()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->hasSymbol:Z

    return v0
.end method

.method public hasSymbolUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->hasSymbolUrl:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->setSymbol(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->setSymbolUrl(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->setCompany(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->setExchange(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->setExchangeCode(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;

    goto :goto_0

    :sswitch_6
    new-instance v1, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult$TopLink;

    invoke-direct {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult$TopLink;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->addTopLink(Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult$TopLink;)Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->setChartUrl(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->setChartLink(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;

    goto :goto_0

    :sswitch_9
    new-instance v1, Lcom/google/majel/proto/EcoutezStructuredResponse$StockResult;

    invoke-direct {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$StockResult;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->setStockResult(Lcom/google/majel/proto/EcoutezStructuredResponse$StockResult;)Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;

    goto :goto_0

    :sswitch_a
    new-instance v1, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;

    invoke-direct {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->setEcnResult(Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;)Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;

    move-result-object v0

    return-object v0
.end method

.method public setChartLink(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->hasChartLink:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->chartLink_:Ljava/lang/String;

    return-object p0
.end method

.method public setChartUrl(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->hasChartUrl:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->chartUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setCompany(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->hasCompany:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->company_:Ljava/lang/String;

    return-object p0
.end method

.method public setEcnResult(Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;)Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->hasEcnResult:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->ecnResult_:Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;

    return-object p0
.end method

.method public setExchange(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->hasExchange:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->exchange_:Ljava/lang/String;

    return-object p0
.end method

.method public setExchangeCode(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->hasExchangeCode:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->exchangeCode_:Ljava/lang/String;

    return-object p0
.end method

.method public setStockResult(Lcom/google/majel/proto/EcoutezStructuredResponse$StockResult;)Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$StockResult;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->hasStockResult:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->stockResult_:Lcom/google/majel/proto/EcoutezStructuredResponse$StockResult;

    return-object p0
.end method

.method public setSymbol(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->hasSymbol:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->symbol_:Ljava/lang/String;

    return-object p0
.end method

.method public setSymbolUrl(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->hasSymbolUrl:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->symbolUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->hasSymbol()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->getSymbol()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->hasSymbolUrl()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->getSymbolUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->hasCompany()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->getCompany()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->hasExchange()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->getExchange()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->hasExchangeCode()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->getExchangeCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->getTopLinkList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult$TopLink;

    const/4 v2, 0x6

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->hasChartUrl()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->getChartUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->hasChartLink()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->getChartLink()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->hasStockResult()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->getStockResult()Lcom/google/majel/proto/EcoutezStructuredResponse$StockResult;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->hasEcnResult()Z

    move-result v2

    if-eqz v2, :cond_9

    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$FinanceResult;->getEcnResult()Lcom/google/majel/proto/EcoutezStructuredResponse$EcnResult;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_9
    return-void
.end method
