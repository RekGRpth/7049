.class public final Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "EcoutezStructuredResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/majel/proto/EcoutezStructuredResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "KnowledgeResult"
.end annotation


# instance fields
.field private cachedSize:I

.field private descriptionAttribution_:Lcom/google/majel/proto/AttributionProtos$Attribution;

.field private description_:Ljava/lang/String;

.field private fact_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/EcoutezStructuredResponse$Fact;",
            ">;"
        }
    .end annotation
.end field

.field private hasDescription:Z

.field private hasDescriptionAttribution:Z

.field private hasTitle:Z

.field private title_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;->title_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;->description_:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;->descriptionAttribution_:Lcom/google/majel/proto/AttributionProtos$Attribution;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;->fact_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addFact(Lcom/google/majel/proto/EcoutezStructuredResponse$Fact;)Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$Fact;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;->fact_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;->fact_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;->fact_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;->cachedSize:I

    return v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;->description_:Ljava/lang/String;

    return-object v0
.end method

.method public getDescriptionAttribution()Lcom/google/majel/proto/AttributionProtos$Attribution;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;->descriptionAttribution_:Lcom/google/majel/proto/AttributionProtos$Attribution;

    return-object v0
.end method

.method public getFactList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/EcoutezStructuredResponse$Fact;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;->fact_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;->hasTitle()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;->hasDescription()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;->getDescription()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;->hasDescriptionAttribution()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;->getDescriptionAttribution()Lcom/google/majel/proto/AttributionProtos$Attribution;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;->getFactList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/EcoutezStructuredResponse$Fact;

    const/4 v3, 0x4

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_3
    iput v2, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;->cachedSize:I

    return v2
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;->title_:Ljava/lang/String;

    return-object v0
.end method

.method public hasDescription()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;->hasDescription:Z

    return v0
.end method

.method public hasDescriptionAttribution()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;->hasDescriptionAttribution:Z

    return v0
.end method

.method public hasTitle()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;->hasTitle:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;->setTitle(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;->setDescription(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/majel/proto/AttributionProtos$Attribution;

    invoke-direct {v1}, Lcom/google/majel/proto/AttributionProtos$Attribution;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;->setDescriptionAttribution(Lcom/google/majel/proto/AttributionProtos$Attribution;)Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/majel/proto/EcoutezStructuredResponse$Fact;

    invoke-direct {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$Fact;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;->addFact(Lcom/google/majel/proto/EcoutezStructuredResponse$Fact;)Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;

    move-result-object v0

    return-object v0
.end method

.method public setDescription(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;->hasDescription:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;->description_:Ljava/lang/String;

    return-object p0
.end method

.method public setDescriptionAttribution(Lcom/google/majel/proto/AttributionProtos$Attribution;)Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;
    .locals 1
    .param p1    # Lcom/google/majel/proto/AttributionProtos$Attribution;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;->hasDescriptionAttribution:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;->descriptionAttribution_:Lcom/google/majel/proto/AttributionProtos$Attribution;

    return-object p0
.end method

.method public setTitle(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;->hasTitle:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;->title_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;->hasTitle()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;->hasDescription()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;->getDescription()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;->hasDescriptionAttribution()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;->getDescriptionAttribution()Lcom/google/majel/proto/AttributionProtos$Attribution;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$KnowledgeResult;->getFactList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/EcoutezStructuredResponse$Fact;

    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_3
    return-void
.end method
