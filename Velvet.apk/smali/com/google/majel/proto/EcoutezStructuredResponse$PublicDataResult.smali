.class public final Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "EcoutezStructuredResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/majel/proto/EcoutezStructuredResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PublicDataResult"
.end annotation


# instance fields
.field private cachedSize:I

.field private disclaimerText_:Ljava/lang/String;

.field private disclaimerUrl_:Ljava/lang/String;

.field private hasDisclaimerText:Z

.field private hasDisclaimerUrl:Z

.field private hasSourceLabel:Z

.field private hasSourceName:Z

.field private hasSymbolAfter:Z

.field private hasSymbolBefore:Z

.field private hasTime:Z

.field private hasUnit:Z

.field private hasValue:Z

.field private sourceLabel_:Ljava/lang/String;

.field private sourceName_:Ljava/lang/String;

.field private symbolAfter_:Ljava/lang/String;

.field private symbolBefore_:Ljava/lang/String;

.field private time_:Ljava/lang/String;

.field private unit_:Ljava/lang/String;

.field private value_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->symbolBefore_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->value_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->symbolAfter_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->unit_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->time_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->sourceLabel_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->sourceName_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->disclaimerUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->disclaimerText_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->cachedSize:I

    return v0
.end method

.method public getDisclaimerText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->disclaimerText_:Ljava/lang/String;

    return-object v0
.end method

.method public getDisclaimerUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->disclaimerUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->hasSymbolBefore()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->getSymbolBefore()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->hasValue()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->hasSymbolAfter()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->getSymbolAfter()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->hasUnit()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->getUnit()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->hasTime()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->getTime()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->hasSourceLabel()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->getSourceLabel()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->hasSourceName()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->getSourceName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->hasDisclaimerUrl()Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->getDisclaimerUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->hasDisclaimerText()Z

    move-result v1

    if-eqz v1, :cond_8

    const/16 v1, 0x9

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->getDisclaimerText()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iput v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->cachedSize:I

    return v0
.end method

.method public getSourceLabel()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->sourceLabel_:Ljava/lang/String;

    return-object v0
.end method

.method public getSourceName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->sourceName_:Ljava/lang/String;

    return-object v0
.end method

.method public getSymbolAfter()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->symbolAfter_:Ljava/lang/String;

    return-object v0
.end method

.method public getSymbolBefore()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->symbolBefore_:Ljava/lang/String;

    return-object v0
.end method

.method public getTime()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->time_:Ljava/lang/String;

    return-object v0
.end method

.method public getUnit()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->unit_:Ljava/lang/String;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->value_:Ljava/lang/String;

    return-object v0
.end method

.method public hasDisclaimerText()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->hasDisclaimerText:Z

    return v0
.end method

.method public hasDisclaimerUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->hasDisclaimerUrl:Z

    return v0
.end method

.method public hasSourceLabel()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->hasSourceLabel:Z

    return v0
.end method

.method public hasSourceName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->hasSourceName:Z

    return v0
.end method

.method public hasSymbolAfter()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->hasSymbolAfter:Z

    return v0
.end method

.method public hasSymbolBefore()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->hasSymbolBefore:Z

    return v0
.end method

.method public hasTime()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->hasTime:Z

    return v0
.end method

.method public hasUnit()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->hasUnit:Z

    return v0
.end method

.method public hasValue()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->hasValue:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->setSymbolBefore(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->setValue(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->setSymbolAfter(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->setUnit(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->setTime(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->setSourceLabel(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->setSourceName(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->setDisclaimerUrl(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->setDisclaimerText(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;

    move-result-object v0

    return-object v0
.end method

.method public setDisclaimerText(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->hasDisclaimerText:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->disclaimerText_:Ljava/lang/String;

    return-object p0
.end method

.method public setDisclaimerUrl(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->hasDisclaimerUrl:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->disclaimerUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setSourceLabel(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->hasSourceLabel:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->sourceLabel_:Ljava/lang/String;

    return-object p0
.end method

.method public setSourceName(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->hasSourceName:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->sourceName_:Ljava/lang/String;

    return-object p0
.end method

.method public setSymbolAfter(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->hasSymbolAfter:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->symbolAfter_:Ljava/lang/String;

    return-object p0
.end method

.method public setSymbolBefore(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->hasSymbolBefore:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->symbolBefore_:Ljava/lang/String;

    return-object p0
.end method

.method public setTime(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->hasTime:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->time_:Ljava/lang/String;

    return-object p0
.end method

.method public setUnit(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->hasUnit:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->unit_:Ljava/lang/String;

    return-object p0
.end method

.method public setValue(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->hasValue:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->value_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->hasSymbolBefore()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->getSymbolBefore()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->hasValue()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->hasSymbolAfter()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->getSymbolAfter()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->hasUnit()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->getUnit()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->hasTime()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->getTime()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->hasSourceLabel()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->getSourceLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->hasSourceName()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->getSourceName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->hasDisclaimerUrl()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->getDisclaimerUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->hasDisclaimerText()Z

    move-result v0

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$PublicDataResult;->getDisclaimerText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_8
    return-void
.end method
