.class public final Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "EcoutezStructuredResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/majel/proto/EcoutezStructuredResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TeamData"
.end annotation


# instance fields
.field private cachedSize:I

.field private dEPRECATEDLogoUrl_:Ljava/lang/String;

.field private hasDEPRECATEDLogoUrl:Z

.field private hasInProgressMatch:Z

.field private hasLastMatch:Z

.field private hasLogo:Z

.field private hasMatchList:Z

.field private hasName:Z

.field private hasNextMatch:Z

.field private hasPlayoffsStandings:Z

.field private hasRegularSeasonStandings:Z

.field private hasShortName:Z

.field private inProgressMatch_:Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

.field private lastMatch_:Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

.field private logo_:Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;

.field private matchList_:Lcom/google/majel/proto/EcoutezStructuredResponse$MatchList;

.field private name_:Ljava/lang/String;

.field private nextMatch_:Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

.field private playoffsStandings_:Lcom/google/majel/proto/EcoutezStructuredResponse$PlayoffsStandings;

.field private regularSeasonStandings_:Lcom/google/majel/proto/EcoutezStructuredResponse$RegularSeasonStandings;

.field private shortName_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->name_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->shortName_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->dEPRECATEDLogoUrl_:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->logo_:Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;

    iput-object v1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->regularSeasonStandings_:Lcom/google/majel/proto/EcoutezStructuredResponse$RegularSeasonStandings;

    iput-object v1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->playoffsStandings_:Lcom/google/majel/proto/EcoutezStructuredResponse$PlayoffsStandings;

    iput-object v1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->matchList_:Lcom/google/majel/proto/EcoutezStructuredResponse$MatchList;

    iput-object v1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->lastMatch_:Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    iput-object v1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->nextMatch_:Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    iput-object v1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->inProgressMatch_:Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->cachedSize:I

    return v0
.end method

.method public getDEPRECATEDLogoUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->dEPRECATEDLogoUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getInProgressMatch()Lcom/google/majel/proto/EcoutezStructuredResponse$Match;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->inProgressMatch_:Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    return-object v0
.end method

.method public getLastMatch()Lcom/google/majel/proto/EcoutezStructuredResponse$Match;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->lastMatch_:Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    return-object v0
.end method

.method public getLogo()Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->logo_:Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;

    return-object v0
.end method

.method public getMatchList()Lcom/google/majel/proto/EcoutezStructuredResponse$MatchList;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->matchList_:Lcom/google/majel/proto/EcoutezStructuredResponse$MatchList;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getNextMatch()Lcom/google/majel/proto/EcoutezStructuredResponse$Match;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->nextMatch_:Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    return-object v0
.end method

.method public getPlayoffsStandings()Lcom/google/majel/proto/EcoutezStructuredResponse$PlayoffsStandings;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->playoffsStandings_:Lcom/google/majel/proto/EcoutezStructuredResponse$PlayoffsStandings;

    return-object v0
.end method

.method public getRegularSeasonStandings()Lcom/google/majel/proto/EcoutezStructuredResponse$RegularSeasonStandings;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->regularSeasonStandings_:Lcom/google/majel/proto/EcoutezStructuredResponse$RegularSeasonStandings;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->hasName()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->hasRegularSeasonStandings()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->getRegularSeasonStandings()Lcom/google/majel/proto/EcoutezStructuredResponse$RegularSeasonStandings;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->hasPlayoffsStandings()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->getPlayoffsStandings()Lcom/google/majel/proto/EcoutezStructuredResponse$PlayoffsStandings;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->hasMatchList()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->getMatchList()Lcom/google/majel/proto/EcoutezStructuredResponse$MatchList;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->hasLastMatch()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->getLastMatch()Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->hasNextMatch()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->getNextMatch()Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->hasInProgressMatch()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->getInProgressMatch()Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->hasDEPRECATEDLogoUrl()Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->getDEPRECATEDLogoUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->hasLogo()Z

    move-result v1

    if-eqz v1, :cond_8

    const/16 v1, 0x9

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->getLogo()Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->hasShortName()Z

    move-result v1

    if-eqz v1, :cond_9

    const/16 v1, 0xa

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->getShortName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iput v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->cachedSize:I

    return v0
.end method

.method public getShortName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->shortName_:Ljava/lang/String;

    return-object v0
.end method

.method public hasDEPRECATEDLogoUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->hasDEPRECATEDLogoUrl:Z

    return v0
.end method

.method public hasInProgressMatch()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->hasInProgressMatch:Z

    return v0
.end method

.method public hasLastMatch()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->hasLastMatch:Z

    return v0
.end method

.method public hasLogo()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->hasLogo:Z

    return v0
.end method

.method public hasMatchList()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->hasMatchList:Z

    return v0
.end method

.method public hasName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->hasName:Z

    return v0
.end method

.method public hasNextMatch()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->hasNextMatch:Z

    return v0
.end method

.method public hasPlayoffsStandings()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->hasPlayoffsStandings:Z

    return v0
.end method

.method public hasRegularSeasonStandings()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->hasRegularSeasonStandings:Z

    return v0
.end method

.method public hasShortName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->hasShortName:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->setName(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/majel/proto/EcoutezStructuredResponse$RegularSeasonStandings;

    invoke-direct {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$RegularSeasonStandings;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->setRegularSeasonStandings(Lcom/google/majel/proto/EcoutezStructuredResponse$RegularSeasonStandings;)Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/majel/proto/EcoutezStructuredResponse$PlayoffsStandings;

    invoke-direct {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$PlayoffsStandings;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->setPlayoffsStandings(Lcom/google/majel/proto/EcoutezStructuredResponse$PlayoffsStandings;)Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/majel/proto/EcoutezStructuredResponse$MatchList;

    invoke-direct {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$MatchList;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->setMatchList(Lcom/google/majel/proto/EcoutezStructuredResponse$MatchList;)Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    invoke-direct {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->setLastMatch(Lcom/google/majel/proto/EcoutezStructuredResponse$Match;)Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;

    goto :goto_0

    :sswitch_6
    new-instance v1, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    invoke-direct {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->setNextMatch(Lcom/google/majel/proto/EcoutezStructuredResponse$Match;)Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;

    goto :goto_0

    :sswitch_7
    new-instance v1, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    invoke-direct {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$Match;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->setInProgressMatch(Lcom/google/majel/proto/EcoutezStructuredResponse$Match;)Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->setDEPRECATEDLogoUrl(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;

    goto :goto_0

    :sswitch_9
    new-instance v1, Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;

    invoke-direct {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->setLogo(Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;)Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->setShortName(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;

    move-result-object v0

    return-object v0
.end method

.method public setDEPRECATEDLogoUrl(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->hasDEPRECATEDLogoUrl:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->dEPRECATEDLogoUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setInProgressMatch(Lcom/google/majel/proto/EcoutezStructuredResponse$Match;)Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->hasInProgressMatch:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->inProgressMatch_:Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    return-object p0
.end method

.method public setLastMatch(Lcom/google/majel/proto/EcoutezStructuredResponse$Match;)Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->hasLastMatch:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->lastMatch_:Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    return-object p0
.end method

.method public setLogo(Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;)Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->hasLogo:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->logo_:Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;

    return-object p0
.end method

.method public setMatchList(Lcom/google/majel/proto/EcoutezStructuredResponse$MatchList;)Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$MatchList;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->hasMatchList:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->matchList_:Lcom/google/majel/proto/EcoutezStructuredResponse$MatchList;

    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->hasName:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->name_:Ljava/lang/String;

    return-object p0
.end method

.method public setNextMatch(Lcom/google/majel/proto/EcoutezStructuredResponse$Match;)Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->hasNextMatch:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->nextMatch_:Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    return-object p0
.end method

.method public setPlayoffsStandings(Lcom/google/majel/proto/EcoutezStructuredResponse$PlayoffsStandings;)Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$PlayoffsStandings;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->hasPlayoffsStandings:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->playoffsStandings_:Lcom/google/majel/proto/EcoutezStructuredResponse$PlayoffsStandings;

    return-object p0
.end method

.method public setRegularSeasonStandings(Lcom/google/majel/proto/EcoutezStructuredResponse$RegularSeasonStandings;)Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$RegularSeasonStandings;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->hasRegularSeasonStandings:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->regularSeasonStandings_:Lcom/google/majel/proto/EcoutezStructuredResponse$RegularSeasonStandings;

    return-object p0
.end method

.method public setShortName(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->hasShortName:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->shortName_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->hasName()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->hasRegularSeasonStandings()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->getRegularSeasonStandings()Lcom/google/majel/proto/EcoutezStructuredResponse$RegularSeasonStandings;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->hasPlayoffsStandings()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->getPlayoffsStandings()Lcom/google/majel/proto/EcoutezStructuredResponse$PlayoffsStandings;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->hasMatchList()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->getMatchList()Lcom/google/majel/proto/EcoutezStructuredResponse$MatchList;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->hasLastMatch()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->getLastMatch()Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->hasNextMatch()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->getNextMatch()Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->hasInProgressMatch()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->getInProgressMatch()Lcom/google/majel/proto/EcoutezStructuredResponse$Match;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->hasDEPRECATEDLogoUrl()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->getDEPRECATEDLogoUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->hasLogo()Z

    move-result v0

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->getLogo()Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->hasShortName()Z

    move-result v0

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamData;->getShortName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_9
    return-void
.end method
