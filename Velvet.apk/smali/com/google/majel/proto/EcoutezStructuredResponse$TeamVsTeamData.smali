.class public final Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "EcoutezStructuredResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/majel/proto/EcoutezStructuredResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TeamVsTeamData"
.end annotation


# instance fields
.field private cachedSize:I

.field private dEPRECATEDFirstTeamLogoUrl_:Ljava/lang/String;

.field private dEPRECATEDSecondTeamLogoUrl_:Ljava/lang/String;

.field private firstTeamLogo_:Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;

.field private firstTeamShortName_:Ljava/lang/String;

.field private firstTeam_:Ljava/lang/String;

.field private hasDEPRECATEDFirstTeamLogoUrl:Z

.field private hasDEPRECATEDSecondTeamLogoUrl:Z

.field private hasFirstTeam:Z

.field private hasFirstTeamLogo:Z

.field private hasFirstTeamShortName:Z

.field private hasMatchList:Z

.field private hasSecondTeam:Z

.field private hasSecondTeamLogo:Z

.field private hasSecondTeamShortName:Z

.field private matchList_:Lcom/google/majel/proto/EcoutezStructuredResponse$MatchList;

.field private secondTeamLogo_:Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;

.field private secondTeamShortName_:Ljava/lang/String;

.field private secondTeam_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->firstTeam_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->firstTeamShortName_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->dEPRECATEDFirstTeamLogoUrl_:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->firstTeamLogo_:Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->secondTeam_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->secondTeamShortName_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->dEPRECATEDSecondTeamLogoUrl_:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->secondTeamLogo_:Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;

    iput-object v1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->matchList_:Lcom/google/majel/proto/EcoutezStructuredResponse$MatchList;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->cachedSize:I

    return v0
.end method

.method public getDEPRECATEDFirstTeamLogoUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->dEPRECATEDFirstTeamLogoUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getDEPRECATEDSecondTeamLogoUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->dEPRECATEDSecondTeamLogoUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getFirstTeam()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->firstTeam_:Ljava/lang/String;

    return-object v0
.end method

.method public getFirstTeamLogo()Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->firstTeamLogo_:Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;

    return-object v0
.end method

.method public getFirstTeamShortName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->firstTeamShortName_:Ljava/lang/String;

    return-object v0
.end method

.method public getMatchList()Lcom/google/majel/proto/EcoutezStructuredResponse$MatchList;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->matchList_:Lcom/google/majel/proto/EcoutezStructuredResponse$MatchList;

    return-object v0
.end method

.method public getSecondTeam()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->secondTeam_:Ljava/lang/String;

    return-object v0
.end method

.method public getSecondTeamLogo()Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->secondTeamLogo_:Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;

    return-object v0
.end method

.method public getSecondTeamShortName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->secondTeamShortName_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->hasFirstTeam()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->getFirstTeam()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->hasSecondTeam()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->getSecondTeam()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->hasMatchList()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->getMatchList()Lcom/google/majel/proto/EcoutezStructuredResponse$MatchList;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->hasDEPRECATEDFirstTeamLogoUrl()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->getDEPRECATEDFirstTeamLogoUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->hasDEPRECATEDSecondTeamLogoUrl()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->getDEPRECATEDSecondTeamLogoUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->hasFirstTeamLogo()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->getFirstTeamLogo()Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->hasSecondTeamLogo()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->getSecondTeamLogo()Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->hasFirstTeamShortName()Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->getFirstTeamShortName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->hasSecondTeamShortName()Z

    move-result v1

    if-eqz v1, :cond_8

    const/16 v1, 0x9

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->getSecondTeamShortName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iput v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->cachedSize:I

    return v0
.end method

.method public hasDEPRECATEDFirstTeamLogoUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->hasDEPRECATEDFirstTeamLogoUrl:Z

    return v0
.end method

.method public hasDEPRECATEDSecondTeamLogoUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->hasDEPRECATEDSecondTeamLogoUrl:Z

    return v0
.end method

.method public hasFirstTeam()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->hasFirstTeam:Z

    return v0
.end method

.method public hasFirstTeamLogo()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->hasFirstTeamLogo:Z

    return v0
.end method

.method public hasFirstTeamShortName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->hasFirstTeamShortName:Z

    return v0
.end method

.method public hasMatchList()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->hasMatchList:Z

    return v0
.end method

.method public hasSecondTeam()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->hasSecondTeam:Z

    return v0
.end method

.method public hasSecondTeamLogo()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->hasSecondTeamLogo:Z

    return v0
.end method

.method public hasSecondTeamShortName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->hasSecondTeamShortName:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->setFirstTeam(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->setSecondTeam(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/majel/proto/EcoutezStructuredResponse$MatchList;

    invoke-direct {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$MatchList;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->setMatchList(Lcom/google/majel/proto/EcoutezStructuredResponse$MatchList;)Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->setDEPRECATEDFirstTeamLogoUrl(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->setDEPRECATEDSecondTeamLogoUrl(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;

    goto :goto_0

    :sswitch_6
    new-instance v1, Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;

    invoke-direct {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->setFirstTeamLogo(Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;)Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;

    goto :goto_0

    :sswitch_7
    new-instance v1, Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;

    invoke-direct {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->setSecondTeamLogo(Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;)Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->setFirstTeamShortName(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->setSecondTeamShortName(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;

    move-result-object v0

    return-object v0
.end method

.method public setDEPRECATEDFirstTeamLogoUrl(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->hasDEPRECATEDFirstTeamLogoUrl:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->dEPRECATEDFirstTeamLogoUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setDEPRECATEDSecondTeamLogoUrl(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->hasDEPRECATEDSecondTeamLogoUrl:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->dEPRECATEDSecondTeamLogoUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setFirstTeam(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->hasFirstTeam:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->firstTeam_:Ljava/lang/String;

    return-object p0
.end method

.method public setFirstTeamLogo(Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;)Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->hasFirstTeamLogo:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->firstTeamLogo_:Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;

    return-object p0
.end method

.method public setFirstTeamShortName(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->hasFirstTeamShortName:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->firstTeamShortName_:Ljava/lang/String;

    return-object p0
.end method

.method public setMatchList(Lcom/google/majel/proto/EcoutezStructuredResponse$MatchList;)Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$MatchList;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->hasMatchList:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->matchList_:Lcom/google/majel/proto/EcoutezStructuredResponse$MatchList;

    return-object p0
.end method

.method public setSecondTeam(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->hasSecondTeam:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->secondTeam_:Ljava/lang/String;

    return-object p0
.end method

.method public setSecondTeamLogo(Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;)Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->hasSecondTeamLogo:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->secondTeamLogo_:Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;

    return-object p0
.end method

.method public setSecondTeamShortName(Ljava/lang/String;)Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->hasSecondTeamShortName:Z

    iput-object p1, p0, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->secondTeamShortName_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->hasFirstTeam()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->getFirstTeam()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->hasSecondTeam()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->getSecondTeam()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->hasMatchList()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->getMatchList()Lcom/google/majel/proto/EcoutezStructuredResponse$MatchList;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->hasDEPRECATEDFirstTeamLogoUrl()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->getDEPRECATEDFirstTeamLogoUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->hasDEPRECATEDSecondTeamLogoUrl()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->getDEPRECATEDSecondTeamLogoUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->hasFirstTeamLogo()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->getFirstTeamLogo()Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->hasSecondTeamLogo()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->getSecondTeamLogo()Lcom/google/majel/proto/EcoutezStructuredResponse$Logo;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->hasFirstTeamShortName()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->getFirstTeamShortName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->hasSecondTeamShortName()Z

    move-result v0

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    invoke-virtual {p0}, Lcom/google/majel/proto/EcoutezStructuredResponse$TeamVsTeamData;->getSecondTeamShortName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_8
    return-void
.end method
