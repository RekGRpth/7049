.class public final Lcom/google/majel/proto/PeanutProtos$ClientSideAction;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "PeanutProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/majel/proto/PeanutProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ClientSideAction"
.end annotation


# instance fields
.field private cachedSize:I

.field private email_:Lcom/google/majel/proto/ActionProtos$Email;

.field private hasEmail:Z

.field private hasIdentifyAudio:Z

.field private hasNavigate:Z

.field private hasPhone:Z

.field private hasSms:Z

.field private hasTvControl:Z

.field private identifyAudio_:Lcom/google/majel/proto/ActionProtos$IdentifyAudio;

.field private navigate_:Lcom/google/majel/proto/ActionProtos$Navigate;

.field private phone_:Lcom/google/majel/proto/ActionProtos$Phone;

.field private sms_:Lcom/google/majel/proto/ActionProtos$Sms;

.field private tvControl_:Lcom/google/majel/proto/ActionProtos$TvControl;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-object v0, p0, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->phone_:Lcom/google/majel/proto/ActionProtos$Phone;

    iput-object v0, p0, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->sms_:Lcom/google/majel/proto/ActionProtos$Sms;

    iput-object v0, p0, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->navigate_:Lcom/google/majel/proto/ActionProtos$Navigate;

    iput-object v0, p0, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->identifyAudio_:Lcom/google/majel/proto/ActionProtos$IdentifyAudio;

    iput-object v0, p0, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->email_:Lcom/google/majel/proto/ActionProtos$Email;

    iput-object v0, p0, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->tvControl_:Lcom/google/majel/proto/ActionProtos$TvControl;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->cachedSize:I

    return v0
.end method

.method public getEmail()Lcom/google/majel/proto/ActionProtos$Email;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->email_:Lcom/google/majel/proto/ActionProtos$Email;

    return-object v0
.end method

.method public getIdentifyAudio()Lcom/google/majel/proto/ActionProtos$IdentifyAudio;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->identifyAudio_:Lcom/google/majel/proto/ActionProtos$IdentifyAudio;

    return-object v0
.end method

.method public getNavigate()Lcom/google/majel/proto/ActionProtos$Navigate;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->navigate_:Lcom/google/majel/proto/ActionProtos$Navigate;

    return-object v0
.end method

.method public getPhone()Lcom/google/majel/proto/ActionProtos$Phone;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->phone_:Lcom/google/majel/proto/ActionProtos$Phone;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->hasPhone()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->getPhone()Lcom/google/majel/proto/ActionProtos$Phone;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->hasSms()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->getSms()Lcom/google/majel/proto/ActionProtos$Sms;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->hasNavigate()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->getNavigate()Lcom/google/majel/proto/ActionProtos$Navigate;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->hasIdentifyAudio()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->getIdentifyAudio()Lcom/google/majel/proto/ActionProtos$IdentifyAudio;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->hasEmail()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->getEmail()Lcom/google/majel/proto/ActionProtos$Email;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->hasTvControl()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->getTvControl()Lcom/google/majel/proto/ActionProtos$TvControl;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iput v0, p0, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->cachedSize:I

    return v0
.end method

.method public getSms()Lcom/google/majel/proto/ActionProtos$Sms;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->sms_:Lcom/google/majel/proto/ActionProtos$Sms;

    return-object v0
.end method

.method public getTvControl()Lcom/google/majel/proto/ActionProtos$TvControl;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->tvControl_:Lcom/google/majel/proto/ActionProtos$TvControl;

    return-object v0
.end method

.method public hasEmail()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->hasEmail:Z

    return v0
.end method

.method public hasIdentifyAudio()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->hasIdentifyAudio:Z

    return v0
.end method

.method public hasNavigate()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->hasNavigate:Z

    return v0
.end method

.method public hasPhone()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->hasPhone:Z

    return v0
.end method

.method public hasSms()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->hasSms:Z

    return v0
.end method

.method public hasTvControl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->hasTvControl:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/PeanutProtos$ClientSideAction;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/majel/proto/ActionProtos$Phone;

    invoke-direct {v1}, Lcom/google/majel/proto/ActionProtos$Phone;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->setPhone(Lcom/google/majel/proto/ActionProtos$Phone;)Lcom/google/majel/proto/PeanutProtos$ClientSideAction;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/majel/proto/ActionProtos$Sms;

    invoke-direct {v1}, Lcom/google/majel/proto/ActionProtos$Sms;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->setSms(Lcom/google/majel/proto/ActionProtos$Sms;)Lcom/google/majel/proto/PeanutProtos$ClientSideAction;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/majel/proto/ActionProtos$Navigate;

    invoke-direct {v1}, Lcom/google/majel/proto/ActionProtos$Navigate;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->setNavigate(Lcom/google/majel/proto/ActionProtos$Navigate;)Lcom/google/majel/proto/PeanutProtos$ClientSideAction;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/majel/proto/ActionProtos$IdentifyAudio;

    invoke-direct {v1}, Lcom/google/majel/proto/ActionProtos$IdentifyAudio;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->setIdentifyAudio(Lcom/google/majel/proto/ActionProtos$IdentifyAudio;)Lcom/google/majel/proto/PeanutProtos$ClientSideAction;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/majel/proto/ActionProtos$Email;

    invoke-direct {v1}, Lcom/google/majel/proto/ActionProtos$Email;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->setEmail(Lcom/google/majel/proto/ActionProtos$Email;)Lcom/google/majel/proto/PeanutProtos$ClientSideAction;

    goto :goto_0

    :sswitch_6
    new-instance v1, Lcom/google/majel/proto/ActionProtos$TvControl;

    invoke-direct {v1}, Lcom/google/majel/proto/ActionProtos$TvControl;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->setTvControl(Lcom/google/majel/proto/ActionProtos$TvControl;)Lcom/google/majel/proto/PeanutProtos$ClientSideAction;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/PeanutProtos$ClientSideAction;

    move-result-object v0

    return-object v0
.end method

.method public setEmail(Lcom/google/majel/proto/ActionProtos$Email;)Lcom/google/majel/proto/PeanutProtos$ClientSideAction;
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionProtos$Email;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->hasEmail:Z

    iput-object p1, p0, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->email_:Lcom/google/majel/proto/ActionProtos$Email;

    return-object p0
.end method

.method public setIdentifyAudio(Lcom/google/majel/proto/ActionProtos$IdentifyAudio;)Lcom/google/majel/proto/PeanutProtos$ClientSideAction;
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionProtos$IdentifyAudio;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->hasIdentifyAudio:Z

    iput-object p1, p0, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->identifyAudio_:Lcom/google/majel/proto/ActionProtos$IdentifyAudio;

    return-object p0
.end method

.method public setNavigate(Lcom/google/majel/proto/ActionProtos$Navigate;)Lcom/google/majel/proto/PeanutProtos$ClientSideAction;
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionProtos$Navigate;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->hasNavigate:Z

    iput-object p1, p0, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->navigate_:Lcom/google/majel/proto/ActionProtos$Navigate;

    return-object p0
.end method

.method public setPhone(Lcom/google/majel/proto/ActionProtos$Phone;)Lcom/google/majel/proto/PeanutProtos$ClientSideAction;
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionProtos$Phone;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->hasPhone:Z

    iput-object p1, p0, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->phone_:Lcom/google/majel/proto/ActionProtos$Phone;

    return-object p0
.end method

.method public setSms(Lcom/google/majel/proto/ActionProtos$Sms;)Lcom/google/majel/proto/PeanutProtos$ClientSideAction;
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionProtos$Sms;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->hasSms:Z

    iput-object p1, p0, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->sms_:Lcom/google/majel/proto/ActionProtos$Sms;

    return-object p0
.end method

.method public setTvControl(Lcom/google/majel/proto/ActionProtos$TvControl;)Lcom/google/majel/proto/PeanutProtos$ClientSideAction;
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionProtos$TvControl;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->hasTvControl:Z

    iput-object p1, p0, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->tvControl_:Lcom/google/majel/proto/ActionProtos$TvControl;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->hasPhone()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->getPhone()Lcom/google/majel/proto/ActionProtos$Phone;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->hasSms()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->getSms()Lcom/google/majel/proto/ActionProtos$Sms;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->hasNavigate()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->getNavigate()Lcom/google/majel/proto/ActionProtos$Navigate;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->hasIdentifyAudio()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->getIdentifyAudio()Lcom/google/majel/proto/ActionProtos$IdentifyAudio;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->hasEmail()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->getEmail()Lcom/google/majel/proto/ActionProtos$Email;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->hasTvControl()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$ClientSideAction;->getTvControl()Lcom/google/majel/proto/ActionProtos$TvControl;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_5
    return-void
.end method
