.class public final Lcom/google/majel/proto/PeanutProtos$Image;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "PeanutProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/majel/proto/PeanutProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Image"
.end annotation


# instance fields
.field private attribution_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/AttributionProtos$Attribution;",
            ">;"
        }
    .end annotation
.end field

.field private cachedSize:I

.field private data_:Lcom/google/protobuf/micro/ByteStringMicro;

.field private hasData:Z

.field private hasHeight:Z

.field private hasThumbData:Z

.field private hasThumbHeight:Z

.field private hasThumbUrl:Z

.field private hasThumbWidth:Z

.field private hasUrl:Z

.field private hasWidth:Z

.field private height_:I

.field private thumbData_:Lcom/google/protobuf/micro/ByteStringMicro;

.field private thumbHeight_:I

.field private thumbUrl_:Ljava/lang/String;

.field private thumbWidth_:I

.field private url_:Ljava/lang/String;

.field private width_:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Image;->url_:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/micro/ByteStringMicro;->EMPTY:Lcom/google/protobuf/micro/ByteStringMicro;

    iput-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Image;->data_:Lcom/google/protobuf/micro/ByteStringMicro;

    iput v1, p0, Lcom/google/majel/proto/PeanutProtos$Image;->width_:I

    iput v1, p0, Lcom/google/majel/proto/PeanutProtos$Image;->height_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Image;->thumbUrl_:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/micro/ByteStringMicro;->EMPTY:Lcom/google/protobuf/micro/ByteStringMicro;

    iput-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Image;->thumbData_:Lcom/google/protobuf/micro/ByteStringMicro;

    iput v1, p0, Lcom/google/majel/proto/PeanutProtos$Image;->thumbWidth_:I

    iput v1, p0, Lcom/google/majel/proto/PeanutProtos$Image;->thumbHeight_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Image;->attribution_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/majel/proto/PeanutProtos$Image;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addAttribution(Lcom/google/majel/proto/AttributionProtos$Attribution;)Lcom/google/majel/proto/PeanutProtos$Image;
    .locals 1
    .param p1    # Lcom/google/majel/proto/AttributionProtos$Attribution;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Image;->attribution_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Image;->attribution_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Image;->attribution_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getAttribution(I)Lcom/google/majel/proto/AttributionProtos$Attribution;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Image;->attribution_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/AttributionProtos$Attribution;

    return-object v0
.end method

.method public getAttributionCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Image;->attribution_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getAttributionList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/AttributionProtos$Attribution;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Image;->attribution_:Ljava/util/List;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/PeanutProtos$Image;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Image;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/majel/proto/PeanutProtos$Image;->cachedSize:I

    return v0
.end method

.method public getData()Lcom/google/protobuf/micro/ByteStringMicro;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Image;->data_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object v0
.end method

.method public getHeight()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/PeanutProtos$Image;->height_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Image;->hasUrl()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Image;->getUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Image;->hasThumbUrl()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Image;->getThumbUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Image;->getAttributionList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/AttributionProtos$Attribution;

    const/4 v3, 0x3

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Image;->hasWidth()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Image;->getWidth()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Image;->hasHeight()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Image;->getHeight()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Image;->hasThumbWidth()Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x6

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Image;->getThumbWidth()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_5
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Image;->hasThumbHeight()Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v3, 0x7

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Image;->getThumbHeight()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_6
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Image;->hasData()Z

    move-result v3

    if-eqz v3, :cond_7

    const/16 v3, 0x8

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Image;->getData()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBytesSize(ILcom/google/protobuf/micro/ByteStringMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_7
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Image;->hasThumbData()Z

    move-result v3

    if-eqz v3, :cond_8

    const/16 v3, 0x9

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Image;->getThumbData()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBytesSize(ILcom/google/protobuf/micro/ByteStringMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_8
    iput v2, p0, Lcom/google/majel/proto/PeanutProtos$Image;->cachedSize:I

    return v2
.end method

.method public getThumbData()Lcom/google/protobuf/micro/ByteStringMicro;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Image;->thumbData_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object v0
.end method

.method public getThumbHeight()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/PeanutProtos$Image;->thumbHeight_:I

    return v0
.end method

.method public getThumbUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Image;->thumbUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getThumbWidth()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/PeanutProtos$Image;->thumbWidth_:I

    return v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/majel/proto/PeanutProtos$Image;->url_:Ljava/lang/String;

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    iget v0, p0, Lcom/google/majel/proto/PeanutProtos$Image;->width_:I

    return v0
.end method

.method public hasData()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Image;->hasData:Z

    return v0
.end method

.method public hasHeight()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Image;->hasHeight:Z

    return v0
.end method

.method public hasThumbData()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Image;->hasThumbData:Z

    return v0
.end method

.method public hasThumbHeight()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Image;->hasThumbHeight:Z

    return v0
.end method

.method public hasThumbUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Image;->hasThumbUrl:Z

    return v0
.end method

.method public hasThumbWidth()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Image;->hasThumbWidth:Z

    return v0
.end method

.method public hasUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Image;->hasUrl:Z

    return v0
.end method

.method public hasWidth()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Image;->hasWidth:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/PeanutProtos$Image;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/majel/proto/PeanutProtos$Image;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/PeanutProtos$Image;->setUrl(Ljava/lang/String;)Lcom/google/majel/proto/PeanutProtos$Image;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/PeanutProtos$Image;->setThumbUrl(Ljava/lang/String;)Lcom/google/majel/proto/PeanutProtos$Image;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/majel/proto/AttributionProtos$Attribution;

    invoke-direct {v1}, Lcom/google/majel/proto/AttributionProtos$Attribution;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/majel/proto/PeanutProtos$Image;->addAttribution(Lcom/google/majel/proto/AttributionProtos$Attribution;)Lcom/google/majel/proto/PeanutProtos$Image;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/PeanutProtos$Image;->setWidth(I)Lcom/google/majel/proto/PeanutProtos$Image;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/PeanutProtos$Image;->setHeight(I)Lcom/google/majel/proto/PeanutProtos$Image;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/PeanutProtos$Image;->setThumbWidth(I)Lcom/google/majel/proto/PeanutProtos$Image;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/PeanutProtos$Image;->setThumbHeight(I)Lcom/google/majel/proto/PeanutProtos$Image;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBytes()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/PeanutProtos$Image;->setData(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/majel/proto/PeanutProtos$Image;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBytes()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/majel/proto/PeanutProtos$Image;->setThumbData(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/majel/proto/PeanutProtos$Image;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/majel/proto/PeanutProtos$Image;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/majel/proto/PeanutProtos$Image;

    move-result-object v0

    return-object v0
.end method

.method public setData(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/majel/proto/PeanutProtos$Image;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/ByteStringMicro;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Image;->hasData:Z

    iput-object p1, p0, Lcom/google/majel/proto/PeanutProtos$Image;->data_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object p0
.end method

.method public setHeight(I)Lcom/google/majel/proto/PeanutProtos$Image;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Image;->hasHeight:Z

    iput p1, p0, Lcom/google/majel/proto/PeanutProtos$Image;->height_:I

    return-object p0
.end method

.method public setThumbData(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/majel/proto/PeanutProtos$Image;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/ByteStringMicro;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Image;->hasThumbData:Z

    iput-object p1, p0, Lcom/google/majel/proto/PeanutProtos$Image;->thumbData_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object p0
.end method

.method public setThumbHeight(I)Lcom/google/majel/proto/PeanutProtos$Image;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Image;->hasThumbHeight:Z

    iput p1, p0, Lcom/google/majel/proto/PeanutProtos$Image;->thumbHeight_:I

    return-object p0
.end method

.method public setThumbUrl(Ljava/lang/String;)Lcom/google/majel/proto/PeanutProtos$Image;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Image;->hasThumbUrl:Z

    iput-object p1, p0, Lcom/google/majel/proto/PeanutProtos$Image;->thumbUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setThumbWidth(I)Lcom/google/majel/proto/PeanutProtos$Image;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Image;->hasThumbWidth:Z

    iput p1, p0, Lcom/google/majel/proto/PeanutProtos$Image;->thumbWidth_:I

    return-object p0
.end method

.method public setUrl(Ljava/lang/String;)Lcom/google/majel/proto/PeanutProtos$Image;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Image;->hasUrl:Z

    iput-object p1, p0, Lcom/google/majel/proto/PeanutProtos$Image;->url_:Ljava/lang/String;

    return-object p0
.end method

.method public setWidth(I)Lcom/google/majel/proto/PeanutProtos$Image;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/majel/proto/PeanutProtos$Image;->hasWidth:Z

    iput p1, p0, Lcom/google/majel/proto/PeanutProtos$Image;->width_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Image;->hasUrl()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Image;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Image;->hasThumbUrl()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Image;->getThumbUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Image;->getAttributionList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/AttributionProtos$Attribution;

    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Image;->hasWidth()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Image;->getWidth()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Image;->hasHeight()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Image;->getHeight()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Image;->hasThumbWidth()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Image;->getThumbWidth()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Image;->hasThumbHeight()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Image;->getThumbHeight()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Image;->hasData()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Image;->getData()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBytes(ILcom/google/protobuf/micro/ByteStringMicro;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Image;->hasThumbData()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/majel/proto/PeanutProtos$Image;->getThumbData()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBytes(ILcom/google/protobuf/micro/ByteStringMicro;)V

    :cond_8
    return-void
.end method
