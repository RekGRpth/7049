.class public final Lcom/google/speech/common/Alternates$AlternateParams;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Alternates.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/common/Alternates;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AlternateParams"
.end annotation


# instance fields
.field private cachedSize:I

.field private hasMaxSpanLength:Z

.field private hasMaxTotalSpanLength:Z

.field private hasUnit:Z

.field private maxSpanLength_:I

.field private maxTotalSpanLength_:I

.field private unit_:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput v0, p0, Lcom/google/speech/common/Alternates$AlternateParams;->maxSpanLength_:I

    iput v0, p0, Lcom/google/speech/common/Alternates$AlternateParams;->maxTotalSpanLength_:I

    iput v0, p0, Lcom/google/speech/common/Alternates$AlternateParams;->unit_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/common/Alternates$AlternateParams;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/speech/common/Alternates$AlternateParams;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/common/Alternates$AlternateParams;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/speech/common/Alternates$AlternateParams;->cachedSize:I

    return v0
.end method

.method public getMaxSpanLength()I
    .locals 1

    iget v0, p0, Lcom/google/speech/common/Alternates$AlternateParams;->maxSpanLength_:I

    return v0
.end method

.method public getMaxTotalSpanLength()I
    .locals 1

    iget v0, p0, Lcom/google/speech/common/Alternates$AlternateParams;->maxTotalSpanLength_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/speech/common/Alternates$AlternateParams;->hasMaxSpanLength()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/speech/common/Alternates$AlternateParams;->getMaxSpanLength()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/common/Alternates$AlternateParams;->hasMaxTotalSpanLength()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/speech/common/Alternates$AlternateParams;->getMaxTotalSpanLength()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/common/Alternates$AlternateParams;->hasUnit()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/speech/common/Alternates$AlternateParams;->getUnit()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Lcom/google/speech/common/Alternates$AlternateParams;->cachedSize:I

    return v0
.end method

.method public getUnit()I
    .locals 1

    iget v0, p0, Lcom/google/speech/common/Alternates$AlternateParams;->unit_:I

    return v0
.end method

.method public hasMaxSpanLength()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/common/Alternates$AlternateParams;->hasMaxSpanLength:Z

    return v0
.end method

.method public hasMaxTotalSpanLength()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/common/Alternates$AlternateParams;->hasMaxTotalSpanLength:Z

    return v0
.end method

.method public hasUnit()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/common/Alternates$AlternateParams;->hasUnit:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/speech/common/Alternates$AlternateParams;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/common/Alternates$AlternateParams;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/common/Alternates$AlternateParams;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/speech/common/Alternates$AlternateParams;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/common/Alternates$AlternateParams;->setMaxSpanLength(I)Lcom/google/speech/common/Alternates$AlternateParams;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/common/Alternates$AlternateParams;->setMaxTotalSpanLength(I)Lcom/google/speech/common/Alternates$AlternateParams;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/common/Alternates$AlternateParams;->setUnit(I)Lcom/google/speech/common/Alternates$AlternateParams;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public setMaxSpanLength(I)Lcom/google/speech/common/Alternates$AlternateParams;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/common/Alternates$AlternateParams;->hasMaxSpanLength:Z

    iput p1, p0, Lcom/google/speech/common/Alternates$AlternateParams;->maxSpanLength_:I

    return-object p0
.end method

.method public setMaxTotalSpanLength(I)Lcom/google/speech/common/Alternates$AlternateParams;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/common/Alternates$AlternateParams;->hasMaxTotalSpanLength:Z

    iput p1, p0, Lcom/google/speech/common/Alternates$AlternateParams;->maxTotalSpanLength_:I

    return-object p0
.end method

.method public setUnit(I)Lcom/google/speech/common/Alternates$AlternateParams;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/common/Alternates$AlternateParams;->hasUnit:Z

    iput p1, p0, Lcom/google/speech/common/Alternates$AlternateParams;->unit_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/speech/common/Alternates$AlternateParams;->hasMaxSpanLength()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/common/Alternates$AlternateParams;->getMaxSpanLength()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/common/Alternates$AlternateParams;->hasMaxTotalSpanLength()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/common/Alternates$AlternateParams;->getMaxTotalSpanLength()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/common/Alternates$AlternateParams;->hasUnit()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/speech/common/Alternates$AlternateParams;->getUnit()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_2
    return-void
.end method
