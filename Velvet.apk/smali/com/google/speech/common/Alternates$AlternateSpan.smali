.class public final Lcom/google/speech/common/Alternates$AlternateSpan;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Alternates.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/common/Alternates;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AlternateSpan"
.end annotation


# instance fields
.field private alternates_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/common/Alternates$Alternate;",
            ">;"
        }
    .end annotation
.end field

.field private cachedSize:I

.field private confidence_:F

.field private hasConfidence:Z

.field private hasLength:Z

.field private hasStart:Z

.field private length_:I

.field private start_:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput v0, p0, Lcom/google/speech/common/Alternates$AlternateSpan;->start_:I

    iput v0, p0, Lcom/google/speech/common/Alternates$AlternateSpan;->length_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/common/Alternates$AlternateSpan;->alternates_:Ljava/util/List;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/speech/common/Alternates$AlternateSpan;->confidence_:F

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/common/Alternates$AlternateSpan;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addAlternates(Lcom/google/speech/common/Alternates$Alternate;)Lcom/google/speech/common/Alternates$AlternateSpan;
    .locals 1
    .param p1    # Lcom/google/speech/common/Alternates$Alternate;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/speech/common/Alternates$AlternateSpan;->alternates_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/speech/common/Alternates$AlternateSpan;->alternates_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/speech/common/Alternates$AlternateSpan;->alternates_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public clearAlternates()Lcom/google/speech/common/Alternates$AlternateSpan;
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/common/Alternates$AlternateSpan;->alternates_:Ljava/util/List;

    return-object p0
.end method

.method public getAlternates(I)Lcom/google/speech/common/Alternates$Alternate;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/speech/common/Alternates$AlternateSpan;->alternates_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/common/Alternates$Alternate;

    return-object v0
.end method

.method public getAlternatesCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/speech/common/Alternates$AlternateSpan;->alternates_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getAlternatesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/common/Alternates$Alternate;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/speech/common/Alternates$AlternateSpan;->alternates_:Ljava/util/List;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/speech/common/Alternates$AlternateSpan;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/common/Alternates$AlternateSpan;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/speech/common/Alternates$AlternateSpan;->cachedSize:I

    return v0
.end method

.method public getConfidence()F
    .locals 1

    iget v0, p0, Lcom/google/speech/common/Alternates$AlternateSpan;->confidence_:F

    return v0
.end method

.method public getLength()I
    .locals 1

    iget v0, p0, Lcom/google/speech/common/Alternates$AlternateSpan;->length_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/speech/common/Alternates$AlternateSpan;->hasStart()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/speech/common/Alternates$AlternateSpan;->getStart()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/common/Alternates$AlternateSpan;->hasLength()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/speech/common/Alternates$AlternateSpan;->getLength()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/common/Alternates$AlternateSpan;->getAlternatesList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/common/Alternates$Alternate;

    const/4 v3, 0x3

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/common/Alternates$AlternateSpan;->hasConfidence()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/google/speech/common/Alternates$AlternateSpan;->getConfidence()F

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v3

    add-int/2addr v2, v3

    :cond_3
    iput v2, p0, Lcom/google/speech/common/Alternates$AlternateSpan;->cachedSize:I

    return v2
.end method

.method public getStart()I
    .locals 1

    iget v0, p0, Lcom/google/speech/common/Alternates$AlternateSpan;->start_:I

    return v0
.end method

.method public hasConfidence()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/common/Alternates$AlternateSpan;->hasConfidence:Z

    return v0
.end method

.method public hasLength()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/common/Alternates$AlternateSpan;->hasLength:Z

    return v0
.end method

.method public hasStart()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/common/Alternates$AlternateSpan;->hasStart:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/speech/common/Alternates$AlternateSpan;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/common/Alternates$AlternateSpan;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/common/Alternates$AlternateSpan;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/speech/common/Alternates$AlternateSpan;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/common/Alternates$AlternateSpan;->setStart(I)Lcom/google/speech/common/Alternates$AlternateSpan;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/common/Alternates$AlternateSpan;->setLength(I)Lcom/google/speech/common/Alternates$AlternateSpan;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/speech/common/Alternates$Alternate;

    invoke-direct {v1}, Lcom/google/speech/common/Alternates$Alternate;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/common/Alternates$AlternateSpan;->addAlternates(Lcom/google/speech/common/Alternates$Alternate;)Lcom/google/speech/common/Alternates$AlternateSpan;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/common/Alternates$AlternateSpan;->setConfidence(F)Lcom/google/speech/common/Alternates$AlternateSpan;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x25 -> :sswitch_4
    .end sparse-switch
.end method

.method public setConfidence(F)Lcom/google/speech/common/Alternates$AlternateSpan;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/common/Alternates$AlternateSpan;->hasConfidence:Z

    iput p1, p0, Lcom/google/speech/common/Alternates$AlternateSpan;->confidence_:F

    return-object p0
.end method

.method public setLength(I)Lcom/google/speech/common/Alternates$AlternateSpan;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/common/Alternates$AlternateSpan;->hasLength:Z

    iput p1, p0, Lcom/google/speech/common/Alternates$AlternateSpan;->length_:I

    return-object p0
.end method

.method public setStart(I)Lcom/google/speech/common/Alternates$AlternateSpan;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/common/Alternates$AlternateSpan;->hasStart:Z

    iput p1, p0, Lcom/google/speech/common/Alternates$AlternateSpan;->start_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/speech/common/Alternates$AlternateSpan;->hasStart()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/common/Alternates$AlternateSpan;->getStart()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/common/Alternates$AlternateSpan;->hasLength()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/common/Alternates$AlternateSpan;->getLength()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/common/Alternates$AlternateSpan;->getAlternatesList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/common/Alternates$Alternate;

    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/common/Alternates$AlternateSpan;->hasConfidence()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/speech/common/Alternates$AlternateSpan;->getConfidence()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_3
    return-void
.end method
