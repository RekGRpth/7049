.class public final Lcom/google/speech/common/Alternates$RecognitionClientAlternates;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Alternates.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/common/Alternates;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RecognitionClientAlternates"
.end annotation


# instance fields
.field private cachedSize:I

.field private hasMaxSpanLength:Z

.field private hasUnit:Z

.field private maxSpanLength_:I

.field private span_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/common/Alternates$AlternateSpan;",
            ">;"
        }
    .end annotation
.end field

.field private unit_:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/common/Alternates$RecognitionClientAlternates;->span_:Ljava/util/List;

    iput v1, p0, Lcom/google/speech/common/Alternates$RecognitionClientAlternates;->maxSpanLength_:I

    iput v1, p0, Lcom/google/speech/common/Alternates$RecognitionClientAlternates;->unit_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/common/Alternates$RecognitionClientAlternates;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addSpan(Lcom/google/speech/common/Alternates$AlternateSpan;)Lcom/google/speech/common/Alternates$RecognitionClientAlternates;
    .locals 1
    .param p1    # Lcom/google/speech/common/Alternates$AlternateSpan;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/speech/common/Alternates$RecognitionClientAlternates;->span_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/speech/common/Alternates$RecognitionClientAlternates;->span_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/speech/common/Alternates$RecognitionClientAlternates;->span_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/speech/common/Alternates$RecognitionClientAlternates;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/common/Alternates$RecognitionClientAlternates;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/speech/common/Alternates$RecognitionClientAlternates;->cachedSize:I

    return v0
.end method

.method public getMaxSpanLength()I
    .locals 1

    iget v0, p0, Lcom/google/speech/common/Alternates$RecognitionClientAlternates;->maxSpanLength_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/speech/common/Alternates$RecognitionClientAlternates;->getSpanList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/common/Alternates$AlternateSpan;

    const/4 v3, 0x1

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/common/Alternates$RecognitionClientAlternates;->hasMaxSpanLength()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/speech/common/Alternates$RecognitionClientAlternates;->getMaxSpanLength()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/common/Alternates$RecognitionClientAlternates;->hasUnit()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/speech/common/Alternates$RecognitionClientAlternates;->getUnit()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    iput v2, p0, Lcom/google/speech/common/Alternates$RecognitionClientAlternates;->cachedSize:I

    return v2
.end method

.method public getSpanList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/common/Alternates$AlternateSpan;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/speech/common/Alternates$RecognitionClientAlternates;->span_:Ljava/util/List;

    return-object v0
.end method

.method public getUnit()I
    .locals 1

    iget v0, p0, Lcom/google/speech/common/Alternates$RecognitionClientAlternates;->unit_:I

    return v0
.end method

.method public hasMaxSpanLength()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/common/Alternates$RecognitionClientAlternates;->hasMaxSpanLength:Z

    return v0
.end method

.method public hasUnit()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/common/Alternates$RecognitionClientAlternates;->hasUnit:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/speech/common/Alternates$RecognitionClientAlternates;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/common/Alternates$RecognitionClientAlternates;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/common/Alternates$RecognitionClientAlternates;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/speech/common/Alternates$RecognitionClientAlternates;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/speech/common/Alternates$AlternateSpan;

    invoke-direct {v1}, Lcom/google/speech/common/Alternates$AlternateSpan;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/common/Alternates$RecognitionClientAlternates;->addSpan(Lcom/google/speech/common/Alternates$AlternateSpan;)Lcom/google/speech/common/Alternates$RecognitionClientAlternates;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/common/Alternates$RecognitionClientAlternates;->setMaxSpanLength(I)Lcom/google/speech/common/Alternates$RecognitionClientAlternates;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/common/Alternates$RecognitionClientAlternates;->setUnit(I)Lcom/google/speech/common/Alternates$RecognitionClientAlternates;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public setMaxSpanLength(I)Lcom/google/speech/common/Alternates$RecognitionClientAlternates;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/common/Alternates$RecognitionClientAlternates;->hasMaxSpanLength:Z

    iput p1, p0, Lcom/google/speech/common/Alternates$RecognitionClientAlternates;->maxSpanLength_:I

    return-object p0
.end method

.method public setUnit(I)Lcom/google/speech/common/Alternates$RecognitionClientAlternates;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/common/Alternates$RecognitionClientAlternates;->hasUnit:Z

    iput p1, p0, Lcom/google/speech/common/Alternates$RecognitionClientAlternates;->unit_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/speech/common/Alternates$RecognitionClientAlternates;->getSpanList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/common/Alternates$AlternateSpan;

    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/common/Alternates$RecognitionClientAlternates;->hasMaxSpanLength()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/common/Alternates$RecognitionClientAlternates;->getMaxSpanLength()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/common/Alternates$RecognitionClientAlternates;->hasUnit()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/common/Alternates$RecognitionClientAlternates;->getUnit()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_2
    return-void
.end method
