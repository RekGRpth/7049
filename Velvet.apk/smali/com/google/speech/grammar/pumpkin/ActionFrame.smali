.class public Lcom/google/speech/grammar/pumpkin/ActionFrame;
.super Ljava/lang/Object;
.source "ActionFrame.java"


# instance fields
.field private nativeActionFrame:J


# direct methods
.method protected constructor <init>(J)V
    .locals 2
    .param p1    # J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Can\'t initialize ActionFrame wrapper with a null ActionFrame"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-wide p1, p0, Lcom/google/speech/grammar/pumpkin/ActionFrame;->nativeActionFrame:J

    return-void
.end method

.method private static native nativeDelete(J)V
.end method


# virtual methods
.method public declared-synchronized delete()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/speech/grammar/pumpkin/ActionFrame;->nativeActionFrame:J

    invoke-static {v0, v1}, Lcom/google/speech/grammar/pumpkin/ActionFrame;->nativeDelete(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected finalize()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/ActionFrame;->delete()V

    return-void
.end method

.method public getNativeActionFrame()J
    .locals 2

    iget-wide v0, p0, Lcom/google/speech/grammar/pumpkin/ActionFrame;->nativeActionFrame:J

    return-wide v0
.end method
