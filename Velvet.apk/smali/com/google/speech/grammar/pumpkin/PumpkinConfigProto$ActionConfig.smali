.class public final Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "PumpkinConfigProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ActionConfig"
.end annotation


# instance fields
.field private actionName_:Ljava/lang/String;

.field private cachedSize:I

.field private description_:Ljava/lang/String;

.field private exportName_:Ljava/lang/String;

.field private farData_:Lcom/google/protobuf/micro/ByteStringMicro;

.field private farFilename_:Ljava/lang/String;

.field private hasActionName:Z

.field private hasDescription:Z

.field private hasExportName:Z

.field private hasFarData:Z

.field private hasFarFilename:Z

.field private hasUseEditedFst:Z

.field private useEditedFst_:Z

.field private var_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$VariableMapping;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->actionName_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->farFilename_:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/micro/ByteStringMicro;->EMPTY:Lcom/google/protobuf/micro/ByteStringMicro;

    iput-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->farData_:Lcom/google/protobuf/micro/ByteStringMicro;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->exportName_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->var_:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->description_:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->useEditedFst_:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addVar(Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$VariableMapping;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;
    .locals 1
    .param p1    # Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$VariableMapping;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->var_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->var_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->var_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getActionName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->actionName_:Ljava/lang/String;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->cachedSize:I

    return v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->description_:Ljava/lang/String;

    return-object v0
.end method

.method public getExportName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->exportName_:Ljava/lang/String;

    return-object v0
.end method

.method public getFarData()Lcom/google/protobuf/micro/ByteStringMicro;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->farData_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object v0
.end method

.method public getFarFilename()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->farFilename_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->hasActionName()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->getActionName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->hasFarFilename()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->getFarFilename()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->hasFarData()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->getFarData()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBytesSize(ILcom/google/protobuf/micro/ByteStringMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->hasExportName()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->getExportName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->getVarList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$VariableMapping;

    const/4 v3, 0x5

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->hasDescription()Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x6

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->getDescription()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->hasUseEditedFst()Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v3, 0x7

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->getUseEditedFst()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_6
    iput v2, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->cachedSize:I

    return v2
.end method

.method public getUseEditedFst()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->useEditedFst_:Z

    return v0
.end method

.method public getVarList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$VariableMapping;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->var_:Ljava/util/List;

    return-object v0
.end method

.method public hasActionName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->hasActionName:Z

    return v0
.end method

.method public hasDescription()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->hasDescription:Z

    return v0
.end method

.method public hasExportName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->hasExportName:Z

    return v0
.end method

.method public hasFarData()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->hasFarData:Z

    return v0
.end method

.method public hasFarFilename()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->hasFarFilename:Z

    return v0
.end method

.method public hasUseEditedFst()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->hasUseEditedFst:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->setActionName(Ljava/lang/String;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->setFarFilename(Ljava/lang/String;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBytes()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->setFarData(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->setExportName(Ljava/lang/String;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$VariableMapping;

    invoke-direct {v1}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$VariableMapping;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->addVar(Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$VariableMapping;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->setDescription(Ljava/lang/String;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->setUseEditedFst(Z)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public setActionName(Ljava/lang/String;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->hasActionName:Z

    iput-object p1, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->actionName_:Ljava/lang/String;

    return-object p0
.end method

.method public setDescription(Ljava/lang/String;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->hasDescription:Z

    iput-object p1, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->description_:Ljava/lang/String;

    return-object p0
.end method

.method public setExportName(Ljava/lang/String;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->hasExportName:Z

    iput-object p1, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->exportName_:Ljava/lang/String;

    return-object p0
.end method

.method public setFarData(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/ByteStringMicro;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->hasFarData:Z

    iput-object p1, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->farData_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object p0
.end method

.method public setFarFilename(Ljava/lang/String;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->hasFarFilename:Z

    iput-object p1, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->farFilename_:Ljava/lang/String;

    return-object p0
.end method

.method public setUseEditedFst(Z)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->hasUseEditedFst:Z

    iput-boolean p1, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->useEditedFst_:Z

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->hasActionName()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->getActionName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->hasFarFilename()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->getFarFilename()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->hasFarData()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->getFarData()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBytes(ILcom/google/protobuf/micro/ByteStringMicro;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->hasExportName()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->getExportName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->getVarList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$VariableMapping;

    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->hasDescription()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->getDescription()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->hasUseEditedFst()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->getUseEditedFst()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_6
    return-void
.end method
