.class public final Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "PumpkinConfigProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PumpkinResource"
.end annotation


# instance fields
.field private cachedSize:I

.field private data_:Lcom/google/protobuf/micro/ByteStringMicro;

.field private filename_:Ljava/lang/String;

.field private hasData:Z

.field private hasFilename:Z

.field private hasName:Z

.field private name_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;->name_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;->filename_:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/micro/ByteStringMicro;->EMPTY:Lcom/google/protobuf/micro/ByteStringMicro;

    iput-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;->data_:Lcom/google/protobuf/micro/ByteStringMicro;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;->cachedSize:I

    return v0
.end method

.method public getData()Lcom/google/protobuf/micro/ByteStringMicro;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;->data_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object v0
.end method

.method public getFilename()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;->filename_:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;->hasName()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;->hasFilename()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;->hasData()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;->getData()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBytesSize(ILcom/google/protobuf/micro/ByteStringMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;->cachedSize:I

    return v0
.end method

.method public hasData()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;->hasData:Z

    return v0
.end method

.method public hasFilename()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;->hasFilename:Z

    return v0
.end method

.method public hasName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;->hasName:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;->setName(Ljava/lang/String;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;->setFilename(Ljava/lang/String;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBytes()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;->setData(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public setData(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/ByteStringMicro;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;->hasData:Z

    iput-object p1, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;->data_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object p0
.end method

.method public setFilename(Ljava/lang/String;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;->hasFilename:Z

    iput-object p1, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;->filename_:Ljava/lang/String;

    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;->hasName:Z

    iput-object p1, p0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;->name_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;->hasName()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;->hasFilename()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;->getFilename()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;->hasData()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;->getData()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBytes(ILcom/google/protobuf/micro/ByteStringMicro;)V

    :cond_2
    return-void
.end method
