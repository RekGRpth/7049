.class public final Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$PumpkinTaggerResults;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "PumpkinTaggerResultsProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PumpkinTaggerResults"
.end annotation


# instance fields
.field private cachedSize:I

.field private hypothesis_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$HypothesisResult;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$PumpkinTaggerResults;->hypothesis_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$PumpkinTaggerResults;->cachedSize:I

    return-void
.end method

.method public static parseFrom([B)Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$PumpkinTaggerResults;
    .locals 1
    .param p0    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;
        }
    .end annotation

    new-instance v0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$PumpkinTaggerResults;

    invoke-direct {v0}, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$PumpkinTaggerResults;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$PumpkinTaggerResults;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v0

    check-cast v0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$PumpkinTaggerResults;

    check-cast v0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$PumpkinTaggerResults;

    return-object v0
.end method


# virtual methods
.method public addHypothesis(Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$HypothesisResult;)Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$PumpkinTaggerResults;
    .locals 1
    .param p1    # Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$HypothesisResult;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$PumpkinTaggerResults;->hypothesis_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$PumpkinTaggerResults;->hypothesis_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$PumpkinTaggerResults;->hypothesis_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$PumpkinTaggerResults;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$PumpkinTaggerResults;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$PumpkinTaggerResults;->cachedSize:I

    return v0
.end method

.method public getHypothesis(I)Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$HypothesisResult;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$PumpkinTaggerResults;->hypothesis_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$HypothesisResult;

    return-object v0
.end method

.method public getHypothesisCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$PumpkinTaggerResults;->hypothesis_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getHypothesisList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$HypothesisResult;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$PumpkinTaggerResults;->hypothesis_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$PumpkinTaggerResults;->getHypothesisList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$HypothesisResult;

    const/4 v3, 0x1

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_0
    iput v2, p0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$PumpkinTaggerResults;->cachedSize:I

    return v2
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$PumpkinTaggerResults;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$PumpkinTaggerResults;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$PumpkinTaggerResults;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$PumpkinTaggerResults;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$HypothesisResult;

    invoke-direct {v1}, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$HypothesisResult;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$PumpkinTaggerResults;->addHypothesis(Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$HypothesisResult;)Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$PumpkinTaggerResults;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$PumpkinTaggerResults;->getHypothesisList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$HypothesisResult;

    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_0
    return-void
.end method
