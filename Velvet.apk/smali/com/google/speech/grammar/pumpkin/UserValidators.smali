.class public Lcom/google/speech/grammar/pumpkin/UserValidators;
.super Ljava/lang/Object;
.source "UserValidators.java"


# instance fields
.field private nativeUserValidators:J


# direct methods
.method public constructor <init>(Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;)V
    .locals 1
    .param p1    # Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;

    invoke-virtual {p1}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->toByteArray()[B

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/speech/grammar/pumpkin/UserValidators;-><init>([B)V

    return-void
.end method

.method public constructor <init>([B)V
    .locals 4
    .param p1    # [B

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/speech/grammar/pumpkin/UserValidators;->nativeCreate([B)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/speech/grammar/pumpkin/UserValidators;->nativeUserValidators:J

    iget-wide v0, p0, Lcom/google/speech/grammar/pumpkin/UserValidators;->nativeUserValidators:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Couldn\'t create UserValidator native object from the provided config"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method private declared-synchronized delete()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/speech/grammar/pumpkin/UserValidators;->nativeUserValidators:J

    invoke-static {v0, v1}, Lcom/google/speech/grammar/pumpkin/UserValidators;->nativeDelete(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static native nativeAddUserValidator(JLjava/lang/String;[Ljava/lang/String;)V
.end method

.method private static native nativeCreate([B)J
.end method

.method private static native nativeDelete(J)V
.end method

.method private static native nativeSetContacts(J[Ljava/lang/String;)V
.end method


# virtual methods
.method public addUserValidator(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # [Ljava/lang/String;

    iget-wide v0, p0, Lcom/google/speech/grammar/pumpkin/UserValidators;->nativeUserValidators:J

    invoke-static {v0, v1, p1, p2}, Lcom/google/speech/grammar/pumpkin/UserValidators;->nativeAddUserValidator(JLjava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method protected finalize()V
    .locals 0

    invoke-direct {p0}, Lcom/google/speech/grammar/pumpkin/UserValidators;->delete()V

    return-void
.end method

.method public getNativeUserValidators()J
    .locals 2

    iget-wide v0, p0, Lcom/google/speech/grammar/pumpkin/UserValidators;->nativeUserValidators:J

    return-wide v0
.end method

.method public setContacts([Ljava/lang/String;)V
    .locals 2
    .param p1    # [Ljava/lang/String;

    iget-wide v0, p0, Lcom/google/speech/grammar/pumpkin/UserValidators;->nativeUserValidators:J

    invoke-static {v0, v1, p1}, Lcom/google/speech/grammar/pumpkin/UserValidators;->nativeSetContacts(J[Ljava/lang/String;)V

    return-void
.end method
