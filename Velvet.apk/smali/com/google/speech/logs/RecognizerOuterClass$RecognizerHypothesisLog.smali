.class public final Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "RecognizerOuterClass.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/logs/RecognizerOuterClass;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RecognizerHypothesisLog"
.end annotation


# instance fields
.field private amCost_:F

.field private cachedSize:I

.field private confidence_:F

.field private hasAmCost:Z

.field private hasConfidence:Z

.field private hasHypothesis:Z

.field private hasIsRedacted:Z

.field private hasLmCost:Z

.field private hasPrenormHypothesis:Z

.field private hasRecognizerCost:Z

.field private hypothesis_:Ljava/lang/String;

.field private isRedacted_:Z

.field private lmCost_:F

.field private prenormHypothesis_:Ljava/lang/String;

.field private recognizerCost_:F


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->prenormHypothesis_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->hypothesis_:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->isRedacted_:Z

    iput v1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->confidence_:F

    iput v1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->recognizerCost_:F

    iput v1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->amCost_:F

    iput v1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->lmCost_:F

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getAmCost()F
    .locals 1

    iget v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->amCost_:F

    return v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->cachedSize:I

    return v0
.end method

.method public getConfidence()F
    .locals 1

    iget v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->confidence_:F

    return v0
.end method

.method public getHypothesis()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->hypothesis_:Ljava/lang/String;

    return-object v0
.end method

.method public getIsRedacted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->isRedacted_:Z

    return v0
.end method

.method public getLmCost()F
    .locals 1

    iget v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->lmCost_:F

    return v0
.end method

.method public getPrenormHypothesis()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->prenormHypothesis_:Ljava/lang/String;

    return-object v0
.end method

.method public getRecognizerCost()F
    .locals 1

    iget v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->recognizerCost_:F

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->hasPrenormHypothesis()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->getPrenormHypothesis()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->hasHypothesis()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->getHypothesis()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->hasConfidence()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->getConfidence()F

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->hasRecognizerCost()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->getRecognizerCost()F

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->hasAmCost()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->getAmCost()F

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->hasLmCost()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->getLmCost()F

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->hasIsRedacted()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->getIsRedacted()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iput v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->cachedSize:I

    return v0
.end method

.method public hasAmCost()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->hasAmCost:Z

    return v0
.end method

.method public hasConfidence()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->hasConfidence:Z

    return v0
.end method

.method public hasHypothesis()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->hasHypothesis:Z

    return v0
.end method

.method public hasIsRedacted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->hasIsRedacted:Z

    return v0
.end method

.method public hasLmCost()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->hasLmCost:Z

    return v0
.end method

.method public hasPrenormHypothesis()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->hasPrenormHypothesis:Z

    return v0
.end method

.method public hasRecognizerCost()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->hasRecognizerCost:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->setPrenormHypothesis(Ljava/lang/String;)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->setHypothesis(Ljava/lang/String;)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->setConfidence(F)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->setRecognizerCost(F)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->setAmCost(F)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->setLmCost(F)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->setIsRedacted(Z)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1d -> :sswitch_3
        0x25 -> :sswitch_4
        0x2d -> :sswitch_5
        0x35 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public setAmCost(F)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->hasAmCost:Z

    iput p1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->amCost_:F

    return-object p0
.end method

.method public setConfidence(F)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->hasConfidence:Z

    iput p1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->confidence_:F

    return-object p0
.end method

.method public setHypothesis(Ljava/lang/String;)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->hasHypothesis:Z

    iput-object p1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->hypothesis_:Ljava/lang/String;

    return-object p0
.end method

.method public setIsRedacted(Z)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->hasIsRedacted:Z

    iput-boolean p1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->isRedacted_:Z

    return-object p0
.end method

.method public setLmCost(F)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->hasLmCost:Z

    iput p1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->lmCost_:F

    return-object p0
.end method

.method public setPrenormHypothesis(Ljava/lang/String;)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->hasPrenormHypothesis:Z

    iput-object p1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->prenormHypothesis_:Ljava/lang/String;

    return-object p0
.end method

.method public setRecognizerCost(F)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->hasRecognizerCost:Z

    iput p1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->recognizerCost_:F

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->hasPrenormHypothesis()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->getPrenormHypothesis()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->hasHypothesis()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->getHypothesis()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->hasConfidence()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->getConfidence()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->hasRecognizerCost()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->getRecognizerCost()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->hasAmCost()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->getAmCost()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->hasLmCost()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->getLmCost()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->hasIsRedacted()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;->getIsRedacted()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_6
    return-void
.end method
