.class public final Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "RecognizerOuterClass.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/logs/RecognizerOuterClass;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RecognizerSegmentLog"
.end annotation


# instance fields
.field private cachedSize:I

.field private dEPRECATEDFinalEndpointerFiredMs_:I

.field private dEPRECATEDFinalRecognitionResultComputedMs_:J

.field private hasDEPRECATEDFinalEndpointerFiredMs:Z

.field private hasDEPRECATEDFinalRecognitionResultComputedMs:Z

.field private hasRelativeEndTimeMs:Z

.field private hasRelativeStartTimeMs:Z

.field private hypothesis_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;",
            ">;"
        }
    .end annotation
.end field

.field private relativeEndTimeMs_:I

.field private relativeStartTimeMs_:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->relativeStartTimeMs_:I

    iput v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->relativeEndTimeMs_:I

    iput v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->dEPRECATEDFinalEndpointerFiredMs_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->dEPRECATEDFinalRecognitionResultComputedMs_:J

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->hypothesis_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addHypothesis(Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;
    .locals 1
    .param p1    # Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->hypothesis_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->hypothesis_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->hypothesis_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->cachedSize:I

    return v0
.end method

.method public getDEPRECATEDFinalEndpointerFiredMs()I
    .locals 1

    iget v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->dEPRECATEDFinalEndpointerFiredMs_:I

    return v0
.end method

.method public getDEPRECATEDFinalRecognitionResultComputedMs()J
    .locals 2

    iget-wide v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->dEPRECATEDFinalRecognitionResultComputedMs_:J

    return-wide v0
.end method

.method public getHypothesisList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->hypothesis_:Ljava/util/List;

    return-object v0
.end method

.method public getRelativeEndTimeMs()I
    .locals 1

    iget v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->relativeEndTimeMs_:I

    return v0
.end method

.method public getRelativeStartTimeMs()I
    .locals 1

    iget v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->relativeStartTimeMs_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 6

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->hasRelativeStartTimeMs()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->getRelativeStartTimeMs()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->hasRelativeEndTimeMs()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->getRelativeEndTimeMs()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->hasDEPRECATEDFinalEndpointerFiredMs()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->getDEPRECATEDFinalEndpointerFiredMs()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->hasDEPRECATEDFinalRecognitionResultComputedMs()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->getDEPRECATEDFinalRecognitionResultComputedMs()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->getHypothesisList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;

    const/4 v3, 0x5

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_4
    iput v2, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->cachedSize:I

    return v2
.end method

.method public hasDEPRECATEDFinalEndpointerFiredMs()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->hasDEPRECATEDFinalEndpointerFiredMs:Z

    return v0
.end method

.method public hasDEPRECATEDFinalRecognitionResultComputedMs()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->hasDEPRECATEDFinalRecognitionResultComputedMs:Z

    return v0
.end method

.method public hasRelativeEndTimeMs()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->hasRelativeEndTimeMs:Z

    return v0
.end method

.method public hasRelativeStartTimeMs()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->hasRelativeStartTimeMs:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->setRelativeStartTimeMs(I)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->setRelativeEndTimeMs(I)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->setDEPRECATEDFinalEndpointerFiredMs(I)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->setDEPRECATEDFinalRecognitionResultComputedMs(J)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;

    invoke-direct {v1}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->addHypothesis(Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public setDEPRECATEDFinalEndpointerFiredMs(I)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->hasDEPRECATEDFinalEndpointerFiredMs:Z

    iput p1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->dEPRECATEDFinalEndpointerFiredMs_:I

    return-object p0
.end method

.method public setDEPRECATEDFinalRecognitionResultComputedMs(J)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->hasDEPRECATEDFinalRecognitionResultComputedMs:Z

    iput-wide p1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->dEPRECATEDFinalRecognitionResultComputedMs_:J

    return-object p0
.end method

.method public setRelativeEndTimeMs(I)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->hasRelativeEndTimeMs:Z

    iput p1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->relativeEndTimeMs_:I

    return-object p0
.end method

.method public setRelativeStartTimeMs(I)Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->hasRelativeStartTimeMs:Z

    iput p1, p0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->relativeStartTimeMs_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 5
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->hasRelativeStartTimeMs()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->getRelativeStartTimeMs()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->hasRelativeEndTimeMs()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->getRelativeEndTimeMs()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->hasDEPRECATEDFinalEndpointerFiredMs()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->getDEPRECATEDFinalEndpointerFiredMs()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->hasDEPRECATEDFinalRecognitionResultComputedMs()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->getDEPRECATEDFinalRecognitionResultComputedMs()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerSegmentLog;->getHypothesisList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/logs/RecognizerOuterClass$RecognizerHypothesisLog;

    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_4
    return-void
.end method
