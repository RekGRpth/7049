.class public final Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "VoicesearchClientLogProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/logs/VoicesearchClientLogProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "VoiceSearchClientLog"
.end annotation


# instance fields
.field private applicationId_:Ljava/lang/String;

.field private applicationVersionName_:Ljava/lang/String;

.field private applicationVersion_:Ljava/lang/String;

.field private bundledClientEvents_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;",
            ">;"
        }
    .end annotation
.end field

.field private cachedSize:I

.field private deviceModel_:Ljava/lang/String;

.field private experimentId_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private hasApplicationId:Z

.field private hasApplicationVersion:Z

.field private hasApplicationVersionName:Z

.field private hasDeviceModel:Z

.field private hasImeLangCount:Z

.field private hasInstallId:Z

.field private hasLocale:Z

.field private hasPackageId:Z

.field private hasPairedBluetooth:Z

.field private hasPlatformId:Z

.field private hasPlatformVersion:Z

.field private hasRequestTimeMsec:Z

.field private hasTriggerApplicationId:Z

.field private hasVoicesearchLangCount:Z

.field private imeLangCount_:I

.field private installId_:Ljava/lang/String;

.field private locale_:Ljava/lang/String;

.field private packageId_:Ljava/lang/String;

.field private pairedBluetooth_:Z

.field private platformId_:Ljava/lang/String;

.field private platformVersion_:Ljava/lang/String;

.field private requestTimeMsec_:J

.field private triggerApplicationId_:Ljava/lang/String;

.field private voicesearchLangCount_:I


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->requestTimeMsec_:J

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->installId_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->packageId_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->platformId_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->platformVersion_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->deviceModel_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->applicationId_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->triggerApplicationId_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->applicationVersion_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->applicationVersionName_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->locale_:Ljava/lang/String;

    iput v2, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->imeLangCount_:I

    iput v2, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->voicesearchLangCount_:I

    iput-boolean v2, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->pairedBluetooth_:Z

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->bundledClientEvents_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->experimentId_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->cachedSize:I

    return-void
.end method

.method public static parseFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;
    .locals 1
    .param p0    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    invoke-direct {v0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public addBundledClientEvents(Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;
    .locals 1
    .param p1    # Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->bundledClientEvents_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->bundledClientEvents_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->bundledClientEvents_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addExperimentId(Ljava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->experimentId_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->experimentId_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->experimentId_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getApplicationId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->applicationId_:Ljava/lang/String;

    return-object v0
.end method

.method public getApplicationVersion()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->applicationVersion_:Ljava/lang/String;

    return-object v0
.end method

.method public getApplicationVersionName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->applicationVersionName_:Ljava/lang/String;

    return-object v0
.end method

.method public getBundledClientEvents(I)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->bundledClientEvents_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    return-object v0
.end method

.method public getBundledClientEventsCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->bundledClientEvents_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getBundledClientEventsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->bundledClientEvents_:Ljava/util/List;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->cachedSize:I

    return v0
.end method

.method public getDeviceModel()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->deviceModel_:Ljava/lang/String;

    return-object v0
.end method

.method public getExperimentIdList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->experimentId_:Ljava/util/List;

    return-object v0
.end method

.method public getImeLangCount()I
    .locals 1

    iget v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->imeLangCount_:I

    return v0
.end method

.method public getInstallId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->installId_:Ljava/lang/String;

    return-object v0
.end method

.method public getLocale()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->locale_:Ljava/lang/String;

    return-object v0
.end method

.method public getPackageId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->packageId_:Ljava/lang/String;

    return-object v0
.end method

.method public getPairedBluetooth()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->pairedBluetooth_:Z

    return v0
.end method

.method public getPlatformId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->platformId_:Ljava/lang/String;

    return-object v0
.end method

.method public getPlatformVersion()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->platformVersion_:Ljava/lang/String;

    return-object v0
.end method

.method public getRequestTimeMsec()J
    .locals 2

    iget-wide v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->requestTimeMsec_:J

    return-wide v0
.end method

.method public getSerializedSize()I
    .locals 7

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasRequestTimeMsec()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->getRequestTimeMsec()J

    move-result-wide v5

    invoke-static {v4, v5, v6}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeUInt64Size(IJ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasInstallId()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->getInstallId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasPlatformId()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x3

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->getPlatformId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasPlatformVersion()Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x4

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->getPlatformVersion()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasDeviceModel()Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v4, 0x5

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->getDeviceModel()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasApplicationId()Z

    move-result v4

    if-eqz v4, :cond_5

    const/4 v4, 0x6

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->getApplicationId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasTriggerApplicationId()Z

    move-result v4

    if-eqz v4, :cond_6

    const/4 v4, 0x7

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->getTriggerApplicationId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasApplicationVersion()Z

    move-result v4

    if-eqz v4, :cond_7

    const/16 v4, 0x8

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->getApplicationVersion()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasLocale()Z

    move-result v4

    if-eqz v4, :cond_8

    const/16 v4, 0x9

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->getLocale()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->getBundledClientEventsList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    const/16 v4, 0xa

    invoke-static {v4, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    goto :goto_0

    :cond_9
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasPackageId()Z

    move-result v4

    if-eqz v4, :cond_a

    const/16 v4, 0xb

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->getPackageId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_a
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasImeLangCount()Z

    move-result v4

    if-eqz v4, :cond_b

    const/16 v4, 0xc

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->getImeLangCount()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_b
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasVoicesearchLangCount()Z

    move-result v4

    if-eqz v4, :cond_c

    const/16 v4, 0xd

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->getVoicesearchLangCount()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_c
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasPairedBluetooth()Z

    move-result v4

    if-eqz v4, :cond_d

    const/16 v4, 0xe

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->getPairedBluetooth()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_d
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->getExperimentIdList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_1

    :cond_e
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->getExperimentIdList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasApplicationVersionName()Z

    move-result v4

    if-eqz v4, :cond_f

    const/16 v4, 0x10

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->getApplicationVersionName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_f
    iput v3, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->cachedSize:I

    return v3
.end method

.method public getTriggerApplicationId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->triggerApplicationId_:Ljava/lang/String;

    return-object v0
.end method

.method public getVoicesearchLangCount()I
    .locals 1

    iget v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->voicesearchLangCount_:I

    return v0
.end method

.method public hasApplicationId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasApplicationId:Z

    return v0
.end method

.method public hasApplicationVersion()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasApplicationVersion:Z

    return v0
.end method

.method public hasApplicationVersionName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasApplicationVersionName:Z

    return v0
.end method

.method public hasDeviceModel()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasDeviceModel:Z

    return v0
.end method

.method public hasImeLangCount()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasImeLangCount:Z

    return v0
.end method

.method public hasInstallId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasInstallId:Z

    return v0
.end method

.method public hasLocale()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasLocale:Z

    return v0
.end method

.method public hasPackageId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasPackageId:Z

    return v0
.end method

.method public hasPairedBluetooth()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasPairedBluetooth:Z

    return v0
.end method

.method public hasPlatformId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasPlatformId:Z

    return v0
.end method

.method public hasPlatformVersion()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasPlatformVersion:Z

    return v0
.end method

.method public hasRequestTimeMsec()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasRequestTimeMsec:Z

    return v0
.end method

.method public hasTriggerApplicationId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasTriggerApplicationId:Z

    return v0
.end method

.method public hasVoicesearchLangCount()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasVoicesearchLangCount:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readUInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->setRequestTimeMsec(J)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->setInstallId(Ljava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->setPlatformId(Ljava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->setPlatformVersion(Ljava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->setDeviceModel(Ljava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->setApplicationId(Ljava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->setTriggerApplicationId(Ljava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->setApplicationVersion(Ljava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->setLocale(Ljava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    goto :goto_0

    :sswitch_a
    new-instance v1, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    invoke-direct {v1}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->addBundledClientEvents(Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->setPackageId(Ljava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->setImeLangCount(I)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->setVoicesearchLangCount(I)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    goto :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->setPairedBluetooth(Z)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->addExperimentId(Ljava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->setApplicationVersionName(Ljava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
        0x70 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
    .end sparse-switch
.end method

.method public setApplicationId(Ljava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasApplicationId:Z

    iput-object p1, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->applicationId_:Ljava/lang/String;

    return-object p0
.end method

.method public setApplicationVersion(Ljava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasApplicationVersion:Z

    iput-object p1, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->applicationVersion_:Ljava/lang/String;

    return-object p0
.end method

.method public setApplicationVersionName(Ljava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasApplicationVersionName:Z

    iput-object p1, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->applicationVersionName_:Ljava/lang/String;

    return-object p0
.end method

.method public setDeviceModel(Ljava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasDeviceModel:Z

    iput-object p1, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->deviceModel_:Ljava/lang/String;

    return-object p0
.end method

.method public setImeLangCount(I)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasImeLangCount:Z

    iput p1, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->imeLangCount_:I

    return-object p0
.end method

.method public setInstallId(Ljava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasInstallId:Z

    iput-object p1, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->installId_:Ljava/lang/String;

    return-object p0
.end method

.method public setLocale(Ljava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasLocale:Z

    iput-object p1, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->locale_:Ljava/lang/String;

    return-object p0
.end method

.method public setPackageId(Ljava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasPackageId:Z

    iput-object p1, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->packageId_:Ljava/lang/String;

    return-object p0
.end method

.method public setPairedBluetooth(Z)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasPairedBluetooth:Z

    iput-boolean p1, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->pairedBluetooth_:Z

    return-object p0
.end method

.method public setPlatformId(Ljava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasPlatformId:Z

    iput-object p1, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->platformId_:Ljava/lang/String;

    return-object p0
.end method

.method public setPlatformVersion(Ljava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasPlatformVersion:Z

    iput-object p1, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->platformVersion_:Ljava/lang/String;

    return-object p0
.end method

.method public setRequestTimeMsec(J)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasRequestTimeMsec:Z

    iput-wide p1, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->requestTimeMsec_:J

    return-object p0
.end method

.method public setTriggerApplicationId(Ljava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasTriggerApplicationId:Z

    iput-object p1, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->triggerApplicationId_:Ljava/lang/String;

    return-object p0
.end method

.method public setVoicesearchLangCount(I)Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasVoicesearchLangCount:Z

    iput p1, p0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->voicesearchLangCount_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 5
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasRequestTimeMsec()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->getRequestTimeMsec()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeUInt64(IJ)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasInstallId()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->getInstallId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasPlatformId()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->getPlatformId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasPlatformVersion()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->getPlatformVersion()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasDeviceModel()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->getDeviceModel()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasApplicationId()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->getApplicationId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasTriggerApplicationId()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->getTriggerApplicationId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasApplicationVersion()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->getApplicationVersion()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasLocale()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->getLocale()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->getBundledClientEventsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    const/16 v2, 0xa

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_9
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasPackageId()Z

    move-result v2

    if-eqz v2, :cond_a

    const/16 v2, 0xb

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->getPackageId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasImeLangCount()Z

    move-result v2

    if-eqz v2, :cond_b

    const/16 v2, 0xc

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->getImeLangCount()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_b
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasVoicesearchLangCount()Z

    move-result v2

    if-eqz v2, :cond_c

    const/16 v2, 0xd

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->getVoicesearchLangCount()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_c
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasPairedBluetooth()Z

    move-result v2

    if-eqz v2, :cond_d

    const/16 v2, 0xe

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->getPairedBluetooth()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_d
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->getExperimentIdList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/16 v2, 0xf

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    goto :goto_1

    :cond_e
    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->hasApplicationVersionName()Z

    move-result v2

    if-eqz v2, :cond_f

    const/16 v2, 0x10

    invoke-virtual {p0}, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;->getApplicationVersionName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_f
    return-void
.end method
