.class public final Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "HotwordFeature.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/recognizer/api/HotwordFeature;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "HotwordConfidenceFeature"
.end annotation


# instance fields
.field private amScore_:F

.field private ascoreBest_:F

.field private ascoreMean_:F

.field private ascoreMeandiff_:F

.field private ascoreStddev_:F

.field private ascoreWorst_:F

.field private baseline_:Z

.field private cachedSize:I

.field private durScore_:F

.field private frameDistance_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private hasAmScore:Z

.field private hasAscoreBest:Z

.field private hasAscoreMean:Z

.field private hasAscoreMeandiff:Z

.field private hasAscoreStddev:Z

.field private hasAscoreWorst:Z

.field private hasBaseline:Z

.field private hasDurScore:Z

.field private hasLmScore:Z

.field private hasNormalizedWordDuration:Z

.field private hasNumPhones:Z

.field private hasPhoneDurationScore:Z

.field private hasSpeakerRate:Z

.field private hasStability:Z

.field private hasStartFrame:Z

.field private hasWordDurationFrames:Z

.field private lmScore_:F

.field private normalizedWordDuration_:F

.field private numPhones_:F

.field private phAlignDelete_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private phAlignInsert_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private phAlign_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private phExpectationAlign_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private phExpectationDeleteAlign_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private phExpectationDeleteNn_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private phExpectationInsertAlign_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private phExpectationInsertNn_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private phExpectationNn_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private phNnDelete_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private phNnInsert_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private phNn_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private phoneDurationScore_:F

.field private speakerRate_:F

.field private stability_:F

.field private startFrame_:F

.field private wordDurationFrames_:F


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput v1, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phoneDurationScore_:F

    iput v1, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->speakerRate_:F

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->frameDistance_:Ljava/util/List;

    iput v1, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->wordDurationFrames_:F

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->baseline_:Z

    iput v1, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->numPhones_:F

    iput v1, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->normalizedWordDuration_:F

    iput v1, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->ascoreMean_:F

    iput v1, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->ascoreStddev_:F

    iput v1, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->ascoreWorst_:F

    iput v1, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->ascoreMeandiff_:F

    iput v1, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->ascoreBest_:F

    iput v1, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->lmScore_:F

    iput v1, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->durScore_:F

    iput v1, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->amScore_:F

    iput v1, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->startFrame_:F

    iput v1, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->stability_:F

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phAlign_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phAlignDelete_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phAlignInsert_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phNn_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phNnDelete_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phNnInsert_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phExpectationAlign_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phExpectationNn_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phExpectationDeleteAlign_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phExpectationInsertAlign_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phExpectationDeleteNn_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phExpectationInsertNn_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addFrameDistance(F)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;
    .locals 2
    .param p1    # F

    iget-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->frameDistance_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->frameDistance_:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->frameDistance_:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addPhAlign(Z)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phAlign_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phAlign_:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phAlign_:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addPhAlignDelete(Z)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phAlignDelete_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phAlignDelete_:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phAlignDelete_:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addPhAlignInsert(Z)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phAlignInsert_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phAlignInsert_:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phAlignInsert_:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addPhExpectationAlign(Z)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phExpectationAlign_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phExpectationAlign_:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phExpectationAlign_:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addPhExpectationDeleteAlign(Z)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phExpectationDeleteAlign_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phExpectationDeleteAlign_:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phExpectationDeleteAlign_:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addPhExpectationDeleteNn(Z)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phExpectationDeleteNn_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phExpectationDeleteNn_:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phExpectationDeleteNn_:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addPhExpectationInsertAlign(Z)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phExpectationInsertAlign_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phExpectationInsertAlign_:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phExpectationInsertAlign_:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addPhExpectationInsertNn(Z)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phExpectationInsertNn_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phExpectationInsertNn_:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phExpectationInsertNn_:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addPhExpectationNn(Z)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phExpectationNn_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phExpectationNn_:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phExpectationNn_:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addPhNn(Z)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phNn_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phNn_:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phNn_:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addPhNnDelete(Z)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phNnDelete_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phNnDelete_:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phNnDelete_:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addPhNnInsert(Z)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phNnInsert_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phNnInsert_:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phNnInsert_:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getAmScore()F
    .locals 1

    iget v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->amScore_:F

    return v0
.end method

.method public getAscoreBest()F
    .locals 1

    iget v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->ascoreBest_:F

    return v0
.end method

.method public getAscoreMean()F
    .locals 1

    iget v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->ascoreMean_:F

    return v0
.end method

.method public getAscoreMeandiff()F
    .locals 1

    iget v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->ascoreMeandiff_:F

    return v0
.end method

.method public getAscoreStddev()F
    .locals 1

    iget v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->ascoreStddev_:F

    return v0
.end method

.method public getAscoreWorst()F
    .locals 1

    iget v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->ascoreWorst_:F

    return v0
.end method

.method public getBaseline()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->baseline_:Z

    return v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->cachedSize:I

    return v0
.end method

.method public getDurScore()F
    .locals 1

    iget v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->durScore_:F

    return v0
.end method

.method public getFrameDistanceList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->frameDistance_:Ljava/util/List;

    return-object v0
.end method

.method public getLmScore()F
    .locals 1

    iget v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->lmScore_:F

    return v0
.end method

.method public getNormalizedWordDuration()F
    .locals 1

    iget v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->normalizedWordDuration_:F

    return v0
.end method

.method public getNumPhones()F
    .locals 1

    iget v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->numPhones_:F

    return v0
.end method

.method public getPhAlignDeleteList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phAlignDelete_:Ljava/util/List;

    return-object v0
.end method

.method public getPhAlignInsertList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phAlignInsert_:Ljava/util/List;

    return-object v0
.end method

.method public getPhAlignList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phAlign_:Ljava/util/List;

    return-object v0
.end method

.method public getPhExpectationAlignList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phExpectationAlign_:Ljava/util/List;

    return-object v0
.end method

.method public getPhExpectationDeleteAlignList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phExpectationDeleteAlign_:Ljava/util/List;

    return-object v0
.end method

.method public getPhExpectationDeleteNnList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phExpectationDeleteNn_:Ljava/util/List;

    return-object v0
.end method

.method public getPhExpectationInsertAlignList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phExpectationInsertAlign_:Ljava/util/List;

    return-object v0
.end method

.method public getPhExpectationInsertNnList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phExpectationInsertNn_:Ljava/util/List;

    return-object v0
.end method

.method public getPhExpectationNnList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phExpectationNn_:Ljava/util/List;

    return-object v0
.end method

.method public getPhNnDeleteList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phNnDelete_:Ljava/util/List;

    return-object v0
.end method

.method public getPhNnInsertList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phNnInsert_:Ljava/util/List;

    return-object v0
.end method

.method public getPhNnList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phNn_:Ljava/util/List;

    return-object v0
.end method

.method public getPhoneDurationScore()F
    .locals 1

    iget v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phoneDurationScore_:F

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasPhoneDurationScore()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getPhoneDurationScore()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v1, v2

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasSpeakerRate()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getSpeakerRate()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v1, v2

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getFrameDistanceList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v0, v2, 0x4

    add-int/2addr v1, v0

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getFrameDistanceList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasWordDurationFrames()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getWordDurationFrames()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v1, v2

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasBaseline()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getBaseline()Z

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v1, v2

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasNumPhones()Z

    move-result v2

    if-eqz v2, :cond_4

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getNumPhones()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v1, v2

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasNormalizedWordDuration()Z

    move-result v2

    if-eqz v2, :cond_5

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getNormalizedWordDuration()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v1, v2

    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasAscoreMean()Z

    move-result v2

    if-eqz v2, :cond_6

    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getAscoreMean()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v1, v2

    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasAscoreStddev()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0xb

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getAscoreStddev()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v1, v2

    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasAscoreWorst()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0xc

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getAscoreWorst()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v1, v2

    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasAscoreMeandiff()Z

    move-result v2

    if-eqz v2, :cond_9

    const/16 v2, 0xd

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getAscoreMeandiff()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v1, v2

    :cond_9
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasAscoreBest()Z

    move-result v2

    if-eqz v2, :cond_a

    const/16 v2, 0xe

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getAscoreBest()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v1, v2

    :cond_a
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasLmScore()Z

    move-result v2

    if-eqz v2, :cond_b

    const/16 v2, 0xf

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getLmScore()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v1, v2

    :cond_b
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasDurScore()Z

    move-result v2

    if-eqz v2, :cond_c

    const/16 v2, 0x10

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getDurScore()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v1, v2

    :cond_c
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasAmScore()Z

    move-result v2

    if-eqz v2, :cond_d

    const/16 v2, 0x11

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getAmScore()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v1, v2

    :cond_d
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasStartFrame()Z

    move-result v2

    if-eqz v2, :cond_e

    const/16 v2, 0x12

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getStartFrame()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v1, v2

    :cond_e
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getPhExpectationAlignList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v0, v2, 0x1

    add-int/2addr v1, v0

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getPhExpectationAlignList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getPhExpectationNnList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v0, v2, 0x1

    add-int/2addr v1, v0

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getPhExpectationNnList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getPhExpectationDeleteAlignList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v0, v2, 0x1

    add-int/2addr v1, v0

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getPhExpectationDeleteAlignList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getPhExpectationInsertAlignList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v0, v2, 0x1

    add-int/2addr v1, v0

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getPhExpectationInsertAlignList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasStability()Z

    move-result v2

    if-eqz v2, :cond_f

    const/16 v2, 0x17

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getStability()F

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v2

    add-int/2addr v1, v2

    :cond_f
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getPhExpectationDeleteNnList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v0, v2, 0x1

    add-int/2addr v1, v0

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getPhExpectationDeleteNnList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getPhExpectationInsertNnList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v0, v2, 0x1

    add-int/2addr v1, v0

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getPhExpectationInsertNnList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getPhAlignList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v0, v2, 0x1

    add-int/2addr v1, v0

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getPhAlignList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getPhAlignDeleteList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v0, v2, 0x1

    add-int/2addr v1, v0

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getPhAlignDeleteList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getPhAlignInsertList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v0, v2, 0x1

    add-int/2addr v1, v0

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getPhAlignInsertList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getPhNnList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v0, v2, 0x1

    add-int/2addr v1, v0

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getPhNnList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getPhNnDeleteList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v0, v2, 0x1

    add-int/2addr v1, v0

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getPhNnDeleteList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getPhNnInsertList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v0, v2, 0x1

    add-int/2addr v1, v0

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getPhNnInsertList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->cachedSize:I

    return v1
.end method

.method public getSpeakerRate()F
    .locals 1

    iget v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->speakerRate_:F

    return v0
.end method

.method public getStability()F
    .locals 1

    iget v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->stability_:F

    return v0
.end method

.method public getStartFrame()F
    .locals 1

    iget v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->startFrame_:F

    return v0
.end method

.method public getWordDurationFrames()F
    .locals 1

    iget v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->wordDurationFrames_:F

    return v0
.end method

.method public hasAmScore()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasAmScore:Z

    return v0
.end method

.method public hasAscoreBest()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasAscoreBest:Z

    return v0
.end method

.method public hasAscoreMean()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasAscoreMean:Z

    return v0
.end method

.method public hasAscoreMeandiff()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasAscoreMeandiff:Z

    return v0
.end method

.method public hasAscoreStddev()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasAscoreStddev:Z

    return v0
.end method

.method public hasAscoreWorst()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasAscoreWorst:Z

    return v0
.end method

.method public hasBaseline()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasBaseline:Z

    return v0
.end method

.method public hasDurScore()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasDurScore:Z

    return v0
.end method

.method public hasLmScore()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasLmScore:Z

    return v0
.end method

.method public hasNormalizedWordDuration()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasNormalizedWordDuration:Z

    return v0
.end method

.method public hasNumPhones()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasNumPhones:Z

    return v0
.end method

.method public hasPhoneDurationScore()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasPhoneDurationScore:Z

    return v0
.end method

.method public hasSpeakerRate()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasSpeakerRate:Z

    return v0
.end method

.method public hasStability()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasStability:Z

    return v0
.end method

.method public hasStartFrame()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasStartFrame:Z

    return v0
.end method

.method public hasWordDurationFrames()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasWordDurationFrames:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->setPhoneDurationScore(F)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->setSpeakerRate(F)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->addFrameDistance(F)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->setWordDurationFrames(F)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->setBaseline(Z)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->setNumPhones(F)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->setNormalizedWordDuration(F)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->setAscoreMean(F)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->setAscoreStddev(F)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->setAscoreWorst(F)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->setAscoreMeandiff(F)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->setAscoreBest(F)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->setLmScore(F)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;

    goto :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->setDurScore(F)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;

    goto :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->setAmScore(F)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->setStartFrame(F)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->addPhExpectationAlign(Z)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->addPhExpectationNn(Z)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;

    goto/16 :goto_0

    :sswitch_13
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->addPhExpectationDeleteAlign(Z)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;

    goto/16 :goto_0

    :sswitch_14
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->addPhExpectationInsertAlign(Z)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;

    goto/16 :goto_0

    :sswitch_15
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->setStability(F)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;

    goto/16 :goto_0

    :sswitch_16
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->addPhExpectationDeleteNn(Z)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;

    goto/16 :goto_0

    :sswitch_17
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->addPhExpectationInsertNn(Z)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;

    goto/16 :goto_0

    :sswitch_18
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->addPhAlign(Z)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;

    goto/16 :goto_0

    :sswitch_19
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->addPhAlignDelete(Z)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;

    goto/16 :goto_0

    :sswitch_1a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->addPhAlignInsert(Z)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;

    goto/16 :goto_0

    :sswitch_1b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->addPhNn(Z)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;

    goto/16 :goto_0

    :sswitch_1c
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->addPhNnDelete(Z)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;

    goto/16 :goto_0

    :sswitch_1d
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->addPhNnInsert(Z)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x15 -> :sswitch_2
        0x1d -> :sswitch_3
        0x25 -> :sswitch_4
        0x30 -> :sswitch_5
        0x45 -> :sswitch_6
        0x4d -> :sswitch_7
        0x55 -> :sswitch_8
        0x5d -> :sswitch_9
        0x65 -> :sswitch_a
        0x6d -> :sswitch_b
        0x75 -> :sswitch_c
        0x7d -> :sswitch_d
        0x85 -> :sswitch_e
        0x8d -> :sswitch_f
        0x95 -> :sswitch_10
        0x98 -> :sswitch_11
        0xa0 -> :sswitch_12
        0xa8 -> :sswitch_13
        0xb0 -> :sswitch_14
        0xbd -> :sswitch_15
        0xc0 -> :sswitch_16
        0xc8 -> :sswitch_17
        0xd0 -> :sswitch_18
        0xd8 -> :sswitch_19
        0xe0 -> :sswitch_1a
        0xe8 -> :sswitch_1b
        0xf0 -> :sswitch_1c
        0xf8 -> :sswitch_1d
    .end sparse-switch
.end method

.method public setAmScore(F)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasAmScore:Z

    iput p1, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->amScore_:F

    return-object p0
.end method

.method public setAscoreBest(F)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasAscoreBest:Z

    iput p1, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->ascoreBest_:F

    return-object p0
.end method

.method public setAscoreMean(F)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasAscoreMean:Z

    iput p1, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->ascoreMean_:F

    return-object p0
.end method

.method public setAscoreMeandiff(F)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasAscoreMeandiff:Z

    iput p1, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->ascoreMeandiff_:F

    return-object p0
.end method

.method public setAscoreStddev(F)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasAscoreStddev:Z

    iput p1, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->ascoreStddev_:F

    return-object p0
.end method

.method public setAscoreWorst(F)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasAscoreWorst:Z

    iput p1, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->ascoreWorst_:F

    return-object p0
.end method

.method public setBaseline(Z)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasBaseline:Z

    iput-boolean p1, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->baseline_:Z

    return-object p0
.end method

.method public setDurScore(F)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasDurScore:Z

    iput p1, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->durScore_:F

    return-object p0
.end method

.method public setLmScore(F)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasLmScore:Z

    iput p1, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->lmScore_:F

    return-object p0
.end method

.method public setNormalizedWordDuration(F)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasNormalizedWordDuration:Z

    iput p1, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->normalizedWordDuration_:F

    return-object p0
.end method

.method public setNumPhones(F)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasNumPhones:Z

    iput p1, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->numPhones_:F

    return-object p0
.end method

.method public setPhoneDurationScore(F)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasPhoneDurationScore:Z

    iput p1, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->phoneDurationScore_:F

    return-object p0
.end method

.method public setSpeakerRate(F)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasSpeakerRate:Z

    iput p1, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->speakerRate_:F

    return-object p0
.end method

.method public setStability(F)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasStability:Z

    iput p1, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->stability_:F

    return-object p0
.end method

.method public setStartFrame(F)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasStartFrame:Z

    iput p1, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->startFrame_:F

    return-object p0
.end method

.method public setWordDurationFrames(F)Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasWordDurationFrames:Z

    iput p1, p0, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->wordDurationFrames_:F

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasPhoneDurationScore()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getPhoneDurationScore()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasSpeakerRate()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getSpeakerRate()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getFrameDistanceList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasWordDurationFrames()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getWordDurationFrames()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasBaseline()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getBaseline()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasNumPhones()Z

    move-result v2

    if-eqz v2, :cond_5

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getNumPhones()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasNormalizedWordDuration()Z

    move-result v2

    if-eqz v2, :cond_6

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getNormalizedWordDuration()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasAscoreMean()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getAscoreMean()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasAscoreStddev()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0xb

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getAscoreStddev()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasAscoreWorst()Z

    move-result v2

    if-eqz v2, :cond_9

    const/16 v2, 0xc

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getAscoreWorst()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasAscoreMeandiff()Z

    move-result v2

    if-eqz v2, :cond_a

    const/16 v2, 0xd

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getAscoreMeandiff()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasAscoreBest()Z

    move-result v2

    if-eqz v2, :cond_b

    const/16 v2, 0xe

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getAscoreBest()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_b
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasLmScore()Z

    move-result v2

    if-eqz v2, :cond_c

    const/16 v2, 0xf

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getLmScore()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_c
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasDurScore()Z

    move-result v2

    if-eqz v2, :cond_d

    const/16 v2, 0x10

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getDurScore()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_d
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasAmScore()Z

    move-result v2

    if-eqz v2, :cond_e

    const/16 v2, 0x11

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getAmScore()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_e
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasStartFrame()Z

    move-result v2

    if-eqz v2, :cond_f

    const/16 v2, 0x12

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getStartFrame()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_f
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getPhExpectationAlignList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_10

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const/16 v2, 0x13

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    goto :goto_1

    :cond_10
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getPhExpectationNnList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_11

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const/16 v2, 0x14

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    goto :goto_2

    :cond_11
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getPhExpectationDeleteAlignList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_12

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const/16 v2, 0x15

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    goto :goto_3

    :cond_12
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getPhExpectationInsertAlignList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_13

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const/16 v2, 0x16

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    goto :goto_4

    :cond_13
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->hasStability()Z

    move-result v2

    if-eqz v2, :cond_14

    const/16 v2, 0x17

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getStability()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_14
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getPhExpectationDeleteNnList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_15

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const/16 v2, 0x18

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    goto :goto_5

    :cond_15
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getPhExpectationInsertNnList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const/16 v2, 0x19

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    goto :goto_6

    :cond_16
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getPhAlignList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_17

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const/16 v2, 0x1a

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    goto :goto_7

    :cond_17
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getPhAlignDeleteList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_18

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const/16 v2, 0x1b

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    goto :goto_8

    :cond_18
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getPhAlignInsertList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_19

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const/16 v2, 0x1c

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    goto :goto_9

    :cond_19
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getPhNnList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const/16 v2, 0x1d

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    goto :goto_a

    :cond_1a
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getPhNnDeleteList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const/16 v2, 0x1e

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    goto :goto_b

    :cond_1b
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/HotwordFeature$HotwordConfidenceFeature;->getPhNnInsertList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const/16 v2, 0x1f

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    goto :goto_c

    :cond_1c
    return-void
.end method
