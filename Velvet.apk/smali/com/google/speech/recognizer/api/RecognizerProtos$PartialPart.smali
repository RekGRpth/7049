.class public final Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "RecognizerProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/recognizer/api/RecognizerProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PartialPart"
.end annotation


# instance fields
.field private cachedSize:I

.field private hasSemanticResult:Z

.field private hasStability:Z

.field private hasText:Z

.field private semanticResult_:Lcom/google/speech/recognizer/api/RecognizerProtos$SemanticResult;

.field private stability_:D

.field private text_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;->text_:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;->stability_:D

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;->semanticResult_:Lcom/google/speech/recognizer/api/RecognizerProtos$SemanticResult;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;->cachedSize:I

    return v0
.end method

.method public getSemanticResult()Lcom/google/speech/recognizer/api/RecognizerProtos$SemanticResult;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;->semanticResult_:Lcom/google/speech/recognizer/api/RecognizerProtos$SemanticResult;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;->hasText()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;->hasStability()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;->getStability()D

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeDoubleSize(ID)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;->hasSemanticResult()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;->getSemanticResult()Lcom/google/speech/recognizer/api/RecognizerProtos$SemanticResult;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;->cachedSize:I

    return v0
.end method

.method public getStability()D
    .locals 2

    iget-wide v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;->stability_:D

    return-wide v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;->text_:Ljava/lang/String;

    return-object v0
.end method

.method public hasSemanticResult()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;->hasSemanticResult:Z

    return v0
.end method

.method public hasStability()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;->hasStability:Z

    return v0
.end method

.method public hasText()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;->hasText:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;->setText(Ljava/lang/String;)Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readDouble()D

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;->setStability(D)Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/speech/recognizer/api/RecognizerProtos$SemanticResult;

    invoke-direct {v1}, Lcom/google/speech/recognizer/api/RecognizerProtos$SemanticResult;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;->setSemanticResult(Lcom/google/speech/recognizer/api/RecognizerProtos$SemanticResult;)Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x11 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public setSemanticResult(Lcom/google/speech/recognizer/api/RecognizerProtos$SemanticResult;)Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;
    .locals 1
    .param p1    # Lcom/google/speech/recognizer/api/RecognizerProtos$SemanticResult;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;->hasSemanticResult:Z

    iput-object p1, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;->semanticResult_:Lcom/google/speech/recognizer/api/RecognizerProtos$SemanticResult;

    return-object p0
.end method

.method public setStability(D)Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;
    .locals 1
    .param p1    # D

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;->hasStability:Z

    iput-wide p1, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;->stability_:D

    return-object p0
.end method

.method public setText(Ljava/lang/String;)Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;->hasText:Z

    iput-object p1, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;->text_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;->hasText()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;->hasStability()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;->getStability()D

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeDouble(ID)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;->hasSemanticResult()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;->getSemanticResult()Lcom/google/speech/recognizer/api/RecognizerProtos$SemanticResult;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    return-void
.end method
