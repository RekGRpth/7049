.class public final Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "RecognizerProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/recognizer/api/RecognizerProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RecognitionResult"
.end annotation


# instance fields
.field private cachedSize:I

.field private endTimeUsec_:J

.field private hasEndTimeUsec:Z

.field private hasLatticeFst:Z

.field private hasStartTimeUsec:Z

.field private hypothesis_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;",
            ">;"
        }
    .end annotation
.end field

.field private latticeFst_:Lcom/google/protobuf/micro/ByteStringMicro;

.field private startTimeUsec_:J


# direct methods
.method public constructor <init>()V
    .locals 2

    const-wide/16 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-wide v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->startTimeUsec_:J

    iput-wide v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->endTimeUsec_:J

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->hypothesis_:Ljava/util/List;

    sget-object v0, Lcom/google/protobuf/micro/ByteStringMicro;->EMPTY:Lcom/google/protobuf/micro/ByteStringMicro;

    iput-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->latticeFst_:Lcom/google/protobuf/micro/ByteStringMicro;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addHypothesis(Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;)Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;
    .locals 1
    .param p1    # Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->hypothesis_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->hypothesis_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->hypothesis_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->cachedSize:I

    return v0
.end method

.method public getEndTimeUsec()J
    .locals 2

    iget-wide v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->endTimeUsec_:J

    return-wide v0
.end method

.method public getHypothesis(I)Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->hypothesis_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;

    return-object v0
.end method

.method public getHypothesisCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->hypothesis_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getHypothesisList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->hypothesis_:Ljava/util/List;

    return-object v0
.end method

.method public getLatticeFst()Lcom/google/protobuf/micro/ByteStringMicro;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->latticeFst_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->hasStartTimeUsec()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->getStartTimeUsec()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->hasEndTimeUsec()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->getEndTimeUsec()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->getHypothesisList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;

    const/4 v3, 0x3

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->hasLatticeFst()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->getLatticeFst()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBytesSize(ILcom/google/protobuf/micro/ByteStringMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_3
    iput v2, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->cachedSize:I

    return v2
.end method

.method public getStartTimeUsec()J
    .locals 2

    iget-wide v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->startTimeUsec_:J

    return-wide v0
.end method

.method public hasEndTimeUsec()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->hasEndTimeUsec:Z

    return v0
.end method

.method public hasLatticeFst()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->hasLatticeFst:Z

    return v0
.end method

.method public hasStartTimeUsec()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->hasStartTimeUsec:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->setStartTimeUsec(J)Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->setEndTimeUsec(J)Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;

    invoke-direct {v1}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->addHypothesis(Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;)Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBytes()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->setLatticeFst(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public setEndTimeUsec(J)Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->hasEndTimeUsec:Z

    iput-wide p1, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->endTimeUsec_:J

    return-object p0
.end method

.method public setLatticeFst(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/ByteStringMicro;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->hasLatticeFst:Z

    iput-object p1, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->latticeFst_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object p0
.end method

.method public setStartTimeUsec(J)Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->hasStartTimeUsec:Z

    iput-wide p1, p0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->startTimeUsec_:J

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 5
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->hasStartTimeUsec()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->getStartTimeUsec()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->hasEndTimeUsec()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->getEndTimeUsec()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->getHypothesisList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;

    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->hasLatticeFst()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->getLatticeFst()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBytes(ILcom/google/protobuf/micro/ByteStringMicro;)V

    :cond_3
    return-void
.end method
