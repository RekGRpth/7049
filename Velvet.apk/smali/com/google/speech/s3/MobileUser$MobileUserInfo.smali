.class public final Lcom/google/speech/s3/MobileUser$MobileUserInfo;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "MobileUser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/s3/MobileUser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MobileUserInfo"
.end annotation


# instance fields
.field private cachedSize:I

.field private hasNetworkMcc:Z

.field private hasNetworkMnc:Z

.field private hasNetworkType:Z

.field private hasSimMcc:Z

.field private hasSimMnc:Z

.field private networkMcc_:I

.field private networkMnc_:I

.field private networkType_:I

.field private simMcc_:I

.field private simMnc_:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput v0, p0, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->networkMcc_:I

    iput v0, p0, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->networkMnc_:I

    iput v0, p0, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->simMcc_:I

    iput v0, p0, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->simMnc_:I

    iput v0, p0, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->networkType_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->cachedSize:I

    return v0
.end method

.method public getNetworkMcc()I
    .locals 1

    iget v0, p0, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->networkMcc_:I

    return v0
.end method

.method public getNetworkMnc()I
    .locals 1

    iget v0, p0, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->networkMnc_:I

    return v0
.end method

.method public getNetworkType()I
    .locals 1

    iget v0, p0, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->networkType_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->hasNetworkMcc()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->getNetworkMcc()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->hasNetworkMnc()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->getNetworkMnc()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->hasSimMcc()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->getSimMcc()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->hasSimMnc()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->getSimMnc()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->hasNetworkType()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->getNetworkType()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iput v0, p0, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->cachedSize:I

    return v0
.end method

.method public getSimMcc()I
    .locals 1

    iget v0, p0, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->simMcc_:I

    return v0
.end method

.method public getSimMnc()I
    .locals 1

    iget v0, p0, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->simMnc_:I

    return v0
.end method

.method public hasNetworkMcc()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->hasNetworkMcc:Z

    return v0
.end method

.method public hasNetworkMnc()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->hasNetworkMnc:Z

    return v0
.end method

.method public hasNetworkType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->hasNetworkType:Z

    return v0
.end method

.method public hasSimMcc()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->hasSimMcc:Z

    return v0
.end method

.method public hasSimMnc()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->hasSimMnc:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/s3/MobileUser$MobileUserInfo;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/s3/MobileUser$MobileUserInfo;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->setNetworkMcc(I)Lcom/google/speech/s3/MobileUser$MobileUserInfo;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->setNetworkMnc(I)Lcom/google/speech/s3/MobileUser$MobileUserInfo;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->setSimMcc(I)Lcom/google/speech/s3/MobileUser$MobileUserInfo;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->setSimMnc(I)Lcom/google/speech/s3/MobileUser$MobileUserInfo;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->setNetworkType(I)Lcom/google/speech/s3/MobileUser$MobileUserInfo;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public setNetworkMcc(I)Lcom/google/speech/s3/MobileUser$MobileUserInfo;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->hasNetworkMcc:Z

    iput p1, p0, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->networkMcc_:I

    return-object p0
.end method

.method public setNetworkMnc(I)Lcom/google/speech/s3/MobileUser$MobileUserInfo;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->hasNetworkMnc:Z

    iput p1, p0, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->networkMnc_:I

    return-object p0
.end method

.method public setNetworkType(I)Lcom/google/speech/s3/MobileUser$MobileUserInfo;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->hasNetworkType:Z

    iput p1, p0, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->networkType_:I

    return-object p0
.end method

.method public setSimMcc(I)Lcom/google/speech/s3/MobileUser$MobileUserInfo;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->hasSimMcc:Z

    iput p1, p0, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->simMcc_:I

    return-object p0
.end method

.method public setSimMnc(I)Lcom/google/speech/s3/MobileUser$MobileUserInfo;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->hasSimMnc:Z

    iput p1, p0, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->simMnc_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->hasNetworkMcc()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->getNetworkMcc()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->hasNetworkMnc()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->getNetworkMnc()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->hasSimMcc()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->getSimMcc()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->hasSimMnc()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->getSimMnc()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->hasNetworkType()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/speech/s3/MobileUser$MobileUserInfo;->getNetworkType()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_4
    return-void
.end method
