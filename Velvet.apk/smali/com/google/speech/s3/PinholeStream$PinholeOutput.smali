.class public final Lcom/google/speech/s3/PinholeStream$PinholeOutput;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "PinholeStream.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/s3/PinholeStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PinholeOutput"
.end annotation


# instance fields
.field private cachedSize:I

.field private gwsBodyFragment_:Ljava/lang/String;

.field private gwsHeaderComplete_:Z

.field private gwsHeaderFragment_:Ljava/lang/String;

.field private gwsResponseComplete_:Z

.field private hasGwsBodyFragment:Z

.field private hasGwsHeaderComplete:Z

.field private hasGwsHeaderFragment:Z

.field private hasGwsResponseComplete:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->gwsHeaderFragment_:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->gwsHeaderComplete_:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->gwsBodyFragment_:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->gwsResponseComplete_:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->cachedSize:I

    return v0
.end method

.method public getGwsBodyFragment()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->gwsBodyFragment_:Ljava/lang/String;

    return-object v0
.end method

.method public getGwsHeaderComplete()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->gwsHeaderComplete_:Z

    return v0
.end method

.method public getGwsHeaderFragment()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->gwsHeaderFragment_:Ljava/lang/String;

    return-object v0
.end method

.method public getGwsResponseComplete()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->gwsResponseComplete_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->hasGwsHeaderFragment()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->getGwsHeaderFragment()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->hasGwsHeaderComplete()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->getGwsHeaderComplete()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->hasGwsBodyFragment()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->getGwsBodyFragment()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->hasGwsResponseComplete()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->getGwsResponseComplete()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iput v0, p0, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->cachedSize:I

    return v0
.end method

.method public hasGwsBodyFragment()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->hasGwsBodyFragment:Z

    return v0
.end method

.method public hasGwsHeaderComplete()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->hasGwsHeaderComplete:Z

    return v0
.end method

.method public hasGwsHeaderFragment()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->hasGwsHeaderFragment:Z

    return v0
.end method

.method public hasGwsResponseComplete()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->hasGwsResponseComplete:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/s3/PinholeStream$PinholeOutput;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/s3/PinholeStream$PinholeOutput;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->setGwsHeaderFragment(Ljava/lang/String;)Lcom/google/speech/s3/PinholeStream$PinholeOutput;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->setGwsHeaderComplete(Z)Lcom/google/speech/s3/PinholeStream$PinholeOutput;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->setGwsBodyFragment(Ljava/lang/String;)Lcom/google/speech/s3/PinholeStream$PinholeOutput;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->setGwsResponseComplete(Z)Lcom/google/speech/s3/PinholeStream$PinholeOutput;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public setGwsBodyFragment(Ljava/lang/String;)Lcom/google/speech/s3/PinholeStream$PinholeOutput;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->hasGwsBodyFragment:Z

    iput-object p1, p0, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->gwsBodyFragment_:Ljava/lang/String;

    return-object p0
.end method

.method public setGwsHeaderComplete(Z)Lcom/google/speech/s3/PinholeStream$PinholeOutput;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->hasGwsHeaderComplete:Z

    iput-boolean p1, p0, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->gwsHeaderComplete_:Z

    return-object p0
.end method

.method public setGwsHeaderFragment(Ljava/lang/String;)Lcom/google/speech/s3/PinholeStream$PinholeOutput;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->hasGwsHeaderFragment:Z

    iput-object p1, p0, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->gwsHeaderFragment_:Ljava/lang/String;

    return-object p0
.end method

.method public setGwsResponseComplete(Z)Lcom/google/speech/s3/PinholeStream$PinholeOutput;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->hasGwsResponseComplete:Z

    iput-boolean p1, p0, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->gwsResponseComplete_:Z

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->hasGwsHeaderFragment()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->getGwsHeaderFragment()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->hasGwsHeaderComplete()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->getGwsHeaderComplete()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->hasGwsBodyFragment()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->getGwsBodyFragment()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->hasGwsResponseComplete()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/speech/s3/PinholeStream$PinholeOutput;->getGwsResponseComplete()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_3
    return-void
.end method
