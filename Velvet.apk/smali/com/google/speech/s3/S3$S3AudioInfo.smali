.class public final Lcom/google/speech/s3/S3$S3AudioInfo;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "S3.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/s3/S3;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "S3AudioInfo"
.end annotation


# instance fields
.field private cachedSize:I

.field private encoding_:I

.field private hasEncoding:Z

.field private hasSampleRateHz:Z

.field private sampleRateHz_:F


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/speech/s3/S3$S3AudioInfo;->encoding_:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/speech/s3/S3$S3AudioInfo;->sampleRateHz_:F

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/s3/S3$S3AudioInfo;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/speech/s3/S3$S3AudioInfo;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3AudioInfo;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/speech/s3/S3$S3AudioInfo;->cachedSize:I

    return v0
.end method

.method public getEncoding()I
    .locals 1

    iget v0, p0, Lcom/google/speech/s3/S3$S3AudioInfo;->encoding_:I

    return v0
.end method

.method public getSampleRateHz()F
    .locals 1

    iget v0, p0, Lcom/google/speech/s3/S3$S3AudioInfo;->sampleRateHz_:F

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3AudioInfo;->hasSampleRateHz()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3AudioInfo;->getSampleRateHz()F

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3AudioInfo;->hasEncoding()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3AudioInfo;->getEncoding()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iput v0, p0, Lcom/google/speech/s3/S3$S3AudioInfo;->cachedSize:I

    return v0
.end method

.method public hasEncoding()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3AudioInfo;->hasEncoding:Z

    return v0
.end method

.method public hasSampleRateHz()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3AudioInfo;->hasSampleRateHz:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/speech/s3/S3$S3AudioInfo;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/s3/S3$S3AudioInfo;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/s3/S3$S3AudioInfo;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/speech/s3/S3$S3AudioInfo;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3AudioInfo;->setSampleRateHz(F)Lcom/google/speech/s3/S3$S3AudioInfo;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3AudioInfo;->setEncoding(I)Lcom/google/speech/s3/S3$S3AudioInfo;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x15 -> :sswitch_1
        0x18 -> :sswitch_2
    .end sparse-switch
.end method

.method public setEncoding(I)Lcom/google/speech/s3/S3$S3AudioInfo;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3AudioInfo;->hasEncoding:Z

    iput p1, p0, Lcom/google/speech/s3/S3$S3AudioInfo;->encoding_:I

    return-object p0
.end method

.method public setSampleRateHz(F)Lcom/google/speech/s3/S3$S3AudioInfo;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3AudioInfo;->hasSampleRateHz:Z

    iput p1, p0, Lcom/google/speech/s3/S3$S3AudioInfo;->sampleRateHz_:F

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3AudioInfo;->hasSampleRateHz()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3AudioInfo;->getSampleRateHz()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3AudioInfo;->hasEncoding()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3AudioInfo;->getEncoding()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    return-void
.end method
