.class public final Lcom/google/speech/s3/S3$S3Request;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "S3.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/s3/S3;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "S3Request"
.end annotation


# instance fields
.field private cachedSize:I

.field private clientLogRequestExtension_:Lcom/google/speech/s3/ClientLogRequestProto$ClientLogRequest;

.field private debuggingEnabled_:Z

.field private endOfData_:Z

.field private gogglesClientLogExtension_:Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesClientLog;

.field private gogglesS3SessionOptionsExtension_:Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;

.field private gogglesStreamRequestExtension_:Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;

.field private hasClientLogRequestExtension:Z

.field private hasDebuggingEnabled:Z

.field private hasEndOfData:Z

.field private hasGogglesClientLogExtension:Z

.field private hasGogglesS3SessionOptionsExtension:Z

.field private hasGogglesStreamRequestExtension:Z

.field private hasLoggingEnabled:Z

.field private hasMajelClientInfoExtension:Z

.field private hasMajelServiceRequestExtension:Z

.field private hasMobileUserInfoExtension:Z

.field private hasPinholeParamsExtension:Z

.field private hasPinholeTtsBridgeParamsExtension:Z

.field private hasS3AudioDataExtension:Z

.field private hasS3AudioInfoExtension:Z

.field private hasS3ClientInfoExtension:Z

.field private hasS3ConnectionInfoExtension:Z

.field private hasS3ExperimentInfoExtension:Z

.field private hasS3RecognizerInfoExtension:Z

.field private hasS3SessionInfoExtension:Z

.field private hasS3UserInfoExtension:Z

.field private hasService:Z

.field private hasSoundSearchInfoExtension:Z

.field private hasTtsServiceRequestExtension:Z

.field private loggingEnabled_:Z

.field private majelClientInfoExtension_:Lcom/google/speech/speech/s3/Majel$MajelClientInfo;

.field private majelServiceRequestExtension_:Lcom/google/speech/speech/s3/Majel$MajelServiceRequest;

.field private mobileUserInfoExtension_:Lcom/google/speech/s3/MobileUser$MobileUserInfo;

.field private pinholeParamsExtension_:Lcom/google/speech/s3/PinholeStream$PinholeParams;

.field private pinholeTtsBridgeParamsExtension_:Lcom/google/speech/s3/PinholeStream$PinholeTtsBridgeParams;

.field private s3AudioDataExtension_:Lcom/google/speech/s3/S3$S3AudioData;

.field private s3AudioInfoExtension_:Lcom/google/speech/s3/S3$S3AudioInfo;

.field private s3ClientInfoExtension_:Lcom/google/speech/s3/S3$S3ClientInfo;

.field private s3ConnectionInfoExtension_:Lcom/google/speech/s3/S3$S3ConnectionInfo;

.field private s3ExperimentInfoExtension_:Lcom/google/speech/s3/S3$S3ExperimentInfo;

.field private s3RecognizerInfoExtension_:Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;

.field private s3SessionInfoExtension_:Lcom/google/speech/s3/S3$S3SessionInfo;

.field private s3UserInfoExtension_:Lcom/google/speech/s3/S3$S3UserInfo;

.field private service_:Ljava/lang/String;

.field private soundSearchInfoExtension_:Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;

.field private ttsServiceRequestExtension_:Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/speech/s3/S3$S3Request;->service_:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->loggingEnabled_:Z

    iput-boolean v2, p0, Lcom/google/speech/s3/S3$S3Request;->debuggingEnabled_:Z

    iput-boolean v2, p0, Lcom/google/speech/s3/S3$S3Request;->endOfData_:Z

    iput-object v1, p0, Lcom/google/speech/s3/S3$S3Request;->s3UserInfoExtension_:Lcom/google/speech/s3/S3$S3UserInfo;

    iput-object v1, p0, Lcom/google/speech/s3/S3$S3Request;->s3AudioInfoExtension_:Lcom/google/speech/s3/S3$S3AudioInfo;

    iput-object v1, p0, Lcom/google/speech/s3/S3$S3Request;->s3AudioDataExtension_:Lcom/google/speech/s3/S3$S3AudioData;

    iput-object v1, p0, Lcom/google/speech/s3/S3$S3Request;->s3ClientInfoExtension_:Lcom/google/speech/s3/S3$S3ClientInfo;

    iput-object v1, p0, Lcom/google/speech/s3/S3$S3Request;->s3RecognizerInfoExtension_:Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;

    iput-object v1, p0, Lcom/google/speech/s3/S3$S3Request;->mobileUserInfoExtension_:Lcom/google/speech/s3/MobileUser$MobileUserInfo;

    iput-object v1, p0, Lcom/google/speech/s3/S3$S3Request;->s3SessionInfoExtension_:Lcom/google/speech/s3/S3$S3SessionInfo;

    iput-object v1, p0, Lcom/google/speech/s3/S3$S3Request;->clientLogRequestExtension_:Lcom/google/speech/s3/ClientLogRequestProto$ClientLogRequest;

    iput-object v1, p0, Lcom/google/speech/s3/S3$S3Request;->ttsServiceRequestExtension_:Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;

    iput-object v1, p0, Lcom/google/speech/s3/S3$S3Request;->majelServiceRequestExtension_:Lcom/google/speech/speech/s3/Majel$MajelServiceRequest;

    iput-object v1, p0, Lcom/google/speech/s3/S3$S3Request;->majelClientInfoExtension_:Lcom/google/speech/speech/s3/Majel$MajelClientInfo;

    iput-object v1, p0, Lcom/google/speech/s3/S3$S3Request;->s3ExperimentInfoExtension_:Lcom/google/speech/s3/S3$S3ExperimentInfo;

    iput-object v1, p0, Lcom/google/speech/s3/S3$S3Request;->pinholeParamsExtension_:Lcom/google/speech/s3/PinholeStream$PinholeParams;

    iput-object v1, p0, Lcom/google/speech/s3/S3$S3Request;->s3ConnectionInfoExtension_:Lcom/google/speech/s3/S3$S3ConnectionInfo;

    iput-object v1, p0, Lcom/google/speech/s3/S3$S3Request;->soundSearchInfoExtension_:Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;

    iput-object v1, p0, Lcom/google/speech/s3/S3$S3Request;->gogglesClientLogExtension_:Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesClientLog;

    iput-object v1, p0, Lcom/google/speech/s3/S3$S3Request;->gogglesS3SessionOptionsExtension_:Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;

    iput-object v1, p0, Lcom/google/speech/s3/S3$S3Request;->gogglesStreamRequestExtension_:Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;

    iput-object v1, p0, Lcom/google/speech/s3/S3$S3Request;->pinholeTtsBridgeParamsExtension_:Lcom/google/speech/s3/PinholeStream$PinholeTtsBridgeParams;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/s3/S3$S3Request;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/speech/s3/S3$S3Request;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/speech/s3/S3$S3Request;->cachedSize:I

    return v0
.end method

.method public getClientLogRequestExtension()Lcom/google/speech/s3/ClientLogRequestProto$ClientLogRequest;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3Request;->clientLogRequestExtension_:Lcom/google/speech/s3/ClientLogRequestProto$ClientLogRequest;

    return-object v0
.end method

.method public getDebuggingEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->debuggingEnabled_:Z

    return v0
.end method

.method public getEndOfData()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->endOfData_:Z

    return v0
.end method

.method public getGogglesClientLogExtension()Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesClientLog;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3Request;->gogglesClientLogExtension_:Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesClientLog;

    return-object v0
.end method

.method public getGogglesS3SessionOptionsExtension()Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3Request;->gogglesS3SessionOptionsExtension_:Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;

    return-object v0
.end method

.method public getGogglesStreamRequestExtension()Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3Request;->gogglesStreamRequestExtension_:Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;

    return-object v0
.end method

.method public getLoggingEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->loggingEnabled_:Z

    return v0
.end method

.method public getMajelClientInfoExtension()Lcom/google/speech/speech/s3/Majel$MajelClientInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3Request;->majelClientInfoExtension_:Lcom/google/speech/speech/s3/Majel$MajelClientInfo;

    return-object v0
.end method

.method public getMajelServiceRequestExtension()Lcom/google/speech/speech/s3/Majel$MajelServiceRequest;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3Request;->majelServiceRequestExtension_:Lcom/google/speech/speech/s3/Majel$MajelServiceRequest;

    return-object v0
.end method

.method public getMobileUserInfoExtension()Lcom/google/speech/s3/MobileUser$MobileUserInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3Request;->mobileUserInfoExtension_:Lcom/google/speech/s3/MobileUser$MobileUserInfo;

    return-object v0
.end method

.method public getPinholeParamsExtension()Lcom/google/speech/s3/PinholeStream$PinholeParams;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3Request;->pinholeParamsExtension_:Lcom/google/speech/s3/PinholeStream$PinholeParams;

    return-object v0
.end method

.method public getPinholeTtsBridgeParamsExtension()Lcom/google/speech/s3/PinholeStream$PinholeTtsBridgeParams;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3Request;->pinholeTtsBridgeParamsExtension_:Lcom/google/speech/s3/PinholeStream$PinholeTtsBridgeParams;

    return-object v0
.end method

.method public getS3AudioDataExtension()Lcom/google/speech/s3/S3$S3AudioData;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3Request;->s3AudioDataExtension_:Lcom/google/speech/s3/S3$S3AudioData;

    return-object v0
.end method

.method public getS3AudioInfoExtension()Lcom/google/speech/s3/S3$S3AudioInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3Request;->s3AudioInfoExtension_:Lcom/google/speech/s3/S3$S3AudioInfo;

    return-object v0
.end method

.method public getS3ClientInfoExtension()Lcom/google/speech/s3/S3$S3ClientInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3Request;->s3ClientInfoExtension_:Lcom/google/speech/s3/S3$S3ClientInfo;

    return-object v0
.end method

.method public getS3ConnectionInfoExtension()Lcom/google/speech/s3/S3$S3ConnectionInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3Request;->s3ConnectionInfoExtension_:Lcom/google/speech/s3/S3$S3ConnectionInfo;

    return-object v0
.end method

.method public getS3ExperimentInfoExtension()Lcom/google/speech/s3/S3$S3ExperimentInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3Request;->s3ExperimentInfoExtension_:Lcom/google/speech/s3/S3$S3ExperimentInfo;

    return-object v0
.end method

.method public getS3RecognizerInfoExtension()Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3Request;->s3RecognizerInfoExtension_:Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;

    return-object v0
.end method

.method public getS3SessionInfoExtension()Lcom/google/speech/s3/S3$S3SessionInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3Request;->s3SessionInfoExtension_:Lcom/google/speech/s3/S3$S3SessionInfo;

    return-object v0
.end method

.method public getS3UserInfoExtension()Lcom/google/speech/s3/S3$S3UserInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3Request;->s3UserInfoExtension_:Lcom/google/speech/s3/S3$S3UserInfo;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->hasService()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->getService()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->hasLoggingEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->getLoggingEnabled()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->hasEndOfData()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->getEndOfData()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->hasDebuggingEnabled()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->getDebuggingEnabled()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->hasS3UserInfoExtension()Z

    move-result v1

    if-eqz v1, :cond_4

    const v1, 0x47888

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->getS3UserInfoExtension()Lcom/google/speech/s3/S3$S3UserInfo;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->hasS3AudioInfoExtension()Z

    move-result v1

    if-eqz v1, :cond_5

    const v1, 0x478ec

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->getS3AudioInfoExtension()Lcom/google/speech/s3/S3$S3AudioInfo;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->hasS3AudioDataExtension()Z

    move-result v1

    if-eqz v1, :cond_6

    const v1, 0x478ed

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->getS3AudioDataExtension()Lcom/google/speech/s3/S3$S3AudioData;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->hasS3ClientInfoExtension()Z

    move-result v1

    if-eqz v1, :cond_7

    const v1, 0x47c70

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->getS3ClientInfoExtension()Lcom/google/speech/s3/S3$S3ClientInfo;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->hasS3RecognizerInfoExtension()Z

    move-result v1

    if-eqz v1, :cond_8

    const v1, 0x47e64

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->getS3RecognizerInfoExtension()Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->hasMobileUserInfoExtension()Z

    move-result v1

    if-eqz v1, :cond_9

    const v1, 0x1a09496

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->getMobileUserInfoExtension()Lcom/google/speech/s3/MobileUser$MobileUserInfo;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->hasS3SessionInfoExtension()Z

    move-result v1

    if-eqz v1, :cond_a

    const v1, 0x1a27214

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->getS3SessionInfoExtension()Lcom/google/speech/s3/S3$S3SessionInfo;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->hasClientLogRequestExtension()Z

    move-result v1

    if-eqz v1, :cond_b

    const v1, 0x1a833e7

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->getClientLogRequestExtension()Lcom/google/speech/s3/ClientLogRequestProto$ClientLogRequest;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->hasTtsServiceRequestExtension()Z

    move-result v1

    if-eqz v1, :cond_c

    const v1, 0x1a837ac

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->getTtsServiceRequestExtension()Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_c
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->hasMajelServiceRequestExtension()Z

    move-result v1

    if-eqz v1, :cond_d

    const v1, 0x1a8ba0c

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->getMajelServiceRequestExtension()Lcom/google/speech/speech/s3/Majel$MajelServiceRequest;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_d
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->hasMajelClientInfoExtension()Z

    move-result v1

    if-eqz v1, :cond_e

    const v1, 0x1c5b670

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->getMajelClientInfoExtension()Lcom/google/speech/speech/s3/Majel$MajelClientInfo;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_e
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->hasS3ExperimentInfoExtension()Z

    move-result v1

    if-eqz v1, :cond_f

    const v1, 0x1fcfef8

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->getS3ExperimentInfoExtension()Lcom/google/speech/s3/S3$S3ExperimentInfo;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_f
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->hasPinholeParamsExtension()Z

    move-result v1

    if-eqz v1, :cond_10

    const v1, 0x20c2c16

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->getPinholeParamsExtension()Lcom/google/speech/s3/PinholeStream$PinholeParams;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_10
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->hasS3ConnectionInfoExtension()Z

    move-result v1

    if-eqz v1, :cond_11

    const v1, 0x20f3b67

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->getS3ConnectionInfoExtension()Lcom/google/speech/s3/S3$S3ConnectionInfo;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_11
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->hasSoundSearchInfoExtension()Z

    move-result v1

    if-eqz v1, :cond_12

    const v1, 0x21b6d89

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->getSoundSearchInfoExtension()Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_12
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->hasGogglesClientLogExtension()Z

    move-result v1

    if-eqz v1, :cond_13

    const v1, 0x21bd8a3

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->getGogglesClientLogExtension()Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesClientLog;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_13
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->hasGogglesS3SessionOptionsExtension()Z

    move-result v1

    if-eqz v1, :cond_14

    const v1, 0x21bdb9a

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->getGogglesS3SessionOptionsExtension()Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_14
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->hasGogglesStreamRequestExtension()Z

    move-result v1

    if-eqz v1, :cond_15

    const v1, 0x21ec2a6

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->getGogglesStreamRequestExtension()Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_15
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->hasPinholeTtsBridgeParamsExtension()Z

    move-result v1

    if-eqz v1, :cond_16

    const v1, 0x270b683

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->getPinholeTtsBridgeParamsExtension()Lcom/google/speech/s3/PinholeStream$PinholeTtsBridgeParams;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_16
    iput v0, p0, Lcom/google/speech/s3/S3$S3Request;->cachedSize:I

    return v0
.end method

.method public getService()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3Request;->service_:Ljava/lang/String;

    return-object v0
.end method

.method public getSoundSearchInfoExtension()Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3Request;->soundSearchInfoExtension_:Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;

    return-object v0
.end method

.method public getTtsServiceRequestExtension()Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/s3/S3$S3Request;->ttsServiceRequestExtension_:Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;

    return-object v0
.end method

.method public hasClientLogRequestExtension()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->hasClientLogRequestExtension:Z

    return v0
.end method

.method public hasDebuggingEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->hasDebuggingEnabled:Z

    return v0
.end method

.method public hasEndOfData()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->hasEndOfData:Z

    return v0
.end method

.method public hasGogglesClientLogExtension()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->hasGogglesClientLogExtension:Z

    return v0
.end method

.method public hasGogglesS3SessionOptionsExtension()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->hasGogglesS3SessionOptionsExtension:Z

    return v0
.end method

.method public hasGogglesStreamRequestExtension()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->hasGogglesStreamRequestExtension:Z

    return v0
.end method

.method public hasLoggingEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->hasLoggingEnabled:Z

    return v0
.end method

.method public hasMajelClientInfoExtension()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->hasMajelClientInfoExtension:Z

    return v0
.end method

.method public hasMajelServiceRequestExtension()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->hasMajelServiceRequestExtension:Z

    return v0
.end method

.method public hasMobileUserInfoExtension()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->hasMobileUserInfoExtension:Z

    return v0
.end method

.method public hasPinholeParamsExtension()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->hasPinholeParamsExtension:Z

    return v0
.end method

.method public hasPinholeTtsBridgeParamsExtension()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->hasPinholeTtsBridgeParamsExtension:Z

    return v0
.end method

.method public hasS3AudioDataExtension()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->hasS3AudioDataExtension:Z

    return v0
.end method

.method public hasS3AudioInfoExtension()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->hasS3AudioInfoExtension:Z

    return v0
.end method

.method public hasS3ClientInfoExtension()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->hasS3ClientInfoExtension:Z

    return v0
.end method

.method public hasS3ConnectionInfoExtension()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->hasS3ConnectionInfoExtension:Z

    return v0
.end method

.method public hasS3ExperimentInfoExtension()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->hasS3ExperimentInfoExtension:Z

    return v0
.end method

.method public hasS3RecognizerInfoExtension()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->hasS3RecognizerInfoExtension:Z

    return v0
.end method

.method public hasS3SessionInfoExtension()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->hasS3SessionInfoExtension:Z

    return v0
.end method

.method public hasS3UserInfoExtension()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->hasS3UserInfoExtension:Z

    return v0
.end method

.method public hasService()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->hasService:Z

    return v0
.end method

.method public hasSoundSearchInfoExtension()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->hasSoundSearchInfoExtension:Z

    return v0
.end method

.method public hasTtsServiceRequestExtension()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->hasTtsServiceRequestExtension:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/speech/s3/S3$S3Request;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/s3/S3$S3Request;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/s3/S3$S3Request;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/speech/s3/S3$S3Request;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/speech/s3/S3$S3Request;->setService(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3Request;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/s3/S3$S3Request;->setLoggingEnabled(Z)Lcom/google/speech/s3/S3$S3Request;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/s3/S3$S3Request;->setEndOfData(Z)Lcom/google/speech/s3/S3$S3Request;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/s3/S3$S3Request;->setDebuggingEnabled(Z)Lcom/google/speech/s3/S3$S3Request;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/speech/s3/S3$S3UserInfo;

    invoke-direct {v1}, Lcom/google/speech/s3/S3$S3UserInfo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3Request;->setS3UserInfoExtension(Lcom/google/speech/s3/S3$S3UserInfo;)Lcom/google/speech/s3/S3$S3Request;

    goto :goto_0

    :sswitch_6
    new-instance v1, Lcom/google/speech/s3/S3$S3AudioInfo;

    invoke-direct {v1}, Lcom/google/speech/s3/S3$S3AudioInfo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3Request;->setS3AudioInfoExtension(Lcom/google/speech/s3/S3$S3AudioInfo;)Lcom/google/speech/s3/S3$S3Request;

    goto :goto_0

    :sswitch_7
    new-instance v1, Lcom/google/speech/s3/S3$S3AudioData;

    invoke-direct {v1}, Lcom/google/speech/s3/S3$S3AudioData;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3Request;->setS3AudioDataExtension(Lcom/google/speech/s3/S3$S3AudioData;)Lcom/google/speech/s3/S3$S3Request;

    goto :goto_0

    :sswitch_8
    new-instance v1, Lcom/google/speech/s3/S3$S3ClientInfo;

    invoke-direct {v1}, Lcom/google/speech/s3/S3$S3ClientInfo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3Request;->setS3ClientInfoExtension(Lcom/google/speech/s3/S3$S3ClientInfo;)Lcom/google/speech/s3/S3$S3Request;

    goto :goto_0

    :sswitch_9
    new-instance v1, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;

    invoke-direct {v1}, Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3Request;->setS3RecognizerInfoExtension(Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;)Lcom/google/speech/s3/S3$S3Request;

    goto :goto_0

    :sswitch_a
    new-instance v1, Lcom/google/speech/s3/MobileUser$MobileUserInfo;

    invoke-direct {v1}, Lcom/google/speech/s3/MobileUser$MobileUserInfo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3Request;->setMobileUserInfoExtension(Lcom/google/speech/s3/MobileUser$MobileUserInfo;)Lcom/google/speech/s3/S3$S3Request;

    goto :goto_0

    :sswitch_b
    new-instance v1, Lcom/google/speech/s3/S3$S3SessionInfo;

    invoke-direct {v1}, Lcom/google/speech/s3/S3$S3SessionInfo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3Request;->setS3SessionInfoExtension(Lcom/google/speech/s3/S3$S3SessionInfo;)Lcom/google/speech/s3/S3$S3Request;

    goto/16 :goto_0

    :sswitch_c
    new-instance v1, Lcom/google/speech/s3/ClientLogRequestProto$ClientLogRequest;

    invoke-direct {v1}, Lcom/google/speech/s3/ClientLogRequestProto$ClientLogRequest;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3Request;->setClientLogRequestExtension(Lcom/google/speech/s3/ClientLogRequestProto$ClientLogRequest;)Lcom/google/speech/s3/S3$S3Request;

    goto/16 :goto_0

    :sswitch_d
    new-instance v1, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;

    invoke-direct {v1}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3Request;->setTtsServiceRequestExtension(Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;)Lcom/google/speech/s3/S3$S3Request;

    goto/16 :goto_0

    :sswitch_e
    new-instance v1, Lcom/google/speech/speech/s3/Majel$MajelServiceRequest;

    invoke-direct {v1}, Lcom/google/speech/speech/s3/Majel$MajelServiceRequest;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3Request;->setMajelServiceRequestExtension(Lcom/google/speech/speech/s3/Majel$MajelServiceRequest;)Lcom/google/speech/s3/S3$S3Request;

    goto/16 :goto_0

    :sswitch_f
    new-instance v1, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;

    invoke-direct {v1}, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3Request;->setMajelClientInfoExtension(Lcom/google/speech/speech/s3/Majel$MajelClientInfo;)Lcom/google/speech/s3/S3$S3Request;

    goto/16 :goto_0

    :sswitch_10
    new-instance v1, Lcom/google/speech/s3/S3$S3ExperimentInfo;

    invoke-direct {v1}, Lcom/google/speech/s3/S3$S3ExperimentInfo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3Request;->setS3ExperimentInfoExtension(Lcom/google/speech/s3/S3$S3ExperimentInfo;)Lcom/google/speech/s3/S3$S3Request;

    goto/16 :goto_0

    :sswitch_11
    new-instance v1, Lcom/google/speech/s3/PinholeStream$PinholeParams;

    invoke-direct {v1}, Lcom/google/speech/s3/PinholeStream$PinholeParams;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3Request;->setPinholeParamsExtension(Lcom/google/speech/s3/PinholeStream$PinholeParams;)Lcom/google/speech/s3/S3$S3Request;

    goto/16 :goto_0

    :sswitch_12
    new-instance v1, Lcom/google/speech/s3/S3$S3ConnectionInfo;

    invoke-direct {v1}, Lcom/google/speech/s3/S3$S3ConnectionInfo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3Request;->setS3ConnectionInfoExtension(Lcom/google/speech/s3/S3$S3ConnectionInfo;)Lcom/google/speech/s3/S3$S3Request;

    goto/16 :goto_0

    :sswitch_13
    new-instance v1, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;

    invoke-direct {v1}, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3Request;->setSoundSearchInfoExtension(Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;)Lcom/google/speech/s3/S3$S3Request;

    goto/16 :goto_0

    :sswitch_14
    new-instance v1, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesClientLog;

    invoke-direct {v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesClientLog;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3Request;->setGogglesClientLogExtension(Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesClientLog;)Lcom/google/speech/s3/S3$S3Request;

    goto/16 :goto_0

    :sswitch_15
    new-instance v1, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;

    invoke-direct {v1}, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3Request;->setGogglesS3SessionOptionsExtension(Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;)Lcom/google/speech/s3/S3$S3Request;

    goto/16 :goto_0

    :sswitch_16
    new-instance v1, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;

    invoke-direct {v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3Request;->setGogglesStreamRequestExtension(Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;)Lcom/google/speech/s3/S3$S3Request;

    goto/16 :goto_0

    :sswitch_17
    new-instance v1, Lcom/google/speech/s3/PinholeStream$PinholeTtsBridgeParams;

    invoke-direct {v1}, Lcom/google/speech/s3/PinholeStream$PinholeTtsBridgeParams;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/s3/S3$S3Request;->setPinholeTtsBridgeParamsExtension(Lcom/google/speech/s3/PinholeStream$PinholeTtsBridgeParams;)Lcom/google/speech/s3/S3$S3Request;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x23c442 -> :sswitch_5
        0x23c762 -> :sswitch_6
        0x23c76a -> :sswitch_7
        0x23e382 -> :sswitch_8
        0x23f322 -> :sswitch_9
        0xd04a4b2 -> :sswitch_a
        0xd1390a2 -> :sswitch_b
        0xd419f3a -> :sswitch_c
        0xd41bd62 -> :sswitch_d
        0xd45d062 -> :sswitch_e
        0xe2db382 -> :sswitch_f
        0xfe7f7c2 -> :sswitch_10
        0x106160b2 -> :sswitch_11
        0x1079db3a -> :sswitch_12
        0x10db6c4a -> :sswitch_13
        0x10dec51a -> :sswitch_14
        0x10dedcd2 -> :sswitch_15
        0x10f61532 -> :sswitch_16
        0x1385b41a -> :sswitch_17
    .end sparse-switch
.end method

.method public setClientLogRequestExtension(Lcom/google/speech/s3/ClientLogRequestProto$ClientLogRequest;)Lcom/google/speech/s3/S3$S3Request;
    .locals 1
    .param p1    # Lcom/google/speech/s3/ClientLogRequestProto$ClientLogRequest;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->hasClientLogRequestExtension:Z

    iput-object p1, p0, Lcom/google/speech/s3/S3$S3Request;->clientLogRequestExtension_:Lcom/google/speech/s3/ClientLogRequestProto$ClientLogRequest;

    return-object p0
.end method

.method public setDebuggingEnabled(Z)Lcom/google/speech/s3/S3$S3Request;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->hasDebuggingEnabled:Z

    iput-boolean p1, p0, Lcom/google/speech/s3/S3$S3Request;->debuggingEnabled_:Z

    return-object p0
.end method

.method public setEndOfData(Z)Lcom/google/speech/s3/S3$S3Request;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->hasEndOfData:Z

    iput-boolean p1, p0, Lcom/google/speech/s3/S3$S3Request;->endOfData_:Z

    return-object p0
.end method

.method public setGogglesClientLogExtension(Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesClientLog;)Lcom/google/speech/s3/S3$S3Request;
    .locals 1
    .param p1    # Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesClientLog;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->hasGogglesClientLogExtension:Z

    iput-object p1, p0, Lcom/google/speech/s3/S3$S3Request;->gogglesClientLogExtension_:Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesClientLog;

    return-object p0
.end method

.method public setGogglesS3SessionOptionsExtension(Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;)Lcom/google/speech/s3/S3$S3Request;
    .locals 1
    .param p1    # Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->hasGogglesS3SessionOptionsExtension:Z

    iput-object p1, p0, Lcom/google/speech/s3/S3$S3Request;->gogglesS3SessionOptionsExtension_:Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;

    return-object p0
.end method

.method public setGogglesStreamRequestExtension(Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;)Lcom/google/speech/s3/S3$S3Request;
    .locals 1
    .param p1    # Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->hasGogglesStreamRequestExtension:Z

    iput-object p1, p0, Lcom/google/speech/s3/S3$S3Request;->gogglesStreamRequestExtension_:Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;

    return-object p0
.end method

.method public setLoggingEnabled(Z)Lcom/google/speech/s3/S3$S3Request;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->hasLoggingEnabled:Z

    iput-boolean p1, p0, Lcom/google/speech/s3/S3$S3Request;->loggingEnabled_:Z

    return-object p0
.end method

.method public setMajelClientInfoExtension(Lcom/google/speech/speech/s3/Majel$MajelClientInfo;)Lcom/google/speech/s3/S3$S3Request;
    .locals 1
    .param p1    # Lcom/google/speech/speech/s3/Majel$MajelClientInfo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->hasMajelClientInfoExtension:Z

    iput-object p1, p0, Lcom/google/speech/s3/S3$S3Request;->majelClientInfoExtension_:Lcom/google/speech/speech/s3/Majel$MajelClientInfo;

    return-object p0
.end method

.method public setMajelServiceRequestExtension(Lcom/google/speech/speech/s3/Majel$MajelServiceRequest;)Lcom/google/speech/s3/S3$S3Request;
    .locals 1
    .param p1    # Lcom/google/speech/speech/s3/Majel$MajelServiceRequest;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->hasMajelServiceRequestExtension:Z

    iput-object p1, p0, Lcom/google/speech/s3/S3$S3Request;->majelServiceRequestExtension_:Lcom/google/speech/speech/s3/Majel$MajelServiceRequest;

    return-object p0
.end method

.method public setMobileUserInfoExtension(Lcom/google/speech/s3/MobileUser$MobileUserInfo;)Lcom/google/speech/s3/S3$S3Request;
    .locals 1
    .param p1    # Lcom/google/speech/s3/MobileUser$MobileUserInfo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->hasMobileUserInfoExtension:Z

    iput-object p1, p0, Lcom/google/speech/s3/S3$S3Request;->mobileUserInfoExtension_:Lcom/google/speech/s3/MobileUser$MobileUserInfo;

    return-object p0
.end method

.method public setPinholeParamsExtension(Lcom/google/speech/s3/PinholeStream$PinholeParams;)Lcom/google/speech/s3/S3$S3Request;
    .locals 1
    .param p1    # Lcom/google/speech/s3/PinholeStream$PinholeParams;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->hasPinholeParamsExtension:Z

    iput-object p1, p0, Lcom/google/speech/s3/S3$S3Request;->pinholeParamsExtension_:Lcom/google/speech/s3/PinholeStream$PinholeParams;

    return-object p0
.end method

.method public setPinholeTtsBridgeParamsExtension(Lcom/google/speech/s3/PinholeStream$PinholeTtsBridgeParams;)Lcom/google/speech/s3/S3$S3Request;
    .locals 1
    .param p1    # Lcom/google/speech/s3/PinholeStream$PinholeTtsBridgeParams;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->hasPinholeTtsBridgeParamsExtension:Z

    iput-object p1, p0, Lcom/google/speech/s3/S3$S3Request;->pinholeTtsBridgeParamsExtension_:Lcom/google/speech/s3/PinholeStream$PinholeTtsBridgeParams;

    return-object p0
.end method

.method public setS3AudioDataExtension(Lcom/google/speech/s3/S3$S3AudioData;)Lcom/google/speech/s3/S3$S3Request;
    .locals 1
    .param p1    # Lcom/google/speech/s3/S3$S3AudioData;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->hasS3AudioDataExtension:Z

    iput-object p1, p0, Lcom/google/speech/s3/S3$S3Request;->s3AudioDataExtension_:Lcom/google/speech/s3/S3$S3AudioData;

    return-object p0
.end method

.method public setS3AudioInfoExtension(Lcom/google/speech/s3/S3$S3AudioInfo;)Lcom/google/speech/s3/S3$S3Request;
    .locals 1
    .param p1    # Lcom/google/speech/s3/S3$S3AudioInfo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->hasS3AudioInfoExtension:Z

    iput-object p1, p0, Lcom/google/speech/s3/S3$S3Request;->s3AudioInfoExtension_:Lcom/google/speech/s3/S3$S3AudioInfo;

    return-object p0
.end method

.method public setS3ClientInfoExtension(Lcom/google/speech/s3/S3$S3ClientInfo;)Lcom/google/speech/s3/S3$S3Request;
    .locals 1
    .param p1    # Lcom/google/speech/s3/S3$S3ClientInfo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->hasS3ClientInfoExtension:Z

    iput-object p1, p0, Lcom/google/speech/s3/S3$S3Request;->s3ClientInfoExtension_:Lcom/google/speech/s3/S3$S3ClientInfo;

    return-object p0
.end method

.method public setS3ConnectionInfoExtension(Lcom/google/speech/s3/S3$S3ConnectionInfo;)Lcom/google/speech/s3/S3$S3Request;
    .locals 1
    .param p1    # Lcom/google/speech/s3/S3$S3ConnectionInfo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->hasS3ConnectionInfoExtension:Z

    iput-object p1, p0, Lcom/google/speech/s3/S3$S3Request;->s3ConnectionInfoExtension_:Lcom/google/speech/s3/S3$S3ConnectionInfo;

    return-object p0
.end method

.method public setS3ExperimentInfoExtension(Lcom/google/speech/s3/S3$S3ExperimentInfo;)Lcom/google/speech/s3/S3$S3Request;
    .locals 1
    .param p1    # Lcom/google/speech/s3/S3$S3ExperimentInfo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->hasS3ExperimentInfoExtension:Z

    iput-object p1, p0, Lcom/google/speech/s3/S3$S3Request;->s3ExperimentInfoExtension_:Lcom/google/speech/s3/S3$S3ExperimentInfo;

    return-object p0
.end method

.method public setS3RecognizerInfoExtension(Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;)Lcom/google/speech/s3/S3$S3Request;
    .locals 1
    .param p1    # Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->hasS3RecognizerInfoExtension:Z

    iput-object p1, p0, Lcom/google/speech/s3/S3$S3Request;->s3RecognizerInfoExtension_:Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;

    return-object p0
.end method

.method public setS3SessionInfoExtension(Lcom/google/speech/s3/S3$S3SessionInfo;)Lcom/google/speech/s3/S3$S3Request;
    .locals 1
    .param p1    # Lcom/google/speech/s3/S3$S3SessionInfo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->hasS3SessionInfoExtension:Z

    iput-object p1, p0, Lcom/google/speech/s3/S3$S3Request;->s3SessionInfoExtension_:Lcom/google/speech/s3/S3$S3SessionInfo;

    return-object p0
.end method

.method public setS3UserInfoExtension(Lcom/google/speech/s3/S3$S3UserInfo;)Lcom/google/speech/s3/S3$S3Request;
    .locals 1
    .param p1    # Lcom/google/speech/s3/S3$S3UserInfo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->hasS3UserInfoExtension:Z

    iput-object p1, p0, Lcom/google/speech/s3/S3$S3Request;->s3UserInfoExtension_:Lcom/google/speech/s3/S3$S3UserInfo;

    return-object p0
.end method

.method public setService(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3Request;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->hasService:Z

    iput-object p1, p0, Lcom/google/speech/s3/S3$S3Request;->service_:Ljava/lang/String;

    return-object p0
.end method

.method public setSoundSearchInfoExtension(Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;)Lcom/google/speech/s3/S3$S3Request;
    .locals 1
    .param p1    # Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->hasSoundSearchInfoExtension:Z

    iput-object p1, p0, Lcom/google/speech/s3/S3$S3Request;->soundSearchInfoExtension_:Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;

    return-object p0
.end method

.method public setTtsServiceRequestExtension(Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;)Lcom/google/speech/s3/S3$S3Request;
    .locals 1
    .param p1    # Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/s3/S3$S3Request;->hasTtsServiceRequestExtension:Z

    iput-object p1, p0, Lcom/google/speech/s3/S3$S3Request;->ttsServiceRequestExtension_:Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->hasService()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->getService()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->hasLoggingEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->getLoggingEnabled()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->hasEndOfData()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->getEndOfData()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->hasDebuggingEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->getDebuggingEnabled()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->hasS3UserInfoExtension()Z

    move-result v0

    if-eqz v0, :cond_4

    const v0, 0x47888

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->getS3UserInfoExtension()Lcom/google/speech/s3/S3$S3UserInfo;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->hasS3AudioInfoExtension()Z

    move-result v0

    if-eqz v0, :cond_5

    const v0, 0x478ec

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->getS3AudioInfoExtension()Lcom/google/speech/s3/S3$S3AudioInfo;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->hasS3AudioDataExtension()Z

    move-result v0

    if-eqz v0, :cond_6

    const v0, 0x478ed

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->getS3AudioDataExtension()Lcom/google/speech/s3/S3$S3AudioData;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->hasS3ClientInfoExtension()Z

    move-result v0

    if-eqz v0, :cond_7

    const v0, 0x47c70

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->getS3ClientInfoExtension()Lcom/google/speech/s3/S3$S3ClientInfo;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->hasS3RecognizerInfoExtension()Z

    move-result v0

    if-eqz v0, :cond_8

    const v0, 0x47e64

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->getS3RecognizerInfoExtension()Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->hasMobileUserInfoExtension()Z

    move-result v0

    if-eqz v0, :cond_9

    const v0, 0x1a09496

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->getMobileUserInfoExtension()Lcom/google/speech/s3/MobileUser$MobileUserInfo;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->hasS3SessionInfoExtension()Z

    move-result v0

    if-eqz v0, :cond_a

    const v0, 0x1a27214

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->getS3SessionInfoExtension()Lcom/google/speech/s3/S3$S3SessionInfo;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->hasClientLogRequestExtension()Z

    move-result v0

    if-eqz v0, :cond_b

    const v0, 0x1a833e7

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->getClientLogRequestExtension()Lcom/google/speech/s3/ClientLogRequestProto$ClientLogRequest;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_b
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->hasTtsServiceRequestExtension()Z

    move-result v0

    if-eqz v0, :cond_c

    const v0, 0x1a837ac

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->getTtsServiceRequestExtension()Lcom/google/speech/speech/s3/Synthesis$TtsServiceRequest;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_c
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->hasMajelServiceRequestExtension()Z

    move-result v0

    if-eqz v0, :cond_d

    const v0, 0x1a8ba0c

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->getMajelServiceRequestExtension()Lcom/google/speech/speech/s3/Majel$MajelServiceRequest;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_d
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->hasMajelClientInfoExtension()Z

    move-result v0

    if-eqz v0, :cond_e

    const v0, 0x1c5b670

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->getMajelClientInfoExtension()Lcom/google/speech/speech/s3/Majel$MajelClientInfo;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_e
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->hasS3ExperimentInfoExtension()Z

    move-result v0

    if-eqz v0, :cond_f

    const v0, 0x1fcfef8

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->getS3ExperimentInfoExtension()Lcom/google/speech/s3/S3$S3ExperimentInfo;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_f
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->hasPinholeParamsExtension()Z

    move-result v0

    if-eqz v0, :cond_10

    const v0, 0x20c2c16

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->getPinholeParamsExtension()Lcom/google/speech/s3/PinholeStream$PinholeParams;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_10
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->hasS3ConnectionInfoExtension()Z

    move-result v0

    if-eqz v0, :cond_11

    const v0, 0x20f3b67

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->getS3ConnectionInfoExtension()Lcom/google/speech/s3/S3$S3ConnectionInfo;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_11
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->hasSoundSearchInfoExtension()Z

    move-result v0

    if-eqz v0, :cond_12

    const v0, 0x21b6d89

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->getSoundSearchInfoExtension()Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_12
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->hasGogglesClientLogExtension()Z

    move-result v0

    if-eqz v0, :cond_13

    const v0, 0x21bd8a3

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->getGogglesClientLogExtension()Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesClientLog;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_13
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->hasGogglesS3SessionOptionsExtension()Z

    move-result v0

    if-eqz v0, :cond_14

    const v0, 0x21bdb9a

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->getGogglesS3SessionOptionsExtension()Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_14
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->hasGogglesStreamRequestExtension()Z

    move-result v0

    if-eqz v0, :cond_15

    const v0, 0x21ec2a6

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->getGogglesStreamRequestExtension()Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_15
    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->hasPinholeTtsBridgeParamsExtension()Z

    move-result v0

    if-eqz v0, :cond_16

    const v0, 0x270b683

    invoke-virtual {p0}, Lcom/google/speech/s3/S3$S3Request;->getPinholeTtsBridgeParamsExtension()Lcom/google/speech/s3/PinholeStream$PinholeTtsBridgeParams;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_16
    return-void
.end method
