.class public final Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Recognizer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/speech/s3/Recognizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RecognizerEvent"
.end annotation


# instance fields
.field private cachedSize:I

.field private endpointerEvent_:Lcom/google/speech/recognizer/api/RecognizerProtos$EndpointerEvent;

.field private hasEndpointerEvent:Z

.field private hasRecognitionEvent:Z

.field private recognitionEvent_:Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-object v0, p0, Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;->recognitionEvent_:Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    iput-object v0, p0, Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;->endpointerEvent_:Lcom/google/speech/recognizer/api/RecognizerProtos$EndpointerEvent;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;->cachedSize:I

    return v0
.end method

.method public getEndpointerEvent()Lcom/google/speech/recognizer/api/RecognizerProtos$EndpointerEvent;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;->endpointerEvent_:Lcom/google/speech/recognizer/api/RecognizerProtos$EndpointerEvent;

    return-object v0
.end method

.method public getRecognitionEvent()Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;->recognitionEvent_:Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;->hasRecognitionEvent()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;->getRecognitionEvent()Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;->hasEndpointerEvent()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;->getEndpointerEvent()Lcom/google/speech/recognizer/api/RecognizerProtos$EndpointerEvent;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iput v0, p0, Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;->cachedSize:I

    return v0
.end method

.method public hasEndpointerEvent()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;->hasEndpointerEvent:Z

    return v0
.end method

.method public hasRecognitionEvent()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;->hasRecognitionEvent:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    invoke-direct {v1}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;->setRecognitionEvent(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/speech/recognizer/api/RecognizerProtos$EndpointerEvent;

    invoke-direct {v1}, Lcom/google/speech/recognizer/api/RecognizerProtos$EndpointerEvent;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;->setEndpointerEvent(Lcom/google/speech/recognizer/api/RecognizerProtos$EndpointerEvent;)Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public setEndpointerEvent(Lcom/google/speech/recognizer/api/RecognizerProtos$EndpointerEvent;)Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;
    .locals 1
    .param p1    # Lcom/google/speech/recognizer/api/RecognizerProtos$EndpointerEvent;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;->hasEndpointerEvent:Z

    iput-object p1, p0, Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;->endpointerEvent_:Lcom/google/speech/recognizer/api/RecognizerProtos$EndpointerEvent;

    return-object p0
.end method

.method public setRecognitionEvent(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;
    .locals 1
    .param p1    # Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;->hasRecognitionEvent:Z

    iput-object p1, p0, Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;->recognitionEvent_:Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;->hasRecognitionEvent()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;->getRecognitionEvent()Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;->hasEndpointerEvent()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;->getEndpointerEvent()Lcom/google/speech/recognizer/api/RecognizerProtos$EndpointerEvent;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    return-void
.end method
