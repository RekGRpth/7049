.class public final Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "SoundSearch.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/speech/speech/s3/SoundSearch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SoundSearchInfo"
.end annotation


# instance fields
.field private cachedSize:I

.field private hasLookupRequest:Z

.field private hasStreamRequest:Z

.field private hasTtsOutputEnabled:Z

.field private lookupRequest_:Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;

.field private streamRequest_:Lcom/google/audio/ears/proto/EarsService$EarsStreamRequest;

.field private ttsOutputEnabled_:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-object v0, p0, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;->lookupRequest_:Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;

    iput-object v0, p0, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;->streamRequest_:Lcom/google/audio/ears/proto/EarsService$EarsStreamRequest;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;->ttsOutputEnabled_:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;->cachedSize:I

    return v0
.end method

.method public getLookupRequest()Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;->lookupRequest_:Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;->hasLookupRequest()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;->getLookupRequest()Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;->hasStreamRequest()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;->getStreamRequest()Lcom/google/audio/ears/proto/EarsService$EarsStreamRequest;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;->hasTtsOutputEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;->getTtsOutputEnabled()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;->cachedSize:I

    return v0
.end method

.method public getStreamRequest()Lcom/google/audio/ears/proto/EarsService$EarsStreamRequest;
    .locals 1

    iget-object v0, p0, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;->streamRequest_:Lcom/google/audio/ears/proto/EarsService$EarsStreamRequest;

    return-object v0
.end method

.method public getTtsOutputEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;->ttsOutputEnabled_:Z

    return v0
.end method

.method public hasLookupRequest()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;->hasLookupRequest:Z

    return v0
.end method

.method public hasStreamRequest()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;->hasStreamRequest:Z

    return v0
.end method

.method public hasTtsOutputEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;->hasTtsOutputEnabled:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;

    invoke-direct {v1}, Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;->setLookupRequest(Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;)Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/audio/ears/proto/EarsService$EarsStreamRequest;

    invoke-direct {v1}, Lcom/google/audio/ears/proto/EarsService$EarsStreamRequest;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;->setStreamRequest(Lcom/google/audio/ears/proto/EarsService$EarsStreamRequest;)Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;->setTtsOutputEnabled(Z)Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public setLookupRequest(Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;)Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;
    .locals 1
    .param p1    # Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;->hasLookupRequest:Z

    iput-object p1, p0, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;->lookupRequest_:Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;

    return-object p0
.end method

.method public setStreamRequest(Lcom/google/audio/ears/proto/EarsService$EarsStreamRequest;)Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;
    .locals 1
    .param p1    # Lcom/google/audio/ears/proto/EarsService$EarsStreamRequest;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;->hasStreamRequest:Z

    iput-object p1, p0, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;->streamRequest_:Lcom/google/audio/ears/proto/EarsService$EarsStreamRequest;

    return-object p0
.end method

.method public setTtsOutputEnabled(Z)Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;->hasTtsOutputEnabled:Z

    iput-boolean p1, p0, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;->ttsOutputEnabled_:Z

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;->hasLookupRequest()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;->getLookupRequest()Lcom/google/audio/ears/proto/EarsService$EarsLookupRequest;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;->hasStreamRequest()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;->getStreamRequest()Lcom/google/audio/ears/proto/EarsService$EarsStreamRequest;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;->hasTtsOutputEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;->getTtsOutputEnabled()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_2
    return-void
.end method
