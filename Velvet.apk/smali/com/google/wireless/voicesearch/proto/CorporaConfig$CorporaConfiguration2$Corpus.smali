.class public final Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "CorporaConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Corpus"
.end annotation


# instance fields
.field private cachedSize:I

.field private corpusIdentifier_:Ljava/lang/String;

.field private googleApiIncludeLocation_:Z

.field private googleApiUrl_:Ljava/lang/String;

.field private hasCorpusIdentifier:Z

.field private hasGoogleApiIncludeLocation:Z

.field private hasGoogleApiUrl:Z

.field private hasIcon:Z

.field private hasMaximumAppVersion:Z

.field private hasMinimumAppVersion:Z

.field private hasName:Z

.field private hasPrefetchPattern:Z

.field private hasRequiresLocation:Z

.field private hasShowCards:Z

.field private hasUrlAuthority:Z

.field private hasUrlPath:Z

.field private hasWebSearchPattern:Z

.field private icon_:Ljava/lang/String;

.field private maximumAppVersion_:I

.field private minimumAppVersion_:I

.field private name_:Ljava/lang/String;

.field private prefetchPattern_:Ljava/lang/String;

.field private queryParams_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Parameter;",
            ">;"
        }
    .end annotation
.end field

.field private requiresLocation_:Z

.field private showCards_:Z

.field private supportedLocale_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private urlAuthority_:Ljava/lang/String;

.field private urlParams_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Parameter;",
            ">;"
        }
    .end annotation
.end field

.field private urlPath_:Ljava/lang/String;

.field private webSearchPattern_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->corpusIdentifier_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->icon_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->name_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->webSearchPattern_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->queryParams_:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->urlPath_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->urlAuthority_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->urlParams_:Ljava/util/List;

    iput-boolean v1, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->googleApiIncludeLocation_:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->googleApiUrl_:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->requiresLocation_:Z

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->supportedLocale_:Ljava/util/List;

    iput-boolean v1, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->showCards_:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->prefetchPattern_:Ljava/lang/String;

    iput v1, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->minimumAppVersion_:I

    iput v1, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->maximumAppVersion_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addQueryParams(Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Parameter;)Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;
    .locals 1
    .param p1    # Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Parameter;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->queryParams_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->queryParams_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->queryParams_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addSupportedLocale(Ljava/lang/String;)Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->supportedLocale_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->supportedLocale_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->supportedLocale_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addUrlParams(Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Parameter;)Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;
    .locals 1
    .param p1    # Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Parameter;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->urlParams_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->urlParams_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->urlParams_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->cachedSize:I

    return v0
.end method

.method public getCorpusIdentifier()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->corpusIdentifier_:Ljava/lang/String;

    return-object v0
.end method

.method public getGoogleApiIncludeLocation()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->googleApiIncludeLocation_:Z

    return v0
.end method

.method public getGoogleApiUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->googleApiUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getIcon()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->icon_:Ljava/lang/String;

    return-object v0
.end method

.method public getMaximumAppVersion()I
    .locals 1

    iget v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->maximumAppVersion_:I

    return v0
.end method

.method public getMinimumAppVersion()I
    .locals 1

    iget v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->minimumAppVersion_:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getPrefetchPattern()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->prefetchPattern_:Ljava/lang/String;

    return-object v0
.end method

.method public getQueryParamsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Parameter;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->queryParams_:Ljava/util/List;

    return-object v0
.end method

.method public getRequiresLocation()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->requiresLocation_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 6

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasCorpusIdentifier()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getCorpusIdentifier()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_0
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasIcon()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getIcon()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_1
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasName()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x3

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_2
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasWebSearchPattern()Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x4

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getWebSearchPattern()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_3
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getQueryParamsList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Parameter;

    const/4 v4, 0x5

    invoke-static {v4, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasUrlPath()Z

    move-result v4

    if-eqz v4, :cond_5

    const/4 v4, 0x6

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getUrlPath()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_5
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasUrlAuthority()Z

    move-result v4

    if-eqz v4, :cond_6

    const/4 v4, 0x7

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getUrlAuthority()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_6
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getUrlParamsList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Parameter;

    const/16 v4, 0x8

    invoke-static {v4, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    goto :goto_1

    :cond_7
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasGoogleApiIncludeLocation()Z

    move-result v4

    if-eqz v4, :cond_8

    const/16 v4, 0x9

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getGoogleApiIncludeLocation()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_8
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasGoogleApiUrl()Z

    move-result v4

    if-eqz v4, :cond_9

    const/16 v4, 0xa

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getGoogleApiUrl()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_9
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasRequiresLocation()Z

    move-result v4

    if-eqz v4, :cond_a

    const/16 v4, 0xb

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getRequiresLocation()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_a
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getSupportedLocaleList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_2

    :cond_b
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getSupportedLocaleList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasShowCards()Z

    move-result v4

    if-eqz v4, :cond_c

    const/16 v4, 0xd

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getShowCards()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_c
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasPrefetchPattern()Z

    move-result v4

    if-eqz v4, :cond_d

    const/16 v4, 0xe

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getPrefetchPattern()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_d
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasMinimumAppVersion()Z

    move-result v4

    if-eqz v4, :cond_e

    const/16 v4, 0xf

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getMinimumAppVersion()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_e
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasMaximumAppVersion()Z

    move-result v4

    if-eqz v4, :cond_f

    const/16 v4, 0x10

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getMaximumAppVersion()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_f
    iput v3, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->cachedSize:I

    return v3
.end method

.method public getShowCards()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->showCards_:Z

    return v0
.end method

.method public getSupportedLocaleList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->supportedLocale_:Ljava/util/List;

    return-object v0
.end method

.method public getUrlAuthority()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->urlAuthority_:Ljava/lang/String;

    return-object v0
.end method

.method public getUrlParamsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Parameter;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->urlParams_:Ljava/util/List;

    return-object v0
.end method

.method public getUrlPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->urlPath_:Ljava/lang/String;

    return-object v0
.end method

.method public getWebSearchPattern()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->webSearchPattern_:Ljava/lang/String;

    return-object v0
.end method

.method public hasCorpusIdentifier()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasCorpusIdentifier:Z

    return v0
.end method

.method public hasGoogleApiIncludeLocation()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasGoogleApiIncludeLocation:Z

    return v0
.end method

.method public hasGoogleApiUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasGoogleApiUrl:Z

    return v0
.end method

.method public hasIcon()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasIcon:Z

    return v0
.end method

.method public hasMaximumAppVersion()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasMaximumAppVersion:Z

    return v0
.end method

.method public hasMinimumAppVersion()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasMinimumAppVersion:Z

    return v0
.end method

.method public hasName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasName:Z

    return v0
.end method

.method public hasPrefetchPattern()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasPrefetchPattern:Z

    return v0
.end method

.method public hasRequiresLocation()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasRequiresLocation:Z

    return v0
.end method

.method public hasShowCards()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasShowCards:Z

    return v0
.end method

.method public hasUrlAuthority()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasUrlAuthority:Z

    return v0
.end method

.method public hasUrlPath()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasUrlPath:Z

    return v0
.end method

.method public hasWebSearchPattern()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasWebSearchPattern:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->setCorpusIdentifier(Ljava/lang/String;)Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->setIcon(Ljava/lang/String;)Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->setName(Ljava/lang/String;)Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->setWebSearchPattern(Ljava/lang/String;)Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Parameter;

    invoke-direct {v1}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Parameter;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->addQueryParams(Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Parameter;)Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->setUrlPath(Ljava/lang/String;)Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->setUrlAuthority(Ljava/lang/String;)Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;

    goto :goto_0

    :sswitch_8
    new-instance v1, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Parameter;

    invoke-direct {v1}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Parameter;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->addUrlParams(Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Parameter;)Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->setGoogleApiIncludeLocation(Z)Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->setGoogleApiUrl(Ljava/lang/String;)Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->setRequiresLocation(Z)Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->addSupportedLocale(Ljava/lang/String;)Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->setShowCards(Z)Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;

    goto :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->setPrefetchPattern(Ljava/lang/String;)Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->setMinimumAppVersion(I)Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->setMaximumAppVersion(I)Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
        0x62 -> :sswitch_c
        0x68 -> :sswitch_d
        0x72 -> :sswitch_e
        0x78 -> :sswitch_f
        0x80 -> :sswitch_10
    .end sparse-switch
.end method

.method public setCorpusIdentifier(Ljava/lang/String;)Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasCorpusIdentifier:Z

    iput-object p1, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->corpusIdentifier_:Ljava/lang/String;

    return-object p0
.end method

.method public setGoogleApiIncludeLocation(Z)Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasGoogleApiIncludeLocation:Z

    iput-boolean p1, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->googleApiIncludeLocation_:Z

    return-object p0
.end method

.method public setGoogleApiUrl(Ljava/lang/String;)Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasGoogleApiUrl:Z

    iput-object p1, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->googleApiUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setIcon(Ljava/lang/String;)Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasIcon:Z

    iput-object p1, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->icon_:Ljava/lang/String;

    return-object p0
.end method

.method public setMaximumAppVersion(I)Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasMaximumAppVersion:Z

    iput p1, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->maximumAppVersion_:I

    return-object p0
.end method

.method public setMinimumAppVersion(I)Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasMinimumAppVersion:Z

    iput p1, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->minimumAppVersion_:I

    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasName:Z

    iput-object p1, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->name_:Ljava/lang/String;

    return-object p0
.end method

.method public setPrefetchPattern(Ljava/lang/String;)Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasPrefetchPattern:Z

    iput-object p1, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->prefetchPattern_:Ljava/lang/String;

    return-object p0
.end method

.method public setRequiresLocation(Z)Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasRequiresLocation:Z

    iput-boolean p1, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->requiresLocation_:Z

    return-object p0
.end method

.method public setShowCards(Z)Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasShowCards:Z

    iput-boolean p1, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->showCards_:Z

    return-object p0
.end method

.method public setUrlAuthority(Ljava/lang/String;)Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasUrlAuthority:Z

    iput-object p1, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->urlAuthority_:Ljava/lang/String;

    return-object p0
.end method

.method public setUrlPath(Ljava/lang/String;)Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasUrlPath:Z

    iput-object p1, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->urlPath_:Ljava/lang/String;

    return-object p0
.end method

.method public setWebSearchPattern(Ljava/lang/String;)Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasWebSearchPattern:Z

    iput-object p1, p0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->webSearchPattern_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasCorpusIdentifier()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getCorpusIdentifier()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasIcon()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getIcon()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasName()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasWebSearchPattern()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getWebSearchPattern()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getQueryParamsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Parameter;

    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasUrlPath()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getUrlPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasUrlAuthority()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getUrlAuthority()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getUrlParamsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Parameter;

    const/16 v2, 0x8

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_1

    :cond_7
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasGoogleApiIncludeLocation()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getGoogleApiIncludeLocation()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasGoogleApiUrl()Z

    move-result v2

    if-eqz v2, :cond_9

    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getGoogleApiUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasRequiresLocation()Z

    move-result v2

    if-eqz v2, :cond_a

    const/16 v2, 0xb

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getRequiresLocation()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getSupportedLocaleList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/16 v2, 0xc

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    goto :goto_2

    :cond_b
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasShowCards()Z

    move-result v2

    if-eqz v2, :cond_c

    const/16 v2, 0xd

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getShowCards()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_c
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasPrefetchPattern()Z

    move-result v2

    if-eqz v2, :cond_d

    const/16 v2, 0xe

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getPrefetchPattern()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_d
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasMinimumAppVersion()Z

    move-result v2

    if-eqz v2, :cond_e

    const/16 v2, 0xf

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getMinimumAppVersion()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_e
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->hasMaximumAppVersion()Z

    move-result v2

    if-eqz v2, :cond_f

    const/16 v2, 0x10

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2$Corpus;->getMaximumAppVersion()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_f
    return-void
.end method
