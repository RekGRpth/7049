.class public final Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "EmbeddedAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/voicesearch/proto/EmbeddedAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EmbeddedActionContact"
.end annotation


# instance fields
.field private cachedSize:I

.field private grammarWeight_:D

.field private hasGrammarWeight:Z

.field private hasMerged:Z

.field private hasSynced:Z

.field private merged_:Z

.field private synced_:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;->grammarWeight_:D

    iput-boolean v2, p0, Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;->synced_:Z

    iput-boolean v2, p0, Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;->merged_:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;->cachedSize:I

    return v0
.end method

.method public getGrammarWeight()D
    .locals 2

    iget-wide v0, p0, Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;->grammarWeight_:D

    return-wide v0
.end method

.method public getMerged()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;->merged_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;->hasGrammarWeight()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;->getGrammarWeight()D

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeDoubleSize(ID)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;->hasSynced()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;->getSynced()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;->hasMerged()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;->getMerged()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;->cachedSize:I

    return v0
.end method

.method public getSynced()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;->synced_:Z

    return v0
.end method

.method public hasGrammarWeight()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;->hasGrammarWeight:Z

    return v0
.end method

.method public hasMerged()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;->hasMerged:Z

    return v0
.end method

.method public hasSynced()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;->hasSynced:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readDouble()D

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;->setGrammarWeight(D)Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;->setSynced(Z)Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;->setMerged(Z)Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public setGrammarWeight(D)Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;
    .locals 1
    .param p1    # D

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;->hasGrammarWeight:Z

    iput-wide p1, p0, Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;->grammarWeight_:D

    return-object p0
.end method

.method public setMerged(Z)Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;->hasMerged:Z

    iput-boolean p1, p0, Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;->merged_:Z

    return-object p0
.end method

.method public setSynced(Z)Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;->hasSynced:Z

    iput-boolean p1, p0, Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;->synced_:Z

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;->hasGrammarWeight()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;->getGrammarWeight()D

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeDouble(ID)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;->hasSynced()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;->getSynced()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;->hasMerged()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;->getMerged()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_2
    return-void
.end method
