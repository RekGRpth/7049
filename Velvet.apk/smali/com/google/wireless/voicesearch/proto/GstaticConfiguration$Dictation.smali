.class public final Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "GstaticConfiguration.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/voicesearch/proto/GstaticConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Dictation"
.end annotation


# instance fields
.field private cachedSize:I

.field private delayBetweenCommittingNewTextMsec_:I

.field private embeddedRecognizerFallbackTimeout_:I

.field private endpointerParams_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;

.field private hasDelayBetweenCommittingNewTextMsec:Z

.field private hasEmbeddedRecognizerFallbackTimeout:Z

.field private hasEndpointerParams:Z

.field private hasLogEditDistanceMaxContiguousChars:Z

.field private hasLogEditDistanceMaxNewCharsPerc:Z

.field private hasMaxSpanLength:Z

.field private hasMaxTotalSpanLength:Z

.field private hasPartialResultMinConfidence:Z

.field private logEditDistanceMaxContiguousChars_:I

.field private logEditDistanceMaxNewCharsPerc_:I

.field private maxSpanLength_:I

.field private maxTotalSpanLength_:I

.field private partialResultMinConfidence_:F


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const/16 v0, 0xa

    iput v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->maxSpanLength_:I

    const/16 v0, 0xfa

    iput v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->maxTotalSpanLength_:I

    const/16 v0, 0xc8

    iput v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->delayBetweenCommittingNewTextMsec_:I

    const v0, 0x3f666666

    iput v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->partialResultMinConfidence_:F

    const/16 v0, 0xf

    iput v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->logEditDistanceMaxContiguousChars_:I

    const/16 v0, 0x32

    iput v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->logEditDistanceMaxNewCharsPerc_:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->endpointerParams_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->embeddedRecognizerFallbackTimeout_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->cachedSize:I

    return v0
.end method

.method public getDelayBetweenCommittingNewTextMsec()I
    .locals 1

    iget v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->delayBetweenCommittingNewTextMsec_:I

    return v0
.end method

.method public getEmbeddedRecognizerFallbackTimeout()I
    .locals 1

    iget v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->embeddedRecognizerFallbackTimeout_:I

    return v0
.end method

.method public getEndpointerParams()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->endpointerParams_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;

    return-object v0
.end method

.method public getLogEditDistanceMaxContiguousChars()I
    .locals 1

    iget v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->logEditDistanceMaxContiguousChars_:I

    return v0
.end method

.method public getLogEditDistanceMaxNewCharsPerc()I
    .locals 1

    iget v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->logEditDistanceMaxNewCharsPerc_:I

    return v0
.end method

.method public getMaxSpanLength()I
    .locals 1

    iget v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->maxSpanLength_:I

    return v0
.end method

.method public getMaxTotalSpanLength()I
    .locals 1

    iget v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->maxTotalSpanLength_:I

    return v0
.end method

.method public getPartialResultMinConfidence()F
    .locals 1

    iget v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->partialResultMinConfidence_:F

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->hasMaxSpanLength()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->getMaxSpanLength()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->hasMaxTotalSpanLength()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->getMaxTotalSpanLength()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->hasDelayBetweenCommittingNewTextMsec()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->getDelayBetweenCommittingNewTextMsec()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->hasPartialResultMinConfidence()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->getPartialResultMinConfidence()F

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->hasLogEditDistanceMaxContiguousChars()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->getLogEditDistanceMaxContiguousChars()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->hasLogEditDistanceMaxNewCharsPerc()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->getLogEditDistanceMaxNewCharsPerc()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->hasEndpointerParams()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->getEndpointerParams()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->hasEmbeddedRecognizerFallbackTimeout()Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->getEmbeddedRecognizerFallbackTimeout()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iput v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->cachedSize:I

    return v0
.end method

.method public hasDelayBetweenCommittingNewTextMsec()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->hasDelayBetweenCommittingNewTextMsec:Z

    return v0
.end method

.method public hasEmbeddedRecognizerFallbackTimeout()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->hasEmbeddedRecognizerFallbackTimeout:Z

    return v0
.end method

.method public hasEndpointerParams()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->hasEndpointerParams:Z

    return v0
.end method

.method public hasLogEditDistanceMaxContiguousChars()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->hasLogEditDistanceMaxContiguousChars:Z

    return v0
.end method

.method public hasLogEditDistanceMaxNewCharsPerc()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->hasLogEditDistanceMaxNewCharsPerc:Z

    return v0
.end method

.method public hasMaxSpanLength()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->hasMaxSpanLength:Z

    return v0
.end method

.method public hasMaxTotalSpanLength()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->hasMaxTotalSpanLength:Z

    return v0
.end method

.method public hasPartialResultMinConfidence()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->hasPartialResultMinConfidence:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->setMaxSpanLength(I)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->setMaxTotalSpanLength(I)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->setDelayBetweenCommittingNewTextMsec(I)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->setPartialResultMinConfidence(F)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->setLogEditDistanceMaxContiguousChars(I)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->setLogEditDistanceMaxNewCharsPerc(I)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;

    goto :goto_0

    :sswitch_7
    new-instance v1, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;

    invoke-direct {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->setEndpointerParams(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->setEmbeddedRecognizerFallbackTimeout(I)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x25 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method public setDelayBetweenCommittingNewTextMsec(I)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->hasDelayBetweenCommittingNewTextMsec:Z

    iput p1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->delayBetweenCommittingNewTextMsec_:I

    return-object p0
.end method

.method public setEmbeddedRecognizerFallbackTimeout(I)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->hasEmbeddedRecognizerFallbackTimeout:Z

    iput p1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->embeddedRecognizerFallbackTimeout_:I

    return-object p0
.end method

.method public setEndpointerParams(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;
    .locals 1
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->hasEndpointerParams:Z

    iput-object p1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->endpointerParams_:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;

    return-object p0
.end method

.method public setLogEditDistanceMaxContiguousChars(I)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->hasLogEditDistanceMaxContiguousChars:Z

    iput p1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->logEditDistanceMaxContiguousChars_:I

    return-object p0
.end method

.method public setLogEditDistanceMaxNewCharsPerc(I)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->hasLogEditDistanceMaxNewCharsPerc:Z

    iput p1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->logEditDistanceMaxNewCharsPerc_:I

    return-object p0
.end method

.method public setMaxSpanLength(I)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->hasMaxSpanLength:Z

    iput p1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->maxSpanLength_:I

    return-object p0
.end method

.method public setMaxTotalSpanLength(I)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->hasMaxTotalSpanLength:Z

    iput p1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->maxTotalSpanLength_:I

    return-object p0
.end method

.method public setPartialResultMinConfidence(F)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->hasPartialResultMinConfidence:Z

    iput p1, p0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->partialResultMinConfidence_:F

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->hasMaxSpanLength()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->getMaxSpanLength()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->hasMaxTotalSpanLength()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->getMaxTotalSpanLength()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->hasDelayBetweenCommittingNewTextMsec()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->getDelayBetweenCommittingNewTextMsec()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->hasPartialResultMinConfidence()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->getPartialResultMinConfidence()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->hasLogEditDistanceMaxContiguousChars()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->getLogEditDistanceMaxContiguousChars()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->hasLogEditDistanceMaxNewCharsPerc()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->getLogEditDistanceMaxNewCharsPerc()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->hasEndpointerParams()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->getEndpointerParams()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->hasEmbeddedRecognizerFallbackTimeout()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    invoke-virtual {p0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->getEmbeddedRecognizerFallbackTimeout()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_7
    return-void
.end method
