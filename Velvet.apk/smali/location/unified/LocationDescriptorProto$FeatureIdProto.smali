.class public final Llocation/unified/LocationDescriptorProto$FeatureIdProto;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "LocationDescriptorProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Llocation/unified/LocationDescriptorProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FeatureIdProto"
.end annotation


# instance fields
.field private cachedSize:I

.field private cellId_:J

.field private fprint_:J

.field private hasCellId:Z

.field private hasFprint:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const-wide/16 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-wide v0, p0, Llocation/unified/LocationDescriptorProto$FeatureIdProto;->cellId_:J

    iput-wide v0, p0, Llocation/unified/LocationDescriptorProto$FeatureIdProto;->fprint_:J

    const/4 v0, -0x1

    iput v0, p0, Llocation/unified/LocationDescriptorProto$FeatureIdProto;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Llocation/unified/LocationDescriptorProto$FeatureIdProto;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$FeatureIdProto;->getSerializedSize()I

    :cond_0
    iget v0, p0, Llocation/unified/LocationDescriptorProto$FeatureIdProto;->cachedSize:I

    return v0
.end method

.method public getCellId()J
    .locals 2

    iget-wide v0, p0, Llocation/unified/LocationDescriptorProto$FeatureIdProto;->cellId_:J

    return-wide v0
.end method

.method public getFprint()J
    .locals 2

    iget-wide v0, p0, Llocation/unified/LocationDescriptorProto$FeatureIdProto;->fprint_:J

    return-wide v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$FeatureIdProto;->hasCellId()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$FeatureIdProto;->getCellId()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFixed64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$FeatureIdProto;->hasFprint()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$FeatureIdProto;->getFprint()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFixed64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iput v0, p0, Llocation/unified/LocationDescriptorProto$FeatureIdProto;->cachedSize:I

    return v0
.end method

.method public hasCellId()Z
    .locals 1

    iget-boolean v0, p0, Llocation/unified/LocationDescriptorProto$FeatureIdProto;->hasCellId:Z

    return v0
.end method

.method public hasFprint()Z
    .locals 1

    iget-boolean v0, p0, Llocation/unified/LocationDescriptorProto$FeatureIdProto;->hasFprint:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Llocation/unified/LocationDescriptorProto$FeatureIdProto;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Llocation/unified/LocationDescriptorProto$FeatureIdProto;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Llocation/unified/LocationDescriptorProto$FeatureIdProto;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Llocation/unified/LocationDescriptorProto$FeatureIdProto;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFixed64()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Llocation/unified/LocationDescriptorProto$FeatureIdProto;->setCellId(J)Llocation/unified/LocationDescriptorProto$FeatureIdProto;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFixed64()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Llocation/unified/LocationDescriptorProto$FeatureIdProto;->setFprint(J)Llocation/unified/LocationDescriptorProto$FeatureIdProto;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x11 -> :sswitch_2
    .end sparse-switch
.end method

.method public setCellId(J)Llocation/unified/LocationDescriptorProto$FeatureIdProto;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Llocation/unified/LocationDescriptorProto$FeatureIdProto;->hasCellId:Z

    iput-wide p1, p0, Llocation/unified/LocationDescriptorProto$FeatureIdProto;->cellId_:J

    return-object p0
.end method

.method public setFprint(J)Llocation/unified/LocationDescriptorProto$FeatureIdProto;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Llocation/unified/LocationDescriptorProto$FeatureIdProto;->hasFprint:Z

    iput-wide p1, p0, Llocation/unified/LocationDescriptorProto$FeatureIdProto;->fprint_:J

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$FeatureIdProto;->hasCellId()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$FeatureIdProto;->getCellId()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFixed64(IJ)V

    :cond_0
    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$FeatureIdProto;->hasFprint()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$FeatureIdProto;->getFprint()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFixed64(IJ)V

    :cond_1
    return-void
.end method
