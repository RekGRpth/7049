.class public final Llocation/unified/LocationDescriptorProto$LatLng;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "LocationDescriptorProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Llocation/unified/LocationDescriptorProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LatLng"
.end annotation


# instance fields
.field private cachedSize:I

.field private hasLatitudeE7:Z

.field private hasLongitudeE7:Z

.field private latitudeE7_:I

.field private longitudeE7_:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput v0, p0, Llocation/unified/LocationDescriptorProto$LatLng;->latitudeE7_:I

    iput v0, p0, Llocation/unified/LocationDescriptorProto$LatLng;->longitudeE7_:I

    const/4 v0, -0x1

    iput v0, p0, Llocation/unified/LocationDescriptorProto$LatLng;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Llocation/unified/LocationDescriptorProto$LatLng;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LatLng;->getSerializedSize()I

    :cond_0
    iget v0, p0, Llocation/unified/LocationDescriptorProto$LatLng;->cachedSize:I

    return v0
.end method

.method public getLatitudeE7()I
    .locals 1

    iget v0, p0, Llocation/unified/LocationDescriptorProto$LatLng;->latitudeE7_:I

    return v0
.end method

.method public getLongitudeE7()I
    .locals 1

    iget v0, p0, Llocation/unified/LocationDescriptorProto$LatLng;->longitudeE7_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LatLng;->hasLatitudeE7()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LatLng;->getLatitudeE7()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeSFixed32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LatLng;->hasLongitudeE7()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LatLng;->getLongitudeE7()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeSFixed32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iput v0, p0, Llocation/unified/LocationDescriptorProto$LatLng;->cachedSize:I

    return v0
.end method

.method public hasLatitudeE7()Z
    .locals 1

    iget-boolean v0, p0, Llocation/unified/LocationDescriptorProto$LatLng;->hasLatitudeE7:Z

    return v0
.end method

.method public hasLongitudeE7()Z
    .locals 1

    iget-boolean v0, p0, Llocation/unified/LocationDescriptorProto$LatLng;->hasLongitudeE7:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Llocation/unified/LocationDescriptorProto$LatLng;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Llocation/unified/LocationDescriptorProto$LatLng;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Llocation/unified/LocationDescriptorProto$LatLng;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Llocation/unified/LocationDescriptorProto$LatLng;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readSFixed32()I

    move-result v1

    invoke-virtual {p0, v1}, Llocation/unified/LocationDescriptorProto$LatLng;->setLatitudeE7(I)Llocation/unified/LocationDescriptorProto$LatLng;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readSFixed32()I

    move-result v1

    invoke-virtual {p0, v1}, Llocation/unified/LocationDescriptorProto$LatLng;->setLongitudeE7(I)Llocation/unified/LocationDescriptorProto$LatLng;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x15 -> :sswitch_2
    .end sparse-switch
.end method

.method public setLatitudeE7(I)Llocation/unified/LocationDescriptorProto$LatLng;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Llocation/unified/LocationDescriptorProto$LatLng;->hasLatitudeE7:Z

    iput p1, p0, Llocation/unified/LocationDescriptorProto$LatLng;->latitudeE7_:I

    return-object p0
.end method

.method public setLongitudeE7(I)Llocation/unified/LocationDescriptorProto$LatLng;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Llocation/unified/LocationDescriptorProto$LatLng;->hasLongitudeE7:Z

    iput p1, p0, Llocation/unified/LocationDescriptorProto$LatLng;->longitudeE7_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LatLng;->hasLatitudeE7()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LatLng;->getLatitudeE7()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeSFixed32(II)V

    :cond_0
    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LatLng;->hasLongitudeE7()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LatLng;->getLongitudeE7()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeSFixed32(II)V

    :cond_1
    return-void
.end method
