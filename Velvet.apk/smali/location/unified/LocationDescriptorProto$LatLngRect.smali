.class public final Llocation/unified/LocationDescriptorProto$LatLngRect;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "LocationDescriptorProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Llocation/unified/LocationDescriptorProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LatLngRect"
.end annotation


# instance fields
.field private cachedSize:I

.field private hasHi:Z

.field private hasLo:Z

.field private hi_:Llocation/unified/LocationDescriptorProto$LatLng;

.field private lo_:Llocation/unified/LocationDescriptorProto$LatLng;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-object v0, p0, Llocation/unified/LocationDescriptorProto$LatLngRect;->lo_:Llocation/unified/LocationDescriptorProto$LatLng;

    iput-object v0, p0, Llocation/unified/LocationDescriptorProto$LatLngRect;->hi_:Llocation/unified/LocationDescriptorProto$LatLng;

    const/4 v0, -0x1

    iput v0, p0, Llocation/unified/LocationDescriptorProto$LatLngRect;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Llocation/unified/LocationDescriptorProto$LatLngRect;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LatLngRect;->getSerializedSize()I

    :cond_0
    iget v0, p0, Llocation/unified/LocationDescriptorProto$LatLngRect;->cachedSize:I

    return v0
.end method

.method public getHi()Llocation/unified/LocationDescriptorProto$LatLng;
    .locals 1

    iget-object v0, p0, Llocation/unified/LocationDescriptorProto$LatLngRect;->hi_:Llocation/unified/LocationDescriptorProto$LatLng;

    return-object v0
.end method

.method public getLo()Llocation/unified/LocationDescriptorProto$LatLng;
    .locals 1

    iget-object v0, p0, Llocation/unified/LocationDescriptorProto$LatLngRect;->lo_:Llocation/unified/LocationDescriptorProto$LatLng;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LatLngRect;->hasLo()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LatLngRect;->getLo()Llocation/unified/LocationDescriptorProto$LatLng;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LatLngRect;->hasHi()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LatLngRect;->getHi()Llocation/unified/LocationDescriptorProto$LatLng;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iput v0, p0, Llocation/unified/LocationDescriptorProto$LatLngRect;->cachedSize:I

    return v0
.end method

.method public hasHi()Z
    .locals 1

    iget-boolean v0, p0, Llocation/unified/LocationDescriptorProto$LatLngRect;->hasHi:Z

    return v0
.end method

.method public hasLo()Z
    .locals 1

    iget-boolean v0, p0, Llocation/unified/LocationDescriptorProto$LatLngRect;->hasLo:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Llocation/unified/LocationDescriptorProto$LatLngRect;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Llocation/unified/LocationDescriptorProto$LatLngRect;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Llocation/unified/LocationDescriptorProto$LatLngRect;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Llocation/unified/LocationDescriptorProto$LatLngRect;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Llocation/unified/LocationDescriptorProto$LatLng;

    invoke-direct {v1}, Llocation/unified/LocationDescriptorProto$LatLng;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Llocation/unified/LocationDescriptorProto$LatLngRect;->setLo(Llocation/unified/LocationDescriptorProto$LatLng;)Llocation/unified/LocationDescriptorProto$LatLngRect;

    goto :goto_0

    :sswitch_2
    new-instance v1, Llocation/unified/LocationDescriptorProto$LatLng;

    invoke-direct {v1}, Llocation/unified/LocationDescriptorProto$LatLng;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Llocation/unified/LocationDescriptorProto$LatLngRect;->setHi(Llocation/unified/LocationDescriptorProto$LatLng;)Llocation/unified/LocationDescriptorProto$LatLngRect;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public setHi(Llocation/unified/LocationDescriptorProto$LatLng;)Llocation/unified/LocationDescriptorProto$LatLngRect;
    .locals 1
    .param p1    # Llocation/unified/LocationDescriptorProto$LatLng;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Llocation/unified/LocationDescriptorProto$LatLngRect;->hasHi:Z

    iput-object p1, p0, Llocation/unified/LocationDescriptorProto$LatLngRect;->hi_:Llocation/unified/LocationDescriptorProto$LatLng;

    return-object p0
.end method

.method public setLo(Llocation/unified/LocationDescriptorProto$LatLng;)Llocation/unified/LocationDescriptorProto$LatLngRect;
    .locals 1
    .param p1    # Llocation/unified/LocationDescriptorProto$LatLng;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Llocation/unified/LocationDescriptorProto$LatLngRect;->hasLo:Z

    iput-object p1, p0, Llocation/unified/LocationDescriptorProto$LatLngRect;->lo_:Llocation/unified/LocationDescriptorProto$LatLng;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LatLngRect;->hasLo()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LatLngRect;->getLo()Llocation/unified/LocationDescriptorProto$LatLng;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LatLngRect;->hasHi()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Llocation/unified/LocationDescriptorProto$LatLngRect;->getHi()Llocation/unified/LocationDescriptorProto$LatLng;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    return-void
.end method
