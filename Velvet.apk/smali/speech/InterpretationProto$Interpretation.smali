.class public final Lspeech/InterpretationProto$Interpretation;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "InterpretationProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/InterpretationProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Interpretation"
.end annotation


# instance fields
.field private cachedSize:I

.field private confidence_:F

.field private grammarId_:Ljava/lang/String;

.field private hasConfidence:Z

.field private hasGrammarId:Z

.field private slot_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lspeech/InterpretationProto$Slot;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/InterpretationProto$Interpretation;->slot_:Ljava/util/List;

    const/high16 v0, 0x3f800000

    iput v0, p0, Lspeech/InterpretationProto$Interpretation;->confidence_:F

    const-string v0, ""

    iput-object v0, p0, Lspeech/InterpretationProto$Interpretation;->grammarId_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lspeech/InterpretationProto$Interpretation;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addSlot(Lspeech/InterpretationProto$Slot;)Lspeech/InterpretationProto$Interpretation;
    .locals 1
    .param p1    # Lspeech/InterpretationProto$Slot;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lspeech/InterpretationProto$Interpretation;->slot_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lspeech/InterpretationProto$Interpretation;->slot_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lspeech/InterpretationProto$Interpretation;->slot_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lspeech/InterpretationProto$Interpretation;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lspeech/InterpretationProto$Interpretation;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lspeech/InterpretationProto$Interpretation;->cachedSize:I

    return v0
.end method

.method public getConfidence()F
    .locals 1

    iget v0, p0, Lspeech/InterpretationProto$Interpretation;->confidence_:F

    return v0
.end method

.method public getGrammarId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lspeech/InterpretationProto$Interpretation;->grammarId_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lspeech/InterpretationProto$Interpretation;->getSlotList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/InterpretationProto$Slot;

    const/4 v3, 0x1

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lspeech/InterpretationProto$Interpretation;->hasConfidence()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lspeech/InterpretationProto$Interpretation;->getConfidence()F

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lspeech/InterpretationProto$Interpretation;->hasGrammarId()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p0}, Lspeech/InterpretationProto$Interpretation;->getGrammarId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    iput v2, p0, Lspeech/InterpretationProto$Interpretation;->cachedSize:I

    return v2
.end method

.method public getSlot(I)Lspeech/InterpretationProto$Slot;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lspeech/InterpretationProto$Interpretation;->slot_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/InterpretationProto$Slot;

    return-object v0
.end method

.method public getSlotCount()I
    .locals 1

    iget-object v0, p0, Lspeech/InterpretationProto$Interpretation;->slot_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getSlotList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lspeech/InterpretationProto$Slot;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lspeech/InterpretationProto$Interpretation;->slot_:Ljava/util/List;

    return-object v0
.end method

.method public hasConfidence()Z
    .locals 1

    iget-boolean v0, p0, Lspeech/InterpretationProto$Interpretation;->hasConfidence:Z

    return v0
.end method

.method public hasGrammarId()Z
    .locals 1

    iget-boolean v0, p0, Lspeech/InterpretationProto$Interpretation;->hasGrammarId:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lspeech/InterpretationProto$Interpretation;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lspeech/InterpretationProto$Interpretation;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lspeech/InterpretationProto$Interpretation;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lspeech/InterpretationProto$Interpretation;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lspeech/InterpretationProto$Slot;

    invoke-direct {v1}, Lspeech/InterpretationProto$Slot;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lspeech/InterpretationProto$Interpretation;->addSlot(Lspeech/InterpretationProto$Slot;)Lspeech/InterpretationProto$Interpretation;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v2

    invoke-virtual {p0, v2}, Lspeech/InterpretationProto$Interpretation;->setConfidence(F)Lspeech/InterpretationProto$Interpretation;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lspeech/InterpretationProto$Interpretation;->setGrammarId(Ljava/lang/String;)Lspeech/InterpretationProto$Interpretation;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x15 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public setConfidence(F)Lspeech/InterpretationProto$Interpretation;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lspeech/InterpretationProto$Interpretation;->hasConfidence:Z

    iput p1, p0, Lspeech/InterpretationProto$Interpretation;->confidence_:F

    return-object p0
.end method

.method public setGrammarId(Ljava/lang/String;)Lspeech/InterpretationProto$Interpretation;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lspeech/InterpretationProto$Interpretation;->hasGrammarId:Z

    iput-object p1, p0, Lspeech/InterpretationProto$Interpretation;->grammarId_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lspeech/InterpretationProto$Interpretation;->getSlotList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lspeech/InterpretationProto$Slot;

    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lspeech/InterpretationProto$Interpretation;->hasConfidence()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lspeech/InterpretationProto$Interpretation;->getConfidence()F

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_1
    invoke-virtual {p0}, Lspeech/InterpretationProto$Interpretation;->hasGrammarId()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lspeech/InterpretationProto$Interpretation;->getGrammarId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    return-void
.end method
