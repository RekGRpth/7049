.class public final Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "GogglesS3.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lspeech/s3/goggles/GogglesS3;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GogglesS3SessionOptions"
.end annotation


# instance fields
.field private annotation_:Ljava/lang/String;

.field private cachedSize:I

.field private canLogImage_:Z

.field private canLogLocation_:Z

.field private disclosedCapabilities_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private hasAnnotation:Z

.field private hasCanLogImage:Z

.field private hasCanLogLocation:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->annotation_:Ljava/lang/String;

    iput-boolean v1, p0, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->canLogImage_:Z

    iput-boolean v1, p0, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->canLogLocation_:Z

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->disclosedCapabilities_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addDisclosedCapabilities(I)Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->disclosedCapabilities_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->disclosedCapabilities_:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->disclosedCapabilities_:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getAnnotation()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->annotation_:Ljava/lang/String;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->cachedSize:I

    return v0
.end method

.method public getCanLogImage()Z
    .locals 1

    iget-boolean v0, p0, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->canLogImage_:Z

    return v0
.end method

.method public getCanLogLocation()Z
    .locals 1

    iget-boolean v0, p0, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->canLogLocation_:Z

    return v0
.end method

.method public getDisclosedCapabilitiesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->disclosedCapabilities_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    const/4 v3, 0x0

    invoke-virtual {p0}, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->hasAnnotation()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    invoke-virtual {p0}, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->getAnnotation()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_0
    invoke-virtual {p0}, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->hasCanLogImage()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x2

    invoke-virtual {p0}, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->getCanLogImage()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_1
    invoke-virtual {p0}, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->hasCanLogLocation()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x3

    invoke-virtual {p0}, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->getCanLogLocation()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p0}, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->getDisclosedCapabilitiesList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32SizeNoTag(I)I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_0

    :cond_3
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->getDisclosedCapabilitiesList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->cachedSize:I

    return v3
.end method

.method public hasAnnotation()Z
    .locals 1

    iget-boolean v0, p0, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->hasAnnotation:Z

    return v0
.end method

.method public hasCanLogImage()Z
    .locals 1

    iget-boolean v0, p0, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->hasCanLogImage:Z

    return v0
.end method

.method public hasCanLogLocation()Z
    .locals 1

    iget-boolean v0, p0, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->hasCanLogLocation:Z

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->setAnnotation(Ljava/lang/String;)Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->setCanLogImage(Z)Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->setCanLogLocation(Z)Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->addDisclosedCapabilities(I)Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public setAnnotation(Ljava/lang/String;)Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->hasAnnotation:Z

    iput-object p1, p0, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->annotation_:Ljava/lang/String;

    return-object p0
.end method

.method public setCanLogImage(Z)Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->hasCanLogImage:Z

    iput-boolean p1, p0, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->canLogImage_:Z

    return-object p0
.end method

.method public setCanLogLocation(Z)Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->hasCanLogLocation:Z

    iput-boolean p1, p0, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->canLogLocation_:Z

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->hasAnnotation()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->getAnnotation()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->hasCanLogImage()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->getCanLogImage()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_1
    invoke-virtual {p0}, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->hasCanLogLocation()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->getCanLogLocation()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_2
    invoke-virtual {p0}, Lspeech/s3/goggles/GogglesS3$GogglesS3SessionOptions;->getDisclosedCapabilitiesList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    const/4 v2, 0x4

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    goto :goto_0

    :cond_3
    return-void
.end method
