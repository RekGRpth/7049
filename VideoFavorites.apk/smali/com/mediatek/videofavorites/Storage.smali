.class public final Lcom/mediatek/videofavorites/Storage;
.super Ljava/lang/Object;
.source "Storage.java"


# static fields
.field public static final AVAILABLE:J = 0x0L

.field public static final FULL_SDCARD:J = -0x4L

.field public static final LOW_STORAGE_THRESHOLD:J = 0x80000L

.field public static final PREPARING:J = -0x2L

.field private static final TAG:Ljava/lang/String; = "Storage"

.field public static final TRANSCODE_PATH:Ljava/lang/String;

.field public static final TRANSCODE_PATH_BASE:Ljava/lang/String;

.field public static final UNAVAILABLE:J = -0x1L

.field public static final UNKNOWN_SIZE:J = -0x3L


# direct methods
.method static constructor <clinit>()V
    .locals 2

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mediatek/videofavorites/Storage;->TRANSCODE_PATH_BASE:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/mediatek/videofavorites/Storage;->TRANSCODE_PATH_BASE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/VideoFavorite/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mediatek/videofavorites/Storage;->TRANSCODE_PATH:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static checkStorageState(Ljava/lang/String;Landroid/content/Context;)J
    .locals 7
    .param p0    # Ljava/lang/String;
    .param p1    # Landroid/content/Context;

    const-wide/16 v2, -0x1

    invoke-static {p1}, Lcom/mediatek/videofavorites/Storage;->getStorageManager(Landroid/content/Context;)Landroid/os/storage/StorageManager;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-wide v2

    :cond_1
    invoke-virtual {v0, p0}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v4, "Storage"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "External storage state="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", mount point = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/mediatek/videofavorites/Storage;->TRANSCODE_PATH_BASE:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "checking"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-wide/16 v2, -0x2

    goto :goto_0

    :cond_2
    const-string v4, "mounted"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method public static getAvailableSpace(Landroid/content/Context;)J
    .locals 2
    .param p0    # Landroid/content/Context;

    sget-object v0, Lcom/mediatek/videofavorites/Storage;->TRANSCODE_PATH_BASE:Ljava/lang/String;

    invoke-static {v0, p0}, Lcom/mediatek/videofavorites/Storage;->getAvailableSpace(Ljava/lang/String;Landroid/content/Context;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static getAvailableSpace(Ljava/lang/String;Landroid/content/Context;)J
    .locals 9
    .param p0    # Ljava/lang/String;
    .param p1    # Landroid/content/Context;

    invoke-static {p0, p1}, Lcom/mediatek/videofavorites/Storage;->checkStorageState(Ljava/lang/String;Landroid/content/Context;)J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v5, v3, v5

    if-eqz v5, :cond_0

    :goto_0
    return-wide v3

    :cond_0
    new-instance v0, Ljava/io/File;

    sget-object v5, Lcom/mediatek/videofavorites/Storage;->TRANSCODE_PATH:Ljava/lang/String;

    invoke-direct {v0, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->canWrite()Z

    move-result v5

    if-nez v5, :cond_2

    :cond_1
    const-string v5, "Storage"

    const-string v6, "directory create failed"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v3, -0x4

    goto :goto_0

    :cond_2
    :try_start_0
    new-instance v2, Landroid/os/StatFs;

    sget-object v5, Lcom/mediatek/videofavorites/Storage;->TRANSCODE_PATH:Ljava/lang/String;

    invoke-direct {v2, v5}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v5

    int-to-long v5, v5

    invoke-virtual {v2}, Landroid/os/StatFs;->getBlockSize()I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    int-to-long v7, v7

    mul-long v3, v5, v7

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v5, "Storage"

    const-string v6, "Fail to access external storage"

    invoke-static {v5, v6, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const-wide/16 v3, -0x3

    goto :goto_0
.end method

.method private static getStorageManager(Landroid/content/Context;)Landroid/os/storage/StorageManager;
    .locals 1
    .param p0    # Landroid/content/Context;

    const-string v0, "storage"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/storage/StorageManager;

    return-object v0
.end method

.method public static isAvailable(Landroid/content/Context;)Z
    .locals 4
    .param p0    # Landroid/content/Context;

    sget-object v0, Lcom/mediatek/videofavorites/Storage;->TRANSCODE_PATH_BASE:Ljava/lang/String;

    invoke-static {v0, p0}, Lcom/mediatek/videofavorites/Storage;->checkStorageState(Ljava/lang/String;Landroid/content/Context;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
