.class Lcom/mediatek/videofavorites/VideoFavoritesRootView$2;
.super Landroid/content/BroadcastReceiver;
.source "VideoFavoritesRootView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/videofavorites/VideoFavoritesRootView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/videofavorites/VideoFavoritesRootView;


# direct methods
.method constructor <init>(Lcom/mediatek/videofavorites/VideoFavoritesRootView;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView$2;->this$0:Lcom/mediatek/videofavorites/VideoFavoritesRootView;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/16 v6, 0x3e8

    const/4 v5, 0x2

    const/4 v4, 0x1

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "VFRootView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Received intent action="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "com.mediatek.videofavorites.REFRESH"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView$2;->this$0:Lcom/mediatek/videofavorites/VideoFavoritesRootView;

    invoke-static {v1}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->access$500(Lcom/mediatek/videofavorites/VideoFavoritesRootView;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v1, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView$2;->this$0:Lcom/mediatek/videofavorites/VideoFavoritesRootView;

    invoke-static {v1}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->access$500(Lcom/mediatek/videofavorites/VideoFavoritesRootView;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v1, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView$2;->this$0:Lcom/mediatek/videofavorites/VideoFavoritesRootView;

    invoke-static {v1}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->access$600(Lcom/mediatek/videofavorites/VideoFavoritesRootView;)V

    iget-object v1, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView$2;->this$0:Lcom/mediatek/videofavorites/VideoFavoritesRootView;

    invoke-static {v1}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->access$700(Lcom/mediatek/videofavorites/VideoFavoritesRootView;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView$2;->this$0:Lcom/mediatek/videofavorites/VideoFavoritesRootView;

    invoke-static {v1}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->access$800(Lcom/mediatek/videofavorites/VideoFavoritesRootView;)I

    move-result v1

    if-ne v1, v4, :cond_0

    iget-object v1, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView$2;->this$0:Lcom/mediatek/videofavorites/VideoFavoritesRootView;

    const/16 v2, 0x12c

    invoke-virtual {v1, v2}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->startPlayVideoRandomly(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView$2;->this$0:Lcom/mediatek/videofavorites/VideoFavoritesRootView;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->access$702(Lcom/mediatek/videofavorites/VideoFavoritesRootView;Z)Z

    iget-object v1, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView$2;->this$0:Lcom/mediatek/videofavorites/VideoFavoritesRootView;

    invoke-static {v1}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->access$500(Lcom/mediatek/videofavorites/VideoFavoritesRootView;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v1, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView$2;->this$0:Lcom/mediatek/videofavorites/VideoFavoritesRootView;

    invoke-static {v1}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->access$500(Lcom/mediatek/videofavorites/VideoFavoritesRootView;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v1, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView$2;->this$0:Lcom/mediatek/videofavorites/VideoFavoritesRootView;

    invoke-static {v1}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->access$600(Lcom/mediatek/videofavorites/VideoFavoritesRootView;)V

    iget-object v1, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView$2;->this$0:Lcom/mediatek/videofavorites/VideoFavoritesRootView;

    invoke-static {v1, v6}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->access$900(Lcom/mediatek/videofavorites/VideoFavoritesRootView;I)V

    goto :goto_0

    :cond_2
    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView$2;->this$0:Lcom/mediatek/videofavorites/VideoFavoritesRootView;

    invoke-static {v1, v4}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->access$702(Lcom/mediatek/videofavorites/VideoFavoritesRootView;Z)Z

    iget-object v1, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView$2;->this$0:Lcom/mediatek/videofavorites/VideoFavoritesRootView;

    invoke-static {v1, v6}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->access$900(Lcom/mediatek/videofavorites/VideoFavoritesRootView;I)V

    goto :goto_0
.end method
