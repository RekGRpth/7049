.class public Lcom/mediatek/videofavorites/VideoFavoritesRootView;
.super Landroid/widget/RelativeLayout;
.source "VideoFavoritesRootView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/IMTKWidget;


# annotations
.annotation runtime Landroid/widget/RemoteViews$RemoteView;
.end annotation


# static fields
.field private static final DETACHED:I = 0x0

.field private static final DRAG:I = 0x2

.field private static final IDLE:I = 0x1

.field private static final INTERCEPT_THRESHOLD:I = 0xa

.field private static final MAX_ALLOWED_WIDGET_COUNT:I = 0x5

.field private static final MAX_VIDEO_CAN_PLAY:I = 0x2

.field private static final MAX_VIDEO_VIEW:I = 0x4

.field private static final MOVING_OUT:I = 0x3

.field private static final MSG_BROCAST_REFRESH:I = 0x4

.field private static final MSG_PAGE_SWITCH_OUT:I = 0x3

.field private static final MSG_START_NEXT_VIDEO_VIEW:I = 0x2

.field private static final MSG_START_RANDOM_PLAY:I = 0x1

.field private static final PAUSED:I = 0x4

.field private static final STOP_VIDEO_ABRUPT_WHEN_PAGE_SWITCH:Z = false

.field private static final TAG:Ljava/lang/String; = "VFRootView"

.field private static final VIDEO_DELAY_TIME_MS:I = 0xc8

.field private static final VIDEO_DELAY_TIME_MS_RESUME:I = 0x12c

.field private static final VIDEO_DELAY_TIME_MS_SHORT:I = 0x32


# instance fields
.field private mDeleteIconId:I

.field private final mDeleteIcons:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mDeleteModeViewId:I

.field private final mHandler:Landroid/os/Handler;

.field private mHierarchyChangeListener:Landroid/view/ViewGroup$OnHierarchyChangeListener;

.field private mIsDeleteMode:Z

.field private mIsListeningIntent:Z

.field private mIsMounted:Z

.field private mLastY:F

.field private mListView:Landroid/widget/AbsListView;

.field private mNumOfEmptyView:I

.field private mNumOfImgView:I

.field private mRandom:Ljava/util/Random;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mScreenIndex:I

.field private mState:I

.field private final mVideoActive:[Lcom/mediatek/videofavorites/VideoTextureView;

.field private mVideoActiveCount:I

.field private final mVideoCompleteListener:Landroid/media/MediaPlayer$OnCompletionListener;

.field private final mVideoErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

.field private final mVideoViews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/videofavorites/VideoTextureView;",
            ">;"
        }
    .end annotation
.end field

.field private mWidgetId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v1, -0x1

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/mediatek/videofavorites/VideoTextureView;

    iput-object v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mVideoActive:[Lcom/mediatek/videofavorites/VideoTextureView;

    iput v1, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mDeleteModeViewId:I

    iput v1, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mDeleteIconId:I

    new-instance v0, Lcom/mediatek/videofavorites/VideoFavoritesRootView$1;

    invoke-direct {v0, p0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView$1;-><init>(Lcom/mediatek/videofavorites/VideoFavoritesRootView;)V

    iput-object v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mHandler:Landroid/os/Handler;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mIsMounted:Z

    new-instance v0, Lcom/mediatek/videofavorites/VideoFavoritesRootView$2;

    invoke-direct {v0, p0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView$2;-><init>(Lcom/mediatek/videofavorites/VideoFavoritesRootView;)V

    iput-object v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mVideoViews:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mDeleteIcons:Ljava/util/ArrayList;

    new-instance v0, Lcom/mediatek/videofavorites/VideoFavoritesRootView$3;

    invoke-direct {v0, p0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView$3;-><init>(Lcom/mediatek/videofavorites/VideoFavoritesRootView;)V

    iput-object v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mHierarchyChangeListener:Landroid/view/ViewGroup$OnHierarchyChangeListener;

    new-instance v0, Lcom/mediatek/videofavorites/VideoFavoritesRootView$4;

    invoke-direct {v0, p0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView$4;-><init>(Lcom/mediatek/videofavorites/VideoFavoritesRootView;)V

    iput-object v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mVideoCompleteListener:Landroid/media/MediaPlayer$OnCompletionListener;

    new-instance v0, Lcom/mediatek/videofavorites/VideoFavoritesRootView$5;

    invoke-direct {v0, p0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView$5;-><init>(Lcom/mediatek/videofavorites/VideoFavoritesRootView;)V

    iput-object v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mVideoErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v1, -0x1

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/mediatek/videofavorites/VideoTextureView;

    iput-object v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mVideoActive:[Lcom/mediatek/videofavorites/VideoTextureView;

    iput v1, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mDeleteModeViewId:I

    iput v1, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mDeleteIconId:I

    new-instance v0, Lcom/mediatek/videofavorites/VideoFavoritesRootView$1;

    invoke-direct {v0, p0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView$1;-><init>(Lcom/mediatek/videofavorites/VideoFavoritesRootView;)V

    iput-object v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mHandler:Landroid/os/Handler;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mIsMounted:Z

    new-instance v0, Lcom/mediatek/videofavorites/VideoFavoritesRootView$2;

    invoke-direct {v0, p0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView$2;-><init>(Lcom/mediatek/videofavorites/VideoFavoritesRootView;)V

    iput-object v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mVideoViews:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mDeleteIcons:Ljava/util/ArrayList;

    new-instance v0, Lcom/mediatek/videofavorites/VideoFavoritesRootView$3;

    invoke-direct {v0, p0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView$3;-><init>(Lcom/mediatek/videofavorites/VideoFavoritesRootView;)V

    iput-object v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mHierarchyChangeListener:Landroid/view/ViewGroup$OnHierarchyChangeListener;

    new-instance v0, Lcom/mediatek/videofavorites/VideoFavoritesRootView$4;

    invoke-direct {v0, p0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView$4;-><init>(Lcom/mediatek/videofavorites/VideoFavoritesRootView;)V

    iput-object v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mVideoCompleteListener:Landroid/media/MediaPlayer$OnCompletionListener;

    new-instance v0, Lcom/mediatek/videofavorites/VideoFavoritesRootView$5;

    invoke-direct {v0, p0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView$5;-><init>(Lcom/mediatek/videofavorites/VideoFavoritesRootView;)V

    iput-object v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mVideoErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v1, -0x1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/mediatek/videofavorites/VideoTextureView;

    iput-object v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mVideoActive:[Lcom/mediatek/videofavorites/VideoTextureView;

    iput v1, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mDeleteModeViewId:I

    iput v1, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mDeleteIconId:I

    new-instance v0, Lcom/mediatek/videofavorites/VideoFavoritesRootView$1;

    invoke-direct {v0, p0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView$1;-><init>(Lcom/mediatek/videofavorites/VideoFavoritesRootView;)V

    iput-object v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mHandler:Landroid/os/Handler;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mIsMounted:Z

    new-instance v0, Lcom/mediatek/videofavorites/VideoFavoritesRootView$2;

    invoke-direct {v0, p0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView$2;-><init>(Lcom/mediatek/videofavorites/VideoFavoritesRootView;)V

    iput-object v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mVideoViews:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mDeleteIcons:Ljava/util/ArrayList;

    new-instance v0, Lcom/mediatek/videofavorites/VideoFavoritesRootView$3;

    invoke-direct {v0, p0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView$3;-><init>(Lcom/mediatek/videofavorites/VideoFavoritesRootView;)V

    iput-object v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mHierarchyChangeListener:Landroid/view/ViewGroup$OnHierarchyChangeListener;

    new-instance v0, Lcom/mediatek/videofavorites/VideoFavoritesRootView$4;

    invoke-direct {v0, p0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView$4;-><init>(Lcom/mediatek/videofavorites/VideoFavoritesRootView;)V

    iput-object v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mVideoCompleteListener:Landroid/media/MediaPlayer$OnCompletionListener;

    new-instance v0, Lcom/mediatek/videofavorites/VideoFavoritesRootView$5;

    invoke-direct {v0, p0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView$5;-><init>(Lcom/mediatek/videofavorites/VideoFavoritesRootView;)V

    iput-object v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mVideoErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/videofavorites/VideoFavoritesRootView;)Z
    .locals 1
    .param p0    # Lcom/mediatek/videofavorites/VideoFavoritesRootView;

    invoke-direct {p0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->buildVideoViewList()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/mediatek/videofavorites/VideoFavoritesRootView;)I
    .locals 1
    .param p0    # Lcom/mediatek/videofavorites/VideoFavoritesRootView;

    iget v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mNumOfImgView:I

    return v0
.end method

.method static synthetic access$1000(Lcom/mediatek/videofavorites/VideoFavoritesRootView;)V
    .locals 0
    .param p0    # Lcom/mediatek/videofavorites/VideoFavoritesRootView;

    invoke-direct {p0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->restartVideoViews()V

    return-void
.end method

.method static synthetic access$1100(Lcom/mediatek/videofavorites/VideoFavoritesRootView;)[Lcom/mediatek/videofavorites/VideoTextureView;
    .locals 1
    .param p0    # Lcom/mediatek/videofavorites/VideoFavoritesRootView;

    iget-object v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mVideoActive:[Lcom/mediatek/videofavorites/VideoTextureView;

    return-object v0
.end method

.method static synthetic access$1210(Lcom/mediatek/videofavorites/VideoFavoritesRootView;)I
    .locals 2
    .param p0    # Lcom/mediatek/videofavorites/VideoFavoritesRootView;

    iget v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mVideoActiveCount:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mVideoActiveCount:I

    return v0
.end method

.method static synthetic access$200(Lcom/mediatek/videofavorites/VideoFavoritesRootView;)V
    .locals 0
    .param p0    # Lcom/mediatek/videofavorites/VideoFavoritesRootView;

    invoke-direct {p0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->showVideoAndRandomStrart()V

    return-void
.end method

.method static synthetic access$300(Lcom/mediatek/videofavorites/VideoFavoritesRootView;)V
    .locals 0
    .param p0    # Lcom/mediatek/videofavorites/VideoFavoritesRootView;

    invoke-direct {p0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->onPageSwitchOutInternal()V

    return-void
.end method

.method static synthetic access$400(Lcom/mediatek/videofavorites/VideoFavoritesRootView;)V
    .locals 0
    .param p0    # Lcom/mediatek/videofavorites/VideoFavoritesRootView;

    invoke-direct {p0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->sendRefreshBroadcast()V

    return-void
.end method

.method static synthetic access$500(Lcom/mediatek/videofavorites/VideoFavoritesRootView;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/videofavorites/VideoFavoritesRootView;

    iget-object v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$600(Lcom/mediatek/videofavorites/VideoFavoritesRootView;)V
    .locals 0
    .param p0    # Lcom/mediatek/videofavorites/VideoFavoritesRootView;

    invoke-direct {p0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->stopAllVideoView()V

    return-void
.end method

.method static synthetic access$700(Lcom/mediatek/videofavorites/VideoFavoritesRootView;)Z
    .locals 1
    .param p0    # Lcom/mediatek/videofavorites/VideoFavoritesRootView;

    iget-boolean v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mIsMounted:Z

    return v0
.end method

.method static synthetic access$702(Lcom/mediatek/videofavorites/VideoFavoritesRootView;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/videofavorites/VideoFavoritesRootView;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mIsMounted:Z

    return p1
.end method

.method static synthetic access$800(Lcom/mediatek/videofavorites/VideoFavoritesRootView;)I
    .locals 1
    .param p0    # Lcom/mediatek/videofavorites/VideoFavoritesRootView;

    iget v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mState:I

    return v0
.end method

.method static synthetic access$900(Lcom/mediatek/videofavorites/VideoFavoritesRootView;I)V
    .locals 0
    .param p0    # Lcom/mediatek/videofavorites/VideoFavoritesRootView;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->sendRefreshBroadcast(I)V

    return-void
.end method

.method private addVideoToActiveList(Lcom/mediatek/videofavorites/RemoteVideoView;)V
    .locals 3
    .param p1    # Lcom/mediatek/videofavorites/RemoteVideoView;

    const/4 v2, 0x2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    iget-object v1, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mVideoActive:[Lcom/mediatek/videofavorites/VideoTextureView;

    aget-object v1, v1, v0

    if-nez v1, :cond_1

    :cond_0
    if-lt v0, v2, :cond_2

    const-string v1, "VFRootView"

    const-string v2, "unable to add Video into Active List"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return-void

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mVideoActive:[Lcom/mediatek/videofavorites/VideoTextureView;

    aput-object p1, v1, v0

    iget v1, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mVideoActiveCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mVideoActiveCount:I

    goto :goto_1
.end method

.method private addVideoViews(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    instance-of v3, p1, Landroid/view/ViewGroup;

    if-eqz v3, :cond_2

    move-object v2, p1

    check-cast v2, Landroid/view/ViewGroup;

    instance-of v3, p1, Landroid/widget/FrameLayout;

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    const v4, 0x7f080007

    if-ne v3, v4, :cond_1

    iget v3, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mNumOfEmptyView:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mNumOfEmptyView:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v0, :cond_0

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->addVideoViews(Landroid/view/View;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    instance-of v3, p1, Lcom/mediatek/videofavorites/VideoTextureView;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mVideoViews:Ljava/util/ArrayList;

    check-cast p1, Lcom/mediatek/videofavorites/VideoTextureView;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    instance-of v3, p1, Landroid/widget/ImageView;

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mNumOfImgView:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mNumOfImgView:I

    goto :goto_0
.end method

.method private addViews(Landroid/view/View;ILjava/util/ArrayList;)V
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    if-ne v3, p2, :cond_0

    invoke-virtual {p3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    instance-of v3, p1, Landroid/view/ViewGroup;

    if-eqz v3, :cond_1

    move-object v2, p1

    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-direct {p0, v3, p2, p3}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->addViews(Landroid/view/View;ILjava/util/ArrayList;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private buildVideoViewList()Z
    .locals 7

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->getListView()Landroid/widget/AbsListView;

    move-result-object v1

    if-nez v1, :cond_0

    const-string v3, "VFRootView"

    const-string v4, "rootview not found"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v2

    :cond_0
    iget-object v4, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mVideoViews:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    iput v2, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mNumOfImgView:I

    iput v2, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mNumOfEmptyView:I

    invoke-direct {p0, v1}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->addVideoViews(Landroid/view/View;)V

    const-string v4, "VFRootView"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "numofImgView: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mNumOfImgView:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->getListView()Landroid/widget/AbsListView;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-direct {p0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->getListView()Landroid/widget/AbsListView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    mul-int/lit8 v4, v4, 0x2

    iget v5, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mNumOfImgView:I

    iget v6, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mNumOfEmptyView:I

    mul-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    if-eq v4, v5, :cond_1

    invoke-direct {p0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->getListView()Landroid/widget/AbsListView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    mul-int/lit8 v4, v4, 0x2

    iget v5, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mNumOfImgView:I

    add-int/lit8 v5, v5, 0x1

    iget v6, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mNumOfEmptyView:I

    mul-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    if-ne v4, v5, :cond_3

    :cond_1
    const v4, 0x7f08000f

    invoke-virtual {p0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget v4, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mNumOfImgView:I

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    :goto_1
    move v2, v3

    goto :goto_0

    :cond_2
    invoke-virtual {v0, v3}, Landroid/view/View;->setClickable(Z)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_1

    :cond_3
    const-string v3, "VFRootView"

    const-string v4, "not all videoViews being found, Retry later"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private findAllViewsById(ILjava/util/ArrayList;)V
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->getListView()Landroid/widget/AbsListView;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v1, "VFRootView"

    const-string v2, "rootview not found"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->clear()V

    invoke-direct {p0, v0, p1, p2}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->addViews(Landroid/view/View;ILjava/util/ArrayList;)V

    goto :goto_0
.end method

.method private findFirstAbsListView()Landroid/widget/AbsListView;
    .locals 5

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    const-string v3, "VFRootView"

    const-string v4, "findFirstAbsListView()"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    instance-of v3, v2, Landroid/widget/AbsListView;

    if-eqz v3, :cond_0

    check-cast v2, Landroid/widget/AbsListView;

    :goto_1
    return-object v2

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private getListView()Landroid/widget/AbsListView;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mListView:Landroid/widget/AbsListView;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->findFirstAbsListView()Landroid/widget/AbsListView;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mListView:Landroid/widget/AbsListView;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mListView:Landroid/widget/AbsListView;

    return-object v0
.end method

.method private installIntentFilter()V
    .locals 6

    const/4 v3, 0x1

    const-string v4, "VFRootView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "installIntentFilter(): "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v2, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mIsListeningIntent:Z

    if-nez v2, :cond_1

    move v2, v3

    :goto_0
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v2, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mIsListeningIntent:Z

    if-nez v2, :cond_0

    new-instance v0, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.MEDIA_EJECT"

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v2, "file"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    const-string v2, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v4, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v4, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.mediatek.videofavorites.REFRESH"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v4, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v4, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iput-boolean v3, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mIsListeningIntent:Z

    :cond_0
    return-void

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private isInActiveList(Lcom/mediatek/videofavorites/RemoteVideoView;)Z
    .locals 1
    .param p1    # Lcom/mediatek/videofavorites/RemoteVideoView;

    iget-object v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mVideoActive:[Lcom/mediatek/videofavorites/VideoTextureView;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private onPageSwitchIn()V
    .locals 1

    const/16 v0, 0x32

    invoke-direct {p0, v0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->onPageSwitchIn(I)V

    return-void
.end method

.method private onPageSwitchIn(I)V
    .locals 3
    .param p1    # I

    const/4 v2, 0x1

    const-string v0, "VFRootView"

    const-string v1, "onPageSwitchIn"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mState:I

    if-eq v0, v2, :cond_0

    iget v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mState:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    :cond_0
    const-string v0, "VFRootView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "State is invalid: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", ignore"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, p1}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->startPlayVideoRandomly(I)V

    iget-object v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    invoke-virtual {p0, v2}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->setState(I)V

    goto :goto_0
.end method

.method private onPageSwitchOut()V
    .locals 4

    const/4 v3, 0x3

    iget-object v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mHandler:Landroid/os/Handler;

    const-wide/16 v1, 0x64

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    invoke-virtual {p0, v3}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->setState(I)V

    return-void
.end method

.method private onPageSwitchOutInternal()V
    .locals 1

    invoke-direct {p0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->stopAllVideoViewAsync()V

    iget-boolean v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mIsDeleteMode:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->setDeleteMode(Z)V

    :cond_0
    return-void
.end method

.method private pauseAllVideoView()V
    .locals 4

    const/4 v3, 0x2

    const-string v1, "VFRootView"

    const-string v2, "pauseAllVideoView()"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    iget-object v1, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mVideoActive:[Lcom/mediatek/videofavorites/VideoTextureView;

    aget-object v1, v1, v0

    if-nez v1, :cond_0

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mVideoActive:[Lcom/mediatek/videofavorites/VideoTextureView;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/mediatek/videofavorites/VideoTextureView;->pause()V

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    return-void
.end method

.method private restartVideoViews()V
    .locals 3

    invoke-direct {p0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->stopAllVideoView()V

    iget v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/16 v0, 0x12c

    invoke-virtual {p0, v0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->startPlayVideoRandomly(I)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "VFRootView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mState: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private resumeAllVideoView()V
    .locals 3

    const-string v1, "VFRootView"

    const-string v2, "resumeAllVideoView()"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mVideoActive:[Lcom/mediatek/videofavorites/VideoTextureView;

    aget-object v1, v1, v0

    if-nez v1, :cond_0

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mVideoActive:[Lcom/mediatek/videofavorites/VideoTextureView;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/mediatek/videofavorites/VideoTextureView;->start()V

    goto :goto_1

    :cond_1
    return-void
.end method

.method private sendRefreshBroadcast()V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.mediatek.videofavorites.REFRESH"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method private sendRefreshBroadcast(I)V
    .locals 4
    .param p1    # I

    const/4 v3, 0x4

    iget-object v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mHandler:Landroid/os/Handler;

    int-to-long v1, p1

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->sendRefreshBroadcast()V

    goto :goto_0
.end method

.method private setDeleteMode(Z)V
    .locals 6
    .param p1    # Z

    invoke-direct {p0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->stopAllVideoViewAsync()V

    iput-boolean p1, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mIsDeleteMode:Z

    iget-object v3, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mDeleteIcons:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mDeleteIconId:I

    iget-object v4, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mDeleteIcons:Ljava/util/ArrayList;

    invoke-direct {p0, v3, v4}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->findAllViewsById(ILjava/util/ArrayList;)V

    :cond_0
    iget-boolean v3, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mIsDeleteMode:Z

    if-eqz v3, :cond_1

    const/4 v2, 0x0

    :goto_0
    const-string v3, "VFRootView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "switchDeleteMode: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mIsDeleteMode:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mDeleteIcons:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_1
    const/4 v2, 0x4

    goto :goto_0

    :cond_2
    return-void
.end method

.method private showVideoAndRandomStrart()V
    .locals 11

    const/4 v8, 0x2

    iget-object v7, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mVideoViews:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, v8, :cond_1

    move v7, v8

    :goto_0
    iget v9, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mVideoActiveCount:I

    sub-int v1, v7, v9

    const-string v7, "VFRootView"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "showVideoAndRandomStrart, available:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " Active"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mVideoActiveCount:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " count:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-gtz v1, :cond_2

    :cond_0
    :goto_1
    return-void

    :cond_1
    move v7, v0

    goto :goto_0

    :cond_2
    const/4 v5, 0x6

    :cond_3
    iget-object v7, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mRandom:Ljava/util/Random;

    invoke-virtual {v7, v0}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    iget-object v7, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mVideoViews:Ljava/util/ArrayList;

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/mediatek/videofavorites/RemoteVideoView;

    add-int/lit8 v5, v5, -0x1

    if-nez v5, :cond_4

    const-string v7, "VFRootView"

    const-string v8, "cannot find a different video after trying"

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_4
    invoke-virtual {v6}, Lcom/mediatek/videofavorites/RemoteVideoView;->isUriAvailable()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-direct {p0, v6}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->isInActiveList(Lcom/mediatek/videofavorites/RemoteVideoView;)Z

    move-result v7

    if-nez v7, :cond_3

    invoke-virtual {v6}, Lcom/mediatek/videofavorites/RemoteVideoView;->getUriPath()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5

    const-string v7, ""

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_5

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_5

    const-string v7, "VFRootView"

    const-string v8, "file is not found"

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->sendRefreshBroadcast()V

    goto :goto_1

    :cond_5
    iget-object v7, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mVideoCompleteListener:Landroid/media/MediaPlayer$OnCompletionListener;

    invoke-virtual {v6, v7}, Lcom/mediatek/videofavorites/VideoTextureView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    iget-object v7, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mVideoErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    invoke-virtual {v6, v7}, Lcom/mediatek/videofavorites/VideoTextureView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    invoke-virtual {v6}, Lcom/mediatek/videofavorites/VideoTextureView;->isInPlaybackState()Z

    move-result v7

    if-nez v7, :cond_6

    const-string v7, "VFRootView"

    const-string v9, "openVideo()"

    invoke-static {v7, v9}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v6}, Lcom/mediatek/videofavorites/RemoteVideoView;->openVideo()V

    :cond_6
    const-string v7, "VFRootView"

    const-string v9, "start()"

    invoke-static {v7, v9}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v6}, Lcom/mediatek/videofavorites/VideoTextureView;->start()V

    invoke-virtual {v6}, Lcom/mediatek/videofavorites/VideoTextureView;->isInErrorState()Z

    move-result v7

    if-eqz v7, :cond_7

    const/4 v7, 0x4

    invoke-virtual {v6, v7}, Lcom/mediatek/videofavorites/RemoteVideoView;->setVisibility(I)V

    :goto_2
    const/4 v7, 0x1

    if-le v1, v7, :cond_0

    iget-object v7, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v7, v8}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v7, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mHandler:Landroid/os/Handler;

    const-wide/16 v9, 0xbb8

    invoke-virtual {v7, v8, v9, v10}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_1

    :cond_7
    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/mediatek/videofavorites/RemoteVideoView;->setVisibility(I)V

    invoke-direct {p0, v6}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->addVideoToActiveList(Lcom/mediatek/videofavorites/RemoteVideoView;)V

    goto :goto_2
.end method

.method private stopAllVideoView()V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x2

    const-string v2, "VFRootView"

    const-string v3, "stopAllVideoView()"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v4}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v2, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget v2, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mVideoActiveCount:I

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v4, :cond_2

    iget-object v2, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mVideoActive:[Lcom/mediatek/videofavorites/VideoTextureView;

    aget-object v1, v2, v0

    if-nez v1, :cond_1

    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {v1}, Lcom/mediatek/videofavorites/VideoTextureView;->pause()V

    invoke-virtual {v1, v5}, Lcom/mediatek/videofavorites/VideoTextureView;->seekTo(I)V

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mVideoActive:[Lcom/mediatek/videofavorites/VideoTextureView;

    const/4 v3, 0x0

    aput-object v3, v2, v0

    invoke-virtual {v1}, Lcom/mediatek/videofavorites/VideoTextureView;->stopPlayback()V

    goto :goto_2

    :cond_2
    iget-object v2, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v4}, Landroid/os/Handler;->removeMessages(I)V

    iput v5, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mVideoActiveCount:I

    const-string v2, "VFRootView"

    const-string v3, "stopAllVideoView() done"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private stopAllVideoViewAsync()V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v5}, Landroid/os/Handler;->removeMessages(I)V

    iget v2, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mVideoActiveCount:I

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v2, "VFRootView"

    const-string v3, "stopAllVideoView()"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v5, :cond_2

    iget-object v2, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mVideoActive:[Lcom/mediatek/videofavorites/VideoTextureView;

    aget-object v1, v2, v0

    if-nez v1, :cond_1

    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {v1}, Lcom/mediatek/videofavorites/VideoTextureView;->pause()V

    invoke-virtual {v1, v4}, Lcom/mediatek/videofavorites/VideoTextureView;->seekTo(I)V

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mVideoActive:[Lcom/mediatek/videofavorites/VideoTextureView;

    const/4 v3, 0x0

    aput-object v3, v2, v0

    invoke-virtual {v1}, Lcom/mediatek/videofavorites/VideoTextureView;->stopPlaybackAsync()V

    goto :goto_2

    :cond_2
    iput v4, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mVideoActiveCount:I

    goto :goto_0
.end method

.method private switchDeleteMode()V
    .locals 3

    iget-boolean v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mIsDeleteMode:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->setDeleteMode(Z)V

    iget-boolean v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mIsDeleteMode:Z

    if-nez v0, :cond_0

    const-string v0, "VFRootView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "startPlayVideoRandomly after switchDeleteMode"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mIsDeleteMode:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0xc8

    invoke-virtual {p0, v0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->startPlayVideoRandomly(I)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private uninstallIntentFilter()V
    .locals 3

    const-string v0, "VFRootView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "uninstallIntentFilter(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mIsListeningIntent:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mIsListeningIntent:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mIsListeningIntent:Z

    :cond_0
    return-void
.end method


# virtual methods
.method public enterAppwidgetScreen()V
    .locals 2

    const-string v0, "VFRootView"

    const-string v1, "enterAppwidgetScreen()"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->onPageSwitchIn()V

    return-void
.end method

.method public getPermittedCount()I
    .locals 1

    const/4 v0, 0x5

    return v0
.end method

.method public getScreen()I
    .locals 1

    iget v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mScreenIndex:I

    return v0
.end method

.method public getVideoCount()I
    .locals 1

    iget-object v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mVideoViews:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getWidgetId()I
    .locals 1

    iget v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mWidgetId:I

    return v0
.end method

.method public isDeleteMode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mIsDeleteMode:Z

    return v0
.end method

.method public leaveAppwidgetScreen()V
    .locals 2

    const-string v0, "VFRootView"

    const-string v1, "leaveAppwidgetScreen()"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->onPageSwitchOut()V

    return-void
.end method

.method public moveIn(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method public moveOut(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 3

    const-string v0, "VFRootView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onAttachedToWindow():"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->setState(I)V

    iget-object v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mRandom:Ljava/util/Random;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mRandom:Ljava/util/Random;

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->getListView()Landroid/widget/AbsListView;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->getListView()Landroid/widget/AbsListView;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mHierarchyChangeListener:Landroid/view/ViewGroup$OnHierarchyChangeListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V

    :goto_0
    invoke-direct {p0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->installIntentFilter()V

    const/16 v0, 0x5dc

    invoke-virtual {p0, v0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->startPlayVideoRandomly(I)V

    return-void

    :cond_1
    const-string v0, "VFRootView"

    const-string v1, "Failed to set onChildViewChangeListener"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    const-string v0, "VFRootView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ViewId clicked: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget v1, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mDeleteModeViewId:I

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->switchDeleteMode()V

    :cond_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 3

    const-string v0, "VFRootView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDetachedFromWindow():"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->uninstallIntentFilter()V

    invoke-direct {p0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->stopAllVideoView()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->setState(I)V

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1    # Landroid/view/MotionEvent;

    const/4 v1, 0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v2, v0, 0xff

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    const/4 v1, 0x0

    :goto_1
    :sswitch_0
    return v1

    :sswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iput v1, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mLastY:F

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget v3, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mLastY:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const/high16 v3, 0x41200000

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x2 -> :sswitch_2
        0x8 -> :sswitch_0
    .end sparse-switch
.end method

.method public onPauseWhenShown(I)V
    .locals 2
    .param p1    # I

    const-string v0, "VFRootView"

    const-string v1, "onPauseWhenShown()"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    invoke-direct {p0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->stopAllVideoViewAsync()V

    iget-boolean v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mIsDeleteMode:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->setDeleteMode(Z)V

    :cond_0
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->setState(I)V

    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;

    return-void
.end method

.method public onResumeWhenShown(I)V
    .locals 2
    .param p1    # I

    const-string v0, "VFRootView"

    const-string v1, "onResumeWhenShown()"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mDeleteIcons:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mIsDeleteMode:Z

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->setState(I)V

    const/16 v0, 0x12c

    invoke-virtual {p0, v0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->startPlayVideoRandomly(I)V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1    # Landroid/view/MotionEvent;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v1, v0, 0xff

    sparse-switch v1, :sswitch_data_0

    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    :goto_0
    return v1

    :sswitch_0
    const/4 v1, 0x0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x8 -> :sswitch_0
    .end sparse-switch
.end method

.method public setDeleteIconId(I)V
    .locals 0
    .param p1    # I
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    iput p1, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mDeleteIconId:I

    return-void
.end method

.method public setDeleteModeViewId(I)V
    .locals 3
    .param p1    # I
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    iput p1, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mDeleteModeViewId:I

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v1, "VFRootView"

    const-string v2, "delete view not found!"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public setScreen(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mScreenIndex:I

    return-void
.end method

.method public setState(I)V
    .locals 3
    .param p1    # I

    const-string v0, "VFRootView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setState:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " -> "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput p1, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mState:I

    return-void
.end method

.method public setWidgetId(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mWidgetId:I

    return-void
.end method

.method public startCovered(I)V
    .locals 2
    .param p1    # I

    const-string v0, "VFRootView"

    const-string v1, "startCovered()"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->setState(I)V

    invoke-direct {p0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->onPageSwitchOut()V

    iget-boolean v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mIsDeleteMode:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->setDeleteMode(Z)V

    :cond_0
    return-void
.end method

.method public startDrag()V
    .locals 2

    const-string v0, "VFRootView"

    const-string v1, "startDrag()"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    invoke-direct {p0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->stopAllVideoView()V

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->setState(I)V

    iget-boolean v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mIsDeleteMode:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->setDeleteMode(Z)V

    :cond_0
    return-void
.end method

.method public startPlayVideoRandomly(I)V
    .locals 4
    .param p1    # I
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    const/4 v3, 0x1

    const-string v0, "VFRootView"

    const-string v1, "startvideoplay"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-boolean v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mIsMounted:Z

    if-nez v0, :cond_0

    const-string v0, "VFRootView"

    const-string v1, "some media is ejecting, return"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mHandler:Landroid/os/Handler;

    int-to-long v1, p1

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method public stopCovered(I)V
    .locals 2
    .param p1    # I

    iget v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mState:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const-string v0, "VFRootView"

    const-string v1, "stopCovered() ignored"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const-string v0, "VFRootView"

    const-string v1, "stopCovered()"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0x190

    invoke-direct {p0, v0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->onPageSwitchIn(I)V

    goto :goto_0
.end method

.method public stopDrag()V
    .locals 2

    const-string v0, "VFRootView"

    const-string v1, "stopDrag()"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0x32

    invoke-virtual {p0, v0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->startPlayVideoRandomly(I)V

    iget-object v0, p0, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->mDeleteIcons:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/mediatek/videofavorites/VideoFavoritesRootView;->setState(I)V

    return-void
.end method
