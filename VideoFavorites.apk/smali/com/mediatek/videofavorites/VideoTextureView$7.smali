.class Lcom/mediatek/videofavorites/VideoTextureView$7;
.super Ljava/lang/Object;
.source "VideoTextureView.java"

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/videofavorites/VideoTextureView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/videofavorites/VideoTextureView;


# direct methods
.method constructor <init>(Lcom/mediatek/videofavorites/VideoTextureView;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/videofavorites/VideoTextureView$7;->this$0:Lcom/mediatek/videofavorites/VideoTextureView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 2
    .param p1    # Landroid/graphics/SurfaceTexture;
    .param p2    # I
    .param p3    # I

    const-string v0, "VideoTextureView"

    const-string v1, "onSurfaceTextureAvailable"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/videofavorites/VideoTextureView$7;->this$0:Lcom/mediatek/videofavorites/VideoTextureView;

    new-instance v1, Landroid/view/Surface;

    invoke-direct {v1, p1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    iput-object v1, v0, Lcom/mediatek/videofavorites/VideoTextureView;->mSurface:Landroid/view/Surface;

    iget-object v0, p0, Lcom/mediatek/videofavorites/VideoTextureView$7;->this$0:Lcom/mediatek/videofavorites/VideoTextureView;

    iput p2, v0, Lcom/mediatek/videofavorites/VideoTextureView;->mSurfaceWidth:I

    iget-object v0, p0, Lcom/mediatek/videofavorites/VideoTextureView$7;->this$0:Lcom/mediatek/videofavorites/VideoTextureView;

    iput p3, v0, Lcom/mediatek/videofavorites/VideoTextureView;->mSurfaceHeight:I

    iget-object v0, p0, Lcom/mediatek/videofavorites/VideoTextureView$7;->this$0:Lcom/mediatek/videofavorites/VideoTextureView;

    invoke-virtual {v0}, Lcom/mediatek/videofavorites/VideoTextureView;->openVideo()V

    return-void
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 3
    .param p1    # Landroid/graphics/SurfaceTexture;

    const/4 v2, 0x1

    const-string v0, "VideoTextureView"

    const-string v1, "onSurfaceTextureDestroyed"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/videofavorites/VideoTextureView$7;->this$0:Lcom/mediatek/videofavorites/VideoTextureView;

    iget-object v0, v0, Lcom/mediatek/videofavorites/VideoTextureView;->mSurface:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    iget-object v0, p0, Lcom/mediatek/videofavorites/VideoTextureView$7;->this$0:Lcom/mediatek/videofavorites/VideoTextureView;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/mediatek/videofavorites/VideoTextureView;->mSurface:Landroid/view/Surface;

    iget-object v0, p0, Lcom/mediatek/videofavorites/VideoTextureView$7;->this$0:Lcom/mediatek/videofavorites/VideoTextureView;

    iget-object v0, v0, Lcom/mediatek/videofavorites/VideoTextureView;->mMediaController:Landroid/widget/MediaController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/videofavorites/VideoTextureView$7;->this$0:Lcom/mediatek/videofavorites/VideoTextureView;

    iget-object v0, v0, Lcom/mediatek/videofavorites/VideoTextureView;->mMediaController:Landroid/widget/MediaController;

    invoke-virtual {v0}, Landroid/widget/MediaController;->hide()V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/videofavorites/VideoTextureView$7;->this$0:Lcom/mediatek/videofavorites/VideoTextureView;

    invoke-virtual {v0, v2}, Lcom/mediatek/videofavorites/VideoTextureView;->release(Z)V

    return v2
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 6
    .param p1    # Landroid/graphics/SurfaceTexture;
    .param p2    # I
    .param p3    # I

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/mediatek/videofavorites/VideoTextureView$7;->this$0:Lcom/mediatek/videofavorites/VideoTextureView;

    iput p2, v4, Lcom/mediatek/videofavorites/VideoTextureView;->mSurfaceWidth:I

    iget-object v4, p0, Lcom/mediatek/videofavorites/VideoTextureView$7;->this$0:Lcom/mediatek/videofavorites/VideoTextureView;

    iput p3, v4, Lcom/mediatek/videofavorites/VideoTextureView;->mSurfaceHeight:I

    iget-object v4, p0, Lcom/mediatek/videofavorites/VideoTextureView$7;->this$0:Lcom/mediatek/videofavorites/VideoTextureView;

    iget v4, v4, Lcom/mediatek/videofavorites/VideoTextureView;->mTargetState:I

    const/4 v5, 0x3

    if-ne v4, v5, :cond_2

    move v1, v2

    :goto_0
    iget-object v4, p0, Lcom/mediatek/videofavorites/VideoTextureView$7;->this$0:Lcom/mediatek/videofavorites/VideoTextureView;

    iget v4, v4, Lcom/mediatek/videofavorites/VideoTextureView;->mVideoWidth:I

    if-ne v4, p2, :cond_3

    iget-object v4, p0, Lcom/mediatek/videofavorites/VideoTextureView$7;->this$0:Lcom/mediatek/videofavorites/VideoTextureView;

    iget v4, v4, Lcom/mediatek/videofavorites/VideoTextureView;->mVideoHeight:I

    if-ne v4, p3, :cond_3

    move v0, v2

    :goto_1
    iget-object v2, p0, Lcom/mediatek/videofavorites/VideoTextureView$7;->this$0:Lcom/mediatek/videofavorites/VideoTextureView;

    iget-object v2, v2, Lcom/mediatek/videofavorites/VideoTextureView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/mediatek/videofavorites/VideoTextureView$7;->this$0:Lcom/mediatek/videofavorites/VideoTextureView;

    iget v2, v2, Lcom/mediatek/videofavorites/VideoTextureView;->mSeekWhenPrepared:I

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/videofavorites/VideoTextureView$7;->this$0:Lcom/mediatek/videofavorites/VideoTextureView;

    iget-object v3, p0, Lcom/mediatek/videofavorites/VideoTextureView$7;->this$0:Lcom/mediatek/videofavorites/VideoTextureView;

    iget v3, v3, Lcom/mediatek/videofavorites/VideoTextureView;->mSeekWhenPrepared:I

    invoke-virtual {v2, v3}, Lcom/mediatek/videofavorites/VideoTextureView;->seekTo(I)V

    :cond_0
    iget-object v2, p0, Lcom/mediatek/videofavorites/VideoTextureView$7;->this$0:Lcom/mediatek/videofavorites/VideoTextureView;

    invoke-virtual {v2}, Lcom/mediatek/videofavorites/VideoTextureView;->start()V

    :cond_1
    return-void

    :cond_2
    move v1, v3

    goto :goto_0

    :cond_3
    move v0, v3

    goto :goto_1
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 0
    .param p1    # Landroid/graphics/SurfaceTexture;

    return-void
.end method
