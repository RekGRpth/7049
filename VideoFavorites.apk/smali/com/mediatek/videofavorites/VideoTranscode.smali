.class public final Lcom/mediatek/videofavorites/VideoTranscode;
.super Ljava/lang/Object;
.source "VideoTranscode.java"


# static fields
.field public static final ERROR_TRANSCODE_FAIL:I = -0x7fffffff

.field public static final ERROR_UNSUPPORTED_VIDEO:I = -0x80000000

.field public static final NO_ERROR:I = 0x0

.field private static final TAG:Ljava/lang/String; = "TRANSCODER_JNI"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "jtranscode"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static native cancel()V
.end method

.method public static native getProgress()I
.end method

.method public static native init()V
.end method

.method public static native transcode(Ljava/lang/String;Ljava/lang/String;JJJJ)I
.end method
