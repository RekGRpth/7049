.class Lcom/mediatek/videofavorites/WidgetActionActivity$1;
.super Landroid/os/Handler;
.source "WidgetActionActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/videofavorites/WidgetActionActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/videofavorites/WidgetActionActivity;


# direct methods
.method constructor <init>(Lcom/mediatek/videofavorites/WidgetActionActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/videofavorites/WidgetActionActivity$1;->this$0:Lcom/mediatek/videofavorites/WidgetActionActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9
    .param p1    # Landroid/os/Message;

    const-wide/16 v7, 0xc8

    const/16 v3, 0xc8

    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/mediatek/videofavorites/WidgetActionActivity$1;->this$0:Lcom/mediatek/videofavorites/WidgetActionActivity;

    iget-object v1, v1, Lcom/mediatek/videofavorites/WidgetActionActivity;->mProgressDlg:Lcom/mediatek/videofavorites/ProgressDialogFragment;

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mediatek/videofavorites/VideoTranscode;->getProgress()I

    move-result v0

    iget-object v1, p0, Lcom/mediatek/videofavorites/WidgetActionActivity$1;->this$0:Lcom/mediatek/videofavorites/WidgetActionActivity;

    iget-object v1, v1, Lcom/mediatek/videofavorites/WidgetActionActivity;->mProgressDlg:Lcom/mediatek/videofavorites/ProgressDialogFragment;

    invoke-virtual {v1, v0}, Lcom/mediatek/videofavorites/ProgressDialogFragment;->setProgress(I)V

    const/16 v1, 0x64

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/videofavorites/WidgetActionActivity$1;->this$0:Lcom/mediatek/videofavorites/WidgetActionActivity;

    invoke-static {v1}, Lcom/mediatek/videofavorites/WidgetActionActivity;->access$000(Lcom/mediatek/videofavorites/WidgetActionActivity;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v7, v8}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/mediatek/videofavorites/WidgetActionActivity$1;->this$0:Lcom/mediatek/videofavorites/WidgetActionActivity;

    iget-object v1, v1, Lcom/mediatek/videofavorites/WidgetActionActivity;->mProgressDlg:Lcom/mediatek/videofavorites/ProgressDialogFragment;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/videofavorites/WidgetActionActivity$1;->this$0:Lcom/mediatek/videofavorites/WidgetActionActivity;

    iget-object v1, v1, Lcom/mediatek/videofavorites/WidgetActionActivity;->mProgressDlg:Lcom/mediatek/videofavorites/ProgressDialogFragment;

    invoke-virtual {v1}, Landroid/app/Fragment;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/videofavorites/WidgetActionActivity$1;->this$0:Lcom/mediatek/videofavorites/WidgetActionActivity;

    iget-object v1, v1, Lcom/mediatek/videofavorites/WidgetActionActivity;->mProgressDlg:Lcom/mediatek/videofavorites/ProgressDialogFragment;

    invoke-virtual {v1}, Landroid/app/DialogFragment;->dismiss()V

    :cond_1
    iget-object v1, p0, Lcom/mediatek/videofavorites/WidgetActionActivity$1;->this$0:Lcom/mediatek/videofavorites/WidgetActionActivity;

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/mediatek/videofavorites/WidgetActionActivity$1;->this$0:Lcom/mediatek/videofavorites/WidgetActionActivity;

    const v2, 0x7f06000d

    invoke-static {v1, v2, v3}, Lcom/mediatek/videofavorites/WidgetActionActivity;->access$100(Lcom/mediatek/videofavorites/WidgetActionActivity;II)V

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/mediatek/videofavorites/WidgetActionActivity$1;->this$0:Lcom/mediatek/videofavorites/WidgetActionActivity;

    const v2, 0x7f06000f

    invoke-static {v1, v2, v3}, Lcom/mediatek/videofavorites/WidgetActionActivity;->access$100(Lcom/mediatek/videofavorites/WidgetActionActivity;II)V

    goto :goto_0

    :pswitch_4
    const-string v1, "WidgetAction"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MSG_TIMER_TEST, waited:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    iget-object v5, p0, Lcom/mediatek/videofavorites/WidgetActionActivity$1;->this$0:Lcom/mediatek/videofavorites/WidgetActionActivity;

    invoke-static {v5}, Lcom/mediatek/videofavorites/WidgetActionActivity;->access$200(Lcom/mediatek/videofavorites/WidgetActionActivity;)J

    move-result-wide v5

    sub-long/2addr v3, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/videofavorites/WidgetActionActivity$1;->this$0:Lcom/mediatek/videofavorites/WidgetActionActivity;

    invoke-static {v1}, Lcom/mediatek/videofavorites/WidgetActionActivity;->access$300(Lcom/mediatek/videofavorites/WidgetActionActivity;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "WidgetAction"

    const-string v2, "sendEmptyMessageDelayed(), delay:200"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/videofavorites/WidgetActionActivity$1;->this$0:Lcom/mediatek/videofavorites/WidgetActionActivity;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/mediatek/videofavorites/WidgetActionActivity;->access$202(Lcom/mediatek/videofavorites/WidgetActionActivity;J)J

    iget-object v1, p0, Lcom/mediatek/videofavorites/WidgetActionActivity$1;->this$0:Lcom/mediatek/videofavorites/WidgetActionActivity;

    invoke-static {v1}, Lcom/mediatek/videofavorites/WidgetActionActivity;->access$000(Lcom/mediatek/videofavorites/WidgetActionActivity;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2, v7, v8}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
