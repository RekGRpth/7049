.class Lcom/mediatek/videofavorites/WidgetActionActivity$TranscodeTask;
.super Landroid/os/AsyncTask;
.source "WidgetActionActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/videofavorites/WidgetActionActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TranscodeTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Landroid/net/Uri;",
        "Ljava/lang/Integer;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final RESULT_INVALID_VIDEO:I = -0x2

.field private static final RESULT_OK:I = 0x0

.field private static final RESULT_RESOULTION_TOO_HIGH:I = -0x1


# instance fields
.field private mResult:I

.field private mTranscode:Z

.field private mUri:Landroid/net/Uri;

.field final synthetic this$0:Lcom/mediatek/videofavorites/WidgetActionActivity;


# direct methods
.method private constructor <init>(Lcom/mediatek/videofavorites/WidgetActionActivity;)V
    .locals 1

    iput-object p1, p0, Lcom/mediatek/videofavorites/WidgetActionActivity$TranscodeTask;->this$0:Lcom/mediatek/videofavorites/WidgetActionActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity$TranscodeTask;->mResult:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/mediatek/videofavorites/WidgetActionActivity;Lcom/mediatek/videofavorites/WidgetActionActivity$1;)V
    .locals 0
    .param p1    # Lcom/mediatek/videofavorites/WidgetActionActivity;
    .param p2    # Lcom/mediatek/videofavorites/WidgetActionActivity$1;

    invoke-direct {p0, p1}, Lcom/mediatek/videofavorites/WidgetActionActivity$TranscodeTask;-><init>(Lcom/mediatek/videofavorites/WidgetActionActivity;)V

    return-void
.end method

.method private createName(J)Ljava/lang/String;
    .locals 4
    .param p1    # J

    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, p1, p2}, Ljava/util/Date;-><init>(J)V

    new-instance v1, Ljava/text/SimpleDateFormat;

    iget-object v2, p0, Lcom/mediatek/videofavorites/WidgetActionActivity$TranscodeTask;->this$0:Lcom/mediatek/videofavorites/WidgetActionActivity;

    const v3, 0x7f060006

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    invoke-virtual {v1, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private generateOutputPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1    # Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/mediatek/videofavorites/WidgetActionActivity$TranscodeTask;->createName(J)Ljava/lang/String;

    move-result-object v4

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sget-object v6, Lcom/mediatek/videofavorites/Storage;->TRANSCODE_PATH:Ljava/lang/String;

    invoke-direct {p0, v6}, Lcom/mediatek/videofavorites/WidgetActionActivity$TranscodeTask;->prepareFolder(Ljava/lang/String;)V

    new-instance v5, Ljava/lang/StringBuilder;

    sget-object v6, Lcom/mediatek/videofavorites/Storage;->TRANSCODE_PATH:Ljava/lang/String;

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    const/4 v6, -0x1

    if-ne v2, v6, :cond_0

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6

    :cond_0
    invoke-virtual {v5, v2, v4}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private getSourceVideoRect(Ljava/lang/String;Landroid/graphics/Rect;)Z
    .locals 12
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/graphics/Rect;

    const/4 v7, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x0

    new-instance v3, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v3}, Landroid/media/MediaMetadataRetriever;-><init>()V

    const/4 v1, 0x0

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-nez v8, :cond_0

    :goto_0
    return v7

    :cond_0
    :try_start_0
    invoke-virtual {v3, p1}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    const/16 v8, 0x11

    invoke-virtual {v3, v8}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    const-string v8, "WidgetAction"

    const-string v9, "getSourceVideoRect, no videoTrack"

    invoke-static {v8, v9}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v3}, Landroid/media/MediaMetadataRetriever;->release()V

    goto :goto_0

    :cond_1
    const/16 v8, 0x12

    :try_start_1
    invoke-virtual {v3, v8}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v5

    const/16 v8, 0x13

    invoke-virtual {v3, v8}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v4

    invoke-virtual {v3}, Landroid/media/MediaMetadataRetriever;->release()V

    :goto_1
    if-eqz v5, :cond_2

    if-nez v4, :cond_3

    :cond_2
    const-string v8, "WidgetAction"

    const-string v9, "invalid video width/height"

    invoke-static {v8, v9}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_0
    move-exception v8

    invoke-virtual {v3}, Landroid/media/MediaMetadataRetriever;->release()V

    goto :goto_1

    :catchall_0
    move-exception v7

    invoke-virtual {v3}, Landroid/media/MediaMetadataRetriever;->release()V

    throw v7

    :cond_3
    invoke-static {v5}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-static {v4}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eqz v6, :cond_4

    if-nez v2, :cond_5

    :cond_4
    const-string v8, "WidgetAction"

    const-string v9, "video width/height is 0"

    invoke-static {v8, v9}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_5
    invoke-virtual {p2, v7, v7, v6, v2}, Landroid/graphics/Rect;->set(IIII)V

    const/4 v7, 0x1

    goto :goto_0
.end method

.method private getTargetRect(IIII)Landroid/graphics/Rect;
    .locals 7
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v6, 0x0

    int-to-float v4, p1

    int-to-float v5, p2

    div-float v1, v4, v5

    int-to-float v4, p3

    int-to-float v5, p4

    div-float v0, v4, v5

    cmpg-float v4, v1, v0

    if-gez v4, :cond_1

    move v3, p3

    mul-int v4, v3, p2

    div-int v2, v4, p1

    :cond_0
    :goto_0
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4, v6, v6, v3, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v4

    :cond_1
    move v2, p4

    mul-int v4, v2, p1

    div-int v3, v4, p2

    rem-int/lit8 v4, v3, 0x10

    if-eqz v4, :cond_0

    add-int/lit8 v4, v3, -0xf

    shr-int/lit8 v4, v4, 0x4

    shl-int/lit8 v3, v4, 0x4

    mul-int v4, v3, p2

    div-int v2, v4, p1

    goto :goto_0
.end method

.method private prepareFolder(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "WidgetAction"

    const-string v2, "folder creation failed!"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Landroid/net/Uri;

    invoke-virtual {p0, p1}, Lcom/mediatek/videofavorites/WidgetActionActivity$TranscodeTask;->doInBackground([Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Landroid/net/Uri;)Ljava/lang/String;
    .locals 20
    .param p1    # [Landroid/net/Uri;

    const/4 v4, 0x0

    aget-object v4, p1, v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/mediatek/videofavorites/WidgetActionActivity$TranscodeTask;->mUri:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/videofavorites/WidgetActionActivity$TranscodeTask;->this$0:Lcom/mediatek/videofavorites/WidgetActionActivity;

    const/4 v5, 0x0

    aget-object v5, p1, v5

    invoke-virtual {v4, v5}, Lcom/mediatek/videofavorites/WidgetActionActivity;->getRealPathFromUri(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    new-instance v14, Landroid/graphics/Rect;

    invoke-direct {v14}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v14}, Lcom/mediatek/videofavorites/WidgetActionActivity$TranscodeTask;->getSourceVideoRect(Ljava/lang/String;Landroid/graphics/Rect;)Z

    move-result v12

    if-nez v12, :cond_1

    const/4 v4, -0x2

    move-object/from16 v0, p0

    iput v4, v0, Lcom/mediatek/videofavorites/WidgetActionActivity$TranscodeTask;->mResult:I

    const/4 v3, 0x0

    :cond_0
    :goto_0
    return-object v3

    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v15

    invoke-virtual {v14}, Landroid/graphics/Rect;->width()I

    move-result v4

    const/16 v5, 0x140

    if-le v4, v5, :cond_2

    invoke-virtual {v14}, Landroid/graphics/Rect;->height()I

    move-result v4

    const/16 v5, 0xf0

    if-gt v4, v5, :cond_4

    :cond_2
    move-object v3, v2

    :cond_3
    :goto_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    sub-long v18, v4, v15

    const-string v4, "WidgetAction"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "transcode spend(ms):"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v18

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/mediatek/videofavorites/WidgetActionActivity$TranscodeTask;->mResult:I

    if-eqz v4, :cond_0

    const/4 v3, 0x0

    goto :goto_0

    :cond_4
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/mediatek/videofavorites/WidgetActionActivity$TranscodeTask;->mTranscode:Z

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/mediatek/videofavorites/WidgetActionActivity$TranscodeTask;->generateOutputPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v14}, Landroid/graphics/Rect;->width()I

    move-result v4

    invoke-virtual {v14}, Landroid/graphics/Rect;->height()I

    move-result v5

    const/16 v6, 0x140

    const/16 v7, 0xf0

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v6, v7}, Lcom/mediatek/videofavorites/WidgetActionActivity$TranscodeTask;->getTargetRect(IIII)Landroid/graphics/Rect;

    move-result-object v17

    const-string v4, "WidgetAction"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "srcRect: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " targetRect: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Rect;->width()I

    move-result v4

    int-to-long v4, v4

    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Rect;->height()I

    move-result v6

    int-to-long v6, v6

    const-wide/16 v8, 0x0

    const-wide/16 v10, 0x2710

    invoke-static/range {v2 .. v11}, Lcom/mediatek/videofavorites/VideoTranscode;->transcode(Ljava/lang/String;Ljava/lang/String;JJJJ)I

    move-result v13

    const-string v4, "WidgetAction"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "transcode result: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v13, :cond_3

    const/high16 v4, -0x80000000

    if-ne v13, v4, :cond_5

    const/4 v4, -0x1

    :goto_2
    move-object/from16 v0, p0

    iput v4, v0, Lcom/mediatek/videofavorites/WidgetActionActivity$TranscodeTask;->mResult:I

    goto/16 :goto_1

    :cond_5
    const/4 v4, -0x2

    goto :goto_2
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/mediatek/videofavorites/WidgetActionActivity$TranscodeTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 11
    .param p1    # Ljava/lang/String;

    const/4 v7, 0x5

    iget-object v8, p0, Lcom/mediatek/videofavorites/WidgetActionActivity$TranscodeTask;->this$0:Lcom/mediatek/videofavorites/WidgetActionActivity;

    iget-object v8, v8, Lcom/mediatek/videofavorites/WidgetActionActivity;->mProgressDlg:Lcom/mediatek/videofavorites/ProgressDialogFragment;

    if-nez v8, :cond_0

    const-string v7, "WidgetAction"

    const-string v8, "cancelled due to dialog dismissed"

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v8, p0, Lcom/mediatek/videofavorites/WidgetActionActivity$TranscodeTask;->this$0:Lcom/mediatek/videofavorites/WidgetActionActivity;

    invoke-static {v8}, Lcom/mediatek/videofavorites/WidgetActionActivity;->access$000(Lcom/mediatek/videofavorites/WidgetActionActivity;)Landroid/os/Handler;

    move-result-object v8

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/os/Handler;->removeMessages(I)V

    iget v8, p0, Lcom/mediatek/videofavorites/WidgetActionActivity$TranscodeTask;->mResult:I

    if-nez v8, :cond_1

    if-nez p1, :cond_3

    :cond_1
    const-string v8, "WidgetAction"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "result: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/mediatek/videofavorites/WidgetActionActivity$TranscodeTask;->mResult:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v8, p0, Lcom/mediatek/videofavorites/WidgetActionActivity$TranscodeTask;->this$0:Lcom/mediatek/videofavorites/WidgetActionActivity;

    invoke-static {v8}, Lcom/mediatek/videofavorites/WidgetActionActivity;->access$000(Lcom/mediatek/videofavorites/WidgetActionActivity;)Landroid/os/Handler;

    move-result-object v8

    iget v9, p0, Lcom/mediatek/videofavorites/WidgetActionActivity$TranscodeTask;->mResult:I

    const/4 v10, -0x1

    if-ne v9, v10, :cond_2

    const/4 v7, 0x4

    :cond_2
    invoke-virtual {v8, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_3
    iget-boolean v8, p0, Lcom/mediatek/videofavorites/WidgetActionActivity$TranscodeTask;->mTranscode:Z

    if-eqz v8, :cond_5

    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5, v7}, Landroid/content/ContentValues;-><init>(I)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    new-instance v8, Ljava/io/File;

    invoke-direct {v8, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/16 v8, 0x0

    cmp-long v8, v2, v8

    if-nez v8, :cond_4

    const-string v8, "WidgetAction"

    const-string v9, "File length is 0, transcode failed or disk full, don\'t insert it"

    invoke-static {v8, v9}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v8, p0, Lcom/mediatek/videofavorites/WidgetActionActivity$TranscodeTask;->this$0:Lcom/mediatek/videofavorites/WidgetActionActivity;

    invoke-static {v8}, Lcom/mediatek/videofavorites/WidgetActionActivity;->access$000(Lcom/mediatek/videofavorites/WidgetActionActivity;)Landroid/os/Handler;

    move-result-object v8

    invoke-virtual {v8, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_4
    const-string v7, "datetaken"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v7, "_size"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v7, "_data"

    invoke-virtual {v5, v7, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "_display_name"

    invoke-virtual {v5, v7, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "mime_type"

    const-string v8, "video/mp4"

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "content://media/external/video/media"

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    iget-object v7, p0, Lcom/mediatek/videofavorites/WidgetActionActivity$TranscodeTask;->this$0:Lcom/mediatek/videofavorites/WidgetActionActivity;

    invoke-virtual {v7}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    invoke-virtual {v7, v6, v5}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v4

    if-nez v4, :cond_5

    const-string v7, "WidgetAction"

    const-string v8, "insert failed"

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    iget-object v7, p0, Lcom/mediatek/videofavorites/WidgetActionActivity$TranscodeTask;->this$0:Lcom/mediatek/videofavorites/WidgetActionActivity;

    new-instance v8, Ljava/io/File;

    invoke-direct {v8, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v8}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/videofavorites/WidgetActionActivity;->access$502(Lcom/mediatek/videofavorites/WidgetActionActivity;Landroid/net/Uri;)Landroid/net/Uri;

    const-string v7, "WidgetAction"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onPostExecute(), video inserted: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/mediatek/videofavorites/WidgetActionActivity$TranscodeTask;->this$0:Lcom/mediatek/videofavorites/WidgetActionActivity;

    invoke-static {v9}, Lcom/mediatek/videofavorites/WidgetActionActivity;->access$500(Lcom/mediatek/videofavorites/WidgetActionActivity;)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v7, p0, Lcom/mediatek/videofavorites/WidgetActionActivity$TranscodeTask;->this$0:Lcom/mediatek/videofavorites/WidgetActionActivity;

    iget-object v8, p0, Lcom/mediatek/videofavorites/WidgetActionActivity$TranscodeTask;->this$0:Lcom/mediatek/videofavorites/WidgetActionActivity;

    invoke-static {v8}, Lcom/mediatek/videofavorites/WidgetActionActivity;->access$500(Lcom/mediatek/videofavorites/WidgetActionActivity;)Landroid/net/Uri;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/videofavorites/WidgetActionActivity;->access$600(Lcom/mediatek/videofavorites/WidgetActionActivity;Landroid/net/Uri;)V

    iget-object v7, p0, Lcom/mediatek/videofavorites/WidgetActionActivity$TranscodeTask;->this$0:Lcom/mediatek/videofavorites/WidgetActionActivity;

    iget-object v7, v7, Lcom/mediatek/videofavorites/WidgetActionActivity;->mProgressDlg:Lcom/mediatek/videofavorites/ProgressDialogFragment;

    const/16 v8, 0x64

    invoke-virtual {v7, v8}, Lcom/mediatek/videofavorites/ProgressDialogFragment;->setProgress(I)V

    iget-object v7, p0, Lcom/mediatek/videofavorites/WidgetActionActivity$TranscodeTask;->this$0:Lcom/mediatek/videofavorites/WidgetActionActivity;

    invoke-static {v7}, Lcom/mediatek/videofavorites/WidgetActionActivity;->access$000(Lcom/mediatek/videofavorites/WidgetActionActivity;)Landroid/os/Handler;

    move-result-object v7

    const/4 v8, 0x2

    invoke-virtual {v7, v8}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0
.end method

.method protected onPreExecute()V
    .locals 4

    invoke-static {}, Lcom/mediatek/videofavorites/VideoTranscode;->init()V

    iget-object v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity$TranscodeTask;->this$0:Lcom/mediatek/videofavorites/WidgetActionActivity;

    invoke-static {v0}, Lcom/mediatek/videofavorites/WidgetActionActivity;->access$000(Lcom/mediatek/videofavorites/WidgetActionActivity;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method
