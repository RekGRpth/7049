.class public Lcom/mediatek/videofavorites/WidgetActionActivity;
.super Landroid/app/Activity;
.source "WidgetActionActivity.java"

# interfaces
.implements Lcom/mediatek/videofavorites/AlertDialogFragment$OnClickListener;
.implements Lcom/mediatek/videofavorites/ProgressDialogFragment$DialogActionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/videofavorites/WidgetActionActivity$TranscodeTask;
    }
.end annotation


# static fields
.field public static final ACTION_LAUNCH_RECORDER:Ljava/lang/String; = "action_launch_recorder"

.field public static final CODE_PICK_BOTH:I = 0x0

.field public static final CODE_PICK_CONTACT:I = 0x1

.field public static final CODE_PICK_VIDEO:I = 0x2

.field public static final CODE_RECORD_VIDEO:I = 0x3

.field public static final CODE_VIEW_CONTACT:I = 0x4

.field private static final CONTACT_PROJECTION:[Ljava/lang/String;

.field private static final ENCODE_HEIGHT:I = 0xf0

.field private static final ENCODE_WIDTH:I = 0x140

.field public static final KEY_ACTION_PICK_TYPE:Ljava/lang/String; = "action_pick_type"

.field private static final KEY_CONTACT_URI:Ljava/lang/String; = "contact_uri"

.field private static final KEY_ENABLE_TRANSCODE:Ljava/lang/String; = "key_enable_transcode"

.field private static final KEY_NAME:Ljava/lang/String; = "name"

.field private static final KEY_VIDEO_URI:Ljava/lang/String; = "video_uri"

.field private static final MSG_SUICIDE:I = 0x2

.field private static final MSG_TIMER_TEST:I = 0x3

.field private static final MSG_TRANSCODE_INVALID_SUICIDE:I = 0x5

.field private static final MSG_TRANSCODE_UNSUPPORTED_SUICIDE:I = 0x4

.field private static final MSG_UPDATE_PROGRESS:I = 0x1

.field private static final PROJ_COLS:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "WidgetAction"

.field private static final UPDATE_INTERVAL:I = 0xc8


# instance fields
.field private mAlertDlg:Lcom/mediatek/videofavorites/AlertDialogFragment;

.field private mContactUri:Landroid/net/Uri;

.field private mEnableTimerTest:Z

.field private final mHandler:Landroid/os/Handler;

.field private mIndexId:I

.field private mIsTransCoding:Z

.field private mLaunchActivity:Z

.field private mLeaveForActivity:Z

.field private mPaused:Z

.field private mPickType:I

.field mProgressDlg:Lcom/mediatek/videofavorites/ProgressDialogFragment;

.field private mStartTime:J

.field private mUri:Landroid/net/Uri;

.field private final mValues:Landroid/content/ContentValues;

.field private mVideoUri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "display_name"

    aput-object v1, v0, v2

    sput-object v0, Lcom/mediatek/videofavorites/WidgetActionActivity;->CONTACT_PROJECTION:[Ljava/lang/String;

    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "count(*)"

    aput-object v1, v0, v2

    sput-object v0, Lcom/mediatek/videofavorites/WidgetActionActivity;->PROJ_COLS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    iput-object v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mValues:Landroid/content/ContentValues;

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mIndexId:I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mStartTime:J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mPaused:Z

    new-instance v0, Lcom/mediatek/videofavorites/WidgetActionActivity$1;

    invoke-direct {v0, p0}, Lcom/mediatek/videofavorites/WidgetActionActivity$1;-><init>(Lcom/mediatek/videofavorites/WidgetActionActivity;)V

    iput-object v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/videofavorites/WidgetActionActivity;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/videofavorites/WidgetActionActivity;

    iget-object v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/videofavorites/WidgetActionActivity;II)V
    .locals 0
    .param p0    # Lcom/mediatek/videofavorites/WidgetActionActivity;
    .param p1    # I
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/mediatek/videofavorites/WidgetActionActivity;->showToastAndSuicide(II)V

    return-void
.end method

.method static synthetic access$200(Lcom/mediatek/videofavorites/WidgetActionActivity;)J
    .locals 2
    .param p0    # Lcom/mediatek/videofavorites/WidgetActionActivity;

    iget-wide v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mStartTime:J

    return-wide v0
.end method

.method static synthetic access$202(Lcom/mediatek/videofavorites/WidgetActionActivity;J)J
    .locals 0
    .param p0    # Lcom/mediatek/videofavorites/WidgetActionActivity;
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mStartTime:J

    return-wide p1
.end method

.method static synthetic access$300(Lcom/mediatek/videofavorites/WidgetActionActivity;)Z
    .locals 1
    .param p0    # Lcom/mediatek/videofavorites/WidgetActionActivity;

    iget-boolean v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mPaused:Z

    return v0
.end method

.method static synthetic access$500(Lcom/mediatek/videofavorites/WidgetActionActivity;)Landroid/net/Uri;
    .locals 1
    .param p0    # Lcom/mediatek/videofavorites/WidgetActionActivity;

    iget-object v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mVideoUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$502(Lcom/mediatek/videofavorites/WidgetActionActivity;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0
    .param p0    # Lcom/mediatek/videofavorites/WidgetActionActivity;
    .param p1    # Landroid/net/Uri;

    iput-object p1, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mVideoUri:Landroid/net/Uri;

    return-object p1
.end method

.method static synthetic access$600(Lcom/mediatek/videofavorites/WidgetActionActivity;Landroid/net/Uri;)V
    .locals 0
    .param p0    # Lcom/mediatek/videofavorites/WidgetActionActivity;
    .param p1    # Landroid/net/Uri;

    invoke-direct {p0, p1}, Lcom/mediatek/videofavorites/WidgetActionActivity;->insertOrUpdateVideoUriToProvider(Landroid/net/Uri;)V

    return-void
.end method

.method private cancelTranscode()V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    invoke-static {}, Lcom/mediatek/videofavorites/VideoTranscode;->cancel()V

    iget-object v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mProgressDlg:Lcom/mediatek/videofavorites/ProgressDialogFragment;

    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mProgressDlg:Lcom/mediatek/videofavorites/ProgressDialogFragment;

    const v0, 0x7f060007

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method private fireContactSelectActivity()V
    .locals 3

    const/4 v2, 0x1

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.GET_CONTENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "vnd.android.cursor.item/contact"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    iput-boolean v2, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mLeaveForActivity:Z

    invoke-virtual {p0, v0, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private fireVideoSelectActivity()V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "android.intent.action.PICK"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "video/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mLeaveForActivity:Z

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private getContactName(Landroid/net/Uri;)Ljava/lang/String;
    .locals 8
    .param p1    # Landroid/net/Uri;

    const/4 v3, 0x0

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/mediatek/videofavorites/WidgetActionActivity;->CONTACT_PROJECTION:[Ljava/lang/String;

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    const-string v0, "display_name"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return-object v7
.end method

.method private getFavoriteCount()I
    .locals 8

    const/4 v3, 0x0

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/mediatek/videofavorites/VideoFavoritesProviderValues$Columns;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/mediatek/videofavorites/WidgetActionActivity;->PROJ_COLS:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    const-string v0, "WidgetAction"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getFavoriteCount():"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v6
.end method

.method private initActionPick()V
    .locals 4

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v1, "WidgetAction"

    const-string v2, "initActionPick, extra is null"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/videofavorites/WidgetActionActivity;->getFavoriteCount()I

    move-result v1

    const/4 v2, 0x4

    if-lt v1, v2, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "action_pick_type"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mPickType:I

    const-string v1, "WidgetAction"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initActionPick, picktype: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mPickType:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/videofavorites/WidgetActionActivity;->isPickContactOnly()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mVideoUri:Landroid/net/Uri;

    :cond_2
    :goto_1
    const-string v1, "_id"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mIndexId:I

    const-string v1, "WidgetAction"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initActionPick(), indexId:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mIndexId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/mediatek/videofavorites/WidgetActionActivity;->isPickVideoOnly()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-direct {p0}, Lcom/mediatek/videofavorites/WidgetActionActivity;->isStorageAvailable()Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "WidgetAction"

    const-string v2, "storage is low"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/videofavorites/WidgetActionActivity;->showLowStorageToast()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_4
    const-string v1, "WidgetAction"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "put() KEY_CONTACT_URI, value:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mValues:Landroid/content/ContentValues;

    const-string v2, "contact_uri"

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    invoke-direct {p0}, Lcom/mediatek/videofavorites/WidgetActionActivity;->isStorageAvailable()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "WidgetAction"

    const-string v2, "storage is low"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/videofavorites/WidgetActionActivity;->showLowStorageToast()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_1
.end method

.method private initActionView()V
    .locals 11

    const/4 v10, 0x0

    const/4 v9, 0x1

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v4, "_id"

    const/4 v5, -0x1

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mIndexId:I

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/mediatek/videofavorites/WidgetActionActivity;->isValidUri(Landroid/net/Uri;)Z

    move-result v4

    if-eqz v4, :cond_0

    new-instance v1, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v5

    invoke-direct {v1, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    iput-boolean v9, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mLeaveForActivity:Z

    invoke-virtual {p0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    :goto_0
    return-void

    :cond_0
    const-string v4, "WidgetAction"

    const-string v5, "contact is cone, clear it"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "%s/%d"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    sget-object v8, Lcom/mediatek/videofavorites/VideoFavoritesProviderValues$Columns;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    iget v7, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mIndexId:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v4, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mValues:Landroid/content/ContentValues;

    const-string v5, "contact_uri"

    invoke-virtual {v5}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mValues:Landroid/content/ContentValues;

    const-string v5, "name"

    invoke-virtual {v5}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mValues:Landroid/content/ContentValues;

    invoke-virtual {v4, v2, v5, v10, v10}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/videofavorites/WidgetActionActivity;->sendRefreshBroadcast()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method private initDelete()V
    .locals 5

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mUri:Landroid/net/Uri;

    if-nez v0, :cond_0

    const-string v3, "WidgetAction"

    const-string v4, "extras should not be null"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :goto_0
    return-void

    :cond_0
    const-string v3, "com.mediatek.videofavorites.NAME"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f060003

    const v4, 0x7f060004

    invoke-static {v3, v4, v2}, Lcom/mediatek/videofavorites/AlertDialogFragment;->newInstance(IILjava/lang/String;)Lcom/mediatek/videofavorites/AlertDialogFragment;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mAlertDlg:Lcom/mediatek/videofavorites/AlertDialogFragment;

    goto :goto_0
.end method

.method private insertOrUpdateVideoUriToProvider(Landroid/net/Uri;)V
    .locals 7
    .param p1    # Landroid/net/Uri;

    const/4 v6, 0x0

    const-string v1, "WidgetAction"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "insertOrUpdateVideoUriToPrivider(), indexId"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mIndexId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mValues:Landroid/content/ContentValues;

    const-string v2, "video_uri"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget v1, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mIndexId:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/mediatek/videofavorites/VideoFavoritesProviderValues$Columns;->CONTENT_URI:Landroid/net/Uri;

    iget-object v3, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mValues:Landroid/content/ContentValues;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    :goto_0
    invoke-direct {p0}, Lcom/mediatek/videofavorites/WidgetActionActivity;->sendRefreshBroadcast()V

    return-void

    :cond_0
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%s/%d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    sget-object v5, Lcom/mediatek/videofavorites/VideoFavoritesProviderValues$Columns;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget v5, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mIndexId:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mValues:Landroid/content/ContentValues;

    invoke-virtual {v1, v0, v2, v6, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method private isPickContactOnly()Z
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    const-string v3, "WidgetAction"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isPickContactOnly()"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mPickType:I

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mPickType:I

    if-ne v0, v1, :cond_1

    :goto_1
    return v1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method private isPickVideoOnly()Z
    .locals 6

    const/4 v5, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v3, "WidgetAction"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isPickVideoOnly()"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mPickType:I

    if-ne v0, v5, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mPickType:I

    if-ne v0, v5, :cond_1

    :goto_1
    return v1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method private isStorageAvailable()Z
    .locals 5

    sget-object v2, Lcom/mediatek/videofavorites/Storage;->TRANSCODE_PATH_BASE:Ljava/lang/String;

    invoke-static {v2, p0}, Lcom/mediatek/videofavorites/Storage;->getAvailableSpace(Ljava/lang/String;Landroid/content/Context;)J

    move-result-wide v0

    const-string v2, "WidgetAction"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "checkStorageSpace: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/32 v2, 0x80000

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private isTransCodeEnable()Z
    .locals 3

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "key_enable_transcode"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method private isValidUri(Landroid/net/Uri;)Z
    .locals 8
    .param p1    # Landroid/net/Uri;

    const/4 v3, 0x0

    const-string v0, "WidgetAction"

    const-string v1, "isValidUri"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/mediatek/videofavorites/WidgetActionActivity;->CONTACT_PROJECTION:[Ljava/lang/String;

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const/4 v7, 0x1

    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v7, 0x0

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :goto_0
    if-nez v7, :cond_1

    const-string v0, "WidgetAction"

    const-string v1, "contact is gone"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return v7

    :cond_2
    const/4 v7, 0x0

    goto :goto_0
.end method

.method private launchVideoRecorder()V
    .locals 3

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mLeaveForActivity:Z

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.VIDEO_CAPTURE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "android.intent.extra.videoQuality"

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private loadFromBundle(Landroid/os/Bundle;Landroid/content/ContentValues;Ljava/lang/String;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
    .param p2    # Landroid/content/ContentValues;
    .param p3    # Ljava/lang/String;

    if-eqz p2, :cond_0

    if-nez p1, :cond_2

    :cond_0
    const-string v0, "WidgetAction"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to load bundle: b:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", v:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p1, p3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1, p3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, p3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "WidgetAction"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Loaded key:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private saveToBundle(Landroid/os/Bundle;Landroid/content/ContentValues;Ljava/lang/String;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
    .param p2    # Landroid/content/ContentValues;
    .param p3    # Ljava/lang/String;

    if-eqz p2, :cond_0

    if-nez p1, :cond_2

    :cond_0
    const-string v0, "WidgetAction"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to save bundle b:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", v:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p2, p3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "WidgetAction"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "saving key: #"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2, p3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, p3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private sendRefreshBroadcast()V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.mediatek.videofavorites.REFRESH"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method private showLowStorageToast()V
    .locals 2

    const v0, 0x7f06000e

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method private showToastAndSuicide(II)V
    .locals 4
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mProgressDlg:Lcom/mediatek/videofavorites/ProgressDialogFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mProgressDlg:Lcom/mediatek/videofavorites/ProgressDialogFragment;

    invoke-virtual {v0}, Landroid/app/Fragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mProgressDlg:Lcom/mediatek/videofavorites/ProgressDialogFragment;

    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mProgressDlg:Lcom/mediatek/videofavorites/ProgressDialogFragment;

    :cond_0
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    int-to-long v2, p2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method

.method private updateContactToProvider()V
    .locals 7

    const/4 v6, 0x0

    iget-object v1, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mValues:Landroid/content/ContentValues;

    const-string v2, "video_uri"

    iget-object v3, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mVideoUri:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%s/%d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    sget-object v5, Lcom/mediatek/videofavorites/VideoFavoritesProviderValues$Columns;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget v5, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mIndexId:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mValues:Landroid/content/ContentValues;

    invoke-virtual {v1, v0, v2, v6, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/videofavorites/WidgetActionActivity;->sendRefreshBroadcast()V

    return-void
.end method


# virtual methods
.method public getRealPathFromUri(Landroid/net/Uri;)Ljava/lang/String;
    .locals 9
    .param p1    # Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_data"

    aput-object v1, v2, v0

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const-string v0, "_data"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return-object v8
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 8
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/4 v7, 0x3

    const/4 v6, 0x1

    const/4 v5, 0x0

    const-string v2, "WidgetAction"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onActivityResult(): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v5, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mLeaveForActivity:Z

    const/4 v2, -0x1

    if-ne v2, p2, :cond_5

    iput-boolean v5, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mLaunchActivity:Z

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v2, "WidgetAction"

    const-string v3, "data uri is null"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mValues:Landroid/content/ContentValues;

    invoke-virtual {v2}, Landroid/content/ContentValues;->clear()V

    if-ne v7, p1, :cond_0

    const v2, 0x7f060007

    invoke-static {p0, v2, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    if-ne v6, p1, :cond_3

    const-string v2, "WidgetAction"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "contact selected: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mValues:Landroid/content/ContentValues;

    const-string v3, "contact_uri"

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mValues:Landroid/content/ContentValues;

    const-string v3, "name"

    invoke-direct {p0, v0}, Lcom/mediatek/videofavorites/WidgetActionActivity;->getContactName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const/4 v2, 0x2

    if-ne v2, p1, :cond_4

    const-string v2, "WidgetAction"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "video selected: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-object v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mVideoUri:Landroid/net/Uri;

    goto :goto_0

    :cond_4
    if-ne v7, p1, :cond_1

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "video_uri"

    new-instance v3, Ljava/io/File;

    invoke-virtual {p0, v0}, Lcom/mediatek/videofavorites/WidgetActionActivity;->getRealPathFromUri(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/mediatek/videofavorites/VideoFavoritesProviderValues$Columns;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    invoke-direct {p0}, Lcom/mediatek/videofavorites/WidgetActionActivity;->sendRefreshBroadcast()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mValues:Landroid/content/ContentValues;

    invoke-virtual {v2}, Landroid/content/ContentValues;->clear()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0
.end method

.method public onCancel()V
    .locals 4

    invoke-direct {p0}, Lcom/mediatek/videofavorites/WidgetActionActivity;->cancelTranscode()V

    iget-object v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method

.method public onClickNegativeButton()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mAlertDlg:Lcom/mediatek/videofavorites/AlertDialogFragment;

    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismiss()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public onClickPositiveButton()V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mUri:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mAlertDlg:Lcom/mediatek/videofavorites/AlertDialogFragment;

    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismiss()V

    invoke-direct {p0}, Lcom/mediatek/videofavorites/WidgetActionActivity;->sendRefreshBroadcast()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;

    const/4 v8, 0x4

    const/4 v5, 0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v4, "WidgetAction"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "action="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "android.intent.action.PICK"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-direct {p0}, Lcom/mediatek/videofavorites/WidgetActionActivity;->initActionPick()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v4, "android.intent.action.DELETE"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-direct {p0}, Lcom/mediatek/videofavorites/WidgetActionActivity;->initDelete()V

    goto :goto_0

    :cond_2
    const-string v4, "android.intent.action.VIEW"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    iput v8, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mPickType:I

    iput-boolean v5, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mLaunchActivity:Z

    goto :goto_0

    :cond_3
    const-string v4, "action_launch_recorder"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-direct {p0}, Lcom/mediatek/videofavorites/WidgetActionActivity;->getFavoriteCount()I

    move-result v4

    if-lt v4, v8, :cond_4

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_4
    const/4 v4, 0x3

    iput v4, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mPickType:I

    iput-boolean v5, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mLaunchActivity:Z

    goto :goto_0

    :cond_5
    const-string v4, "SWITCH_TRANSCODE"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v4, "key_enable_transcode"

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v6, "key_enable_transcode"

    if-nez v2, :cond_6

    move v4, v5

    :goto_1
    invoke-interface {v1, v6, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "trnascode "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    if-eqz v2, :cond_7

    const-string v4, "disabled"

    :goto_2
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_6
    const/4 v4, 0x0

    goto :goto_1

    :cond_7
    const-string v4, "enabled"

    goto :goto_2

    :cond_8
    const-string v4, "TIMER_TEST"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iput-boolean v5, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mEnableTimerTest:Z

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/app/Activity;->finishActivity(I)V

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Landroid/app/Activity;->finishActivity(I)V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onPause()V
    .locals 2

    const-string v0, "WidgetAction"

    const-string v1, "actionactivity.onPause()"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mEnableTimerTest:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mPaused:Z

    iget-object v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const-string v0, "WidgetAction"

    const-string v1, "onRestoreInstanceState()"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mValues:Landroid/content/ContentValues;

    const-string v1, "name"

    invoke-direct {p0, p1, v0, v1}, Lcom/mediatek/videofavorites/WidgetActionActivity;->loadFromBundle(Landroid/os/Bundle;Landroid/content/ContentValues;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mValues:Landroid/content/ContentValues;

    const-string v1, "contact_uri"

    invoke-direct {p0, p1, v0, v1}, Lcom/mediatek/videofavorites/WidgetActionActivity;->loadFromBundle(Landroid/os/Bundle;Landroid/content/ContentValues;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mValues:Landroid/content/ContentValues;

    const-string v1, "video_uri"

    invoke-direct {p0, p1, v0, v1}, Lcom/mediatek/videofavorites/WidgetActionActivity;->loadFromBundle(Landroid/os/Bundle;Landroid/content/ContentValues;Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public onResume()V
    .locals 6

    const/4 v2, 0x4

    const/4 v5, 0x1

    const/4 v3, 0x3

    const/4 v4, 0x0

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const-string v0, "WidgetAction"

    const-string v1, "onResume()"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v4, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mLeaveForActivity:Z

    iget-object v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mLaunchActivity:Z

    if-eqz v0, :cond_3

    iput-boolean v4, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mLaunchActivity:Z

    iget v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mPickType:I

    if-ne v0, v3, :cond_2

    invoke-direct {p0}, Lcom/mediatek/videofavorites/WidgetActionActivity;->launchVideoRecorder()V

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mPickType:I

    if-ne v0, v2, :cond_0

    invoke-direct {p0}, Lcom/mediatek/videofavorites/WidgetActionActivity;->initActionView()V

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mPickType:I

    if-ne v0, v2, :cond_4

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mAlertDlg:Lcom/mediatek/videofavorites/AlertDialogFragment;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mAlertDlg:Lcom/mediatek/videofavorites/AlertDialogFragment;

    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "alertDialog"

    invoke-virtual {v0, v1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    iget-boolean v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mIsTransCoding:Z

    if-eqz v0, :cond_6

    const-string v0, "WidgetAction"

    const-string v1, "transcoding, skipping rest onResume()"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_6
    iget-boolean v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mEnableTimerTest:Z

    if-eqz v0, :cond_7

    iput-boolean v4, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mPaused:Z

    iget-object v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    const-string v0, "WidgetAction"

    const-string v1, "sendEmptyMessageDelayed(), delay:200"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mStartTime:J

    iget-object v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mHandler:Landroid/os/Handler;

    const-wide/16 v1, 0xc8

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    :cond_7
    const-string v0, "WidgetAction"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mValues.containsKey(KEY_CONTACT_URI): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mValues:Landroid/content/ContentValues;

    const-string v3, "contact_uri"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mValues:Landroid/content/ContentValues;

    const-string v1, "contact_uri"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    const-string v0, "WidgetAction"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mVideoUri: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mVideoUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mVideoUri:Landroid/net/Uri;

    if-nez v0, :cond_9

    invoke-direct {p0}, Lcom/mediatek/videofavorites/WidgetActionActivity;->isStorageAvailable()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-direct {p0}, Lcom/mediatek/videofavorites/WidgetActionActivity;->fireVideoSelectActivity()V

    goto/16 :goto_0

    :cond_8
    invoke-direct {p0}, Lcom/mediatek/videofavorites/WidgetActionActivity;->showLowStorageToast()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    :cond_9
    invoke-direct {p0}, Lcom/mediatek/videofavorites/WidgetActionActivity;->isTransCodeEnable()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-direct {p0}, Lcom/mediatek/videofavorites/WidgetActionActivity;->isPickContactOnly()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-direct {p0}, Lcom/mediatek/videofavorites/WidgetActionActivity;->updateContactToProvider()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    :cond_a
    const/high16 v0, 0x7f060000

    const v1, 0x7f060005

    invoke-static {v0, v1}, Lcom/mediatek/videofavorites/ProgressDialogFragment;->newInstance(II)Lcom/mediatek/videofavorites/ProgressDialogFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mProgressDlg:Lcom/mediatek/videofavorites/ProgressDialogFragment;

    iget-object v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mProgressDlg:Lcom/mediatek/videofavorites/ProgressDialogFragment;

    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "progressDialog"

    invoke-virtual {v0, v1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    iput-boolean v5, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mIsTransCoding:Z

    new-instance v0, Lcom/mediatek/videofavorites/WidgetActionActivity$TranscodeTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/mediatek/videofavorites/WidgetActionActivity$TranscodeTask;-><init>(Lcom/mediatek/videofavorites/WidgetActionActivity;Lcom/mediatek/videofavorites/WidgetActionActivity$1;)V

    new-array v1, v5, [Landroid/net/Uri;

    iget-object v2, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mVideoUri:Landroid/net/Uri;

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    :cond_b
    invoke-direct {p0}, Lcom/mediatek/videofavorites/WidgetActionActivity;->isPickContactOnly()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-direct {p0}, Lcom/mediatek/videofavorites/WidgetActionActivity;->updateContactToProvider()V

    :goto_1
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    :cond_c
    iget-object v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mVideoUri:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lcom/mediatek/videofavorites/WidgetActionActivity;->insertOrUpdateVideoUriToProvider(Landroid/net/Uri;)V

    goto :goto_1

    :cond_d
    invoke-direct {p0}, Lcom/mediatek/videofavorites/WidgetActionActivity;->fireContactSelectActivity()V

    goto/16 :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const-string v0, "WidgetAction"

    const-string v1, "onSaveInstanceState()"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mValues:Landroid/content/ContentValues;

    const-string v1, "name"

    invoke-direct {p0, p1, v0, v1}, Lcom/mediatek/videofavorites/WidgetActionActivity;->saveToBundle(Landroid/os/Bundle;Landroid/content/ContentValues;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mValues:Landroid/content/ContentValues;

    const-string v1, "contact_uri"

    invoke-direct {p0, p1, v0, v1}, Lcom/mediatek/videofavorites/WidgetActionActivity;->saveToBundle(Landroid/os/Bundle;Landroid/content/ContentValues;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mValues:Landroid/content/ContentValues;

    const-string v1, "video_uri"

    invoke-direct {p0, p1, v0, v1}, Lcom/mediatek/videofavorites/WidgetActionActivity;->saveToBundle(Landroid/os/Bundle;Landroid/content/ContentValues;Ljava/lang/String;)V

    return-void
.end method

.method public onUserLeaveHint()V
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mLeaveForActivity:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mAlertDlg:Lcom/mediatek/videofavorites/AlertDialogFragment;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mAlertDlg:Lcom/mediatek/videofavorites/AlertDialogFragment;

    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismiss()V

    :cond_1
    iget-object v0, p0, Lcom/mediatek/videofavorites/WidgetActionActivity;->mProgressDlg:Lcom/mediatek/videofavorites/ProgressDialogFragment;

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/mediatek/videofavorites/WidgetActionActivity;->cancelTranscode()V

    :cond_2
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method
