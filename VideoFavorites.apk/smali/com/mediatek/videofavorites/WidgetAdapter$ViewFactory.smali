.class Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;
.super Ljava/lang/Object;
.source "WidgetAdapter.java"

# interfaces
.implements Landroid/widget/RemoteViewsService$RemoteViewsFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/videofavorites/WidgetAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ViewFactory"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory$AddressObserver;,
        Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory$FavoriteEntry;
    }
.end annotation


# instance fields
.field private mAddView:Landroid/widget/RemoteViews;

.field private final mContactObserver:Landroid/database/ContentObserver;

.field private final mFavoriteEntries:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory$FavoriteEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mHeight:I

.field private mIndexContactUri:I

.field private mIndexId:I

.field private mIndexName:I

.field private mIndexVideoUri:I

.field private final mLayoutId:I

.field private mPackageName:Ljava/lang/String;

.field private mResolver:Landroid/content/ContentResolver;

.field private mWidth:I

.field final synthetic this$0:Lcom/mediatek/videofavorites/WidgetAdapter;


# direct methods
.method public constructor <init>(Lcom/mediatek/videofavorites/WidgetAdapter;I)V
    .locals 2
    .param p2    # I

    iput-object p1, p0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->this$0:Lcom/mediatek/videofavorites/WidgetAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory$AddressObserver;

    iget-object v1, p0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->this$0:Lcom/mediatek/videofavorites/WidgetAdapter;

    invoke-static {v1}, Lcom/mediatek/videofavorites/WidgetAdapter;->access$100(Lcom/mediatek/videofavorites/WidgetAdapter;)Landroid/os/Handler;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory$AddressObserver;-><init>(Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->mContactObserver:Landroid/database/ContentObserver;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->mFavoriteEntries:Ljava/util/ArrayList;

    iput p2, p0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->mLayoutId:I

    return-void
.end method

.method private getAddIcon()Landroid/widget/RemoteViews;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->mAddView:Landroid/widget/RemoteViews;

    return-object v0
.end method

.method private getEmptyView()Landroid/widget/RemoteViews;
    .locals 3

    new-instance v0, Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->mPackageName:Ljava/lang/String;

    const v2, 0x7f030001

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    return-object v0
.end method

.method private getFillInIntentFor(Landroid/net/Uri;I)Landroid/content/Intent;
    .locals 2
    .param p1    # Landroid/net/Uri;
    .param p2    # I

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v1, "_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    return-object v0
.end method

.method private getFillInIntentForPickContact(Landroid/net/Uri;I)Landroid/content/Intent;
    .locals 4
    .param p1    # Landroid/net/Uri;
    .param p2    # I

    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.PICK"

    invoke-direct {v0, v2, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v2, "_id"

    invoke-virtual {v0, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 v1, 0x1

    if-eqz p1, :cond_0

    const-string v2, ""

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v1, 0x0

    :cond_1
    const-string v2, "action_pick_type"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    return-object v0
.end method

.method private getFillInIntentForPickVideo(Landroid/net/Uri;I)Landroid/content/Intent;
    .locals 3
    .param p1    # Landroid/net/Uri;
    .param p2    # I

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    invoke-direct {v0, v1, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v1, "_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "action_pick_type"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    return-object v0
.end method

.method private getFillInIntentOfDeleteIcon(ILjava/lang/String;)Landroid/content/Intent;
    .locals 7
    .param p1    # I
    .param p2    # Ljava/lang/String;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "%s/%d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    sget-object v6, Lcom/mediatek/videofavorites/VideoFavoritesProviderValues$Columns;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.DELETE"

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v2, "android.intent.action.DELETE"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "com.mediatek.videofavorites.NAME"

    invoke-virtual {v2, v3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method private loadEntries()V
    .locals 9

    invoke-virtual {p0}, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->refreshContactdata()Z

    iget-object v0, p0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/mediatek/videofavorites/VideoFavoritesProviderValues$Columns;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {}, Lcom/mediatek/videofavorites/WidgetAdapter;->access$200()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->selection()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->sortOrder()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    invoke-direct {p0, v6}, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->updateColumnIndex(Landroid/database/Cursor;)V

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v7

    iget-object v1, p0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->mFavoriteEntries:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    const/4 v8, 0x0

    :goto_0
    if-ge v8, v7, :cond_0

    new-instance v0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory$FavoriteEntry;

    iget v1, p0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->mIndexVideoUri:I

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget v1, p0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->mIndexName:I

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget v1, p0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->mIndexContactUri:I

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget v1, p0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->mIndexId:I

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory$FavoriteEntry;-><init>(Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->mFavoriteEntries:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_0
    const-string v1, "WidgetAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Entry loaded: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return-void
.end method

.method private selectionContactUri(Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 v5, 0x0

    :goto_0
    return-object v5

    :cond_0
    const-string v1, " OR "

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "contact_uri=\'"

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v4, :cond_2

    const-string v5, "contact_uri=\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v5, v0, 0x1

    if-ge v5, v4, :cond_1

    const-string v5, " OR "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method

.method private updateColumnIndex(Landroid/database/Cursor;)V
    .locals 1
    .param p1    # Landroid/database/Cursor;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v0, "contact_uri"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->mIndexContactUri:I

    const-string v0, "video_uri"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->mIndexVideoUri:I

    const-string v0, "name"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->mIndexName:I

    const-string v0, "_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->mIndexId:I

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    const/4 v0, 0x4

    return v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getLoadingView()Landroid/widget/RemoteViews;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getViewAt(I)Landroid/widget/RemoteViews;
    .locals 11
    .param p1    # I

    const v10, 0x7f080002

    const v9, 0x7f080003

    const-string v4, "WidgetAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getViewAt(): "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->mFavoriteEntries:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge p1, v4, :cond_4

    new-instance v3, Landroid/widget/RemoteViews;

    iget-object v4, p0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->mPackageName:Ljava/lang/String;

    iget v5, p0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->mLayoutId:I

    invoke-direct {v3, v4, v5}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    const/4 v4, 0x4

    invoke-virtual {v3, v9, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    iget-object v4, p0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->mFavoriteEntries:Ljava/util/ArrayList;

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory$FavoriteEntry;

    iget-object v4, p0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->this$0:Lcom/mediatek/videofavorites/WidgetAdapter;

    iget-object v5, p0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->mResolver:Landroid/content/ContentResolver;

    iget-object v6, v1, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory$FavoriteEntry;->videoUri:Ljava/lang/String;

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    iget v7, p0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->mWidth:I

    iget v8, p0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->mHeight:I

    invoke-static {v4, v5, v6, v7, v8}, Lcom/mediatek/videofavorites/WidgetAdapter;->access$300(Lcom/mediatek/videofavorites/WidgetAdapter;Landroid/content/ContentResolver;Landroid/net/Uri;II)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_0

    const v4, 0x7f020016

    invoke-virtual {v3, v10, v4}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    const-string v4, "setVideoUriWithoutOpenVideo"

    const-string v5, ""

    invoke-virtual {v3, v9, v4, v5}, Landroid/widget/RemoteViews;->setString(ILjava/lang/String;Ljava/lang/String;)V

    :goto_0
    const-string v4, ""

    iget-object v5, v1, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory$FavoriteEntry;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    if-nez v0, :cond_1

    const/4 v4, 0x0

    :goto_1
    iget v5, v1, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory$FavoriteEntry;->dbId:I

    invoke-direct {p0, v4, v5}, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->getFillInIntentForPickContact(Landroid/net/Uri;I)Landroid/content/Intent;

    move-result-object v2

    :goto_2
    const v4, 0x7f080006

    iget-object v5, v1, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory$FavoriteEntry;->name:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const/high16 v4, 0x7f080000

    invoke-virtual {v3, v4, v2}, Landroid/widget/RemoteViews;->setOnClickFillInIntent(ILandroid/content/Intent;)V

    const v4, 0x7f080005

    iget v5, v1, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory$FavoriteEntry;->dbId:I

    iget-object v6, v1, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory$FavoriteEntry;->name:Ljava/lang/String;

    invoke-direct {p0, v5, v6}, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->getFillInIntentOfDeleteIcon(ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/widget/RemoteViews;->setOnClickFillInIntent(ILandroid/content/Intent;)V

    :goto_3
    return-object v3

    :cond_0
    invoke-virtual {v3, v10, v0}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    const-string v4, "setVideoUriWithoutOpenVideo"

    iget-object v5, v1, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory$FavoriteEntry;->videoUri:Ljava/lang/String;

    invoke-virtual {v3, v9, v4, v5}, Landroid/widget/RemoteViews;->setString(ILjava/lang/String;Ljava/lang/String;)V

    const-string v4, "setAudioMute"

    const/4 v5, 0x1

    invoke-virtual {v3, v9, v4, v5}, Landroid/widget/RemoteViews;->setBoolean(ILjava/lang/String;Z)V

    goto :goto_0

    :cond_1
    iget-object v4, v1, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory$FavoriteEntry;->videoUri:Ljava/lang/String;

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    goto :goto_1

    :cond_2
    if-nez v0, :cond_3

    iget-object v4, v1, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory$FavoriteEntry;->contactUri:Ljava/lang/String;

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    iget v5, v1, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory$FavoriteEntry;->dbId:I

    invoke-direct {p0, v4, v5}, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->getFillInIntentForPickVideo(Landroid/net/Uri;I)Landroid/content/Intent;

    move-result-object v2

    goto :goto_2

    :cond_3
    iget-object v4, v1, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory$FavoriteEntry;->contactUri:Ljava/lang/String;

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    iget v5, v1, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory$FavoriteEntry;->dbId:I

    invoke-direct {p0, v4, v5}, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->getFillInIntentFor(Landroid/net/Uri;I)Landroid/content/Intent;

    move-result-object v2

    goto :goto_2

    :cond_4
    iget-object v4, p0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->mFavoriteEntries:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ne p1, v4, :cond_5

    invoke-direct {p0}, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->getAddIcon()Landroid/widget/RemoteViews;

    move-result-object v3

    goto :goto_3

    :cond_5
    invoke-direct {p0}, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->getEmptyView()Landroid/widget/RemoteViews;

    move-result-object v3

    goto :goto_3
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public init(Landroid/content/Context;)V
    .locals 4
    .param p1    # Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->mPackageName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->mResolver:Landroid/content/ContentResolver;

    new-instance v0, Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->mPackageName:Ljava/lang/String;

    const v2, 0x7f030002

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->mAddView:Landroid/widget/RemoteViews;

    iget-object v0, p0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->mAddView:Landroid/widget/RemoteViews;

    const v1, 0x7f080009

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.PICK"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setOnClickFillInIntent(ILandroid/content/Intent;)V

    iget-object v0, p0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->this$0:Lcom/mediatek/videofavorites/WidgetAdapter;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f050000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->mWidth:I

    iget-object v0, p0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->this$0:Lcom/mediatek/videofavorites/WidgetAdapter;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->mHeight:I

    return-void
.end method

.method public onContactDataChanged()V
    .locals 1

    invoke-virtual {p0}, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->refreshContactdata()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->this$0:Lcom/mediatek/videofavorites/WidgetAdapter;

    invoke-static {v0}, Lcom/mediatek/videofavorites/WidgetAdapter;->access$500(Lcom/mediatek/videofavorites/WidgetAdapter;)V

    :cond_0
    return-void
.end method

.method public onCreate()V
    .locals 4

    iget-object v0, p0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->mContactObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    invoke-direct {p0}, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->loadEntries()V

    return-void
.end method

.method public onDataSetChanged()V
    .locals 0

    invoke-direct {p0}, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->loadEntries()V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->mResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->mContactObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    return-void
.end method

.method protected projection()[Ljava/lang/String;
    .locals 1

    # getter for: Lcom/mediatek/videofavorites/WidgetAdapter;->PROJECTION:[Ljava/lang/String;
    invoke-static {}, Lcom/mediatek/videofavorites/WidgetAdapter;->access$200()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public refreshContactdata()Z
    .locals 15

    const/4 v13, 0x0

    const-string v0, "WidgetAdapter"

    const-string v1, "onContactDataChanged()"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v11, 0x0

    iget-object v0, p0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->mFavoriteEntries:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->mFavoriteEntries:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    const/4 v9, 0x0

    :goto_1
    if-ge v9, v7, :cond_8

    iget-object v0, p0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->mFavoriteEntries:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory$FavoriteEntry;

    iget-object v0, p0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->mResolver:Landroid/content/ContentResolver;

    iget-object v1, v8, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory$FavoriteEntry;->contactUri:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {}, Lcom/mediatek/videofavorites/WidgetAdapter;->access$400()[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_5

    :cond_1
    const-string v0, "WidgetAdapter"

    const-string v1, "a contact url is gone"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v13, :cond_2

    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    :cond_2
    iget-object v0, v8, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory$FavoriteEntry;->contactUri:Ljava/lang/String;

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    :goto_2
    if-eqz v6, :cond_4

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    :cond_5
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    const-string v0, "display_name"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    iget-object v0, v8, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory$FavoriteEntry;->name:Ljava/lang/String;

    if-nez v0, :cond_6

    if-nez v10, :cond_7

    :cond_6
    iget-object v0, v8, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory$FavoriteEntry;->name:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, v8, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory$FavoriteEntry;->name:Ljava/lang/String;

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_7
    const-string v0, "WidgetAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "name changed from "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v8, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory$FavoriteEntry;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%s/%d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    sget-object v4, Lcom/mediatek/videofavorites/VideoFavoritesProviderValues$Columns;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, v8, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory$FavoriteEntry;->dbId:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v12

    new-instance v14, Landroid/content/ContentValues;

    const/4 v0, 0x1

    invoke-direct {v14, v0}, Landroid/content/ContentValues;-><init>(I)V

    const-string v0, "name"

    invoke-virtual {v14, v0, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->mResolver:Landroid/content/ContentResolver;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v12, v14, v1, v2}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    const/4 v11, 0x1

    goto :goto_2

    :cond_8
    if-eqz v13, :cond_9

    new-instance v14, Landroid/content/ContentValues;

    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    const-string v0, "contact_uri"

    const-string v1, ""

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "name"

    const-string v1, ""

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/mediatek/videofavorites/VideoFavoritesProviderValues$Columns;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {p0, v13}, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->selectionContactUri(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v14, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_9
    if-nez v11, :cond_a

    if-eqz v13, :cond_b

    :cond_a
    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method protected selection()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected sortOrder()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method
