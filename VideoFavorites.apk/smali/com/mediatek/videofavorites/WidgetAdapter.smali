.class public Lcom/mediatek/videofavorites/WidgetAdapter;
.super Landroid/widget/RemoteViewsService;
.source "WidgetAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;
    }
.end annotation


# static fields
.field private static final CONTACT_PROJECTION:[Ljava/lang/String;

.field private static final CONTACT_UPDATE_DATA_DELAY:I = 0x7d0

.field public static final KEY_NAME:Ljava/lang/String; = "com.mediatek.videofavorites.NAME"

.field public static final LARGE_MAX_NUM_VIDEOS:I = 0x4

.field private static final MSG_CONTACT_DATA_CHANGED:I = 0x1

.field private static final PROJECTION:[Ljava/lang/String;

.field private static final PROVIDER_KEY_NAME:Ljava/lang/String; = "name"

.field private static final SORT_ASCENDING:Ljava/lang/String; = " ASC"

.field private static final SORT_DESCENDING:Ljava/lang/String; = " DESC"

.field private static final TAG:Ljava/lang/String; = "WidgetAdapter"


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field private mViewFactory:Landroid/widget/RemoteViewsService$RemoteViewsFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "contact_uri"

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const-string v2, "video_uri"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "name"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mediatek/videofavorites/WidgetAdapter;->PROJECTION:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "display_name"

    aput-object v1, v0, v3

    sput-object v0, Lcom/mediatek/videofavorites/WidgetAdapter;->CONTACT_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/widget/RemoteViewsService;-><init>()V

    new-instance v0, Lcom/mediatek/videofavorites/WidgetAdapter$1;

    invoke-direct {v0, p0}, Lcom/mediatek/videofavorites/WidgetAdapter$1;-><init>(Lcom/mediatek/videofavorites/WidgetAdapter;)V

    iput-object v0, p0, Lcom/mediatek/videofavorites/WidgetAdapter;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/videofavorites/WidgetAdapter;)Landroid/widget/RemoteViewsService$RemoteViewsFactory;
    .locals 1
    .param p0    # Lcom/mediatek/videofavorites/WidgetAdapter;

    iget-object v0, p0, Lcom/mediatek/videofavorites/WidgetAdapter;->mViewFactory:Landroid/widget/RemoteViewsService$RemoteViewsFactory;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/videofavorites/WidgetAdapter;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/videofavorites/WidgetAdapter;

    iget-object v0, p0, Lcom/mediatek/videofavorites/WidgetAdapter;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/mediatek/videofavorites/WidgetAdapter;->PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/videofavorites/WidgetAdapter;Landroid/content/ContentResolver;Landroid/net/Uri;II)Landroid/graphics/Bitmap;
    .locals 1
    .param p0    # Lcom/mediatek/videofavorites/WidgetAdapter;
    .param p1    # Landroid/content/ContentResolver;
    .param p2    # Landroid/net/Uri;
    .param p3    # I
    .param p4    # I

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/mediatek/videofavorites/WidgetAdapter;->getThumbnail(Landroid/content/ContentResolver;Landroid/net/Uri;II)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/mediatek/videofavorites/WidgetAdapter;->CONTACT_PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/mediatek/videofavorites/WidgetAdapter;)V
    .locals 0
    .param p0    # Lcom/mediatek/videofavorites/WidgetAdapter;

    invoke-direct {p0}, Lcom/mediatek/videofavorites/WidgetAdapter;->sendRefreshBroadcast()V

    return-void
.end method

.method public static createVideoThumbnail(Landroid/content/Context;Landroid/net/Uri;)Landroid/graphics/Bitmap;
    .locals 12
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/net/Uri;

    const/4 v0, 0x0

    new-instance v4, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v4}, Landroid/media/MediaMetadataRetriever;-><init>()V

    :try_start_0
    invoke-virtual {v4, p0, p1}, Landroid/media/MediaMetadataRetriever;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    const-wide/16 v9, 0x0

    const/4 v11, 0x3

    invoke-virtual {v4, v9, v10, v11}, Landroid/media/MediaMetadataRetriever;->getFrameAtTime(JI)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V

    :goto_0
    if-nez v0, :cond_0

    const/4 v9, 0x0

    :goto_1
    return-object v9

    :catch_0
    move-exception v9

    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V

    goto :goto_0

    :catch_1
    move-exception v5

    :try_start_1
    const-string v9, "WidgetAdapter"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Runtime Exception occure getting thumbnal: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x0

    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V

    goto :goto_0

    :catchall_0
    move-exception v9

    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V

    throw v9

    :cond_0
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-static {v8, v2}, Ljava/lang/Math;->max(II)I

    move-result v3

    const/16 v9, 0x200

    if-le v3, v9, :cond_1

    const/high16 v9, 0x44000000

    int-to-float v10, v3

    div-float v6, v9, v10

    int-to-float v9, v8

    mul-float/2addr v9, v6

    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    move-result v7

    int-to-float v9, v2

    mul-float/2addr v9, v6

    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    move-result v1

    const/4 v9, 0x1

    invoke-static {v0, v7, v1, v9}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    :cond_1
    move-object v9, v0

    goto :goto_1
.end method

.method private getSourceRect(IIII)Landroid/graphics/Rect;
    .locals 9
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    int-to-float v6, p1

    int-to-float v7, p2

    div-float v3, v6, v7

    int-to-float v6, p3

    int-to-float v7, p4

    div-float v2, v6, v7

    const/4 v0, 0x0

    const/4 v1, 0x0

    cmpg-float v6, v3, v2

    if-gez v6, :cond_0

    move v5, p1

    mul-int v6, v5, p4

    div-int v4, v6, p3

    sub-int v6, p2, v4

    div-int/lit8 v1, v6, 0x2

    :goto_0
    new-instance v6, Landroid/graphics/Rect;

    add-int v7, v0, v5

    add-int v8, v1, v4

    invoke-direct {v6, v0, v1, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v6

    :cond_0
    move v4, p2

    mul-int v6, v4, p3

    div-int v5, v6, p4

    sub-int v6, p1, v5

    div-int/lit8 v0, v6, 0x2

    goto :goto_0
.end method

.method private getThumbnail(Landroid/content/ContentResolver;Landroid/net/Uri;II)Landroid/graphics/Bitmap;
    .locals 10
    .param p1    # Landroid/content/ContentResolver;
    .param p2    # Landroid/net/Uri;
    .param p3    # I
    .param p4    # I

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/mediatek/videofavorites/Storage;->isAvailable(Landroid/content/Context;)Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "WidgetAdapter"

    const-string v8, "Storage is not ready"

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7, p2}, Lcom/mediatek/videofavorites/WidgetAdapter;->createVideoThumbnail(Landroid/content/Context;Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v6

    if-nez v6, :cond_1

    const-string v7, "WidgetAdapter"

    const-string v8, "bitmap create failed!!"

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-direct {p0, v5, v3, p3, p4}, Lcom/mediatek/videofavorites/WidgetAdapter;->getSourceRect(IIII)Landroid/graphics/Rect;

    move-result-object v4

    new-instance v2, Landroid/graphics/Rect;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct {v2, v7, v8, p3, p4}, Landroid/graphics/Rect;-><init>(IIII)V

    const-string v7, "@@@"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "srcRect:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", dstRect:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v7

    invoke-static {p3, p4, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v7, "WidgetAdapter"

    const-string v8, "bitmap create failed!!"

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const/4 v7, 0x0

    invoke-virtual {v0, v7}, Landroid/graphics/Bitmap;->eraseColor(I)V

    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    const/4 v7, 0x0

    invoke-virtual {v1, v6, v4, v2, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0
.end method

.method private sendRefreshBroadcast()V
    .locals 3

    const-string v1, "WidgetAdapter"

    const-string v2, "sending refresh broadcast"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.mediatek.videofavorites.REFRESH"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public onGetViewFactory(Landroid/content/Intent;)Landroid/widget/RemoteViewsService$RemoteViewsFactory;
    .locals 2
    .param p1    # Landroid/content/Intent;

    iget-object v1, p0, Lcom/mediatek/videofavorites/WidgetAdapter;->mViewFactory:Landroid/widget/RemoteViewsService$RemoteViewsFactory;

    if-nez v1, :cond_0

    new-instance v0, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;

    const/high16 v1, 0x7f030000

    invoke-direct {v0, p0, v1}, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;-><init>(Lcom/mediatek/videofavorites/WidgetAdapter;I)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mediatek/videofavorites/WidgetAdapter$ViewFactory;->init(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/videofavorites/WidgetAdapter;->mViewFactory:Landroid/widget/RemoteViewsService$RemoteViewsFactory;

    :cond_0
    iget-object v1, p0, Lcom/mediatek/videofavorites/WidgetAdapter;->mViewFactory:Landroid/widget/RemoteViewsService$RemoteViewsFactory;

    return-object v1
.end method
