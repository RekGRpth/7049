.class public Lcom/mediatek/videofavorites/WidgetXLarge;
.super Lcom/mediatek/videofavorites/AbsVideoFavoritesWidget;
.source "WidgetXLarge.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/mediatek/videofavorites/AbsVideoFavoritesWidget;-><init>()V

    return-void
.end method


# virtual methods
.method protected getCollectionViewId()I
    .locals 1

    const v0, 0x7f08000c

    return v0
.end method

.method protected update(Landroid/content/Context;)Landroid/widget/RemoteViews;
    .locals 8
    .param p1    # Landroid/content/Context;

    const v7, 0x7f08000c

    const v6, 0x7f08000b

    new-instance v3, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f030003

    invoke-direct {v3, v4, v5}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    new-instance v1, Landroid/content/Intent;

    const-class v4, Lcom/mediatek/videofavorites/WidgetAdapter;

    invoke-direct {v1, p1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-virtual {v3, v7, v1}, Landroid/widget/RemoteViews;->setRemoteAdapter(ILandroid/content/Intent;)V

    const-string v4, "setDeleteModeViewId"

    const v5, 0x7f08000e

    invoke-virtual {v3, v6, v4, v5}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    const-string v4, "setDeleteIconId"

    const v5, 0x7f080004

    invoke-virtual {v3, v6, v4, v5}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    const v4, 0x7f08000f

    invoke-virtual {p0, p1}, Lcom/mediatek/videofavorites/AbsVideoFavoritesWidget;->getRecordPendingIntent(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    new-instance v0, Landroid/content/Intent;

    const-class v4, Lcom/mediatek/videofavorites/WidgetActionActivity;

    invoke-direct {v0, p1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v4, 0x14000000

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const/4 v4, 0x0

    const/high16 v5, 0x8000000

    invoke-static {p1, v4, v0, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v3, v7, v2}, Landroid/widget/RemoteViews;->setPendingIntentTemplate(ILandroid/app/PendingIntent;)V

    return-object v3
.end method
