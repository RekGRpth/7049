.class Lcom/mediatek/videoplayer/CachedThumbnail$1;
.super Landroid/os/Handler;
.source "CachedThumbnail.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/videoplayer/CachedThumbnail;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/videoplayer/CachedThumbnail;


# direct methods
.method constructor <init>(Lcom/mediatek/videoplayer/CachedThumbnail;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/videoplayer/CachedThumbnail$1;->this$0:Lcom/mediatek/videoplayer/CachedThumbnail;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1    # Landroid/os/Message;

    iget v4, p1, Landroid/os/Message;->what:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v4, v4, Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;

    if-eqz v4, :cond_0

    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;

    iget-object v0, v3, Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;->mDrawable:Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;

    iget-object v4, p0, Lcom/mediatek/videoplayer/CachedThumbnail$1;->this$0:Lcom/mediatek/videoplayer/CachedThumbnail;

    invoke-static {v4}, Lcom/mediatek/videoplayer/CachedThumbnail;->access$000(Lcom/mediatek/videoplayer/CachedThumbnail;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/videoplayer/CachedThumbnail$DrawableStateListener;

    iget-wide v4, v3, Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;->mRowId:J

    invoke-static {v0}, Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;->access$100(Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;)I

    move-result v6

    invoke-static {v0}, Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;->access$200(Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;)Landroid/graphics/Bitmap;

    move-result-object v7

    invoke-interface {v2, v4, v5, v6, v7}, Lcom/mediatek/videoplayer/CachedThumbnail$DrawableStateListener;->onChanged(JILandroid/graphics/Bitmap;)V

    goto :goto_0

    :cond_0
    return-void
.end method
