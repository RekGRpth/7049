.class public Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;
.super Ljava/lang/Object;
.source "CachedThumbnail.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/videoplayer/CachedThumbnail;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MyDrawable"
.end annotation


# instance fields
.field private mDateModified:J

.field private mDrawable:Landroid/graphics/Bitmap;

.field private mSupport3D:Z

.field private mType:I

.field final synthetic this$0:Lcom/mediatek/videoplayer/CachedThumbnail;


# direct methods
.method public constructor <init>(Lcom/mediatek/videoplayer/CachedThumbnail;Landroid/graphics/Bitmap;IJZ)V
    .locals 0
    .param p2    # Landroid/graphics/Bitmap;
    .param p3    # I
    .param p4    # J
    .param p6    # Z

    iput-object p1, p0, Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;->this$0:Lcom/mediatek/videoplayer/CachedThumbnail;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p3, p0, Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;->mType:I

    iput-object p2, p0, Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;->mDrawable:Landroid/graphics/Bitmap;

    iput-wide p4, p0, Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;->mDateModified:J

    iput-boolean p6, p0, Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;->mSupport3D:Z

    return-void
.end method

.method static synthetic access$100(Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;)I
    .locals 1
    .param p0    # Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;

    iget v0, p0, Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;->mType:I

    return v0
.end method

.method static synthetic access$102(Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;I)I
    .locals 0
    .param p0    # Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;->mType:I

    return p1
.end method

.method static synthetic access$1200(Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;)J
    .locals 2
    .param p0    # Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;

    iget-wide v0, p0, Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;->mDateModified:J

    return-wide v0
.end method

.method static synthetic access$1202(Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;J)J
    .locals 0
    .param p0    # Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;->mDateModified:J

    return-wide p1
.end method

.method static synthetic access$200(Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0    # Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;

    iget-object v0, p0, Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;->mDrawable:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$900(Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;)Z
    .locals 1
    .param p0    # Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;

    iget-boolean v0, p0, Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;->mSupport3D:Z

    return v0
.end method


# virtual methods
.method public set(Landroid/graphics/Bitmap;I)V
    .locals 0
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # I

    iput p2, p0, Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;->mType:I

    iput-object p1, p0, Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;->mDrawable:Landroid/graphics/Bitmap;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MyDrawable(type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;->mType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", drawable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/videoplayer/CachedThumbnail$MyDrawable;->mDrawable:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
