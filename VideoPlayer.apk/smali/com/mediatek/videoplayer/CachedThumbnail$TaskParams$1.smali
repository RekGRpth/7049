.class final Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams$1;
.super Ljava/lang/Object;
.source "CachedThumbnail.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;->getComparator()Ljava/util/Comparator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;)I
    .locals 4
    .param p1    # Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;
    .param p2    # Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;

    iget-wide v0, p1, Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;->mPriority:J

    iget-wide v2, p2, Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;->mPriority:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    iget-wide v0, p1, Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;->mPriority:J

    iget-wide v2, p2, Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;->mPriority:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;

    check-cast p2, Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;

    invoke-virtual {p0, p1, p2}, Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams$1;->compare(Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;Lcom/mediatek/videoplayer/CachedThumbnail$TaskParams;)I

    move-result v0

    return v0
.end method
