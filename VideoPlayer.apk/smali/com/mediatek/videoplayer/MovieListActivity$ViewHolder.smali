.class public Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;
.super Ljava/lang/Object;
.source "MovieListActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/videoplayer/MovieListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ViewHolder"
.end annotation


# instance fields
.field mData:Ljava/lang/String;

.field mDateModified:J

.field mDateTaken:Ljava/lang/Long;

.field mDuration:Ljava/lang/Long;

.field mDurationView:Landroid/widget/TextView;

.field mFastDrawable:Lcom/mediatek/videoplayer/MovieListActivity$FastBitmapDrawable;

.field mFileSize:Ljava/lang/Long;

.field mFileSizeView:Landroid/widget/TextView;

.field mIcon:Landroid/widget/ImageView;

.field mId:J

.field mIsDrm:Z

.field mMimetype:Ljava/lang/String;

.field mSupport3D:Z

.field mTitle:Ljava/lang/String;

.field mTitleView:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/mediatek/videoplayer/MovieListActivity;


# direct methods
.method public constructor <init>(Lcom/mediatek/videoplayer/MovieListActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;->this$0:Lcom/mediatek/videoplayer/MovieListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected clone()Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;
    .locals 3

    new-instance v0, Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;

    iget-object v1, p0, Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;->this$0:Lcom/mediatek/videoplayer/MovieListActivity;

    invoke-direct {v0, v1}, Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;-><init>(Lcom/mediatek/videoplayer/MovieListActivity;)V

    iget-wide v1, p0, Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;->mId:J

    iput-wide v1, v0, Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;->mId:J

    iget-object v1, p0, Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;->mTitle:Ljava/lang/String;

    iput-object v1, v0, Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;->mTitle:Ljava/lang/String;

    iget-object v1, p0, Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;->mMimetype:Ljava/lang/String;

    iput-object v1, v0, Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;->mMimetype:Ljava/lang/String;

    iget-object v1, p0, Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;->mData:Ljava/lang/String;

    iput-object v1, v0, Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;->mData:Ljava/lang/String;

    iget-object v1, p0, Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;->mDuration:Ljava/lang/Long;

    iput-object v1, v0, Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;->mDuration:Ljava/lang/Long;

    iget-object v1, p0, Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;->mDateTaken:Ljava/lang/Long;

    iput-object v1, v0, Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;->mDateTaken:Ljava/lang/Long;

    iget-object v1, p0, Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;->mFileSize:Ljava/lang/Long;

    iput-object v1, v0, Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;->mFileSize:Ljava/lang/Long;

    iget-boolean v1, p0, Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;->mIsDrm:Z

    iput-boolean v1, v0, Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;->mIsDrm:Z

    iget-wide v1, p0, Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;->mDateModified:J

    iput-wide v1, v0, Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;->mDateModified:J

    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;->clone()Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ViewHolder(_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;->mId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isDrm="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;->mIsDrm:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", _data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/videoplayer/MovieListActivity$ViewHolder;->mData:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
