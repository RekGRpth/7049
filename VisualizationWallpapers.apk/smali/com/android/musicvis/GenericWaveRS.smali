.class public Lcom/android/musicvis/GenericWaveRS;
.super Lcom/android/musicvis/RenderScriptScene;
.source "GenericWaveRS.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/musicvis/GenericWaveRS$WorldState;
    }
.end annotation


# static fields
.field private static final DENSITY_MODE_HIGH:I = 0x2

.field private static final DENSITY_MODE_HIGHER:I = 0x4

.field private static final DENSITY_MODE_LOW:I = 0x0

.field private static final DENSITY_MODE_LOWER:I = 0x3

.field private static final DENSITY_MODE_MEDIUM:I = 0x1

.field private static final RSID_LINES:I = 0x2

.field private static final RSID_POINTS:I = 0x1

.field private static final RSID_PROGRAMVERTEX:I = 0x3

.field private static final RSID_STATE:I = 0x0

.field private static final TAG:Ljava/lang/String; = "GenericWave"


# instance fields
.field protected mAudioCapture:Lcom/android/musicvis/AudioCapture;

.field private mCubeMesh:Landroid/renderscript/Mesh;

.field private mDensityMode:I

.field private final mDrawCube:Ljava/lang/Runnable;

.field private final mHandler:Landroid/os/Handler;

.field private mPVAlloc:Landroid/renderscript/ProgramVertexFixedFunction$Constants;

.field private mPVBackground:Landroid/renderscript/ProgramVertex;

.field private mPfBackground:Landroid/renderscript/ProgramFragment;

.field protected mPointAlloc:Landroid/renderscript/Allocation;

.field protected mPointData:[F

.field private mSampler:Landroid/renderscript/Sampler;

.field protected mScale_Param0:I

.field protected mScale_Param1:I

.field mScript:Lcom/android/musicvis/ScriptC_waveform;

.field private mTexId:I

.field private mTexture:Landroid/renderscript/Allocation;

.field private mVertexBuffer:Lcom/android/musicvis/ScriptField_Vertex;

.field private mVisible:Z

.field protected mVizData:[I

.field protected mWorldState:Lcom/android/musicvis/GenericWaveRS$WorldState;


# direct methods
.method protected constructor <init>(III)V
    .locals 7
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v6, 0x0

    invoke-direct {p0, p1, p2}, Lcom/android/musicvis/RenderScriptScene;-><init>(II)V

    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    iput-object v3, p0, Lcom/android/musicvis/GenericWaveRS;->mHandler:Landroid/os/Handler;

    new-instance v3, Lcom/android/musicvis/GenericWaveRS$1;

    invoke-direct {v3, p0}, Lcom/android/musicvis/GenericWaveRS$1;-><init>(Lcom/android/musicvis/GenericWaveRS;)V

    iput-object v3, p0, Lcom/android/musicvis/GenericWaveRS;->mDrawCube:Ljava/lang/Runnable;

    new-instance v3, Lcom/android/musicvis/GenericWaveRS$WorldState;

    invoke-direct {v3}, Lcom/android/musicvis/GenericWaveRS$WorldState;-><init>()V

    iput-object v3, p0, Lcom/android/musicvis/GenericWaveRS;->mWorldState:Lcom/android/musicvis/GenericWaveRS$WorldState;

    const/16 v3, 0x2000

    new-array v3, v3, [F

    iput-object v3, p0, Lcom/android/musicvis/GenericWaveRS;->mPointData:[F

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/android/musicvis/GenericWaveRS;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    const/16 v3, 0x400

    new-array v3, v3, [I

    iput-object v3, p0, Lcom/android/musicvis/GenericWaveRS;->mVizData:[I

    iput p3, p0, Lcom/android/musicvis/GenericWaveRS;->mTexId:I

    iput p1, p0, Lcom/android/musicvis/RenderScriptScene;->mWidth:I

    iput p2, p0, Lcom/android/musicvis/RenderScriptScene;->mHeight:I

    iget-object v3, p0, Lcom/android/musicvis/GenericWaveRS;->mPointData:[F

    array-length v3, v3

    div-int/lit8 v2, v3, 0x8

    div-int/lit8 v0, v2, 0x2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v3, p0, Lcom/android/musicvis/GenericWaveRS;->mPointData:[F

    mul-int/lit8 v4, v1, 0x8

    sub-int v5, v1, v0

    int-to-float v5, v5

    aput v5, v3, v4

    iget-object v3, p0, Lcom/android/musicvis/GenericWaveRS;->mPointData:[F

    mul-int/lit8 v4, v1, 0x8

    add-int/lit8 v4, v4, 0x2

    aput v6, v3, v4

    iget-object v3, p0, Lcom/android/musicvis/GenericWaveRS;->mPointData:[F

    mul-int/lit8 v4, v1, 0x8

    add-int/lit8 v4, v4, 0x3

    aput v6, v3, v4

    iget-object v3, p0, Lcom/android/musicvis/GenericWaveRS;->mPointData:[F

    mul-int/lit8 v4, v1, 0x8

    add-int/lit8 v4, v4, 0x4

    sub-int v5, v1, v0

    int-to-float v5, v5

    aput v5, v3, v4

    iget-object v3, p0, Lcom/android/musicvis/GenericWaveRS;->mPointData:[F

    mul-int/lit8 v4, v1, 0x8

    add-int/lit8 v4, v4, 0x6

    const/high16 v5, 0x3f800000

    aput v5, v3, v4

    iget-object v3, p0, Lcom/android/musicvis/GenericWaveRS;->mPointData:[F

    mul-int/lit8 v4, v1, 0x8

    add-int/lit8 v4, v4, 0x7

    aput v6, v3, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/android/musicvis/GenericWaveRS;->getDensityMode()V

    invoke-direct {p0}, Lcom/android/musicvis/GenericWaveRS;->getConfig()V

    return-void
.end method

.method private getConfig()V
    .locals 6

    const/16 v5, 0x320

    const/16 v4, 0x1e0

    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v1, 0x1

    iget v0, p0, Lcom/android/musicvis/GenericWaveRS;->mDensityMode:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iput v1, p0, Lcom/android/musicvis/GenericWaveRS;->mScale_Param0:I

    iput v2, p0, Lcom/android/musicvis/GenericWaveRS;->mScale_Param1:I

    goto :goto_0

    :pswitch_1
    iget v0, p0, Lcom/android/musicvis/RenderScriptScene;->mWidth:I

    if-lt v0, v5, :cond_0

    iget v0, p0, Lcom/android/musicvis/RenderScriptScene;->mHeight:I

    if-ge v0, v4, :cond_1

    :cond_0
    iget v0, p0, Lcom/android/musicvis/RenderScriptScene;->mWidth:I

    if-lt v0, v4, :cond_2

    iget v0, p0, Lcom/android/musicvis/RenderScriptScene;->mHeight:I

    if-lt v0, v5, :cond_2

    :cond_1
    iput v1, p0, Lcom/android/musicvis/GenericWaveRS;->mScale_Param0:I

    iput v1, p0, Lcom/android/musicvis/GenericWaveRS;->mScale_Param1:I

    goto :goto_0

    :cond_2
    iput v2, p0, Lcom/android/musicvis/GenericWaveRS;->mScale_Param0:I

    iput v3, p0, Lcom/android/musicvis/GenericWaveRS;->mScale_Param1:I

    goto :goto_0

    :pswitch_2
    iput v1, p0, Lcom/android/musicvis/GenericWaveRS;->mScale_Param0:I

    iput v1, p0, Lcom/android/musicvis/GenericWaveRS;->mScale_Param1:I

    goto :goto_0

    :pswitch_3
    iput v3, p0, Lcom/android/musicvis/GenericWaveRS;->mScale_Param0:I

    iput v2, p0, Lcom/android/musicvis/GenericWaveRS;->mScale_Param1:I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private getDensityMode()V
    .locals 3

    const/4 v2, -0x1

    iput v2, p0, Lcom/android/musicvis/GenericWaveRS;->mDensityMode:I

    sget v0, Landroid/util/DisplayMetrics;->DENSITY_DEVICE:I

    sparse-switch v0, :sswitch_data_0

    :goto_0
    iget v1, p0, Lcom/android/musicvis/GenericWaveRS;->mDensityMode:I

    if-ne v1, v2, :cond_0

    if-gez v0, :cond_1

    const/4 v1, 0x3

    iput v1, p0, Lcom/android/musicvis/GenericWaveRS;->mDensityMode:I

    :cond_0
    :goto_1
    return-void

    :sswitch_0
    const/4 v1, 0x0

    iput v1, p0, Lcom/android/musicvis/GenericWaveRS;->mDensityMode:I

    goto :goto_0

    :sswitch_1
    const/4 v1, 0x1

    iput v1, p0, Lcom/android/musicvis/GenericWaveRS;->mDensityMode:I

    goto :goto_0

    :sswitch_2
    const/4 v1, 0x2

    iput v1, p0, Lcom/android/musicvis/GenericWaveRS;->mDensityMode:I

    goto :goto_0

    :cond_1
    const/4 v1, 0x4

    iput v1, p0, Lcom/android/musicvis/GenericWaveRS;->mDensityMode:I

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x78 -> :sswitch_0
        0xa0 -> :sswitch_1
        0xf0 -> :sswitch_2
    .end sparse-switch
.end method


# virtual methods
.method protected createScript()Landroid/renderscript/ScriptC;
    .locals 10

    const/4 v9, 0x0

    new-instance v5, Lcom/android/musicvis/ScriptC_waveform;

    iget-object v6, p0, Lcom/android/musicvis/RenderScriptScene;->mRS:Landroid/renderscript/RenderScriptGL;

    iget-object v7, p0, Lcom/android/musicvis/RenderScriptScene;->mResources:Landroid/content/res/Resources;

    const v8, 0x7f050002

    invoke-direct {v5, v6, v7, v8}, Lcom/android/musicvis/ScriptC_waveform;-><init>(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)V

    iput-object v5, p0, Lcom/android/musicvis/GenericWaveRS;->mScript:Lcom/android/musicvis/ScriptC_waveform;

    iget-object v5, p0, Lcom/android/musicvis/GenericWaveRS;->mWorldState:Lcom/android/musicvis/GenericWaveRS$WorldState;

    const/4 v6, 0x0

    iput v6, v5, Lcom/android/musicvis/GenericWaveRS$WorldState;->yRotation:F

    iget-object v5, p0, Lcom/android/musicvis/GenericWaveRS;->mWorldState:Lcom/android/musicvis/GenericWaveRS$WorldState;

    iget v6, p0, Lcom/android/musicvis/RenderScriptScene;->mWidth:I

    iget v7, p0, Lcom/android/musicvis/GenericWaveRS;->mScale_Param1:I

    mul-int/2addr v6, v7

    iget v7, p0, Lcom/android/musicvis/GenericWaveRS;->mScale_Param0:I

    div-int/2addr v6, v7

    iput v6, v5, Lcom/android/musicvis/GenericWaveRS$WorldState;->width:I

    invoke-virtual {p0}, Lcom/android/musicvis/GenericWaveRS;->updateWorldState()V

    new-instance v3, Landroid/renderscript/ProgramVertexFixedFunction$Builder;

    iget-object v5, p0, Lcom/android/musicvis/RenderScriptScene;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v3, v5}, Landroid/renderscript/ProgramVertexFixedFunction$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    invoke-virtual {v3}, Landroid/renderscript/ProgramVertexFixedFunction$Builder;->create()Landroid/renderscript/ProgramVertexFixedFunction;

    move-result-object v5

    iput-object v5, p0, Lcom/android/musicvis/GenericWaveRS;->mPVBackground:Landroid/renderscript/ProgramVertex;

    new-instance v5, Landroid/renderscript/ProgramVertexFixedFunction$Constants;

    iget-object v6, p0, Lcom/android/musicvis/RenderScriptScene;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v5, v6}, Landroid/renderscript/ProgramVertexFixedFunction$Constants;-><init>(Landroid/renderscript/RenderScript;)V

    iput-object v5, p0, Lcom/android/musicvis/GenericWaveRS;->mPVAlloc:Landroid/renderscript/ProgramVertexFixedFunction$Constants;

    iget-object v5, p0, Lcom/android/musicvis/GenericWaveRS;->mPVBackground:Landroid/renderscript/ProgramVertex;

    check-cast v5, Landroid/renderscript/ProgramVertexFixedFunction;

    iget-object v6, p0, Lcom/android/musicvis/GenericWaveRS;->mPVAlloc:Landroid/renderscript/ProgramVertexFixedFunction$Constants;

    invoke-virtual {v5, v6}, Landroid/renderscript/ProgramVertexFixedFunction;->bindConstants(Landroid/renderscript/ProgramVertexFixedFunction$Constants;)V

    new-instance v2, Landroid/renderscript/Matrix4f;

    invoke-direct {v2}, Landroid/renderscript/Matrix4f;-><init>()V

    iget v5, p0, Lcom/android/musicvis/RenderScriptScene;->mWidth:I

    iget v6, p0, Lcom/android/musicvis/RenderScriptScene;->mHeight:I

    invoke-virtual {v2, v5, v6}, Landroid/renderscript/Matrix4f;->loadProjectionNormalized(II)V

    iget-object v5, p0, Lcom/android/musicvis/GenericWaveRS;->mPVAlloc:Landroid/renderscript/ProgramVertexFixedFunction$Constants;

    invoke-virtual {v5, v2}, Landroid/renderscript/ProgramVertexFixedFunction$Constants;->setProjection(Landroid/renderscript/Matrix4f;)V

    iget-object v5, p0, Lcom/android/musicvis/GenericWaveRS;->mScript:Lcom/android/musicvis/ScriptC_waveform;

    iget-object v6, p0, Lcom/android/musicvis/GenericWaveRS;->mPVBackground:Landroid/renderscript/ProgramVertex;

    invoke-virtual {v5, v6}, Lcom/android/musicvis/ScriptC_waveform;->set_gPVBackground(Landroid/renderscript/ProgramVertex;)V

    new-instance v5, Lcom/android/musicvis/ScriptField_Vertex;

    iget-object v6, p0, Lcom/android/musicvis/RenderScriptScene;->mRS:Landroid/renderscript/RenderScriptGL;

    iget-object v7, p0, Lcom/android/musicvis/GenericWaveRS;->mPointData:[F

    array-length v7, v7

    div-int/lit8 v7, v7, 0x4

    invoke-direct {v5, v6, v7}, Lcom/android/musicvis/ScriptField_Vertex;-><init>(Landroid/renderscript/RenderScript;I)V

    iput-object v5, p0, Lcom/android/musicvis/GenericWaveRS;->mVertexBuffer:Lcom/android/musicvis/ScriptField_Vertex;

    new-instance v1, Landroid/renderscript/Mesh$AllocationBuilder;

    iget-object v5, p0, Lcom/android/musicvis/RenderScriptScene;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v1, v5}, Landroid/renderscript/Mesh$AllocationBuilder;-><init>(Landroid/renderscript/RenderScript;)V

    iget-object v5, p0, Lcom/android/musicvis/GenericWaveRS;->mVertexBuffer:Lcom/android/musicvis/ScriptField_Vertex;

    invoke-virtual {v5}, Landroid/renderscript/Script$FieldBase;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/renderscript/Mesh$AllocationBuilder;->addVertexAllocation(Landroid/renderscript/Allocation;)Landroid/renderscript/Mesh$AllocationBuilder;

    sget-object v5, Landroid/renderscript/Mesh$Primitive;->TRIANGLE_STRIP:Landroid/renderscript/Mesh$Primitive;

    invoke-virtual {v1, v5}, Landroid/renderscript/Mesh$AllocationBuilder;->addIndexSetType(Landroid/renderscript/Mesh$Primitive;)Landroid/renderscript/Mesh$AllocationBuilder;

    invoke-virtual {v1}, Landroid/renderscript/Mesh$AllocationBuilder;->create()Landroid/renderscript/Mesh;

    move-result-object v5

    iput-object v5, p0, Lcom/android/musicvis/GenericWaveRS;->mCubeMesh:Landroid/renderscript/Mesh;

    iget-object v5, p0, Lcom/android/musicvis/GenericWaveRS;->mVertexBuffer:Lcom/android/musicvis/ScriptField_Vertex;

    invoke-virtual {v5}, Landroid/renderscript/Script$FieldBase;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v5

    iput-object v5, p0, Lcom/android/musicvis/GenericWaveRS;->mPointAlloc:Landroid/renderscript/Allocation;

    iget-object v5, p0, Lcom/android/musicvis/GenericWaveRS;->mScript:Lcom/android/musicvis/ScriptC_waveform;

    iget-object v6, p0, Lcom/android/musicvis/GenericWaveRS;->mVertexBuffer:Lcom/android/musicvis/ScriptField_Vertex;

    invoke-virtual {v5, v6}, Lcom/android/musicvis/ScriptC_waveform;->bind_gPoints(Lcom/android/musicvis/ScriptField_Vertex;)V

    iget-object v5, p0, Lcom/android/musicvis/GenericWaveRS;->mScript:Lcom/android/musicvis/ScriptC_waveform;

    iget-object v6, p0, Lcom/android/musicvis/GenericWaveRS;->mPointAlloc:Landroid/renderscript/Allocation;

    invoke-virtual {v5, v6}, Lcom/android/musicvis/ScriptC_waveform;->set_gPointBuffer(Landroid/renderscript/Allocation;)V

    iget-object v5, p0, Lcom/android/musicvis/GenericWaveRS;->mScript:Lcom/android/musicvis/ScriptC_waveform;

    iget-object v6, p0, Lcom/android/musicvis/GenericWaveRS;->mCubeMesh:Landroid/renderscript/Mesh;

    invoke-virtual {v5, v6}, Lcom/android/musicvis/ScriptC_waveform;->set_gCubeMesh(Landroid/renderscript/Mesh;)V

    iget-object v5, p0, Lcom/android/musicvis/GenericWaveRS;->mPointAlloc:Landroid/renderscript/Allocation;

    iget-object v6, p0, Lcom/android/musicvis/GenericWaveRS;->mPointData:[F

    invoke-virtual {v5, v6}, Landroid/renderscript/Allocation;->copyFromUnchecked([F)V

    iget-object v5, p0, Lcom/android/musicvis/RenderScriptScene;->mRS:Landroid/renderscript/RenderScriptGL;

    iget-object v6, p0, Lcom/android/musicvis/RenderScriptScene;->mResources:Landroid/content/res/Resources;

    iget v7, p0, Lcom/android/musicvis/GenericWaveRS;->mTexId:I

    invoke-static {v5, v6, v7}, Landroid/renderscript/Allocation;->createFromBitmapResource(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)Landroid/renderscript/Allocation;

    move-result-object v5

    iput-object v5, p0, Lcom/android/musicvis/GenericWaveRS;->mTexture:Landroid/renderscript/Allocation;

    iget-object v5, p0, Lcom/android/musicvis/GenericWaveRS;->mScript:Lcom/android/musicvis/ScriptC_waveform;

    iget-object v6, p0, Lcom/android/musicvis/GenericWaveRS;->mTexture:Landroid/renderscript/Allocation;

    invoke-virtual {v5, v6}, Lcom/android/musicvis/ScriptC_waveform;->set_gTlinetexture(Landroid/renderscript/Allocation;)V

    new-instance v4, Landroid/renderscript/Sampler$Builder;

    iget-object v5, p0, Lcom/android/musicvis/RenderScriptScene;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v4, v5}, Landroid/renderscript/Sampler$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    sget-object v5, Landroid/renderscript/Sampler$Value;->LINEAR:Landroid/renderscript/Sampler$Value;

    invoke-virtual {v4, v5}, Landroid/renderscript/Sampler$Builder;->setMinification(Landroid/renderscript/Sampler$Value;)V

    sget-object v5, Landroid/renderscript/Sampler$Value;->LINEAR:Landroid/renderscript/Sampler$Value;

    invoke-virtual {v4, v5}, Landroid/renderscript/Sampler$Builder;->setMagnification(Landroid/renderscript/Sampler$Value;)V

    sget-object v5, Landroid/renderscript/Sampler$Value;->WRAP:Landroid/renderscript/Sampler$Value;

    invoke-virtual {v4, v5}, Landroid/renderscript/Sampler$Builder;->setWrapS(Landroid/renderscript/Sampler$Value;)V

    sget-object v5, Landroid/renderscript/Sampler$Value;->WRAP:Landroid/renderscript/Sampler$Value;

    invoke-virtual {v4, v5}, Landroid/renderscript/Sampler$Builder;->setWrapT(Landroid/renderscript/Sampler$Value;)V

    invoke-virtual {v4}, Landroid/renderscript/Sampler$Builder;->create()Landroid/renderscript/Sampler;

    move-result-object v5

    iput-object v5, p0, Lcom/android/musicvis/GenericWaveRS;->mSampler:Landroid/renderscript/Sampler;

    new-instance v0, Landroid/renderscript/ProgramFragmentFixedFunction$Builder;

    iget-object v5, p0, Lcom/android/musicvis/RenderScriptScene;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v0, v5}, Landroid/renderscript/ProgramFragmentFixedFunction$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    sget-object v5, Landroid/renderscript/ProgramFragmentFixedFunction$Builder$EnvMode;->REPLACE:Landroid/renderscript/ProgramFragmentFixedFunction$Builder$EnvMode;

    sget-object v6, Landroid/renderscript/ProgramFragmentFixedFunction$Builder$Format;->RGBA:Landroid/renderscript/ProgramFragmentFixedFunction$Builder$Format;

    invoke-virtual {v0, v5, v6, v9}, Landroid/renderscript/ProgramFragmentFixedFunction$Builder;->setTexture(Landroid/renderscript/ProgramFragmentFixedFunction$Builder$EnvMode;Landroid/renderscript/ProgramFragmentFixedFunction$Builder$Format;I)Landroid/renderscript/ProgramFragmentFixedFunction$Builder;

    invoke-virtual {v0}, Landroid/renderscript/ProgramFragmentFixedFunction$Builder;->create()Landroid/renderscript/ProgramFragmentFixedFunction;

    move-result-object v5

    iput-object v5, p0, Lcom/android/musicvis/GenericWaveRS;->mPfBackground:Landroid/renderscript/ProgramFragment;

    iget-object v5, p0, Lcom/android/musicvis/GenericWaveRS;->mPfBackground:Landroid/renderscript/ProgramFragment;

    iget-object v6, p0, Lcom/android/musicvis/GenericWaveRS;->mSampler:Landroid/renderscript/Sampler;

    invoke-virtual {v5, v6, v9}, Landroid/renderscript/Program;->bindSampler(Landroid/renderscript/Sampler;I)V

    iget-object v5, p0, Lcom/android/musicvis/GenericWaveRS;->mScript:Lcom/android/musicvis/ScriptC_waveform;

    iget-object v6, p0, Lcom/android/musicvis/GenericWaveRS;->mPfBackground:Landroid/renderscript/ProgramFragment;

    invoke-virtual {v5, v6}, Lcom/android/musicvis/ScriptC_waveform;->set_gPFBackground(Landroid/renderscript/ProgramFragment;)V

    iget-object v5, p0, Lcom/android/musicvis/GenericWaveRS;->mScript:Lcom/android/musicvis/ScriptC_waveform;

    return-object v5
.end method

.method public resize(II)V
    .locals 4
    .param p1    # I
    .param p2    # I

    invoke-super {p0, p1, p2}, Lcom/android/musicvis/RenderScriptScene;->resize(II)V

    iget-object v1, p0, Lcom/android/musicvis/GenericWaveRS;->mWorldState:Lcom/android/musicvis/GenericWaveRS$WorldState;

    iget v2, p0, Lcom/android/musicvis/GenericWaveRS;->mScale_Param1:I

    mul-int/2addr v2, p1

    iget v3, p0, Lcom/android/musicvis/GenericWaveRS;->mScale_Param0:I

    div-int/2addr v2, v3

    iput v2, v1, Lcom/android/musicvis/GenericWaveRS$WorldState;->width:I

    iget-object v1, p0, Lcom/android/musicvis/GenericWaveRS;->mPVAlloc:Landroid/renderscript/ProgramVertexFixedFunction$Constants;

    if-eqz v1, :cond_0

    new-instance v0, Landroid/renderscript/Matrix4f;

    invoke-direct {v0}, Landroid/renderscript/Matrix4f;-><init>()V

    iget v1, p0, Lcom/android/musicvis/RenderScriptScene;->mWidth:I

    iget v2, p0, Lcom/android/musicvis/RenderScriptScene;->mHeight:I

    invoke-virtual {v0, v1, v2}, Landroid/renderscript/Matrix4f;->loadProjectionNormalized(II)V

    iget-object v1, p0, Lcom/android/musicvis/GenericWaveRS;->mPVAlloc:Landroid/renderscript/ProgramVertexFixedFunction$Constants;

    invoke-virtual {v1, v0}, Landroid/renderscript/ProgramVertexFixedFunction$Constants;->setProjection(Landroid/renderscript/Matrix4f;)V

    :cond_0
    return-void
.end method

.method public setOffset(FFII)V
    .locals 3
    .param p1    # F
    .param p2    # F
    .param p3    # I
    .param p4    # I

    iget-object v0, p0, Lcom/android/musicvis/GenericWaveRS;->mWorldState:Lcom/android/musicvis/GenericWaveRS$WorldState;

    const/high16 v1, 0x40800000

    mul-float/2addr v1, p1

    const/high16 v2, 0x43340000

    mul-float/2addr v1, v2

    iput v1, v0, Lcom/android/musicvis/GenericWaveRS$WorldState;->yRotation:F

    invoke-virtual {p0}, Lcom/android/musicvis/GenericWaveRS;->updateWorldState()V

    return-void
.end method

.method public start()V
    .locals 2

    invoke-super {p0}, Lcom/android/musicvis/RenderScriptScene;->start()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/musicvis/GenericWaveRS;->mVisible:Z

    iget-object v0, p0, Lcom/android/musicvis/GenericWaveRS;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/musicvis/GenericWaveRS;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    invoke-virtual {v0}, Lcom/android/musicvis/AudioCapture;->start()V

    :cond_0
    const-wide/16 v0, 0xc8

    invoke-static {v0, v1}, Landroid/os/SystemClock;->sleep(J)V

    invoke-virtual {p0}, Lcom/android/musicvis/GenericWaveRS;->updateWave()V

    return-void
.end method

.method public stop()V
    .locals 1

    invoke-super {p0}, Lcom/android/musicvis/RenderScriptScene;->stop()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/musicvis/GenericWaveRS;->mVisible:Z

    iget-object v0, p0, Lcom/android/musicvis/GenericWaveRS;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/musicvis/GenericWaveRS;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    invoke-virtual {v0}, Lcom/android/musicvis/AudioCapture;->stop()V

    :cond_0
    invoke-virtual {p0}, Lcom/android/musicvis/GenericWaveRS;->updateWave()V

    return-void
.end method

.method public update()V
    .locals 0

    return-void
.end method

.method updateWave()V
    .locals 4

    iget-object v0, p0, Lcom/android/musicvis/GenericWaveRS;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/musicvis/GenericWaveRS;->mDrawCube:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-boolean v0, p0, Lcom/android/musicvis/GenericWaveRS;->mVisible:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/musicvis/GenericWaveRS;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/musicvis/GenericWaveRS;->mDrawCube:Ljava/lang/Runnable;

    const-wide/16 v2, 0x14

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    invoke-virtual {p0}, Lcom/android/musicvis/GenericWaveRS;->update()V

    iget-object v0, p0, Lcom/android/musicvis/GenericWaveRS;->mWorldState:Lcom/android/musicvis/GenericWaveRS$WorldState;

    iget v1, v0, Lcom/android/musicvis/GenericWaveRS$WorldState;->waveCounter:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/android/musicvis/GenericWaveRS$WorldState;->waveCounter:I

    invoke-virtual {p0}, Lcom/android/musicvis/GenericWaveRS;->updateWorldState()V

    goto :goto_0
.end method

.method protected updateWorldState()V
    .locals 2

    iget-object v0, p0, Lcom/android/musicvis/GenericWaveRS;->mScript:Lcom/android/musicvis/ScriptC_waveform;

    iget-object v1, p0, Lcom/android/musicvis/GenericWaveRS;->mWorldState:Lcom/android/musicvis/GenericWaveRS$WorldState;

    iget v1, v1, Lcom/android/musicvis/GenericWaveRS$WorldState;->yRotation:F

    invoke-virtual {v0, v1}, Lcom/android/musicvis/ScriptC_waveform;->set_gYRotation(F)V

    iget-object v0, p0, Lcom/android/musicvis/GenericWaveRS;->mScript:Lcom/android/musicvis/ScriptC_waveform;

    iget-object v1, p0, Lcom/android/musicvis/GenericWaveRS;->mWorldState:Lcom/android/musicvis/GenericWaveRS$WorldState;

    iget v1, v1, Lcom/android/musicvis/GenericWaveRS$WorldState;->idle:I

    invoke-virtual {v0, v1}, Lcom/android/musicvis/ScriptC_waveform;->set_gIdle(I)V

    iget-object v0, p0, Lcom/android/musicvis/GenericWaveRS;->mScript:Lcom/android/musicvis/ScriptC_waveform;

    iget-object v1, p0, Lcom/android/musicvis/GenericWaveRS;->mWorldState:Lcom/android/musicvis/GenericWaveRS$WorldState;

    iget v1, v1, Lcom/android/musicvis/GenericWaveRS$WorldState;->waveCounter:I

    invoke-virtual {v0, v1}, Lcom/android/musicvis/ScriptC_waveform;->set_gWaveCounter(I)V

    iget-object v0, p0, Lcom/android/musicvis/GenericWaveRS;->mScript:Lcom/android/musicvis/ScriptC_waveform;

    iget-object v1, p0, Lcom/android/musicvis/GenericWaveRS;->mWorldState:Lcom/android/musicvis/GenericWaveRS$WorldState;

    iget v1, v1, Lcom/android/musicvis/GenericWaveRS$WorldState;->width:I

    invoke-virtual {v0, v1}, Lcom/android/musicvis/ScriptC_waveform;->set_gWidth(I)V

    return-void
.end method
