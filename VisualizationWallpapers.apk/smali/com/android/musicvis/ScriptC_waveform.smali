.class public Lcom/android/musicvis/ScriptC_waveform;
.super Landroid/renderscript/ScriptC;
.source "ScriptC_waveform.java"


# static fields
.field private static final mExportVarIdx_gCubeMesh:I = 0x9

.field private static final mExportVarIdx_gIdle:I = 0x1

.field private static final mExportVarIdx_gPFBackground:I = 0x5

.field private static final mExportVarIdx_gPVBackground:I = 0x4

.field private static final mExportVarIdx_gPointBuffer:I = 0x7

.field private static final mExportVarIdx_gPoints:I = 0x6

.field private static final mExportVarIdx_gTlinetexture:I = 0x8

.field private static final mExportVarIdx_gWaveCounter:I = 0x2

.field private static final mExportVarIdx_gWidth:I = 0x3

.field private static final mExportVarIdx_gYRotation:I


# instance fields
.field private __ALLOCATION:Landroid/renderscript/Element;

.field private __F32:Landroid/renderscript/Element;

.field private __I32:Landroid/renderscript/Element;

.field private __MESH:Landroid/renderscript/Element;

.field private __PROGRAM_FRAGMENT:Landroid/renderscript/Element;

.field private __PROGRAM_VERTEX:Landroid/renderscript/Element;

.field private mExportVar_gCubeMesh:Landroid/renderscript/Mesh;

.field private mExportVar_gIdle:I

.field private mExportVar_gPFBackground:Landroid/renderscript/ProgramFragment;

.field private mExportVar_gPVBackground:Landroid/renderscript/ProgramVertex;

.field private mExportVar_gPointBuffer:Landroid/renderscript/Allocation;

.field private mExportVar_gPoints:Lcom/android/musicvis/ScriptField_Vertex;

.field private mExportVar_gTlinetexture:Landroid/renderscript/Allocation;

.field private mExportVar_gWaveCounter:I

.field private mExportVar_gWidth:I

.field private mExportVar_gYRotation:F


# direct methods
.method public constructor <init>(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)V
    .locals 1
    .param p1    # Landroid/renderscript/RenderScript;
    .param p2    # Landroid/content/res/Resources;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/renderscript/ScriptC;-><init>(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)V

    invoke-static {p1}, Landroid/renderscript/Element;->F32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/musicvis/ScriptC_waveform;->__F32:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->I32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/musicvis/ScriptC_waveform;->__I32:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->PROGRAM_VERTEX(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/musicvis/ScriptC_waveform;->__PROGRAM_VERTEX:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->PROGRAM_FRAGMENT(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/musicvis/ScriptC_waveform;->__PROGRAM_FRAGMENT:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->ALLOCATION(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/musicvis/ScriptC_waveform;->__ALLOCATION:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->MESH(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/musicvis/ScriptC_waveform;->__MESH:Landroid/renderscript/Element;

    return-void
.end method


# virtual methods
.method public bind_gPoints(Lcom/android/musicvis/ScriptField_Vertex;)V
    .locals 2
    .param p1    # Lcom/android/musicvis/ScriptField_Vertex;

    const/4 v1, 0x6

    iput-object p1, p0, Lcom/android/musicvis/ScriptC_waveform;->mExportVar_gPoints:Lcom/android/musicvis/ScriptField_Vertex;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/renderscript/Script;->bindAllocation(Landroid/renderscript/Allocation;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/renderscript/Script$FieldBase;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Landroid/renderscript/Script;->bindAllocation(Landroid/renderscript/Allocation;I)V

    goto :goto_0
.end method

.method public get_gCubeMesh()Landroid/renderscript/Mesh;
    .locals 1

    iget-object v0, p0, Lcom/android/musicvis/ScriptC_waveform;->mExportVar_gCubeMesh:Landroid/renderscript/Mesh;

    return-object v0
.end method

.method public get_gIdle()I
    .locals 1

    iget v0, p0, Lcom/android/musicvis/ScriptC_waveform;->mExportVar_gIdle:I

    return v0
.end method

.method public get_gPFBackground()Landroid/renderscript/ProgramFragment;
    .locals 1

    iget-object v0, p0, Lcom/android/musicvis/ScriptC_waveform;->mExportVar_gPFBackground:Landroid/renderscript/ProgramFragment;

    return-object v0
.end method

.method public get_gPVBackground()Landroid/renderscript/ProgramVertex;
    .locals 1

    iget-object v0, p0, Lcom/android/musicvis/ScriptC_waveform;->mExportVar_gPVBackground:Landroid/renderscript/ProgramVertex;

    return-object v0
.end method

.method public get_gPointBuffer()Landroid/renderscript/Allocation;
    .locals 1

    iget-object v0, p0, Lcom/android/musicvis/ScriptC_waveform;->mExportVar_gPointBuffer:Landroid/renderscript/Allocation;

    return-object v0
.end method

.method public get_gPoints()Lcom/android/musicvis/ScriptField_Vertex;
    .locals 1

    iget-object v0, p0, Lcom/android/musicvis/ScriptC_waveform;->mExportVar_gPoints:Lcom/android/musicvis/ScriptField_Vertex;

    return-object v0
.end method

.method public get_gTlinetexture()Landroid/renderscript/Allocation;
    .locals 1

    iget-object v0, p0, Lcom/android/musicvis/ScriptC_waveform;->mExportVar_gTlinetexture:Landroid/renderscript/Allocation;

    return-object v0
.end method

.method public get_gWaveCounter()I
    .locals 1

    iget v0, p0, Lcom/android/musicvis/ScriptC_waveform;->mExportVar_gWaveCounter:I

    return v0
.end method

.method public get_gWidth()I
    .locals 1

    iget v0, p0, Lcom/android/musicvis/ScriptC_waveform;->mExportVar_gWidth:I

    return v0
.end method

.method public get_gYRotation()F
    .locals 1

    iget v0, p0, Lcom/android/musicvis/ScriptC_waveform;->mExportVar_gYRotation:F

    return v0
.end method

.method public set_gCubeMesh(Landroid/renderscript/Mesh;)V
    .locals 1
    .param p1    # Landroid/renderscript/Mesh;

    iput-object p1, p0, Lcom/android/musicvis/ScriptC_waveform;->mExportVar_gCubeMesh:Landroid/renderscript/Mesh;

    const/16 v0, 0x9

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gIdle(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/android/musicvis/ScriptC_waveform;->mExportVar_gIdle:I

    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(II)V

    return-void
.end method

.method public set_gPFBackground(Landroid/renderscript/ProgramFragment;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramFragment;

    iput-object p1, p0, Lcom/android/musicvis/ScriptC_waveform;->mExportVar_gPFBackground:Landroid/renderscript/ProgramFragment;

    const/4 v0, 0x5

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gPVBackground(Landroid/renderscript/ProgramVertex;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramVertex;

    iput-object p1, p0, Lcom/android/musicvis/ScriptC_waveform;->mExportVar_gPVBackground:Landroid/renderscript/ProgramVertex;

    const/4 v0, 0x4

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gPointBuffer(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    iput-object p1, p0, Lcom/android/musicvis/ScriptC_waveform;->mExportVar_gPointBuffer:Landroid/renderscript/Allocation;

    const/4 v0, 0x7

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gTlinetexture(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    iput-object p1, p0, Lcom/android/musicvis/ScriptC_waveform;->mExportVar_gTlinetexture:Landroid/renderscript/Allocation;

    const/16 v0, 0x8

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gWaveCounter(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/android/musicvis/ScriptC_waveform;->mExportVar_gWaveCounter:I

    const/4 v0, 0x2

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(II)V

    return-void
.end method

.method public set_gWidth(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/android/musicvis/ScriptC_waveform;->mExportVar_gWidth:I

    const/4 v0, 0x3

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(II)V

    return-void
.end method

.method public set_gYRotation(F)V
    .locals 1
    .param p1    # F

    iput p1, p0, Lcom/android/musicvis/ScriptC_waveform;->mExportVar_gYRotation:F

    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(IF)V

    return-void
.end method
