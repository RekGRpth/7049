.class Lcom/android/musicvis/vis3/Visualization3RS;
.super Lcom/android/musicvis/GenericWaveRS;
.source "Visualization3RS.java"


# instance fields
.field lastOffset:F

.field private mAnalyzer:[S


# direct methods
.method constructor <init>(II)V
    .locals 1
    .param p1    # I
    .param p2    # I

    const v0, 0x7f020006

    invoke-direct {p0, p1, p2, v0}, Lcom/android/musicvis/GenericWaveRS;-><init>(III)V

    const/16 v0, 0x200

    new-array v0, v0, [S

    iput-object v0, p0, Lcom/android/musicvis/vis3/Visualization3RS;->mAnalyzer:[S

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/musicvis/vis3/Visualization3RS;->lastOffset:F

    return-void
.end method


# virtual methods
.method public setOffset(FFII)V
    .locals 3
    .param p1    # F
    .param p2    # F
    .param p3    # I
    .param p4    # I

    iget-object v0, p0, Lcom/android/musicvis/GenericWaveRS;->mWorldState:Lcom/android/musicvis/GenericWaveRS$WorldState;

    const/high16 v1, 0x40800000

    mul-float/2addr v1, p1

    const/high16 v2, 0x43b40000

    mul-float/2addr v1, v2

    iput v1, v0, Lcom/android/musicvis/GenericWaveRS$WorldState;->yRotation:F

    invoke-virtual {p0}, Lcom/android/musicvis/GenericWaveRS;->updateWorldState()V

    return-void
.end method

.method public start()V
    .locals 3

    iget-object v0, p0, Lcom/android/musicvis/GenericWaveRS;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/musicvis/AudioCapture;

    const/4 v1, 0x1

    const/16 v2, 0x200

    invoke-direct {v0, v1, v2}, Lcom/android/musicvis/AudioCapture;-><init>(II)V

    iput-object v0, p0, Lcom/android/musicvis/GenericWaveRS;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    :cond_0
    invoke-super {p0}, Lcom/android/musicvis/GenericWaveRS;->start()V

    return-void
.end method

.method public stop()V
    .locals 1

    invoke-super {p0}, Lcom/android/musicvis/GenericWaveRS;->stop()V

    iget-object v0, p0, Lcom/android/musicvis/GenericWaveRS;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/musicvis/GenericWaveRS;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    invoke-virtual {v0}, Lcom/android/musicvis/AudioCapture;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/musicvis/GenericWaveRS;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    :cond_0
    return-void
.end method

.method public update()V
    .locals 15

    const/4 v13, 0x1

    const/4 v2, 0x0

    iget-object v12, p0, Lcom/android/musicvis/GenericWaveRS;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    if-eqz v12, :cond_0

    iget-object v12, p0, Lcom/android/musicvis/GenericWaveRS;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    invoke-virtual {v12, v13, v13}, Lcom/android/musicvis/AudioCapture;->getFormattedData(II)[I

    move-result-object v12

    iput-object v12, p0, Lcom/android/musicvis/GenericWaveRS;->mVizData:[I

    iget-object v12, p0, Lcom/android/musicvis/GenericWaveRS;->mVizData:[I

    array-length v12, v12

    div-int/lit8 v2, v12, 0x2

    :cond_0
    if-nez v2, :cond_2

    iget-object v12, p0, Lcom/android/musicvis/GenericWaveRS;->mWorldState:Lcom/android/musicvis/GenericWaveRS$WorldState;

    iget v12, v12, Lcom/android/musicvis/GenericWaveRS$WorldState;->idle:I

    if-nez v12, :cond_1

    iget-object v12, p0, Lcom/android/musicvis/GenericWaveRS;->mWorldState:Lcom/android/musicvis/GenericWaveRS$WorldState;

    iput v13, v12, Lcom/android/musicvis/GenericWaveRS$WorldState;->idle:I

    invoke-virtual {p0}, Lcom/android/musicvis/GenericWaveRS;->updateWorldState()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    div-int/lit8 v2, v2, 0x2

    iget-object v12, p0, Lcom/android/musicvis/vis3/Visualization3RS;->mAnalyzer:[S

    array-length v12, v12

    if-le v2, v12, :cond_3

    iget-object v12, p0, Lcom/android/musicvis/vis3/Visualization3RS;->mAnalyzer:[S

    array-length v2, v12

    :cond_3
    iget-object v12, p0, Lcom/android/musicvis/GenericWaveRS;->mWorldState:Lcom/android/musicvis/GenericWaveRS$WorldState;

    iget v12, v12, Lcom/android/musicvis/GenericWaveRS$WorldState;->idle:I

    if-eqz v12, :cond_4

    iget-object v12, p0, Lcom/android/musicvis/GenericWaveRS;->mWorldState:Lcom/android/musicvis/GenericWaveRS$WorldState;

    const/4 v13, 0x0

    iput v13, v12, Lcom/android/musicvis/GenericWaveRS$WorldState;->idle:I

    invoke-virtual {p0}, Lcom/android/musicvis/GenericWaveRS;->updateWorldState()V

    :cond_4
    const/4 v1, 0x1

    :goto_1
    add-int/lit8 v12, v2, -0x1

    if-ge v1, v12, :cond_6

    iget-object v12, p0, Lcom/android/musicvis/GenericWaveRS;->mVizData:[I

    mul-int/lit8 v13, v1, 0x2

    aget v9, v12, v13

    iget-object v12, p0, Lcom/android/musicvis/GenericWaveRS;->mVizData:[I

    mul-int/lit8 v13, v1, 0x2

    add-int/lit8 v13, v13, 0x1

    aget v10, v12, v13

    mul-int v12, v9, v9

    mul-int v13, v10, v10

    add-int v8, v12, v13

    div-int/lit8 v12, v1, 0x10

    add-int/lit8 v12, v12, 0x1

    mul-int/2addr v12, v8

    int-to-short v3, v12

    iget-object v12, p0, Lcom/android/musicvis/vis3/Visualization3RS;->mAnalyzer:[S

    aget-short v4, v12, v1

    add-int/lit16 v12, v4, -0x320

    if-lt v3, v12, :cond_5

    :goto_2
    iget-object v12, p0, Lcom/android/musicvis/vis3/Visualization3RS;->mAnalyzer:[S

    aput-short v3, v12, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_5
    add-int/lit16 v12, v4, -0x320

    int-to-short v3, v12

    goto :goto_2

    :cond_6
    iget-object v12, p0, Lcom/android/musicvis/GenericWaveRS;->mPointData:[F

    array-length v12, v12

    div-int/lit8 v5, v12, 0x8

    iget v12, p0, Lcom/android/musicvis/RenderScriptScene;->mWidth:I

    iget v13, p0, Lcom/android/musicvis/GenericWaveRS;->mScale_Param1:I

    mul-int/2addr v12, v13

    iget v13, p0, Lcom/android/musicvis/GenericWaveRS;->mScale_Param0:I

    div-int v11, v12, v13

    sub-int v12, v5, v11

    div-int/lit8 v6, v12, 0x2

    const/4 v7, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_3
    if-ge v1, v11, :cond_9

    iget-object v12, p0, Lcom/android/musicvis/vis3/Visualization3RS;->mAnalyzer:[S

    aget-short v12, v12, v7

    div-int/lit8 v12, v12, 0x8

    int-to-float v8, v12

    const/high16 v12, 0x3f800000

    cmpg-float v12, v8, v12

    if-gez v12, :cond_7

    const/high16 v12, -0x40800000

    cmpl-float v12, v8, v12

    if-lez v12, :cond_7

    const/high16 v8, 0x3f800000

    :cond_7
    iget-object v12, p0, Lcom/android/musicvis/GenericWaveRS;->mPointData:[F

    add-int v13, v1, v6

    mul-int/lit8 v13, v13, 0x8

    add-int/lit8 v13, v13, 0x1

    aput v8, v12, v13

    iget-object v12, p0, Lcom/android/musicvis/GenericWaveRS;->mPointData:[F

    add-int v13, v1, v6

    mul-int/lit8 v13, v13, 0x8

    add-int/lit8 v13, v13, 0x5

    neg-float v14, v8

    aput v14, v12, v13

    add-int/2addr v0, v2

    if-le v0, v11, :cond_8

    add-int/lit8 v7, v7, 0x1

    sub-int/2addr v0, v11

    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_9
    iget-object v12, p0, Lcom/android/musicvis/GenericWaveRS;->mPointAlloc:Landroid/renderscript/Allocation;

    iget-object v13, p0, Lcom/android/musicvis/GenericWaveRS;->mPointData:[F

    invoke-virtual {v12, v13}, Landroid/renderscript/Allocation;->copyFromUnchecked([F)V

    goto/16 :goto_0
.end method
