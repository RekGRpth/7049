.class public Lcom/android/musicvis/vis4/ScriptC_vu;
.super Landroid/renderscript/ScriptC;
.source "ScriptC_vu.java"


# static fields
.field private static final mExportVarIdx_gAngle:I = 0x0

.field private static final mExportVarIdx_gPFBackground:I = 0x3

.field private static final mExportVarIdx_gPFSBackground:I = 0xa

.field private static final mExportVarIdx_gPVBackground:I = 0x2

.field private static final mExportVarIdx_gPeak:I = 0x1

.field private static final mExportVarIdx_gTvumeter_background:I = 0x4

.field private static final mExportVarIdx_gTvumeter_black:I = 0x8

.field private static final mExportVarIdx_gTvumeter_frame:I = 0x9

.field private static final mExportVarIdx_gTvumeter_needle:I = 0x7

.field private static final mExportVarIdx_gTvumeter_peak_off:I = 0x6

.field private static final mExportVarIdx_gTvumeter_peak_on:I = 0x5


# instance fields
.field private __ALLOCATION:Landroid/renderscript/Element;

.field private __F32:Landroid/renderscript/Element;

.field private __I32:Landroid/renderscript/Element;

.field private __PROGRAM_FRAGMENT:Landroid/renderscript/Element;

.field private __PROGRAM_STORE:Landroid/renderscript/Element;

.field private __PROGRAM_VERTEX:Landroid/renderscript/Element;

.field private mExportVar_gAngle:F

.field private mExportVar_gPFBackground:Landroid/renderscript/ProgramFragment;

.field private mExportVar_gPFSBackground:Landroid/renderscript/ProgramStore;

.field private mExportVar_gPVBackground:Landroid/renderscript/ProgramVertex;

.field private mExportVar_gPeak:I

.field private mExportVar_gTvumeter_background:Landroid/renderscript/Allocation;

.field private mExportVar_gTvumeter_black:Landroid/renderscript/Allocation;

.field private mExportVar_gTvumeter_frame:Landroid/renderscript/Allocation;

.field private mExportVar_gTvumeter_needle:Landroid/renderscript/Allocation;

.field private mExportVar_gTvumeter_peak_off:Landroid/renderscript/Allocation;

.field private mExportVar_gTvumeter_peak_on:Landroid/renderscript/Allocation;


# direct methods
.method public constructor <init>(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)V
    .locals 1
    .param p1    # Landroid/renderscript/RenderScript;
    .param p2    # Landroid/content/res/Resources;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/renderscript/ScriptC;-><init>(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)V

    invoke-static {p1}, Landroid/renderscript/Element;->F32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/musicvis/vis4/ScriptC_vu;->__F32:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->I32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/musicvis/vis4/ScriptC_vu;->__I32:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->PROGRAM_VERTEX(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/musicvis/vis4/ScriptC_vu;->__PROGRAM_VERTEX:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->PROGRAM_FRAGMENT(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/musicvis/vis4/ScriptC_vu;->__PROGRAM_FRAGMENT:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->ALLOCATION(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/musicvis/vis4/ScriptC_vu;->__ALLOCATION:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->PROGRAM_STORE(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/musicvis/vis4/ScriptC_vu;->__PROGRAM_STORE:Landroid/renderscript/Element;

    return-void
.end method


# virtual methods
.method public get_gAngle()F
    .locals 1

    iget v0, p0, Lcom/android/musicvis/vis4/ScriptC_vu;->mExportVar_gAngle:F

    return v0
.end method

.method public get_gPFBackground()Landroid/renderscript/ProgramFragment;
    .locals 1

    iget-object v0, p0, Lcom/android/musicvis/vis4/ScriptC_vu;->mExportVar_gPFBackground:Landroid/renderscript/ProgramFragment;

    return-object v0
.end method

.method public get_gPFSBackground()Landroid/renderscript/ProgramStore;
    .locals 1

    iget-object v0, p0, Lcom/android/musicvis/vis4/ScriptC_vu;->mExportVar_gPFSBackground:Landroid/renderscript/ProgramStore;

    return-object v0
.end method

.method public get_gPVBackground()Landroid/renderscript/ProgramVertex;
    .locals 1

    iget-object v0, p0, Lcom/android/musicvis/vis4/ScriptC_vu;->mExportVar_gPVBackground:Landroid/renderscript/ProgramVertex;

    return-object v0
.end method

.method public get_gPeak()I
    .locals 1

    iget v0, p0, Lcom/android/musicvis/vis4/ScriptC_vu;->mExportVar_gPeak:I

    return v0
.end method

.method public get_gTvumeter_background()Landroid/renderscript/Allocation;
    .locals 1

    iget-object v0, p0, Lcom/android/musicvis/vis4/ScriptC_vu;->mExportVar_gTvumeter_background:Landroid/renderscript/Allocation;

    return-object v0
.end method

.method public get_gTvumeter_black()Landroid/renderscript/Allocation;
    .locals 1

    iget-object v0, p0, Lcom/android/musicvis/vis4/ScriptC_vu;->mExportVar_gTvumeter_black:Landroid/renderscript/Allocation;

    return-object v0
.end method

.method public get_gTvumeter_frame()Landroid/renderscript/Allocation;
    .locals 1

    iget-object v0, p0, Lcom/android/musicvis/vis4/ScriptC_vu;->mExportVar_gTvumeter_frame:Landroid/renderscript/Allocation;

    return-object v0
.end method

.method public get_gTvumeter_needle()Landroid/renderscript/Allocation;
    .locals 1

    iget-object v0, p0, Lcom/android/musicvis/vis4/ScriptC_vu;->mExportVar_gTvumeter_needle:Landroid/renderscript/Allocation;

    return-object v0
.end method

.method public get_gTvumeter_peak_off()Landroid/renderscript/Allocation;
    .locals 1

    iget-object v0, p0, Lcom/android/musicvis/vis4/ScriptC_vu;->mExportVar_gTvumeter_peak_off:Landroid/renderscript/Allocation;

    return-object v0
.end method

.method public get_gTvumeter_peak_on()Landroid/renderscript/Allocation;
    .locals 1

    iget-object v0, p0, Lcom/android/musicvis/vis4/ScriptC_vu;->mExportVar_gTvumeter_peak_on:Landroid/renderscript/Allocation;

    return-object v0
.end method

.method public set_gAngle(F)V
    .locals 1
    .param p1    # F

    iput p1, p0, Lcom/android/musicvis/vis4/ScriptC_vu;->mExportVar_gAngle:F

    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(IF)V

    return-void
.end method

.method public set_gPFBackground(Landroid/renderscript/ProgramFragment;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramFragment;

    iput-object p1, p0, Lcom/android/musicvis/vis4/ScriptC_vu;->mExportVar_gPFBackground:Landroid/renderscript/ProgramFragment;

    const/4 v0, 0x3

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gPFSBackground(Landroid/renderscript/ProgramStore;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramStore;

    iput-object p1, p0, Lcom/android/musicvis/vis4/ScriptC_vu;->mExportVar_gPFSBackground:Landroid/renderscript/ProgramStore;

    const/16 v0, 0xa

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gPVBackground(Landroid/renderscript/ProgramVertex;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramVertex;

    iput-object p1, p0, Lcom/android/musicvis/vis4/ScriptC_vu;->mExportVar_gPVBackground:Landroid/renderscript/ProgramVertex;

    const/4 v0, 0x2

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gPeak(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/android/musicvis/vis4/ScriptC_vu;->mExportVar_gPeak:I

    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(II)V

    return-void
.end method

.method public set_gTvumeter_background(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    iput-object p1, p0, Lcom/android/musicvis/vis4/ScriptC_vu;->mExportVar_gTvumeter_background:Landroid/renderscript/Allocation;

    const/4 v0, 0x4

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gTvumeter_black(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    iput-object p1, p0, Lcom/android/musicvis/vis4/ScriptC_vu;->mExportVar_gTvumeter_black:Landroid/renderscript/Allocation;

    const/16 v0, 0x8

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gTvumeter_frame(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    iput-object p1, p0, Lcom/android/musicvis/vis4/ScriptC_vu;->mExportVar_gTvumeter_frame:Landroid/renderscript/Allocation;

    const/16 v0, 0x9

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gTvumeter_needle(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    iput-object p1, p0, Lcom/android/musicvis/vis4/ScriptC_vu;->mExportVar_gTvumeter_needle:Landroid/renderscript/Allocation;

    const/4 v0, 0x7

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gTvumeter_peak_off(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    iput-object p1, p0, Lcom/android/musicvis/vis4/ScriptC_vu;->mExportVar_gTvumeter_peak_off:Landroid/renderscript/Allocation;

    const/4 v0, 0x6

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gTvumeter_peak_on(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    iput-object p1, p0, Lcom/android/musicvis/vis4/ScriptC_vu;->mExportVar_gTvumeter_peak_on:Landroid/renderscript/Allocation;

    const/4 v0, 0x5

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method
