.class public Lcom/android/musicvis/vis5/ScriptC_many;
.super Landroid/renderscript/ScriptC;
.source "ScriptC_many.java"


# static fields
.field private static final mExportVarIdx_autorotation:I = 0x23

.field private static final mExportVarIdx_fadeincounter:I = 0x17

.field private static final mExportVarIdx_fadeoutcounter:I = 0x16

.field private static final mExportVarIdx_gAngle:I = 0x0

.field private static final mExportVarIdx_gCubeMesh:I = 0x15

.field private static final mExportVarIdx_gIdle:I = 0x4

.field private static final mExportVarIdx_gPFBackgroundMip:I = 0x7

.field private static final mExportVarIdx_gPFBackgroundNoMip:I = 0x8

.field private static final mExportVarIdx_gPFSBackground:I = 0x11

.field private static final mExportVarIdx_gPR:I = 0x9

.field private static final mExportVarIdx_gPVBackground:I = 0x6

.field private static final mExportVarIdx_gPeak:I = 0x1

.field private static final mExportVarIdx_gPointBuffer:I = 0x13

.field private static final mExportVarIdx_gPoints:I = 0x12

.field private static final mExportVarIdx_gRotate:I = 0x2

.field private static final mExportVarIdx_gTilt:I = 0x3

.field private static final mExportVarIdx_gTlinetexture:I = 0x14

.field private static final mExportVarIdx_gTvumeter_album:I = 0x10

.field private static final mExportVarIdx_gTvumeter_background:I = 0xa

.field private static final mExportVarIdx_gTvumeter_black:I = 0xe

.field private static final mExportVarIdx_gTvumeter_frame:I = 0xf

.field private static final mExportVarIdx_gTvumeter_needle:I = 0xd

.field private static final mExportVarIdx_gTvumeter_peak_off:I = 0xc

.field private static final mExportVarIdx_gTvumeter_peak_on:I = 0xb

.field private static final mExportVarIdx_gWaveCounter:I = 0x5

.field private static final mExportVarIdx_idle:I = 0x20

.field private static final mExportVarIdx_lastuptime:I = 0x22

.field private static final mExportVarIdx_wave1amp:I = 0x19

.field private static final mExportVarIdx_wave1pos:I = 0x18

.field private static final mExportVarIdx_wave2amp:I = 0x1b

.field private static final mExportVarIdx_wave2pos:I = 0x1a

.field private static final mExportVarIdx_wave3amp:I = 0x1d

.field private static final mExportVarIdx_wave3pos:I = 0x1c

.field private static final mExportVarIdx_wave4amp:I = 0x1f

.field private static final mExportVarIdx_wave4pos:I = 0x1e

.field private static final mExportVarIdx_waveCounter:I = 0x21


# instance fields
.field private __ALLOCATION:Landroid/renderscript/Element;

.field private __F32:Landroid/renderscript/Element;

.field private __I32:Landroid/renderscript/Element;

.field private __MESH:Landroid/renderscript/Element;

.field private __PROGRAM_FRAGMENT:Landroid/renderscript/Element;

.field private __PROGRAM_RASTER:Landroid/renderscript/Element;

.field private __PROGRAM_STORE:Landroid/renderscript/Element;

.field private __PROGRAM_VERTEX:Landroid/renderscript/Element;

.field private mExportVar_autorotation:F

.field private mExportVar_fadeincounter:I

.field private mExportVar_fadeoutcounter:I

.field private mExportVar_gAngle:F

.field private mExportVar_gCubeMesh:Landroid/renderscript/Mesh;

.field private mExportVar_gIdle:I

.field private mExportVar_gPFBackgroundMip:Landroid/renderscript/ProgramFragment;

.field private mExportVar_gPFBackgroundNoMip:Landroid/renderscript/ProgramFragment;

.field private mExportVar_gPFSBackground:Landroid/renderscript/ProgramStore;

.field private mExportVar_gPR:Landroid/renderscript/ProgramRaster;

.field private mExportVar_gPVBackground:Landroid/renderscript/ProgramVertex;

.field private mExportVar_gPeak:I

.field private mExportVar_gPointBuffer:Landroid/renderscript/Allocation;

.field private mExportVar_gPoints:Lcom/android/musicvis/vis5/ScriptField_Vertex;

.field private mExportVar_gRotate:F

.field private mExportVar_gTilt:F

.field private mExportVar_gTlinetexture:Landroid/renderscript/Allocation;

.field private mExportVar_gTvumeter_album:Landroid/renderscript/Allocation;

.field private mExportVar_gTvumeter_background:Landroid/renderscript/Allocation;

.field private mExportVar_gTvumeter_black:Landroid/renderscript/Allocation;

.field private mExportVar_gTvumeter_frame:Landroid/renderscript/Allocation;

.field private mExportVar_gTvumeter_needle:Landroid/renderscript/Allocation;

.field private mExportVar_gTvumeter_peak_off:Landroid/renderscript/Allocation;

.field private mExportVar_gTvumeter_peak_on:Landroid/renderscript/Allocation;

.field private mExportVar_gWaveCounter:I

.field private mExportVar_idle:[F

.field private mExportVar_lastuptime:I

.field private mExportVar_wave1amp:I

.field private mExportVar_wave1pos:I

.field private mExportVar_wave2amp:I

.field private mExportVar_wave2pos:I

.field private mExportVar_wave3amp:I

.field private mExportVar_wave3pos:I

.field private mExportVar_wave4amp:I

.field private mExportVar_wave4pos:I

.field private mExportVar_waveCounter:I


# direct methods
.method public constructor <init>(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)V
    .locals 2
    .param p1    # Landroid/renderscript/RenderScript;
    .param p2    # Landroid/content/res/Resources;
    .param p3    # I

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/renderscript/ScriptC;-><init>(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)V

    invoke-static {p1}, Landroid/renderscript/Element;->F32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->__F32:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->I32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->__I32:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->PROGRAM_VERTEX(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->__PROGRAM_VERTEX:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->PROGRAM_FRAGMENT(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->__PROGRAM_FRAGMENT:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->PROGRAM_RASTER(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->__PROGRAM_RASTER:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->ALLOCATION(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->__ALLOCATION:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->PROGRAM_STORE(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->__PROGRAM_STORE:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->MESH(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->__MESH:Landroid/renderscript/Element;

    iput v1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_fadeoutcounter:I

    iput v1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_fadeincounter:I

    iput v1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_wave1pos:I

    iput v1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_wave1amp:I

    iput v1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_wave2pos:I

    iput v1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_wave2amp:I

    iput v1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_wave3pos:I

    iput v1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_wave3amp:I

    iput v1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_wave4pos:I

    iput v1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_wave4amp:I

    iput v1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_waveCounter:I

    iput v1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_lastuptime:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_autorotation:F

    return-void
.end method


# virtual methods
.method public bind_gPoints(Lcom/android/musicvis/vis5/ScriptField_Vertex;)V
    .locals 2
    .param p1    # Lcom/android/musicvis/vis5/ScriptField_Vertex;

    const/16 v1, 0x12

    iput-object p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gPoints:Lcom/android/musicvis/vis5/ScriptField_Vertex;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/renderscript/Script;->bindAllocation(Landroid/renderscript/Allocation;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/renderscript/Script$FieldBase;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Landroid/renderscript/Script;->bindAllocation(Landroid/renderscript/Allocation;I)V

    goto :goto_0
.end method

.method public get_autorotation()F
    .locals 1

    iget v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_autorotation:F

    return v0
.end method

.method public get_fadeincounter()I
    .locals 1

    iget v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_fadeincounter:I

    return v0
.end method

.method public get_fadeoutcounter()I
    .locals 1

    iget v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_fadeoutcounter:I

    return v0
.end method

.method public get_gAngle()F
    .locals 1

    iget v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gAngle:F

    return v0
.end method

.method public get_gCubeMesh()Landroid/renderscript/Mesh;
    .locals 1

    iget-object v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gCubeMesh:Landroid/renderscript/Mesh;

    return-object v0
.end method

.method public get_gIdle()I
    .locals 1

    iget v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gIdle:I

    return v0
.end method

.method public get_gPFBackgroundMip()Landroid/renderscript/ProgramFragment;
    .locals 1

    iget-object v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gPFBackgroundMip:Landroid/renderscript/ProgramFragment;

    return-object v0
.end method

.method public get_gPFBackgroundNoMip()Landroid/renderscript/ProgramFragment;
    .locals 1

    iget-object v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gPFBackgroundNoMip:Landroid/renderscript/ProgramFragment;

    return-object v0
.end method

.method public get_gPFSBackground()Landroid/renderscript/ProgramStore;
    .locals 1

    iget-object v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gPFSBackground:Landroid/renderscript/ProgramStore;

    return-object v0
.end method

.method public get_gPR()Landroid/renderscript/ProgramRaster;
    .locals 1

    iget-object v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gPR:Landroid/renderscript/ProgramRaster;

    return-object v0
.end method

.method public get_gPVBackground()Landroid/renderscript/ProgramVertex;
    .locals 1

    iget-object v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gPVBackground:Landroid/renderscript/ProgramVertex;

    return-object v0
.end method

.method public get_gPeak()I
    .locals 1

    iget v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gPeak:I

    return v0
.end method

.method public get_gPointBuffer()Landroid/renderscript/Allocation;
    .locals 1

    iget-object v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gPointBuffer:Landroid/renderscript/Allocation;

    return-object v0
.end method

.method public get_gPoints()Lcom/android/musicvis/vis5/ScriptField_Vertex;
    .locals 1

    iget-object v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gPoints:Lcom/android/musicvis/vis5/ScriptField_Vertex;

    return-object v0
.end method

.method public get_gRotate()F
    .locals 1

    iget v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gRotate:F

    return v0
.end method

.method public get_gTilt()F
    .locals 1

    iget v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gTilt:F

    return v0
.end method

.method public get_gTlinetexture()Landroid/renderscript/Allocation;
    .locals 1

    iget-object v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gTlinetexture:Landroid/renderscript/Allocation;

    return-object v0
.end method

.method public get_gTvumeter_album()Landroid/renderscript/Allocation;
    .locals 1

    iget-object v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gTvumeter_album:Landroid/renderscript/Allocation;

    return-object v0
.end method

.method public get_gTvumeter_background()Landroid/renderscript/Allocation;
    .locals 1

    iget-object v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gTvumeter_background:Landroid/renderscript/Allocation;

    return-object v0
.end method

.method public get_gTvumeter_black()Landroid/renderscript/Allocation;
    .locals 1

    iget-object v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gTvumeter_black:Landroid/renderscript/Allocation;

    return-object v0
.end method

.method public get_gTvumeter_frame()Landroid/renderscript/Allocation;
    .locals 1

    iget-object v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gTvumeter_frame:Landroid/renderscript/Allocation;

    return-object v0
.end method

.method public get_gTvumeter_needle()Landroid/renderscript/Allocation;
    .locals 1

    iget-object v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gTvumeter_needle:Landroid/renderscript/Allocation;

    return-object v0
.end method

.method public get_gTvumeter_peak_off()Landroid/renderscript/Allocation;
    .locals 1

    iget-object v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gTvumeter_peak_off:Landroid/renderscript/Allocation;

    return-object v0
.end method

.method public get_gTvumeter_peak_on()Landroid/renderscript/Allocation;
    .locals 1

    iget-object v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gTvumeter_peak_on:Landroid/renderscript/Allocation;

    return-object v0
.end method

.method public get_gWaveCounter()I
    .locals 1

    iget v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gWaveCounter:I

    return v0
.end method

.method public get_idle()[F
    .locals 1

    iget-object v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_idle:[F

    return-object v0
.end method

.method public get_lastuptime()I
    .locals 1

    iget v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_lastuptime:I

    return v0
.end method

.method public get_wave1amp()I
    .locals 1

    iget v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_wave1amp:I

    return v0
.end method

.method public get_wave1pos()I
    .locals 1

    iget v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_wave1pos:I

    return v0
.end method

.method public get_wave2amp()I
    .locals 1

    iget v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_wave2amp:I

    return v0
.end method

.method public get_wave2pos()I
    .locals 1

    iget v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_wave2pos:I

    return v0
.end method

.method public get_wave3amp()I
    .locals 1

    iget v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_wave3amp:I

    return v0
.end method

.method public get_wave3pos()I
    .locals 1

    iget v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_wave3pos:I

    return v0
.end method

.method public get_wave4amp()I
    .locals 1

    iget v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_wave4amp:I

    return v0
.end method

.method public get_wave4pos()I
    .locals 1

    iget v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_wave4pos:I

    return v0
.end method

.method public get_waveCounter()I
    .locals 1

    iget v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_waveCounter:I

    return v0
.end method

.method public set_autorotation(F)V
    .locals 1
    .param p1    # F

    iput p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_autorotation:F

    const/16 v0, 0x23

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(IF)V

    return-void
.end method

.method public set_fadeincounter(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_fadeincounter:I

    const/16 v0, 0x17

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(II)V

    return-void
.end method

.method public set_fadeoutcounter(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_fadeoutcounter:I

    const/16 v0, 0x16

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(II)V

    return-void
.end method

.method public set_gAngle(F)V
    .locals 1
    .param p1    # F

    iput p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gAngle:F

    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(IF)V

    return-void
.end method

.method public set_gCubeMesh(Landroid/renderscript/Mesh;)V
    .locals 1
    .param p1    # Landroid/renderscript/Mesh;

    iput-object p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gCubeMesh:Landroid/renderscript/Mesh;

    const/16 v0, 0x15

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gIdle(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gIdle:I

    const/4 v0, 0x4

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(II)V

    return-void
.end method

.method public set_gPFBackgroundMip(Landroid/renderscript/ProgramFragment;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramFragment;

    iput-object p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gPFBackgroundMip:Landroid/renderscript/ProgramFragment;

    const/4 v0, 0x7

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gPFBackgroundNoMip(Landroid/renderscript/ProgramFragment;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramFragment;

    iput-object p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gPFBackgroundNoMip:Landroid/renderscript/ProgramFragment;

    const/16 v0, 0x8

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gPFSBackground(Landroid/renderscript/ProgramStore;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramStore;

    iput-object p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gPFSBackground:Landroid/renderscript/ProgramStore;

    const/16 v0, 0x11

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gPR(Landroid/renderscript/ProgramRaster;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramRaster;

    iput-object p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gPR:Landroid/renderscript/ProgramRaster;

    const/16 v0, 0x9

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gPVBackground(Landroid/renderscript/ProgramVertex;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramVertex;

    iput-object p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gPVBackground:Landroid/renderscript/ProgramVertex;

    const/4 v0, 0x6

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gPeak(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gPeak:I

    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(II)V

    return-void
.end method

.method public set_gPointBuffer(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    iput-object p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gPointBuffer:Landroid/renderscript/Allocation;

    const/16 v0, 0x13

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gRotate(F)V
    .locals 1
    .param p1    # F

    iput p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gRotate:F

    const/4 v0, 0x2

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(IF)V

    return-void
.end method

.method public set_gTilt(F)V
    .locals 1
    .param p1    # F

    iput p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gTilt:F

    const/4 v0, 0x3

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(IF)V

    return-void
.end method

.method public set_gTlinetexture(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    iput-object p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gTlinetexture:Landroid/renderscript/Allocation;

    const/16 v0, 0x14

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gTvumeter_album(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    iput-object p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gTvumeter_album:Landroid/renderscript/Allocation;

    const/16 v0, 0x10

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gTvumeter_background(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    iput-object p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gTvumeter_background:Landroid/renderscript/Allocation;

    const/16 v0, 0xa

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gTvumeter_black(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    iput-object p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gTvumeter_black:Landroid/renderscript/Allocation;

    const/16 v0, 0xe

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gTvumeter_frame(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    iput-object p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gTvumeter_frame:Landroid/renderscript/Allocation;

    const/16 v0, 0xf

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gTvumeter_needle(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    iput-object p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gTvumeter_needle:Landroid/renderscript/Allocation;

    const/16 v0, 0xd

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gTvumeter_peak_off(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    iput-object p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gTvumeter_peak_off:Landroid/renderscript/Allocation;

    const/16 v0, 0xc

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gTvumeter_peak_on(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    iput-object p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gTvumeter_peak_on:Landroid/renderscript/Allocation;

    const/16 v0, 0xb

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/BaseObj;)V

    return-void
.end method

.method public set_gWaveCounter(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gWaveCounter:I

    const/4 v0, 0x5

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(II)V

    return-void
.end method

.method public set_idle([F)V
    .locals 5
    .param p1    # [F

    const/16 v4, 0x1000

    iput-object p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_idle:[F

    new-instance v2, Landroid/renderscript/FieldPacker;

    const/16 v3, 0x4000

    invoke-direct {v2, v3}, Landroid/renderscript/FieldPacker;-><init>(I)V

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v4, :cond_0

    aget v3, p1, v1

    invoke-virtual {v2, v3}, Landroid/renderscript/FieldPacker;->addF32(F)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x1

    new-array v0, v3, [I

    const/4 v3, 0x0

    aput v4, v0, v3

    const/16 v3, 0x20

    iget-object v4, p0, Lcom/android/musicvis/vis5/ScriptC_many;->__F32:Landroid/renderscript/Element;

    invoke-virtual {p0, v3, v2, v4, v0}, Landroid/renderscript/Script;->setVar(ILandroid/renderscript/FieldPacker;Landroid/renderscript/Element;[I)V

    return-void
.end method

.method public set_lastuptime(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_lastuptime:I

    const/16 v0, 0x22

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(II)V

    return-void
.end method

.method public set_wave1amp(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_wave1amp:I

    const/16 v0, 0x19

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(II)V

    return-void
.end method

.method public set_wave1pos(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_wave1pos:I

    const/16 v0, 0x18

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(II)V

    return-void
.end method

.method public set_wave2amp(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_wave2amp:I

    const/16 v0, 0x1b

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(II)V

    return-void
.end method

.method public set_wave2pos(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_wave2pos:I

    const/16 v0, 0x1a

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(II)V

    return-void
.end method

.method public set_wave3amp(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_wave3amp:I

    const/16 v0, 0x1d

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(II)V

    return-void
.end method

.method public set_wave3pos(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_wave3pos:I

    const/16 v0, 0x1c

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(II)V

    return-void
.end method

.method public set_wave4amp(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_wave4amp:I

    const/16 v0, 0x1f

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(II)V

    return-void
.end method

.method public set_wave4pos(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_wave4pos:I

    const/16 v0, 0x1e

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(II)V

    return-void
.end method

.method public set_waveCounter(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_waveCounter:I

    const/16 v0, 0x21

    invoke-virtual {p0, v0, p1}, Landroid/renderscript/Script;->setVar(II)V

    return-void
.end method
