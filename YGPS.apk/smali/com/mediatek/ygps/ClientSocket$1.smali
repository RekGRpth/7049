.class Lcom/mediatek/ygps/ClientSocket$1;
.super Ljava/lang/Object;
.source "ClientSocket.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/ygps/ClientSocket;-><init>(Lcom/mediatek/ygps/YgpsActivity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/ygps/ClientSocket;


# direct methods
.method constructor <init>(Lcom/mediatek/ygps/ClientSocket;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/ygps/ClientSocket$1;->this$0:Lcom/mediatek/ygps/ClientSocket;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    const/16 v8, 0xa

    :goto_0
    :try_start_0
    iget-object v6, p0, Lcom/mediatek/ygps/ClientSocket$1;->this$0:Lcom/mediatek/ygps/ClientSocket;

    iget-object v5, p0, Lcom/mediatek/ygps/ClientSocket$1;->this$0:Lcom/mediatek/ygps/ClientSocket;

    invoke-static {v5}, Lcom/mediatek/ygps/ClientSocket;->access$100(Lcom/mediatek/ygps/ClientSocket;)Ljava/util/concurrent/BlockingQueue;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v6, v5}, Lcom/mediatek/ygps/ClientSocket;->access$002(Lcom/mediatek/ygps/ClientSocket;Ljava/lang/String;)Ljava/lang/String;

    const-string v5, "YGPS/ClientSocket"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Queue take command:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/mediatek/ygps/ClientSocket$1;->this$0:Lcom/mediatek/ygps/ClientSocket;

    invoke-static {v7}, Lcom/mediatek/ygps/ClientSocket;->access$000(Lcom/mediatek/ygps/ClientSocket;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v5, p0, Lcom/mediatek/ygps/ClientSocket$1;->this$0:Lcom/mediatek/ygps/ClientSocket;

    invoke-static {v5}, Lcom/mediatek/ygps/ClientSocket;->access$200(Lcom/mediatek/ygps/ClientSocket;)V

    iget-object v5, p0, Lcom/mediatek/ygps/ClientSocket$1;->this$0:Lcom/mediatek/ygps/ClientSocket;

    invoke-static {v5}, Lcom/mediatek/ygps/ClientSocket;->access$300(Lcom/mediatek/ygps/ClientSocket;)Ljava/io/DataOutputStream;

    move-result-object v5

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/mediatek/ygps/ClientSocket$1;->this$0:Lcom/mediatek/ygps/ClientSocket;

    invoke-static {v5}, Lcom/mediatek/ygps/ClientSocket;->access$400(Lcom/mediatek/ygps/ClientSocket;)Ljava/io/DataInputStream;

    move-result-object v5

    if-eqz v5, :cond_4

    :try_start_1
    iget-object v5, p0, Lcom/mediatek/ygps/ClientSocket$1;->this$0:Lcom/mediatek/ygps/ClientSocket;

    invoke-static {v5}, Lcom/mediatek/ygps/ClientSocket;->access$300(Lcom/mediatek/ygps/ClientSocket;)Ljava/io/DataOutputStream;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/ygps/ClientSocket$1;->this$0:Lcom/mediatek/ygps/ClientSocket;

    invoke-static {v6}, Lcom/mediatek/ygps/ClientSocket;->access$000(Lcom/mediatek/ygps/ClientSocket;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/mediatek/ygps/ClientSocket$1;->this$0:Lcom/mediatek/ygps/ClientSocket;

    invoke-static {v5}, Lcom/mediatek/ygps/ClientSocket;->access$300(Lcom/mediatek/ygps/ClientSocket;)Ljava/io/DataOutputStream;

    move-result-object v5

    const/16 v6, 0xd

    invoke-virtual {v5, v6}, Ljava/io/DataOutputStream;->write(I)V

    iget-object v5, p0, Lcom/mediatek/ygps/ClientSocket$1;->this$0:Lcom/mediatek/ygps/ClientSocket;

    invoke-static {v5}, Lcom/mediatek/ygps/ClientSocket;->access$300(Lcom/mediatek/ygps/ClientSocket;)Ljava/io/DataOutputStream;

    move-result-object v5

    const/16 v6, 0xa

    invoke-virtual {v5, v6}, Ljava/io/DataOutputStream;->write(I)V

    iget-object v5, p0, Lcom/mediatek/ygps/ClientSocket$1;->this$0:Lcom/mediatek/ygps/ClientSocket;

    invoke-static {v5}, Lcom/mediatek/ygps/ClientSocket;->access$300(Lcom/mediatek/ygps/ClientSocket;)Ljava/io/DataOutputStream;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/DataOutputStream;->flush()V

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v0, -0x1

    :cond_0
    iget-object v5, p0, Lcom/mediatek/ygps/ClientSocket$1;->this$0:Lcom/mediatek/ygps/ClientSocket;

    invoke-static {v5}, Lcom/mediatek/ygps/ClientSocket;->access$400(Lcom/mediatek/ygps/ClientSocket;)Ljava/io/DataInputStream;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/ygps/ClientSocket$1;->this$0:Lcom/mediatek/ygps/ClientSocket;

    invoke-static {v6}, Lcom/mediatek/ygps/ClientSocket;->access$500(Lcom/mediatek/ygps/ClientSocket;)[B

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/DataInputStream;->read([B)I

    move-result v0

    const/4 v5, -0x1

    if-eq v0, v5, :cond_1

    add-int/lit8 v3, v3, 0x1

    new-instance v4, Ljava/lang/String;

    iget-object v5, p0, Lcom/mediatek/ygps/ClientSocket$1;->this$0:Lcom/mediatek/ygps/ClientSocket;

    invoke-static {v5}, Lcom/mediatek/ygps/ClientSocket;->access$500(Lcom/mediatek/ygps/ClientSocket;)[B

    move-result-object v5

    const/4 v6, 0x0

    invoke-direct {v4, v5, v6, v0}, Ljava/lang/String;-><init>([BII)V

    const-string v5, "YGPS/ClientSocket"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "line: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " sentence: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "PMTK"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/mediatek/ygps/ClientSocket$1;->this$0:Lcom/mediatek/ygps/ClientSocket;

    invoke-static {v5, v4}, Lcom/mediatek/ygps/ClientSocket;->access$602(Lcom/mediatek/ygps/ClientSocket;Ljava/lang/String;)Ljava/lang/String;

    const-string v5, "YGPS/ClientSocket"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Get response from MNL: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_1
    iget-object v5, p0, Lcom/mediatek/ygps/ClientSocket$1;->this$0:Lcom/mediatek/ygps/ClientSocket;

    invoke-static {v5}, Lcom/mediatek/ygps/ClientSocket;->access$700(Lcom/mediatek/ygps/ClientSocket;)Lcom/mediatek/ygps/YgpsActivity;

    move-result-object v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/mediatek/ygps/ClientSocket$1;->this$0:Lcom/mediatek/ygps/ClientSocket;

    invoke-static {v5}, Lcom/mediatek/ygps/ClientSocket;->access$700(Lcom/mediatek/ygps/ClientSocket;)Lcom/mediatek/ygps/YgpsActivity;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/ygps/ClientSocket$1;->this$0:Lcom/mediatek/ygps/ClientSocket;

    invoke-static {v6}, Lcom/mediatek/ygps/ClientSocket;->access$600(Lcom/mediatek/ygps/ClientSocket;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/mediatek/ygps/YgpsActivity;->onResponse(Ljava/lang/String;)V

    :cond_2
    iget-object v5, p0, Lcom/mediatek/ygps/ClientSocket$1;->this$0:Lcom/mediatek/ygps/ClientSocket;

    const/4 v6, 0x0

    invoke-static {v5, v6}, Lcom/mediatek/ygps/ClientSocket;->access$002(Lcom/mediatek/ygps/ClientSocket;Ljava/lang/String;)Ljava/lang/String;

    iget-object v5, p0, Lcom/mediatek/ygps/ClientSocket$1;->this$0:Lcom/mediatek/ygps/ClientSocket;

    invoke-static {v5}, Lcom/mediatek/ygps/ClientSocket;->access$800(Lcom/mediatek/ygps/ClientSocket;)V

    goto/16 :goto_0

    :catch_0
    move-exception v2

    const-string v5, "YGPS/ClientSocket"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Take command interrupted:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_3
    if-le v3, v8, :cond_0

    :try_start_2
    iget-object v5, p0, Lcom/mediatek/ygps/ClientSocket$1;->this$0:Lcom/mediatek/ygps/ClientSocket;

    const-string v6, "TIMEOUT"

    invoke-static {v5, v6}, Lcom/mediatek/ygps/ClientSocket;->access$602(Lcom/mediatek/ygps/ClientSocket;Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    :catch_1
    move-exception v1

    const-string v5, "YGPS/ClientSocket"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "sendCommand IOException: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/mediatek/ygps/ClientSocket$1;->this$0:Lcom/mediatek/ygps/ClientSocket;

    const-string v6, "ERROR"

    invoke-static {v5, v6}, Lcom/mediatek/ygps/ClientSocket;->access$602(Lcom/mediatek/ygps/ClientSocket;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_1

    :cond_4
    const-string v5, "YGPS/ClientSocket"

    const-string v6, "out is null"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/mediatek/ygps/ClientSocket$1;->this$0:Lcom/mediatek/ygps/ClientSocket;

    const-string v6, "ERROR"

    invoke-static {v5, v6}, Lcom/mediatek/ygps/ClientSocket;->access$602(Lcom/mediatek/ygps/ClientSocket;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_1
.end method
