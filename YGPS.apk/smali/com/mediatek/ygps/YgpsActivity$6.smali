.class Lcom/mediatek/ygps/YgpsActivity$6;
.super Ljava/lang/Object;
.source "YgpsActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/ygps/YgpsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/ygps/YgpsActivity;


# direct methods
.method constructor <init>(Lcom/mediatek/ygps/YgpsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1    # Landroid/view/View;

    const-wide/16 v7, 0xc8

    const/16 v6, 0x3ea

    const/4 v5, 0x0

    const/4 v4, 0x1

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$2900(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Toast;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$2900(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->cancel()V

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$100(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;

    move-result-object v2

    if-ne p1, v2, :cond_2

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$100(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->refreshDrawableState()V

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$100(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    const-string v2, "YGPS/Activity"

    const-string v3, "GPSTest Start button is pressed"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$2300(Lcom/mediatek/ygps/YgpsActivity;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v6, v7, v8}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$3000(Lcom/mediatek/ygps/YgpsActivity;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$200(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;

    move-result-object v2

    if-ne p1, v2, :cond_4

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$200(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$200(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->refreshDrawableState()V

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$3100(Lcom/mediatek/ygps/YgpsActivity;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2, v4}, Lcom/mediatek/ygps/YgpsActivity;->access$3102(Lcom/mediatek/ygps/YgpsActivity;Z)Z

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-virtual {v2, v5}, Landroid/app/Activity;->showDialog(I)V

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2, v5}, Lcom/mediatek/ygps/YgpsActivity;->access$2402(Lcom/mediatek/ygps/YgpsActivity;Z)Z

    goto :goto_0

    :cond_3
    const-string v2, "YGPS/Activity"

    const-string v3, "stop has been clicked."

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$3200(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;

    move-result-object v2

    if-ne p1, v2, :cond_5

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$3300(Lcom/mediatek/ygps/YgpsActivity;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2, v4}, Lcom/mediatek/ygps/YgpsActivity;->access$1702(Lcom/mediatek/ygps/YgpsActivity;Z)Z

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2, v5}, Lcom/mediatek/ygps/YgpsActivity;->access$3400(Lcom/mediatek/ygps/YgpsActivity;Z)V

    const-string v2, "YGPS/Activity"

    const-string v3, "Hot Start button is pressed"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "rti"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2, v0, v5}, Lcom/mediatek/ygps/YgpsActivity;->access$500(Lcom/mediatek/ygps/YgpsActivity;Landroid/os/Bundle;Z)V

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2, v4}, Lcom/mediatek/ygps/YgpsActivity;->access$3400(Lcom/mediatek/ygps/YgpsActivity;Z)V

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$2300(Lcom/mediatek/ygps/YgpsActivity;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v6, v7, v8}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$3500(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;

    move-result-object v2

    if-ne p1, v2, :cond_6

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$3300(Lcom/mediatek/ygps/YgpsActivity;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2, v4}, Lcom/mediatek/ygps/YgpsActivity;->access$1702(Lcom/mediatek/ygps/YgpsActivity;Z)Z

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2, v5}, Lcom/mediatek/ygps/YgpsActivity;->access$3400(Lcom/mediatek/ygps/YgpsActivity;Z)V

    const-string v2, "YGPS/Activity"

    const-string v3, "Warm Start button is pressed"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "ephemeris"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2, v0, v5}, Lcom/mediatek/ygps/YgpsActivity;->access$500(Lcom/mediatek/ygps/YgpsActivity;Landroid/os/Bundle;Z)V

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2, v4}, Lcom/mediatek/ygps/YgpsActivity;->access$3400(Lcom/mediatek/ygps/YgpsActivity;Z)V

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$2300(Lcom/mediatek/ygps/YgpsActivity;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v6, v7, v8}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    :cond_6
    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$3600(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;

    move-result-object v2

    if-ne p1, v2, :cond_7

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$3300(Lcom/mediatek/ygps/YgpsActivity;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2, v4}, Lcom/mediatek/ygps/YgpsActivity;->access$1702(Lcom/mediatek/ygps/YgpsActivity;Z)Z

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2, v5}, Lcom/mediatek/ygps/YgpsActivity;->access$3400(Lcom/mediatek/ygps/YgpsActivity;Z)V

    const-string v2, "YGPS/Activity"

    const-string v3, "Cold Start button is pressed"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "ephemeris"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "position"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "time"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "iono"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "utc"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "health"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2, v0, v5}, Lcom/mediatek/ygps/YgpsActivity;->access$500(Lcom/mediatek/ygps/YgpsActivity;Landroid/os/Bundle;Z)V

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2, v4}, Lcom/mediatek/ygps/YgpsActivity;->access$3400(Lcom/mediatek/ygps/YgpsActivity;Z)V

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$2300(Lcom/mediatek/ygps/YgpsActivity;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v6, v7, v8}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    :cond_7
    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$3700(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;

    move-result-object v2

    if-ne p1, v2, :cond_8

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$3300(Lcom/mediatek/ygps/YgpsActivity;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2, v4}, Lcom/mediatek/ygps/YgpsActivity;->access$1702(Lcom/mediatek/ygps/YgpsActivity;Z)Z

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2, v5}, Lcom/mediatek/ygps/YgpsActivity;->access$3400(Lcom/mediatek/ygps/YgpsActivity;Z)V

    const-string v2, "YGPS/Activity"

    const-string v3, "Full Start button is pressed"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "all"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2, v0, v5}, Lcom/mediatek/ygps/YgpsActivity;->access$500(Lcom/mediatek/ygps/YgpsActivity;Landroid/os/Bundle;Z)V

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2, v4}, Lcom/mediatek/ygps/YgpsActivity;->access$3400(Lcom/mediatek/ygps/YgpsActivity;Z)V

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$2300(Lcom/mediatek/ygps/YgpsActivity;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v6, v7, v8}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    :cond_8
    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$3800(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;

    move-result-object v2

    if-ne p1, v2, :cond_9

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$3300(Lcom/mediatek/ygps/YgpsActivity;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2, v5}, Lcom/mediatek/ygps/YgpsActivity;->access$3400(Lcom/mediatek/ygps/YgpsActivity;Z)V

    const-string v2, "YGPS/Activity"

    const-string v3, "Restart button is pressed"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "ephemeris"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "almanac"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "position"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "time"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "iono"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "utc"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2, v0, v5}, Lcom/mediatek/ygps/YgpsActivity;->access$500(Lcom/mediatek/ygps/YgpsActivity;Landroid/os/Bundle;Z)V

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2, v4}, Lcom/mediatek/ygps/YgpsActivity;->access$3400(Lcom/mediatek/ygps/YgpsActivity;Z)V

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$2300(Lcom/mediatek/ygps/YgpsActivity;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v6, v7, v8}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    :cond_9
    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$3900(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;

    move-result-object v2

    if-ne p1, v2, :cond_b

    const-string v2, "YGPS/Activity"

    const-string v3, "NMEA Start button is pressed"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$4000(Lcom/mediatek/ygps/YgpsActivity;)Z

    move-result v2

    if-nez v2, :cond_a

    const-string v2, "YGPS/Activity"

    const-string v3, "createFileForSavingNMEALog return false"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_a
    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2, v4}, Lcom/mediatek/ygps/YgpsActivity;->access$1102(Lcom/mediatek/ygps/YgpsActivity;Z)Z

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$3900(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$4100(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    goto/16 :goto_0

    :cond_b
    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$4100(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;

    move-result-object v2

    if-ne p1, v2, :cond_c

    const-string v2, "YGPS/Activity"

    const-string v3, "NMEA Stop button is pressed"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2, v5}, Lcom/mediatek/ygps/YgpsActivity;->access$1102(Lcom/mediatek/ygps/YgpsActivity;Z)Z

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$4200(Lcom/mediatek/ygps/YgpsActivity;)V

    goto/16 :goto_0

    :cond_c
    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$4300(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;

    move-result-object v2

    if-ne p1, v2, :cond_e

    const-string v2, "YGPS/Activity"

    const-string v3, "NMEA DbgDbg is pressed"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "debug.dbg2socket"

    const-string v3, "0"

    invoke-static {v2, v3}, Lcom/mediatek/ygps/GpsMnlSetting;->getMnlProp(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$4300(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;

    move-result-object v2

    const v3, 0x7f050051

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    const-string v2, "debug.dbg2socket"

    const-string v3, "1"

    invoke-static {v2, v3}, Lcom/mediatek/ygps/GpsMnlSetting;->setMnlProp(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_d
    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$4300(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;

    move-result-object v2

    const v3, 0x7f050050

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    const-string v2, "debug.dbg2socket"

    const-string v3, "0"

    invoke-static {v2, v3}, Lcom/mediatek/ygps/GpsMnlSetting;->setMnlProp(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_e
    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$4400(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;

    move-result-object v2

    if-ne p1, v2, :cond_10

    const-string v2, "YGPS/Activity"

    const-string v3, "NMEA DbgNmea button is pressed"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "debug.nmea2socket"

    const-string v3, "0"

    invoke-static {v2, v3}, Lcom/mediatek/ygps/GpsMnlSetting;->getMnlProp(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$4400(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;

    move-result-object v2

    const v3, 0x7f050053

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    const-string v2, "debug.nmea2socket"

    const-string v3, "1"

    invoke-static {v2, v3}, Lcom/mediatek/ygps/GpsMnlSetting;->setMnlProp(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_f
    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$4400(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;

    move-result-object v2

    const v3, 0x7f050052

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    const-string v2, "debug.nmea2socket"

    const-string v3, "0"

    invoke-static {v2, v3}, Lcom/mediatek/ygps/GpsMnlSetting;->setMnlProp(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_10
    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$4500(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;

    move-result-object v2

    if-ne p1, v2, :cond_12

    const-string v2, "YGPS/Activity"

    const-string v3, "NMEA DbgDbgFile is pressed"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "debug.dbg2file"

    const-string v3, "0"

    invoke-static {v2, v3}, Lcom/mediatek/ygps/GpsMnlSetting;->getMnlProp(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$4500(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;

    move-result-object v2

    const v3, 0x7f050055

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    const-string v2, "debug.dbg2file"

    const-string v3, "1"

    invoke-static {v2, v3}, Lcom/mediatek/ygps/GpsMnlSetting;->setMnlProp(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_11
    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$4500(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;

    move-result-object v2

    const v3, 0x7f050054

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    const-string v2, "debug.dbg2file"

    const-string v3, "0"

    invoke-static {v2, v3}, Lcom/mediatek/ygps/GpsMnlSetting;->setMnlProp(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_12
    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$4600(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;

    move-result-object v2

    if-ne p1, v2, :cond_14

    const-string v2, "YGPS/Activity"

    const-string v3, "NMEA debug2ddms button is pressed"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "debug.debug_nmea"

    const-string v3, "1"

    invoke-static {v2, v3}, Lcom/mediatek/ygps/GpsMnlSetting;->getMnlProp(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_13

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$4600(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;

    move-result-object v2

    const v3, 0x7f050057

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    const-string v2, "debug.debug_nmea"

    const-string v3, "0"

    invoke-static {v2, v3}, Lcom/mediatek/ygps/GpsMnlSetting;->setMnlProp(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_13
    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$4600(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;

    move-result-object v2

    const v3, 0x7f050056

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    const-string v2, "debug.debug_nmea"

    const-string v3, "1"

    invoke-static {v2, v3}, Lcom/mediatek/ygps/GpsMnlSetting;->setMnlProp(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_14
    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$4700(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;

    move-result-object v2

    if-ne p1, v2, :cond_16

    const-string v2, "YGPS/Activity"

    const-string v3, "Hot still button is pressed"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "BEE_enabled"

    const-string v3, "1"

    invoke-static {v2, v3}, Lcom/mediatek/ygps/GpsMnlSetting;->getMnlProp(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_15

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$4700(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;

    move-result-object v2

    const v3, 0x7f050059

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    const-string v2, "BEE_enabled"

    const-string v3, "0"

    invoke-static {v2, v3}, Lcom/mediatek/ygps/GpsMnlSetting;->setMnlProp(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_15
    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$4700(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;

    move-result-object v2

    const v3, 0x7f050058

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    const-string v2, "BEE_enabled"

    const-string v3, "1"

    invoke-static {v2, v3}, Lcom/mediatek/ygps/GpsMnlSetting;->setMnlProp(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_16
    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$4800(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;

    move-result-object v2

    if-ne p1, v2, :cond_17

    const-string v2, "YGPS/Activity"

    const-string v3, "NMEA Clear button is pressed"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$1300(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/TextView;

    move-result-object v2

    const v3, 0x7f050002

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0

    :cond_17
    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$4900(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;

    move-result-object v2

    if-ne p1, v2, :cond_18

    const-string v2, "YGPS/Activity"

    const-string v3, "NMEA Save button is pressed"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$5000(Lcom/mediatek/ygps/YgpsActivity;)V

    goto/16 :goto_0

    :cond_18
    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$5100(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;

    move-result-object v2

    if-ne p1, v2, :cond_19

    const-string v2, "YGPS/Activity"

    const-string v3, "mBtnGPSHwTest Button is pressed"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$5200(Lcom/mediatek/ygps/YgpsActivity;)V

    goto/16 :goto_0

    :cond_19
    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$5300(Lcom/mediatek/ygps/YgpsActivity;)Landroid/widget/Button;

    move-result-object v2

    if-ne p1, v2, :cond_1

    const-string v2, "YGPS/Activity"

    const-string v3, "mBtnGPSJamming Button is pressed"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/ygps/YgpsActivity$6;->this$0:Lcom/mediatek/ygps/YgpsActivity;

    invoke-static {v2}, Lcom/mediatek/ygps/YgpsActivity;->access$5400(Lcom/mediatek/ygps/YgpsActivity;)V

    goto/16 :goto_0
.end method
