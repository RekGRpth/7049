.class public abstract Landroid/support/place/api/mesh/MeshService$EndpointBase;
.super Landroid/support/place/connector/Connector;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/place/connector/Broker;Landroid/support/place/connector/PlaceInfo;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/support/place/connector/Connector;-><init>(Landroid/content/Context;Landroid/support/place/connector/Broker;Landroid/support/place/connector/PlaceInfo;)V

    return-void
.end method


# virtual methods
.method public abstract listNodes(Landroid/support/place/rpc/RpcContext;)Ljava/util/List;
.end method

.method public process(Ljava/lang/String;[BLandroid/support/place/rpc/RpcContext;Landroid/support/place/rpc/RpcError;)[B
    .locals 10

    const/4 v9, 0x0

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p2}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v1, "snmpSet"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "address"

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "group"

    invoke-virtual {v0, v2}, Landroid/support/place/rpc/RpcData;->getInteger(Ljava/lang/String;)I

    move-result v2

    const-string v3, "id"

    invoke-virtual {v0, v3}, Landroid/support/place/rpc/RpcData;->getInteger(Ljava/lang/String;)I

    move-result v3

    const-string v4, "size"

    invoke-virtual {v0, v4}, Landroid/support/place/rpc/RpcData;->getInteger(Ljava/lang/String;)I

    move-result v4

    const-string v5, "value"

    invoke-virtual {v0, v5}, Landroid/support/place/rpc/RpcData;->getLong(Ljava/lang/String;)J

    move-result-wide v5

    const-string v7, "timeout"

    invoke-virtual {v0, v7}, Landroid/support/place/rpc/RpcData;->getInteger(Ljava/lang/String;)I

    move-result v7

    move-object v0, p0

    move-object v8, p3

    invoke-virtual/range {v0 .. v8}, Landroid/support/place/api/mesh/MeshService$EndpointBase;->snmpSet(Ljava/lang/String;IIIJILandroid/support/place/rpc/RpcContext;)Z

    move-result v1

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v2, "_result"

    invoke-virtual {v0, v2, v1}, Landroid/support/place/rpc/RpcData;->putBoolean(Ljava/lang/String;Z)V

    :goto_0
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    const-string v1, "snmpSetSilent"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "address"

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "group"

    invoke-virtual {v0, v2}, Landroid/support/place/rpc/RpcData;->getInteger(Ljava/lang/String;)I

    move-result v2

    const-string v3, "id"

    invoke-virtual {v0, v3}, Landroid/support/place/rpc/RpcData;->getInteger(Ljava/lang/String;)I

    move-result v3

    const-string v4, "size"

    invoke-virtual {v0, v4}, Landroid/support/place/rpc/RpcData;->getInteger(Ljava/lang/String;)I

    move-result v4

    const-string v5, "value"

    invoke-virtual {v0, v5}, Landroid/support/place/rpc/RpcData;->getLong(Ljava/lang/String;)J

    move-result-wide v5

    move-object v0, p0

    move-object v7, p3

    invoke-virtual/range {v0 .. v7}, Landroid/support/place/api/mesh/MeshService$EndpointBase;->snmpSetSilent(Ljava/lang/String;IIIJLandroid/support/place/rpc/RpcContext;)V

    move-object v0, v9

    goto :goto_0

    :cond_1
    const-string v1, "snmpGet"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "address"

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "group"

    invoke-virtual {v0, v2}, Landroid/support/place/rpc/RpcData;->getInteger(Ljava/lang/String;)I

    move-result v2

    const-string v3, "id"

    invoke-virtual {v0, v3}, Landroid/support/place/rpc/RpcData;->getInteger(Ljava/lang/String;)I

    move-result v3

    const-string v4, "size"

    invoke-virtual {v0, v4}, Landroid/support/place/rpc/RpcData;->getInteger(Ljava/lang/String;)I

    move-result v4

    const-string v5, "timeout"

    invoke-virtual {v0, v5}, Landroid/support/place/rpc/RpcData;->getInteger(Ljava/lang/String;)I

    move-result v5

    move-object v0, p0

    move-object v6, p3

    invoke-virtual/range {v0 .. v6}, Landroid/support/place/api/mesh/MeshService$EndpointBase;->snmpGet(Ljava/lang/String;IIIILandroid/support/place/rpc/RpcContext;)J

    move-result-wide v1

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v3, "_result"

    invoke-virtual {v0, v3, v1, v2}, Landroid/support/place/rpc/RpcData;->putLong(Ljava/lang/String;J)V

    goto :goto_0

    :cond_2
    const-string v0, "listNodes"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0, p3}, Landroid/support/place/api/mesh/MeshService$EndpointBase;->listNodes(Landroid/support/place/rpc/RpcContext;)Ljava/util/List;

    move-result-object v1

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v2, "_result"

    invoke-virtual {v0, v2, v1}, Landroid/support/place/rpc/RpcData;->putList(Ljava/lang/String;Ljava/util/List;)V

    goto/16 :goto_0

    :cond_3
    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/place/connector/Connector;->process(Ljava/lang/String;[BLandroid/support/place/rpc/RpcContext;Landroid/support/place/rpc/RpcError;)[B

    move-result-object v0

    goto/16 :goto_1

    :cond_4
    move-object v0, v9

    goto/16 :goto_1
.end method

.method public pushOnNodeSeen(Landroid/support/place/rpc/RpcData;)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v1, "data"

    invoke-virtual {v0, v1, p1}, Landroid/support/place/rpc/RpcData;->putRpcData(Ljava/lang/String;Landroid/support/place/rpc/RpcData;)V

    const-string v1, "onNodeSeen"

    invoke-virtual {v0}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Landroid/support/place/api/mesh/MeshService$EndpointBase;->pushEvent(Ljava/lang/String;[B)V

    return-void
.end method

.method public pushOnSnmpSet(Landroid/support/place/rpc/RpcData;)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v1, "data"

    invoke-virtual {v0, v1, p1}, Landroid/support/place/rpc/RpcData;->putRpcData(Ljava/lang/String;Landroid/support/place/rpc/RpcData;)V

    const-string v1, "onSnmpSet"

    invoke-virtual {v0}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Landroid/support/place/api/mesh/MeshService$EndpointBase;->pushEvent(Ljava/lang/String;[B)V

    return-void
.end method

.method public abstract snmpGet(Ljava/lang/String;IIIILandroid/support/place/rpc/RpcContext;)J
.end method

.method public abstract snmpSet(Ljava/lang/String;IIIJILandroid/support/place/rpc/RpcContext;)Z
.end method

.method public abstract snmpSetSilent(Ljava/lang/String;IIIJLandroid/support/place/rpc/RpcContext;)V
.end method
