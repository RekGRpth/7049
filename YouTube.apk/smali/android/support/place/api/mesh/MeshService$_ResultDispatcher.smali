.class final Landroid/support/place/api/mesh/MeshService$_ResultDispatcher;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/place/rpc/RpcResultHandler;


# instance fields
.field private callback:Ljava/lang/Object;

.field private methodId:I

.field final synthetic this$0:Landroid/support/place/api/mesh/MeshService;


# direct methods
.method public constructor <init>(Landroid/support/place/api/mesh/MeshService;ILjava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Landroid/support/place/api/mesh/MeshService$_ResultDispatcher;->this$0:Landroid/support/place/api/mesh/MeshService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Landroid/support/place/api/mesh/MeshService$_ResultDispatcher;->methodId:I

    iput-object p3, p0, Landroid/support/place/api/mesh/MeshService$_ResultDispatcher;->callback:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final listNodes([B)V
    .locals 3

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v1, "_result"

    const-class v2, Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/support/place/rpc/RpcData;->getList(Ljava/lang/String;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v1

    iget-object v0, p0, Landroid/support/place/api/mesh/MeshService$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/api/mesh/MeshService$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v0, Landroid/support/place/api/mesh/MeshService$OnListNodes;

    invoke-interface {v0, v1}, Landroid/support/place/api/mesh/MeshService$OnListNodes;->onListNodes(Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method public final onResult([B)V
    .locals 1

    iget v0, p0, Landroid/support/place/api/mesh/MeshService$_ResultDispatcher;->methodId:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-virtual {p0, p1}, Landroid/support/place/api/mesh/MeshService$_ResultDispatcher;->snmpSet([B)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0, p1}, Landroid/support/place/api/mesh/MeshService$_ResultDispatcher;->snmpGet([B)V

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0, p1}, Landroid/support/place/api/mesh/MeshService$_ResultDispatcher;->listNodes([B)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final snmpGet([B)V
    .locals 3

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v1, "_result"

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcData;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    iget-object v0, p0, Landroid/support/place/api/mesh/MeshService$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/api/mesh/MeshService$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v0, Landroid/support/place/api/mesh/MeshService$OnSnmpGet;

    invoke-interface {v0, v1, v2}, Landroid/support/place/api/mesh/MeshService$OnSnmpGet;->onSnmpGet(J)V

    :cond_0
    return-void
.end method

.method public final snmpSet([B)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v1, "_result"

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcData;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iget-object v0, p0, Landroid/support/place/api/mesh/MeshService$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/api/mesh/MeshService$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v0, Landroid/support/place/api/mesh/MeshService$OnSnmpSet;

    invoke-interface {v0, v1}, Landroid/support/place/api/mesh/MeshService$OnSnmpSet;->onSnmpSet(Z)V

    :cond_0
    return-void
.end method
