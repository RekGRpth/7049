.class Landroid/support/place/beacon/BeaconScanner$2;
.super Landroid/support/place/beacon/SafeServiceConnection;
.source "SourceFile"


# instance fields
.field final synthetic this$0:Landroid/support/place/beacon/BeaconScanner;


# direct methods
.method constructor <init>(Landroid/support/place/beacon/BeaconScanner;)V
    .locals 0

    iput-object p1, p0, Landroid/support/place/beacon/BeaconScanner$2;->this$0:Landroid/support/place/beacon/BeaconScanner;

    invoke-direct {p0}, Landroid/support/place/beacon/SafeServiceConnection;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3

    iget-object v0, p0, Landroid/support/place/beacon/BeaconScanner$2;->this$0:Landroid/support/place/beacon/BeaconScanner;

    invoke-static {p2}, Landroid/support/place/beacon/IBeaconScanner$Stub;->asInterface(Landroid/os/IBinder;)Landroid/support/place/beacon/IBeaconScanner;

    move-result-object v1

    # setter for: Landroid/support/place/beacon/BeaconScanner;->mBeaconScanner:Landroid/support/place/beacon/IBeaconScanner;
    invoke-static {v0, v1}, Landroid/support/place/beacon/BeaconScanner;->access$102(Landroid/support/place/beacon/BeaconScanner;Landroid/support/place/beacon/IBeaconScanner;)Landroid/support/place/beacon/IBeaconScanner;

    :try_start_0
    iget-object v0, p0, Landroid/support/place/beacon/BeaconScanner$2;->this$0:Landroid/support/place/beacon/BeaconScanner;

    # getter for: Landroid/support/place/beacon/BeaconScanner;->mBeaconScanner:Landroid/support/place/beacon/IBeaconScanner;
    invoke-static {v0}, Landroid/support/place/beacon/BeaconScanner;->access$100(Landroid/support/place/beacon/BeaconScanner;)Landroid/support/place/beacon/IBeaconScanner;

    move-result-object v0

    iget-object v1, p0, Landroid/support/place/beacon/BeaconScanner$2;->this$0:Landroid/support/place/beacon/BeaconScanner;

    # getter for: Landroid/support/place/beacon/BeaconScanner;->mBeaconScannerListener:Landroid/support/place/beacon/IBeaconScannerListener;
    invoke-static {v1}, Landroid/support/place/beacon/BeaconScanner;->access$200(Landroid/support/place/beacon/BeaconScanner;)Landroid/support/place/beacon/IBeaconScannerListener;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/support/place/beacon/IBeaconScanner;->addListener(Landroid/support/place/beacon/IBeaconScannerListener;)V

    iget-object v0, p0, Landroid/support/place/beacon/BeaconScanner$2;->this$0:Landroid/support/place/beacon/BeaconScanner;

    # invokes: Landroid/support/place/beacon/BeaconScanner;->postBeaconScannerChange()V
    invoke-static {v0}, Landroid/support/place/beacon/BeaconScanner;->access$300(Landroid/support/place/beacon/BeaconScanner;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "aah.BeaconScanner"

    const-string v2, "BeaconScanner service failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    iget-object v0, p0, Landroid/support/place/beacon/BeaconScanner$2;->this$0:Landroid/support/place/beacon/BeaconScanner;

    const/4 v1, 0x0

    # setter for: Landroid/support/place/beacon/BeaconScanner;->mBeaconScanner:Landroid/support/place/beacon/IBeaconScanner;
    invoke-static {v0, v1}, Landroid/support/place/beacon/BeaconScanner;->access$102(Landroid/support/place/beacon/BeaconScanner;Landroid/support/place/beacon/IBeaconScanner;)Landroid/support/place/beacon/IBeaconScanner;

    return-void
.end method
