.class public Landroid/support/place/beacon/BeaconScanner;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final TAG:Ljava/lang/String; = "aah.BeaconScanner"


# instance fields
.field private mBeaconScanner:Landroid/support/place/beacon/IBeaconScanner;

.field private final mBeaconScannerConnection:Landroid/support/place/beacon/SafeServiceConnection;

.field private final mBeaconScannerListener:Landroid/support/place/beacon/IBeaconScannerListener;

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mListener:Landroid/support/place/beacon/BeaconScanner$Listener;

.field private mLock:Ljava/lang/Object;

.field private mSendCallbacks:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/place/beacon/BeaconScanner$Listener;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Landroid/support/place/beacon/BeaconScanner;->mHandler:Landroid/os/Handler;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Landroid/support/place/beacon/BeaconScanner;->mLock:Ljava/lang/Object;

    new-instance v0, Landroid/support/place/beacon/BeaconScanner$2;

    invoke-direct {v0, p0}, Landroid/support/place/beacon/BeaconScanner$2;-><init>(Landroid/support/place/beacon/BeaconScanner;)V

    iput-object v0, p0, Landroid/support/place/beacon/BeaconScanner;->mBeaconScannerConnection:Landroid/support/place/beacon/SafeServiceConnection;

    new-instance v0, Landroid/support/place/beacon/BeaconScanner$3;

    invoke-direct {v0, p0}, Landroid/support/place/beacon/BeaconScanner$3;-><init>(Landroid/support/place/beacon/BeaconScanner;)V

    iput-object v0, p0, Landroid/support/place/beacon/BeaconScanner;->mBeaconScannerListener:Landroid/support/place/beacon/IBeaconScannerListener;

    iput-object p1, p0, Landroid/support/place/beacon/BeaconScanner;->mContext:Landroid/content/Context;

    iput-object p2, p0, Landroid/support/place/beacon/BeaconScanner;->mListener:Landroid/support/place/beacon/BeaconScanner$Listener;

    invoke-direct {p0}, Landroid/support/place/beacon/BeaconScanner;->startBeaconScanner()Z

    return-void
.end method

.method static synthetic access$000(Landroid/support/place/beacon/BeaconScanner;)Landroid/support/place/beacon/BeaconScanner$Listener;
    .locals 1

    iget-object v0, p0, Landroid/support/place/beacon/BeaconScanner;->mListener:Landroid/support/place/beacon/BeaconScanner$Listener;

    return-object v0
.end method

.method static synthetic access$100(Landroid/support/place/beacon/BeaconScanner;)Landroid/support/place/beacon/IBeaconScanner;
    .locals 1

    iget-object v0, p0, Landroid/support/place/beacon/BeaconScanner;->mBeaconScanner:Landroid/support/place/beacon/IBeaconScanner;

    return-object v0
.end method

.method static synthetic access$102(Landroid/support/place/beacon/BeaconScanner;Landroid/support/place/beacon/IBeaconScanner;)Landroid/support/place/beacon/IBeaconScanner;
    .locals 0

    iput-object p1, p0, Landroid/support/place/beacon/BeaconScanner;->mBeaconScanner:Landroid/support/place/beacon/IBeaconScanner;

    return-object p1
.end method

.method static synthetic access$200(Landroid/support/place/beacon/BeaconScanner;)Landroid/support/place/beacon/IBeaconScannerListener;
    .locals 1

    iget-object v0, p0, Landroid/support/place/beacon/BeaconScanner;->mBeaconScannerListener:Landroid/support/place/beacon/IBeaconScannerListener;

    return-object v0
.end method

.method static synthetic access$300(Landroid/support/place/beacon/BeaconScanner;)V
    .locals 0

    invoke-direct {p0}, Landroid/support/place/beacon/BeaconScanner;->postBeaconScannerChange()V

    return-void
.end method

.method private postBeaconScannerChange()V
    .locals 4

    iget-object v1, p0, Landroid/support/place/beacon/BeaconScanner;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Landroid/support/place/beacon/BeaconScanner;->mBeaconScanner:Landroid/support/place/beacon/IBeaconScanner;

    invoke-interface {v0}, Landroid/support/place/beacon/IBeaconScanner;->getBeacons()Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :try_start_1
    iget-boolean v2, p0, Landroid/support/place/beacon/BeaconScanner;->mSendCallbacks:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Landroid/support/place/beacon/BeaconScanner;->mListener:Landroid/support/place/beacon/BeaconScanner$Listener;

    if-eqz v2, :cond_0

    iget-object v2, p0, Landroid/support/place/beacon/BeaconScanner;->mHandler:Landroid/os/Handler;

    new-instance v3, Landroid/support/place/beacon/BeaconScanner$1;

    invoke-direct {v3, p0, v0}, Landroid/support/place/beacon/BeaconScanner$1;-><init>(Landroid/support/place/beacon/BeaconScanner;Ljava/util/List;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    monitor-exit v1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "aah.BeaconScanner"

    const-string v2, "Error requesting places from BeaconScanner"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private startBeaconScanner()Z
    .locals 4

    iget-object v0, p0, Landroid/support/place/beacon/BeaconScanner;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/place/connector/BeaconScannerConstants;->getServiceIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Landroid/support/place/beacon/BeaconScanner;->mBeaconScannerConnection:Landroid/support/place/beacon/SafeServiceConnection;

    iget-object v2, p0, Landroid/support/place/beacon/BeaconScanner;->mContext:Landroid/content/Context;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/support/place/beacon/SafeServiceConnection;->bindService(Landroid/content/Context;Landroid/content/Intent;I)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public getBeacons()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Landroid/support/place/beacon/BeaconScanner;->mBeaconScanner:Landroid/support/place/beacon/IBeaconScanner;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Landroid/support/place/beacon/BeaconScanner;->mBeaconScanner:Landroid/support/place/beacon/IBeaconScanner;

    invoke-interface {v0}, Landroid/support/place/beacon/IBeaconScanner;->getBeacons()Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public scanForBeacons()Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/place/beacon/BeaconScanner;->mBeaconScanner:Landroid/support/place/beacon/IBeaconScanner;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Landroid/support/place/beacon/BeaconScanner;->mBeaconScanner:Landroid/support/place/beacon/IBeaconScanner;

    invoke-interface {v1}, Landroid/support/place/beacon/IBeaconScanner;->scanForBeacons()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public startListening()V
    .locals 2

    iget-object v1, p0, Landroid/support/place/beacon/BeaconScanner;->mLock:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Landroid/support/place/beacon/BeaconScanner;->mSendCallbacks:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Landroid/support/place/beacon/BeaconScanner;->mBeaconScanner:Landroid/support/place/beacon/IBeaconScanner;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Landroid/support/place/beacon/BeaconScanner;->postBeaconScannerChange()V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public stopListening()V
    .locals 2

    iget-object v1, p0, Landroid/support/place/beacon/BeaconScanner;->mLock:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Landroid/support/place/beacon/BeaconScanner;->mSendCallbacks:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public terminate()V
    .locals 2

    iget-object v0, p0, Landroid/support/place/beacon/BeaconScanner;->mBeaconScanner:Landroid/support/place/beacon/IBeaconScanner;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Landroid/support/place/beacon/BeaconScanner;->mBeaconScanner:Landroid/support/place/beacon/IBeaconScanner;

    iget-object v1, p0, Landroid/support/place/beacon/BeaconScanner;->mBeaconScannerListener:Landroid/support/place/beacon/IBeaconScannerListener;

    invoke-interface {v0, v1}, Landroid/support/place/beacon/IBeaconScanner;->removeListener(Landroid/support/place/beacon/IBeaconScannerListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v0, p0, Landroid/support/place/beacon/BeaconScanner;->mBeaconScannerConnection:Landroid/support/place/beacon/SafeServiceConnection;

    iget-object v1, p0, Landroid/support/place/beacon/BeaconScanner;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/support/place/beacon/SafeServiceConnection;->unbindService(Landroid/content/Context;)V

    :cond_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "aah.BeaconScanner"

    const-string v1, "Error removing listener from BeaconScanner while terminating(terminating anyway)"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
