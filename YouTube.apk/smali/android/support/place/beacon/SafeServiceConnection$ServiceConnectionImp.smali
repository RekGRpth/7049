.class Landroid/support/place/beacon/SafeServiceConnection$ServiceConnectionImp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/ServiceConnection;


# static fields
.field private static final STATE_BOUND:I = 0x1

.field private static final STATE_CONNECTED:I = 0x2

.field private static final STATE_START:I = 0x0

.field private static final STATE_UNBOUND_WAITING_FOR_LATE_CONNECT:I = 0x3


# instance fields
.field private mContextForDelayedUnbind:Landroid/content/Context;

.field private mFlags:I

.field private mService:Landroid/content/Intent;

.field private mState:I

.field final synthetic this$0:Landroid/support/place/beacon/SafeServiceConnection;


# direct methods
.method public constructor <init>(Landroid/support/place/beacon/SafeServiceConnection;)V
    .locals 1

    iput-object p1, p0, Landroid/support/place/beacon/SafeServiceConnection$ServiceConnectionImp;->this$0:Landroid/support/place/beacon/SafeServiceConnection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Landroid/support/place/beacon/SafeServiceConnection$ServiceConnectionImp;->mState:I

    return-void
.end method

.method private reportBadState()V
    .locals 3

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected method for state "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Landroid/support/place/beacon/SafeServiceConnection$ServiceConnectionImp;->mState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public bindService(Landroid/content/Context;Landroid/content/Intent;I)Z
    .locals 3

    const/4 v1, 0x0

    const/4 v0, 0x1

    monitor-enter p0

    :try_start_0
    iget v2, p0, Landroid/support/place/beacon/SafeServiceConnection$ServiceConnectionImp;->mState:I

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    invoke-direct {p0}, Landroid/support/place/beacon/SafeServiceConnection$ServiceConnectionImp;->reportBadState()V

    monitor-exit p0

    move v0, v1

    :goto_0
    return v0

    :pswitch_1
    iget-object v0, p0, Landroid/support/place/beacon/SafeServiceConnection$ServiceConnectionImp;->mService:Landroid/content/Intent;

    if-nez v0, :cond_2

    iput-object p2, p0, Landroid/support/place/beacon/SafeServiceConnection$ServiceConnectionImp;->mService:Landroid/content/Intent;

    iput p3, p0, Landroid/support/place/beacon/SafeServiceConnection$ServiceConnectionImp;->mFlags:I

    :cond_0
    const/4 v0, 0x1

    iput v0, p0, Landroid/support/place/beacon/SafeServiceConnection$ServiceConnectionImp;->mState:I

    invoke-virtual {p1, p2, p0, p3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v1, 0x0

    iput v1, p0, Landroid/support/place/beacon/SafeServiceConnection$ServiceConnectionImp;->mState:I

    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_1
    iget-object v0, p0, Landroid/support/place/beacon/SafeServiceConnection$ServiceConnectionImp;->mService:Landroid/content/Intent;

    invoke-virtual {v0, p2}, Landroid/content/Intent;->filterEquals(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "service must be equivalent for every call to bindService"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iget v0, p0, Landroid/support/place/beacon/SafeServiceConnection$ServiceConnectionImp;->mFlags:I

    if-eq v0, p3, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "flags must be equivalent for every call to bindService"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_2
    const/4 v1, 0x1

    iput v1, p0, Landroid/support/place/beacon/SafeServiceConnection$ServiceConnectionImp;->mState:I

    const/4 v1, 0x0

    iput-object v1, p0, Landroid/support/place/beacon/SafeServiceConnection$ServiceConnectionImp;->mContextForDelayedUnbind:Landroid/content/Context;

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v3, 0x2

    monitor-enter p0

    :try_start_0
    iget v2, p0, Landroid/support/place/beacon/SafeServiceConnection$ServiceConnectionImp;->mState:I

    packed-switch v2, :pswitch_data_0

    invoke-direct {p0}, Landroid/support/place/beacon/SafeServiceConnection$ServiceConnectionImp;->reportBadState()V

    :goto_0
    :pswitch_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v2, p0, Landroid/support/place/beacon/SafeServiceConnection$ServiceConnectionImp;->this$0:Landroid/support/place/beacon/SafeServiceConnection;

    invoke-virtual {v2, p1, p2}, Landroid/support/place/beacon/SafeServiceConnection;->onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V

    if-eqz v1, :cond_1

    monitor-enter p0

    :try_start_1
    iget v1, p0, Landroid/support/place/beacon/SafeServiceConnection$ServiceConnectionImp;->mState:I

    if-ne v1, v3, :cond_0

    const/4 v1, 0x0

    iput v1, p0, Landroid/support/place/beacon/SafeServiceConnection$ServiceConnectionImp;->mState:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-virtual {v0, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_0
    :goto_1
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :cond_1
    return-void

    :pswitch_1
    const/4 v2, 0x2

    :try_start_4
    iput v2, p0, Landroid/support/place/beacon/SafeServiceConnection$ServiceConnectionImp;->mState:I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :pswitch_2
    const/4 v1, 0x1

    :try_start_5
    iget-object v0, p0, Landroid/support/place/beacon/SafeServiceConnection$ServiceConnectionImp;->mContextForDelayedUnbind:Landroid/content/Context;

    const/4 v2, 0x0

    iput-object v2, p0, Landroid/support/place/beacon/SafeServiceConnection$ServiceConnectionImp;->mContextForDelayedUnbind:Landroid/content/Context;

    const/4 v2, 0x2

    iput v2, p0, Landroid/support/place/beacon/SafeServiceConnection$ServiceConnectionImp;->mState:I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_6
    const-string v1, "SafeServiceConnection"

    const-string v2, "Unable to perform delayed unbind."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1

    iget-object v0, p0, Landroid/support/place/beacon/SafeServiceConnection$ServiceConnectionImp;->this$0:Landroid/support/place/beacon/SafeServiceConnection;

    invoke-virtual {v0, p1}, Landroid/support/place/beacon/SafeServiceConnection;->onServiceDisconnected(Landroid/content/ComponentName;)V

    return-void
.end method

.method public unbindService(Landroid/content/Context;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Landroid/support/place/beacon/SafeServiceConnection$ServiceConnectionImp;->mState:I

    packed-switch v0, :pswitch_data_0

    invoke-direct {p0}, Landroid/support/place/beacon/SafeServiceConnection$ServiceConnectionImp;->reportBadState()V

    :goto_0
    monitor-exit p0

    return-void

    :pswitch_0
    const/4 v0, 0x3

    iput v0, p0, Landroid/support/place/beacon/SafeServiceConnection$ServiceConnectionImp;->mState:I

    iput-object p1, p0, Landroid/support/place/beacon/SafeServiceConnection$ServiceConnectionImp;->mContextForDelayedUnbind:Landroid/content/Context;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :pswitch_1
    const/4 v0, 0x0

    :try_start_1
    iput v0, p0, Landroid/support/place/beacon/SafeServiceConnection$ServiceConnectionImp;->mState:I

    invoke-virtual {p1, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
