.class public Landroid/support/place/connector/Broker$PlaceListener;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field binder:Landroid/support/place/connector/IPlaceListener;

.field handler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Landroid/support/place/connector/Broker$PlaceListener;->handler:Landroid/os/Handler;

    new-instance v0, Landroid/support/place/connector/Broker$PlaceListener$1;

    invoke-direct {v0, p0}, Landroid/support/place/connector/Broker$PlaceListener$1;-><init>(Landroid/support/place/connector/Broker$PlaceListener;)V

    iput-object v0, p0, Landroid/support/place/connector/Broker$PlaceListener;->binder:Landroid/support/place/connector/IPlaceListener;

    return-void
.end method


# virtual methods
.method public onPlaceAdded(Landroid/support/place/connector/PlaceInfo;)V
    .locals 0

    return-void
.end method

.method public onPlaceRemoved(Landroid/support/place/connector/PlaceInfo;)V
    .locals 0

    return-void
.end method

.method public onPlaceUpdated(Landroid/support/place/connector/PlaceInfo;)V
    .locals 0

    return-void
.end method
