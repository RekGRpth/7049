.class public Landroid/support/place/connector/BrokerConnection;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final HUB_APK_PACKAGE:Ljava/lang/String; = "com.google.android.hubbroker"

.field private static final PHONE_APK_PACKAGE:Ljava/lang/String; = "com.google.android.setupwarlock"

.field private static final TAG:Ljava/lang/String; = "aah.BrokerConnection"

.field private static mBrokerApk:Ljava/lang/String;


# instance fields
.field mBroker:Landroid/support/place/connector/Broker;

.field mBrokerConnection:Landroid/support/place/connector/IBrokerConnection$Stub;

.field mBrokerService:Landroid/support/place/connector/IBrokerService;

.field private final mBrokerServiceDeath:Landroid/os/IBinder$DeathRecipient;

.field mContainer:Landroid/support/place/connector/ConnectorContainer;

.field final mContext:Landroid/content/Context;

.field final mHandler:Landroid/os/Handler;

.field mListener:Landroid/support/place/connector/BrokerConnection$ListenerRec;

.field mServiceConnection:Landroid/content/ServiceConnection;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    invoke-direct {p0, p1, v0}, Landroid/support/place/connector/BrokerConnection;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/support/place/connector/BrokerConnection$1;

    invoke-direct {v0, p0}, Landroid/support/place/connector/BrokerConnection$1;-><init>(Landroid/support/place/connector/BrokerConnection;)V

    iput-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mServiceConnection:Landroid/content/ServiceConnection;

    new-instance v0, Landroid/support/place/connector/BrokerConnection$2;

    invoke-direct {v0, p0}, Landroid/support/place/connector/BrokerConnection$2;-><init>(Landroid/support/place/connector/BrokerConnection;)V

    iput-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mBrokerServiceDeath:Landroid/os/IBinder$DeathRecipient;

    iput-object p1, p0, Landroid/support/place/connector/BrokerConnection;->mContext:Landroid/content/Context;

    iput-object v1, p0, Landroid/support/place/connector/BrokerConnection;->mListener:Landroid/support/place/connector/BrokerConnection$ListenerRec;

    iput-object v1, p0, Landroid/support/place/connector/BrokerConnection;->mContainer:Landroid/support/place/connector/ConnectorContainer;

    iput-object p2, p0, Landroid/support/place/connector/BrokerConnection;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/support/place/connector/ConnectorContainer;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/support/place/connector/BrokerConnection;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Landroid/support/place/connector/BrokerConnection;->mContainer:Landroid/support/place/connector/ConnectorContainer;

    new-instance v0, Landroid/support/place/connector/BrokerConnection$Callback;

    invoke-direct {v0, p0}, Landroid/support/place/connector/BrokerConnection$Callback;-><init>(Landroid/support/place/connector/BrokerConnection;)V

    iput-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mBrokerConnection:Landroid/support/place/connector/IBrokerConnection$Stub;

    return-void
.end method

.method static synthetic access$100(Landroid/support/place/connector/BrokerConnection;)V
    .locals 0

    invoke-direct {p0}, Landroid/support/place/connector/BrokerConnection;->handleBrokerServiceDisconnected()V

    return-void
.end method

.method static synthetic access$200(Landroid/support/place/connector/BrokerConnection;Landroid/support/place/connector/IBrokerService;Landroid/support/place/rpc/EndpointInfo;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/support/place/connector/BrokerConnection;->handleBrokerConnected(Landroid/support/place/connector/IBrokerService;Landroid/support/place/rpc/EndpointInfo;)V

    return-void
.end method

.method static synthetic access$300(Landroid/support/place/connector/BrokerConnection;Landroid/support/place/connector/PlaceInfo;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/support/place/connector/BrokerConnection;->handlePlaceConnected(Landroid/support/place/connector/PlaceInfo;)V

    return-void
.end method

.method static synthetic access$400(Landroid/support/place/connector/BrokerConnection;)V
    .locals 0

    invoke-direct {p0}, Landroid/support/place/connector/BrokerConnection;->handlePlaceDisconnected()V

    return-void
.end method

.method static synthetic access$500(Landroid/support/place/connector/BrokerConnection;)V
    .locals 0

    invoke-direct {p0}, Landroid/support/place/connector/BrokerConnection;->handleBrokerDisconnected()V

    return-void
.end method

.method public static bindToBroker(Landroid/content/Context;Landroid/content/ServiceConnection;)V
    .locals 5

    invoke-static {p0}, Landroid/support/place/connector/BrokerConnection;->getServiceIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    if-nez v1, :cond_0

    const-string v0, "aah.BrokerConnection"

    const-string v1, "Broker is not installed"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0, v1, p1, v0}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    const-string v0, "aah.BrokerConnection"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "binding to service: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "aah.BrokerConnection"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error connecting to broker service: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static getBrokerAPK(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    sget-object v0, Landroid/support/place/connector/BrokerConnection;->mBrokerApk:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "com.google.android.hubbroker"

    invoke-static {p0, v0}, Landroid/support/place/connector/BrokerConnection;->getPackage(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Landroid/support/place/connector/BrokerConnection;->mBrokerApk:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "com.google.android.setupwarlock"

    invoke-static {p0, v0}, Landroid/support/place/connector/BrokerConnection;->getPackage(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Landroid/support/place/connector/BrokerConnection;->mBrokerApk:Ljava/lang/String;

    :cond_0
    sget-object v0, Landroid/support/place/connector/BrokerConnection;->mBrokerApk:Ljava/lang/String;

    return-object v0
.end method

.method private static getPackage(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object p1

    :catch_0
    move-exception v0

    const/4 p1, 0x0

    goto :goto_0
.end method

.method public static getServiceIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 4

    invoke-static {p0}, Landroid/support/place/connector/BrokerConnection;->getBrokerAPK(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    new-instance v2, Landroid/content/ComponentName;

    const-string v3, "com.android.athome.broker.service.BrokerService"

    invoke-direct {v2, v1, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    goto :goto_0
.end method

.method private handleBrokerConnected(Landroid/support/place/connector/IBrokerService;Landroid/support/place/rpc/EndpointInfo;)V
    .locals 3

    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mBroker:Landroid/support/place/connector/Broker;

    if-eqz v0, :cond_0

    const-string v0, "aah.BrokerConnection"

    const-string v1, "received BrokerConnected but already connected to broker"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0, p1, p2}, Landroid/support/place/connector/BrokerConnection;->createBroker(Landroid/support/place/connector/IBrokerService;Landroid/support/place/rpc/EndpointInfo;)Landroid/support/place/connector/Broker;

    move-result-object v0

    iput-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mBroker:Landroid/support/place/connector/Broker;

    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mContainer:Landroid/support/place/connector/ConnectorContainer;

    if-eqz v0, :cond_1

    iput-object p1, p0, Landroid/support/place/connector/BrokerConnection;->mBrokerService:Landroid/support/place/connector/IBrokerService;

    :try_start_0
    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mBrokerService:Landroid/support/place/connector/IBrokerService;

    invoke-interface {v0}, Landroid/support/place/connector/IBrokerService;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    iget-object v1, p0, Landroid/support/place/connector/BrokerConnection;->mBrokerServiceDeath:Landroid/os/IBinder$DeathRecipient;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    invoke-direct {p0}, Landroid/support/place/connector/BrokerConnection;->updateListener()V

    return-void

    :catch_0
    move-exception v0

    const-string v1, "aah.BrokerConnection"

    const-string v2, "handleBrokerConnected linkToDeath failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private handleBrokerDisconnected()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mBroker:Landroid/support/place/connector/Broker;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mBroker:Landroid/support/place/connector/Broker;

    invoke-virtual {v0, v3}, Landroid/support/place/connector/Broker;->setPlace(Landroid/support/place/connector/PlaceInfo;)V

    iput-object v3, p0, Landroid/support/place/connector/BrokerConnection;->mBroker:Landroid/support/place/connector/Broker;

    :cond_0
    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mContainer:Landroid/support/place/connector/ConnectorContainer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mBrokerService:Landroid/support/place/connector/IBrokerService;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mBrokerService:Landroid/support/place/connector/IBrokerService;

    invoke-interface {v0}, Landroid/support/place/connector/IBrokerService;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    iget-object v1, p0, Landroid/support/place/connector/BrokerConnection;->mBrokerServiceDeath:Landroid/os/IBinder$DeathRecipient;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    iput-object v3, p0, Landroid/support/place/connector/BrokerConnection;->mBrokerService:Landroid/support/place/connector/IBrokerService;

    :cond_1
    invoke-direct {p0}, Landroid/support/place/connector/BrokerConnection;->updateListener()V

    return-void
.end method

.method private handleBrokerServiceDisconnected()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mListener:Landroid/support/place/connector/BrokerConnection$ListenerRec;

    if-eqz v0, :cond_0

    const-string v0, "aah.BrokerConnection"

    const-string v1, "Disconnected from BrokerService with a  listener still attached"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iput-object v2, p0, Landroid/support/place/connector/BrokerConnection;->mBrokerService:Landroid/support/place/connector/IBrokerService;

    iput-object v2, p0, Landroid/support/place/connector/BrokerConnection;->mBroker:Landroid/support/place/connector/Broker;

    invoke-direct {p0}, Landroid/support/place/connector/BrokerConnection;->updateListener()V

    return-void
.end method

.method private handlePlaceConnected(Landroid/support/place/connector/PlaceInfo;)V
    .locals 2

    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mBroker:Landroid/support/place/connector/Broker;

    if-nez v0, :cond_0

    const-string v0, "aah.BrokerConnection"

    const-string v1, "received PlaceConnected but not connected to broker; ignoring"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mBroker:Landroid/support/place/connector/Broker;

    invoke-virtual {v0, p1}, Landroid/support/place/connector/Broker;->setPlace(Landroid/support/place/connector/PlaceInfo;)V

    invoke-direct {p0}, Landroid/support/place/connector/BrokerConnection;->updateListener()V

    goto :goto_0
.end method

.method private handlePlaceDisconnected()V
    .locals 2

    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mBroker:Landroid/support/place/connector/Broker;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mBroker:Landroid/support/place/connector/Broker;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/place/connector/Broker;->setPlace(Landroid/support/place/connector/PlaceInfo;)V

    :cond_0
    invoke-direct {p0}, Landroid/support/place/connector/BrokerConnection;->updateListener()V

    return-void
.end method

.method public static isBrokerInstalled(Landroid/content/Context;)Z
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    :try_start_0
    invoke-static {p0}, Landroid/support/place/connector/BrokerConnection;->getBrokerAPK(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private updateDisconnectedListener(Landroid/support/place/connector/BrokerConnection$ListenerRec;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p1, Landroid/support/place/connector/BrokerConnection$ListenerRec;->place:Landroid/support/place/connector/PlaceInfo;

    if-eqz v0, :cond_0

    iget-object v0, p1, Landroid/support/place/connector/BrokerConnection$ListenerRec;->listener:Landroid/support/place/connector/BrokerConnection$Listener;

    invoke-interface {v0}, Landroid/support/place/connector/BrokerConnection$Listener;->onPlaceDisconnected()V

    :cond_0
    iget-object v0, p1, Landroid/support/place/connector/BrokerConnection$ListenerRec;->broker:Landroid/support/place/connector/Broker;

    if-eqz v0, :cond_1

    iget-object v0, p1, Landroid/support/place/connector/BrokerConnection$ListenerRec;->listener:Landroid/support/place/connector/BrokerConnection$Listener;

    invoke-interface {v0}, Landroid/support/place/connector/BrokerConnection$Listener;->onBrokerDisconnected()V

    :cond_1
    iput-object v1, p1, Landroid/support/place/connector/BrokerConnection$ListenerRec;->place:Landroid/support/place/connector/PlaceInfo;

    iput-object v1, p1, Landroid/support/place/connector/BrokerConnection$ListenerRec;->broker:Landroid/support/place/connector/Broker;

    return-void
.end method

.method private updateListener()V
    .locals 2

    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mListener:Landroid/support/place/connector/BrokerConnection$ListenerRec;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mBroker:Landroid/support/place/connector/Broker;

    if-nez v0, :cond_2

    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mListener:Landroid/support/place/connector/BrokerConnection$ListenerRec;

    invoke-direct {p0, v0}, Landroid/support/place/connector/BrokerConnection;->updateDisconnectedListener(Landroid/support/place/connector/BrokerConnection$ListenerRec;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mListener:Landroid/support/place/connector/BrokerConnection$ListenerRec;

    iget-object v0, v0, Landroid/support/place/connector/BrokerConnection$ListenerRec;->broker:Landroid/support/place/connector/Broker;

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mListener:Landroid/support/place/connector/BrokerConnection$ListenerRec;

    iget-object v0, v0, Landroid/support/place/connector/BrokerConnection$ListenerRec;->broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/connector/BrokerConnection;->mBroker:Landroid/support/place/connector/Broker;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mListener:Landroid/support/place/connector/BrokerConnection$ListenerRec;

    invoke-direct {p0, v0}, Landroid/support/place/connector/BrokerConnection;->updateDisconnectedListener(Landroid/support/place/connector/BrokerConnection$ListenerRec;)V

    :cond_3
    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mListener:Landroid/support/place/connector/BrokerConnection$ListenerRec;

    iget-object v0, v0, Landroid/support/place/connector/BrokerConnection$ListenerRec;->broker:Landroid/support/place/connector/Broker;

    if-nez v0, :cond_4

    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mListener:Landroid/support/place/connector/BrokerConnection$ListenerRec;

    iget-object v1, p0, Landroid/support/place/connector/BrokerConnection;->mBroker:Landroid/support/place/connector/Broker;

    iput-object v1, v0, Landroid/support/place/connector/BrokerConnection$ListenerRec;->broker:Landroid/support/place/connector/Broker;

    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mListener:Landroid/support/place/connector/BrokerConnection$ListenerRec;

    iget-object v0, v0, Landroid/support/place/connector/BrokerConnection$ListenerRec;->listener:Landroid/support/place/connector/BrokerConnection$Listener;

    iget-object v1, p0, Landroid/support/place/connector/BrokerConnection;->mListener:Landroid/support/place/connector/BrokerConnection$ListenerRec;

    iget-object v1, v1, Landroid/support/place/connector/BrokerConnection$ListenerRec;->broker:Landroid/support/place/connector/Broker;

    invoke-interface {v0, v1}, Landroid/support/place/connector/BrokerConnection$Listener;->onBrokerConnected(Landroid/support/place/connector/Broker;)V

    :cond_4
    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mBroker:Landroid/support/place/connector/Broker;

    invoke-virtual {v0}, Landroid/support/place/connector/Broker;->getPlace()Landroid/support/place/connector/PlaceInfo;

    move-result-object v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mListener:Landroid/support/place/connector/BrokerConnection$ListenerRec;

    iget-object v0, v0, Landroid/support/place/connector/BrokerConnection$ListenerRec;->place:Landroid/support/place/connector/PlaceInfo;

    if-nez v0, :cond_5

    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mListener:Landroid/support/place/connector/BrokerConnection$ListenerRec;

    iget-object v1, p0, Landroid/support/place/connector/BrokerConnection;->mBroker:Landroid/support/place/connector/Broker;

    invoke-virtual {v1}, Landroid/support/place/connector/Broker;->getPlace()Landroid/support/place/connector/PlaceInfo;

    move-result-object v1

    iput-object v1, v0, Landroid/support/place/connector/BrokerConnection$ListenerRec;->place:Landroid/support/place/connector/PlaceInfo;

    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mListener:Landroid/support/place/connector/BrokerConnection$ListenerRec;

    iget-object v0, v0, Landroid/support/place/connector/BrokerConnection$ListenerRec;->listener:Landroid/support/place/connector/BrokerConnection$Listener;

    iget-object v1, p0, Landroid/support/place/connector/BrokerConnection;->mListener:Landroid/support/place/connector/BrokerConnection$ListenerRec;

    iget-object v1, v1, Landroid/support/place/connector/BrokerConnection$ListenerRec;->place:Landroid/support/place/connector/PlaceInfo;

    invoke-interface {v0, v1}, Landroid/support/place/connector/BrokerConnection$Listener;->onPlaceConnected(Landroid/support/place/connector/PlaceInfo;)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mListener:Landroid/support/place/connector/BrokerConnection$ListenerRec;

    iget-object v0, v0, Landroid/support/place/connector/BrokerConnection$ListenerRec;->place:Landroid/support/place/connector/PlaceInfo;

    invoke-virtual {v0}, Landroid/support/place/connector/PlaceInfo;->getPlaceId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Landroid/support/place/connector/BrokerConnection;->mBroker:Landroid/support/place/connector/Broker;

    invoke-virtual {v1}, Landroid/support/place/connector/Broker;->getPlace()Landroid/support/place/connector/PlaceInfo;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/place/connector/PlaceInfo;->getPlaceId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mListener:Landroid/support/place/connector/BrokerConnection$ListenerRec;

    iget-object v1, p0, Landroid/support/place/connector/BrokerConnection;->mBroker:Landroid/support/place/connector/Broker;

    invoke-virtual {v1}, Landroid/support/place/connector/Broker;->getPlace()Landroid/support/place/connector/PlaceInfo;

    move-result-object v1

    iput-object v1, v0, Landroid/support/place/connector/BrokerConnection$ListenerRec;->place:Landroid/support/place/connector/PlaceInfo;

    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mListener:Landroid/support/place/connector/BrokerConnection$ListenerRec;

    iget-object v0, v0, Landroid/support/place/connector/BrokerConnection$ListenerRec;->listener:Landroid/support/place/connector/BrokerConnection$Listener;

    invoke-interface {v0}, Landroid/support/place/connector/BrokerConnection$Listener;->onPlaceDisconnected()V

    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mListener:Landroid/support/place/connector/BrokerConnection$ListenerRec;

    iget-object v0, v0, Landroid/support/place/connector/BrokerConnection$ListenerRec;->listener:Landroid/support/place/connector/BrokerConnection$Listener;

    iget-object v1, p0, Landroid/support/place/connector/BrokerConnection;->mListener:Landroid/support/place/connector/BrokerConnection$ListenerRec;

    iget-object v1, v1, Landroid/support/place/connector/BrokerConnection$ListenerRec;->place:Landroid/support/place/connector/PlaceInfo;

    invoke-interface {v0, v1}, Landroid/support/place/connector/BrokerConnection$Listener;->onPlaceConnected(Landroid/support/place/connector/PlaceInfo;)V

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mListener:Landroid/support/place/connector/BrokerConnection$ListenerRec;

    iget-object v0, v0, Landroid/support/place/connector/BrokerConnection$ListenerRec;->place:Landroid/support/place/connector/PlaceInfo;

    iget-object v1, p0, Landroid/support/place/connector/BrokerConnection;->mBroker:Landroid/support/place/connector/Broker;

    invoke-virtual {v1}, Landroid/support/place/connector/Broker;->getPlace()Landroid/support/place/connector/PlaceInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/place/connector/PlaceInfo;->hasSameMaster(Landroid/support/place/connector/PlaceInfo;)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mListener:Landroid/support/place/connector/BrokerConnection$ListenerRec;

    iget-object v1, p0, Landroid/support/place/connector/BrokerConnection;->mBroker:Landroid/support/place/connector/Broker;

    invoke-virtual {v1}, Landroid/support/place/connector/Broker;->getPlace()Landroid/support/place/connector/PlaceInfo;

    move-result-object v1

    iput-object v1, v0, Landroid/support/place/connector/BrokerConnection$ListenerRec;->place:Landroid/support/place/connector/PlaceInfo;

    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mListener:Landroid/support/place/connector/BrokerConnection$ListenerRec;

    iget-object v0, v0, Landroid/support/place/connector/BrokerConnection$ListenerRec;->listener:Landroid/support/place/connector/BrokerConnection$Listener;

    iget-object v1, p0, Landroid/support/place/connector/BrokerConnection;->mListener:Landroid/support/place/connector/BrokerConnection$ListenerRec;

    iget-object v1, v1, Landroid/support/place/connector/BrokerConnection$ListenerRec;->place:Landroid/support/place/connector/PlaceInfo;

    invoke-virtual {v1}, Landroid/support/place/connector/PlaceInfo;->getMaster()Landroid/support/place/rpc/EndpointInfo;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/support/place/connector/BrokerConnection$Listener;->onMasterChanged(Landroid/support/place/rpc/EndpointInfo;)V

    goto/16 :goto_0

    :cond_7
    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mListener:Landroid/support/place/connector/BrokerConnection$ListenerRec;

    iget-object v0, v0, Landroid/support/place/connector/BrokerConnection$ListenerRec;->place:Landroid/support/place/connector/PlaceInfo;

    invoke-virtual {v0}, Landroid/support/place/connector/PlaceInfo;->getPlaceName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Landroid/support/place/connector/BrokerConnection;->mBroker:Landroid/support/place/connector/Broker;

    invoke-virtual {v1}, Landroid/support/place/connector/Broker;->getPlace()Landroid/support/place/connector/PlaceInfo;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/place/connector/PlaceInfo;->getPlaceName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mListener:Landroid/support/place/connector/BrokerConnection$ListenerRec;

    iget-object v1, p0, Landroid/support/place/connector/BrokerConnection;->mBroker:Landroid/support/place/connector/Broker;

    invoke-virtual {v1}, Landroid/support/place/connector/Broker;->getPlace()Landroid/support/place/connector/PlaceInfo;

    move-result-object v1

    iput-object v1, v0, Landroid/support/place/connector/BrokerConnection$ListenerRec;->place:Landroid/support/place/connector/PlaceInfo;

    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mListener:Landroid/support/place/connector/BrokerConnection$ListenerRec;

    iget-object v0, v0, Landroid/support/place/connector/BrokerConnection$ListenerRec;->listener:Landroid/support/place/connector/BrokerConnection$Listener;

    iget-object v1, p0, Landroid/support/place/connector/BrokerConnection;->mListener:Landroid/support/place/connector/BrokerConnection$ListenerRec;

    iget-object v1, v1, Landroid/support/place/connector/BrokerConnection$ListenerRec;->place:Landroid/support/place/connector/PlaceInfo;

    invoke-virtual {v1}, Landroid/support/place/connector/PlaceInfo;->getPlaceName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/support/place/connector/BrokerConnection$Listener;->onPlaceNameChanged(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mListener:Landroid/support/place/connector/BrokerConnection$ListenerRec;

    iget-object v0, v0, Landroid/support/place/connector/BrokerConnection$ListenerRec;->place:Landroid/support/place/connector/PlaceInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mListener:Landroid/support/place/connector/BrokerConnection$ListenerRec;

    const/4 v1, 0x0

    iput-object v1, v0, Landroid/support/place/connector/BrokerConnection$ListenerRec;->place:Landroid/support/place/connector/PlaceInfo;

    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mListener:Landroid/support/place/connector/BrokerConnection$ListenerRec;

    iget-object v0, v0, Landroid/support/place/connector/BrokerConnection$ListenerRec;->listener:Landroid/support/place/connector/BrokerConnection$Listener;

    invoke-interface {v0}, Landroid/support/place/connector/BrokerConnection$Listener;->onPlaceDisconnected()V

    goto/16 :goto_0
.end method


# virtual methods
.method public connect(Landroid/support/place/connector/BrokerConnection$Listener;)Z
    .locals 5

    const/4 v0, 0x0

    const/4 v4, 0x0

    iget-object v1, p0, Landroid/support/place/connector/BrokerConnection;->mListener:Landroid/support/place/connector/BrokerConnection$ListenerRec;

    if-eqz v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This BrokerConnection is already in use. Call disconnect first"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v1, p0, Landroid/support/place/connector/BrokerConnection;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/support/place/connector/BrokerConnection;->isBrokerInstalled(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "aah.BrokerConnection"

    const-string v2, "Broker is not installed"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-object v4, p0, Landroid/support/place/connector/BrokerConnection;->mListener:Landroid/support/place/connector/BrokerConnection$ListenerRec;

    :goto_0
    return v0

    :cond_1
    const-string v1, "aah.BrokerConnection"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "connect "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Landroid/support/place/connector/BrokerConnection$ListenerRec;

    invoke-direct {v1, p0, v4}, Landroid/support/place/connector/BrokerConnection$ListenerRec;-><init>(Landroid/support/place/connector/BrokerConnection;Landroid/support/place/connector/BrokerConnection$1;)V

    iput-object p1, v1, Landroid/support/place/connector/BrokerConnection$ListenerRec;->listener:Landroid/support/place/connector/BrokerConnection$Listener;

    iput-object v1, p0, Landroid/support/place/connector/BrokerConnection;->mListener:Landroid/support/place/connector/BrokerConnection$ListenerRec;

    iget-object v1, p0, Landroid/support/place/connector/BrokerConnection;->mContainer:Landroid/support/place/connector/ConnectorContainer;

    if-nez v1, :cond_2

    :try_start_0
    iget-object v1, p0, Landroid/support/place/connector/BrokerConnection;->mContext:Landroid/content/Context;

    iget-object v2, p0, Landroid/support/place/connector/BrokerConnection;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-static {v1, v2}, Landroid/support/place/connector/BrokerConnection;->bindToBroker(Landroid/content/Context;Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    invoke-direct {p0}, Landroid/support/place/connector/BrokerConnection;->updateListener()V

    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    iput-object v4, p0, Landroid/support/place/connector/BrokerConnection;->mListener:Landroid/support/place/connector/BrokerConnection$ListenerRec;

    goto :goto_0
.end method

.method createBroker(Landroid/support/place/connector/IBrokerService;Landroid/support/place/rpc/EndpointInfo;)Landroid/support/place/connector/Broker;
    .locals 7

    new-instance v0, Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/connector/BrokerConnection;->mBrokerConnection:Landroid/support/place/connector/IBrokerConnection$Stub;

    iget-object v2, p0, Landroid/support/place/connector/BrokerConnection;->mContainer:Landroid/support/place/connector/ConnectorContainer;

    const/4 v5, 0x0

    iget-object v6, p0, Landroid/support/place/connector/BrokerConnection;->mHandler:Landroid/os/Handler;

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v6}, Landroid/support/place/connector/Broker;-><init>(Landroid/support/place/connector/IBrokerConnection;Landroid/support/place/connector/ConnectorContainer;Landroid/support/place/connector/IBrokerService;Landroid/support/place/rpc/EndpointInfo;Landroid/support/place/rpc/EndpointInfo;Landroid/os/Handler;)V

    return-object v0
.end method

.method public disconnect(Landroid/support/place/connector/BrokerConnection$Listener;)V
    .locals 4

    const/4 v3, 0x0

    const-string v0, "aah.BrokerConnection"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "disconnect "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mListener:Landroid/support/place/connector/BrokerConnection$ListenerRec;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "listener not attached: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mBroker:Landroid/support/place/connector/Broker;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mBroker:Landroid/support/place/connector/Broker;

    invoke-virtual {v0, v3}, Landroid/support/place/connector/Broker;->setPlace(Landroid/support/place/connector/PlaceInfo;)V

    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mBroker:Landroid/support/place/connector/Broker;

    iput-object v3, v0, Landroid/support/place/connector/Broker;->mConnection:Landroid/support/place/connector/IBrokerConnection;

    :cond_1
    :try_start_0
    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mBrokerService:Landroid/support/place/connector/IBrokerService;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mBrokerService:Landroid/support/place/connector/IBrokerService;

    iget-object v1, p0, Landroid/support/place/connector/BrokerConnection;->mBrokerConnection:Landroid/support/place/connector/IBrokerConnection$Stub;

    invoke-interface {v0, v1}, Landroid/support/place/connector/IBrokerService;->cancelRequests(Landroid/support/place/connector/IBrokerConnection;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_2

    :cond_2
    :goto_0
    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mContainer:Landroid/support/place/connector/ConnectorContainer;

    if-nez v0, :cond_4

    :try_start_1
    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mBrokerService:Landroid/support/place/connector/IBrokerService;

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mBrokerService:Landroid/support/place/connector/IBrokerService;

    iget-object v1, p0, Landroid/support/place/connector/BrokerConnection;->mBrokerConnection:Landroid/support/place/connector/IBrokerConnection$Stub;

    invoke-interface {v0, v1}, Landroid/support/place/connector/IBrokerService;->unregisterCallback(Landroid/support/place/connector/IBrokerConnection;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_3
    :goto_1
    :try_start_2
    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mContext:Landroid/content/Context;

    iget-object v1, p0, Landroid/support/place/connector/BrokerConnection;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0

    :goto_2
    iput-object v3, p0, Landroid/support/place/connector/BrokerConnection;->mBrokerService:Landroid/support/place/connector/IBrokerService;

    iput-object v3, p0, Landroid/support/place/connector/BrokerConnection;->mBroker:Landroid/support/place/connector/Broker;

    iput-object v3, p0, Landroid/support/place/connector/BrokerConnection;->mListener:Landroid/support/place/connector/BrokerConnection$ListenerRec;

    :cond_4
    return-void

    :catch_0
    move-exception v0

    goto :goto_2

    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_0
.end method

.method public getBroker()Landroid/support/place/connector/Broker;
    .locals 1

    iget-object v0, p0, Landroid/support/place/connector/BrokerConnection;->mBroker:Landroid/support/place/connector/Broker;

    return-object v0
.end method

.method getBrokerService(Landroid/os/IBinder;)Landroid/support/place/connector/IBrokerService;
    .locals 1

    invoke-static {p1}, Landroid/support/place/connector/IBrokerService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/support/place/connector/IBrokerService;

    move-result-object v0

    return-object v0
.end method
