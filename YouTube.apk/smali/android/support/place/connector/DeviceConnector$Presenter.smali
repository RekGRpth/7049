.class Landroid/support/place/connector/DeviceConnector$Presenter;
.super Landroid/support/place/connector/EventListener;
.source "SourceFile"


# instance fields
.field private _listener:Landroid/support/place/connector/DeviceConnector$Listener;

.field final synthetic this$0:Landroid/support/place/connector/DeviceConnector;


# direct methods
.method public constructor <init>(Landroid/support/place/connector/DeviceConnector;Landroid/support/place/connector/Broker;Landroid/support/place/connector/DeviceConnector$Listener;)V
    .locals 0

    iput-object p1, p0, Landroid/support/place/connector/DeviceConnector$Presenter;->this$0:Landroid/support/place/connector/DeviceConnector;

    invoke-direct {p0, p2, p3}, Landroid/support/place/connector/EventListener;-><init>(Landroid/support/place/connector/Broker;Landroid/support/place/connector/EventListener$Listener;)V

    iput-object p3, p0, Landroid/support/place/connector/DeviceConnector$Presenter;->_listener:Landroid/support/place/connector/DeviceConnector$Listener;

    return-void
.end method


# virtual methods
.method public process(Ljava/lang/String;[BLandroid/support/place/rpc/RpcContext;Landroid/support/place/rpc/RpcError;)[B
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p2}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v1, "onDeviceNameChanged"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "name"

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Landroid/support/place/connector/DeviceConnector$Presenter;->_listener:Landroid/support/place/connector/DeviceConnector$Listener;

    invoke-virtual {v1, v0, p3}, Landroid/support/place/connector/DeviceConnector$Listener;->onDeviceNameChanged(Ljava/lang/String;Landroid/support/place/rpc/RpcContext;)V

    :goto_0
    const/4 v0, 0x0

    :goto_1
    return-object v0

    :cond_0
    const-string v1, "onLegalInformationReady"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "info"

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Landroid/support/place/connector/DeviceConnector$Presenter;->_listener:Landroid/support/place/connector/DeviceConnector$Listener;

    invoke-virtual {v1, v0, p3}, Landroid/support/place/connector/DeviceConnector$Listener;->onLegalInformationReady(Ljava/lang/String;Landroid/support/place/rpc/RpcContext;)V

    goto :goto_0

    :cond_1
    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/place/connector/EventListener;->process(Ljava/lang/String;[BLandroid/support/place/rpc/RpcContext;Landroid/support/place/rpc/RpcError;)[B

    move-result-object v0

    goto :goto_1
.end method
