.class public Landroid/support/place/connector/DeviceConnector;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private _broker:Landroid/support/place/connector/Broker;

.field private _endpoint:Landroid/support/place/rpc/EndpointInfo;

.field private _presenter:Landroid/support/place/connector/DeviceConnector$Presenter;


# direct methods
.method public constructor <init>(Landroid/support/place/connector/Broker;Landroid/support/place/rpc/EndpointInfo;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/support/place/connector/DeviceConnector;->_broker:Landroid/support/place/connector/Broker;

    iput-object p2, p0, Landroid/support/place/connector/DeviceConnector;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    return-void
.end method


# virtual methods
.method public factoryReset(Ljava/lang/String;Landroid/support/place/connector/DeviceConnector$OnFactoryReset;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v0, "confirmation"

    invoke-virtual {v3, v0, p1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/place/connector/DeviceConnector;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/connector/DeviceConnector;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "factoryReset"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    new-instance v4, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;

    const/16 v5, 0x16

    invoke-direct {v4, p0, v5, p2}, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;-><init>(Landroid/support/place/connector/DeviceConnector;ILjava/lang/Object;)V

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public getAdbState(Landroid/support/place/connector/DeviceConnector$OnGetAdbState;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    iget-object v0, p0, Landroid/support/place/connector/DeviceConnector;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/connector/DeviceConnector;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "getAdbState"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    new-instance v4, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;

    const/16 v5, 0xd

    invoke-direct {v4, p0, v5, p1}, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;-><init>(Landroid/support/place/connector/DeviceConnector;ILjava/lang/Object;)V

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public getAvailableUpdate(Landroid/support/place/connector/DeviceConnector$OnGetAvailableUpdate;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    iget-object v0, p0, Landroid/support/place/connector/DeviceConnector;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/connector/DeviceConnector;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "getAvailableUpdate"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    new-instance v4, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;

    const/16 v5, 0xa

    invoke-direct {v4, p0, v5, p1}, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;-><init>(Landroid/support/place/connector/DeviceConnector;ILjava/lang/Object;)V

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public getBluetoothMac(Landroid/support/place/connector/DeviceConnector$OnGetBluetoothMac;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    iget-object v0, p0, Landroid/support/place/connector/DeviceConnector;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/connector/DeviceConnector;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "getBluetoothMac"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    new-instance v4, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;

    const/4 v5, 0x4

    invoke-direct {v4, p0, v5, p1}, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;-><init>(Landroid/support/place/connector/DeviceConnector;ILjava/lang/Object;)V

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public getBuildVersion(Landroid/support/place/connector/DeviceConnector$OnGetBuildVersion;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    iget-object v0, p0, Landroid/support/place/connector/DeviceConnector;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/connector/DeviceConnector;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "getBuildVersion"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    new-instance v4, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;

    const/16 v5, 0x9

    invoke-direct {v4, p0, v5, p1}, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;-><init>(Landroid/support/place/connector/DeviceConnector;ILjava/lang/Object;)V

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public getDebugInfo(Landroid/support/place/connector/DeviceConnector$OnGetDebugInfo;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    iget-object v0, p0, Landroid/support/place/connector/DeviceConnector;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/connector/DeviceConnector;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "getDebugInfo"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    new-instance v4, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;

    const/16 v5, 0xb

    invoke-direct {v4, p0, v5, p1}, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;-><init>(Landroid/support/place/connector/DeviceConnector;ILjava/lang/Object;)V

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public getDeviceName(Landroid/support/place/connector/DeviceConnector$OnGetDeviceName;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    iget-object v0, p0, Landroid/support/place/connector/DeviceConnector;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/connector/DeviceConnector;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "getDeviceName"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    new-instance v4, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;

    const/4 v5, 0x6

    invoke-direct {v4, p0, v5, p1}, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;-><init>(Landroid/support/place/connector/DeviceConnector;ILjava/lang/Object;)V

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public getDeviceSerialNumber(Landroid/support/place/connector/DeviceConnector$OnGetDeviceSerialNumber;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    iget-object v0, p0, Landroid/support/place/connector/DeviceConnector;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/connector/DeviceConnector;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "getDeviceSerialNumber"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    new-instance v4, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;

    const/4 v5, 0x7

    invoke-direct {v4, p0, v5, p1}, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;-><init>(Landroid/support/place/connector/DeviceConnector;ILjava/lang/Object;)V

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public getDeviceState(Landroid/support/place/connector/DeviceConnector$OnGetDeviceState;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    iget-object v0, p0, Landroid/support/place/connector/DeviceConnector;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/connector/DeviceConnector;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "getDeviceState"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    new-instance v4, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;

    const/16 v5, 0x10

    invoke-direct {v4, p0, v5, p1}, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;-><init>(Landroid/support/place/connector/DeviceConnector;ILjava/lang/Object;)V

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public getDeviceVersion(Landroid/support/place/connector/DeviceConnector$OnGetDeviceVersion;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    iget-object v0, p0, Landroid/support/place/connector/DeviceConnector;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/connector/DeviceConnector;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "getDeviceVersion"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    new-instance v4, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;

    const/16 v5, 0x8

    invoke-direct {v4, p0, v5, p1}, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;-><init>(Landroid/support/place/connector/DeviceConnector;ILjava/lang/Object;)V

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public getEndpointInfo()Landroid/support/place/rpc/EndpointInfo;
    .locals 1

    iget-object v0, p0, Landroid/support/place/connector/DeviceConnector;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    return-object v0
.end method

.method public getLegalInfo(Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    iget-object v0, p0, Landroid/support/place/connector/DeviceConnector;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/connector/DeviceConnector;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "getLegalInfo"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    const/4 v4, 0x0

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public getManufacturerName(Landroid/support/place/connector/DeviceConnector$OnGetManufacturerName;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    iget-object v0, p0, Landroid/support/place/connector/DeviceConnector;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/connector/DeviceConnector;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "getManufacturerName"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    new-instance v4, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;

    const/16 v5, 0x12

    invoke-direct {v4, p0, v5, p1}, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;-><init>(Landroid/support/place/connector/DeviceConnector;ILjava/lang/Object;)V

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public getMaster(Ljava/lang/String;Landroid/support/place/connector/DeviceConnector$OnGetMaster;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v0, "placeId"

    invoke-virtual {v3, v0, p1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/place/connector/DeviceConnector;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/connector/DeviceConnector;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "getMaster"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    new-instance v4, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;

    const/4 v5, 0x3

    invoke-direct {v4, p0, v5, p2}, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;-><init>(Landroid/support/place/connector/DeviceConnector;ILjava/lang/Object;)V

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public getModelName(Landroid/support/place/connector/DeviceConnector$OnGetModelName;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    iget-object v0, p0, Landroid/support/place/connector/DeviceConnector;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/connector/DeviceConnector;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "getModelName"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    new-instance v4, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;

    const/16 v5, 0x13

    invoke-direct {v4, p0, v5, p1}, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;-><init>(Landroid/support/place/connector/DeviceConnector;ILjava/lang/Object;)V

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public getUpdateWindow(Landroid/support/place/connector/DeviceConnector$OnGetUpdateWindow;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    iget-object v0, p0, Landroid/support/place/connector/DeviceConnector;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/connector/DeviceConnector;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "getUpdateWindow"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    new-instance v4, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;

    const/16 v5, 0xf

    invoke-direct {v4, p0, v5, p1}, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;-><init>(Landroid/support/place/connector/DeviceConnector;ILjava/lang/Object;)V

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public goodbye(Landroid/support/place/rpc/EndpointInfo;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v0, "hub"

    invoke-virtual {v3, v0, p1}, Landroid/support/place/rpc/RpcData;->putFlattenable(Ljava/lang/String;Landroid/support/place/rpc/Flattenable;)V

    iget-object v0, p0, Landroid/support/place/connector/DeviceConnector;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/connector/DeviceConnector;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "goodbye"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    const/4 v4, 0x0

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public helloFromHub(Landroid/support/place/rpc/EndpointInfo;Landroid/support/place/connector/DeviceConnector$OnHelloFromHub;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v0, "hub"

    invoke-virtual {v3, v0, p1}, Landroid/support/place/rpc/RpcData;->putFlattenable(Ljava/lang/String;Landroid/support/place/rpc/Flattenable;)V

    iget-object v0, p0, Landroid/support/place/connector/DeviceConnector;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/connector/DeviceConnector;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "helloFromHub"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    new-instance v4, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5, p2}, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;-><init>(Landroid/support/place/connector/DeviceConnector;ILjava/lang/Object;)V

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public ping(Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    iget-object v0, p0, Landroid/support/place/connector/DeviceConnector;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/connector/DeviceConnector;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "ping"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    const/4 v4, 0x0

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public setAdbState(ZLandroid/support/place/connector/DeviceConnector$OnSetAdbState;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v0, "adbEnabled"

    invoke-virtual {v3, v0, p1}, Landroid/support/place/rpc/RpcData;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Landroid/support/place/connector/DeviceConnector;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/connector/DeviceConnector;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "setAdbState"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    new-instance v4, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;

    const/16 v5, 0xc

    invoke-direct {v4, p0, v5, p2}, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;-><init>(Landroid/support/place/connector/DeviceConnector;ILjava/lang/Object;)V

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public setDeviceName(Ljava/lang/String;Landroid/support/place/connector/DeviceConnector$OnSetDeviceName;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v0, "name"

    invoke-virtual {v3, v0, p1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/place/connector/DeviceConnector;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/connector/DeviceConnector;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "setDeviceName"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    new-instance v4, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;

    const/4 v5, 0x5

    invoke-direct {v4, p0, v5, p2}, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;-><init>(Landroid/support/place/connector/DeviceConnector;ILjava/lang/Object;)V

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public setUpdateWindow(Ljava/lang/String;Landroid/support/place/connector/DeviceConnector$OnSetUpdateWindow;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v0, "window"

    invoke-virtual {v3, v0, p1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/place/connector/DeviceConnector;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/connector/DeviceConnector;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "setUpdateWindow"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    new-instance v4, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;

    const/16 v5, 0xe

    invoke-direct {v4, p0, v5, p2}, Landroid/support/place/connector/DeviceConnector$_ResultDispatcher;-><init>(Landroid/support/place/connector/DeviceConnector;ILjava/lang/Object;)V

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public startListening(Landroid/support/place/connector/DeviceConnector$Listener;)V
    .locals 2

    invoke-virtual {p0}, Landroid/support/place/connector/DeviceConnector;->stopListening()V

    new-instance v0, Landroid/support/place/connector/DeviceConnector$Presenter;

    iget-object v1, p0, Landroid/support/place/connector/DeviceConnector;->_broker:Landroid/support/place/connector/Broker;

    invoke-direct {v0, p0, v1, p1}, Landroid/support/place/connector/DeviceConnector$Presenter;-><init>(Landroid/support/place/connector/DeviceConnector;Landroid/support/place/connector/Broker;Landroid/support/place/connector/DeviceConnector$Listener;)V

    iput-object v0, p0, Landroid/support/place/connector/DeviceConnector;->_presenter:Landroid/support/place/connector/DeviceConnector$Presenter;

    iget-object v0, p0, Landroid/support/place/connector/DeviceConnector;->_presenter:Landroid/support/place/connector/DeviceConnector$Presenter;

    iget-object v1, p0, Landroid/support/place/connector/DeviceConnector;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    invoke-virtual {v0, v1}, Landroid/support/place/connector/DeviceConnector$Presenter;->startListening(Landroid/support/place/rpc/EndpointInfo;)V

    return-void
.end method

.method public stopListening()V
    .locals 1

    iget-object v0, p0, Landroid/support/place/connector/DeviceConnector;->_presenter:Landroid/support/place/connector/DeviceConnector$Presenter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/connector/DeviceConnector;->_presenter:Landroid/support/place/connector/DeviceConnector$Presenter;

    invoke-virtual {v0}, Landroid/support/place/connector/DeviceConnector$Presenter;->stopListening()V

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/place/connector/DeviceConnector;->_presenter:Landroid/support/place/connector/DeviceConnector$Presenter;

    :cond_0
    return-void
.end method
