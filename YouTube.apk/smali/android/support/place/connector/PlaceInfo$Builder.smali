.class public Landroid/support/place/connector/PlaceInfo$Builder;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private extras:Landroid/support/place/rpc/RpcData;

.field private master:Landroid/support/place/rpc/EndpointInfo;

.field private masterSessionId:Ljava/lang/String;

.field private placeId:Ljava/lang/String;

.field private placeName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Landroid/support/place/connector/PlaceInfo;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    # getter for: Landroid/support/place/connector/PlaceInfo;->mPlaceId:Ljava/lang/String;
    invoke-static {p1}, Landroid/support/place/connector/PlaceInfo;->access$500(Landroid/support/place/connector/PlaceInfo;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/place/connector/PlaceInfo$Builder;->placeId:Ljava/lang/String;

    # getter for: Landroid/support/place/connector/PlaceInfo;->mPlaceName:Ljava/lang/String;
    invoke-static {p1}, Landroid/support/place/connector/PlaceInfo;->access$600(Landroid/support/place/connector/PlaceInfo;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/place/connector/PlaceInfo$Builder;->placeName:Ljava/lang/String;

    invoke-virtual {p0, p1}, Landroid/support/place/connector/PlaceInfo$Builder;->copyMaster(Landroid/support/place/connector/PlaceInfo;)Landroid/support/place/connector/PlaceInfo$Builder;

    # getter for: Landroid/support/place/connector/PlaceInfo;->mPlaceExtras:Landroid/support/place/rpc/RpcData;
    invoke-static {p1}, Landroid/support/place/connector/PlaceInfo;->access$700(Landroid/support/place/connector/PlaceInfo;)Landroid/support/place/rpc/RpcData;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/support/place/rpc/RpcData;

    # getter for: Landroid/support/place/connector/PlaceInfo;->mPlaceExtras:Landroid/support/place/rpc/RpcData;
    invoke-static {p1}, Landroid/support/place/connector/PlaceInfo;->access$700(Landroid/support/place/connector/PlaceInfo;)Landroid/support/place/rpc/RpcData;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    iput-object v0, p0, Landroid/support/place/connector/PlaceInfo$Builder;->extras:Landroid/support/place/rpc/RpcData;

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/place/connector/PlaceInfo$Builder;->extras:Landroid/support/place/rpc/RpcData;

    goto :goto_0
.end method

.method static synthetic access$000(Landroid/support/place/connector/PlaceInfo$Builder;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/support/place/connector/PlaceInfo$Builder;->placeId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Landroid/support/place/connector/PlaceInfo$Builder;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/support/place/connector/PlaceInfo$Builder;->placeName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Landroid/support/place/connector/PlaceInfo$Builder;)Landroid/support/place/rpc/EndpointInfo;
    .locals 1

    iget-object v0, p0, Landroid/support/place/connector/PlaceInfo$Builder;->master:Landroid/support/place/rpc/EndpointInfo;

    return-object v0
.end method

.method static synthetic access$300(Landroid/support/place/connector/PlaceInfo$Builder;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/support/place/connector/PlaceInfo$Builder;->masterSessionId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Landroid/support/place/connector/PlaceInfo$Builder;)Landroid/support/place/rpc/RpcData;
    .locals 1

    iget-object v0, p0, Landroid/support/place/connector/PlaceInfo$Builder;->extras:Landroid/support/place/rpc/RpcData;

    return-object v0
.end method


# virtual methods
.method public build()Landroid/support/place/connector/PlaceInfo;
    .locals 2

    new-instance v0, Landroid/support/place/connector/PlaceInfo;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Landroid/support/place/connector/PlaceInfo;-><init>(Landroid/support/place/connector/PlaceInfo$Builder;Landroid/support/place/connector/PlaceInfo$1;)V

    return-object v0
.end method

.method public copyMaster(Landroid/support/place/connector/PlaceInfo;)Landroid/support/place/connector/PlaceInfo$Builder;
    .locals 1

    # getter for: Landroid/support/place/connector/PlaceInfo;->mMaster:Landroid/support/place/rpc/EndpointInfo;
    invoke-static {p1}, Landroid/support/place/connector/PlaceInfo;->access$900(Landroid/support/place/connector/PlaceInfo;)Landroid/support/place/rpc/EndpointInfo;

    move-result-object v0

    iput-object v0, p0, Landroid/support/place/connector/PlaceInfo$Builder;->master:Landroid/support/place/rpc/EndpointInfo;

    # getter for: Landroid/support/place/connector/PlaceInfo;->mMasterSessionId:Ljava/lang/String;
    invoke-static {p1}, Landroid/support/place/connector/PlaceInfo;->access$1000(Landroid/support/place/connector/PlaceInfo;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/place/connector/PlaceInfo$Builder;->masterSessionId:Ljava/lang/String;

    return-object p0
.end method

.method public copyPlace(Landroid/support/place/connector/PlaceInfo;)Landroid/support/place/connector/PlaceInfo$Builder;
    .locals 1

    # getter for: Landroid/support/place/connector/PlaceInfo;->mPlaceId:Ljava/lang/String;
    invoke-static {p1}, Landroid/support/place/connector/PlaceInfo;->access$500(Landroid/support/place/connector/PlaceInfo;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/place/connector/PlaceInfo$Builder;->placeId:Ljava/lang/String;

    # getter for: Landroid/support/place/connector/PlaceInfo;->mPlaceName:Ljava/lang/String;
    invoke-static {p1}, Landroid/support/place/connector/PlaceInfo;->access$600(Landroid/support/place/connector/PlaceInfo;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/place/connector/PlaceInfo$Builder;->placeName:Ljava/lang/String;

    return-object p0
.end method

.method public setCertificate(Ljava/lang/String;)Landroid/support/place/connector/PlaceInfo$Builder;
    .locals 2

    iget-object v0, p0, Landroid/support/place/connector/PlaceInfo$Builder;->extras:Landroid/support/place/rpc/RpcData;

    if-nez v0, :cond_0

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    iput-object v0, p0, Landroid/support/place/connector/PlaceInfo$Builder;->extras:Landroid/support/place/rpc/RpcData;

    :cond_0
    iget-object v0, p0, Landroid/support/place/connector/PlaceInfo$Builder;->extras:Landroid/support/place/rpc/RpcData;

    const-string v1, "master_cert"

    invoke-virtual {v0, v1, p1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method

.method public setExtras(Landroid/support/place/rpc/RpcData;)Landroid/support/place/connector/PlaceInfo$Builder;
    .locals 0

    iput-object p1, p0, Landroid/support/place/connector/PlaceInfo$Builder;->extras:Landroid/support/place/rpc/RpcData;

    return-object p0
.end method

.method public setGuestMode(Z)Landroid/support/place/connector/PlaceInfo$Builder;
    .locals 2

    iget-object v0, p0, Landroid/support/place/connector/PlaceInfo$Builder;->extras:Landroid/support/place/rpc/RpcData;

    if-nez v0, :cond_0

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    iput-object v0, p0, Landroid/support/place/connector/PlaceInfo$Builder;->extras:Landroid/support/place/rpc/RpcData;

    :cond_0
    iget-object v0, p0, Landroid/support/place/connector/PlaceInfo$Builder;->extras:Landroid/support/place/rpc/RpcData;

    const-string v1, "guest_mode"

    invoke-virtual {v0, v1, p1}, Landroid/support/place/rpc/RpcData;->putBoolean(Ljava/lang/String;Z)V

    return-object p0
.end method

.method public setMaster(Ljava/lang/String;ILjava/lang/String;)Landroid/support/place/connector/PlaceInfo$Builder;
    .locals 2

    new-instance v0, Landroid/support/place/rpc/EndpointInfo;

    const-string v1, "_broker"

    invoke-direct {v0, v1, p1, p2}, Landroid/support/place/rpc/EndpointInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    iput-object v0, p0, Landroid/support/place/connector/PlaceInfo$Builder;->master:Landroid/support/place/rpc/EndpointInfo;

    iput-object p3, p0, Landroid/support/place/connector/PlaceInfo$Builder;->masterSessionId:Ljava/lang/String;

    return-object p0
.end method

.method public setPlace(Ljava/lang/String;Ljava/lang/String;)Landroid/support/place/connector/PlaceInfo$Builder;
    .locals 0

    iput-object p1, p0, Landroid/support/place/connector/PlaceInfo$Builder;->placeId:Ljava/lang/String;

    iput-object p2, p0, Landroid/support/place/connector/PlaceInfo$Builder;->placeName:Ljava/lang/String;

    return-object p0
.end method

.method public setPlaceName(Ljava/lang/String;)Landroid/support/place/connector/PlaceInfo$Builder;
    .locals 0

    iput-object p1, p0, Landroid/support/place/connector/PlaceInfo$Builder;->placeName:Ljava/lang/String;

    return-object p0
.end method
