.class Landroid/support/place/music/MediaPlayerConfigurator$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;


# instance fields
.field final synthetic this$0:Landroid/support/place/music/MediaPlayerConfigurator;


# direct methods
.method constructor <init>(Landroid/support/place/music/MediaPlayerConfigurator;)V
    .locals 0

    iput-object p1, p0, Landroid/support/place/music/MediaPlayerConfigurator$1;->this$0:Landroid/support/place/music/MediaPlayerConfigurator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 4

    const/16 v0, 0x64

    if-ne p2, v0, :cond_1

    iget-object v0, p0, Landroid/support/place/music/MediaPlayerConfigurator$1;->this$0:Landroid/support/place/music/MediaPlayerConfigurator;

    # getter for: Landroid/support/place/music/MediaPlayerConfigurator;->mUnicastTransmitGroupMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Landroid/support/place/music/MediaPlayerConfigurator;->access$000(Landroid/support/place/music/MediaPlayerConfigurator;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    iget-object v0, p0, Landroid/support/place/music/MediaPlayerConfigurator$1;->this$0:Landroid/support/place/music/MediaPlayerConfigurator;

    const/4 v1, 0x0

    # setter for: Landroid/support/place/music/MediaPlayerConfigurator;->mUnicastTransmitGroupMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0, v1}, Landroid/support/place/music/MediaPlayerConfigurator;->access$002(Landroid/support/place/music/MediaPlayerConfigurator;Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer;

    iget-object v0, p0, Landroid/support/place/music/MediaPlayerConfigurator$1;->this$0:Landroid/support/place/music/MediaPlayerConfigurator;

    const/4 v1, -0x1

    # setter for: Landroid/support/place/music/MediaPlayerConfigurator;->mUnicastCncPort:I
    invoke-static {v0, v1}, Landroid/support/place/music/MediaPlayerConfigurator;->access$102(Landroid/support/place/music/MediaPlayerConfigurator;I)I

    iget-object v0, p0, Landroid/support/place/music/MediaPlayerConfigurator$1;->this$0:Landroid/support/place/music/MediaPlayerConfigurator;

    # getter for: Landroid/support/place/music/MediaPlayerConfigurator;->mErrorHandler:Landroid/support/place/music/MediaPlayerConfigurator$ErrorHandler;
    invoke-static {v0}, Landroid/support/place/music/MediaPlayerConfigurator;->access$200(Landroid/support/place/music/MediaPlayerConfigurator;)Landroid/support/place/music/MediaPlayerConfigurator$ErrorHandler;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/music/MediaPlayerConfigurator$1;->this$0:Landroid/support/place/music/MediaPlayerConfigurator;

    # getter for: Landroid/support/place/music/MediaPlayerConfigurator;->mErrorHandler:Landroid/support/place/music/MediaPlayerConfigurator$ErrorHandler;
    invoke-static {v0}, Landroid/support/place/music/MediaPlayerConfigurator;->access$200(Landroid/support/place/music/MediaPlayerConfigurator;)Landroid/support/place/music/MediaPlayerConfigurator$ErrorHandler;

    move-result-object v0

    invoke-interface {v0}, Landroid/support/place/music/MediaPlayerConfigurator$ErrorHandler;->onConfigurationLost()V

    :cond_0
    iget-object v0, p0, Landroid/support/place/music/MediaPlayerConfigurator$1;->this$0:Landroid/support/place/music/MediaPlayerConfigurator;

    # getter for: Landroid/support/place/music/MediaPlayerConfigurator;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Landroid/support/place/music/MediaPlayerConfigurator;->access$400(Landroid/support/place/music/MediaPlayerConfigurator;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Landroid/support/place/music/MediaPlayerConfigurator$1;->this$0:Landroid/support/place/music/MediaPlayerConfigurator;

    # getter for: Landroid/support/place/music/MediaPlayerConfigurator;->mRestoreUnicastGroupRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Landroid/support/place/music/MediaPlayerConfigurator;->access$300(Landroid/support/place/music/MediaPlayerConfigurator;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
