.class public Landroid/support/place/music/MediaPlayerConfigurator;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final INADDR_ANY:Ljava/lang/String; = "0.0.0.0"

.field private static final INVOKE_GET_CNC_PORT:I = 0xb33977

.field private static final MODE_MULTICAST:I = 0x2

.field private static final MODE_UNCONFIGURED:I = 0x0

.field private static final MODE_UNICAST:I = 0x1

.field private static final PLACEHOLDER_URL:Ljava/lang/String; = "http://127.0.0.1/foo.mp3"

.field private static RE_MULTICAST:Ljava/util/regex/Pattern; = null

.field private static RE_UNICAST:Ljava/util/regex/Pattern; = null

.field public static final TAG:Ljava/lang/String; = "MediaPlayerConfigurator"

.field private static TGS_REQUEST_FIELD_MODE:Ljava/lang/String;

.field private static TGS_REQUEST_FIELD_MULTICAST_ADDR:Ljava/lang/String;

.field private static TGS_REQUEST_FIELD_MULTICAST_PORT:Ljava/lang/String;

.field private static TGS_REQUEST_VALUE_MODE_MULTICAST:Ljava/lang/String;

.field private static TGS_REQUEST_VALUE_MODE_UNICAST:Ljava/lang/String;


# instance fields
.field private mErrorHandler:Landroid/support/place/music/MediaPlayerConfigurator$ErrorHandler;

.field private mHandler:Landroid/os/Handler;

.field private mMode:I

.field private mMulticastRetransmitHostname:Ljava/lang/String;

.field private mMulticastRetransmitPort:I

.field private mRestoreUnicastGroupRunnable:Ljava/lang/Runnable;

.field private mUnicastCncPort:I

.field private mUnicastGroupErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

.field private mUnicastTransmitGroupMediaPlayer:Landroid/media/MediaPlayer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "mode"

    sput-object v0, Landroid/support/place/music/MediaPlayerConfigurator;->TGS_REQUEST_FIELD_MODE:Ljava/lang/String;

    const-string v0, "multicast"

    sput-object v0, Landroid/support/place/music/MediaPlayerConfigurator;->TGS_REQUEST_VALUE_MODE_MULTICAST:Ljava/lang/String;

    const-string v0, "unicast"

    sput-object v0, Landroid/support/place/music/MediaPlayerConfigurator;->TGS_REQUEST_VALUE_MODE_UNICAST:Ljava/lang/String;

    const-string v0, "multicastAddr"

    sput-object v0, Landroid/support/place/music/MediaPlayerConfigurator;->TGS_REQUEST_FIELD_MULTICAST_ADDR:Ljava/lang/String;

    const-string v0, "multicastPort"

    sput-object v0, Landroid/support/place/music/MediaPlayerConfigurator;->TGS_REQUEST_FIELD_MULTICAST_PORT:Ljava/lang/String;

    const-string v0, "mode=1;port=(\\d+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Landroid/support/place/music/MediaPlayerConfigurator;->RE_UNICAST:Ljava/util/regex/Pattern;

    const-string v0, "mode=2;host=([\\d\\.]+);port=(\\d+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Landroid/support/place/music/MediaPlayerConfigurator;->RE_MULTICAST:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Landroid/support/place/music/MediaPlayerConfigurator;->mHandler:Landroid/os/Handler;

    new-instance v0, Landroid/support/place/music/MediaPlayerConfigurator$1;

    invoke-direct {v0, p0}, Landroid/support/place/music/MediaPlayerConfigurator$1;-><init>(Landroid/support/place/music/MediaPlayerConfigurator;)V

    iput-object v0, p0, Landroid/support/place/music/MediaPlayerConfigurator;->mUnicastGroupErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    new-instance v0, Landroid/support/place/music/MediaPlayerConfigurator$2;

    invoke-direct {v0, p0}, Landroid/support/place/music/MediaPlayerConfigurator$2;-><init>(Landroid/support/place/music/MediaPlayerConfigurator;)V

    iput-object v0, p0, Landroid/support/place/music/MediaPlayerConfigurator;->mRestoreUnicastGroupRunnable:Ljava/lang/Runnable;

    const/4 v0, 0x0

    iput v0, p0, Landroid/support/place/music/MediaPlayerConfigurator;->mMode:I

    return-void
.end method

.method static synthetic access$000(Landroid/support/place/music/MediaPlayerConfigurator;)Landroid/media/MediaPlayer;
    .locals 1

    iget-object v0, p0, Landroid/support/place/music/MediaPlayerConfigurator;->mUnicastTransmitGroupMediaPlayer:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method static synthetic access$002(Landroid/support/place/music/MediaPlayerConfigurator;Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer;
    .locals 0

    iput-object p1, p0, Landroid/support/place/music/MediaPlayerConfigurator;->mUnicastTransmitGroupMediaPlayer:Landroid/media/MediaPlayer;

    return-object p1
.end method

.method static synthetic access$102(Landroid/support/place/music/MediaPlayerConfigurator;I)I
    .locals 0

    iput p1, p0, Landroid/support/place/music/MediaPlayerConfigurator;->mUnicastCncPort:I

    return p1
.end method

.method static synthetic access$200(Landroid/support/place/music/MediaPlayerConfigurator;)Landroid/support/place/music/MediaPlayerConfigurator$ErrorHandler;
    .locals 1

    iget-object v0, p0, Landroid/support/place/music/MediaPlayerConfigurator;->mErrorHandler:Landroid/support/place/music/MediaPlayerConfigurator$ErrorHandler;

    return-object v0
.end method

.method static synthetic access$300(Landroid/support/place/music/MediaPlayerConfigurator;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Landroid/support/place/music/MediaPlayerConfigurator;->mRestoreUnicastGroupRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$400(Landroid/support/place/music/MediaPlayerConfigurator;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Landroid/support/place/music/MediaPlayerConfigurator;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500(Landroid/support/place/music/MediaPlayerConfigurator;)V
    .locals 0

    invoke-direct {p0}, Landroid/support/place/music/MediaPlayerConfigurator;->restoreUnicastGroup()V

    return-void
.end method

.method public static configureMediaPlayer(Landroid/media/MediaPlayer;Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x1

    sget-object v0, Landroid/support/place/music/MediaPlayerConfigurator;->RE_UNICAST:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    new-instance v0, Ljava/net/InetSocketAddress;

    const-string v2, "0.0.0.0"

    invoke-direct {v0, v2, v1}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    :goto_0
    invoke-static {p0, v0}, Landroid/support/place/music/MediaPlayerConfigurator;->setRetransmitEndpoint(Landroid/media/MediaPlayer;Ljava/net/InetSocketAddress;)V

    return-void

    :cond_0
    sget-object v0, Landroid/support/place/music/MediaPlayerConfigurator;->RE_MULTICAST:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    new-instance v0, Ljava/net/InetSocketAddress;

    invoke-direct {v0, v1, v2}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized config string: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static getConfigureArgsForMulticast(Ljava/lang/String;I)Landroid/support/place/rpc/RpcData;
    .locals 3

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    sget-object v1, Landroid/support/place/music/MediaPlayerConfigurator;->TGS_REQUEST_FIELD_MODE:Ljava/lang/String;

    sget-object v2, Landroid/support/place/music/MediaPlayerConfigurator;->TGS_REQUEST_VALUE_MODE_MULTICAST:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Landroid/support/place/music/MediaPlayerConfigurator;->TGS_REQUEST_FIELD_MULTICAST_ADDR:Ljava/lang/String;

    invoke-virtual {v0, v1, p0}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Landroid/support/place/music/MediaPlayerConfigurator;->TGS_REQUEST_FIELD_MULTICAST_PORT:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroid/support/place/rpc/RpcData;->putInteger(Ljava/lang/String;I)V

    return-object v0
.end method

.method public static getConfigureArgsForUnicast()Landroid/support/place/rpc/RpcData;
    .locals 3

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    sget-object v1, Landroid/support/place/music/MediaPlayerConfigurator;->TGS_REQUEST_FIELD_MODE:Ljava/lang/String;

    sget-object v2, Landroid/support/place/music/MediaPlayerConfigurator;->TGS_REQUEST_VALUE_MODE_UNICAST:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private static invokeGetCncPort(Landroid/media/MediaPlayer;)I
    .locals 7

    const/4 v2, 0x0

    :try_start_0
    const-class v0, Landroid/media/MediaPlayer;

    const-string v1, "invoke"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Landroid/os/Parcel;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-class v5, Landroid/os/Parcel;

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    const-class v0, Landroid/media/MediaPlayer;

    const-string v3, "newRequest"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    const/4 v3, 0x0

    :try_start_1
    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, p0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcel;
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    const v3, 0xb33977

    :try_start_2
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;
    :try_end_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    move-result-object v2

    const/4 v3, 0x2

    :try_start_3
    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v4, 0x1

    aput-object v2, v3, v4

    invoke-virtual {v1, p0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eqz v1, :cond_2

    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "invokeGetCncPort failed: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    :catch_0
    move-exception v1

    move-object v6, v1

    move-object v1, v2

    move-object v2, v0

    move-object v0, v6

    :goto_0
    :try_start_4
    new-instance v3, Ljava/lang/RuntimeException;

    invoke-direct {v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catchall_0
    move-exception v0

    move-object v6, v1

    move-object v1, v2

    move-object v2, v6

    :goto_1
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    :cond_0
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    :cond_1
    throw v0

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_2
    :try_start_5
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I
    :try_end_5
    .catch Ljava/lang/IllegalAccessException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    move-result v1

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    :cond_3
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    :cond_4
    return v1

    :catch_2
    move-exception v0

    move-object v1, v2

    :goto_2
    :try_start_6
    new-instance v3, Ljava/lang/RuntimeException;

    invoke-direct {v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :catchall_1
    move-exception v0

    goto :goto_1

    :catchall_2
    move-exception v0

    move-object v1, v2

    goto :goto_1

    :catchall_3
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_1

    :catch_3
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_2

    :catch_4
    move-exception v0

    move-object v1, v2

    goto :goto_0

    :catch_5
    move-exception v1

    move-object v6, v1

    move-object v1, v2

    move-object v2, v0

    move-object v0, v6

    goto :goto_0
.end method

.method private restoreUnicastGroup()V
    .locals 4

    :try_start_0
    invoke-direct {p0}, Landroid/support/place/music/MediaPlayerConfigurator;->setupForUnicast()Landroid/support/place/music/TgsUnicastConfig;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    iget-object v1, p0, Landroid/support/place/music/MediaPlayerConfigurator;->mErrorHandler:Landroid/support/place/music/MediaPlayerConfigurator$ErrorHandler;

    if-eqz v1, :cond_0

    new-instance v1, Landroid/support/place/rpc/RpcData;

    invoke-direct {v1}, Landroid/support/place/rpc/RpcData;-><init>()V

    invoke-virtual {v0, v1}, Landroid/support/place/music/TgsUnicastConfig;->writeToRpcData(Landroid/support/place/rpc/RpcData;)V

    iget-object v0, p0, Landroid/support/place/music/MediaPlayerConfigurator;->mErrorHandler:Landroid/support/place/music/MediaPlayerConfigurator$ErrorHandler;

    invoke-interface {v0, v1}, Landroid/support/place/music/MediaPlayerConfigurator$ErrorHandler;->onConfigurationRestored(Landroid/support/place/rpc/RpcData;)V

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "MediaPlayerConfigurator"

    const-string v1, "restoreUnicastGroup failed; retrying"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Landroid/support/place/music/MediaPlayerConfigurator;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Landroid/support/place/music/MediaPlayerConfigurator;->mRestoreUnicastGroupRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method private static setRetransmitEndpoint(Landroid/media/MediaPlayer;Ljava/net/InetSocketAddress;)V
    .locals 5

    :try_start_0
    const-class v0, Landroid/media/MediaPlayer;

    const-string v1, "setRetransmitEndpoint"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Ljava/net/InetSocketAddress;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    const/4 v1, 0x1

    :try_start_1
    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_0

    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "setRetransmitEndpoint failed, err="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_2

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_2
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_0
    return-void
.end method

.method private setupForMulticast(Ljava/lang/String;I)V
    .locals 1

    iput-object p1, p0, Landroid/support/place/music/MediaPlayerConfigurator;->mMulticastRetransmitHostname:Ljava/lang/String;

    iput p2, p0, Landroid/support/place/music/MediaPlayerConfigurator;->mMulticastRetransmitPort:I

    const/4 v0, 0x2

    iput v0, p0, Landroid/support/place/music/MediaPlayerConfigurator;->mMode:I

    return-void
.end method

.method private setupForUnicast()Landroid/support/place/music/TgsUnicastConfig;
    .locals 4

    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Landroid/support/place/music/MediaPlayerConfigurator;->mUnicastTransmitGroupMediaPlayer:Landroid/media/MediaPlayer;

    :try_start_0
    iget-object v0, p0, Landroid/support/place/music/MediaPlayerConfigurator;->mUnicastTransmitGroupMediaPlayer:Landroid/media/MediaPlayer;

    new-instance v1, Ljava/net/InetSocketAddress;

    const-string v2, "0.0.0.0"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    invoke-static {v0, v1}, Landroid/support/place/music/MediaPlayerConfigurator;->setRetransmitEndpoint(Landroid/media/MediaPlayer;Ljava/net/InetSocketAddress;)V

    iget-object v0, p0, Landroid/support/place/music/MediaPlayerConfigurator;->mUnicastTransmitGroupMediaPlayer:Landroid/media/MediaPlayer;

    const-string v1, "http://127.0.0.1/foo.mp3"

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/place/music/MediaPlayerConfigurator;->mUnicastTransmitGroupMediaPlayer:Landroid/media/MediaPlayer;

    invoke-static {v0}, Landroid/support/place/music/MediaPlayerConfigurator;->invokeGetCncPort(Landroid/media/MediaPlayer;)I

    move-result v0

    iput v0, p0, Landroid/support/place/music/MediaPlayerConfigurator;->mUnicastCncPort:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Landroid/support/place/music/MediaPlayerConfigurator;->mUnicastTransmitGroupMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v1, p0, Landroid/support/place/music/MediaPlayerConfigurator;->mUnicastGroupErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    const/4 v0, 0x1

    iput v0, p0, Landroid/support/place/music/MediaPlayerConfigurator;->mMode:I

    new-instance v0, Landroid/support/place/music/TgsUnicastConfig;

    invoke-static {}, Landroid/support/place/utils/IPUtils;->getLocalIpAddress()Ljava/net/InetAddress;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Landroid/support/place/music/MediaPlayerConfigurator;->mUnicastCncPort:I

    invoke-direct {v0, v1, v2}, Landroid/support/place/music/TgsUnicastConfig;-><init>(Ljava/lang/String;I)V

    return-object v0

    :catch_0
    move-exception v0

    iget-object v1, p0, Landroid/support/place/music/MediaPlayerConfigurator;->mUnicastTransmitGroupMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->release()V

    const/4 v1, 0x0

    iput-object v1, p0, Landroid/support/place/music/MediaPlayerConfigurator;->mUnicastTransmitGroupMediaPlayer:Landroid/media/MediaPlayer;

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public configure(Landroid/support/place/rpc/RpcData;)Landroid/support/place/rpc/RpcData;
    .locals 4

    iget v0, p0, Landroid/support/place/music/MediaPlayerConfigurator;->mMode:I

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Must reset MediaPlayerConfigurator"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    sget-object v0, Landroid/support/place/music/MediaPlayerConfigurator;->TGS_REQUEST_FIELD_MODE:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/support/place/rpc/RpcData;

    invoke-direct {v1}, Landroid/support/place/rpc/RpcData;-><init>()V

    sget-object v2, Landroid/support/place/music/MediaPlayerConfigurator;->TGS_REQUEST_VALUE_MODE_UNICAST:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-direct {p0}, Landroid/support/place/music/MediaPlayerConfigurator;->setupForUnicast()Landroid/support/place/music/TgsUnicastConfig;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/place/music/TgsUnicastConfig;->writeToRpcData(Landroid/support/place/rpc/RpcData;)V

    :goto_0
    return-object v1

    :cond_1
    sget-object v2, Landroid/support/place/music/MediaPlayerConfigurator;->TGS_REQUEST_VALUE_MODE_MULTICAST:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v0, Landroid/support/place/music/MediaPlayerConfigurator;->TGS_REQUEST_FIELD_MULTICAST_ADDR:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Landroid/support/place/music/MediaPlayerConfigurator;->TGS_REQUEST_FIELD_MULTICAST_PORT:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/support/place/rpc/RpcData;->getInteger(Ljava/lang/String;)I

    move-result v2

    invoke-direct {p0, v0, v2}, Landroid/support/place/music/MediaPlayerConfigurator;->setupForMulticast(Ljava/lang/String;I)V

    goto :goto_0

    :cond_2
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected mode: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public getConfigurationString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Landroid/support/place/music/MediaPlayerConfigurator;->mMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    iget v1, p0, Landroid/support/place/music/MediaPlayerConfigurator;->mMode:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    const-string v1, ";port="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Landroid/support/place/music/MediaPlayerConfigurator;->mUnicastCncPort:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget v1, p0, Landroid/support/place/music/MediaPlayerConfigurator;->mMode:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    const-string v1, ";host="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Landroid/support/place/music/MediaPlayerConfigurator;->mMulticastRetransmitHostname:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ";port="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Landroid/support/place/music/MediaPlayerConfigurator;->mMulticastRetransmitPort:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "MediaPlayerConfigurator not configured"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public reset()V
    .locals 2

    const/4 v0, 0x0

    iput v0, p0, Landroid/support/place/music/MediaPlayerConfigurator;->mMode:I

    const/4 v0, -0x1

    iput v0, p0, Landroid/support/place/music/MediaPlayerConfigurator;->mUnicastCncPort:I

    iget-object v0, p0, Landroid/support/place/music/MediaPlayerConfigurator;->mUnicastTransmitGroupMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/music/MediaPlayerConfigurator;->mUnicastTransmitGroupMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/place/music/MediaPlayerConfigurator;->mUnicastTransmitGroupMediaPlayer:Landroid/media/MediaPlayer;

    :cond_0
    iget-object v0, p0, Landroid/support/place/music/MediaPlayerConfigurator;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Landroid/support/place/music/MediaPlayerConfigurator;->mRestoreUnicastGroupRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method

.method public setErrorHandler(Landroid/support/place/music/MediaPlayerConfigurator$ErrorHandler;)V
    .locals 0

    iput-object p1, p0, Landroid/support/place/music/MediaPlayerConfigurator;->mErrorHandler:Landroid/support/place/music/MediaPlayerConfigurator$ErrorHandler;

    return-void
.end method
