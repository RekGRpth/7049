.class public Landroid/support/place/music/TgsGroup;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/place/rpc/Flattenable;


# static fields
.field private static final FIELD_CNC_ADDR:Ljava/lang/String; = "cncAddr"

.field private static final FIELD_CNC_PORT:Ljava/lang/String; = "cncPort"

.field private static final FIELD_GROUP_ID:Ljava/lang/String; = "groupId"

.field private static final FIELD_GROUP_RXS:Ljava/lang/String; = "groupRxs"

.field private static final FIELD_GROUP_TX:Ljava/lang/String; = "groupTx"

.field private static final FIELD_IS_PLAYING:Ljava/lang/String; = "isPlaying"

.field private static final FIELD_SESSION_ID:Ljava/lang/String; = "sessionId"

.field public static final RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;


# instance fields
.field private mCncAddr:Ljava/lang/String;

.field private mCncPort:I

.field private mGroupId:Ljava/lang/String;

.field private mIsPlaying:Z

.field private mRxs:Ljava/util/ArrayList;

.field private mSessionId:Ljava/lang/String;

.field private mTx:Landroid/support/place/music/TgsTxService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/support/place/music/TgsGroup$1;

    invoke-direct {v0}, Landroid/support/place/music/TgsGroup$1;-><init>()V

    sput-object v0, Landroid/support/place/music/TgsGroup;->RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/support/place/rpc/RpcData;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "groupId"

    invoke-virtual {p1, v0}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/place/music/TgsGroup;->mGroupId:Ljava/lang/String;

    const-string v0, "sessionId"

    invoke-virtual {p1, v0}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/place/music/TgsGroup;->mSessionId:Ljava/lang/String;

    new-instance v0, Landroid/support/place/music/TgsTxService;

    const-string v1, "groupTx"

    invoke-virtual {p1, v1}, Landroid/support/place/rpc/RpcData;->getRpcData(Ljava/lang/String;)Landroid/support/place/rpc/RpcData;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/place/music/TgsTxService;-><init>(Landroid/support/place/rpc/RpcData;)V

    iput-object v0, p0, Landroid/support/place/music/TgsGroup;->mTx:Landroid/support/place/music/TgsTxService;

    const-string v0, "cncAddr"

    invoke-virtual {p1, v0}, Landroid/support/place/rpc/RpcData;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "cncAddr"

    invoke-virtual {p1, v0}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/place/music/TgsGroup;->mCncAddr:Ljava/lang/String;

    const-string v0, "cncPort"

    invoke-virtual {p1, v0}, Landroid/support/place/rpc/RpcData;->getInteger(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Landroid/support/place/music/TgsGroup;->mCncPort:I

    :goto_0
    const-string v0, "isPlaying"

    invoke-virtual {p1, v0}, Landroid/support/place/rpc/RpcData;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Landroid/support/place/music/TgsGroup;->mIsPlaying:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/place/music/TgsGroup;->mRxs:Ljava/util/ArrayList;

    const-string v0, "groupRxs"

    invoke-virtual {p1, v0}, Landroid/support/place/rpc/RpcData;->getRpcDataList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/place/rpc/RpcData;

    iget-object v2, p0, Landroid/support/place/music/TgsGroup;->mRxs:Ljava/util/ArrayList;

    new-instance v3, Landroid/support/place/music/TgsService;

    invoke-direct {v3, v0}, Landroid/support/place/music/TgsService;-><init>(Landroid/support/place/rpc/RpcData;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_0
    const-string v0, "multicastAddr"

    invoke-virtual {p1, v0}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/place/music/TgsGroup;->mCncAddr:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Landroid/support/place/music/TgsGroup;->mCncPort:I

    goto :goto_0

    :cond_1
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/support/place/music/TgsTxService;Ljava/lang/String;IZ)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/support/place/music/TgsGroup;->mGroupId:Ljava/lang/String;

    iput-object p2, p0, Landroid/support/place/music/TgsGroup;->mSessionId:Ljava/lang/String;

    iput-object p3, p0, Landroid/support/place/music/TgsGroup;->mTx:Landroid/support/place/music/TgsTxService;

    iput-object p4, p0, Landroid/support/place/music/TgsGroup;->mCncAddr:Ljava/lang/String;

    iput p5, p0, Landroid/support/place/music/TgsGroup;->mCncPort:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/place/music/TgsGroup;->mRxs:Ljava/util/ArrayList;

    iput-boolean p6, p0, Landroid/support/place/music/TgsGroup;->mIsPlaying:Z

    return-void
.end method


# virtual methods
.method public getCncAddr()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/support/place/music/TgsGroup;->mCncAddr:Ljava/lang/String;

    return-object v0
.end method

.method public getCncPort()I
    .locals 1

    iget v0, p0, Landroid/support/place/music/TgsGroup;->mCncPort:I

    return v0
.end method

.method public getGroupId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/support/place/music/TgsGroup;->mGroupId:Ljava/lang/String;

    return-object v0
.end method

.method public getIsPlaying()Z
    .locals 1

    iget-boolean v0, p0, Landroid/support/place/music/TgsGroup;->mIsPlaying:Z

    return v0
.end method

.method public getRxs()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Landroid/support/place/music/TgsGroup;->mRxs:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getSessionId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/support/place/music/TgsGroup;->mSessionId:Ljava/lang/String;

    return-object v0
.end method

.method public getTx()Landroid/support/place/music/TgsTxService;
    .locals 1

    iget-object v0, p0, Landroid/support/place/music/TgsGroup;->mTx:Landroid/support/place/music/TgsTxService;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    invoke-virtual {p0, v0}, Landroid/support/place/music/TgsGroup;->writeToRpcData(Landroid/support/place/rpc/RpcData;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "TgsGroup("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/support/place/rpc/RpcData;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToRpcData(Landroid/support/place/rpc/RpcData;)V
    .locals 4

    const-string v0, "groupId"

    iget-object v1, p0, Landroid/support/place/music/TgsGroup;->mGroupId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "sessionId"

    iget-object v1, p0, Landroid/support/place/music/TgsGroup;->mSessionId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "cncAddr"

    iget-object v1, p0, Landroid/support/place/music/TgsGroup;->mCncAddr:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "cncPort"

    iget v1, p0, Landroid/support/place/music/TgsGroup;->mCncPort:I

    invoke-virtual {p1, v0, v1}, Landroid/support/place/rpc/RpcData;->putInteger(Ljava/lang/String;I)V

    const-string v0, "isPlaying"

    iget-boolean v1, p0, Landroid/support/place/music/TgsGroup;->mIsPlaying:Z

    invoke-virtual {p1, v0, v1}, Landroid/support/place/rpc/RpcData;->putBoolean(Ljava/lang/String;Z)V

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    iget-object v1, p0, Landroid/support/place/music/TgsGroup;->mTx:Landroid/support/place/music/TgsTxService;

    invoke-virtual {v1, v0}, Landroid/support/place/music/TgsTxService;->writeToRpcData(Landroid/support/place/rpc/RpcData;)V

    const-string v1, "groupTx"

    invoke-virtual {p1, v1, v0}, Landroid/support/place/rpc/RpcData;->putRpcData(Ljava/lang/String;Landroid/support/place/rpc/RpcData;)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Landroid/support/place/music/TgsGroup;->mRxs:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/place/music/TgsService;

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    invoke-virtual {v0, v3}, Landroid/support/place/music/TgsService;->writeToRpcData(Landroid/support/place/rpc/RpcData;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const-string v0, "groupRxs"

    invoke-virtual {p1, v0, v1}, Landroid/support/place/rpc/RpcData;->putList(Ljava/lang/String;Ljava/util/List;)V

    return-void
.end method
