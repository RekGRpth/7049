.class final Landroid/support/place/music/TgsTransmitter$_ResultDispatcher;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/place/rpc/RpcResultHandler;


# instance fields
.field private callback:Ljava/lang/Object;

.field private methodId:I

.field final synthetic this$0:Landroid/support/place/music/TgsTransmitter;


# direct methods
.method public constructor <init>(Landroid/support/place/music/TgsTransmitter;ILjava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Landroid/support/place/music/TgsTransmitter$_ResultDispatcher;->this$0:Landroid/support/place/music/TgsTransmitter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Landroid/support/place/music/TgsTransmitter$_ResultDispatcher;->methodId:I

    iput-object p3, p0, Landroid/support/place/music/TgsTransmitter$_ResultDispatcher;->callback:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final configureMediaPlayer([B)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v1, "_result"

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcData;->getRpcData(Ljava/lang/String;)Landroid/support/place/rpc/RpcData;

    move-result-object v1

    iget-object v0, p0, Landroid/support/place/music/TgsTransmitter$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/music/TgsTransmitter$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v0, Landroid/support/place/music/TgsTransmitter$OnConfigureMediaPlayer;

    invoke-interface {v0, v1}, Landroid/support/place/music/TgsTransmitter$OnConfigureMediaPlayer;->onConfigureMediaPlayer(Landroid/support/place/rpc/RpcData;)V

    :cond_0
    return-void
.end method

.method public final getApplicationConnector([B)V
    .locals 3

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v1, "_result"

    sget-object v2, Landroid/support/place/connector/ConnectorInfo;->RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;

    invoke-virtual {v0, v1, v2}, Landroid/support/place/rpc/RpcData;->getFlattenable(Ljava/lang/String;Landroid/support/place/rpc/Flattenable$Creator;)Landroid/support/place/rpc/Flattenable;

    move-result-object v0

    check-cast v0, Landroid/support/place/connector/ConnectorInfo;

    iget-object v1, p0, Landroid/support/place/music/TgsTransmitter$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/place/music/TgsTransmitter$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v1, Landroid/support/place/music/TgsTransmitter$OnGetApplicationConnector;

    invoke-interface {v1, v0}, Landroid/support/place/music/TgsTransmitter$OnGetApplicationConnector;->onGetApplicationConnector(Landroid/support/place/connector/ConnectorInfo;)V

    :cond_0
    return-void
.end method

.method public final getGroupId([B)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v1, "_result"

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Landroid/support/place/music/TgsTransmitter$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/music/TgsTransmitter$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v0, Landroid/support/place/music/TgsTransmitter$OnGetGroupId;

    invoke-interface {v0, v1}, Landroid/support/place/music/TgsTransmitter$OnGetGroupId;->onGetGroupId(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final getPlayState([B)V
    .locals 3

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v1, "_result"

    sget-object v2, Landroid/support/place/music/TgsTransmitterPlayState;->RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;

    invoke-virtual {v0, v1, v2}, Landroid/support/place/rpc/RpcData;->getFlattenable(Ljava/lang/String;Landroid/support/place/rpc/Flattenable$Creator;)Landroid/support/place/rpc/Flattenable;

    move-result-object v0

    check-cast v0, Landroid/support/place/music/TgsTransmitterPlayState;

    iget-object v1, p0, Landroid/support/place/music/TgsTransmitter$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/place/music/TgsTransmitter$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v1, Landroid/support/place/music/TgsTransmitter$OnGetPlayState;

    invoke-interface {v1, v0}, Landroid/support/place/music/TgsTransmitter$OnGetPlayState;->onGetPlayState(Landroid/support/place/music/TgsTransmitterPlayState;)V

    :cond_0
    return-void
.end method

.method public final onResult([B)V
    .locals 1

    iget v0, p0, Landroid/support/place/music/TgsTransmitter$_ResultDispatcher;->methodId:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-virtual {p0, p1}, Landroid/support/place/music/TgsTransmitter$_ResultDispatcher;->versionCheck([B)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0, p1}, Landroid/support/place/music/TgsTransmitter$_ResultDispatcher;->reset([B)V

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0, p1}, Landroid/support/place/music/TgsTransmitter$_ResultDispatcher;->getApplicationConnector([B)V

    goto :goto_0

    :pswitch_4
    invoke-virtual {p0, p1}, Landroid/support/place/music/TgsTransmitter$_ResultDispatcher;->configureMediaPlayer([B)V

    goto :goto_0

    :pswitch_5
    invoke-virtual {p0, p1}, Landroid/support/place/music/TgsTransmitter$_ResultDispatcher;->getGroupId([B)V

    goto :goto_0

    :pswitch_6
    invoke-virtual {p0, p1}, Landroid/support/place/music/TgsTransmitter$_ResultDispatcher;->pauseTransmitter([B)V

    goto :goto_0

    :pswitch_7
    invoke-virtual {p0, p1}, Landroid/support/place/music/TgsTransmitter$_ResultDispatcher;->getPlayState([B)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_7
    .end packed-switch
.end method

.method public final pauseTransmitter([B)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v1, "_result"

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcData;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iget-object v0, p0, Landroid/support/place/music/TgsTransmitter$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/music/TgsTransmitter$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v0, Landroid/support/place/music/TgsTransmitter$OnPauseTransmitter;

    invoke-interface {v0, v1}, Landroid/support/place/music/TgsTransmitter$OnPauseTransmitter;->onPauseTransmitter(Z)V

    :cond_0
    return-void
.end method

.method public final reset([B)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v1, "_result"

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcData;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iget-object v0, p0, Landroid/support/place/music/TgsTransmitter$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/music/TgsTransmitter$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v0, Landroid/support/place/music/TgsTransmitter$OnReset;

    invoke-interface {v0, v1}, Landroid/support/place/music/TgsTransmitter$OnReset;->onReset(Z)V

    :cond_0
    return-void
.end method

.method public final versionCheck([B)V
    .locals 3

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v1, "_result"

    sget-object v2, Landroid/support/place/music/TgsVersionCheck;->RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;

    invoke-virtual {v0, v1, v2}, Landroid/support/place/rpc/RpcData;->getFlattenable(Ljava/lang/String;Landroid/support/place/rpc/Flattenable$Creator;)Landroid/support/place/rpc/Flattenable;

    move-result-object v0

    check-cast v0, Landroid/support/place/music/TgsVersionCheck;

    iget-object v1, p0, Landroid/support/place/music/TgsTransmitter$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/place/music/TgsTransmitter$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v1, Landroid/support/place/music/TgsTransmitter$OnVersionCheck;

    invoke-interface {v1, v0}, Landroid/support/place/music/TgsTransmitter$OnVersionCheck;->onVersionCheck(Landroid/support/place/music/TgsVersionCheck;)V

    :cond_0
    return-void
.end method
