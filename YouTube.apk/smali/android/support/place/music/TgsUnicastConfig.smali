.class public Landroid/support/place/music/TgsUnicastConfig;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/place/rpc/Flattenable;


# static fields
.field private static final FIELD_CNC_ADDRESS:Ljava/lang/String; = "cncAddress"

.field private static final FIELD_CNC_PORT:Ljava/lang/String; = "cncPort"

.field public static final RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;


# instance fields
.field private mCncAddress:Ljava/lang/String;

.field private mCncPort:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/support/place/music/TgsUnicastConfig$1;

    invoke-direct {v0}, Landroid/support/place/music/TgsUnicastConfig$1;-><init>()V

    sput-object v0, Landroid/support/place/music/TgsUnicastConfig;->RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/support/place/rpc/RpcData;)V
    .locals 2

    const-string v0, "cncAddress"

    invoke-virtual {p1, v0}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "cncPort"

    invoke-virtual {p1, v1}, Landroid/support/place/rpc/RpcData;->getInteger(Ljava/lang/String;)I

    move-result v1

    invoke-direct {p0, v0, v1}, Landroid/support/place/music/TgsUnicastConfig;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/support/place/music/TgsUnicastConfig;->mCncAddress:Ljava/lang/String;

    iput p2, p0, Landroid/support/place/music/TgsUnicastConfig;->mCncPort:I

    return-void
.end method


# virtual methods
.method public getCncAddress()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/support/place/music/TgsUnicastConfig;->mCncAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getCncPort()I
    .locals 1

    iget v0, p0, Landroid/support/place/music/TgsUnicastConfig;->mCncPort:I

    return v0
.end method

.method public writeToRpcData(Landroid/support/place/rpc/RpcData;)V
    .locals 2

    const-string v0, "cncAddress"

    iget-object v1, p0, Landroid/support/place/music/TgsUnicastConfig;->mCncAddress:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "cncPort"

    iget v1, p0, Landroid/support/place/music/TgsUnicastConfig;->mCncPort:I

    invoke-virtual {p1, v0, v1}, Landroid/support/place/rpc/RpcData;->putInteger(Ljava/lang/String;I)V

    return-void
.end method
