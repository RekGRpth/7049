.class final Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/place/rpc/RpcResultHandler;


# instance fields
.field private callback:Ljava/lang/Object;

.field private methodId:I

.field final synthetic this$0:Landroid/support/place/music/TungstenReceiverService;


# direct methods
.method public constructor <init>(Landroid/support/place/music/TungstenReceiverService;ILjava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;->this$0:Landroid/support/place/music/TungstenReceiverService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;->methodId:I

    iput-object p3, p0, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;->callback:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final getFixedVolumeLevel([B)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v1, "_result"

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcData;->getFloat(Ljava/lang/String;)F

    move-result v1

    iget-object v0, p0, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v0, Landroid/support/place/music/TungstenReceiverService$OnGetFixedVolumeLevel;

    invoke-interface {v0, v1}, Landroid/support/place/music/TungstenReceiverService$OnGetFixedVolumeLevel;->onGetFixedVolumeLevel(F)V

    :cond_0
    return-void
.end method

.method public final getFixedVolumeOut([B)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v1, "_result"

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcData;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iget-object v0, p0, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v0, Landroid/support/place/music/TungstenReceiverService$OnGetFixedVolumeOut;

    invoke-interface {v0, v1}, Landroid/support/place/music/TungstenReceiverService$OnGetFixedVolumeOut;->onGetFixedVolumeOut(Z)V

    :cond_0
    return-void
.end method

.method public final getMasterMute([B)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v1, "_result"

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcData;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iget-object v0, p0, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v0, Landroid/support/place/music/TungstenReceiverService$OnGetMasterMute;

    invoke-interface {v0, v1}, Landroid/support/place/music/TungstenReceiverService$OnGetMasterMute;->onGetMasterMute(Z)V

    :cond_0
    return-void
.end method

.method public final getMasterVolume([B)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v1, "_result"

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcData;->getFloat(Ljava/lang/String;)F

    move-result v1

    iget-object v0, p0, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v0, Landroid/support/place/music/TungstenReceiverService$OnGetMasterVolume;

    invoke-interface {v0, v1}, Landroid/support/place/music/TungstenReceiverService$OnGetMasterVolume;->onGetMasterVolume(F)V

    :cond_0
    return-void
.end method

.method public final getSyncDelay([B)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v1, "_result"

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcData;->getFloat(Ljava/lang/String;)F

    move-result v1

    iget-object v0, p0, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v0, Landroid/support/place/music/TungstenReceiverService$OnGetSyncDelay;

    invoke-interface {v0, v1}, Landroid/support/place/music/TungstenReceiverService$OnGetSyncDelay;->onGetSyncDelay(F)V

    :cond_0
    return-void
.end method

.method public final isOutputEnabled([B)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v1, "_result"

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcData;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iget-object v0, p0, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v0, Landroid/support/place/music/TungstenReceiverService$OnIsOutputEnabled;

    invoke-interface {v0, v1}, Landroid/support/place/music/TungstenReceiverService$OnIsOutputEnabled;->onIsOutputEnabled(Z)V

    :cond_0
    return-void
.end method

.method public final onResult([B)V
    .locals 1

    iget v0, p0, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;->methodId:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-virtual {p0, p1}, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;->versionCheck([B)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0, p1}, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;->reset([B)V

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0, p1}, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;->getMasterVolume([B)V

    goto :goto_0

    :pswitch_4
    invoke-virtual {p0, p1}, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;->getMasterMute([B)V

    goto :goto_0

    :pswitch_5
    invoke-virtual {p0, p1}, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;->setFixedVolumeOut([B)V

    goto :goto_0

    :pswitch_6
    invoke-virtual {p0, p1}, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;->getFixedVolumeOut([B)V

    goto :goto_0

    :pswitch_7
    invoke-virtual {p0, p1}, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;->setSyncDelay([B)V

    goto :goto_0

    :pswitch_8
    invoke-virtual {p0, p1}, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;->getSyncDelay([B)V

    goto :goto_0

    :pswitch_9
    invoke-virtual {p0, p1}, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;->isOutputEnabled([B)V

    goto :goto_0

    :pswitch_a
    invoke-virtual {p0, p1}, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;->setOutputEnabled([B)V

    goto :goto_0

    :pswitch_b
    invoke-virtual {p0, p1}, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;->setFixedVolumeLevel([B)V

    goto :goto_0

    :pswitch_c
    invoke-virtual {p0, p1}, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;->getFixedVolumeLevel([B)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method public final reset([B)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v1, "_result"

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcData;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iget-object v0, p0, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v0, Landroid/support/place/music/TungstenReceiverService$OnReset;

    invoke-interface {v0, v1}, Landroid/support/place/music/TungstenReceiverService$OnReset;->onReset(Z)V

    :cond_0
    return-void
.end method

.method public final setFixedVolumeLevel([B)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v1, "_result"

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcData;->getFloat(Ljava/lang/String;)F

    move-result v1

    iget-object v0, p0, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v0, Landroid/support/place/music/TungstenReceiverService$OnSetFixedVolumeLevel;

    invoke-interface {v0, v1}, Landroid/support/place/music/TungstenReceiverService$OnSetFixedVolumeLevel;->onSetFixedVolumeLevel(F)V

    :cond_0
    return-void
.end method

.method public final setFixedVolumeOut([B)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v1, "_result"

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcData;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iget-object v0, p0, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v0, Landroid/support/place/music/TungstenReceiverService$OnSetFixedVolumeOut;

    invoke-interface {v0, v1}, Landroid/support/place/music/TungstenReceiverService$OnSetFixedVolumeOut;->onSetFixedVolumeOut(Z)V

    :cond_0
    return-void
.end method

.method public final setOutputEnabled([B)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v1, "_result"

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcData;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iget-object v0, p0, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v0, Landroid/support/place/music/TungstenReceiverService$OnSetOutputEnabled;

    invoke-interface {v0, v1}, Landroid/support/place/music/TungstenReceiverService$OnSetOutputEnabled;->onSetOutputEnabled(Z)V

    :cond_0
    return-void
.end method

.method public final setSyncDelay([B)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v1, "_result"

    invoke-virtual {v0, v1}, Landroid/support/place/rpc/RpcData;->getFloat(Ljava/lang/String;)F

    move-result v1

    iget-object v0, p0, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v0, Landroid/support/place/music/TungstenReceiverService$OnSetSyncDelay;

    invoke-interface {v0, v1}, Landroid/support/place/music/TungstenReceiverService$OnSetSyncDelay;->onSetSyncDelay(F)V

    :cond_0
    return-void
.end method

.method public final versionCheck([B)V
    .locals 3

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v1, "_result"

    sget-object v2, Landroid/support/place/music/TgsVersionCheck;->RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;

    invoke-virtual {v0, v1, v2}, Landroid/support/place/rpc/RpcData;->getFlattenable(Ljava/lang/String;Landroid/support/place/rpc/Flattenable$Creator;)Landroid/support/place/rpc/Flattenable;

    move-result-object v0

    check-cast v0, Landroid/support/place/music/TgsVersionCheck;

    iget-object v1, p0, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/place/music/TungstenReceiverService$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v1, Landroid/support/place/music/TungstenReceiverService$OnVersionCheck;

    invoke-interface {v1, v0}, Landroid/support/place/music/TungstenReceiverService$OnVersionCheck;->onVersionCheck(Landroid/support/place/music/TgsVersionCheck;)V

    :cond_0
    return-void
.end method
