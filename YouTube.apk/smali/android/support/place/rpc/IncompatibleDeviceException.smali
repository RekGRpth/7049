.class public Landroid/support/place/rpc/IncompatibleDeviceException;
.super Ljava/lang/RuntimeException;
.source "SourceFile"


# static fields
.field public static final FAILURE_REASON_BT_SOCKET_CLASS_NOT_FOUND:I = 0x2

.field public static final FAILURE_REASON_NO_BLUETOOTH:I = 0x0

.field public static final FAILURE_REASON_UNKNOWN:I = -0x1

.field public static final FAILURE_REASON_UNSUPPORTED_CIPHER:I = 0x1


# instance fields
.field public mReason:I


# direct methods
.method public constructor <init>(Ljava/lang/Throwable;)V
    .locals 1

    invoke-direct {p0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    const/4 v0, -0x1

    iput v0, p0, Landroid/support/place/rpc/IncompatibleDeviceException;->mReason:I

    return-void
.end method

.method public constructor <init>(Ljava/lang/Throwable;I)V
    .locals 1

    invoke-direct {p0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    const/4 v0, -0x1

    iput v0, p0, Landroid/support/place/rpc/IncompatibleDeviceException;->mReason:I

    iput p2, p0, Landroid/support/place/rpc/IncompatibleDeviceException;->mReason:I

    return-void
.end method
