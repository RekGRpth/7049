.class public Landroid/support/place/theme/Theme;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;
.implements Landroid/support/place/rpc/Flattenable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field private static final KEY_DISPLAY_NAME:Ljava/lang/String; = "displayName"

.field private static final KEY_META_OPTION:Ljava/lang/String; = "metaOption"

.field private static final KEY_OPTIONS:Ljava/lang/String; = "options"

.field private static final KEY_THEME_ENGINE:Ljava/lang/String; = "engine"

.field public static final RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;


# instance fields
.field private mDisplayName:Ljava/lang/String;

.field private mMetaOption:Landroid/support/place/theme/ThemeMetaOption;

.field private mOptions:Landroid/support/place/rpc/RpcData;

.field private mThemeEngine:Landroid/support/place/theme/ThemeEngine;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/support/place/theme/Theme$1;

    invoke-direct {v0}, Landroid/support/place/theme/Theme$1;-><init>()V

    sput-object v0, Landroid/support/place/theme/Theme;->CREATOR:Landroid/os/Parcelable$Creator;

    new-instance v0, Landroid/support/place/theme/Theme$2;

    invoke-direct {v0}, Landroid/support/place/theme/Theme$2;-><init>()V

    sput-object v0, Landroid/support/place/theme/Theme;->RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v0, v0, [B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readByteArray([B)V

    new-instance v1, Landroid/support/place/rpc/RpcData;

    invoke-direct {v1}, Landroid/support/place/rpc/RpcData;-><init>()V

    invoke-virtual {v1, v0}, Landroid/support/place/rpc/RpcData;->deserialize([B)V

    invoke-virtual {p0, v1}, Landroid/support/place/theme/Theme;->readFromRpcData(Landroid/support/place/rpc/RpcData;)V

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/support/place/theme/Theme$1;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/support/place/theme/Theme;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Landroid/support/place/rpc/RpcData;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0, p1}, Landroid/support/place/theme/Theme;->readFromRpcData(Landroid/support/place/rpc/RpcData;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    instance-of v1, p1, Landroid/support/place/theme/Theme;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Landroid/support/place/theme/Theme;

    iget-object v1, p0, Landroid/support/place/theme/Theme;->mDisplayName:Ljava/lang/String;

    if-nez v1, :cond_2

    iget-object v1, p1, Landroid/support/place/theme/Theme;->mDisplayName:Ljava/lang/String;

    if-nez v1, :cond_0

    :cond_2
    iget-object v1, p0, Landroid/support/place/theme/Theme;->mDisplayName:Ljava/lang/String;

    iget-object v2, p1, Landroid/support/place/theme/Theme;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/place/theme/Theme;->mThemeEngine:Landroid/support/place/theme/ThemeEngine;

    if-nez v1, :cond_3

    iget-object v1, p1, Landroid/support/place/theme/Theme;->mThemeEngine:Landroid/support/place/theme/ThemeEngine;

    if-nez v1, :cond_0

    :cond_3
    iget-object v1, p0, Landroid/support/place/theme/Theme;->mThemeEngine:Landroid/support/place/theme/ThemeEngine;

    iget-object v2, p1, Landroid/support/place/theme/Theme;->mThemeEngine:Landroid/support/place/theme/ThemeEngine;

    invoke-virtual {v1, v2}, Landroid/support/place/theme/ThemeEngine;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/place/theme/Theme;->mOptions:Landroid/support/place/rpc/RpcData;

    if-nez v1, :cond_4

    iget-object v1, p1, Landroid/support/place/theme/Theme;->mOptions:Landroid/support/place/rpc/RpcData;

    if-nez v1, :cond_0

    :cond_4
    iget-object v1, p0, Landroid/support/place/theme/Theme;->mOptions:Landroid/support/place/rpc/RpcData;

    iget-object v2, p1, Landroid/support/place/theme/Theme;->mOptions:Landroid/support/place/rpc/RpcData;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/place/theme/Theme;->mMetaOption:Landroid/support/place/theme/ThemeMetaOption;

    if-nez v1, :cond_5

    iget-object v1, p1, Landroid/support/place/theme/Theme;->mMetaOption:Landroid/support/place/theme/ThemeMetaOption;

    if-nez v1, :cond_0

    :cond_5
    iget-object v1, p0, Landroid/support/place/theme/Theme;->mMetaOption:Landroid/support/place/theme/ThemeMetaOption;

    iget-object v2, p1, Landroid/support/place/theme/Theme;->mMetaOption:Landroid/support/place/theme/ThemeMetaOption;

    invoke-virtual {v1, v2}, Landroid/support/place/theme/ThemeMetaOption;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/support/place/theme/Theme;->mDisplayName:Ljava/lang/String;

    return-object v0
.end method

.method public getEngine()Landroid/support/place/theme/ThemeEngine;
    .locals 1

    iget-object v0, p0, Landroid/support/place/theme/Theme;->mThemeEngine:Landroid/support/place/theme/ThemeEngine;

    return-object v0
.end method

.method public getMetaOption()Landroid/support/place/theme/ThemeMetaOption;
    .locals 1

    iget-object v0, p0, Landroid/support/place/theme/Theme;->mMetaOption:Landroid/support/place/theme/ThemeMetaOption;

    return-object v0
.end method

.method public getOptions()Landroid/support/place/rpc/RpcData;
    .locals 1

    iget-object v0, p0, Landroid/support/place/theme/Theme;->mOptions:Landroid/support/place/rpc/RpcData;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/place/theme/Theme;->mDisplayName:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Landroid/support/place/theme/Theme;->mThemeEngine:Landroid/support/place/theme/ThemeEngine;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Landroid/support/place/theme/Theme;->mOptions:Landroid/support/place/rpc/RpcData;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Landroid/support/place/theme/Theme;->mMetaOption:Landroid/support/place/theme/ThemeMetaOption;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Landroid/support/place/theme/Theme;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Landroid/support/place/theme/Theme;->mThemeEngine:Landroid/support/place/theme/ThemeEngine;

    invoke-virtual {v0}, Landroid/support/place/theme/ThemeEngine;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Landroid/support/place/theme/Theme;->mOptions:Landroid/support/place/rpc/RpcData;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_3
    iget-object v1, p0, Landroid/support/place/theme/Theme;->mMetaOption:Landroid/support/place/theme/ThemeMetaOption;

    invoke-virtual {v1}, Landroid/support/place/theme/ThemeMetaOption;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public readFromRpcData(Landroid/support/place/rpc/RpcData;)V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "displayName"

    invoke-virtual {p1, v0}, Landroid/support/place/rpc/RpcData;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "displayName"

    invoke-virtual {p1, v0}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/place/theme/Theme;->mDisplayName:Ljava/lang/String;

    :goto_0
    const-string v0, "engine"

    invoke-virtual {p1, v0}, Landroid/support/place/rpc/RpcData;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Landroid/support/place/theme/ThemeEngine;

    const-string v1, "engine"

    invoke-virtual {p1, v1}, Landroid/support/place/rpc/RpcData;->getRpcData(Ljava/lang/String;)Landroid/support/place/rpc/RpcData;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/place/theme/ThemeEngine;-><init>(Landroid/support/place/rpc/RpcData;)V

    iput-object v0, p0, Landroid/support/place/theme/Theme;->mThemeEngine:Landroid/support/place/theme/ThemeEngine;

    :goto_1
    const-string v0, "options"

    invoke-virtual {p1, v0}, Landroid/support/place/rpc/RpcData;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "options"

    invoke-virtual {p1, v0}, Landroid/support/place/rpc/RpcData;->getRpcData(Ljava/lang/String;)Landroid/support/place/rpc/RpcData;

    move-result-object v0

    iput-object v0, p0, Landroid/support/place/theme/Theme;->mOptions:Landroid/support/place/rpc/RpcData;

    :goto_2
    const-string v0, "metaOption"

    invoke-virtual {p1, v0}, Landroid/support/place/rpc/RpcData;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Landroid/support/place/theme/ThemeMetaOption;

    const-string v1, "metaOption"

    invoke-virtual {p1, v1}, Landroid/support/place/rpc/RpcData;->getRpcData(Ljava/lang/String;)Landroid/support/place/rpc/RpcData;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/place/theme/ThemeMetaOption;-><init>(Landroid/support/place/rpc/RpcData;)V

    iput-object v0, p0, Landroid/support/place/theme/Theme;->mMetaOption:Landroid/support/place/theme/ThemeMetaOption;

    :goto_3
    return-void

    :cond_0
    iput-object v2, p0, Landroid/support/place/theme/Theme;->mDisplayName:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iput-object v2, p0, Landroid/support/place/theme/Theme;->mThemeEngine:Landroid/support/place/theme/ThemeEngine;

    goto :goto_1

    :cond_2
    iput-object v2, p0, Landroid/support/place/theme/Theme;->mOptions:Landroid/support/place/rpc/RpcData;

    goto :goto_2

    :cond_3
    iput-object v2, p0, Landroid/support/place/theme/Theme;->mMetaOption:Landroid/support/place/theme/ThemeMetaOption;

    goto :goto_3
.end method

.method public setDisplayName(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Landroid/support/place/theme/Theme;->mDisplayName:Ljava/lang/String;

    return-void
.end method

.method public setEngine(Landroid/support/place/theme/ThemeEngine;)V
    .locals 0

    iput-object p1, p0, Landroid/support/place/theme/Theme;->mThemeEngine:Landroid/support/place/theme/ThemeEngine;

    return-void
.end method

.method public setMetaOption(Landroid/support/place/theme/ThemeMetaOption;)V
    .locals 0

    iput-object p1, p0, Landroid/support/place/theme/Theme;->mMetaOption:Landroid/support/place/theme/ThemeMetaOption;

    return-void
.end method

.method public setOptions(Landroid/support/place/rpc/RpcData;)V
    .locals 0

    iput-object p1, p0, Landroid/support/place/theme/Theme;->mOptions:Landroid/support/place/rpc/RpcData;

    return-void
.end method

.method public toDebugString()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-virtual {p0}, Landroid/support/place/theme/Theme;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Theme(displayName("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/support/place/theme/Theme;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "), engine("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/place/theme/Theme;->getEngine()Landroid/support/place/theme/ThemeEngine;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "null"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "), options("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/place/theme/Theme;->getOptions()Landroid/support/place/rpc/RpcData;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "null"

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "), metaOptions("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/place/theme/Theme;->getMetaOption()Landroid/support/place/theme/ThemeMetaOption;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, "null"

    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/support/place/theme/Theme;->getEngine()Landroid/support/place/theme/ThemeEngine;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/place/theme/ThemeEngine;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Landroid/support/place/theme/Theme;->getOptions()Landroid/support/place/rpc/RpcData;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/place/rpc/RpcData;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Landroid/support/place/theme/Theme;->getMetaOption()Landroid/support/place/theme/ThemeMetaOption;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/place/theme/ThemeMetaOption;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    invoke-virtual {p0, v0}, Landroid/support/place/theme/Theme;->writeToRpcData(Landroid/support/place/rpc/RpcData;)V

    invoke-virtual {v0}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v0

    array-length v1, v0

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    return-void
.end method

.method public writeToRpcData(Landroid/support/place/rpc/RpcData;)V
    .locals 2

    iget-object v0, p0, Landroid/support/place/theme/Theme;->mDisplayName:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v0, "displayName"

    iget-object v1, p0, Landroid/support/place/theme/Theme;->mDisplayName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Landroid/support/place/theme/Theme;->mThemeEngine:Landroid/support/place/theme/ThemeEngine;

    if-eqz v0, :cond_1

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    iget-object v1, p0, Landroid/support/place/theme/Theme;->mThemeEngine:Landroid/support/place/theme/ThemeEngine;

    invoke-virtual {v1, v0}, Landroid/support/place/theme/ThemeEngine;->writeToRpcData(Landroid/support/place/rpc/RpcData;)V

    const-string v1, "engine"

    invoke-virtual {p1, v1, v0}, Landroid/support/place/rpc/RpcData;->putRpcData(Ljava/lang/String;Landroid/support/place/rpc/RpcData;)V

    :cond_1
    iget-object v0, p0, Landroid/support/place/theme/Theme;->mOptions:Landroid/support/place/rpc/RpcData;

    if-eqz v0, :cond_2

    const-string v0, "options"

    iget-object v1, p0, Landroid/support/place/theme/Theme;->mOptions:Landroid/support/place/rpc/RpcData;

    invoke-virtual {p1, v0, v1}, Landroid/support/place/rpc/RpcData;->putRpcData(Ljava/lang/String;Landroid/support/place/rpc/RpcData;)V

    :cond_2
    iget-object v0, p0, Landroid/support/place/theme/Theme;->mMetaOption:Landroid/support/place/theme/ThemeMetaOption;

    if-eqz v0, :cond_3

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    iget-object v1, p0, Landroid/support/place/theme/Theme;->mMetaOption:Landroid/support/place/theme/ThemeMetaOption;

    invoke-virtual {v1, v0}, Landroid/support/place/theme/ThemeMetaOption;->writeToRpcData(Landroid/support/place/rpc/RpcData;)V

    const-string v1, "metaOption"

    invoke-virtual {p1, v1, v0}, Landroid/support/place/rpc/RpcData;->putRpcData(Ljava/lang/String;Landroid/support/place/rpc/RpcData;)V

    :cond_3
    return-void
.end method
