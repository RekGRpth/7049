.class public abstract Landroid/support/place/theme/ThemeService$EndpointBase;
.super Landroid/support/place/connector/Connector;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/place/connector/Broker;Landroid/support/place/connector/PlaceInfo;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/support/place/connector/Connector;-><init>(Landroid/content/Context;Landroid/support/place/connector/Broker;Landroid/support/place/connector/PlaceInfo;)V

    return-void
.end method


# virtual methods
.method public abstract getTheme(Landroid/support/place/rpc/RpcContext;)Landroid/support/place/theme/Theme;
.end method

.method public abstract getThemeEngines(Landroid/support/place/rpc/RpcContext;)Ljava/util/List;
.end method

.method public process(Ljava/lang/String;[BLandroid/support/place/rpc/RpcContext;Landroid/support/place/rpc/RpcError;)[B
    .locals 4

    const/4 v1, 0x0

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p2}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v2, "setTheme"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "theme"

    sget-object v3, Landroid/support/place/theme/Theme;->RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;

    invoke-virtual {v0, v2, v3}, Landroid/support/place/rpc/RpcData;->getFlattenable(Ljava/lang/String;Landroid/support/place/rpc/Flattenable$Creator;)Landroid/support/place/rpc/Flattenable;

    move-result-object v0

    check-cast v0, Landroid/support/place/theme/Theme;

    invoke-virtual {p0, v0, p3}, Landroid/support/place/theme/ThemeService$EndpointBase;->setTheme(Landroid/support/place/theme/Theme;Landroid/support/place/rpc/RpcContext;)V

    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    const-string v0, "getTheme"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, p3}, Landroid/support/place/theme/ThemeService$EndpointBase;->getTheme(Landroid/support/place/rpc/RpcContext;)Landroid/support/place/theme/Theme;

    move-result-object v2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v3, "_result"

    invoke-virtual {v0, v3, v2}, Landroid/support/place/rpc/RpcData;->putFlattenable(Ljava/lang/String;Landroid/support/place/rpc/Flattenable;)V

    goto :goto_0

    :cond_1
    const-string v0, "getThemeEngines"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0, p3}, Landroid/support/place/theme/ThemeService$EndpointBase;->getThemeEngines(Landroid/support/place/rpc/RpcContext;)Ljava/util/List;

    move-result-object v2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v3, "_result"

    invoke-virtual {v0, v3, v2}, Landroid/support/place/rpc/RpcData;->putFlattenableList(Ljava/lang/String;Ljava/util/List;)V

    goto :goto_0

    :cond_2
    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/place/connector/Connector;->process(Ljava/lang/String;[BLandroid/support/place/rpc/RpcContext;Landroid/support/place/rpc/RpcError;)[B

    move-result-object v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method public pushOnThemeChanged(Landroid/support/place/theme/Theme;)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v1, "theme"

    invoke-virtual {v0, v1, p1}, Landroid/support/place/rpc/RpcData;->putFlattenable(Ljava/lang/String;Landroid/support/place/rpc/Flattenable;)V

    const-string v1, "onThemeChanged"

    invoke-virtual {v0}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Landroid/support/place/theme/ThemeService$EndpointBase;->pushEvent(Ljava/lang/String;[B)V

    return-void
.end method

.method public abstract setTheme(Landroid/support/place/theme/Theme;Landroid/support/place/rpc/RpcContext;)V
.end method
