.class Landroid/support/place/theme/ThemeService$Presenter;
.super Landroid/support/place/connector/EventListener;
.source "SourceFile"


# instance fields
.field private _listener:Landroid/support/place/theme/ThemeService$Listener;

.field final synthetic this$0:Landroid/support/place/theme/ThemeService;


# direct methods
.method public constructor <init>(Landroid/support/place/theme/ThemeService;Landroid/support/place/connector/Broker;Landroid/support/place/theme/ThemeService$Listener;)V
    .locals 0

    iput-object p1, p0, Landroid/support/place/theme/ThemeService$Presenter;->this$0:Landroid/support/place/theme/ThemeService;

    invoke-direct {p0, p2, p3}, Landroid/support/place/connector/EventListener;-><init>(Landroid/support/place/connector/Broker;Landroid/support/place/connector/EventListener$Listener;)V

    iput-object p3, p0, Landroid/support/place/theme/ThemeService$Presenter;->_listener:Landroid/support/place/theme/ThemeService$Listener;

    return-void
.end method


# virtual methods
.method public process(Ljava/lang/String;[BLandroid/support/place/rpc/RpcContext;Landroid/support/place/rpc/RpcError;)[B
    .locals 3

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p2}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v1, "onThemeChanged"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "theme"

    sget-object v2, Landroid/support/place/theme/Theme;->RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;

    invoke-virtual {v0, v1, v2}, Landroid/support/place/rpc/RpcData;->getFlattenable(Ljava/lang/String;Landroid/support/place/rpc/Flattenable$Creator;)Landroid/support/place/rpc/Flattenable;

    move-result-object v0

    check-cast v0, Landroid/support/place/theme/Theme;

    iget-object v1, p0, Landroid/support/place/theme/ThemeService$Presenter;->_listener:Landroid/support/place/theme/ThemeService$Listener;

    invoke-virtual {v1, v0, p3}, Landroid/support/place/theme/ThemeService$Listener;->onThemeChanged(Landroid/support/place/theme/Theme;Landroid/support/place/rpc/RpcContext;)V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/place/connector/EventListener;->process(Ljava/lang/String;[BLandroid/support/place/rpc/RpcContext;Landroid/support/place/rpc/RpcError;)[B

    move-result-object v0

    goto :goto_0
.end method
