.class public Lcom/android/athome/picker/MediaOutput;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final TYPE_AT_HOME_RECEIVER:I = 0x2

.field public static final TYPE_SPEAKER:I = 0x1

.field public static final TYPE_UNKNOWN:I = 0x0

.field public static final TYPE_USER_DEFINED:I = 0x3e8


# instance fields
.field private final mId:Ljava/lang/String;

.field private mIsMuted:Z

.field private mName:Ljava/lang/String;

.field private mStatus:Ljava/lang/String;

.field private final mType:I

.field private mVolume:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/athome/picker/e;

    invoke-direct {v0}, Lcom/android/athome/picker/e;-><init>()V

    sput-object v0, Lcom/android/athome/picker/MediaOutput;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/athome/picker/MediaOutput;->mId:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/athome/picker/MediaOutput;->mType:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/athome/picker/MediaOutput;->mName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/athome/picker/MediaOutput;->mStatus:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/android/athome/picker/MediaOutput;->mVolume:F

    const/4 v0, 0x1

    new-array v0, v0, [Z

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readBooleanArray([Z)V

    const/4 v1, 0x0

    aget-boolean v0, v0, v1

    iput-boolean v0, p0, Lcom/android/athome/picker/MediaOutput;->mIsMuted:Z

    return-void
.end method

.method private constructor <init>(Lcom/android/athome/picker/f;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/android/athome/picker/f;->a(Lcom/android/athome/picker/f;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/athome/picker/MediaOutput;->mId:Ljava/lang/String;

    invoke-static {p1}, Lcom/android/athome/picker/f;->b(Lcom/android/athome/picker/f;)I

    move-result v0

    iput v0, p0, Lcom/android/athome/picker/MediaOutput;->mType:I

    invoke-static {p1}, Lcom/android/athome/picker/f;->c(Lcom/android/athome/picker/f;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/athome/picker/MediaOutput;->mName:Ljava/lang/String;

    invoke-static {p1}, Lcom/android/athome/picker/f;->d(Lcom/android/athome/picker/f;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/athome/picker/MediaOutput;->mStatus:Ljava/lang/String;

    invoke-static {p1}, Lcom/android/athome/picker/f;->e(Lcom/android/athome/picker/f;)F

    move-result v0

    iput v0, p0, Lcom/android/athome/picker/MediaOutput;->mVolume:F

    invoke-static {p1}, Lcom/android/athome/picker/f;->f(Lcom/android/athome/picker/f;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/athome/picker/MediaOutput;->mIsMuted:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/athome/picker/f;Lcom/android/athome/picker/e;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/athome/picker/MediaOutput;-><init>(Lcom/android/athome/picker/f;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;FZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/athome/picker/MediaOutput;->mId:Ljava/lang/String;

    iput p2, p0, Lcom/android/athome/picker/MediaOutput;->mType:I

    iput-object p3, p0, Lcom/android/athome/picker/MediaOutput;->mName:Ljava/lang/String;

    iput-object p4, p0, Lcom/android/athome/picker/MediaOutput;->mStatus:Ljava/lang/String;

    iput p5, p0, Lcom/android/athome/picker/MediaOutput;->mVolume:F

    iput-boolean p6, p0, Lcom/android/athome/picker/MediaOutput;->mIsMuted:Z

    return-void
.end method

.method static synthetic access$1000(Lcom/android/athome/picker/MediaOutput;)F
    .locals 1

    iget v0, p0, Lcom/android/athome/picker/MediaOutput;->mVolume:F

    return v0
.end method

.method static synthetic access$1100(Lcom/android/athome/picker/MediaOutput;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/athome/picker/MediaOutput;->mIsMuted:Z

    return v0
.end method

.method static synthetic access$600(Lcom/android/athome/picker/MediaOutput;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/MediaOutput;->mId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/android/athome/picker/MediaOutput;)I
    .locals 1

    iget v0, p0, Lcom/android/athome/picker/MediaOutput;->mType:I

    return v0
.end method

.method static synthetic access$800(Lcom/android/athome/picker/MediaOutput;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/MediaOutput;->mName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$900(Lcom/android/athome/picker/MediaOutput;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/MediaOutput;->mStatus:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    instance-of v2, p1, Lcom/android/athome/picker/MediaOutput;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-ne p1, p0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/android/athome/picker/MediaOutput;

    iget-object v2, p0, Lcom/android/athome/picker/MediaOutput;->mId:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/android/athome/picker/MediaOutput;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/android/athome/picker/MediaOutput;->mType:I

    invoke-virtual {p1}, Lcom/android/athome/picker/MediaOutput;->getType()I

    move-result v3

    if-ne v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/MediaOutput;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public getIsMuted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/athome/picker/MediaOutput;->mIsMuted:Z

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/MediaOutput;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getStatus()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/MediaOutput;->mStatus:Ljava/lang/String;

    return-object v0
.end method

.method public getType()I
    .locals 1

    iget v0, p0, Lcom/android/athome/picker/MediaOutput;->mType:I

    return v0
.end method

.method public getVolume()F
    .locals 1

    iget v0, p0, Lcom/android/athome/picker/MediaOutput;->mVolume:F

    return v0
.end method

.method public isGroupable()Z
    .locals 2

    iget v0, p0, Lcom/android/athome/picker/MediaOutput;->mType:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/android/athome/picker/MediaOutput;->mType:I

    const/16 v1, 0x3e8

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setIsMuted(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/athome/picker/MediaOutput;->mIsMuted:Z

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/athome/picker/MediaOutput;->mName:Ljava/lang/String;

    return-void
.end method

.method public setStatus(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/athome/picker/MediaOutput;->mStatus:Ljava/lang/String;

    return-void
.end method

.method public setVolume(F)V
    .locals 0

    iput p1, p0, Lcom/android/athome/picker/MediaOutput;->mVolume:F

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MediaOutput(mId="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutput;->mId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/athome/picker/MediaOutput;->mType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutput;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mStatus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutput;->mStatus:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mVolume="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/athome/picker/MediaOutput;->mVolume:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mIsMuted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/athome/picker/MediaOutput;->mIsMuted:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    iget-object v0, p0, Lcom/android/athome/picker/MediaOutput;->mId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/android/athome/picker/MediaOutput;->mType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/android/athome/picker/MediaOutput;->mName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/athome/picker/MediaOutput;->mStatus:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/android/athome/picker/MediaOutput;->mVolume:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    const/4 v0, 0x1

    new-array v0, v0, [Z

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/android/athome/picker/MediaOutput;->mIsMuted:Z

    aput-boolean v2, v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    return-void
.end method
