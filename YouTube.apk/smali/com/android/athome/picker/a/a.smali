.class public final Lcom/android/athome/picker/a/a;
.super Landroid/support/v4/app/d;
.source "SourceFile"


# static fields
.field private static final ab:[I


# instance fields
.field Y:Ljava/lang/Object;

.field final Z:Lcom/android/athome/picker/a/h;

.field final aa:Lcom/android/athome/picker/a/c;

.field private ac:I

.field private ad:Landroid/view/LayoutInflater;

.field private ae:Lcom/android/athome/picker/a/b;

.field private af:Landroid/view/View$OnClickListener;

.field private ag:Lcom/android/athome/picker/a/d;

.field private ah:Landroid/widget/ListView;

.field private ai:Landroid/widget/SeekBar;

.field private aj:Landroid/widget/ImageView;

.field private ak:Z

.field private al:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x5

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget v2, Lcom/android/athome/picker/aj;->k:I

    aput v2, v0, v1

    const/4 v1, 0x1

    sget v2, Lcom/android/athome/picker/aj;->j:I

    aput v2, v0, v1

    const/4 v1, 0x2

    sget v2, Lcom/android/athome/picker/aj;->g:I

    aput v2, v0, v1

    const/4 v1, 0x3

    sget v2, Lcom/android/athome/picker/aj;->h:I

    aput v2, v0, v1

    const/4 v1, 0x4

    sget v2, Lcom/android/athome/picker/aj;->i:I

    aput v2, v0, v1

    sput-object v0, Lcom/android/athome/picker/a/a;->ab:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/support/v4/app/d;-><init>()V

    new-instance v0, Lcom/android/athome/picker/a/h;

    invoke-direct {v0, p0}, Lcom/android/athome/picker/a/h;-><init>(Lcom/android/athome/picker/a/a;)V

    iput-object v0, p0, Lcom/android/athome/picker/a/a;->Z:Lcom/android/athome/picker/a/h;

    new-instance v0, Lcom/android/athome/picker/a/c;

    invoke-direct {v0, p0}, Lcom/android/athome/picker/a/c;-><init>(Lcom/android/athome/picker/a/a;)V

    iput-object v0, p0, Lcom/android/athome/picker/a/a;->aa:Lcom/android/athome/picker/a/c;

    const/4 v0, 0x1

    sget v1, Lcom/android/athome/picker/al;->a:I

    invoke-virtual {p0, v0, v1}, Lcom/android/athome/picker/a/a;->a(II)V

    return-void
.end method

.method static synthetic E()[I
    .locals 1

    sget-object v0, Lcom/android/athome/picker/a/a;->ab:[I

    return-object v0
.end method

.method static synthetic a(Lcom/android/athome/picker/a/a;)I
    .locals 1

    iget v0, p0, Lcom/android/athome/picker/a/a;->ac:I

    return v0
.end method

.method static synthetic a(Lcom/android/athome/picker/a/a;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/athome/picker/a/a;->al:Z

    return p1
.end method

.method static synthetic b(Lcom/android/athome/picker/a/a;)Landroid/widget/ListView;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/a/a;->ah:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic c(Lcom/android/athome/picker/a/a;)Landroid/view/LayoutInflater;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/a/a;->ad:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic d(Lcom/android/athome/picker/a/a;)Lcom/android/athome/picker/a/d;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/a/a;->ag:Lcom/android/athome/picker/a/d;

    return-object v0
.end method

.method static synthetic e(Lcom/android/athome/picker/a/a;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/athome/picker/a/a;->al:Z

    return v0
.end method

.method static synthetic f(Lcom/android/athome/picker/a/a;)Landroid/widget/SeekBar;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/a/a;->ai:Landroid/widget/SeekBar;

    return-object v0
.end method


# virtual methods
.method final D()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/android/athome/picker/a/a;->Y:Ljava/lang/Object;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/athome/picker/a/a;->Y:Ljava/lang/Object;

    iget v1, p0, Lcom/android/athome/picker/a/a;->ac:I

    invoke-static {v0, v1}, Lcom/android/athome/picker/media/k;->b(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lcom/android/athome/picker/a/a;->aj:Landroid/widget/ImageView;

    invoke-static {v1}, Lcom/android/athome/picker/media/q;->l(Ljava/lang/Object;)I

    move-result v0

    if-nez v0, :cond_1

    sget v0, Lcom/android/athome/picker/ah;->a:I

    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    iput-boolean v3, p0, Lcom/android/athome/picker/a/a;->ak:Z

    invoke-static {v1}, Lcom/android/athome/picker/media/q;->k(Ljava/lang/Object;)I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/athome/picker/a/a;->ai:Landroid/widget/SeekBar;

    invoke-virtual {v0, v3}, Landroid/widget/SeekBar;->setMax(I)V

    iget-object v0, p0, Lcom/android/athome/picker/a/a;->ai:Landroid/widget/SeekBar;

    invoke-virtual {v0, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v0, p0, Lcom/android/athome/picker/a/a;->ai:Landroid/widget/SeekBar;

    invoke-virtual {v0, v4}, Landroid/widget/SeekBar;->setEnabled(Z)V

    :goto_2
    iput-boolean v4, p0, Lcom/android/athome/picker/a/a;->ak:Z

    goto :goto_0

    :cond_1
    sget v0, Lcom/android/athome/picker/ah;->b:I

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/android/athome/picker/a/a;->ai:Landroid/widget/SeekBar;

    invoke-virtual {v0, v3}, Landroid/widget/SeekBar;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/athome/picker/a/a;->ai:Landroid/widget/SeekBar;

    invoke-static {v1}, Lcom/android/athome/picker/media/q;->j(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setMax(I)V

    iget-object v0, p0, Lcom/android/athome/picker/a/a;->ai:Landroid/widget/SeekBar;

    invoke-static {v1}, Lcom/android/athome/picker/media/q;->i(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_2
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    const/4 v3, 0x0

    iput-object p1, p0, Lcom/android/athome/picker/a/a;->ad:Landroid/view/LayoutInflater;

    sget v0, Lcom/android/athome/picker/aj;->f:I

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    sget v0, Lcom/android/athome/picker/ai;->B:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/athome/picker/a/a;->aj:Landroid/widget/ImageView;

    sget v0, Lcom/android/athome/picker/ai;->C:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/android/athome/picker/a/a;->ai:Landroid/widget/SeekBar;

    invoke-virtual {p0}, Lcom/android/athome/picker/a/a;->D()V

    iget-object v0, p0, Lcom/android/athome/picker/a/a;->ai:Landroid/widget/SeekBar;

    new-instance v2, Lcom/android/athome/picker/a/j;

    invoke-direct {v2, p0}, Lcom/android/athome/picker/a/j;-><init>(Lcom/android/athome/picker/a/a;)V

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iget-object v0, p0, Lcom/android/athome/picker/a/a;->af:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    sget v0, Lcom/android/athome/picker/ai;->g:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/athome/picker/a/a;->af:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    sget v0, Lcom/android/athome/picker/ai;->o:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    new-instance v2, Lcom/android/athome/picker/a/d;

    invoke-direct {v2, p0}, Lcom/android/athome/picker/a/d;-><init>(Lcom/android/athome/picker/a/a;)V

    iput-object v2, p0, Lcom/android/athome/picker/a/a;->ag:Lcom/android/athome/picker/a/d;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v2, p0, Lcom/android/athome/picker/a/a;->ag:Lcom/android/athome/picker/a/d;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iput-object v0, p0, Lcom/android/athome/picker/a/a;->ah:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/android/athome/picker/a/a;->Y:Ljava/lang/Object;

    iget v2, p0, Lcom/android/athome/picker/a/a;->ac:I

    iget-object v3, p0, Lcom/android/athome/picker/a/a;->aa:Lcom/android/athome/picker/a/c;

    invoke-static {v0, v2, v3}, Lcom/android/athome/picker/media/k;->a(Ljava/lang/Object;ILcom/android/athome/picker/media/b;)V

    iget-object v0, p0, Lcom/android/athome/picker/a/a;->ag:Lcom/android/athome/picker/a/d;

    invoke-virtual {v0}, Lcom/android/athome/picker/a/d;->c()V

    return-object v1
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/support/v4/app/d;->a(Landroid/app/Activity;)V

    invoke-static {p1}, Lcom/android/athome/picker/media/k;->a(Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/android/athome/picker/a/a;->Y:Ljava/lang/Object;

    return-void
.end method

.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 0

    iput-object p1, p0, Lcom/android/athome/picker/a/a;->af:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public final a(Lcom/android/athome/picker/a/b;)V
    .locals 0

    iput-object p1, p0, Lcom/android/athome/picker/a/a;->ae:Lcom/android/athome/picker/a/b;

    return-void
.end method

.method public final b(I)V
    .locals 0

    iput p1, p0, Lcom/android/athome/picker/a/a;->ac:I

    return-void
.end method

.method public final c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    new-instance v0, Lcom/android/athome/picker/a/g;

    invoke-virtual {p0}, Lcom/android/athome/picker/a/a;->j()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/athome/picker/a/a;->c()I

    move-result v2

    invoke-direct {v0, p0, v1, v2}, Lcom/android/athome/picker/a/g;-><init>(Lcom/android/athome/picker/a/a;Landroid/content/Context;I)V

    return-object v0
.end method

.method final c(I)V
    .locals 3

    iget-boolean v0, p0, Lcom/android/athome/picker/a/a;->ak:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/athome/picker/a/a;->Y:Ljava/lang/Object;

    iget v1, p0, Lcom/android/athome/picker/a/a;->ac:I

    invoke-static {v0, v1}, Lcom/android/athome/picker/media/k;->b(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/android/athome/picker/media/q;->k(Ljava/lang/Object;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    invoke-static {v0}, Lcom/android/athome/picker/media/q;->j(Ljava/lang/Object;)I

    move-result v1

    const/4 v2, 0x0

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/athome/picker/media/q;->c(Ljava/lang/Object;I)V

    goto :goto_0
.end method

.method public final d()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Landroid/support/v4/app/d;->d()V

    iget-object v0, p0, Lcom/android/athome/picker/a/a;->ae:Lcom/android/athome/picker/a/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/a/a;->ae:Lcom/android/athome/picker/a/b;

    invoke-interface {v0}, Lcom/android/athome/picker/a/b;->a()V

    :cond_0
    iget-object v0, p0, Lcom/android/athome/picker/a/a;->ag:Lcom/android/athome/picker/a/d;

    if-eqz v0, :cond_1

    iput-object v2, p0, Lcom/android/athome/picker/a/a;->ag:Lcom/android/athome/picker/a/d;

    :cond_1
    iput-object v2, p0, Lcom/android/athome/picker/a/a;->ad:Landroid/view/LayoutInflater;

    iget-object v0, p0, Lcom/android/athome/picker/a/a;->Y:Ljava/lang/Object;

    iget-object v1, p0, Lcom/android/athome/picker/a/a;->aa:Lcom/android/athome/picker/a/c;

    invoke-static {v0, v1}, Lcom/android/athome/picker/media/k;->a(Ljava/lang/Object;Lcom/android/athome/picker/media/b;)V

    iput-object v2, p0, Lcom/android/athome/picker/a/a;->Y:Ljava/lang/Object;

    return-void
.end method

.method public final s()V
    .locals 0

    invoke-super {p0}, Landroid/support/v4/app/d;->s()V

    return-void
.end method
