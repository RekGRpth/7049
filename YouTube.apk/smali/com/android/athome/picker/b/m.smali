.class final Lcom/android/athome/picker/b/m;
.super Lcom/android/athome/picker/media/c;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/android/athome/picker/b/a;


# direct methods
.method constructor <init>(Lcom/android/athome/picker/b/a;)V
    .locals 0

    iput-object p1, p0, Lcom/android/athome/picker/b/m;->a:Lcom/android/athome/picker/b/a;

    invoke-direct {p0}, Lcom/android/athome/picker/media/c;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/Object;)V
    .locals 3

    const-string v0, "AtHomeMediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "OnRouteUnSelected: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/athome/picker/b/m;->a:Lcom/android/athome/picker/b/a;

    invoke-static {v0}, Lcom/android/athome/picker/b/a;->j(Lcom/android/athome/picker/b/a;)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p2, :cond_1

    iget-object v0, p0, Lcom/android/athome/picker/b/m;->a:Lcom/android/athome/picker/b/a;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/athome/picker/b/a;->a(Lcom/android/athome/picker/b/a;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/athome/picker/b/m;->a:Lcom/android/athome/picker/b/a;

    invoke-static {v0}, Lcom/android/athome/picker/b/a;->g(Lcom/android/athome/picker/b/a;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/b/m;->a:Lcom/android/athome/picker/b/a;

    invoke-static {v0}, Lcom/android/athome/picker/b/a;->h(Lcom/android/athome/picker/b/a;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/b/m;->a:Lcom/android/athome/picker/b/a;

    invoke-static {v0}, Lcom/android/athome/picker/b/a;->i(Lcom/android/athome/picker/b/a;)Landroid/support/place/picker/MediaRouteProviderClient;

    move-result-object v0

    iget-object v1, p0, Lcom/android/athome/picker/b/m;->a:Lcom/android/athome/picker/b/a;

    invoke-static {v1}, Lcom/android/athome/picker/b/a;->h(Lcom/android/athome/picker/b/a;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/place/picker/MediaRouteProviderClient;->deleteRouteIdForApplication(Ljava/lang/String;)V

    :cond_0
    invoke-static {p2}, Lcom/android/athome/picker/media/q;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, Lcom/android/athome/picker/b/a;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/athome/picker/b/m;->a:Lcom/android/athome/picker/b/a;

    invoke-static {v0}, Lcom/android/athome/picker/b/a;->f(Lcom/android/athome/picker/b/a;)Lcom/android/athome/picker/b/w;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/athome/picker/b/m;->a:Lcom/android/athome/picker/b/a;

    invoke-static {v0}, Lcom/android/athome/picker/b/a;->f(Lcom/android/athome/picker/b/a;)Lcom/android/athome/picker/b/w;

    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 3

    const-string v0, "AtHomeMediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "OnRouteAdded: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/android/athome/picker/b/a;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " hashCode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public final a(Ljava/lang/Object;ILjava/lang/Object;)V
    .locals 7

    const/4 v6, 0x1

    const/4 v0, 0x0

    const-string v1, "AtHomeMediaRouter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "OnRouteSelected: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/athome/picker/b/m;->a:Lcom/android/athome/picker/b/a;

    invoke-static {v1}, Lcom/android/athome/picker/b/a;->d(Lcom/android/athome/picker/b/a;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "AtHomeMediaRouter"

    const-string v1, "Updating media router. Ignore updates."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    invoke-static {p3}, Lcom/android/athome/picker/media/q;->f(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {}, Lcom/android/athome/picker/b/a;->d()Ljava/lang/Object;

    move-result-object v2

    if-ne v1, v2, :cond_4

    invoke-static {p3}, Lcom/android/athome/picker/media/q;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-static {v2}, Lcom/android/athome/picker/b/a;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_3

    invoke-static {v2}, Lcom/android/athome/picker/media/p;->a(Ljava/lang/Object;)I

    move-result v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    invoke-static {v2, v1}, Lcom/android/athome/picker/media/p;->b(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    iget-object v5, p0, Lcom/android/athome/picker/b/m;->a:Lcom/android/athome/picker/b/a;

    invoke-static {v5}, Lcom/android/athome/picker/b/a;->e(Lcom/android/athome/picker/b/a;)Ljava/util/HashMap;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/place/connector/ConnectorInfo;

    invoke-virtual {v0}, Landroid/support/place/connector/ConnectorInfo;->getEndpointInfo()Landroid/support/place/rpc/EndpointInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/place/rpc/EndpointInfo;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/android/athome/picker/b/m;->a:Lcom/android/athome/picker/b/a;

    invoke-static {v0, v4, v2, v6}, Lcom/android/athome/picker/b/a;->a(Lcom/android/athome/picker/b/a;Ljava/util/List;Ljava/lang/Object;Z)V

    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/android/athome/picker/b/m;->a:Lcom/android/athome/picker/b/a;

    invoke-static {v0, p3}, Lcom/android/athome/picker/b/a;->a(Lcom/android/athome/picker/b/a;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/athome/picker/b/m;->a:Lcom/android/athome/picker/b/a;

    invoke-static {v0}, Lcom/android/athome/picker/b/a;->f(Lcom/android/athome/picker/b/a;)Lcom/android/athome/picker/b/w;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/athome/picker/b/m;->a:Lcom/android/athome/picker/b/a;

    invoke-static {v0}, Lcom/android/athome/picker/b/a;->f(Lcom/android/athome/picker/b/a;)Lcom/android/athome/picker/b/w;

    goto :goto_2

    :cond_4
    invoke-static {p3}, Lcom/android/athome/picker/media/q;->f(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {}, Lcom/android/athome/picker/b/a;->c()Ljava/lang/Object;

    move-result-object v2

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/android/athome/picker/b/m;->a:Lcom/android/athome/picker/b/a;

    invoke-virtual {v1, p3}, Lcom/android/athome/picker/b/a;->a(Ljava/lang/Object;)Lcom/android/athome/picker/b/v;

    move-result-object v1

    iget-object v2, p0, Lcom/android/athome/picker/b/m;->a:Lcom/android/athome/picker/b/a;

    invoke-static {v2}, Lcom/android/athome/picker/b/a;->g(Lcom/android/athome/picker/b/a;)Z

    move-result v2

    if-eqz v2, :cond_2

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/android/athome/picker/b/v;->b()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ne v2, v6, :cond_2

    iget-object v2, p0, Lcom/android/athome/picker/b/m;->a:Lcom/android/athome/picker/b/a;

    invoke-static {v2}, Lcom/android/athome/picker/b/a;->h(Lcom/android/athome/picker/b/a;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/android/athome/picker/b/v;->b()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/place/connector/ConnectorInfo;

    invoke-virtual {v0}, Landroid/support/place/connector/ConnectorInfo;->getEndpointInfo()Landroid/support/place/rpc/EndpointInfo;

    invoke-virtual {v1}, Lcom/android/athome/picker/b/v;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    new-instance v0, Landroid/support/place/picker/MediaRouteProviderClient$SerialNumberRouteId;

    invoke-virtual {v1}, Lcom/android/athome/picker/b/v;->c()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/place/picker/MediaRouteProviderClient$SerialNumberRouteId;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/athome/picker/b/m;->a:Lcom/android/athome/picker/b/a;

    invoke-static {v1}, Lcom/android/athome/picker/b/a;->i(Lcom/android/athome/picker/b/a;)Landroid/support/place/picker/MediaRouteProviderClient;

    move-result-object v1

    iget-object v2, p0, Lcom/android/athome/picker/b/m;->a:Lcom/android/athome/picker/b/a;

    invoke-static {v2}, Lcom/android/athome/picker/b/a;->h(Lcom/android/athome/picker/b/a;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/support/place/picker/MediaRouteProviderClient;->setRouteIdForApplication(Ljava/lang/String;Landroid/support/place/picker/MediaRouteProviderClient$RouteId;)V

    goto :goto_2
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 6

    const/4 v2, 0x0

    const-string v0, "AtHomeMediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "OnRouteGrouped: routeInfo="

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " routeGroup="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/athome/picker/b/m;->a:Lcom/android/athome/picker/b/a;

    invoke-static {v0}, Lcom/android/athome/picker/b/a;->d(Lcom/android/athome/picker/b/a;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "AtHomeMediaRouter"

    const-string v1, "Updating media router. Ignore updates."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p2}, Lcom/android/athome/picker/b/a;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/athome/picker/b/m;->a:Lcom/android/athome/picker/b/a;

    invoke-static {v0, p1, p2, v2}, Lcom/android/athome/picker/b/a;->a(Lcom/android/athome/picker/b/a;Ljava/lang/Object;Ljava/lang/Object;Z)V

    goto :goto_0

    :cond_2
    invoke-static {p2}, Lcom/android/athome/picker/media/p;->a(Ljava/lang/Object;)I

    move-result v3

    const/4 v0, 0x1

    if-le v3, v0, :cond_0

    const-string v0, "AtHomeMediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "RouteCount: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " creating new group."

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    :goto_1
    if-ge v1, v3, :cond_3

    invoke-static {p2, v1}, Lcom/android/athome/picker/media/p;->b(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    iget-object v5, p0, Lcom/android/athome/picker/b/m;->a:Lcom/android/athome/picker/b/a;

    invoke-static {v5}, Lcom/android/athome/picker/b/a;->e(Lcom/android/athome/picker/b/a;)Ljava/util/HashMap;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/place/connector/ConnectorInfo;

    invoke-virtual {v0}, Landroid/support/place/connector/ConnectorInfo;->getEndpointInfo()Landroid/support/place/rpc/EndpointInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/place/rpc/EndpointInfo;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/android/athome/picker/b/m;->a:Lcom/android/athome/picker/b/a;

    invoke-static {v0, v4, p2, v2}, Lcom/android/athome/picker/b/a;->a(Lcom/android/athome/picker/b/a;Ljava/util/List;Ljava/lang/Object;Z)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 3

    const-string v0, "AtHomeMediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "OnRouteRemoved: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/android/athome/picker/b/a;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " hashCode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p1}, Lcom/android/athome/picker/media/q;->f(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {}, Lcom/android/athome/picker/b/a;->d()Ljava/lang/Object;

    move-result-object v1

    if-ne v0, v1, :cond_0

    invoke-static {p1}, Lcom/android/athome/picker/media/q;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/android/athome/picker/b/a;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/athome/picker/b/m;->a:Lcom/android/athome/picker/b/a;

    invoke-static {v1}, Lcom/android/athome/picker/b/a;->b(Lcom/android/athome/picker/b/a;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/android/athome/picker/b/m;->a:Lcom/android/athome/picker/b/a;

    invoke-static {v1}, Lcom/android/athome/picker/b/a;->c(Lcom/android/athome/picker/b/a;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    const-string v0, "AtHomeMediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "OnRouteUnGrouped: routeInfo="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " routeGroup="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/athome/picker/b/m;->a:Lcom/android/athome/picker/b/a;

    invoke-static {v0}, Lcom/android/athome/picker/b/a;->d(Lcom/android/athome/picker/b/a;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "AtHomeMediaRouter"

    const-string v1, "Updating media router. Ignore updates."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p2}, Lcom/android/athome/picker/b/a;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/b/m;->a:Lcom/android/athome/picker/b/a;

    const/4 v1, 0x1

    invoke-static {v0, p1, p2, v1}, Lcom/android/athome/picker/b/a;->a(Lcom/android/athome/picker/b/a;Ljava/lang/Object;Ljava/lang/Object;Z)V

    goto :goto_0
.end method
