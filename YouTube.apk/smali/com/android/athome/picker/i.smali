.class final Lcom/android/athome/picker/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Landroid/graphics/drawable/LevelListDrawable;

.field final synthetic b:Landroid/widget/SeekBar;

.field final synthetic c:Lcom/android/athome/picker/MediaOutput;

.field final synthetic d:Lcom/android/athome/picker/g;


# direct methods
.method constructor <init>(Lcom/android/athome/picker/g;Landroid/graphics/drawable/LevelListDrawable;Landroid/widget/SeekBar;Lcom/android/athome/picker/MediaOutput;)V
    .locals 0

    iput-object p1, p0, Lcom/android/athome/picker/i;->d:Lcom/android/athome/picker/g;

    iput-object p2, p0, Lcom/android/athome/picker/i;->a:Landroid/graphics/drawable/LevelListDrawable;

    iput-object p3, p0, Lcom/android/athome/picker/i;->b:Landroid/widget/SeekBar;

    iput-object p4, p0, Lcom/android/athome/picker/i;->c:Lcom/android/athome/picker/MediaOutput;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 5

    const/16 v3, 0x65

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/athome/picker/i;->d:Lcom/android/athome/picker/g;

    iget-object v4, p0, Lcom/android/athome/picker/i;->d:Lcom/android/athome/picker/g;

    invoke-static {v4}, Lcom/android/athome/picker/g;->a(Lcom/android/athome/picker/g;)Landroid/widget/AdapterView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/AdapterView;->getFirstVisiblePosition()I

    move-result v4

    invoke-static {v0, v4}, Lcom/android/athome/picker/g;->a(Lcom/android/athome/picker/g;I)I

    iget-object v0, p0, Lcom/android/athome/picker/i;->a:Landroid/graphics/drawable/LevelListDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/LevelListDrawable;->getLevel()I

    move-result v0

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_0
    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    iget-object v4, p0, Lcom/android/athome/picker/i;->a:Landroid/graphics/drawable/LevelListDrawable;

    if-eqz v0, :cond_3

    :goto_2
    invoke-virtual {v4, v3}, Landroid/graphics/drawable/LevelListDrawable;->setLevel(I)Z

    iget-object v3, p0, Lcom/android/athome/picker/i;->b:Landroid/widget/SeekBar;

    if-nez v0, :cond_4

    :goto_3
    invoke-virtual {v3, v1}, Landroid/widget/SeekBar;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/athome/picker/i;->d:Lcom/android/athome/picker/g;

    invoke-static {v1}, Lcom/android/athome/picker/g;->b(Lcom/android/athome/picker/g;)Lcom/android/athome/picker/n;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/athome/picker/i;->d:Lcom/android/athome/picker/g;

    invoke-static {v1}, Lcom/android/athome/picker/g;->b(Lcom/android/athome/picker/g;)Lcom/android/athome/picker/n;

    move-result-object v1

    iget-object v2, p0, Lcom/android/athome/picker/i;->c:Lcom/android/athome/picker/MediaOutput;

    invoke-interface {v1, v2, v0}, Lcom/android/athome/picker/n;->a(Lcom/android/athome/picker/MediaOutput;Z)V

    :cond_0
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    iget-object v3, p0, Lcom/android/athome/picker/i;->b:Landroid/widget/SeekBar;

    invoke-virtual {v3}, Landroid/widget/SeekBar;->getProgress()I

    move-result v3

    goto :goto_2

    :cond_4
    move v1, v2

    goto :goto_3
.end method
