.class public Lcom/android/athome/picker/media/MediaRouteButtonFallback;
.super Landroid/view/View;
.source "SourceFile"


# static fields
.field private static final l:[I


# instance fields
.field private a:Lcom/android/athome/picker/media/v;

.field private final b:Lcom/android/athome/picker/media/j;

.field private c:I

.field private d:Z

.field private e:Landroid/graphics/drawable/Drawable;

.field private f:Z

.field private g:Z

.field private h:I

.field private i:I

.field private j:Landroid/view/View$OnClickListener;

.field private k:Lcom/android/athome/picker/a/a;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100a0

    aput v2, v0, v1

    sput-object v0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->l:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    sget v0, Lcom/android/athome/picker/af;->a:I

    invoke-direct {p0, p1, p2, v0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Lcom/android/athome/picker/media/j;

    invoke-direct {v0, p0, v2}, Lcom/android/athome/picker/media/j;-><init>(Lcom/android/athome/picker/media/MediaRouteButtonFallback;B)V

    iput-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->b:Lcom/android/athome/picker/media/j;

    invoke-static {p1}, Lcom/android/athome/picker/media/k;->a(Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/media/v;

    iput-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->a:Lcom/android/athome/picker/media/v;

    sget-object v0, Lcom/android/athome/picker/am;->e:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v3

    const/4 v0, 0x2

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->e:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->e:Landroid/graphics/drawable/Drawable;

    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    iput-object v4, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->e:Landroid/graphics/drawable/Drawable;

    if-eqz v4, :cond_1

    invoke-virtual {v4, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->getDrawableState()[I

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0, v2}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    :cond_1
    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->refreshDrawableState()V

    invoke-virtual {v3, v2, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->h:I

    invoke-virtual {v3, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->i:I

    const/4 v0, 0x3

    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {p0, v1}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->setClickable(Z)V

    invoke-virtual {p0, v0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->setRouteTypes(I)V

    return-void

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/athome/picker/media/MediaRouteButtonFallback;Lcom/android/athome/picker/a/a;)Lcom/android/athome/picker/a/a;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->k:Lcom/android/athome/picker/a/a;

    return-object v0
.end method

.method private c()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->a()V

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->b()V

    return-void
.end method


# virtual methods
.method final a()V
    .locals 2

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->a:Lcom/android/athome/picker/media/v;

    iget v1, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->c:I

    invoke-virtual {v0}, Lcom/android/athome/picker/media/v;->b()Lcom/android/athome/picker/media/z;

    move-result-object v0

    iget-object v1, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->a:Lcom/android/athome/picker/media/v;

    invoke-virtual {v1}, Lcom/android/athome/picker/media/v;->a()Lcom/android/athome/picker/media/z;

    move-result-object v1

    if-eq v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iget-boolean v1, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->f:Z

    if-eq v1, v0, :cond_0

    iput-boolean v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->f:Z

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->refreshDrawableState()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final b()V
    .locals 8

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->a:Lcom/android/athome/picker/media/v;

    invoke-virtual {v0}, Lcom/android/athome/picker/media/v;->e()I

    move-result v5

    move v4, v3

    move v1, v3

    :goto_0
    if-ge v4, v5, :cond_1

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->a:Lcom/android/athome/picker/media/v;

    invoke-virtual {v0, v4}, Lcom/android/athome/picker/media/v;->c(I)Lcom/android/athome/picker/media/z;

    move-result-object v0

    iget v6, v0, Lcom/android/athome/picker/media/z;->d:I

    iget v7, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->c:I

    and-int/2addr v6, v7

    if-eqz v6, :cond_4

    instance-of v6, v0, Lcom/android/athome/picker/media/y;

    if-eqz v6, :cond_0

    check-cast v0, Lcom/android/athome/picker/media/y;

    iget-object v0, v0, Lcom/android/athome/picker/media/y;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move v1, v0

    goto :goto_0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    goto :goto_1

    :cond_1
    if-eqz v1, :cond_2

    move v0, v2

    :goto_2
    invoke-virtual {p0, v0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->setEnabled(Z)V

    const/4 v0, 0x2

    if-ne v1, v0, :cond_3

    iget v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->c:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_3

    :goto_3
    iput-boolean v2, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->g:Z

    return-void

    :cond_2
    move v0, v3

    goto :goto_2

    :cond_3
    move v2, v3

    goto :goto_3

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method protected drawableStateChanged()V
    .locals 2

    invoke-super {p0}, Landroid/view/View;->drawableStateChanged()V

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->e:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->getDrawableState()[I

    move-result-object v0

    iget-object v1, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->invalidate()V

    :cond_0
    return-void
.end method

.method public onAttachedToWindow()V
    .locals 3

    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->d:Z

    iget v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->c:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->a:Lcom/android/athome/picker/media/v;

    iget v1, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->c:I

    iget-object v2, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->b:Lcom/android/athome/picker/media/j;

    invoke-virtual {v0, v1, v2}, Lcom/android/athome/picker/media/v;->a(ILcom/android/athome/picker/media/b;)V

    invoke-direct {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->c()V

    :cond_0
    return-void
.end method

.method protected onCreateDrawableState(I)[I
    .locals 2

    add-int/lit8 v0, p1, 0x1

    invoke-super {p0, v0}, Landroid/view/View;->onCreateDrawableState(I)[I

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->f:Z

    if-eqz v1, :cond_0

    sget-object v1, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->l:[I

    invoke-static {v0, v1}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->mergeDrawableStates([I[I)[I

    :cond_0
    return-object v0
.end method

.method public onDetachedFromWindow()V
    .locals 2

    iget v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->c:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->a:Lcom/android/athome/picker/media/v;

    iget-object v1, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->b:Lcom/android/athome/picker/media/j;

    invoke-virtual {v0, v1}, Lcom/android/athome/picker/media/v;->a(Lcom/android/athome/picker/media/b;)V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->d:Z

    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->e:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->getHeight()I

    move-result v3

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->getPaddingBottom()I

    move-result v4

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    iget-object v5, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    sub-int/2addr v1, v0

    sub-int/2addr v1, v4

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    sub-int v1, v3, v2

    sub-int/2addr v1, v5

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->e:Landroid/graphics/drawable/Drawable;

    add-int v3, v0, v4

    add-int v4, v1, v5

    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 8

    const/4 v1, 0x0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v4

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v5

    iget v6, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->h:I

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->e:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    :goto_0
    invoke-static {v6, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget v6, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->i:I

    iget-object v7, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->e:Landroid/graphics/drawable/Drawable;

    if-eqz v7, :cond_0

    iget-object v1, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    :cond_0
    invoke-static {v6, v1}, Ljava/lang/Math;->max(II)I

    move-result v6

    sparse-switch v4, :sswitch_data_0

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->getPaddingLeft()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    move v1, v0

    :goto_1
    sparse-switch v5, :sswitch_data_1

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->getPaddingTop()I

    move-result v0

    add-int/2addr v0, v6

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->getPaddingBottom()I

    move-result v2

    add-int/2addr v0, v2

    :goto_2
    invoke-virtual {p0, v1, v0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->setMeasuredDimension(II)V

    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :sswitch_0
    move v1, v3

    goto :goto_1

    :sswitch_1
    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->getPaddingLeft()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    move v1, v0

    goto :goto_1

    :sswitch_2
    move v0, v2

    goto :goto_2

    :sswitch_3
    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->getPaddingTop()I

    move-result v0

    add-int/2addr v0, v6

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->getPaddingBottom()I

    move-result v3

    add-int/2addr v0, v3

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x40000000 -> :sswitch_0
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        -0x80000000 -> :sswitch_3
        0x40000000 -> :sswitch_2
    .end sparse-switch
.end method

.method public performClick()Z
    .locals 6

    const/4 v0, 0x0

    invoke-super {p0}, Landroid/view/View;->performClick()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->playSoundEffect(I)V

    :cond_0
    iget-boolean v2, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->g:Z

    if-eqz v2, :cond_4

    iget-boolean v2, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->f:Z

    if-eqz v2, :cond_2

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->a:Lcom/android/athome/picker/media/v;

    iget v2, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->c:I

    iget-object v3, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->a:Lcom/android/athome/picker/media/v;

    invoke-virtual {v3}, Lcom/android/athome/picker/media/v;->a()Lcom/android/athome/picker/media/z;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/android/athome/picker/media/v;->a(ILcom/android/athome/picker/media/z;)V

    :cond_1
    :goto_0
    return v1

    :cond_2
    iget-object v2, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->a:Lcom/android/athome/picker/media/v;

    invoke-virtual {v2}, Lcom/android/athome/picker/media/v;->e()I

    move-result v2

    :goto_1
    if-ge v0, v2, :cond_1

    iget-object v3, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->a:Lcom/android/athome/picker/media/v;

    invoke-virtual {v3, v0}, Lcom/android/athome/picker/media/v;->c(I)Lcom/android/athome/picker/media/z;

    move-result-object v3

    iget v4, v3, Lcom/android/athome/picker/media/z;->d:I

    iget v5, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->c:I

    and-int/2addr v4, v5

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->a:Lcom/android/athome/picker/media/v;

    invoke-virtual {v4}, Lcom/android/athome/picker/media/v;->a()Lcom/android/athome/picker/media/z;

    move-result-object v4

    if-eq v3, v4, :cond_3

    iget-object v4, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->a:Lcom/android/athome/picker/media/v;

    iget v5, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->c:I

    invoke-virtual {v4, v5, v3}, Lcom/android/athome/picker/media/v;->a(ILcom/android/athome/picker/media/z;)V

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->getContext()Landroid/content/Context;

    move-result-object v0

    :goto_2
    instance-of v2, v0, Landroid/content/ContextWrapper;

    if-eqz v2, :cond_5

    instance-of v2, v0, Landroid/app/Activity;

    if-nez v2, :cond_5

    check-cast v0, Landroid/content/ContextWrapper;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    goto :goto_2

    :cond_5
    instance-of v2, v0, Landroid/app/Activity;

    if-nez v2, :cond_6

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The MediaRouteButton\'s Context is not an Activity."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    check-cast v0, Landroid/app/Activity;

    instance-of v2, v0, Landroid/support/v4/app/FragmentActivity;

    if-eqz v2, :cond_8

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->d()Landroid/support/v4/app/l;

    move-result-object v2

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->k:Lcom/android/athome/picker/a/a;

    if-nez v0, :cond_7

    const-string v0, "android:MediaRouteChooserDialogFragment"

    invoke-virtual {v2, v0}, Landroid/support/v4/app/l;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/a/a;

    iput-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->k:Lcom/android/athome/picker/a/a;

    :cond_7
    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->k:Lcom/android/athome/picker/a/a;

    if-eqz v0, :cond_9

    const-string v0, "MediaRouteButtonFallback"

    const-string v2, "showDialog(): Already showing!"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_8
    const-string v0, "MediaRouteButtonFallback"

    const-string v2, "fragments not supported by the activity."

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_9
    new-instance v0, Lcom/android/athome/picker/a/a;

    invoke-direct {v0}, Lcom/android/athome/picker/a/a;-><init>()V

    iput-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->k:Lcom/android/athome/picker/a/a;

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->k:Lcom/android/athome/picker/a/a;

    iget-object v3, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->j:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Lcom/android/athome/picker/a/a;->a(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->k:Lcom/android/athome/picker/a/a;

    new-instance v3, Lcom/android/athome/picker/media/i;

    invoke-direct {v3, p0}, Lcom/android/athome/picker/media/i;-><init>(Lcom/android/athome/picker/media/MediaRouteButtonFallback;)V

    invoke-virtual {v0, v3}, Lcom/android/athome/picker/a/a;->a(Lcom/android/athome/picker/a/b;)V

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->k:Lcom/android/athome/picker/a/a;

    iget v3, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->c:I

    invoke-virtual {v0, v3}, Lcom/android/athome/picker/a/a;->b(I)V

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->k:Lcom/android/athome/picker/a/a;

    const-string v3, "android:MediaRouteChooserDialogFragment"

    invoke-virtual {v0, v2, v3}, Lcom/android/athome/picker/a/a;->a(Landroid/support/v4/app/l;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public setExtendedSettingsClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    iput-object p1, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->j:Landroid/view/View$OnClickListener;

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->k:Lcom/android/athome/picker/a/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->k:Lcom/android/athome/picker/a/a;

    invoke-virtual {v0, p1}, Lcom/android/athome/picker/a/a;->a(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method public setRouteTypes(I)V
    .locals 2

    iget v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->c:I

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->d:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->c:I

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->a:Lcom/android/athome/picker/media/v;

    iget-object v1, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->b:Lcom/android/athome/picker/media/j;

    invoke-virtual {v0, v1}, Lcom/android/athome/picker/media/v;->a(Lcom/android/athome/picker/media/b;)V

    :cond_2
    iput p1, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->c:I

    iget-boolean v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->d:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->c()V

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->a:Lcom/android/athome/picker/media/v;

    iget-object v1, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->b:Lcom/android/athome/picker/media/j;

    invoke-virtual {v0, p1, v1}, Lcom/android/athome/picker/media/v;->a(ILcom/android/athome/picker/media/b;)V

    goto :goto_0
.end method

.method public setVisibility(I)V
    .locals 3

    const/4 v1, 0x0

    invoke-super {p0, p1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->e:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    invoke-super {p0, p1}, Landroid/view/View;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/media/MediaRouteButtonFallback;->e:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
