.class public final Lcom/android/athome/picker/media/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/android/athome/picker/ac;


# static fields
.field static final a:Ljava/util/HashMap;


# instance fields
.field private b:Landroid/content/Context;

.field private c:Landroid/media/AudioManager;

.field private final d:Ljava/util/concurrent/CopyOnWriteArrayList;

.field private final e:Ljava/util/ArrayList;

.field private final f:Ljava/util/ArrayList;

.field private final g:Lcom/android/athome/picker/media/x;

.field private h:Lcom/android/athome/picker/media/z;

.field private i:Lcom/android/athome/picker/media/z;

.field private j:Lcom/android/athome/picker/q;

.field private final k:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/athome/picker/media/v;->a:Ljava/util/HashMap;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 4

    const/4 v3, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/athome/picker/media/v;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/athome/picker/media/v;->e:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/athome/picker/media/v;->f:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/athome/picker/media/v;->k:Ljava/util/HashMap;

    iput-object p1, p0, Lcom/android/athome/picker/media/v;->b:Landroid/content/Context;

    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/android/athome/picker/media/v;->c:Landroid/media/AudioManager;

    new-instance v0, Lcom/android/athome/picker/media/x;

    iget-object v1, p0, Lcom/android/athome/picker/media/v;->b:Landroid/content/Context;

    sget v2, Lcom/android/athome/picker/ak;->b:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v3, v2}, Lcom/android/athome/picker/media/x;-><init>(Lcom/android/athome/picker/media/v;Ljava/lang/CharSequence;IZ)V

    iput-object v0, p0, Lcom/android/athome/picker/media/v;->g:Lcom/android/athome/picker/media/x;

    new-instance v0, Lcom/android/athome/picker/media/z;

    iget-object v1, p0, Lcom/android/athome/picker/media/v;->g:Lcom/android/athome/picker/media/x;

    invoke-direct {v0, p0, v1}, Lcom/android/athome/picker/media/z;-><init>(Lcom/android/athome/picker/media/v;Lcom/android/athome/picker/media/x;)V

    iput-object v0, p0, Lcom/android/athome/picker/media/v;->h:Lcom/android/athome/picker/media/z;

    iget-object v0, p0, Lcom/android/athome/picker/media/v;->h:Lcom/android/athome/picker/media/z;

    iget-object v1, p0, Lcom/android/athome/picker/media/v;->b:Landroid/content/Context;

    sget v2, Lcom/android/athome/picker/ak;->a:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v0, Lcom/android/athome/picker/media/z;->c:Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/android/athome/picker/media/v;->h:Lcom/android/athome/picker/media/z;

    iput v3, v0, Lcom/android/athome/picker/media/z;->d:I

    iget-object v0, p0, Lcom/android/athome/picker/media/v;->h:Lcom/android/athome/picker/media/z;

    iput v3, v0, Lcom/android/athome/picker/media/z;->l:I

    iget-object v0, p0, Lcom/android/athome/picker/media/v;->h:Lcom/android/athome/picker/media/z;

    invoke-direct {p0, v0}, Lcom/android/athome/picker/media/v;->d(Lcom/android/athome/picker/media/z;)V

    new-instance v0, Lcom/android/athome/picker/q;

    invoke-direct {v0}, Lcom/android/athome/picker/q;-><init>()V

    iput-object v0, p0, Lcom/android/athome/picker/media/v;->j:Lcom/android/athome/picker/q;

    return-void
.end method

.method static synthetic a(Lcom/android/athome/picker/media/v;)Landroid/media/AudioManager;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/media/v;->c:Landroid/media/AudioManager;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Lcom/android/athome/picker/media/v;
    .locals 3

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sget-object v0, Lcom/android/athome/picker/media/v;->a:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/athome/picker/media/v;

    invoke-direct {v0, v1}, Lcom/android/athome/picker/media/v;-><init>(Landroid/content/Context;)V

    sget-object v2, Lcom/android/athome/picker/media/v;->a:Ljava/util/HashMap;

    invoke-virtual {v2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/android/athome/picker/media/v;->a:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/media/v;

    goto :goto_0
.end method

.method static a(I)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    and-int/lit8 v1, p0, 0x1

    if-eqz v1, :cond_0

    const-string v1, "ROUTE_TYPE_LIVE_AUDIO "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    const/high16 v1, 0x800000

    and-int/2addr v1, p0

    if-eqz v1, :cond_1

    const-string v1, "ROUTE_TYPE_USER "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/android/athome/picker/media/v;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/media/v;->e:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static c()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const-string v0, "MediaRouterFallback"

    const-string v1, "showPicker() in MediaRouterFallback is obsolete. Use MediaRouteButton instead."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private c(Lcom/android/athome/picker/MediaOutput;)V
    .locals 5

    invoke-direct {p0, p1}, Lcom/android/athome/picker/media/v;->d(Lcom/android/athome/picker/MediaOutput;)Lcom/android/athome/picker/media/z;

    move-result-object v1

    if-eqz v1, :cond_0

    if-eqz p1, :cond_1

    new-instance v0, Lcom/android/athome/picker/an;

    invoke-virtual {p1}, Lcom/android/athome/picker/MediaOutput;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/android/athome/picker/MediaOutput;->getVolume()F

    move-result v3

    invoke-virtual {p1}, Lcom/android/athome/picker/MediaOutput;->getIsMuted()Z

    move-result v4

    invoke-direct {v0, v2, v3, v4}, Lcom/android/athome/picker/an;-><init>(Ljava/lang/String;FZ)V

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v1, v0}, Lcom/android/athome/picker/media/z;->a(Ljava/lang/Object;)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(Lcom/android/athome/picker/MediaOutput;)Lcom/android/athome/picker/media/z;
    .locals 3

    iget-object v0, p0, Lcom/android/athome/picker/media/v;->k:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    if-ne v2, p1, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/media/z;

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(Lcom/android/athome/picker/media/z;)V
    .locals 3

    iget-object v0, p1, Lcom/android/athome/picker/media/z;->f:Lcom/android/athome/picker/media/x;

    iget-object v1, p0, Lcom/android/athome/picker/media/v;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/athome/picker/media/v;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v1, p0, Lcom/android/athome/picker/media/v;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    iget-boolean v0, v0, Lcom/android/athome/picker/media/x;->c:Z

    if-eqz v0, :cond_2

    instance-of v0, p1, Lcom/android/athome/picker/media/y;

    if-nez v0, :cond_2

    new-instance v0, Lcom/android/athome/picker/media/y;

    iget-object v2, p1, Lcom/android/athome/picker/media/z;->f:Lcom/android/athome/picker/media/x;

    invoke-direct {v0, p0, v2}, Lcom/android/athome/picker/media/y;-><init>(Lcom/android/athome/picker/media/v;Lcom/android/athome/picker/media/x;)V

    iget-object v2, p0, Lcom/android/athome/picker/media/v;->e:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0, v0}, Lcom/android/athome/picker/media/v;->e(Lcom/android/athome/picker/media/z;)V

    invoke-virtual {v0, p1}, Lcom/android/athome/picker/media/y;->a(Lcom/android/athome/picker/media/z;)V

    move-object p1, v0

    :goto_0
    if-eqz v1, :cond_1

    iget v0, p1, Lcom/android/athome/picker/media/z;->d:I

    invoke-virtual {p0, v0, p1}, Lcom/android/athome/picker/media/v;->a(ILcom/android/athome/picker/media/z;)V

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/athome/picker/media/v;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0, p1}, Lcom/android/athome/picker/media/v;->e(Lcom/android/athome/picker/media/z;)V

    goto :goto_0
.end method

.method private e(Lcom/android/athome/picker/media/z;)V
    .locals 4

    iget-object v0, p0, Lcom/android/athome/picker/media/v;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/media/w;

    iget v2, v0, Lcom/android/athome/picker/media/w;->a:I

    iget v3, p1, Lcom/android/athome/picker/media/z;->d:I

    and-int/2addr v2, v3

    if-eqz v2, :cond_0

    iget-object v0, v0, Lcom/android/athome/picker/media/w;->b:Lcom/android/athome/picker/media/b;

    invoke-virtual {v0, p1}, Lcom/android/athome/picker/media/b;->a(Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Lcom/android/athome/picker/media/x;)Lcom/android/athome/picker/media/aa;
    .locals 1

    new-instance v0, Lcom/android/athome/picker/media/aa;

    invoke-direct {v0, p0, p1}, Lcom/android/athome/picker/media/aa;-><init>(Lcom/android/athome/picker/media/v;Lcom/android/athome/picker/media/x;)V

    return-object v0
.end method

.method public final a(Ljava/lang/CharSequence;Z)Lcom/android/athome/picker/media/x;
    .locals 2

    new-instance v0, Lcom/android/athome/picker/media/x;

    const/high16 v1, 0x800000

    invoke-direct {v0, p0, p1, v1, p2}, Lcom/android/athome/picker/media/x;-><init>(Lcom/android/athome/picker/media/v;Ljava/lang/CharSequence;IZ)V

    return-object v0
.end method

.method public final a()Lcom/android/athome/picker/media/z;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/media/v;->h:Lcom/android/athome/picker/media/z;

    return-object v0
.end method

.method public final a(ILcom/android/athome/picker/media/b;)V
    .locals 4

    iget-object v0, p0, Lcom/android/athome/picker/media/v;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, Lcom/android/athome/picker/media/v;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/media/w;

    iget-object v3, v0, Lcom/android/athome/picker/media/w;->b:Lcom/android/athome/picker/media/b;

    if-ne v3, p2, :cond_0

    iget v1, v0, Lcom/android/athome/picker/media/w;->a:I

    and-int/2addr v1, p1

    iput v1, v0, Lcom/android/athome/picker/media/w;->a:I

    :goto_1
    return-void

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/athome/picker/media/v;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v1, Lcom/android/athome/picker/media/w;

    invoke-direct {v1, p2, p1}, Lcom/android/athome/picker/media/w;-><init>(Lcom/android/athome/picker/media/b;I)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public final a(ILcom/android/athome/picker/media/z;)V
    .locals 5

    iget-object v0, p0, Lcom/android/athome/picker/media/v;->i:Lcom/android/athome/picker/media/z;

    if-ne v0, p2, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/athome/picker/media/v;->i:Lcom/android/athome/picker/media/z;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/athome/picker/media/v;->i:Lcom/android/athome/picker/media/z;

    iget v0, v0, Lcom/android/athome/picker/media/z;->d:I

    and-int v1, p1, v0

    iget-object v2, p0, Lcom/android/athome/picker/media/v;->i:Lcom/android/athome/picker/media/z;

    iget-object v0, p0, Lcom/android/athome/picker/media/v;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/media/w;

    iget v4, v0, Lcom/android/athome/picker/media/w;->a:I

    and-int/2addr v4, v1

    if-eqz v4, :cond_2

    iget-object v0, v0, Lcom/android/athome/picker/media/w;->b:Lcom/android/athome/picker/media/b;

    invoke-virtual {v0, v1, v2}, Lcom/android/athome/picker/media/b;->a(ILjava/lang/Object;)V

    goto :goto_0

    :cond_3
    iput-object p2, p0, Lcom/android/athome/picker/media/v;->i:Lcom/android/athome/picker/media/z;

    if-eqz p2, :cond_0

    iget v0, p2, Lcom/android/athome/picker/media/z;->d:I

    and-int v1, p1, v0

    iget-object v0, p0, Lcom/android/athome/picker/media/v;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/media/w;

    iget v3, v0, Lcom/android/athome/picker/media/w;->a:I

    and-int/2addr v3, v1

    if-eqz v3, :cond_4

    iget-object v0, v0, Lcom/android/athome/picker/media/w;->b:Lcom/android/athome/picker/media/b;

    invoke-virtual {v0, p0, v1, p2}, Lcom/android/athome/picker/media/b;->a(Ljava/lang/Object;ILjava/lang/Object;)V

    goto :goto_1
.end method

.method public final a(Lcom/android/athome/picker/MediaOutput;)V
    .locals 3

    const-string v0, "MediaRouterFallback"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "OnVolumeChanged: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    instance-of v0, p1, Lcom/android/athome/picker/MediaOutputGroup;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/android/athome/picker/MediaOutputGroup;

    invoke-virtual {p1}, Lcom/android/athome/picker/MediaOutputGroup;->getMediaOutputs()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/MediaOutput;

    invoke-direct {p0, v0}, Lcom/android/athome/picker/media/v;->c(Lcom/android/athome/picker/MediaOutput;)V

    goto :goto_0

    :cond_0
    invoke-direct {p0, p1}, Lcom/android/athome/picker/media/v;->c(Lcom/android/athome/picker/MediaOutput;)V

    :cond_1
    return-void
.end method

.method public final a(Lcom/android/athome/picker/MediaOutput;Lcom/android/athome/picker/MediaOutputGroup;)V
    .locals 5

    iget-object v0, p0, Lcom/android/athome/picker/media/v;->k:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/athome/picker/media/v;->k:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lcom/android/athome/picker/media/v;->d(Lcom/android/athome/picker/MediaOutput;)Lcom/android/athome/picker/media/z;

    move-result-object v1

    iget-object v0, v1, Lcom/android/athome/picker/media/z;->e:Lcom/android/athome/picker/media/y;

    if-eqz v0, :cond_0

    invoke-virtual {v0, v1}, Lcom/android/athome/picker/media/y;->b(Lcom/android/athome/picker/media/z;)V

    :cond_0
    invoke-direct {p0, p2}, Lcom/android/athome/picker/media/v;->d(Lcom/android/athome/picker/MediaOutput;)Lcom/android/athome/picker/media/z;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/media/y;

    const-string v2, "MediaRouterFallback"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Add route: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to group: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0, v1}, Lcom/android/athome/picker/media/y;->a(Lcom/android/athome/picker/media/z;)V

    :cond_1
    return-void
.end method

.method public final a(Lcom/android/athome/picker/media/aa;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/athome/picker/media/v;->d(Lcom/android/athome/picker/media/z;)V

    return-void
.end method

.method public final a(Lcom/android/athome/picker/media/b;)V
    .locals 3

    iget-object v0, p0, Lcom/android/athome/picker/media/v;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, Lcom/android/athome/picker/media/v;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/media/w;

    iget-object v0, v0, Lcom/android/athome/picker/media/w;->b:Lcom/android/athome/picker/media/b;

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/media/v;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(I)Ljava/lang/Object;

    :goto_1
    return-void

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const-string v0, "MediaRouterFallback"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "removeCallback("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "): callback not registered"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method final a(Lcom/android/athome/picker/media/z;)V
    .locals 5

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/athome/picker/media/v;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/athome/picker/media/v;->k:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/media/v;->j:Lcom/android/athome/picker/q;

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/android/athome/picker/media/v;->j:Lcom/android/athome/picker/q;

    iget-object v0, p0, Lcom/android/athome/picker/media/v;->k:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {v2, v0}, Lcom/android/athome/picker/q;->a(Lcom/android/athome/picker/MediaOutput;)V

    :cond_0
    iget-object v3, p1, Lcom/android/athome/picker/media/z;->f:Lcom/android/athome/picker/media/x;

    iget-object v0, p0, Lcom/android/athome/picker/media/v;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_6

    iget-object v0, p0, Lcom/android/athome/picker/media/v;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/media/z;

    iget-object v0, v0, Lcom/android/athome/picker/media/z;->f:Lcom/android/athome/picker/media/x;

    if-ne v3, v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    iget-object v1, p0, Lcom/android/athome/picker/media/v;->i:Lcom/android/athome/picker/media/z;

    if-ne p1, v1, :cond_1

    const v1, 0x800001

    iget-object v2, p0, Lcom/android/athome/picker/media/v;->h:Lcom/android/athome/picker/media/z;

    invoke-virtual {p0, v1, v2}, Lcom/android/athome/picker/media/v;->a(ILcom/android/athome/picker/media/z;)V

    :cond_1
    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/athome/picker/media/v;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_2
    iget-object v0, p0, Lcom/android/athome/picker/media/v;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/media/w;

    iget v2, v0, Lcom/android/athome/picker/media/w;->a:I

    iget v3, p1, Lcom/android/athome/picker/media/z;->d:I

    and-int/2addr v2, v3

    if-eqz v2, :cond_3

    iget-object v0, v0, Lcom/android/athome/picker/media/w;->b:Lcom/android/athome/picker/media/b;

    invoke-virtual {v0, p1}, Lcom/android/athome/picker/media/b;->b(Ljava/lang/Object;)V

    goto :goto_2

    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_5
    return-void

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method final a(Lcom/android/athome/picker/media/z;Lcom/android/athome/picker/media/y;)V
    .locals 4

    iget-object v0, p0, Lcom/android/athome/picker/media/v;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/media/w;

    iget v2, v0, Lcom/android/athome/picker/media/w;->a:I

    iget v3, p2, Lcom/android/athome/picker/media/y;->d:I

    and-int/2addr v2, v3

    if-eqz v2, :cond_0

    iget-object v0, v0, Lcom/android/athome/picker/media/w;->b:Lcom/android/athome/picker/media/b;

    invoke-virtual {v0, p1, p2}, Lcom/android/athome/picker/media/b;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method final a(Lcom/android/athome/picker/media/z;Lcom/android/athome/picker/media/y;I)V
    .locals 4

    iget-object v0, p0, Lcom/android/athome/picker/media/v;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/media/w;

    iget v2, v0, Lcom/android/athome/picker/media/w;->a:I

    iget v3, p2, Lcom/android/athome/picker/media/y;->d:I

    and-int/2addr v2, v3

    if-eqz v2, :cond_0

    iget-object v0, v0, Lcom/android/athome/picker/media/w;->b:Lcom/android/athome/picker/media/b;

    invoke-virtual {v0, p1, p2}, Lcom/android/athome/picker/media/b;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final b(I)Lcom/android/athome/picker/media/x;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/media/v;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/media/x;

    return-object v0
.end method

.method public final b()Lcom/android/athome/picker/media/z;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/media/v;->i:Lcom/android/athome/picker/media/z;

    return-object v0
.end method

.method public final b(Lcom/android/athome/picker/MediaOutput;)V
    .locals 4

    iget-object v0, p0, Lcom/android/athome/picker/media/v;->k:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/android/athome/picker/media/v;->d(Lcom/android/athome/picker/MediaOutput;)Lcom/android/athome/picker/media/z;

    move-result-object v0

    const-string v1, "MediaRouterFallback"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Selected route: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " tag: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/android/athome/picker/media/z;->e()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v1, v0, Lcom/android/athome/picker/media/z;->d:I

    invoke-virtual {p0, v1, v0}, Lcom/android/athome/picker/media/v;->a(ILcom/android/athome/picker/media/z;)V

    :cond_0
    return-void
.end method

.method public final b(Lcom/android/athome/picker/MediaOutput;Lcom/android/athome/picker/MediaOutputGroup;)V
    .locals 5

    iget-object v0, p0, Lcom/android/athome/picker/media/v;->k:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/media/v;->k:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/android/athome/picker/media/v;->d(Lcom/android/athome/picker/MediaOutput;)Lcom/android/athome/picker/media/z;

    move-result-object v1

    invoke-direct {p0, p2}, Lcom/android/athome/picker/media/v;->d(Lcom/android/athome/picker/MediaOutput;)Lcom/android/athome/picker/media/z;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/media/y;

    const-string v2, "MediaRouterFallback"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Remove route: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " from group: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0, v1}, Lcom/android/athome/picker/media/y;->b(Lcom/android/athome/picker/media/z;)V

    :cond_0
    return-void
.end method

.method public final b(Lcom/android/athome/picker/media/aa;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/android/athome/picker/media/v;->a(Lcom/android/athome/picker/media/z;)V

    return-void
.end method

.method final b(Lcom/android/athome/picker/media/z;)V
    .locals 4

    iget-object v0, p0, Lcom/android/athome/picker/media/v;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/media/w;

    iget v2, v0, Lcom/android/athome/picker/media/w;->a:I

    iget v3, p1, Lcom/android/athome/picker/media/z;->d:I

    and-int/2addr v2, v3

    if-eqz v2, :cond_0

    iget-object v0, v0, Lcom/android/athome/picker/media/w;->b:Lcom/android/athome/picker/media/b;

    invoke-virtual {v0, p1}, Lcom/android/athome/picker/media/b;->c(Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final c(I)Lcom/android/athome/picker/media/z;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/media/v;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/media/z;

    return-object v0
.end method

.method final c(Lcom/android/athome/picker/media/z;)V
    .locals 4

    iget-object v0, p0, Lcom/android/athome/picker/media/v;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/media/w;

    iget v2, v0, Lcom/android/athome/picker/media/w;->a:I

    iget v3, p1, Lcom/android/athome/picker/media/z;->d:I

    and-int/2addr v2, v3

    if-eqz v2, :cond_0

    iget-object v0, v0, Lcom/android/athome/picker/media/w;->b:Lcom/android/athome/picker/media/b;

    invoke-virtual {v0}, Lcom/android/athome/picker/media/b;->a()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final d()I
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/media/v;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final e()I
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/media/v;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
