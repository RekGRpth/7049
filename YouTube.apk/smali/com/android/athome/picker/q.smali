.class public final Lcom/android/athome/picker/q;
.super Landroid/support/v4/app/d;
.source "SourceFile"


# static fields
.field private static aB:Ljava/util/Comparator;


# instance fields
.field private Y:I

.field private Z:Ljava/util/HashMap;

.field private aA:Lcom/android/athome/picker/media/c;

.field private aC:Lcom/android/athome/picker/d;

.field private aD:Lcom/android/athome/picker/n;

.field private aa:Ljava/util/HashMap;

.field private ab:Lcom/android/athome/picker/g;

.field private ac:Ljava/util/HashMap;

.field private ad:Lcom/android/athome/picker/MediaOutput;

.field private ae:Landroid/widget/ImageView;

.field private af:Landroid/widget/ImageView;

.field private ag:Landroid/widget/SeekBar;

.field private ah:Landroid/graphics/drawable/LevelListDrawable;

.field private ai:Landroid/widget/ListView;

.field private aj:Landroid/widget/ListView;

.field private ak:Landroid/view/ViewGroup;

.field private al:Landroid/widget/ListView;

.field private am:Landroid/widget/ListView;

.field private an:Landroid/widget/Button;

.field private ao:Landroid/view/ViewGroup;

.field private ap:Landroid/view/ViewGroup;

.field private aq:Landroid/view/ViewGroup;

.field private ar:Landroid/widget/ListView;

.field private as:Landroid/widget/ListView;

.field private at:Landroid/view/ViewGroup;

.field private au:Landroid/view/ViewGroup;

.field private av:Landroid/content/Context;

.field private aw:Landroid/view/View;

.field private ax:Lcom/android/athome/picker/ac;

.field private ay:I

.field private az:Landroid/app/AlertDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/athome/picker/z;

    invoke-direct {v0}, Lcom/android/athome/picker/z;-><init>()V

    sput-object v0, Lcom/android/athome/picker/q;->aB:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/support/v4/app/d;-><init>()V

    iput-object v0, p0, Lcom/android/athome/picker/q;->Z:Ljava/util/HashMap;

    iput-object v0, p0, Lcom/android/athome/picker/q;->aa:Ljava/util/HashMap;

    iput-object v0, p0, Lcom/android/athome/picker/q;->ab:Lcom/android/athome/picker/g;

    iput-object v0, p0, Lcom/android/athome/picker/q;->ac:Ljava/util/HashMap;

    new-instance v0, Lcom/android/athome/picker/r;

    invoke-direct {v0, p0}, Lcom/android/athome/picker/r;-><init>(Lcom/android/athome/picker/q;)V

    iput-object v0, p0, Lcom/android/athome/picker/q;->aA:Lcom/android/athome/picker/media/c;

    new-instance v0, Lcom/android/athome/picker/aa;

    invoke-direct {v0, p0}, Lcom/android/athome/picker/aa;-><init>(Lcom/android/athome/picker/q;)V

    iput-object v0, p0, Lcom/android/athome/picker/q;->aC:Lcom/android/athome/picker/d;

    new-instance v0, Lcom/android/athome/picker/s;

    invoke-direct {v0, p0}, Lcom/android/athome/picker/s;-><init>(Lcom/android/athome/picker/q;)V

    iput-object v0, p0, Lcom/android/athome/picker/q;->aD:Lcom/android/athome/picker/n;

    return-void
.end method

.method private D()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/android/athome/picker/q;->H()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/athome/picker/q;->aa:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/android/athome/picker/q;->ad:Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {v1}, Lcom/android/athome/picker/MediaOutput;->getType()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/a;

    invoke-virtual {v0, v2}, Lcom/android/athome/picker/a;->a(Lcom/android/athome/picker/MediaOutputGroup;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/athome/picker/q;->ad:Lcom/android/athome/picker/MediaOutput;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/q;->Z:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/android/athome/picker/q;->ad:Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {v1}, Lcom/android/athome/picker/MediaOutput;->getType()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/g;

    invoke-virtual {v0, v2}, Lcom/android/athome/picker/g;->a(Lcom/android/athome/picker/MediaOutput;)V

    goto :goto_0
.end method

.method private E()V
    .locals 7

    const/16 v6, 0x3e8

    const/4 v2, 0x2

    const/16 v5, 0x8

    const/4 v4, 0x0

    iget v0, p0, Lcom/android/athome/picker/q;->Y:I

    packed-switch v0, :pswitch_data_0

    const-string v0, "MediaOutputSelector"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported UI mode"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/android/athome/picker/q;->Y:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0}, Lcom/android/athome/picker/q;->F()V

    iget-object v0, p0, Lcom/android/athome/picker/q;->at:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/athome/picker/q;->Z:Ljava/util/HashMap;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/q;->au:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/athome/picker/q;->ai:Landroid/widget/ListView;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setVisibility(I)V

    :goto_1
    iget-object v0, p0, Lcom/android/athome/picker/q;->ac:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/athome/picker/q;->ac:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    const-string v1, "MediaOutputSelector"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknow section UI mode: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    iget-object v0, p0, Lcom/android/athome/picker/q;->ac:Ljava/util/HashMap;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/athome/picker/q;->ac:Ljava/util/HashMap;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_2

    const-string v1, "MediaOutputSelector"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknow section UI mode: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/athome/picker/q;->au:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/athome/picker/q;->ai:Landroid/widget/ListView;

    invoke-virtual {v0, v5}, Landroid/widget/ListView;->setVisibility(I)V

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lcom/android/athome/picker/q;->ak:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/athome/picker/q;->al:Landroid/widget/ListView;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/athome/picker/q;->am:Landroid/widget/ListView;

    invoke-virtual {v0, v5}, Landroid/widget/ListView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/athome/picker/q;->an:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_2

    :pswitch_2
    iget-object v0, p0, Lcom/android/athome/picker/q;->ak:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/athome/picker/q;->am:Landroid/widget/ListView;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/athome/picker/q;->al:Landroid/widget/ListView;

    invoke-virtual {v0, v5}, Landroid/widget/ListView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/athome/picker/q;->an:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_2

    :pswitch_3
    iget-object v0, p0, Lcom/android/athome/picker/q;->ak:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/athome/picker/q;->am:Landroid/widget/ListView;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/athome/picker/q;->al:Landroid/widget/ListView;

    invoke-virtual {v0, v5}, Landroid/widget/ListView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/athome/picker/q;->an:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_2

    :cond_1
    iget-object v0, p0, Lcom/android/athome/picker/q;->ak:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto/16 :goto_2

    :pswitch_4
    iget-object v0, p0, Lcom/android/athome/picker/q;->ao:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/athome/picker/q;->ap:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/athome/picker/q;->ar:Landroid/widget/ListView;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/athome/picker/q;->aq:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto/16 :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/android/athome/picker/q;->ao:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/athome/picker/q;->ap:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/athome/picker/q;->aq:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/athome/picker/q;->as:Landroid/widget/ListView;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/athome/picker/q;->ao:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto/16 :goto_0

    :pswitch_6
    invoke-direct {p0}, Lcom/android/athome/picker/q;->F()V

    iget-object v0, p0, Lcom/android/athome/picker/q;->af:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/athome/picker/q;->at:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/athome/picker/q;->ab:Lcom/android/athome/picker/g;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/athome/picker/q;->ab:Lcom/android/athome/picker/g;

    invoke-virtual {v0}, Lcom/android/athome/picker/g;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    iget-object v0, p0, Lcom/android/athome/picker/q;->aj:Landroid/widget/ListView;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setVisibility(I)V

    :goto_3
    iget-object v0, p0, Lcom/android/athome/picker/q;->au:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/athome/picker/q;->ak:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/athome/picker/q;->ao:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/athome/picker/q;->aj:Landroid/widget/ListView;

    invoke-virtual {v0, v5}, Landroid/widget/ListView;->setVisibility(I)V

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_6
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method private F()V
    .locals 6

    const/16 v2, 0x65

    const/4 v3, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/athome/picker/q;->ad:Lcom/android/athome/picker/MediaOutput;

    if-nez v0, :cond_1

    iput v1, p0, Lcom/android/athome/picker/q;->ay:I

    iget-object v0, p0, Lcom/android/athome/picker/q;->ae:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/athome/picker/q;->ah:Landroid/graphics/drawable/LevelListDrawable;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/LevelListDrawable;->setLevel(I)Z

    iget-object v0, p0, Lcom/android/athome/picker/q;->ag:Landroid/widget/SeekBar;

    :cond_0
    move v3, v1

    :goto_0
    invoke-virtual {v0, v3}, Landroid/widget/SeekBar;->setEnabled(Z)V

    :goto_1
    invoke-direct {p0}, Lcom/android/athome/picker/q;->G()V

    return-void

    :cond_1
    invoke-direct {p0}, Lcom/android/athome/picker/q;->I()Lcom/android/athome/picker/MediaOutputGroup;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/android/athome/picker/MediaOutputGroup;->getMediaOutputs()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-le v4, v3, :cond_5

    invoke-virtual {v0}, Lcom/android/athome/picker/MediaOutputGroup;->getIsMuted()Z

    move-result v4

    const/4 v5, 0x2

    iput v5, p0, Lcom/android/athome/picker/q;->ay:I

    if-eqz v4, :cond_3

    move v0, v1

    :goto_2
    iget-object v5, p0, Lcom/android/athome/picker/q;->ag:Landroid/widget/SeekBar;

    invoke-virtual {v5, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v0, p0, Lcom/android/athome/picker/q;->ae:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v5, p0, Lcom/android/athome/picker/q;->ah:Landroid/graphics/drawable/LevelListDrawable;

    if-eqz v4, :cond_4

    move v0, v2

    :goto_3
    invoke-virtual {v5, v0}, Landroid/graphics/drawable/LevelListDrawable;->setLevel(I)Z

    iget-object v0, p0, Lcom/android/athome/picker/q;->ag:Landroid/widget/SeekBar;

    if-nez v4, :cond_2

    move v1, v3

    :cond_2
    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setEnabled(Z)V

    goto :goto_1

    :cond_3
    invoke-virtual {v0}, Lcom/android/athome/picker/MediaOutputGroup;->getVolume()F

    move-result v0

    iget-object v5, p0, Lcom/android/athome/picker/q;->ag:Landroid/widget/SeekBar;

    invoke-virtual {v5}, Landroid/widget/SeekBar;->getMax()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v0, v5

    float-to-int v0, v0

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/android/athome/picker/q;->ag:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    goto :goto_3

    :cond_5
    iput v3, p0, Lcom/android/athome/picker/q;->ay:I

    iget-object v0, p0, Lcom/android/athome/picker/q;->ad:Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {v0}, Lcom/android/athome/picker/MediaOutput;->getVolume()F

    move-result v0

    iget-object v4, p0, Lcom/android/athome/picker/q;->ag:Landroid/widget/SeekBar;

    iget-object v5, p0, Lcom/android/athome/picker/q;->ag:Landroid/widget/SeekBar;

    invoke-virtual {v5}, Landroid/widget/SeekBar;->getMax()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v0, v5

    float-to-int v0, v0

    invoke-virtual {v4, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v0, p0, Lcom/android/athome/picker/q;->ae:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/athome/picker/q;->ah:Landroid/graphics/drawable/LevelListDrawable;

    iget-object v4, p0, Lcom/android/athome/picker/q;->ad:Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {v4}, Lcom/android/athome/picker/MediaOutput;->getIsMuted()Z

    move-result v4

    if-eqz v4, :cond_6

    :goto_4
    invoke-virtual {v0, v2}, Landroid/graphics/drawable/LevelListDrawable;->setLevel(I)Z

    iget-object v0, p0, Lcom/android/athome/picker/q;->ag:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/android/athome/picker/q;->ad:Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {v2}, Lcom/android/athome/picker/MediaOutput;->getIsMuted()Z

    move-result v2

    if-nez v2, :cond_0

    goto/16 :goto_0

    :cond_6
    iget-object v2, p0, Lcom/android/athome/picker/q;->ag:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getProgress()I

    move-result v2

    goto :goto_4
.end method

.method private G()V
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget v2, p0, Lcom/android/athome/picker/q;->Y:I

    if-nez v2, :cond_0

    invoke-direct {p0}, Lcom/android/athome/picker/q;->I()Lcom/android/athome/picker/MediaOutputGroup;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/android/athome/picker/MediaOutputGroup;->size()I

    move-result v2

    if-le v2, v0, :cond_1

    :goto_0
    iget-object v2, p0, Lcom/android/athome/picker/q;->af:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    :goto_1
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    const/16 v1, 0x8

    goto :goto_1
.end method

.method private H()Z
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/q;->ad:Lcom/android/athome/picker/MediaOutput;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/q;->ad:Lcom/android/athome/picker/MediaOutput;

    instance-of v0, v0, Lcom/android/athome/picker/MediaOutputGroup;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private I()Lcom/android/athome/picker/MediaOutputGroup;
    .locals 1

    invoke-direct {p0}, Lcom/android/athome/picker/q;->H()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/q;->ad:Lcom/android/athome/picker/MediaOutput;

    check-cast v0, Lcom/android/athome/picker/MediaOutputGroup;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/athome/picker/q;Lcom/android/athome/picker/a;)Ljava/util/ArrayList;
    .locals 5

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p1}, Lcom/android/athome/picker/a;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_3

    invoke-virtual {p1, v1}, Lcom/android/athome/picker/a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/MediaOutputGroup;

    invoke-virtual {v0}, Lcom/android/athome/picker/MediaOutputGroup;->getMediaOutputs()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    move-object v0, v2

    goto :goto_0
.end method

.method private static a(Landroid/widget/ArrayAdapter;Ljava/util/ArrayList;)V
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/ArrayAdapter;->setNotifyOnChange(Z)V

    invoke-virtual {p0}, Landroid/widget/ArrayAdapter;->clear()V

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {p0, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/android/athome/picker/q;->aB:Ljava/util/Comparator;

    invoke-virtual {p0, v0}, Landroid/widget/ArrayAdapter;->sort(Ljava/util/Comparator;)V

    invoke-virtual {p0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method private a(Landroid/widget/ListView;Ljava/util/ArrayList;I)V
    .locals 3

    iget-object v0, p0, Lcom/android/athome/picker/q;->ac:Ljava/util/HashMap;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/high16 v0, 0x1000000

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setScrollBarStyle(I)V

    new-instance v0, Lcom/android/athome/picker/g;

    iget-object v1, p0, Lcom/android/athome/picker/q;->av:Landroid/content/Context;

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2, p3}, Lcom/android/athome/picker/g;-><init>(Landroid/content/Context;II)V

    iget-object v1, p0, Lcom/android/athome/picker/q;->Z:Ljava/util/HashMap;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0, p2}, Lcom/android/athome/picker/q;->a(Landroid/widget/ArrayAdapter;Ljava/util/ArrayList;)V

    invoke-virtual {v0, p1}, Lcom/android/athome/picker/g;->a(Landroid/widget/AdapterView;)V

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v1, Lcom/android/athome/picker/x;

    invoke-direct {v1, p0, v0}, Lcom/android/athome/picker/x;-><init>(Lcom/android/athome/picker/q;Lcom/android/athome/picker/g;)V

    invoke-virtual {p1, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method private a(Lcom/android/athome/picker/MediaOutputGroup;)V
    .locals 3

    iget v0, p0, Lcom/android/athome/picker/q;->Y:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/q;->ab:Lcom/android/athome/picker/g;

    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/android/athome/picker/MediaOutputGroup;->getMediaOutputs()Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v0, v1}, Lcom/android/athome/picker/q;->a(Landroid/widget/ArrayAdapter;Ljava/util/ArrayList;)V

    :cond_0
    return-void
.end method

.method private declared-synchronized a(Lcom/android/athome/picker/a;Lcom/android/athome/picker/MediaOutputGroup;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    const-string v0, "MediaOutputSelector"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SelectedOutputGroup: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/athome/picker/q;->D()V

    iput-object p2, p0, Lcom/android/athome/picker/q;->ad:Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {p1, p2}, Lcom/android/athome/picker/a;->a(Lcom/android/athome/picker/MediaOutputGroup;)V

    iget-object v0, p0, Lcom/android/athome/picker/q;->ax:Lcom/android/athome/picker/ac;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/q;->ax:Lcom/android/athome/picker/ac;

    invoke-interface {v0, p2}, Lcom/android/athome/picker/ac;->b(Lcom/android/athome/picker/MediaOutput;)V

    :cond_0
    invoke-direct {p0}, Lcom/android/athome/picker/q;->F()V

    invoke-virtual {p0}, Lcom/android/athome/picker/q;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Lcom/android/athome/picker/g;Lcom/android/athome/picker/MediaOutput;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    const-string v0, "MediaOutputSelector"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SelectedOutput: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/athome/picker/q;->D()V

    iput-object p2, p0, Lcom/android/athome/picker/q;->ad:Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {p1, p2}, Lcom/android/athome/picker/g;->a(Lcom/android/athome/picker/MediaOutput;)V

    iget-object v0, p0, Lcom/android/athome/picker/q;->ax:Lcom/android/athome/picker/ac;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/q;->ax:Lcom/android/athome/picker/ac;

    invoke-interface {v0, p2}, Lcom/android/athome/picker/ac;->b(Lcom/android/athome/picker/MediaOutput;)V

    :cond_0
    invoke-direct {p0}, Lcom/android/athome/picker/q;->F()V

    invoke-virtual {p0}, Lcom/android/athome/picker/q;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(Lcom/android/athome/picker/q;)V
    .locals 4

    const/4 v1, 0x0

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/android/athome/picker/q;->ad:Lcom/android/athome/picker/MediaOutput;

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/android/athome/picker/q;->ay:I

    if-ne v2, v0, :cond_3

    iget-object v2, p0, Lcom/android/athome/picker/q;->ad:Lcom/android/athome/picker/MediaOutput;

    iget-object v3, p0, Lcom/android/athome/picker/q;->ad:Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {v3}, Lcom/android/athome/picker/MediaOutput;->getIsMuted()Z

    move-result v3

    if-nez v3, :cond_2

    :goto_0
    invoke-virtual {v2, v0}, Lcom/android/athome/picker/MediaOutput;->setIsMuted(Z)V

    iget-object v0, p0, Lcom/android/athome/picker/q;->ax:Lcom/android/athome/picker/ac;

    iget-object v1, p0, Lcom/android/athome/picker/q;->ad:Lcom/android/athome/picker/MediaOutput;

    invoke-interface {v0, v1}, Lcom/android/athome/picker/ac;->a(Lcom/android/athome/picker/MediaOutput;)V

    :cond_0
    :goto_1
    invoke-direct {p0}, Lcom/android/athome/picker/q;->F()V

    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    iget v2, p0, Lcom/android/athome/picker/q;->ay:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    invoke-direct {p0}, Lcom/android/athome/picker/q;->I()Lcom/android/athome/picker/MediaOutputGroup;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/athome/picker/MediaOutputGroup;->getIsMuted()Z

    move-result v3

    if-nez v3, :cond_4

    :goto_2
    invoke-virtual {v2, v0}, Lcom/android/athome/picker/MediaOutputGroup;->setIsMuted(Z)V

    iget-object v0, p0, Lcom/android/athome/picker/q;->ax:Lcom/android/athome/picker/ac;

    invoke-interface {v0, v2}, Lcom/android/athome/picker/ac;->a(Lcom/android/athome/picker/MediaOutput;)V

    invoke-direct {p0, v2}, Lcom/android/athome/picker/q;->a(Lcom/android/athome/picker/MediaOutputGroup;)V

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method static synthetic a(Lcom/android/athome/picker/q;Landroid/widget/ArrayAdapter;Ljava/util/ArrayList;)V
    .locals 0

    invoke-static {p1, p2}, Lcom/android/athome/picker/q;->a(Landroid/widget/ArrayAdapter;Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic a(Lcom/android/athome/picker/q;Lcom/android/athome/picker/MediaOutput;)V
    .locals 4

    invoke-direct {p0}, Lcom/android/athome/picker/q;->H()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/q;->ad:Lcom/android/athome/picker/MediaOutput;

    check-cast v0, Lcom/android/athome/picker/MediaOutputGroup;

    invoke-virtual {v0, p1}, Lcom/android/athome/picker/MediaOutputGroup;->contains(Lcom/android/athome/picker/MediaOutput;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "MediaOutputSelector"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Remove "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " from group:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0, p1}, Lcom/android/athome/picker/MediaOutputGroup;->remove(Lcom/android/athome/picker/MediaOutput;)Z

    iget-object v1, p0, Lcom/android/athome/picker/q;->ax:Lcom/android/athome/picker/ac;

    invoke-interface {v1, p1, v0}, Lcom/android/athome/picker/ac;->b(Lcom/android/athome/picker/MediaOutput;Lcom/android/athome/picker/MediaOutputGroup;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "MediaOutputSelector"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Add "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to group:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0, p1}, Lcom/android/athome/picker/MediaOutputGroup;->add(Lcom/android/athome/picker/MediaOutput;)Z

    iget-object v1, p0, Lcom/android/athome/picker/q;->ax:Lcom/android/athome/picker/ac;

    invoke-interface {v1, p1, v0}, Lcom/android/athome/picker/ac;->a(Lcom/android/athome/picker/MediaOutput;Lcom/android/athome/picker/MediaOutputGroup;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/athome/picker/q;Lcom/android/athome/picker/MediaOutputGroup;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/athome/picker/q;->a(Lcom/android/athome/picker/MediaOutputGroup;)V

    return-void
.end method

.method static synthetic a(Lcom/android/athome/picker/q;Lcom/android/athome/picker/a;Lcom/android/athome/picker/MediaOutputGroup;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/athome/picker/q;->a(Lcom/android/athome/picker/a;Lcom/android/athome/picker/MediaOutputGroup;)V

    return-void
.end method

.method static synthetic a(Lcom/android/athome/picker/q;Lcom/android/athome/picker/g;Lcom/android/athome/picker/MediaOutput;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/athome/picker/q;->a(Lcom/android/athome/picker/g;Lcom/android/athome/picker/MediaOutput;)V

    return-void
.end method

.method private b(Landroid/widget/ListView;Ljava/util/ArrayList;I)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/athome/picker/q;->ac:Ljava/util/HashMap;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/high16 v0, 0x1000000

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setScrollBarStyle(I)V

    new-instance v1, Lcom/android/athome/picker/a;

    iget-object v0, p0, Lcom/android/athome/picker/q;->av:Landroid/content/Context;

    const/4 v2, -0x1

    invoke-direct {v1, v0, v2}, Lcom/android/athome/picker/a;-><init>(Landroid/content/Context;I)V

    iget-object v0, p0, Lcom/android/athome/picker/q;->aC:Lcom/android/athome/picker/d;

    invoke-virtual {v1, v0}, Lcom/android/athome/picker/a;->a(Lcom/android/athome/picker/d;)V

    iget-object v0, p0, Lcom/android/athome/picker/q;->aa:Ljava/util/HashMap;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v1, v3}, Lcom/android/athome/picker/a;->setNotifyOnChange(Z)V

    invoke-virtual {v1}, Lcom/android/athome/picker/a;->clear()V

    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/MediaOutputGroup;

    invoke-virtual {v1, v0}, Lcom/android/athome/picker/a;->add(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/android/athome/picker/q;->aB:Ljava/util/Comparator;

    invoke-virtual {v1, v0}, Lcom/android/athome/picker/a;->sort(Ljava/util/Comparator;)V

    invoke-virtual {v1}, Lcom/android/athome/picker/a;->notifyDataSetChanged()V

    invoke-virtual {v1, p1}, Lcom/android/athome/picker/a;->a(Landroid/widget/AdapterView;)V

    new-instance v0, Lcom/android/athome/picker/y;

    invoke-direct {v0, p0, v1}, Lcom/android/athome/picker/y;-><init>(Lcom/android/athome/picker/q;Lcom/android/athome/picker/a;)V

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-virtual {p1, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method static synthetic b(Lcom/android/athome/picker/q;)V
    .locals 5

    invoke-direct {p0}, Lcom/android/athome/picker/q;->I()Lcom/android/athome/picker/MediaOutputGroup;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    iput v1, p0, Lcom/android/athome/picker/q;->Y:I

    new-instance v1, Lcom/android/athome/picker/g;

    iget-object v2, p0, Lcom/android/athome/picker/q;->av:Landroid/content/Context;

    const/4 v3, -0x1

    invoke-virtual {v0}, Lcom/android/athome/picker/MediaOutputGroup;->getType()I

    move-result v4

    invoke-direct {v1, v2, v3, v4}, Lcom/android/athome/picker/g;-><init>(Landroid/content/Context;II)V

    iput-object v1, p0, Lcom/android/athome/picker/q;->ab:Lcom/android/athome/picker/g;

    iget-object v1, p0, Lcom/android/athome/picker/q;->aj:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/android/athome/picker/q;->ab:Lcom/android/athome/picker/g;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v1, p0, Lcom/android/athome/picker/q;->ab:Lcom/android/athome/picker/g;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/android/athome/picker/g;->a(I)V

    iget-object v1, p0, Lcom/android/athome/picker/q;->ab:Lcom/android/athome/picker/g;

    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/android/athome/picker/MediaOutputGroup;->getMediaOutputs()Ljava/util/List;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v1, v2}, Lcom/android/athome/picker/q;->a(Landroid/widget/ArrayAdapter;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/android/athome/picker/q;->ab:Lcom/android/athome/picker/g;

    iget-object v1, p0, Lcom/android/athome/picker/q;->aj:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Lcom/android/athome/picker/g;->a(Landroid/widget/AdapterView;)V

    iget-object v0, p0, Lcom/android/athome/picker/q;->ab:Lcom/android/athome/picker/g;

    iget-object v1, p0, Lcom/android/athome/picker/q;->aD:Lcom/android/athome/picker/n;

    invoke-virtual {v0, v1}, Lcom/android/athome/picker/g;->a(Lcom/android/athome/picker/n;)V

    invoke-direct {p0}, Lcom/android/athome/picker/q;->E()V

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/android/athome/picker/MediaOutput;)Z
    .locals 2

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/android/athome/picker/MediaOutput;->getType()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/android/athome/picker/q;)Ljava/util/HashMap;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/q;->ac:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic d(Lcom/android/athome/picker/q;)Ljava/util/HashMap;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/q;->Z:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic e(Lcom/android/athome/picker/q;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/athome/picker/q;->E()V

    return-void
.end method

.method static synthetic f(Lcom/android/athome/picker/q;)I
    .locals 1

    iget v0, p0, Lcom/android/athome/picker/q;->ay:I

    return v0
.end method

.method static synthetic g(Lcom/android/athome/picker/q;)Lcom/android/athome/picker/MediaOutputGroup;
    .locals 1

    invoke-direct {p0}, Lcom/android/athome/picker/q;->I()Lcom/android/athome/picker/MediaOutputGroup;

    move-result-object v0

    return-object v0
.end method

.method static synthetic h(Lcom/android/athome/picker/q;)Landroid/graphics/drawable/LevelListDrawable;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/q;->ah:Landroid/graphics/drawable/LevelListDrawable;

    return-object v0
.end method

.method static synthetic i(Lcom/android/athome/picker/q;)Lcom/android/athome/picker/MediaOutput;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/q;->ad:Lcom/android/athome/picker/MediaOutput;

    return-object v0
.end method

.method static synthetic j(Lcom/android/athome/picker/q;)Lcom/android/athome/picker/ac;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/q;->ax:Lcom/android/athome/picker/ac;

    return-object v0
.end method

.method static synthetic k(Lcom/android/athome/picker/q;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/athome/picker/q;->F()V

    return-void
.end method

.method static synthetic l(Lcom/android/athome/picker/q;)Ljava/util/HashMap;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/q;->aa:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic m(Lcom/android/athome/picker/q;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/q;->av:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic n(Lcom/android/athome/picker/q;)Landroid/widget/ListView;
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/q;->am:Landroid/widget/ListView;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/app/Activity;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/support/v4/app/d;->a(Landroid/app/Activity;)V

    iput-object p1, p0, Lcom/android/athome/picker/q;->av:Landroid/content/Context;

    invoke-static {p1}, Lcom/android/athome/picker/media/k;->a(Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/ac;

    iput-object v0, p0, Lcom/android/athome/picker/q;->ax:Lcom/android/athome/picker/ac;

    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/support/v4/app/d;->a(Landroid/os/Bundle;)V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/athome/picker/q;->Z:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/athome/picker/q;->aa:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/athome/picker/q;->ac:Ljava/util/HashMap;

    return-void
.end method

.method public final a(Lcom/android/athome/picker/MediaOutput;)V
    .locals 3

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/android/athome/picker/MediaOutput;->getType()I

    move-result v0

    instance-of v1, p1, Lcom/android/athome/picker/MediaOutputGroup;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/athome/picker/q;->aa:Ljava/util/HashMap;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/athome/picker/q;->aa:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/athome/picker/q;->aa:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/a;

    check-cast p1, Lcom/android/athome/picker/MediaOutputGroup;

    invoke-virtual {v0, p1}, Lcom/android/athome/picker/a;->remove(Ljava/lang/Object;)V

    invoke-virtual {v0}, Lcom/android/athome/picker/a;->notifyDataSetChanged()V

    :cond_1
    :goto_1
    invoke-direct {p0}, Lcom/android/athome/picker/q;->F()V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/athome/picker/q;->Z:Ljava/util/HashMap;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/athome/picker/q;->Z:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/athome/picker/q;->Z:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/g;

    invoke-virtual {v0, p1}, Lcom/android/athome/picker/g;->remove(Ljava/lang/Object;)V

    invoke-virtual {v0}, Lcom/android/athome/picker/g;->notifyDataSetChanged()V

    goto :goto_1
.end method

.method public final c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 10

    const/16 v9, 0x3e8

    const/high16 v8, 0x1000000

    const/4 v7, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/android/athome/picker/q;->j()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget v4, Lcom/android/athome/picker/al;->a:I

    invoke-direct {v0, v1, v4}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-direct {v4, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/view/ContextThemeWrapper;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    sget v1, Lcom/android/athome/picker/aj;->e:I

    const/4 v5, 0x0

    invoke-virtual {v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/athome/picker/q;->aw:Landroid/view/View;

    invoke-virtual {p0}, Lcom/android/athome/picker/q;->i()Landroid/os/Bundle;

    move-result-object v5

    const-string v0, "selected_output"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/MediaOutput;

    iput-object v0, p0, Lcom/android/athome/picker/q;->ad:Lcom/android/athome/picker/MediaOutput;

    const-string v0, "MediaOutputSelector"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, "SelectedOutput: "

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/android/athome/picker/q;->ad:Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_1

    const-string v0, "ui_mode"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/athome/picker/q;->Y:I

    :goto_0
    iget-object v0, p0, Lcom/android/athome/picker/q;->ad:Lcom/android/athome/picker/MediaOutput;

    if-eqz v0, :cond_2

    move v1, v2

    :goto_1
    iget-object v0, p0, Lcom/android/athome/picker/q;->aw:Landroid/view/View;

    sget v6, Lcom/android/athome/picker/ai;->n:I

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/athome/picker/q;->ae:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/athome/picker/q;->ae:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/LevelListDrawable;

    iput-object v0, p0, Lcom/android/athome/picker/q;->ah:Landroid/graphics/drawable/LevelListDrawable;

    iget-object v0, p0, Lcom/android/athome/picker/q;->ae:Landroid/widget/ImageView;

    new-instance v6, Lcom/android/athome/picker/t;

    invoke-direct {v6, p0}, Lcom/android/athome/picker/t;-><init>(Lcom/android/athome/picker/q;)V

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/athome/picker/q;->ae:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/athome/picker/q;->aw:Landroid/view/View;

    sget v6, Lcom/android/athome/picker/ai;->C:I

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/android/athome/picker/q;->ag:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/android/athome/picker/q;->ag:Landroid/widget/SeekBar;

    new-instance v6, Lcom/android/athome/picker/ad;

    invoke-direct {v6, p0, v3}, Lcom/android/athome/picker/ad;-><init>(Lcom/android/athome/picker/q;B)V

    invoke-virtual {v0, v6}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iget-object v0, p0, Lcom/android/athome/picker/q;->ag:Landroid/widget/SeekBar;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/athome/picker/q;->aw:Landroid/view/View;

    sget v1, Lcom/android/athome/picker/ai;->i:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/athome/picker/q;->af:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/athome/picker/q;->af:Landroid/widget/ImageView;

    new-instance v1, Lcom/android/athome/picker/u;

    invoke-direct {v1, p0}, Lcom/android/athome/picker/u;-><init>(Lcom/android/athome/picker/q;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/android/athome/picker/q;->G()V

    iget-object v0, p0, Lcom/android/athome/picker/q;->aw:Landroid/view/View;

    sget v1, Lcom/android/athome/picker/ai;->A:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/athome/picker/q;->at:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/android/athome/picker/q;->aw:Landroid/view/View;

    sget v1, Lcom/android/athome/picker/ai;->a:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/android/athome/picker/q;->aj:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/android/athome/picker/q;->aj:Landroid/widget/ListView;

    invoke-virtual {v0, v8}, Landroid/widget/ListView;->setScrollBarStyle(I)V

    iget-object v0, p0, Lcom/android/athome/picker/q;->aw:Landroid/view/View;

    sget v1, Lcom/android/athome/picker/ai;->u:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/athome/picker/q;->au:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/android/athome/picker/q;->aw:Landroid/view/View;

    sget v1, Lcom/android/athome/picker/ai;->t:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/android/athome/picker/q;->ai:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/android/athome/picker/q;->ai:Landroid/widget/ListView;

    invoke-virtual {v0, v8}, Landroid/widget/ListView;->setScrollBarStyle(I)V

    const-string v0, "speaker"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/android/athome/picker/q;->ac:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v1, v3, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lcom/android/athome/picker/g;

    iget-object v3, p0, Lcom/android/athome/picker/q;->av:Landroid/content/Context;

    const/4 v6, -0x1

    invoke-direct {v1, v3, v6, v2}, Lcom/android/athome/picker/g;-><init>(Landroid/content/Context;II)V

    iget-object v3, p0, Lcom/android/athome/picker/q;->Z:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v6, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v1, v0}, Lcom/android/athome/picker/q;->a(Landroid/widget/ArrayAdapter;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/android/athome/picker/q;->ai:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Lcom/android/athome/picker/g;->a(Landroid/widget/AdapterView;)V

    iget-object v0, p0, Lcom/android/athome/picker/q;->ai:Landroid/widget/ListView;

    new-instance v3, Lcom/android/athome/picker/v;

    invoke-direct {v3, p0, v1}, Lcom/android/athome/picker/v;-><init>(Lcom/android/athome/picker/q;Lcom/android/athome/picker/g;)V

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/android/athome/picker/q;->ai:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    :goto_2
    iget-object v0, p0, Lcom/android/athome/picker/q;->aw:Landroid/view/View;

    sget v1, Lcom/android/athome/picker/ai;->d:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/athome/picker/q;->ak:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/android/athome/picker/q;->aw:Landroid/view/View;

    sget v1, Lcom/android/athome/picker/ai;->l:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/athome/picker/q;->an:Landroid/widget/Button;

    iget-object v0, p0, Lcom/android/athome/picker/q;->an:Landroid/widget/Button;

    new-instance v1, Lcom/android/athome/picker/w;

    invoke-direct {v1, p0}, Lcom/android/athome/picker/w;-><init>(Lcom/android/athome/picker/q;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/athome/picker/q;->aw:Landroid/view/View;

    sget v1, Lcom/android/athome/picker/ai;->b:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/android/athome/picker/q;->al:Landroid/widget/ListView;

    const-string v0, "athome_group"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v1, p0, Lcom/android/athome/picker/q;->al:Landroid/widget/ListView;

    invoke-direct {p0, v1, v0, v7}, Lcom/android/athome/picker/q;->b(Landroid/widget/ListView;Ljava/util/ArrayList;I)V

    :goto_3
    iget-object v0, p0, Lcom/android/athome/picker/q;->aw:Landroid/view/View;

    sget v1, Lcom/android/athome/picker/ai;->c:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/android/athome/picker/q;->am:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/android/athome/picker/q;->am:Landroid/widget/ListView;

    invoke-virtual {v0, v8}, Landroid/widget/ListView;->setScrollBarStyle(I)V

    const-string v0, "Nexus Q"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v1, p0, Lcom/android/athome/picker/q;->am:Landroid/widget/ListView;

    invoke-direct {p0, v1, v0, v7}, Lcom/android/athome/picker/q;->a(Landroid/widget/ListView;Ljava/util/ArrayList;I)V

    :goto_4
    iget-object v0, p0, Lcom/android/athome/picker/q;->aw:Landroid/view/View;

    sget v1, Lcom/android/athome/picker/ai;->y:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/athome/picker/q;->ao:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/android/athome/picker/q;->aw:Landroid/view/View;

    sget v1, Lcom/android/athome/picker/ai;->w:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/athome/picker/q;->ap:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/android/athome/picker/q;->aw:Landroid/view/View;

    sget v1, Lcom/android/athome/picker/ai;->v:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/android/athome/picker/q;->ar:Landroid/widget/ListView;

    const-string v0, "user_route_group"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v1, p0, Lcom/android/athome/picker/q;->ar:Landroid/widget/ListView;

    invoke-direct {p0, v1, v0, v9}, Lcom/android/athome/picker/q;->b(Landroid/widget/ListView;Ljava/util/ArrayList;I)V

    :goto_5
    iget-object v0, p0, Lcom/android/athome/picker/q;->aw:Landroid/view/View;

    sget v1, Lcom/android/athome/picker/ai;->z:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/athome/picker/q;->aq:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/android/athome/picker/q;->aw:Landroid/view/View;

    sget v1, Lcom/android/athome/picker/ai;->x:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/android/athome/picker/q;->as:Landroid/widget/ListView;

    const-string v0, "user_route"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v1, p0, Lcom/android/athome/picker/q;->as:Landroid/widget/ListView;

    invoke-direct {p0, v1, v0, v9}, Lcom/android/athome/picker/q;->a(Landroid/widget/ListView;Ljava/util/ArrayList;I)V

    :goto_6
    invoke-direct {p0}, Lcom/android/athome/picker/q;->I()Lcom/android/athome/picker/MediaOutputGroup;

    move-result-object v1

    if-eqz v1, :cond_8

    iget-object v0, p0, Lcom/android/athome/picker/q;->aa:Ljava/util/HashMap;

    invoke-virtual {v1}, Lcom/android/athome/picker/MediaOutputGroup;->getType()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/android/athome/picker/q;->aa:Ljava/util/HashMap;

    iget-object v3, p0, Lcom/android/athome/picker/q;->ad:Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {v3}, Lcom/android/athome/picker/MediaOutput;->getType()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/a;

    invoke-virtual {v0, v1}, Lcom/android/athome/picker/a;->a(Lcom/android/athome/picker/MediaOutputGroup;)V

    :cond_0
    :goto_7
    invoke-direct {p0}, Lcom/android/athome/picker/q;->E()V

    iget-object v0, p0, Lcom/android/athome/picker/q;->aw:Landroid/view/View;

    invoke-virtual {v4, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/athome/picker/q;->az:Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/android/athome/picker/q;->az:Landroid/app/AlertDialog;

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    iget-object v0, p0, Lcom/android/athome/picker/q;->az:Landroid/app/AlertDialog;

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->requestWindowFeature(I)Z

    iget-object v0, p0, Lcom/android/athome/picker/q;->az:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x30

    invoke-virtual {v0, v1}, Landroid/view/Window;->setGravity(I)V

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/athome/picker/q;->k()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/android/athome/picker/ag;->a:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    iget-object v0, p0, Lcom/android/athome/picker/q;->az:Landroid/app/AlertDialog;

    return-object v0

    :cond_1
    iput v3, p0, Lcom/android/athome/picker/q;->Y:I

    goto/16 :goto_0

    :cond_2
    move v1, v3

    goto/16 :goto_1

    :cond_3
    const-string v0, "MediaOutputSelector"

    const-string v1, "No speaker outputs are available."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_4
    const-string v0, "MediaOutputSelector"

    const-string v1, "No athome groups are available."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    :cond_5
    const-string v0, "MediaOutputSelector"

    const-string v1, "No individual athome receivers are passed in."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    :cond_6
    const-string v0, "MediaOutputSelector"

    const-string v1, "No user defined groups are available."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    :cond_7
    const-string v0, "MediaOutputSelector"

    const-string v1, "No individual user defined receivers are passed in."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6

    :cond_8
    iget-object v0, p0, Lcom/android/athome/picker/q;->ad:Lcom/android/athome/picker/MediaOutput;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/q;->Z:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/android/athome/picker/q;->ad:Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {v1}, Lcom/android/athome/picker/MediaOutput;->getType()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/athome/picker/q;->Z:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/android/athome/picker/q;->ad:Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {v1}, Lcom/android/athome/picker/MediaOutput;->getType()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/g;

    iget-object v1, p0, Lcom/android/athome/picker/q;->ad:Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {v0, v1}, Lcom/android/athome/picker/g;->a(Lcom/android/athome/picker/MediaOutput;)V

    goto/16 :goto_7
.end method
