.class public final Lcom/google/android/ads/GADSignalsLimited;
.super Lcom/google/android/ads/q;
.source "SourceFile"


# static fields
.field public static c:Z

.field private static d:Ljava/lang/reflect/Method;

.field private static e:Ljava/lang/reflect/Method;

.field private static f:Ljava/lang/reflect/Method;

.field private static g:Ljava/lang/reflect/Method;

.field private static h:Ljava/lang/reflect/Method;

.field private static i:Ljava/lang/String;

.field private static j:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/ads/GADSignalsLimited;->d:Ljava/lang/reflect/Method;

    sput-object v0, Lcom/google/android/ads/GADSignalsLimited;->e:Ljava/lang/reflect/Method;

    sput-object v0, Lcom/google/android/ads/GADSignalsLimited;->f:Ljava/lang/reflect/Method;

    sput-object v0, Lcom/google/android/ads/GADSignalsLimited;->g:Ljava/lang/reflect/Method;

    sput-object v0, Lcom/google/android/ads/GADSignalsLimited;->h:Ljava/lang/reflect/Method;

    sput-object v0, Lcom/google/android/ads/GADSignalsLimited;->i:Ljava/lang/String;

    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/google/android/ads/GADSignalsLimited;->j:J

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/ads/GADSignalsLimited;->c:Z

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/ads/q;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public static a(Ljava/lang/String;Landroid/content/Context;)Lcom/google/android/ads/GADSignalsLimited;
    .locals 1

    invoke-static {p0, p1}, Lcom/google/android/ads/GADSignalsLimited;->b(Ljava/lang/String;Landroid/content/Context;)V

    new-instance v0, Lcom/google/android/ads/GADSignalsLimited;

    invoke-direct {v0, p1}, Lcom/google/android/ads/GADSignalsLimited;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private static a()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/ads/GADSignalsLimited;->i:Ljava/lang/String;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException;

    invoke-direct {v0}, Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException;-><init>()V

    throw v0

    :cond_0
    sget-object v0, Lcom/google/android/ads/GADSignalsLimited;->i:Ljava/lang/String;

    return-object v0
.end method

.method private static a([BLjava/lang/String;)Ljava/lang/String;
    .locals 3

    :try_start_0
    new-instance v0, Ljava/lang/String;

    invoke-static {p0, p1}, Lcom/google/android/ads/Obfuscator;->a([BLjava/lang/String;)[B

    move-result-object v1

    const-string v2, "UTF-8"

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Lcom/google/android/ads/Obfuscator$ObfuscatorException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/common/util/Base64DecoderException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_2

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException;

    invoke-direct {v1, v0}, Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException;

    invoke-direct {v1, v0}, Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_2
    move-exception v0

    new-instance v1, Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException;

    invoke-direct {v1, v0}, Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static b()Ljava/lang/Long;
    .locals 3

    sget-object v0, Lcom/google/android/ads/GADSignalsLimited;->d:Ljava/lang/reflect/Method;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException;

    invoke-direct {v0}, Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException;-><init>()V

    throw v0

    :cond_0
    :try_start_0
    sget-object v0, Lcom/google/android/ads/GADSignalsLimited;->d:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException;

    invoke-direct {v1, v0}, Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException;

    invoke-direct {v1, v0}, Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static declared-synchronized b(Ljava/lang/String;Landroid/content/Context;)V
    .locals 11

    const/4 v0, 0x0

    const-class v1, Lcom/google/android/ads/GADSignalsLimited;

    monitor-enter v1

    :try_start_0
    sget-boolean v2, Lcom/google/android/ads/GADSignalsLimited;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_0

    :try_start_1
    sput-object p0, Lcom/google/android/ads/GADSignalsLimited;->i:Ljava/lang/String;
    :try_end_1
    .catch Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    const-string v2, "s4JGOVBgUZVHG5BFNmiV15qVu0HiQkXIkvWPURU9T58="

    invoke-static {v2}, Lcom/google/common/util/a;->a(Ljava/lang/String;)[B

    move-result-object v2

    array-length v3, v2

    const/16 v4, 0x20

    if-eq v3, v4, :cond_1

    new-instance v0, Lcom/google/android/ads/Obfuscator$ObfuscatorException;

    invoke-direct {v0}, Lcom/google/android/ads/Obfuscator$ObfuscatorException;-><init>()V

    throw v0
    :try_end_2
    .catch Lcom/google/common/util/Base64DecoderException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/lang/ClassNotFoundException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Lcom/google/android/ads/Obfuscator$ObfuscatorException; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_7
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_8
    .catch Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_0
    move-exception v0

    :try_start_3
    new-instance v2, Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException;

    invoke-direct {v2, v0}, Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_3
    .catch Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catch_1
    move-exception v0

    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    :cond_1
    const/4 v3, 0x4

    const/16 v4, 0x10

    :try_start_4
    invoke-static {v2, v3, v4}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v2

    const/16 v3, 0x10

    new-array v3, v3, [B

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    :goto_1
    array-length v2, v3

    if-ge v0, v2, :cond_2

    aget-byte v2, v3, v0

    xor-int/lit8 v2, v2, 0x44

    int-to-byte v2, v2

    aput-byte v2, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const-string v0, "iKFkwR5grhcE/nn02/NycoDwfhbeolVuqy4d6kIygd9/AjnL3qCE+0dzVPC7nhj1kMqVrboEWDwX4mszf0KRfmigQgvNW6oGuNGWcHn9EglMpr8gdVDJf2JXsEK7f3zlWC9RxBudS368zrn/dwhlaXNC2LURe5CTtb3Q/pt0jKqwNFQwIBakGmtCUdMfGQG2+wo5CT1oWLb5ojXlmbZV44yCui183/UYIBLtVXiWwKsjNjpjpmXhJlAUVQBTgcQaVKwHsOt0QezVVcejRr8gVhA/g8JZHCh7qfUhhKcmQw+dnUm4PBi9DiYyeolBpzfxkbzzIffB0gphca5SY77MRWnj7UYxczBz+crl+/II19SryT5Zsxohwcd+kJdaGBDMw0QHZJ9azp1LJQOB5H31jwCAUEG1o9qYDZ6Le7NAC7yOChzl7AWLIflal7UP3ilvvEjOQTWq9y+8FRlYT1yayr2yZ1RrlTjTTrvlA4NAclN5FsBOqv4htL11nZ+jul6YlSF9L5NNCuXr23pZjw9u3oUn4jY9gf6ugU1zo3vjIM58P1sSprOfAT3ZcH82iaPmz+XOFdRU9g39JTlRLi+XH5uq8N3sAMMFtbBbmtQYPi8tt30WR7TzEg6nY6ZPoCY/3VrR3QFM2PprjIe6L/FUzcmgsflPJEuF2qN+HDTGossbc/biCGo/bqnFOB5/abs/abyKs4PkZxImNWFhs/RlKkovmE1rwnOpx0CcXuzV08iX3opb4vgszc+J1qa3O8LeyrlmSJatP6adAeFzDKSxJRZJfHM2cFSQdWTd5SrBx8p78AmPSYDF7BCwcyjZ6IQNLPqlI7ueU9UE+oj2ZXFk7XqyX6PdpVntztTV4dz7bgn7PmuhZQFjkDloMsdRrz4YUqTUQe49ge86WVhP4f0YZ+Sti6BQ2Xp0MCXOP/9Y/USXsOUxBw2sq+OgYyZxbD4Yqqm7lMQIHF3vdNi1Wzn1bmq3i8nizy/3wMGH2dcginU9zCUR1eQzd8EzqY8X57xrNYLAYkVUBmkORu1qv8gQ1hTQ6FkQyG2M3rMO4TBQ5UAcpXVBRQ4jnmGuAvYqkaORPehyCtH90MyHCNVz3PrjsNzp9Tzwd9TQ5z+Vdd0hYa21evKoVJIP7Sj+L/8qNDNcCVrUrGgKQzCVeyWM4o+i49nmUhX12iaANnvmEZjmKyKzMyVFmd/GKYr+gwxAVcSgSJMrXUDCiRZGKHCGh36i98oylC79xrdwbje5leiA3Mfk2GeGj8X9WLM0lhKsZgXReYixWpi1XNmx+sQsDUMEYI6N9nFWYiL/zr4yxbQTiqX+YUQkmKYZv6GGWgo2dGvWWo1p5NjWFOH1yvWAxzxLMVAA7WFP2ripgHvT40kJKTogO1QFReYcyrMnZBSjGEBFO3HoV7gE/NuGAmOT7jNgPY39e1HjmrpsEGZNUoxnQFnEW6KLENprNJEd6i3dNEeZ9Bhv3y4s1FhRHX3RyG3Mr7JXzoueNd+uvmkR/KHZRKsKrhmXq0m/d0U9VraM0gNrR1kc+DjO4DgGuH9MSXVzy2fro3JOhtdSdxR9pQ25pwrc9EeIw9JZyCLiWcSmUX1Iy4SZSQgTV8EFKclpSt2B6LuxhPhQqg/oC5GfyMPh1G7rAMiF4gsq6Se/3zoUuzm31WtOP3z144vR1R1AXFAQADoPkeUAZh51vCQK67rPUq/SJElz7P4lTKZ4XLwrKFBIRdMGhutUrbGWKCcUmdpySRRGeMSU90ooGgYYwgXCyEU5hh5xv2YETE2usXvUZGk3krCryIMPXf665WsVRCx95k83tg5CWh7s0NVUQ0gKk/zq5T5i/loy2/RyDoOTNsqA0mEECHTRS168Xh0Ctw7KS30QG2FuzR6FgLV5U2nYfT3jCauNju9jdqq169nxDV/f/QEHOI/o4CeorNpQUHL6LoLjQEjlxyuY6K1WMM7cB5+yIGYRfcYgqO3GkHuoU2zMDXzgE/mp3qJTA6377MdhogOLworv/F5Lqm9ScEmGaEOXGMBAXqGtuAG7Qlz4AuZCmEiB6uJ4v/z2PZjAsZHPpws12Sq7NVT6yMwdPeQ/OX3sRy4xCvFLQBGhUWMsOj+uwsEQYSBc+O3m2ZEefikDtltDMfNjZTzKix3VvVvpVSRJtRIhzVNaQVTI0ocIi0gqF0oGBIkqzkn/dBVxNUYj+8+ANo0V06n1I3FqdCDvNLHRBvnRpAyKIhONqu3VoMTh6VcUYo+VTvSB/458HhJtb2J6GLDAVlx7lOb9r0g4lAQ6fswA2ni/a1QmJlQnlBwIHq5fYCSUVWo0KKyPF4PTaSIFDB1aRRMu+LRyP53kf5TqclwTku7BzauhjxGoY5wUuy9rZKxVFzHudmEcx/acAZ6MvW/6C+V6g+jBuQp0lGMatbJus2wrDb6LxgckjRweb8T2FueMlXBfuVemwI8D9cxq8dd8pvcwqE7HI1at5gYYyoFqzIGFeBbANLkSUUPBqi3lhSb9woxyGbQ6FxVjSlmFNoYUwe9C4JAUSQvpDd5I1QKTvJhiywtYGMv1HjQYb8q+2HS9FDcETLV5uVhlhgPMa/RRK984JhvgY7TN0RJY2NDPTMn58nZJFJuTdEcnZ0CMqqDdSrKd69SRLCWLc0zwb3TwaV8Fo/agszOJV1tzP6bJzJVagbcjwR5Cvm7JOveMjgQl0LvpqOb3LYoEBXjDzwCjnStQZlEpblg2598Q6PP5B6H/2qfsg/UOuNAFh22hc02COjI0Gsz+Pe1+Sa8/nnTgeVqMvbjHWpeOkt0rwDfOjx5l1lPVvfmDfgo3ILDhVrll+njfFMV0eU287C6YAyq/XTJUdRYsCLsm4Owkr2NAgs6+6+WRgFufCXzzrIOO8gGnTQkgBSvdfg8XohPpHdxeF6r7cTUFW3onrHsatndypKNr7Cr34wG9mAl/oPpi5j+fXZPGvT2SKSDnluNiB01+I0TdFFyEOxYMVPsU9ptDVT7zYZSs4caGXnhujR9YIS2WH8kWXk43a+7fvmuNLOHdJj0WurI6SaPHtJ+P6xD8t5kAEM5R5KHnx489M1dD7OlSre3DHBjlA5qkx8yweWN5x1UPQY2tQgt7pM4Z9Bx3Vxk05SGd09ayG5KLg1fbZol/KWkzdeyaopScx+1XvsZAFinMpi9v0CJA9/1RDCYxUoxScRkA8fsPfoyfIo+gc1JU1e8XHb39UwTGXZY1FwC3/WHd6DW15AZxZywZnV9dm9/yOUwJptNrOguSU2TAGDv7JlS3oW7ZJaTuoGIFqgIm2FP8bm8lOG4pXAPARYhu5GSq1KCDicxptkc87a0z5SqmVHUC1+Ea+uMyfJ4hW08HsHQ1SqCyDTX0YGw0LIOCIGj+Hk0SCC1lsathWS0shiC20Y6mF4JpjsdC0Br4Ytsx78t1oCDMvuStk/rfnNQUVZ6OhOafVAFr8Gf7kOGtcIWhDraXYDBSux8WMn1Vp5wkLTRsg/lhJEGS0fEdgsr6TYKA2pLebDC2Qb2oAC7LJI3T5lgZGfzNlZNXtipH58iQQhcO67C7VCax6f0HZSmV2Kq2xU8/y0ev1vMuCtIGE1lDETGmqI4IjZJUF0AQSWff5HV3UHjeOw+f/zDP1M0gxCcyHP/sWgsctD/n77wvtrX8SSE06vJVivwA0C3PBeBFD9XR82YMX2LJTyK8ZhmH1YlZ1PsVZPLZLcQteAJM8NEhKyXIQ2UkYEHGSTEMA1nI4ftnfQiX+UnRqc5Sb/XvbDkNFronfLNsiSjgw04XqM1RWwWMkhvIi+qCtJkw3W5nR6xpaYWtiARtoqBi6sVbQRHjGWY8/eQ0r8ShK96b+CUPyzx/Mvb8xwKHKDYEWe5W2jOobaw8uD/Dlg6xQmVl759i9849w4+u6iuxeu1/bVSZHC7lQHrsox6JmkLey4FLeJZtBPmvBk5+wLktqu2VvguT87OjzdOAlcgoGJqby5oKzMVyXhWJkmAZszvqK1eweMMuAMPte+R75Foury4SX6J3SATFBnKdPqFDJOrjdRdeSqCRwghCpRBMEZtyPcpElY0snlwREWGxMd4="

    invoke-static {v3, v0}, Lcom/google/android/ads/Obfuscator;->a([BLjava/lang/String;)[B

    move-result-object v2

    invoke-virtual {p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    if-nez v0, :cond_3

    const-string v0, "dex"

    const/4 v4, 0x0

    invoke-virtual {p1, v0, v4}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException;

    invoke-direct {v0}, Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException;-><init>()V

    throw v0
    :try_end_4
    .catch Lcom/google/common/util/Base64DecoderException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/lang/ClassNotFoundException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Lcom/google/android/ads/Obfuscator$ObfuscatorException; {:try_start_4 .. :try_end_4} :catch_6
    .catch Ljava/lang/NoSuchMethodException; {:try_start_4 .. :try_end_4} :catch_7
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_8
    .catch Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catch_2
    move-exception v0

    :try_start_5
    new-instance v2, Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException;

    invoke-direct {v2, v0}, Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_5
    .catch Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catch_3
    move-exception v0

    goto :goto_0

    :cond_3
    :try_start_6
    const-string v4, "ads"

    const-string v5, ".jar"

    invoke-static {v4, v5, v0}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v4

    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    const/4 v6, 0x0

    array-length v7, v2

    invoke-virtual {v5, v2, v6, v7}, Ljava/io/FileOutputStream;->write([BII)V

    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V

    new-instance v2, Ldalvik/system/DexClassLoader;

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {p1}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v8

    invoke-direct {v2, v5, v6, v7, v8}, Ldalvik/system/DexClassLoader;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)V

    const-string v5, "jH3hprEAk0tQ4maBbJry3JyZT4VIJgRtuL0kb0Il/ZdeXPQU66x9TdKPzQ9vD86Y"

    invoke-static {v3, v5}, Lcom/google/android/ads/GADSignalsLimited;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    const-string v6, "NSnpehAbK0vvvnLzyr4Fv2+QJAzCcZyI87+gpZ50VgsoZ4fpMSVu+WITasM0GfU5"

    invoke-static {v3, v6}, Lcom/google/android/ads/GADSignalsLimited;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v6

    const-string v7, "FjNf6mGXpz2m/6IpJUAg0w6fdS5SoZ24vX+aAzN6xKFZKip6uDwXE2CVkaiIpdnf"

    invoke-static {v3, v7}, Lcom/google/android/ads/GADSignalsLimited;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v7

    const-string v8, "FGNGsqBy8Pxf/IuZP/6oAZE3OixNRQTMItl57ybTV01HojckT72QTQc7DRbJtAt6"

    invoke-static {v3, v8}, Lcom/google/android/ads/GADSignalsLimited;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v8

    const-string v9, "uHt+tQsjhI+zRW9CK5PUICge4Dmfc36dxFKJrMkScmNs+XVXLtQVFI1jc5A2It1R"

    invoke-static {v3, v9}, Lcom/google/android/ads/GADSignalsLimited;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    const-string v9, "qEBHrXr17WnbbC6O+l56fWlgL+QS98r6VfF98ENaXFo="

    invoke-static {v3, v9}, Lcom/google/android/ads/GADSignalsLimited;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    new-array v10, v10, [Ljava/lang/Class;

    invoke-virtual {v5, v9, v10}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    sput-object v5, Lcom/google/android/ads/GADSignalsLimited;->d:Ljava/lang/reflect/Method;

    const-string v5, "C41Qlpu7VTMsHpOfv9D3BPQ+7fLCXKsSIpiZ6yomVPs="

    invoke-static {v3, v5}, Lcom/google/android/ads/GADSignalsLimited;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Class;

    invoke-virtual {v6, v5, v9}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    sput-object v5, Lcom/google/android/ads/GADSignalsLimited;->e:Ljava/lang/reflect/Method;

    const-string v5, "oKeeZyr5zQxr3nykp3I59QQ1FcywEO9W51kuG/jOghY="

    invoke-static {v3, v5}, Lcom/google/android/ads/GADSignalsLimited;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v9, 0x0

    const-class v10, Landroid/content/Context;

    aput-object v10, v6, v9

    invoke-virtual {v7, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    sput-object v5, Lcom/google/android/ads/GADSignalsLimited;->f:Ljava/lang/reflect/Method;

    const-string v5, "uiMqIle+/CmZGTLfbdPH/gtFGq1NCDW7CGaOnJubtkI="

    invoke-static {v3, v5}, Lcom/google/android/ads/GADSignalsLimited;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v9, Landroid/view/MotionEvent;

    aput-object v9, v6, v7

    const/4 v7, 0x1

    const-class v9, Landroid/util/DisplayMetrics;

    aput-object v9, v6, v7

    invoke-virtual {v8, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    sput-object v5, Lcom/google/android/ads/GADSignalsLimited;->g:Ljava/lang/reflect/Method;

    const-string v5, "8PzprRI6Wl4vowU254ZIkxDwEjcUGzqrAXXpCziQM/E="

    invoke-static {v3, v5}, Lcom/google/android/ads/GADSignalsLimited;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Landroid/content/Context;

    aput-object v7, v5, v6

    invoke-virtual {v2, v3, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    sput-object v2, Lcom/google/android/ads/GADSignalsLimited;->h:Ljava/lang/reflect/Method;

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    new-instance v3, Ljava/io/File;

    const-string v4, ".jar"

    const-string v5, ".dex"

    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->delete()Z
    :try_end_6
    .catch Lcom/google/common/util/Base64DecoderException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4
    .catch Ljava/lang/ClassNotFoundException; {:try_start_6 .. :try_end_6} :catch_5
    .catch Lcom/google/android/ads/Obfuscator$ObfuscatorException; {:try_start_6 .. :try_end_6} :catch_6
    .catch Ljava/lang/NoSuchMethodException; {:try_start_6 .. :try_end_6} :catch_7
    .catch Ljava/lang/NullPointerException; {:try_start_6 .. :try_end_6} :catch_8
    .catch Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :try_start_7
    invoke-static {}, Lcom/google/android/ads/GADSignalsLimited;->b()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sput-wide v2, Lcom/google/android/ads/GADSignalsLimited;->j:J

    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/ads/GADSignalsLimited;->c:Z
    :try_end_7
    .catch Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_4
    move-exception v0

    :try_start_8
    new-instance v2, Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException;

    invoke-direct {v2, v0}, Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    :catch_5
    move-exception v0

    new-instance v2, Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException;

    invoke-direct {v2, v0}, Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    :catch_6
    move-exception v0

    new-instance v2, Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException;

    invoke-direct {v2, v0}, Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    :catch_7
    move-exception v0

    new-instance v2, Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException;

    invoke-direct {v2, v0}, Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    :catch_8
    move-exception v0

    new-instance v2, Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException;

    invoke-direct {v2, v0}, Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_8
    .catch Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_0
.end method

.method private static c()Ljava/lang/String;
    .locals 3

    sget-object v0, Lcom/google/android/ads/GADSignalsLimited;->e:Ljava/lang/reflect/Method;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException;

    invoke-direct {v0}, Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException;-><init>()V

    throw v0

    :cond_0
    :try_start_0
    sget-object v0, Lcom/google/android/ads/GADSignalsLimited;->e:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException;

    invoke-direct {v1, v0}, Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException;

    invoke-direct {v1, v0}, Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static c(Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    sget-object v0, Lcom/google/android/ads/GADSignalsLimited;->h:Ljava/lang/reflect/Method;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException;

    invoke-direct {v0}, Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException;-><init>()V

    throw v0

    :cond_0
    :try_start_0
    sget-object v0, Lcom/google/android/ads/GADSignalsLimited;->h:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException;

    invoke-direct {v0}, Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException;

    invoke-direct {v1, v0}, Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException;

    invoke-direct {v1, v0}, Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_1
    return-object v0
.end method


# virtual methods
.method protected final b(Landroid/content/Context;)V
    .locals 3

    const/4 v0, 0x1

    :try_start_0
    invoke-static {}, Lcom/google/android/ads/GADSignalsLimited;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/ads/GADSignalsLimited;->a(ILjava/lang/String;)V
    :try_end_0
    .catch Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v0, 0x2

    :try_start_1
    invoke-static {}, Lcom/google/android/ads/GADSignalsLimited;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/ads/GADSignalsLimited;->a(ILjava/lang/String;)V
    :try_end_1
    .catch Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :goto_1
    const/16 v0, 0x19

    :try_start_2
    invoke-static {}, Lcom/google/android/ads/GADSignalsLimited;->b()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/ads/GADSignalsLimited;->a(IJ)V
    :try_end_2
    .catch Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :goto_2
    const/16 v0, 0x18

    :try_start_3
    invoke-static {p1}, Lcom/google/android/ads/GADSignalsLimited;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/ads/GADSignalsLimited;->a(ILjava/lang/String;)V
    :try_end_3
    .catch Lcom/google/android/ads/GADSignalsLimited$SignalUnavailableException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :goto_3
    return-void

    :catch_0
    move-exception v0

    goto :goto_3

    :catch_1
    move-exception v0

    goto :goto_3

    :catch_2
    move-exception v0

    goto :goto_2

    :catch_3
    move-exception v0

    goto :goto_1

    :catch_4
    move-exception v0

    goto :goto_0
.end method
