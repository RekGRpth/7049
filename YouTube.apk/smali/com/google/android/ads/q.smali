.class public abstract Lcom/google/android/ads/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/ads/p;


# instance fields
.field public a:Landroid/view/MotionEvent;

.field public b:Landroid/util/DisplayMetrics;

.field private c:Lcom/google/protobuf/CodedOutputStream;

.field private d:Ljava/io/ByteArrayOutputStream;


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/ads/q;->c:Lcom/google/protobuf/CodedOutputStream;

    iput-object v0, p0, Lcom/google/android/ads/q;->d:Ljava/io/ByteArrayOutputStream;

    iput-object v0, p0, Lcom/google/android/ads/q;->a:Landroid/view/MotionEvent;

    iput-object v0, p0, Lcom/google/android/ads/q;->b:Landroid/util/DisplayMetrics;

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ads/q;->b:Landroid/util/DisplayMetrics;
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v0, p0, Lcom/google/android/ads/q;->b:Landroid/util/DisplayMetrics;

    iget-object v0, p0, Lcom/google/android/ads/q;->b:Landroid/util/DisplayMetrics;

    const/high16 v1, 0x3f800000

    iput v1, v0, Landroid/util/DisplayMetrics;->density:F

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 7

    const/16 v4, 0xef

    const/16 v6, 0x20

    const/4 v5, 0x7

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/ads/q;->a()V

    invoke-virtual {p0, p1}, Lcom/google/android/ads/q;->b(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/google/android/ads/q;->b()[B

    move-result-object v1

    array-length v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x5

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    array-length v2, v1

    if-le v2, v4, :cond_1

    invoke-direct {p0}, Lcom/google/android/ads/q;->a()V

    const/16 v1, 0x14

    const-wide/16 v2, 0x1

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/ads/q;->a(IJ)V

    invoke-direct {p0}, Lcom/google/android/ads/q;->b()[B

    move-result-object v1

    :cond_1
    array-length v2, v1

    if-ge v2, v4, :cond_4

    array-length v2, v1

    rsub-int v2, v2, 0xef

    new-array v2, v2, [B

    new-instance v3, Ljava/security/SecureRandom;

    invoke-direct {v3}, Ljava/security/SecureRandom;-><init>()V

    invoke-virtual {v3, v2}, Ljava/security/SecureRandom;->nextBytes([B)V

    const/16 v3, 0xf0

    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    array-length v4, v1

    int-to-byte v4, v4

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    :goto_1
    const-string v2, "MD5"

    invoke-static {v2}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/security/MessageDigest;->update([B)V

    invoke-virtual {v2}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v2

    const/16 v3, 0x100

    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    const/16 v2, 0x100

    new-array v2, v2, [B

    new-instance v3, Lcom/google/android/ads/a;

    invoke-direct {v3}, Lcom/google/android/ads/a;-><init>()V

    invoke-virtual {v3, v1, v2}, Lcom/google/android/ads/a;->a([B[B)V

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v6, :cond_2

    const/4 v1, 0x0

    const/16 v3, 0x20

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :cond_2
    const-string v1, "UTF-8"

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    new-instance v1, Lcom/google/b/a/a;

    invoke-direct {v1, v0}, Lcom/google/b/a/a;-><init>([B)V

    invoke-virtual {v1, v2}, Lcom/google/b/a/a;->a([B)V

    :cond_3
    const/4 v0, 0x0

    invoke-static {v2, v0}, Lcom/google/common/util/a;->a([BZ)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_4
    const/16 v2, 0xf0

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    array-length v3, v1

    int-to-byte v3, v3

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v1

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :catch_1
    move-exception v0

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :catch_2
    move-exception v0

    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method private a()V
    .locals 2

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Lcom/google/android/ads/q;->d:Ljava/io/ByteArrayOutputStream;

    iget-object v0, p0, Lcom/google/android/ads/q;->d:Ljava/io/ByteArrayOutputStream;

    const/16 v1, 0x1000

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(Ljava/io/OutputStream;I)Lcom/google/protobuf/CodedOutputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ads/q;->c:Lcom/google/protobuf/CodedOutputStream;

    return-void
.end method

.method private b()[B
    .locals 1

    iget-object v0, p0, Lcom/google/android/ads/q;->c:Lcom/google/protobuf/CodedOutputStream;

    invoke-virtual {v0}, Lcom/google/protobuf/CodedOutputStream;->a()V

    iget-object v0, p0, Lcom/google/android/ads/q;->d:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/ads/q;->a(Landroid/content/Context;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final a(IJ)V
    .locals 5

    iget-object v0, p0, Lcom/google/android/ads/q;->c:Lcom/google/protobuf/CodedOutputStream;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/google/protobuf/CodedOutputStream;->b(II)V

    :goto_0
    const-wide/16 v1, -0x80

    and-long/2addr v1, p2

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    long-to-int v1, p2

    invoke-virtual {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->c(I)V

    return-void

    :cond_0
    long-to-int v1, p2

    and-int/lit8 v1, v1, 0x7f

    or-int/lit16 v1, v1, 0x80

    invoke-virtual {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->c(I)V

    const/4 v1, 0x7

    ushr-long/2addr p2, v1

    goto :goto_0
.end method

.method protected final a(ILjava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/ads/q;->c:Lcom/google/protobuf/CodedOutputStream;

    const/4 v1, 0x2

    invoke-virtual {v0, p1, v1}, Lcom/google/protobuf/CodedOutputStream;->b(II)V

    const-string v1, "UTF-8"

    invoke-virtual {p2, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    array-length v2, v1

    invoke-virtual {v0, v2}, Lcom/google/protobuf/CodedOutputStream;->e(I)V

    const/4 v2, 0x0

    array-length v3, v1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->a([BII)V

    return-void
.end method

.method protected abstract b(Landroid/content/Context;)V
.end method
