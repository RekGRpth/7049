.class public final Lcom/google/android/gms/plus/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/b;


# instance fields
.field private final a:Lcom/google/android/gms/plus/internal/h;


# direct methods
.method public varargs constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/c;Lcom/google/android/gms/common/d;[Ljava/lang/String;)V
    .locals 7

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/plus/a;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/c;Lcom/google/android/gms/common/d;[Ljava/lang/String;)V

    return-void
.end method

.method private varargs constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/c;Lcom/google/android/gms/common/d;[Ljava/lang/String;)V
    .locals 8

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/plus/a;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/c;Lcom/google/android/gms/common/d;[Ljava/lang/String;)V

    return-void
.end method

.method private varargs constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/c;Lcom/google/android/gms/common/d;[Ljava/lang/String;)V
    .locals 8

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/gms/plus/internal/h;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object v7, p7

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/plus/internal/h;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/c;Lcom/google/android/gms/common/d;[Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/a;->a:Lcom/google/android/gms/plus/internal/h;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/a;->a:Lcom/google/android/gms/plus/internal/h;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/h;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/a;->a:Lcom/google/android/gms/plus/internal/h;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/internal/h;->a(Lcom/google/android/gms/common/c;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/common/d;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/a;->a:Lcom/google/android/gms/plus/internal/h;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/internal/h;->a(Lcom/google/android/gms/common/d;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/b;Landroid/net/Uri;I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/a;->a:Lcom/google/android/gms/plus/internal/h;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/plus/internal/h;->a(Lcom/google/android/gms/plus/b;Landroid/net/Uri;I)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/c;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/a;->a:Lcom/google/android/gms/plus/internal/h;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/plus/internal/h;->a(Lcom/google/android/gms/plus/c;Ljava/lang/String;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/c;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/a;->a:Lcom/google/android/gms/plus/internal/h;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/plus/internal/h;->a(Lcom/google/android/gms/plus/c;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/d;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/a;->a:Lcom/google/android/gms/plus/internal/h;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/internal/h;->a(Lcom/google/android/gms/plus/d;)V

    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/a;->a:Lcom/google/android/gms/plus/internal/h;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/h;->c()V

    return-void
.end method

.method public final b(Lcom/google/android/gms/plus/c;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/a;->a:Lcom/google/android/gms/plus/internal/h;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/plus/internal/h;->b(Lcom/google/android/gms/plus/c;Ljava/lang/String;)V

    return-void
.end method

.method public final b(Lcom/google/android/gms/common/c;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/a;->a:Lcom/google/android/gms/plus/internal/h;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/internal/h;->b(Lcom/google/android/gms/common/c;)Z

    move-result v0

    return v0
.end method

.method public final b(Lcom/google/android/gms/common/d;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/a;->a:Lcom/google/android/gms/plus/internal/h;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/internal/h;->b(Lcom/google/android/gms/common/d;)Z

    move-result v0

    return v0
.end method

.method public final c(Lcom/google/android/gms/common/c;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/a;->a:Lcom/google/android/gms/plus/internal/h;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/internal/h;->c(Lcom/google/android/gms/common/c;)V

    return-void
.end method

.method public final c(Lcom/google/android/gms/common/d;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/a;->a:Lcom/google/android/gms/plus/internal/h;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/internal/h;->c(Lcom/google/android/gms/common/d;)V

    return-void
.end method

.method public final c()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/a;->a:Lcom/google/android/gms/plus/internal/h;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/h;->d()Z

    move-result v0

    return v0
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/a;->a:Lcom/google/android/gms/plus/internal/h;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/h;->e()V

    return-void
.end method
