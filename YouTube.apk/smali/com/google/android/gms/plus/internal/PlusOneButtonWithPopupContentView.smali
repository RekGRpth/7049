.class public Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;
.super Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/gms/common/c;
.implements Lcom/google/android/gms/common/d;


# static fields
.field private static final n:Landroid/net/Uri;

.field private static final o:Landroid/net/Uri;

.field private static final p:Landroid/net/Uri;


# instance fields
.field private A:Z

.field private final B:Ljava/lang/Runnable;

.field private final C:Lcom/google/android/gms/plus/c;

.field private final D:Lcom/google/android/gms/plus/c;

.field private final E:Lcom/google/android/gms/plus/d;

.field protected m:Z

.field private final q:Landroid/content/Context;

.field private final r:Landroid/view/Display;

.field private s:Landroid/widget/PopupWindow;

.field private t:Z

.field private final u:Landroid/widget/ImageView;

.field private final v:Landroid/widget/ImageView;

.field private w:Lcom/google/android/gms/plus/data/a/b;

.field private x:Landroid/view/View$OnClickListener;

.field private y:Z

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "plus_one_button_popup_beak_up"

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/w;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->n:Landroid/net/Uri;

    const-string v0, "plus_one_button_popup_beak_down"

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/w;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->o:Landroid/net/Uri;

    const-string v0, "plus_one_button_popup_bg"

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/w;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->p:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/google/android/gms/plus/internal/r;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/internal/r;-><init>(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->B:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/gms/plus/internal/s;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/internal/s;-><init>(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->C:Lcom/google/android/gms/plus/c;

    new-instance v0, Lcom/google/android/gms/plus/internal/t;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/internal/t;-><init>(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->D:Lcom/google/android/gms/plus/c;

    new-instance v0, Lcom/google/android/gms/plus/internal/u;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/internal/u;-><init>(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->E:Lcom/google/android/gms/plus/d;

    iput-object p1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->q:Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->q:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->r:Landroid/view/Display;

    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->q:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->u:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->u:Landroid/widget/ImageView;

    sget-object v1, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->n:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->q:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->v:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->v:Landroid/widget/ImageView;

    sget-object v1, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->p:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->e:Landroid/widget/CompoundButton;

    invoke-virtual {v0, p0}, Landroid/widget/CompoundButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->f:Lcom/google/android/gms/plus/internal/ResizingTextView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/internal/ResizingTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private a(Ljava/lang/String;)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->k:Landroid/content/res/Resources;

    const-string v1, "layout"

    const-string v2, "com.google.android.gms"

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->l:Landroid/view/LayoutInflater;

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->k:Landroid/content/res/Resources;

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getLayout(I)Landroid/content/res/XmlResourceParser;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/view/LayoutInflater;->inflate(Lorg/xmlpull/v1/XmlPullParser;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/view/View;)Landroid/widget/FrameLayout;
    .locals 4

    const/4 v3, -0x1

    const/4 v2, -0x2

    new-instance v0, Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->q:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, p1, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;Landroid/widget/PopupWindow;)Landroid/widget/PopupWindow;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->s:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;Lcom/google/android/gms/plus/data/a/b;)Lcom/google/android/gms/plus/data/a/b;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->w:Lcom/google/android/gms/plus/data/a/b;

    return-object p1
.end method

.method private a(Landroid/widget/FrameLayout;)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, -0x2

    new-instance v1, Lcom/google/android/gms/plus/internal/v;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/plus/internal/v;-><init>(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;Landroid/widget/FrameLayout;)V

    new-instance v2, Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->q:Landroid/content/Context;

    invoke-direct {v2, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iget-boolean v0, v1, Lcom/google/android/gms/plus/internal/v;->a:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->o:Landroid/net/Uri;

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    iget v3, v1, Lcom/google/android/gms/plus/internal/v;->f:I

    invoke-direct {v0, v4, v4, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    iget v3, v1, Lcom/google/android/gms/plus/internal/v;->b:I

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget v4, v1, Lcom/google/android/gms/plus/internal/v;->d:I

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-virtual {v0, v3, v5, v4, v5}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    invoke-virtual {p1, v2, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Landroid/widget/PopupWindow;

    invoke-virtual {p1}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v3

    invoke-direct {v0, p1, v2, v3}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;II)V

    iput-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->s:Landroid/widget/PopupWindow;

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->s:Landroid/widget/PopupWindow;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    iget-boolean v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->t:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->s:Landroid/widget/PopupWindow;

    const/16 v2, 0x33

    iget v3, v1, Lcom/google/android/gms/plus/internal/v;->g:I

    iget v1, v1, Lcom/google/android/gms/plus/internal/v;->h:I

    invoke-virtual {v0, p0, v2, v3, v1}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->B:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->B:Ljava/lang/Runnable;

    const-wide/16 v1, 0xbb8

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void

    :cond_1
    sget-object v0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->n:Landroid/net/Uri;

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->t:Z

    return v0
.end method

.method static synthetic a(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;Z)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->z:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;)Landroid/widget/PopupWindow;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->s:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;)Lcom/google/android/gms/plus/c;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->D:Lcom/google/android/gms/plus/c;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;)Landroid/view/Display;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->r:Landroid/view/Display;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->u:Landroid/widget/ImageView;

    return-object v0
.end method

.method private j()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->t:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->s:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->s:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->s:Landroid/widget/PopupWindow;

    :cond_0
    return-void
.end method

.method private k()V
    .locals 3

    const/4 v2, 0x3

    iget-boolean v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->t:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->y:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->y:Z

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->j:Lcom/google/android/gms/plus/data/a/a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->j:Lcom/google/android/gms/plus/data/a/a;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/data/a/a;->g()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v0, "plus_popup_text"

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->a(Ljava/lang/String;)Landroid/view/View;

    move-result-object v2

    const-string v0, "text"

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, v2}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->a(Landroid/view/View;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->a(Landroid/widget/FrameLayout;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "PlusOneButtonWithPopup"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "PlusOneButtonWithPopup"

    const-string v1, "Text confirmation popup requested but text is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-virtual {p0, v2}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->setType(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    const-string v0, "PlusOneButtonWithPopup"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PlusOneButtonWithPopup"

    const-string v1, "onConnected"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->j:Lcom/google/android/gms/plus/data/a/a;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->h:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->i:Lcom/google/android/gms/plus/a;

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->C:Lcom/google/android/gms/plus/c;

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/a;->a(Lcom/google/android/gms/plus/c;Ljava/lang/String;)V

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->A:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->w:Lcom/google/android/gms/plus/data/a/b;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->i:Lcom/google/android/gms/plus/a;

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->E:Lcom/google/android/gms/plus/d;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/a;->a(Lcom/google/android/gms/plus/d;)V

    :cond_2
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/a;Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0, p1, p2, v1}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->a(Lcom/google/android/gms/plus/a;Ljava/lang/String;I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->w:Lcom/google/android/gms/plus/data/a/b;

    iput-boolean v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->A:Z

    return-void
.end method

.method public final b()V
    .locals 0

    return-void
.end method

.method public final c()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->A:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->f()V

    :cond_0
    return-void
.end method

.method public final f()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->f()V

    invoke-direct {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->j()V

    invoke-direct {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->k()V

    return-void
.end method

.method public final g()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->g()V

    invoke-direct {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->j()V

    invoke-direct {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->k()V

    return-void
.end method

.method public final h()V
    .locals 8

    const/4 v4, 0x3

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-super {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->h()V

    invoke-direct {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->j()V

    iget-boolean v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->t:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->y:Z

    if-eqz v0, :cond_0

    iput-boolean v6, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->y:Z

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->j:Lcom/google/android/gms/plus/data/a/a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->w:Lcom/google/android/gms/plus/data/a/b;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->w:Lcom/google/android/gms/plus/data/a/b;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/data/a/b;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->j:Lcom/google/android/gms/plus/data/a/a;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/data/a/a;->g()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->w:Lcom/google/android/gms/plus/data/a/b;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/data/a/b;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v0, :cond_3

    if-eqz v1, :cond_3

    new-array v3, v7, [Ljava/lang/Object;

    aput-object v1, v3, v6

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->j:Lcom/google/android/gms/plus/data/a/a;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/data/a/a;->h()Ljava/lang/String;

    move-result-object v0

    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    const-string v0, "plus_popup_confirmation"

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->a(Ljava/lang/String;)Landroid/view/View;

    move-result-object v3

    new-instance v4, Lcom/google/android/gms/plus/data/internal/PlusImageView;

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->q:Landroid/content/Context;

    invoke-direct {v4, v0}, Lcom/google/android/gms/plus/data/internal/PlusImageView;-><init>(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->i:Lcom/google/android/gms/plus/a;

    invoke-virtual {v4, v0}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->a(Lcom/google/android/gms/plus/a;)V

    const/high16 v0, 0x42000000

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    invoke-static {v7, v0, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    float-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/gms/common/a/o;->a(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Landroid/net/Uri$Builder;

    invoke-direct {v2}, Landroid/net/Uri$Builder;-><init>()V

    const-string v5, "content"

    invoke-virtual {v2, v5}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v5, "com.google.android.gms.plus"

    invoke-virtual {v2, v5}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v5, "images"

    invoke-virtual {v2, v5}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v5, "url"

    invoke-virtual {v2, v5, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->a(Landroid/net/Uri;)V

    const-string v0, "profile_image"

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4, v6}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;I)V

    const-string v0, "text"

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, v3}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->a(Landroid/view/View;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->a(Landroid/widget/FrameLayout;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    const-string v0, "PlusOneButtonWithPopup"

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "PlusOneButtonWithPopup"

    const-string v1, "Confirmation popup requested but content view cannot be created"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-virtual {p0, v4}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->setType(I)V

    goto :goto_1

    :cond_3
    move-object v1, v0

    goto/16 :goto_0
.end method

.method public onAttachedToWindow()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->onAttachedToWindow()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->t:Z

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    const/4 v1, 0x2

    const/4 v4, 0x1

    iget-boolean v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->z:Z

    if-eqz v0, :cond_1

    const-string v0, "PlusOneButtonWithPopup"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PlusOneButtonWithPopup"

    const-string v1, "onClick: result pending, ignoring +1 button click"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->A:Z

    if-eqz v0, :cond_4

    const-string v0, "PlusOneButtonWithPopup"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "PlusOneButtonWithPopup"

    const-string v1, "onClick: anonymous button, forwarding to external click listener"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->x:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->x:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    :cond_3
    iput-boolean v4, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->m:Z

    goto :goto_0

    :cond_4
    iput-boolean v4, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->y:Z

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->j:Lcom/google/android/gms/plus/data/a/a;

    if-nez v0, :cond_7

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->y:Z

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->h:Ljava/lang/String;

    if-eqz v0, :cond_6

    const-string v0, "PlusOneButtonWithPopup"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "PlusOneButtonWithPopup"

    const-string v1, "onClick: reload +1"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->i:Lcom/google/android/gms/plus/a;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->i:Lcom/google/android/gms/plus/a;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/a;->c()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->i:Lcom/google/android/gms/plus/a;

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->C:Lcom/google/android/gms/plus/c;

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/a;->a(Lcom/google/android/gms/plus/c;Ljava/lang/String;)V

    iput-boolean v4, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->z:Z

    :cond_6
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->x:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->x:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    goto :goto_0

    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->j:Lcom/google/android/gms/plus/data/a/a;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/data/a/a;->a()Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "PlusOneButtonWithPopup"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "PlusOneButtonWithPopup"

    const-string v1, "onClick: undo +1"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->i:Lcom/google/android/gms/plus/a;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->i:Lcom/google/android/gms/plus/a;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/a;->c()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->i:Lcom/google/android/gms/plus/a;

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->C:Lcom/google/android/gms/plus/c;

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/a;->b(Lcom/google/android/gms/plus/c;Ljava/lang/String;)V

    iput-boolean v4, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->z:Z

    goto :goto_1

    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->h:Ljava/lang/String;

    if-eqz v0, :cond_6

    const-string v0, "PlusOneButtonWithPopup"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_a

    const-string v0, "PlusOneButtonWithPopup"

    const-string v1, "onClick: +1"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->i:Lcom/google/android/gms/plus/a;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->i:Lcom/google/android/gms/plus/a;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/a;->c()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->i:Lcom/google/android/gms/plus/a;

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->C:Lcom/google/android/gms/plus/c;

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->h:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->j:Lcom/google/android/gms/plus/data/a/a;

    invoke-virtual {v3}, Lcom/google/android/gms/plus/data/a/a;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/plus/a;->a(Lcom/google/android/gms/plus/c;Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean v4, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->z:Z

    goto :goto_1
.end method

.method public onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->onDetachedFromWindow()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->t:Z

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->s:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->s:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->s:Landroid/widget/PopupWindow;

    :cond_0
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0

    if-ne p1, p0, :cond_0

    invoke-super {p0, p0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->x:Landroid/view/View$OnClickListener;

    goto :goto_0
.end method
