.class public final Lcom/google/android/gms/plus/internal/h;
.super Lcom/google/android/gms/common/a/b;
.source "SourceFile"


# instance fields
.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;


# direct methods
.method public varargs constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/c;Lcom/google/android/gms/common/d;[Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p5, p6, p7}, Lcom/google/android/gms/common/a/b;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/c;Lcom/google/android/gms/common/d;[Ljava/lang/String;)V

    iput-object p2, p0, Lcom/google/android/gms/plus/internal/h;->e:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/plus/internal/h;->f:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/plus/internal/h;->g:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected final bridge synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-static {p1}, Lcom/google/android/gms/plus/internal/f;->a(Landroid/os/IBinder;)Lcom/google/android/gms/plus/internal/e;

    move-result-object v0

    return-object v0
.end method

.method protected final a()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.plus.service.START"

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/a/l;Lcom/google/android/gms/common/a/e;)V
    .locals 8

    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    const-string v0, "skip_oob"

    const/4 v1, 0x0

    invoke-virtual {v7, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/gms/plus/internal/h;->e:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/plus/internal/h;->f:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/h;->f()[Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/gms/plus/internal/h;->g:Ljava/lang/String;

    move-object v0, p1

    move-object v1, p2

    invoke-interface/range {v0 .. v7}, Lcom/google/android/gms/common/a/l;->a(Lcom/google/android/gms/common/a/i;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/b;Landroid/net/Uri;I)V
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/h;->i()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v0, "bounding_box"

    invoke-virtual {v1, v0, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    new-instance v2, Lcom/google/android/gms/plus/internal/i;

    invoke-direct {v2, p0, p1}, Lcom/google/android/gms/plus/internal/i;-><init>(Lcom/google/android/gms/plus/internal/h;Lcom/google/android/gms/plus/b;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/h;->j()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/e;

    invoke-interface {v0, v2, p2, v1}, Lcom/google/android/gms/plus/internal/e;->a(Lcom/google/android/gms/plus/internal/b;Landroid/net/Uri;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v2, v0, v3, v3}, Lcom/google/android/gms/plus/internal/i;->a(ILandroid/os/Bundle;Landroid/os/ParcelFileDescriptor;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/c;Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/h;->i()V

    new-instance v1, Lcom/google/android/gms/plus/internal/k;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/plus/internal/k;-><init>(Lcom/google/android/gms/plus/internal/h;Lcom/google/android/gms/plus/c;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/h;->j()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/e;

    invoke-interface {v0, v1, p2}, Lcom/google/android/gms/plus/internal/e;->a(Lcom/google/android/gms/plus/internal/b;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v2, v2}, Lcom/google/android/gms/plus/internal/k;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/c;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/h;->i()V

    new-instance v1, Lcom/google/android/gms/plus/internal/k;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/plus/internal/k;-><init>(Lcom/google/android/gms/plus/internal/h;Lcom/google/android/gms/plus/c;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/h;->j()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/e;

    invoke-interface {v0, v1, p2, p3}, Lcom/google/android/gms/plus/internal/e;->a(Lcom/google/android/gms/plus/internal/b;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v2, v2}, Lcom/google/android/gms/plus/internal/k;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/d;)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/h;->i()V

    new-instance v1, Lcom/google/android/gms/plus/internal/m;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/plus/internal/m;-><init>(Lcom/google/android/gms/plus/internal/h;Lcom/google/android/gms/plus/d;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/h;->j()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/e;

    invoke-interface {v0, v1}, Lcom/google/android/gms/plus/internal/e;->a(Lcom/google/android/gms/plus/internal/b;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v2, v2}, Lcom/google/android/gms/plus/internal/m;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method protected final b()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.plus.internal.IPlusService"

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/plus/c;Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/h;->i()V

    new-instance v1, Lcom/google/android/gms/plus/internal/k;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/plus/internal/k;-><init>(Lcom/google/android/gms/plus/internal/h;Lcom/google/android/gms/plus/c;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/h;->j()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/e;

    invoke-interface {v0, v1, p2}, Lcom/google/android/gms/plus/internal/e;->b(Lcom/google/android/gms/plus/internal/b;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v2, v2}, Lcom/google/android/gms/plus/internal/k;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final k()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/h;->i()V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/h;->j()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/e;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/e;->a()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
