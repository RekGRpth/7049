.class final Lcom/google/android/gms/plus/internal/s;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/plus/c;


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;


# direct methods
.method constructor <init>(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/plus/internal/s;->a:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/a;Lcom/google/android/gms/plus/data/a/a;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/s;->a:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    iget-boolean v0, v0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/s;->a:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    iput-boolean v2, v0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->a:Z

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/s;->a:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    iget-object v0, v0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->e:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->refreshDrawableState()V

    :cond_0
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/a;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/s;->a:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    iput-object p2, v0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->j:Lcom/google/android/gms/plus/data/a/a;

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/s;->a:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->i()V

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/s;->a:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/s;->a:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    invoke-static {v0, v2}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->a(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;Z)Z

    return-void

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/s;->a:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->f()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/s;->a:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    invoke-static {v1, v2}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->a(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;Z)Z

    throw v0
.end method
