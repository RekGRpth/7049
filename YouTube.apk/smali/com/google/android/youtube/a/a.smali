.class public abstract Lcom/google/android/youtube/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/Object;

.field private final c:Lcom/google/android/youtube/a/b;

.field private final d:I

.field private e:I

.field private final f:Ljava/util/Set;

.field private g:I


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/a/a;->a:Landroid/content/Context;

    invoke-static {p1}, Lcom/android/athome/picker/media/k;->a(Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/a/a;->b:Ljava/lang/Object;

    new-instance v0, Lcom/google/android/youtube/a/b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/a/b;-><init>(Lcom/google/android/youtube/a/a;B)V

    iput-object v0, p0, Lcom/google/android/youtube/a/a;->c:Lcom/google/android/youtube/a/b;

    const v0, 0x800001

    iput v0, p0, Lcom/google/android/youtube/a/a;->d:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/a/a;->e:I

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/a/a;->f:Ljava/util/Set;

    return-void
.end method

.method private a()V
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/youtube/a/a;->b:Ljava/lang/Object;

    invoke-static {v1}, Lcom/android/athome/picker/media/k;->a(Ljava/lang/Object;)I

    move-result v2

    iget-object v1, p0, Lcom/google/android/youtube/a/a;->f:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    iput v0, p0, Lcom/google/android/youtube/a/a;->g:I

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    iget-object v3, p0, Lcom/google/android/youtube/a/a;->b:Ljava/lang/Object;

    invoke-static {v3, v1}, Lcom/android/athome/picker/media/k;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, Lcom/android/athome/picker/media/q;->d(Ljava/lang/Object;)I

    move-result v4

    iget v5, p0, Lcom/google/android/youtube/a/a;->d:I

    and-int/2addr v5, v4

    if-eqz v5, :cond_0

    add-int/lit8 v0, v0, 0x1

    iget-object v5, p0, Lcom/google/android/youtube/a/a;->f:Ljava/util/Set;

    invoke-static {v3}, Lcom/android/athome/picker/media/q;->f(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v5, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    and-int/lit8 v3, v4, 0x1

    if-eqz v3, :cond_1

    iget v3, p0, Lcom/google/android/youtube/a/a;->g:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/android/youtube/a/a;->g:I

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    iget v1, p0, Lcom/google/android/youtube/a/a;->e:I

    if-eq v1, v0, :cond_3

    iput v0, p0, Lcom/google/android/youtube/a/a;->e:I

    invoke-virtual {p0}, Lcom/google/android/youtube/a/a;->d()V

    :cond_3
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/a/a;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/a/a;->a()V

    return-void
.end method


# virtual methods
.method public final b()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/a/a;->b:Ljava/lang/Object;

    iget v1, p0, Lcom/google/android/youtube/a/a;->d:I

    iget-object v2, p0, Lcom/google/android/youtube/a/a;->c:Lcom/google/android/youtube/a/b;

    invoke-static {v0, v1, v2}, Lcom/android/athome/picker/media/k;->a(Ljava/lang/Object;ILcom/android/athome/picker/media/b;)V

    invoke-direct {p0}, Lcom/google/android/youtube/a/a;->a()V

    return-void
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/a/a;->b:Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/youtube/a/a;->c:Lcom/google/android/youtube/a/b;

    invoke-static {v0, v1}, Lcom/android/athome/picker/media/k;->a(Ljava/lang/Object;Lcom/android/athome/picker/media/b;)V

    return-void
.end method

.method protected abstract c(Z)V
.end method

.method public final d()V
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/youtube/a/a;->e:I

    if-le v1, v0, :cond_0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/a/a;->c(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final e()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/a/a;->f:Ljava/util/Set;

    return-object v0
.end method

.method protected final f()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/a/a;->g:I

    return v0
.end method

.method protected final g()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/a/a;->b:Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/youtube/a/a;->a:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/google/android/youtube/a/c;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method
