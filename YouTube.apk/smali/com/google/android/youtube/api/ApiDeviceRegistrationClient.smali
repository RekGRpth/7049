.class public final Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;
.super Lcom/google/android/youtube/core/client/h;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/client/as;


# static fields
.field private static final a:[B

.field private static final h:[B


# instance fields
.field private final i:Lcom/google/android/youtube/core/async/au;

.field private final j:Lcom/google/android/youtube/core/async/au;

.field private final k:Ljava/lang/String;

.field private final l:[B

.field private final m:[B

.field private final n:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x62

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;->a:[B

    const/16 v0, 0x18

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;->h:[B

    return-void

    nop

    :array_0
    .array-data 1
        0x41t
        0x49t
        0x33t
        0x39t
        0x73t
        0x69t
        0x36t
        0x72t
        0x33t
        0x6at
        0x77t
        0x5at
        0x30t
        0x66t
        0x6et
        0x5at
        0x4bt
        0x38t
        0x6dt
        0x62t
        0x38t
        0x33t
        0x4ct
        0x70t
        0x4ct
        0x52t
        0x78t
        0x63t
        0x4dt
        0x4at
        0x71t
        0x75t
        0x79t
        0x35t
        0x71t
        0x51t
        0x41t
        0x55t
        0x72t
        0x61t
        0x73t
        0x31t
        0x5ft
        0x30t
        0x33t
        0x62t
        0x46t
        0x39t
        0x75t
        0x4ft
        0x4ft
        0x73t
        0x52t
        0x78t
        0x73t
        0x78t
        0x77t
        0x53t
        0x49t
        0x54t
        0x33t
        0x69t
        0x42t
        0x37t
        0x34t
        0x6ct
        0x2dt
        0x44t
        0x6et
        0x49t
        0x6et
        0x79t
        0x56t
        0x75t
        0x6bt
        0x48t
        0x38t
        0x72t
        0x34t
        0x41t
        0x47t
        0x52t
        0x6at
        0x48t
        0x50t
        0x4at
        0x57t
        0x44t
        0x68t
        0x57t
        0x64t
        0x73t
        0x70t
        0x34t
        0x61t
        0x65t
        0x5ft
        0x67t
    .end array-data

    nop

    :array_1
    .array-data 1
        0x5at
        0x47t
        0x33t
        0x6ct
        0x50t
        0x78t
        0x78t
        0x67t
        0x38t
        0x4bt
        0x57t
        0x54t
        0x62t
        0x63t
        0x34t
        0x62t
        0x6at
        0x37t
        0x66t
        0x58t
        0x59t
        0x51t
        0x3dt
        0x3dt
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    sget-object v6, Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;->a:[B

    sget-object v7, Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;->h:[B

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Ljava/lang/String;Ljava/lang/String;[B[B)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Ljava/lang/String;Ljava/lang/String;[B[B)V
    .locals 2

    invoke-direct {p0, p2, p3}, Lcom/google/android/youtube/core/client/h;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;)V

    const-string v0, "developerKey cannot be null or empty"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;->k:Ljava/lang/String;

    const-string v0, "serial cannot be null or empty"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;->n:Ljava/lang/String;

    const-string v0, "playerApiKey cannot be null"

    invoke-static {p6, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, p0, Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;->m:[B

    invoke-static {p1}, Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;->a(Landroid/content/Context;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;->l:[B

    new-instance v0, Lcom/google/android/youtube/core/converter/http/ec;

    sget-object v1, Lcom/google/android/youtube/core/converter/http/HttpMethod;->POST:Lcom/google/android/youtube/core/converter/http/HttpMethod;

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/converter/http/ec;-><init>(Lcom/google/android/youtube/core/converter/http/HttpMethod;)V

    new-instance v1, Lcom/google/android/youtube/core/converter/http/aq;

    invoke-direct {v1, p7}, Lcom/google/android/youtube/core/converter/http/aq;-><init>([B)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;->a(Lcom/google/android/youtube/core/converter/b;Lcom/google/android/youtube/core/converter/http/bn;)Lcom/google/android/youtube/core/async/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;->i:Lcom/google/android/youtube/core/async/au;

    new-instance v0, Lcom/google/android/youtube/api/h;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/api/h;-><init>(Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;B)V

    invoke-virtual {p0, v0, v0}, Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;->a(Lcom/google/android/youtube/core/converter/b;Lcom/google/android/youtube/core/converter/http/bn;)Lcom/google/android/youtube/core/async/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;->j:Lcom/google/android/youtube/core/async/au;

    return-void
.end method

.method private a()[B
    .locals 2

    invoke-direct {p0}, Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;->b()Ljavax/crypto/Cipher;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;->l:[B

    invoke-virtual {v0, v1}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_0
    .catch Ljavax/crypto/BadPaddingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static a(Landroid/content/Context;)[B
    .locals 4

    :try_start_0
    const-string v0, "SHA1"

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x40

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget-object v2, v1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v2, v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    new-instance v0, Lcom/google/android/youtube/api/ApiDeviceRegistrationClient$IllegalPackageSignatureException;

    iget-object v1, v1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v1, v1

    invoke-direct {v0, v1}, Lcom/google/android/youtube/api/ApiDeviceRegistrationClient$IllegalPackageSignatureException;-><init>(I)V

    throw v0
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Couldn\'t get package information."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_0
    :try_start_1
    invoke-virtual {v0}, Ljava/security/MessageDigest;->reset()V

    iget-object v1, v1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v1}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/security/MessageDigest;->update([B)V

    invoke-virtual {v0}, Ljava/security/MessageDigest;->digest()[B
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    return-object v0

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Couldn\'t get an instance of the MD5 algorithm."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method static synthetic a(Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;)[B
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;->l:[B

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;->k:Ljava/lang/String;

    return-object v0
.end method

.method private b()Ljavax/crypto/Cipher;
    .locals 5

    const/16 v4, 0x8

    const/4 v0, 0x1

    :try_start_0
    iget-object v1, p0, Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;->n:Ljava/lang/String;

    const-string v2, "UTF-8"

    invoke-virtual {v1, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    new-instance v2, Ljava/util/zip/CRC32;

    invoke-direct {v2}, Ljava/util/zip/CRC32;-><init>()V

    invoke-virtual {v2, v1}, Ljava/util/zip/CRC32;->update([B)V

    invoke-static {v4}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v2}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    array-length v2, v1

    if-ne v2, v4, :cond_0

    :goto_0
    invoke-static {v0}, Lcom/google/android/youtube/core/utils/s;->b(Z)V

    new-instance v2, Ljavax/crypto/spec/SecretKeySpec;

    const-string v0, "RC4"

    invoke-direct {v2, v1, v0}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    :try_start_1
    const-string v0, "RC4"

    invoke-static {v0}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v0

    :goto_1
    const/4 v1, 0x1

    :try_start_2
    invoke-virtual {v0, v1, v2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V
    :try_end_2
    .catch Ljava/security/InvalidKeyException; {:try_start_2 .. :try_end_2} :catch_3

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catch_1
    move-exception v0

    new-instance v0, Lcom/google/android/youtube/api/z;

    invoke-direct {v0}, Lcom/google/android/youtube/api/z;-><init>()V

    goto :goto_1

    :catch_2
    move-exception v0

    new-instance v0, Lcom/google/android/youtube/api/z;

    invoke-direct {v0}, Lcom/google/android/youtube/api/z;-><init>()V

    goto :goto_1

    :catch_3
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method static synthetic c(Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;->n:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;)Lcom/google/android/youtube/core/async/au;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;->j:Lcom/google/android/youtube/core/async/au;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;)[B
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;->a()[B

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/async/n;)V
    .locals 4

    :try_start_0
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;->m:[B

    const-string v2, "UTF-8"

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v1, "https://www.google.com/youtube/accounts/registerDevice?type=GDATA&developer=%s&serialNumber=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    iget-object v3, p0, Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;->n:Ljava/lang/String;

    aput-object v3, v2, v0

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;->i:Lcom/google/android/youtube/core/async/au;

    new-instance v2, Lcom/google/android/youtube/api/i;

    invoke-direct {v2, p0, p1}, Lcom/google/android/youtube/api/i;-><init>(Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;Lcom/google/android/youtube/core/async/n;)V

    invoke-interface {v1, v0, v2}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
