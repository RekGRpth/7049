.class public final Lcom/google/android/youtube/api/ApiPlayer$PlayerState;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final ITERATOR_TYPE_NO_ITERATOR:I = -0x1

.field public static final ITERATOR_TYPE_PLAYLIST_ID:I = 0x0

.field public static final ITERATOR_TYPE_VIDEO_IDS:I = 0x1


# instance fields
.field public final directorState:Lcom/google/android/youtube/core/player/Director$DirectorState;

.field public final iteratorPosition:I

.field public final iteratorType:I

.field public final manageAudioFocus:Z

.field public final playlistId:Ljava/lang/String;

.field public final videoIds:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/youtube/api/u;

    invoke-direct {v0}, Lcom/google/android/youtube/api/u;-><init>()V

    sput-object v0, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/util/List;IZLcom/google/android/youtube/core/player/Director$DirectorState;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;->iteratorType:I

    iput-object p2, p0, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;->playlistId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;->videoIds:Ljava/util/List;

    iput p4, p0, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;->iteratorPosition:I

    iput-boolean p5, p0, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;->manageAudioFocus:Z

    iput-object p6, p0, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;->directorState:Lcom/google/android/youtube/core/player/Director$DirectorState;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;->iteratorType:I

    iget v1, p0, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;->iteratorType:I

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;->playlistId:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;->videoIds:Ljava/util/List;

    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;->iteratorPosition:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_2

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;->manageAudioFocus:Z

    const-class v0, Lcom/google/android/youtube/core/player/Director$DirectorState;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/Director$DirectorState;

    iput-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;->directorState:Lcom/google/android/youtube/core/player/Director$DirectorState;

    return-void

    :cond_0
    iget v1, p0, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;->iteratorType:I

    if-ne v1, v0, :cond_1

    iput-object v2, p0, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;->playlistId:Ljava/lang/String;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;->videoIds:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;->videoIds:Ljava/util/List;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Failed to parse PlayerState parcel"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ApiPlayer.PlayerState{"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " iteratorType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;->iteratorType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " playlistId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;->playlistId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " videoIds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;->videoIds:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " iteratorPosition="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;->iteratorPosition:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " manageAudioFocus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;->manageAudioFocus:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " directorState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;->directorState:Lcom/google/android/youtube/core/player/Director$DirectorState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;->iteratorType:I

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    iget v2, p0, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;->iteratorType:I

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;->playlistId:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    :cond_0
    :goto_0
    iget v2, p0, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;->iteratorPosition:I

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v2, p0, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;->manageAudioFocus:Z

    if-eqz v2, :cond_2

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;->directorState:Lcom/google/android/youtube/core/player/Director$DirectorState;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void

    :cond_1
    iget v2, p0, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;->iteratorType:I

    if-ne v2, v0, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;->videoIds:Ljava/util/List;

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method
