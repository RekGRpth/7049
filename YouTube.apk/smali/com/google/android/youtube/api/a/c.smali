.class final Lcom/google/android/youtube/api/a/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/api/s;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/api/a/b;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/api/a/b;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/api/a/c;->a:Lcom/google/android/youtube/api/a/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/api/a/b;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/api/a/c;-><init>(Lcom/google/android/youtube/api/a/b;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/a/c;->a:Lcom/google/android/youtube/api/a/b;

    invoke-static {v0}, Lcom/google/android/youtube/api/a/b;->a(Lcom/google/android/youtube/api/a/b;)V

    return-void
.end method

.method public final a(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/a/c;->a:Lcom/google/android/youtube/api/a/b;

    invoke-static {v0}, Lcom/google/android/youtube/api/a/b;->k(Lcom/google/android/youtube/api/a/b;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/a/c;->a:Lcom/google/android/youtube/api/a/b;

    invoke-static {v0}, Lcom/google/android/youtube/api/a/b;->l(Lcom/google/android/youtube/api/a/b;)V

    return-void
.end method

.method public final a(II)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/a/c;->a:Lcom/google/android/youtube/api/a/b;

    invoke-static {v0}, Lcom/google/android/youtube/api/a/b;->i(Lcom/google/android/youtube/api/a/b;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/a/c;->a:Lcom/google/android/youtube/api/a/b;

    invoke-static {v0}, Lcom/google/android/youtube/api/a/b;->j(Lcom/google/android/youtube/api/a/b;)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/a/c;->a:Lcom/google/android/youtube/api/a/b;

    invoke-static {v0, p1}, Lcom/google/android/youtube/api/a/b;->a(Lcom/google/android/youtube/api/a/b;Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;IIZZ)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/a/c;->a:Lcom/google/android/youtube/api/a/b;

    invoke-static {v0, p1, p2}, Lcom/google/android/youtube/api/a/b;->a(Lcom/google/android/youtube/api/a/b;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/a/c;->a:Lcom/google/android/youtube/api/a/b;

    invoke-static {v0, p1}, Lcom/google/android/youtube/api/a/b;->a(Lcom/google/android/youtube/api/a/b;Ljava/lang/String;)V

    return-void
.end method

.method public final a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/a/c;->a:Lcom/google/android/youtube/api/a/b;

    invoke-static {v0, p1}, Lcom/google/android/youtube/api/a/b;->b(Lcom/google/android/youtube/api/a/b;Z)V

    iget-object v0, p0, Lcom/google/android/youtube/api/a/c;->a:Lcom/google/android/youtube/api/a/b;

    invoke-static {v0, p1}, Lcom/google/android/youtube/api/a/b;->c(Lcom/google/android/youtube/api/a/b;Z)V

    return-void
.end method

.method public final a(ZI)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/a/c;->a:Lcom/google/android/youtube/api/a/b;

    invoke-static {v0, p1}, Lcom/google/android/youtube/api/a/b;->a(Lcom/google/android/youtube/api/a/b;Z)V

    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/a/c;->a:Lcom/google/android/youtube/api/a/b;

    invoke-static {v0}, Lcom/google/android/youtube/api/a/b;->b(Lcom/google/android/youtube/api/a/b;)V

    return-void
.end method

.method public final b(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/a/c;->a:Lcom/google/android/youtube/api/a/b;

    invoke-static {v0, p1}, Lcom/google/android/youtube/api/a/b;->a(Lcom/google/android/youtube/api/a/b;I)V

    return-void
.end method

.method public final b(II)V
    .locals 0

    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/a/c;->a:Lcom/google/android/youtube/api/a/b;

    invoke-static {v0}, Lcom/google/android/youtube/api/a/b;->c(Lcom/google/android/youtube/api/a/b;)V

    return-void
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/a/c;->a:Lcom/google/android/youtube/api/a/b;

    invoke-static {v0}, Lcom/google/android/youtube/api/a/b;->d(Lcom/google/android/youtube/api/a/b;)V

    return-void
.end method

.method public final e()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/a/c;->a:Lcom/google/android/youtube/api/a/b;

    invoke-static {v0}, Lcom/google/android/youtube/api/a/b;->e(Lcom/google/android/youtube/api/a/b;)V

    return-void
.end method

.method public final f()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/a/c;->a:Lcom/google/android/youtube/api/a/b;

    invoke-static {v0}, Lcom/google/android/youtube/api/a/b;->f(Lcom/google/android/youtube/api/a/b;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/a/c;->a:Lcom/google/android/youtube/api/a/b;

    invoke-static {v0}, Lcom/google/android/youtube/api/a/b;->g(Lcom/google/android/youtube/api/a/b;)V

    return-void
.end method

.method public final g()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/a/c;->a:Lcom/google/android/youtube/api/a/b;

    invoke-static {v0}, Lcom/google/android/youtube/api/a/b;->h(Lcom/google/android/youtube/api/a/b;)V

    return-void
.end method

.method public final h()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/a/c;->a:Lcom/google/android/youtube/api/a/b;

    invoke-static {v0}, Lcom/google/android/youtube/api/a/b;->m(Lcom/google/android/youtube/api/a/b;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/a/c;->a:Lcom/google/android/youtube/api/a/b;

    invoke-static {v0}, Lcom/google/android/youtube/api/a/b;->n(Lcom/google/android/youtube/api/a/b;)V

    return-void
.end method

.method public final i()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/a/c;->a:Lcom/google/android/youtube/api/a/b;

    invoke-static {v0}, Lcom/google/android/youtube/api/a/b;->o(Lcom/google/android/youtube/api/a/b;)V

    return-void
.end method

.method public final j()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/a/c;->a:Lcom/google/android/youtube/api/a/b;

    invoke-static {v0}, Lcom/google/android/youtube/api/a/b;->p(Lcom/google/android/youtube/api/a/b;)V

    return-void
.end method

.method public final k()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/a/c;->a:Lcom/google/android/youtube/api/a/b;

    invoke-static {v0}, Lcom/google/android/youtube/api/a/b;->q(Lcom/google/android/youtube/api/a/b;)V

    return-void
.end method
