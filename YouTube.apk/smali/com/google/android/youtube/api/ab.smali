.class public final Lcom/google/android/youtube/api/ab;
.super Lcom/google/android/youtube/core/client/ba;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/client/bc;


# instance fields
.field private final a:Lcom/google/android/youtube/core/async/GDataRequestFactory;

.field private h:Lcom/google/android/youtube/core/converter/http/bb;

.field private i:Lcom/google/android/youtube/core/converter/http/fq;

.field private j:Lcom/google/android/youtube/core/cache/a;

.field private k:Lcom/google/android/youtube/core/async/au;

.field private l:Lcom/google/android/youtube/core/async/au;

.field private m:Lcom/google/android/youtube/core/async/au;

.field private n:Lcom/google/android/youtube/core/async/au;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/utils/e;Lcom/google/android/youtube/core/converter/m;Lcom/google/android/youtube/core/async/GDataRequestFactory;Lcom/google/android/youtube/core/async/DeviceAuthorizer;Lcom/google/android/youtube/core/async/GDataRequest$Version;)V
    .locals 5

    const/4 v4, 0x1

    invoke-direct {p0, p1, p2, p4, p3}, Lcom/google/android/youtube/core/client/ba;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/m;Lcom/google/android/youtube/core/utils/e;)V

    const-string v0, "gdataRequestFactory cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/GDataRequestFactory;

    iput-object v0, p0, Lcom/google/android/youtube/api/ab;->a:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    const-string v0, "deviceAuthorizer cannot be null"

    invoke-static {p6, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "gdataVersion can\'t be null"

    invoke-static {p7, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/youtube/core/converter/http/bb;

    sget-object v1, Lcom/google/android/youtube/core/converter/http/HttpMethod;->GET:Lcom/google/android/youtube/core/converter/http/HttpMethod;

    invoke-direct {v0, v1, p6, p7}, Lcom/google/android/youtube/core/converter/http/bb;-><init>(Lcom/google/android/youtube/core/converter/http/HttpMethod;Lcom/google/android/youtube/core/async/DeviceAuthorizer;Lcom/google/android/youtube/core/async/GDataRequest$Version;)V

    iput-object v0, p0, Lcom/google/android/youtube/api/ab;->h:Lcom/google/android/youtube/core/converter/http/bb;

    new-instance v0, Lcom/google/android/youtube/core/converter/http/fq;

    iget-object v1, p0, Lcom/google/android/youtube/api/ab;->g:Lcom/google/android/youtube/core/converter/m;

    invoke-direct {v0, v1, v4}, Lcom/google/android/youtube/core/converter/http/fq;-><init>(Lcom/google/android/youtube/core/converter/m;Z)V

    iput-object v0, p0, Lcom/google/android/youtube/api/ab;->i:Lcom/google/android/youtube/core/converter/http/fq;

    const/16 v0, 0x1f4

    invoke-static {v0}, Lcom/google/android/youtube/api/ab;->a(I)Lcom/google/android/youtube/core/cache/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/api/ab;->j:Lcom/google/android/youtube/core/cache/a;

    iget-object v0, p0, Lcom/google/android/youtube/api/ab;->h:Lcom/google/android/youtube/core/converter/http/bb;

    iget-object v1, p0, Lcom/google/android/youtube/api/ab;->i:Lcom/google/android/youtube/core/converter/http/fq;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/api/ab;->a(Lcom/google/android/youtube/core/converter/b;Lcom/google/android/youtube/core/converter/http/bn;)Lcom/google/android/youtube/core/async/an;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/api/ab;->a(Lcom/google/android/youtube/core/async/au;)Lcom/google/android/youtube/core/async/i;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/api/ab;->j:Lcom/google/android/youtube/core/cache/a;

    const-wide/32 v2, 0xdbba00

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/google/android/youtube/api/ab;->a(Lcom/google/android/youtube/core/cache/a;Lcom/google/android/youtube/core/async/au;J)Lcom/google/android/youtube/core/async/bg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/api/ab;->k:Lcom/google/android/youtube/core/async/au;

    new-instance v0, Lcom/google/android/youtube/core/converter/http/fp;

    iget-object v1, p0, Lcom/google/android/youtube/api/ab;->g:Lcom/google/android/youtube/core/converter/m;

    invoke-direct {v0, v1, v4}, Lcom/google/android/youtube/core/converter/http/fp;-><init>(Lcom/google/android/youtube/core/converter/m;Z)V

    iget-object v1, p0, Lcom/google/android/youtube/api/ab;->h:Lcom/google/android/youtube/core/converter/http/bb;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/youtube/api/ab;->a(Lcom/google/android/youtube/core/converter/b;Lcom/google/android/youtube/core/converter/http/bn;)Lcom/google/android/youtube/core/async/an;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/api/ab;->a(Lcom/google/android/youtube/core/async/au;)Lcom/google/android/youtube/core/async/i;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/api/ab;->l:Lcom/google/android/youtube/core/async/au;

    new-instance v0, Lcom/google/android/youtube/core/converter/http/e;

    iget-object v1, p0, Lcom/google/android/youtube/api/ab;->g:Lcom/google/android/youtube/core/converter/m;

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/converter/http/e;-><init>(Lcom/google/android/youtube/core/converter/m;)V

    const/16 v1, 0x14

    invoke-static {v1}, Lcom/google/android/youtube/api/ab;->a(I)Lcom/google/android/youtube/core/cache/b;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/api/ab;->h:Lcom/google/android/youtube/core/converter/http/bb;

    invoke-virtual {p0, v2, v0}, Lcom/google/android/youtube/api/ab;->a(Lcom/google/android/youtube/core/converter/b;Lcom/google/android/youtube/core/converter/http/bn;)Lcom/google/android/youtube/core/async/an;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/api/ab;->a(Lcom/google/android/youtube/core/async/au;)Lcom/google/android/youtube/core/async/i;

    move-result-object v0

    const-wide/32 v2, 0x6ddd00

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/google/android/youtube/api/ab;->a(Lcom/google/android/youtube/core/cache/a;Lcom/google/android/youtube/core/async/au;J)Lcom/google/android/youtube/core/async/bg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/api/ab;->m:Lcom/google/android/youtube/core/async/au;

    new-instance v0, Lcom/google/android/youtube/core/converter/http/bs;

    iget-object v1, p0, Lcom/google/android/youtube/api/ab;->g:Lcom/google/android/youtube/core/converter/m;

    invoke-direct {v0, v1, v4}, Lcom/google/android/youtube/core/converter/http/bs;-><init>(Lcom/google/android/youtube/core/converter/m;Z)V

    iget-object v1, p0, Lcom/google/android/youtube/api/ab;->h:Lcom/google/android/youtube/core/converter/http/bb;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/youtube/api/ab;->a(Lcom/google/android/youtube/core/converter/b;Lcom/google/android/youtube/core/converter/http/bn;)Lcom/google/android/youtube/core/async/an;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/api/ab;->a(Lcom/google/android/youtube/core/async/au;)Lcom/google/android/youtube/core/async/i;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/api/ab;->n:Lcom/google/android/youtube/core/async/au;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/youtube/core/async/GDataRequestFactory;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/ab;->a:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    return-object v0
.end method

.method public final a(Landroid/net/Uri;Lcom/google/android/youtube/core/async/n;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/ab;->n:Lcom/google/android/youtube/core/async/au;

    iget-object v1, p0, Lcom/google/android/youtube/api/ab;->a:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    invoke-static {p1}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->c(Landroid/net/Uri;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/youtube/core/async/n;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/ab;->k:Lcom/google/android/youtube/core/async/au;

    iget-object v1, p0, Lcom/google/android/youtube/api/ab;->a:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    invoke-virtual {v1, p1}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->a(Ljava/lang/String;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method

.method public final b()Lcom/google/android/youtube/core/async/au;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/ab;->l:Lcom/google/android/youtube/core/async/au;

    return-object v0
.end method

.method public final b(Ljava/lang/String;Lcom/google/android/youtube/core/async/n;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/ab;->m:Lcom/google/android/youtube/core/async/au;

    iget-object v1, p0, Lcom/google/android/youtube/api/ab;->a:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    invoke-static {p1}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->k(Ljava/lang/String;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method
