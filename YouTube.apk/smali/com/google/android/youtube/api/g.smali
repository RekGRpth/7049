.class final Lcom/google/android/youtube/api/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;

.field private final b:Lcom/google/android/youtube/core/async/n;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;Lcom/google/android/youtube/core/async/n;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/youtube/api/g;->a:Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "target cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/n;

    iput-object v0, p0, Lcom/google/android/youtube/api/g;->b:Lcom/google/android/youtube/core/async/n;

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2

    check-cast p1, Landroid/util/Pair;

    iget-object v0, p0, Lcom/google/android/youtube/api/g;->b:Lcom/google/android/youtube/core/async/n;

    iget-object v1, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-interface {v0, v1, p2}, Lcom/google/android/youtube/core/async/n;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    check-cast p1, Landroid/util/Pair;

    check-cast p2, Lcom/google/android/youtube/core/model/d;

    iget-object v0, p0, Lcom/google/android/youtube/api/g;->b:Lcom/google/android/youtube/core/async/n;

    iget-object v1, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-interface {v0, v1, p2}, Lcom/google/android/youtube/core/async/n;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method
