.class final Lcom/google/android/youtube/api/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;

.field private final b:Lcom/google/android/youtube/core/async/n;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;Lcom/google/android/youtube/core/async/n;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/youtube/api/i;->a:Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "target cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/n;

    iput-object v0, p0, Lcom/google/android/youtube/api/i;->b:Lcom/google/android/youtube/core/async/n;

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    check-cast p1, Landroid/net/Uri;

    iget-object v0, p0, Lcom/google/android/youtube/api/i;->b:Lcom/google/android/youtube/core/async/n;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/async/n;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 5

    check-cast p2, Lcom/google/android/youtube/core/model/d;

    iget-object v0, p0, Lcom/google/android/youtube/api/i;->a:Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;

    invoke-static {v0}, Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;->a(Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;)[B

    move-result-object v0

    const/16 v1, 0xa

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    const-string v1, "https://www.google.com/youtube/accounts/registerDevice?type=ANDROID_PLAYER_API&developer=%s&fingerprint=%s&serialNumber=%s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/youtube/api/i;->a:Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;

    invoke-static {v4}, Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;->b(Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    const/4 v0, 0x2

    iget-object v3, p0, Lcom/google/android/youtube/api/i;->a:Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;

    invoke-static {v3}, Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;->c(Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/api/i;->a:Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;

    invoke-static {v1}, Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;->d(Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;)Lcom/google/android/youtube/core/async/au;

    move-result-object v1

    invoke-static {v0, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    new-instance v2, Lcom/google/android/youtube/api/g;

    iget-object v3, p0, Lcom/google/android/youtube/api/i;->a:Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;

    iget-object v4, p0, Lcom/google/android/youtube/api/i;->b:Lcom/google/android/youtube/core/async/n;

    invoke-direct {v2, v3, v4}, Lcom/google/android/youtube/api/g;-><init>(Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;Lcom/google/android/youtube/core/async/n;)V

    invoke-interface {v1, v0, v2}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method
