.class public final Lcom/google/android/youtube/api/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/a;
.implements Lcom/google/android/youtube/core/client/bd;
.implements Lcom/google/android/youtube/core/client/bf;
.implements Lcom/google/android/youtube/core/client/bm;
.implements Lcom/google/android/youtube/core/f;
.implements Lcom/google/android/youtube/core/player/am;
.implements Lcom/google/android/youtube/core/player/z;
.implements Lcom/google/android/youtube/core/utils/q;


# static fields
.field private static final a:Ljava/util/Map;

.field private static final b:Ljava/util/concurrent/atomic/AtomicReference;


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:Lcom/google/android/youtube/api/n;

.field private final e:Lcom/google/android/youtube/core/utils/ag;

.field private final f:Landroid/os/Handler;

.field private final g:Ljava/util/concurrent/Executor;

.field private final h:Lorg/apache/http/client/HttpClient;

.field private final i:Lorg/apache/http/client/HttpClient;

.field private final j:Lcom/google/android/youtube/core/converter/m;

.field private final k:Lcom/google/android/youtube/api/ab;

.field private final l:Lcom/google/android/youtube/core/client/d;

.field private final m:Lcom/google/android/youtube/core/client/be;

.field private final n:Lcom/google/android/youtube/core/client/bl;

.field private final o:Lcom/google/android/youtube/api/a;

.field private final p:Lcom/google/android/youtube/core/player/ak;

.field private final q:Lcom/google/android/youtube/core/Analytics;

.field private final r:Landroid/content/SharedPreferences;

.field private final s:Lcom/google/android/youtube/core/utils/p;

.field private final t:Lcom/google/android/youtube/core/e;

.field private final u:Lcom/google/android/youtube/core/player/c;

.field private final v:Lcom/google/android/youtube/core/player/a;

.field private final w:Lcom/google/android/youtube/core/client/bo;

.field private x:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/youtube/api/j;->a:Ljava/util/Map;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    sput-object v0, Lcom/google/android/youtube/api/j;->b:Ljava/util/concurrent/atomic/AtomicReference;

    const-string v0, "YouTubeAndroidPlayerAPI"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/youtube/api/n;)V
    .locals 18

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    const-string v1, "application cannot be null"

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/api/j;->c:Landroid/content/Context;

    const-string v1, "clientIdentifier cannot be null"

    move-object/from16 v0, p3

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/api/n;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/api/j;->d:Lcom/google/android/youtube/api/n;

    invoke-static {}, Lcom/google/android/youtube/core/utils/s;->a()V

    const-string v1, "youtube"

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/api/j;->r:Landroid/content/SharedPreferences;

    new-instance v1, Lcom/google/android/youtube/core/utils/ag;

    invoke-direct {v1}, Lcom/google/android/youtube/core/utils/ag;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/api/j;->e:Lcom/google/android/youtube/core/utils/ag;

    new-instance v1, Landroid/os/Handler;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/api/j;->f:Landroid/os/Handler;

    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    const/16 v2, 0x10

    const/16 v3, 0x10

    const-wide/16 v4, 0x3c

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v7}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    new-instance v8, Lcom/google/android/youtube/core/utils/x;

    const/4 v9, 0x1

    invoke-direct {v8, v9}, Lcom/google/android/youtube/core/utils/x;-><init>(I)V

    invoke-direct/range {v1 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/api/j;->g:Ljava/util/concurrent/Executor;

    move-object/from16 v0, p3

    iget-object v1, v0, Lcom/google/android/youtube/api/n;->a:Ljava/lang/String;

    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/google/android/youtube/api/n;->b:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "com.google.android.youtube.player"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v4, 0x2f

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v4, 0x20

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v4, 0x2f

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-static/range {p1 .. p1}, Lcom/google/android/youtube/player/internal/util/b;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v4, 0x20

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x2f

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x20

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, "(Linux; U; Android "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "; "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    const-string v2, "; "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    sget-object v1, Landroid/os/Build;->ID:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_1

    const-string v2, " Build/"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const/16 v1, 0x29

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/utils/ao;->a(Ljava/lang/String;)Lorg/apache/http/client/HttpClient;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/api/j;->h:Lorg/apache/http/client/HttpClient;

    invoke-static {v1}, Lcom/google/android/youtube/core/utils/ao;->b(Ljava/lang/String;)Lorg/apache/http/client/HttpClient;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/api/j;->i:Lorg/apache/http/client/HttpClient;

    invoke-static {}, Lcom/google/android/youtube/core/converter/m;->a()Lcom/google/android/youtube/core/converter/m;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/api/j;->j:Lcom/google/android/youtube/core/converter/m;

    new-instance v2, Lcom/google/android/youtube/core/utils/h;

    const-string v1, "connectivity"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    invoke-direct {v2, v1}, Lcom/google/android/youtube/core/utils/h;-><init>(Landroid/net/ConnectivityManager;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/api/j;->s:Lcom/google/android/youtube/core/utils/p;

    new-instance v1, Lcom/google/android/youtube/core/d;

    new-instance v2, Lcom/google/android/youtube/core/client/az;

    invoke-direct {v2}, Lcom/google/android/youtube/core/client/az;-><init>()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/api/j;->s:Lcom/google/android/youtube/core/utils/p;

    invoke-direct {v1, v2, v3}, Lcom/google/android/youtube/core/d;-><init>(Lcom/google/android/youtube/core/client/e;Lcom/google/android/youtube/core/utils/p;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/api/j;->q:Lcom/google/android/youtube/core/Analytics;

    new-instance v1, Lcom/google/android/youtube/core/e;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/api/j;->s:Lcom/google/android/youtube/core/utils/p;

    move-object/from16 v0, p1

    invoke-direct {v1, v0, v2}, Lcom/google/android/youtube/core/e;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/utils/p;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/api/j;->t:Lcom/google/android/youtube/core/e;

    new-instance v1, Lcom/google/android/youtube/core/client/aj;

    const/16 v2, 0x78

    const/16 v3, 0x1e0

    const/16 v4, 0x53

    const/4 v5, 0x1

    const/4 v6, 0x1

    invoke-direct/range {v1 .. v6}, Lcom/google/android/youtube/core/client/aj;-><init>(IIIZZ)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/api/j;->g:Ljava/util/concurrent/Executor;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/api/j;->g:Ljava/util/concurrent/Executor;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/api/j;->h:Lorg/apache/http/client/HttpClient;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/api/j;->e:Lcom/google/android/youtube/core/utils/ag;

    const/16 v7, 0x46

    const/16 v8, 0x1e

    move-object v6, v1

    invoke-static/range {v2 .. v8}, Lcom/google/android/youtube/core/client/ai;->a(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/utils/e;Lcom/google/android/youtube/core/client/aj;II)Lcom/google/android/youtube/core/client/ai;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/api/j;->m:Lcom/google/android/youtube/core/client/be;

    new-instance v1, Lcom/google/android/youtube/core/client/ap;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/api/j;->g:Ljava/util/concurrent/Executor;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/api/j;->h:Lorg/apache/http/client/HttpClient;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/api/j;->j:Lcom/google/android/youtube/core/converter/m;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/api/j;->e:Lcom/google/android/youtube/core/utils/ag;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/youtube/core/client/ap;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/m;Lcom/google/android/youtube/core/utils/e;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/api/j;->n:Lcom/google/android/youtube/core/client/bl;

    new-instance v4, Lcom/google/android/youtube/core/g;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "youtube"

    invoke-direct {v4, v1, v2}, Lcom/google/android/youtube/core/g;-><init>(Landroid/content/ContentResolver;Ljava/lang/String;)V

    invoke-static/range {p1 .. p1}, Lcom/google/android/youtube/core/utils/j;->d(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v2, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v3, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    int-to-float v2, v2

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    div-float v1, v2, v1

    float-to-int v1, v1

    const/16 v2, 0x258

    if-lt v1, v2, :cond_3

    :cond_2
    const/4 v1, 0x1

    move v3, v1

    :goto_0
    new-instance v1, Lcom/google/android/youtube/core/player/k;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/api/j;->s:Lcom/google/android/youtube/core/utils/p;

    if-eqz v3, :cond_4

    invoke-interface {v4}, Lcom/google/android/youtube/core/c;->C()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x1

    :goto_1
    const/4 v4, 0x0

    invoke-static/range {p1 .. p1}, Lcom/google/android/youtube/core/utils/Util;->e(Landroid/content/Context;)Z

    move-result v5

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, Lcom/google/android/youtube/core/player/k;-><init>(Lcom/google/android/youtube/core/utils/p;ZZZZ)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/api/j;->p:Lcom/google/android/youtube/core/player/ak;

    new-instance v10, Lcom/google/android/youtube/core/async/GDataRequestFactory;

    const/16 v1, 0xf

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v10, v1, v2, v3}, Lcom/google/android/youtube/core/async/GDataRequestFactory;-><init>(ILcom/google/android/youtube/core/utils/SafeSearch;Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/api/j;->g:Ljava/util/concurrent/Executor;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/api/j;->h:Lorg/apache/http/client/HttpClient;

    move-object/from16 v0, p3

    iget-object v5, v0, Lcom/google/android/youtube/api/n;->c:Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v6, "android_id"

    invoke-static {v2, v6}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/google/android/youtube/api/ApiDeviceRegistrationClient;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Lcom/google/android/youtube/api/a;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/api/j;->g:Ljava/util/concurrent/Executor;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/api/j;->f:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/api/j;->r:Landroid/content/SharedPreferences;

    move-object/from16 v0, p3

    iget-object v7, v0, Lcom/google/android/youtube/api/n;->a:Ljava/lang/String;

    move-object/from16 v0, p3

    iget-object v8, v0, Lcom/google/android/youtube/api/n;->b:Ljava/lang/String;

    move-object/from16 v0, p3

    iget-object v9, v0, Lcom/google/android/youtube/api/n;->c:Ljava/lang/String;

    move-object v3, v1

    invoke-direct/range {v2 .. v9}, Lcom/google/android/youtube/api/a;-><init>(Lcom/google/android/youtube/core/client/as;Ljava/util/concurrent/Executor;Landroid/os/Handler;Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/api/j;->o:Lcom/google/android/youtube/api/a;

    new-instance v1, Lcom/google/android/youtube/api/ab;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/api/j;->g:Ljava/util/concurrent/Executor;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/api/j;->h:Lorg/apache/http/client/HttpClient;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/api/j;->e:Lcom/google/android/youtube/core/utils/ag;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/api/j;->j:Lcom/google/android/youtube/core/converter/m;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/youtube/api/j;->o:Lcom/google/android/youtube/api/a;

    sget-object v8, Lcom/google/android/youtube/core/async/GDataRequest$Version;->V_2_1:Lcom/google/android/youtube/core/async/GDataRequest$Version;

    move-object v6, v10

    invoke-direct/range {v1 .. v8}, Lcom/google/android/youtube/api/ab;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/utils/e;Lcom/google/android/youtube/core/converter/m;Lcom/google/android/youtube/core/async/GDataRequestFactory;Lcom/google/android/youtube/core/async/DeviceAuthorizer;Lcom/google/android/youtube/core/async/GDataRequest$Version;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/api/j;->k:Lcom/google/android/youtube/api/ab;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/pm/PackageManager;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v11, "gtv"

    :goto_2
    new-instance v1, Lcom/google/android/youtube/core/client/l;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/api/j;->g:Ljava/util/concurrent/Executor;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/api/j;->e:Lcom/google/android/youtube/core/utils/ag;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/api/j;->r:Landroid/content/SharedPreferences;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/api/j;->i:Lorg/apache/http/client/HttpClient;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/youtube/api/j;->k:Lcom/google/android/youtube/api/ab;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/youtube/api/j;->j:Lcom/google/android/youtube/core/converter/m;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/youtube/api/j;->p:Lcom/google/android/youtube/core/player/ak;

    new-instance v10, Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v2, 0x0

    invoke-direct {v10, v2}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    invoke-static/range {p1 .. p1}, Lcom/google/android/youtube/player/internal/util/b;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p3

    iget-object v13, v0, Lcom/google/android/youtube/api/n;->a:Ljava/lang/String;

    invoke-static/range {p1 .. p1}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    const-string v16, "googleads.g.doubleclick.net"

    const-string v17, "/pagead/ads"

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v17}, Lcom/google/android/youtube/core/client/l;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/utils/e;Landroid/content/SharedPreferences;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/converter/m;Lcom/google/android/youtube/core/player/ak;Ljava/util/concurrent/atomic/AtomicReference;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/api/j;->l:Lcom/google/android/youtube/core/client/d;

    new-instance v1, Lcom/google/android/youtube/core/player/c;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/api/j;->e:Lcom/google/android/youtube/core/utils/ag;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/google/android/youtube/core/player/c;-><init>(Lcom/google/android/youtube/core/utils/e;Z)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/api/j;->u:Lcom/google/android/youtube/core/player/c;

    new-instance v1, Lcom/google/android/youtube/api/ac;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/api/j;->f:Landroid/os/Handler;

    invoke-direct {v1, v2}, Lcom/google/android/youtube/api/ac;-><init>(Landroid/os/Handler;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/api/j;->v:Lcom/google/android/youtube/core/player/a;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/utils/Util;->b(Landroid/content/pm/PackageManager;)Z

    move-result v1

    if-eqz v1, :cond_6

    sget-object v1, Lcom/google/android/youtube/core/client/StatParams$Platform;->TV:Lcom/google/android/youtube/core/client/StatParams$Platform;

    :goto_3
    new-instance v7, Lcom/google/android/youtube/core/client/StatParams;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".api"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static/range {p1 .. p1}, Lcom/google/android/youtube/player/internal/util/b;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/google/android/youtube/core/client/StatParams$SoftwareInterface;->OTHERAPP:Lcom/google/android/youtube/core/client/StatParams$SoftwareInterface;

    invoke-direct {v7, v2, v3, v1, v4}, Lcom/google/android/youtube/core/client/StatParams;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/core/client/StatParams$Platform;Lcom/google/android/youtube/core/client/StatParams$SoftwareInterface;)V

    new-instance v1, Lcom/google/android/youtube/core/client/i;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/api/j;->e:Lcom/google/android/youtube/core/utils/ag;

    new-instance v3, Ljava/security/SecureRandom;

    invoke-direct {v3}, Ljava/security/SecureRandom;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/api/j;->g:Ljava/util/concurrent/Executor;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/api/j;->h:Lorg/apache/http/client/HttpClient;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/api/j;->o:Lcom/google/android/youtube/api/a;

    invoke-direct/range {v1 .. v7}, Lcom/google/android/youtube/core/client/i;-><init>(Lcom/google/android/youtube/core/utils/e;Ljava/util/Random;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/async/DeviceAuthorizer;Lcom/google/android/youtube/core/client/StatParams;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/api/j;->w:Lcom/google/android/youtube/core/client/bo;

    return-void

    :cond_3
    const/4 v1, 0x0

    move v3, v1

    goto/16 :goto_0

    :cond_4
    const/4 v3, 0x0

    goto/16 :goto_1

    :cond_5
    const-string v11, "android"

    goto/16 :goto_2

    :cond_6
    invoke-static/range {p1 .. p1}, Lcom/google/android/youtube/core/utils/j;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_7

    sget-object v1, Lcom/google/android/youtube/core/client/StatParams$Platform;->TABLET:Lcom/google/android/youtube/core/client/StatParams$Platform;

    goto :goto_3

    :cond_7
    sget-object v1, Lcom/google/android/youtube/core/client/StatParams$Platform;->MOBILE:Lcom/google/android/youtube/core/client/StatParams$Platform;

    goto :goto_3
.end method

.method static synthetic a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/youtube/api/j;
    .locals 4

    new-instance v1, Lcom/google/android/youtube/api/n;

    invoke-direct {v1, p2, p3, p1}, Lcom/google/android/youtube/api/n;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/youtube/api/j;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/api/j;

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/youtube/api/j;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/api/j;

    if-eqz v0, :cond_1

    iget-object v2, v0, Lcom/google/android/youtube/api/j;->d:Lcom/google/android/youtube/api/n;

    invoke-virtual {v2, v1}, Lcom/google/android/youtube/api/n;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, Lcom/google/android/youtube/api/j;->b:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    :goto_0
    sget-object v2, Lcom/google/android/youtube/api/j;->a:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/google/android/youtube/api/j;

    invoke-direct {v0, p0, p4, v1}, Lcom/google/android/youtube/api/j;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/youtube/api/n;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/Exception;)Lcom/google/android/youtube/player/YouTubeInitializationResult;
    .locals 1

    instance-of v0, p0, Lcom/google/android/youtube/api/ApiDeviceRegistrationClient$IllegalPackageSignatureException;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/youtube/player/YouTubeInitializationResult;->INVALID_APPLICATION_SIGNATURE:Lcom/google/android/youtube/player/YouTubeInitializationResult;

    :goto_0
    return-object v0

    :cond_0
    instance-of v0, p0, Lcom/google/android/youtube/api/ApiDeviceRegistrationClient$InvalidDeveloperException;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/youtube/player/YouTubeInitializationResult;->DEVELOPER_KEY_INVALID:Lcom/google/android/youtube/player/YouTubeInitializationResult;

    goto :goto_0

    :cond_1
    instance-of v0, p0, Ljava/net/UnknownHostException;

    if-nez v0, :cond_2

    instance-of v0, p0, Ljava/net/SocketException;

    if-nez v0, :cond_2

    instance-of v0, p0, Lorg/apache/http/client/HttpResponseException;

    if-eqz v0, :cond_3

    :cond_2
    sget-object v0, Lcom/google/android/youtube/player/YouTubeInitializationResult;->NETWORK_ERROR:Lcom/google/android/youtube/player/YouTubeInitializationResult;

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/google/android/youtube/player/YouTubeInitializationResult;->INTERNAL_ERROR:Lcom/google/android/youtube/player/YouTubeInitializationResult;

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/youtube/api/j;)V
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/api/j;->x:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/youtube/api/j;->x:I

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/api/j;Lcom/google/android/youtube/api/m;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/j;->o:Lcom/google/android/youtube/api/a;

    new-instance v1, Lcom/google/android/youtube/api/l;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/l;-><init>(Lcom/google/android/youtube/api/j;Lcom/google/android/youtube/api/m;)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/api/a;->a(Lcom/google/android/youtube/api/f;)V

    return-void
.end method

.method public static a(Lcom/google/android/youtube/api/m;Landroid/os/Handler;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    new-instance v0, Lcom/google/android/youtube/api/k;

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object v6, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/api/k;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/api/m;)V

    invoke-virtual {p1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public static a(Ljava/lang/String;)V
    .locals 3

    sget-object v0, Lcom/google/android/youtube/api/j;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/api/j;

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/android/youtube/api/j;->d:Lcom/google/android/youtube/api/n;

    iget-object v1, v1, Lcom/google/android/youtube/api/n;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/youtube/api/j;->b:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/youtube/core/client/bc;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/j;->k:Lcom/google/android/youtube/api/ab;

    return-object v0
.end method

.method public final a(Z)V
    .locals 2

    iget v0, p0, Lcom/google/android/youtube/api/j;->x:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/api/j;->x:I

    iget v0, p0, Lcom/google/android/youtube/api/j;->x:I

    if-gtz v0, :cond_0

    sget-object v0, Lcom/google/android/youtube/api/j;->a:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/youtube/api/j;->d:Lcom/google/android/youtube/api/n;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p1, :cond_0

    sget-object v0, Lcom/google/android/youtube/api/j;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public final b()Lcom/google/android/youtube/core/client/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/j;->l:Lcom/google/android/youtube/core/client/d;

    return-object v0
.end method

.method public final d()Lcom/google/android/youtube/core/client/bo;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/j;->w:Lcom/google/android/youtube/core/client/bo;

    return-object v0
.end method

.method public final e()Lcom/google/android/youtube/core/client/b;
    .locals 3

    new-instance v0, Lcom/google/android/youtube/core/client/k;

    iget-object v1, p0, Lcom/google/android/youtube/api/j;->g:Ljava/util/concurrent/Executor;

    iget-object v2, p0, Lcom/google/android/youtube/api/j;->h:Lorg/apache/http/client/HttpClient;

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/client/k;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;)V

    return-object v0
.end method

.method public final e_()Lcom/google/android/youtube/core/client/be;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/j;->m:Lcom/google/android/youtube/core/client/be;

    return-object v0
.end method

.method public final f()Lcom/google/android/youtube/core/client/bj;
    .locals 4

    new-instance v0, Lcom/google/android/youtube/core/client/an;

    iget-object v1, p0, Lcom/google/android/youtube/api/j;->e:Lcom/google/android/youtube/core/utils/ag;

    iget-object v2, p0, Lcom/google/android/youtube/api/j;->g:Ljava/util/concurrent/Executor;

    iget-object v3, p0, Lcom/google/android/youtube/api/j;->h:Lorg/apache/http/client/HttpClient;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/youtube/core/client/an;-><init>(Lcom/google/android/youtube/core/utils/e;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;)V

    return-object v0
.end method

.method public final g()Lcom/google/android/youtube/core/client/bl;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/j;->n:Lcom/google/android/youtube/core/client/bl;

    return-object v0
.end method

.method public final h()Lcom/google/android/youtube/core/player/ak;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/j;->p:Lcom/google/android/youtube/core/player/ak;

    return-object v0
.end method

.method public final i()Lcom/google/android/youtube/core/Analytics;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/j;->q:Lcom/google/android/youtube/core/Analytics;

    return-object v0
.end method

.method public final j()Lcom/google/android/youtube/core/e;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/j;->t:Lcom/google/android/youtube/core/e;

    return-object v0
.end method

.method public final k()Landroid/content/SharedPreferences;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/j;->r:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method public final l()Lcom/google/android/youtube/core/utils/p;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/j;->s:Lcom/google/android/youtube/core/utils/p;

    return-object v0
.end method

.method public final m()Lcom/google/android/youtube/core/player/c;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/j;->u:Lcom/google/android/youtube/core/player/c;

    return-object v0
.end method

.method public final n()Lcom/google/android/youtube/core/player/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/j;->v:Lcom/google/android/youtube/core/player/a;

    return-object v0
.end method

.method public final o()Lcom/google/android/youtube/core/player/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/j;->v:Lcom/google/android/youtube/core/player/a;

    return-object v0
.end method
