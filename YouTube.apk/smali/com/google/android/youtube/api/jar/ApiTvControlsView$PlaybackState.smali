.class public final enum Lcom/google/android/youtube/api/jar/ApiTvControlsView$PlaybackState;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/youtube/api/jar/ApiTvControlsView$PlaybackState;

.field public static final enum PAUSED:Lcom/google/android/youtube/api/jar/ApiTvControlsView$PlaybackState;

.field public static final enum PLAYING:Lcom/google/android/youtube/api/jar/ApiTvControlsView$PlaybackState;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/youtube/api/jar/ApiTvControlsView$PlaybackState;

    const-string v1, "PLAYING"

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/api/jar/ApiTvControlsView$PlaybackState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/api/jar/ApiTvControlsView$PlaybackState;->PLAYING:Lcom/google/android/youtube/api/jar/ApiTvControlsView$PlaybackState;

    new-instance v0, Lcom/google/android/youtube/api/jar/ApiTvControlsView$PlaybackState;

    const-string v1, "PAUSED"

    invoke-direct {v0, v1, v3}, Lcom/google/android/youtube/api/jar/ApiTvControlsView$PlaybackState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/api/jar/ApiTvControlsView$PlaybackState;->PAUSED:Lcom/google/android/youtube/api/jar/ApiTvControlsView$PlaybackState;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/youtube/api/jar/ApiTvControlsView$PlaybackState;

    sget-object v1, Lcom/google/android/youtube/api/jar/ApiTvControlsView$PlaybackState;->PLAYING:Lcom/google/android/youtube/api/jar/ApiTvControlsView$PlaybackState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/youtube/api/jar/ApiTvControlsView$PlaybackState;->PAUSED:Lcom/google/android/youtube/api/jar/ApiTvControlsView$PlaybackState;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/youtube/api/jar/ApiTvControlsView$PlaybackState;->$VALUES:[Lcom/google/android/youtube/api/jar/ApiTvControlsView$PlaybackState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/youtube/api/jar/ApiTvControlsView$PlaybackState;
    .locals 1

    const-class v0, Lcom/google/android/youtube/api/jar/ApiTvControlsView$PlaybackState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/api/jar/ApiTvControlsView$PlaybackState;

    return-object v0
.end method

.method public static values()[Lcom/google/android/youtube/api/jar/ApiTvControlsView$PlaybackState;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/api/jar/ApiTvControlsView$PlaybackState;->$VALUES:[Lcom/google/android/youtube/api/jar/ApiTvControlsView$PlaybackState;

    invoke-virtual {v0}, [Lcom/google/android/youtube/api/jar/ApiTvControlsView$PlaybackState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/youtube/api/jar/ApiTvControlsView$PlaybackState;

    return-object v0
.end method
