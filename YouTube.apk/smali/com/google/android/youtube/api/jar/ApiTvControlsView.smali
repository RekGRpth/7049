.class public Lcom/google/android/youtube/api/jar/ApiTvControlsView;
.super Landroid/widget/LinearLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final a:Ljava/util/Map;

.field private static final b:Ljava/util/Map;


# instance fields
.field private final c:Landroid/widget/ToggleButton;

.field private final d:Landroid/widget/Button;

.field private final e:Ljava/util/Map;

.field private final f:Ljava/util/Set;

.field private final g:Ljava/util/Set;

.field private final h:Ljava/util/Set;

.field private final i:Lcom/google/android/youtube/api/jar/n;

.field private j:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;

.field private k:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

.field private l:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const v1, 0x7f07004a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->PLAY:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const v1, 0x7f070048

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->PREVIOUS:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const v1, 0x7f07004c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->NEXT:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const v1, 0x7f070049

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->REWIND:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const v1, 0x7f07004b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->FAST_FORWARD:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const v1, 0x7f07004f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->CC:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const v1, 0x7f070050

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->HOME:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const v1, 0x7f07003e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->SCROLL:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->a:Ljava/util/Map;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sget-object v0, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->b:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    sget-object v0, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->YOUTUBE:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->k:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04000f

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    new-instance v0, Lcom/google/android/youtube/api/jar/n;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/api/jar/n;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->i:Lcom/google/android/youtube/api/jar/n;

    const v0, 0x7f07004a

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->c:Landroid/widget/ToggleButton;

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->c:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p0}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget-object v0, Lcom/google/android/youtube/api/jar/ApiTvControlsView$PlaybackState;->PAUSED:Lcom/google/android/youtube/api/jar/ApiTvControlsView$PlaybackState;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->setPlaybackState(Lcom/google/android/youtube/api/jar/ApiTvControlsView$PlaybackState;)V

    const-class v0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->f:Ljava/util/Set;

    sget-object v0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->CC:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-static {v0}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->g:Ljava/util/Set;

    sget-object v0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->CC:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->HOME:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-static {v0}, Ljava/util/EnumSet;->complementOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->h:Ljava/util/Set;

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    sget-object v0, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-interface {v2, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget-object v4, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->NEXT:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    if-eq v1, v4, :cond_0

    sget-object v4, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->PREVIOUS:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    if-eq v1, v4, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-static {v2}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->e:Ljava/util/Map;

    const v0, 0x7f07004f

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->d:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->d:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f070050

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private a(Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;)V
    .locals 6

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->j:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;

    iget-object v4, p0, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->k:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    invoke-virtual {v1, p1, v4}, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->b(Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;)Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$ActionState;

    move-result-object v1

    sget-object v4, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$ActionState;->GONE:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$ActionState;

    if-eq v1, v4, :cond_2

    iget-object v4, p0, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->g:Ljava/util/Set;

    invoke-interface {v4, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    :cond_2
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_3
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    sget-object v4, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$ActionState;->ACTIVE:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$ActionState;

    if-ne v1, v4, :cond_5

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->f:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    move v1, v2

    :goto_1
    if-eqz v1, :cond_6

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->clearColorFilter()V

    :cond_4
    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->SCROLL:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    if-eq p1, v1, :cond_0

    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusable(Z)V

    goto :goto_0

    :cond_5
    move v1, v3

    goto :goto_1

    :cond_6
    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    new-instance v2, Landroid/graphics/PorterDuffColorFilter;

    const/high16 v4, -0x60000000

    sget-object v5, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v2, v4, v5}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    :cond_7
    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setFocusable(Z)V

    goto :goto_0
.end method

.method private a(ZLcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->f:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :goto_0
    invoke-direct {p0, p2}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->a(Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->f:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->i:Lcom/google/android/youtube/api/jar/n;

    invoke-virtual {v0, v1, v1, v1}, Lcom/google/android/youtube/api/jar/n;->a(III)V

    return-void
.end method

.method public final a(I)V
    .locals 2

    sget-object v0, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->a:Ljava/util/Map;

    const v1, 0x7f070050

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->g:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-direct {p0, v0}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->a(Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;)V

    return-void
.end method

.method public final a(III)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->i:Lcom/google/android/youtube/api/jar/n;

    div-int/lit16 v1, p1, 0x3e8

    div-int/lit16 v2, p2, 0x3e8

    invoke-virtual {v0, v1, v2, p3}, Lcom/google/android/youtube/api/jar/n;->a(III)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->j:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f07004a

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->c:Landroid/widget/ToggleButton;

    invoke-virtual {v0}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->PLAY:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    :cond_0
    :goto_0
    if-eqz v0, :cond_4

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->j:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->execute(Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;)V

    :cond_1
    return-void

    :cond_2
    sget-object v0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->PAUSE:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->a:Ljava/util/Map;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported onClick widget: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected onFinishInflate()V
    .locals 6

    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f0b0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v0, 0x7f07004e

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->getChildCount()I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {p0, v4}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->removeView(Landroid/view/View;)V

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    add-int/lit8 v2, v2, -0x1

    add-int/lit8 v1, v1, -0x1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 1

    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onVisibilityChanged(Landroid/view/View;I)V

    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->l:Z

    if-eqz v0, :cond_0

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->c:Landroid/widget/ToggleButton;

    invoke-virtual {v0}, Landroid/widget/ToggleButton;->requestFocus()Z

    :cond_0
    return-void
.end method

.method public setCcEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->d:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setSelected(Z)V

    return-void
.end method

.method public setErrorState(Z)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez p1, :cond_0

    move v0, v1

    :goto_0
    sget-object v3, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->PLAY:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-direct {p0, v0, v3}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->a(ZLcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;)V

    if-nez p1, :cond_1

    move v0, v1

    :goto_1
    sget-object v3, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->CC:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-direct {p0, v0, v3}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->a(ZLcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;)V

    if-nez p1, :cond_2

    move v0, v1

    :goto_2
    sget-object v3, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->FAST_FORWARD:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-direct {p0, v0, v3}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->a(ZLcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;)V

    if-nez p1, :cond_3

    move v0, v1

    :goto_3
    sget-object v3, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->REWIND:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-direct {p0, v0, v3}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->a(ZLcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;)V

    if-nez p1, :cond_4

    :goto_4
    sget-object v0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->SCROLL:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-direct {p0, v1, v0}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->a(ZLcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v1, v2

    goto :goto_4
.end method

.method public setFocus(Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;)V
    .locals 1

    sget-object v0, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    return-void
.end method

.method public setMediaActionHelper(Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;)V
    .locals 2

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->j:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->i:Lcom/google/android/youtube/api/jar/n;

    new-instance v1, Lcom/google/android/youtube/api/jar/l;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/jar/l;-><init>(Lcom/google/android/youtube/api/jar/ApiTvControlsView;Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/api/jar/n;->a(Lcom/google/android/youtube/api/jar/p;)V

    return-void
.end method

.method public setNextEnabled(Z)V
    .locals 1

    sget-object v0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->NEXT:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->a(ZLcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;)V

    return-void
.end method

.method public setPlayPauseEnabled(Z)V
    .locals 1

    sget-object v0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->PLAY:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->a(ZLcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;)V

    return-void
.end method

.method public setPlayTipState()V
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x1

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->PLAY:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->a(ZLcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;)V

    sget-object v0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->CC:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-direct {p0, v2, v0}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->a(ZLcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;)V

    sget-object v0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->FAST_FORWARD:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-direct {p0, v2, v0}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->a(ZLcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;)V

    sget-object v0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->REWIND:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-direct {p0, v2, v0}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->a(ZLcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;)V

    sget-object v0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->SCROLL:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-direct {p0, v2, v0}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->a(ZLcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;)V

    return-void
.end method

.method public setPlaybackState(Lcom/google/android/youtube/api/jar/ApiTvControlsView$PlaybackState;)V
    .locals 2

    sget-object v0, Lcom/google/android/youtube/api/jar/m;->a:[I

    invoke-virtual {p1}, Lcom/google/android/youtube/api/jar/ApiTvControlsView$PlaybackState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->c:Landroid/widget/ToggleButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setChecked(Z)V

    :goto_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->c:Landroid/widget/ToggleButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setChecked(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setPreviousEnabled(Z)V
    .locals 1

    sget-object v0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->PREVIOUS:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->a(ZLcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;)V

    return-void
.end method

.method public setScrubbingEnabled(Z)V
    .locals 1

    sget-object v0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->FAST_FORWARD:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->a(ZLcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;)V

    sget-object v0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->REWIND:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->a(ZLcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;)V

    sget-object v0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->SCROLL:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->a(ZLcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;)V

    return-void
.end method

.method public setShowCcButton(Z)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->g:Ljava/util/Set;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->CC:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    :goto_0
    sget-object v0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->CC:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->a(Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->g:Ljava/util/Set;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->CC:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public setStealFocusOnVisible(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->l:Z

    return-void
.end method

.method public setStyle(Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->k:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->i:Lcom/google/android/youtube/api/jar/n;

    iget-boolean v1, p1, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->supportsTimeBar:Z

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/api/jar/n;->a(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->h:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/api/jar/ApiTvControlsView;->a(Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;)V

    goto :goto_0

    :cond_0
    return-void
.end method
