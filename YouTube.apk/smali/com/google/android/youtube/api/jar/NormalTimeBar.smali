.class public Lcom/google/android/youtube/api/jar/NormalTimeBar;
.super Landroid/view/View;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/youtube/api/jar/ae;

.field private final b:Lcom/google/android/youtube/api/jar/ad;

.field private final c:Landroid/graphics/drawable/StateListDrawable;

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:Landroid/graphics/Rect;

.field private final i:Landroid/graphics/Rect;

.field private final j:Landroid/graphics/Rect;

.field private final k:Landroid/graphics/Paint;

.field private l:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

.field private m:Z

.field private n:Z

.field private o:J

.field private p:J

.field private q:J

.field private r:I

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/api/jar/ae;)V
    .locals 6

    const/4 v5, 0x0

    const/high16 v4, 0x3f000000

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    const-string v0, "listener cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/api/jar/ae;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->a:Lcom/google/android/youtube/api/jar/ae;

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/NormalTimeBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v2, v1, Landroid/util/DisplayMetrics;->density:F

    const/high16 v3, 0x43660000

    mul-float/2addr v3, v2

    add-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->d:I

    const/high16 v3, 0x40a00000

    mul-float/2addr v3, v2

    add-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->f:I

    const/high16 v3, 0x41400000

    mul-float/2addr v3, v2

    add-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->g:I

    const/high16 v3, 0x41000000

    mul-float/2addr v2, v3

    add-float/2addr v2, v4

    float-to-int v2, v2

    iput v2, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->e:I

    new-instance v2, Lcom/google/android/youtube/api/jar/ad;

    invoke-direct {v2, v0}, Lcom/google/android/youtube/api/jar/ad;-><init>(Landroid/content/res/Resources;)V

    iput-object v2, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->b:Lcom/google/android/youtube/api/jar/ad;

    new-instance v2, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v2}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    iput-object v2, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->c:Landroid/graphics/drawable/StateListDrawable;

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->c:Landroid/graphics/drawable/StateListDrawable;

    sget-object v3, Landroid/view/View;->PRESSED_ENABLED_STATE_SET:[I

    const v4, 0x7f02001e

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->c:Landroid/graphics/drawable/StateListDrawable;

    sget-object v3, Landroid/view/View;->ENABLED_STATE_SET:[I

    const v4, 0x7f02001d

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->c:Landroid/graphics/drawable/StateListDrawable;

    sget-object v2, Landroid/view/View;->ENABLED_STATE_SET:[I

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/StateListDrawable;->setState([I)Z

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->h:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->k:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->k:Landroid/graphics/Paint;

    const/4 v2, 0x2

    const/high16 v3, 0x41600000

    invoke-static {v2, v3, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->k:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->k:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->k:Landroid/graphics/Paint;

    const-string v1, "00:00"

    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->h:Landroid/graphics/Rect;

    invoke-virtual {v0, v1, v5, v2, v3}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->i:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->j:Landroid/graphics/Rect;

    sget-object v0, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->YOUTUBE:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/api/jar/NormalTimeBar;->setStyle(Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;)V

    invoke-virtual {p0, v5, v5, v5}, Lcom/google/android/youtube/api/jar/NormalTimeBar;->setTimes(III)V

    return-void
.end method

.method private a(I)J
    .locals 7

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->b:Lcom/google/android/youtube/api/jar/ad;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/jar/ad;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    iget v1, v0, Landroid/graphics/Rect;->left:I

    iget v2, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->g:I

    add-int/2addr v1, v2

    iget v0, v0, Landroid/graphics/Rect;->right:I

    iget v2, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->g:I

    sub-int/2addr v0, v2

    invoke-static {v1, p1}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    iget-wide v3, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->q:J

    sub-int/2addr v2, v1

    int-to-long v5, v2

    mul-long v2, v3, v5

    sub-int/2addr v0, v1

    int-to-long v0, v0

    div-long v0, v2, v0

    return-wide v0
.end method

.method private a(J)Ljava/lang/String;
    .locals 9

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    long-to-int v0, p1

    div-int/lit16 v1, v0, 0x3e8

    const-string v0, "%02d:%02d"

    new-array v2, v8, [Ljava/lang/Object;

    div-int/lit8 v3, v1, 0x3c

    rem-int/lit8 v3, v3, 0x3c

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    rem-int/lit8 v3, v1, 0x3c

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    div-int/lit16 v1, v1, 0xe10

    iget-wide v2, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->q:J

    const-wide/32 v4, 0x36ee80

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    const-string v2, "%d:%s"

    new-array v3, v8, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v6

    aput-object v0, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private a()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->l:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    iget-boolean v0, v0, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->supportsScrubber:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/NormalTimeBar;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->q:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->m:Z

    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->n:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->m:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/NormalTimeBar;->b()V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/NormalTimeBar;->c()V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(II)V
    .locals 8

    const/4 v7, 0x0

    iget-wide v0, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->q:J

    const-wide/32 v2, 0x2255100

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const-string v0, "10:00:00"

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->k:Landroid/graphics/Paint;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->h:Landroid/graphics/Rect;

    invoke-virtual {v1, v0, v7, v2, v3}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->b:Lcom/google/android/youtube/api/jar/ad;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/jar/ad;->getIntrinsicHeight()I

    move-result v0

    iget v1, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->d:I

    if-lt p1, v1, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->b:Lcom/google/android/youtube/api/jar/ad;

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->h:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    iget v3, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->e:I

    add-int/2addr v2, v3

    div-int/lit8 v3, p2, 0x2

    div-int/lit8 v4, v0, 0x2

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->e:I

    sub-int v4, p1, v4

    iget-object v5, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->h:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    sub-int/2addr v4, v5

    div-int/lit8 v5, p2, 0x2

    div-int/lit8 v6, v0, 0x2

    sub-int/2addr v5, v6

    add-int/2addr v5, v0

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/google/android/youtube/api/jar/ad;->setBounds(IIII)V

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->h:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->b:Lcom/google/android/youtube/api/jar/ad;

    invoke-virtual {v2}, Lcom/google/android/youtube/api/jar/ad;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->h:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->centerY()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {v1, v7, v2}, Landroid/graphics/Rect;->offset(II)V

    :goto_1
    iget-object v1, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->b:Lcom/google/android/youtube/api/jar/ad;

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->j:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/api/jar/ad;->copyBounds(Landroid/graphics/Rect;)V

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->j:Landroid/graphics/Rect;

    sub-int v0, p2, v0

    neg-int v0, v0

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {v1, v7, v0}, Landroid/graphics/Rect;->inset(II)V

    return-void

    :cond_0
    iget-wide v0, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->q:J

    const-wide/32 v2, 0x36ee80

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    const-string v0, "0:00:00"

    goto :goto_0

    :cond_1
    const-string v0, "00:00"

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->b:Lcom/google/android/youtube/api/jar/ad;

    iget v2, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->e:I

    div-int/lit8 v3, p2, 0x2

    div-int/lit8 v4, v0, 0x2

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->e:I

    sub-int v4, p1, v4

    div-int/lit8 v5, p2, 0x2

    div-int/lit8 v6, v0, 0x2

    sub-int/2addr v5, v6

    add-int/2addr v5, v0

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/google/android/youtube/api/jar/ad;->setBounds(IIII)V

    goto :goto_1
.end method

.method private b()V
    .locals 3

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->n:Z

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->c:Landroid/graphics/drawable/StateListDrawable;

    sget-object v1, Lcom/google/android/youtube/api/jar/NormalTimeBar;->ENABLED_STATE_SET:[I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/StateListDrawable;->setState([I)Z

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/NormalTimeBar;->invalidate()V

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/NormalTimeBar;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    return-void
.end method

.method private c()V
    .locals 15

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->b:Lcom/google/android/youtube/api/jar/ad;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/jar/ad;->getBounds()Landroid/graphics/Rect;

    move-result-object v5

    iget v6, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->g:I

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v0

    iget v1, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->g:I

    sub-int v7, v0, v1

    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->n:Z

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->o:J

    :goto_0
    iget-wide v8, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->q:J

    cmp-long v8, v8, v2

    if-nez v8, :cond_2

    :goto_1
    long-to-int v2, v2

    iget-object v3, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->c:Landroid/graphics/drawable/StateListDrawable;

    iget v8, v5, Landroid/graphics/Rect;->left:I

    add-int/2addr v8, v2

    invoke-virtual {v5}, Landroid/graphics/Rect;->centerY()I

    move-result v9

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v10

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v11

    div-int/lit8 v12, v10, 0x2

    sub-int v12, v8, v12

    div-int/lit8 v13, v11, 0x2

    sub-int v13, v9, v13

    div-int/lit8 v14, v10, 0x2

    sub-int/2addr v8, v14

    add-int/2addr v8, v10

    div-int/lit8 v10, v11, 0x2

    sub-int/2addr v9, v10

    add-int/2addr v9, v11

    invoke-virtual {v3, v12, v13, v8, v9}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v3, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->c:Landroid/graphics/drawable/StateListDrawable;

    iget-object v8, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->i:Landroid/graphics/Rect;

    invoke-virtual {v3, v8}, Landroid/graphics/drawable/StateListDrawable;->copyBounds(Landroid/graphics/Rect;)V

    iget-object v3, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->i:Landroid/graphics/Rect;

    iget v8, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->f:I

    neg-int v8, v8

    iget v9, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->f:I

    neg-int v9, v9

    invoke-virtual {v3, v8, v9}, Landroid/graphics/Rect;->inset(II)V

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v3

    if-lez v3, :cond_3

    mul-int/lit8 v3, v2, 0x64

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    div-int/2addr v3, v5

    :goto_2
    sub-int v5, v7, v6

    if-lez v5, :cond_4

    sub-int/2addr v2, v6

    mul-int/lit8 v2, v2, 0x64

    sub-int v4, v7, v6

    div-int/2addr v2, v4

    :goto_3
    iget-boolean v4, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->m:Z

    if-eqz v4, :cond_0

    move v2, v3

    :cond_0
    iget-object v3, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->b:Lcom/google/android/youtube/api/jar/ad;

    iget v4, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->r:I

    mul-int/lit16 v4, v4, 0x3e8

    add-int/2addr v2, v4

    invoke-virtual {v3, v2}, Lcom/google/android/youtube/api/jar/ad;->setLevel(I)Z

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/api/jar/NormalTimeBar;->a(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->s:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/NormalTimeBar;->invalidate()V

    return-void

    :cond_1
    iget-wide v0, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->p:J

    goto :goto_0

    :cond_2
    int-to-long v2, v6

    sub-int v8, v7, v6

    int-to-long v8, v8

    mul-long/2addr v8, v0

    iget-wide v10, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->q:J

    div-long/2addr v8, v10

    add-long/2addr v2, v8

    goto :goto_1

    :cond_3
    move v3, v4

    goto :goto_2

    :cond_4
    move v2, v4

    goto :goto_3
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->b:Lcom/google/android/youtube/api/jar/ad;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/api/jar/ad;->draw(Landroid/graphics/Canvas;)V

    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->m:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->c:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/StateListDrawable;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/NormalTimeBar;->getWidth()I

    move-result v0

    iget v1, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->d:I

    if-lt v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->k:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->s:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->h:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->h:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->k:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->k:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->t:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/NormalTimeBar;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->h:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->k:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->i:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->j:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->h:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->b:Lcom/google/android/youtube/api/jar/ad;

    invoke-virtual {v1}, Lcom/google/android/youtube/api/jar/ad;->getIntrinsicWidth()I

    move-result v1

    invoke-static {v1, p1}, Lcom/google/android/youtube/api/jar/NormalTimeBar;->getDefaultSize(II)I

    move-result v1

    invoke-static {v0, p2}, Lcom/google/android/youtube/api/jar/NormalTimeBar;->getDefaultSize(II)I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/youtube/api/jar/NormalTimeBar;->setMeasuredDimension(II)V

    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/api/jar/NormalTimeBar;->a(II)V

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/NormalTimeBar;->c()V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    move v0, v1

    :goto_0
    return v0

    :pswitch_0
    iget-boolean v2, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->n:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->m:Z

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    iget-object v4, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->i:Landroid/graphics/Rect;

    invoke-virtual {v4, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->j:Landroid/graphics/Rect;

    invoke-virtual {v4, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    move v2, v0

    :goto_1
    if-eqz v2, :cond_0

    iput-boolean v0, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->n:Z

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/NormalTimeBar;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->c:Landroid/graphics/drawable/StateListDrawable;

    sget-object v2, Landroid/view/View;->PRESSED_ENABLED_STATE_SET:[I

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/StateListDrawable;->setState([I)Z

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-direct {p0, v1}, Lcom/google/android/youtube/api/jar/NormalTimeBar;->a(I)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->o:J

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/NormalTimeBar;->c()V

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->a:Lcom/google/android/youtube/api/jar/ae;

    invoke-interface {v1}, Lcom/google/android/youtube/api/jar/ae;->a()V

    goto :goto_0

    :cond_2
    move v2, v1

    goto :goto_1

    :pswitch_1
    iget-boolean v2, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->n:Z

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-direct {p0, v1}, Lcom/google/android/youtube/api/jar/NormalTimeBar;->a(I)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->o:J

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/NormalTimeBar;->c()V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/NormalTimeBar;->b()V

    goto :goto_0

    :pswitch_3
    iget-boolean v2, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->n:Z

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/NormalTimeBar;->b()V

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->a:Lcom/google/android/youtube/api/jar/ae;

    iget-wide v2, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->o:J

    long-to-int v2, v2

    invoke-interface {v1, v2}, Lcom/google/android/youtube/api/jar/ae;->a(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setEnabled(Z)V
    .locals 0

    invoke-super {p0, p1}, Landroid/view/View;->setEnabled(Z)V

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/NormalTimeBar;->a()V

    return-void
.end method

.method public setScrubberTime(I)V
    .locals 2

    int-to-long v0, p1

    iput-wide v0, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->o:J

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/NormalTimeBar;->c()V

    return-void
.end method

.method public setScrubbing(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->n:Z

    return-void
.end method

.method public setStyle(Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->l:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/NormalTimeBar;->a()V

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->b:Lcom/google/android/youtube/api/jar/ad;

    sget-object v0, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->AD:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    if-ne p1, v0, :cond_0

    sget-object v0, Lcom/google/android/youtube/api/jar/af;->b:[I

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/youtube/api/jar/ad;->setState([I)Z

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/NormalTimeBar;->invalidate()V

    return-void

    :cond_0
    sget-object v0, Lcom/google/android/youtube/api/jar/af;->a:[I

    goto :goto_0
.end method

.method public setTimes(III)V
    .locals 6

    const-wide/32 v4, 0x36ee80

    iget-wide v0, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->q:J

    int-to-long v2, p1

    iput-wide v2, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->p:J

    iput p3, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->r:I

    int-to-long v2, p2

    iput-wide v2, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->q:J

    int-to-long v2, p2

    cmp-long v2, v2, v0

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->t:Ljava/lang/String;

    if-nez v2, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/NormalTimeBar;->a()V

    int-to-long v2, p2

    invoke-direct {p0, v2, v3}, Lcom/google/android/youtube/api/jar/NormalTimeBar;->a(J)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/api/jar/NormalTimeBar;->t:Ljava/lang/String;

    int-to-long v2, p2

    div-long/2addr v2, v4

    div-long/2addr v0, v4

    cmp-long v0, v2, v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/NormalTimeBar;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/NormalTimeBar;->getHeight()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/api/jar/NormalTimeBar;->a(II)V

    :cond_1
    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/NormalTimeBar;->c()V

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/NormalTimeBar;->invalidate()V

    return-void
.end method
