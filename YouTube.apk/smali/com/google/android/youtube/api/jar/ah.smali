.class public final Lcom/google/android/youtube/api/jar/ah;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/player/overlay/e;


# instance fields
.field private final a:Lcom/google/android/youtube/core/player/overlay/e;

.field private final b:Lcom/google/android/youtube/api/jar/z;

.field private c:Z


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/player/overlay/e;Lcom/google/android/youtube/api/jar/z;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "delegate cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/overlay/e;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/ah;->a:Lcom/google/android/youtube/core/player/overlay/e;

    const-string v0, "layoutPolice cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/api/jar/z;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/ah;->b:Lcom/google/android/youtube/api/jar/z;

    return-void
.end method

.method private m()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/ah;->c:Z

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/youtube/api/jar/ah;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ah;->b:Lcom/google/android/youtube/api/jar/z;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/jar/z;->c()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ah;->a:Lcom/google/android/youtube/core/player/overlay/e;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/e;->l()V

    :cond_1
    return-void
.end method

.method private static n()Z
    .locals 8

    const/4 v0, 0x0

    const-class v1, Landroid/view/View;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    const-class v1, Lcom/google/android/youtube/api/jar/z;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    array-length v5, v4

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_1

    aget-object v6, v4, v1

    :try_start_0
    invoke-virtual {v6}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    invoke-virtual {v6}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_1

    :catch_0
    move-exception v1

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/ah;->m()V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ah;->a:Lcom/google/android/youtube/core/player/overlay/e;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/e;->a()V

    return-void
.end method

.method public final a(I)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/ah;->m()V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ah;->a:Lcom/google/android/youtube/core/player/overlay/e;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/overlay/e;->a(I)V

    return-void
.end method

.method public final a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/api/jar/ah;->c:Z

    return-void
.end method

.method public final a_(Lcom/google/android/youtube/core/model/SubtitleTrack;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/ah;->m()V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ah;->a:Lcom/google/android/youtube/core/player/overlay/e;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/overlay/e;->a_(Lcom/google/android/youtube/core/model/SubtitleTrack;)V

    return-void
.end method

.method public final b()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/ah;->m()V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ah;->a:Lcom/google/android/youtube/core/player/overlay/e;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/e;->b()V

    return-void
.end method

.method public final b(Z)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/ah;->m()V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ah;->a:Lcom/google/android/youtube/core/player/overlay/e;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/overlay/e;->b(Z)V

    return-void
.end method

.method public final c()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/ah;->m()V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ah;->a:Lcom/google/android/youtube/core/player/overlay/e;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/e;->c()V

    return-void
.end method

.method public final d()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/ah;->m()V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ah;->a:Lcom/google/android/youtube/core/player/overlay/e;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/e;->d()V

    return-void
.end method

.method public final e()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/ah;->m()V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ah;->a:Lcom/google/android/youtube/core/player/overlay/e;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/e;->e()V

    return-void
.end method

.method public final f()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/ah;->m()V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ah;->a:Lcom/google/android/youtube/core/player/overlay/e;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/e;->f()V

    return-void
.end method

.method public final g()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/ah;->m()V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ah;->a:Lcom/google/android/youtube/core/player/overlay/e;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/e;->g()V

    return-void
.end method

.method public final h()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/ah;->m()V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ah;->a:Lcom/google/android/youtube/core/player/overlay/e;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/e;->h()V

    return-void
.end method

.method public final i()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/ah;->m()V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ah;->a:Lcom/google/android/youtube/core/player/overlay/e;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/e;->i()V

    return-void
.end method

.method public final j()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/ah;->m()V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ah;->a:Lcom/google/android/youtube/core/player/overlay/e;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/e;->j()V

    return-void
.end method

.method public final k()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/ah;->m()V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ah;->a:Lcom/google/android/youtube/core/player/overlay/e;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/e;->k()V

    return-void
.end method

.method public final l()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/ah;->a:Lcom/google/android/youtube/core/player/overlay/e;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/e;->l()V

    return-void
.end method
