.class public final Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;
.super Lcom/google/android/youtube/api/jar/client/a;
.source "SourceFile"


# instance fields
.field private i:Lcom/google/android/youtube/api/service/a/ak;

.field private final j:Lcom/google/android/youtube/api/jar/client/da;

.field private final k:Lcom/google/android/youtube/api/jar/client/dy;

.field private final l:Lcom/google/android/youtube/api/jar/client/ek;

.field private final m:Lcom/google/android/youtube/api/jar/client/cv;

.field private final n:Lcom/google/android/youtube/api/jar/client/v;

.field private final o:Lcom/google/android/youtube/api/jar/client/ac;

.field private final p:Lcom/google/android/youtube/api/jar/client/ai;

.field private final q:Lcom/google/android/youtube/api/jar/client/cq;

.field private final r:Lcom/google/android/youtube/api/jar/client/dt;

.field private final s:Lcom/google/android/youtube/api/jar/client/eo;

.field private t:Z

.field private u:Z

.field private v:Z

.field private w:I

.field private x:I


# direct methods
.method private constructor <init>(Landroid/app/Activity;Landroid/app/Activity;Lcom/google/android/youtube/api/service/a/ah;Z)V
    .locals 4

    new-instance v0, Lcom/google/android/youtube/player/internal/util/d;

    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p1}, Landroid/app/Activity;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1}, Landroid/app/Activity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    invoke-direct {v0, p2, v1, v2, v3}, Lcom/google/android/youtube/player/internal/util/d;-><init>(Landroid/content/Context;Landroid/content/res/Resources;Ljava/lang/ClassLoader;Landroid/content/res/Resources$Theme;)V

    new-instance v1, Lcom/google/android/youtube/api/jar/a;

    invoke-direct {v1, p2}, Lcom/google/android/youtube/api/jar/a;-><init>(Landroid/app/Activity;)V

    invoke-direct {p0, v0, v1, p3, p4}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;-><init>(Landroid/content/Context;Lcom/google/android/youtube/api/jar/a;Lcom/google/android/youtube/api/service/a/ah;Z)V

    return-void
.end method

.method private constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/api/service/a/ah;Z)V
    .locals 1

    invoke-static {p1}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a(Landroid/app/Activity;)Landroid/app/Activity;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;-><init>(Landroid/app/Activity;Landroid/app/Activity;Lcom/google/android/youtube/api/service/a/ah;Z)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/api/jar/a;Lcom/google/android/youtube/api/service/a/ah;Z)V
    .locals 14

    new-instance v1, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;

    invoke-direct {v1, p1}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p2

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/youtube/api/jar/client/a;-><init>(Landroid/content/Context;Lcom/google/android/youtube/api/jar/a;Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;)V

    const-string v1, "apiPlayerFactoryService cannot be null"

    move-object/from16 v0, p3

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-nez p4, :cond_0

    new-instance v1, Lcom/google/android/youtube/api/jar/DefaultApiPlayerSurface;

    invoke-direct {v1, p1}, Lcom/google/android/youtube/api/jar/DefaultApiPlayerSurface;-><init>(Landroid/content/Context;)V

    new-instance v2, Lcom/google/android/youtube/api/jar/client/dy;

    iget-object v3, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    invoke-direct {v2, v1, p1, v3}, Lcom/google/android/youtube/api/jar/client/dy;-><init>(Lcom/google/android/youtube/api/jar/client/ei;Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v2, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->k:Lcom/google/android/youtube/api/jar/client/dy;

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->l:Lcom/google/android/youtube/api/jar/client/ek;

    :goto_0
    iget-object v2, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->b:Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;

    invoke-interface {v1}, Lcom/google/android/youtube/api/jar/h;->a()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->setVideoView(Landroid/view/View;)V

    new-instance v2, Lcom/google/android/youtube/api/jar/client/da;

    iget-object v3, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->b:Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;

    iget-object v4, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    invoke-direct {v2, v3, v4}, Lcom/google/android/youtube/api/jar/client/da;-><init>(Lcom/google/android/youtube/core/player/ai;Landroid/os/Handler;)V

    iput-object v2, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lcom/google/android/youtube/api/jar/client/da;

    new-instance v2, Lcom/google/android/youtube/api/jar/client/cv;

    iget-object v3, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    invoke-direct {v2, v1, v3}, Lcom/google/android/youtube/api/jar/client/cv;-><init>(Lcom/google/android/youtube/api/jar/h;Landroid/os/Handler;)V

    iput-object v2, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->m:Lcom/google/android/youtube/api/jar/client/cv;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/v;

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->d:Lcom/google/android/youtube/core/player/overlay/a;

    iget-object v3, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    invoke-direct {v1, v2, v3}, Lcom/google/android/youtube/api/jar/client/v;-><init>(Lcom/google/android/youtube/core/player/overlay/a;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->n:Lcom/google/android/youtube/api/jar/client/v;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/ac;

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->e:Lcom/google/android/youtube/core/player/overlay/c;

    iget-object v3, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    invoke-direct {v1, v2, v3}, Lcom/google/android/youtube/api/jar/client/ac;-><init>(Lcom/google/android/youtube/core/player/overlay/c;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->o:Lcom/google/android/youtube/api/jar/client/ac;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/ai;

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->c:Lcom/google/android/youtube/api/jar/b;

    iget-object v3, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    invoke-direct {v1, v2, v3}, Lcom/google/android/youtube/api/jar/client/ai;-><init>(Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->p:Lcom/google/android/youtube/api/jar/client/ai;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/cq;

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->f:Lcom/google/android/youtube/core/player/overlay/i;

    iget-object v3, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    invoke-direct {v1, v2, v3}, Lcom/google/android/youtube/api/jar/client/cq;-><init>(Lcom/google/android/youtube/core/player/overlay/i;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->q:Lcom/google/android/youtube/api/jar/client/cq;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/dt;

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->g:Lcom/google/android/youtube/core/player/overlay/aa;

    iget-object v3, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    invoke-direct {v1, v2, v3}, Lcom/google/android/youtube/api/jar/client/dt;-><init>(Lcom/google/android/youtube/core/player/overlay/aa;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->r:Lcom/google/android/youtube/api/jar/client/dt;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/eo;

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->h:Lcom/google/android/youtube/core/player/overlay/ab;

    iget-object v3, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    invoke-direct {v1, v2, v3}, Lcom/google/android/youtube/api/jar/client/eo;-><init>(Lcom/google/android/youtube/core/player/overlay/ab;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->s:Lcom/google/android/youtube/api/jar/client/eo;

    new-instance v2, Lcom/google/android/youtube/api/jar/client/de;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/google/android/youtube/api/jar/client/de;-><init>(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;B)V

    iget-object v3, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lcom/google/android/youtube/api/jar/client/da;

    iget-object v4, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->k:Lcom/google/android/youtube/api/jar/client/dy;

    iget-object v5, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->l:Lcom/google/android/youtube/api/jar/client/ek;

    iget-object v6, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->m:Lcom/google/android/youtube/api/jar/client/cv;

    iget-object v7, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->n:Lcom/google/android/youtube/api/jar/client/v;

    iget-object v8, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->o:Lcom/google/android/youtube/api/jar/client/ac;

    iget-object v9, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->p:Lcom/google/android/youtube/api/jar/client/ai;

    iget-object v10, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->q:Lcom/google/android/youtube/api/jar/client/cq;

    iget-object v11, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->r:Lcom/google/android/youtube/api/jar/client/dt;

    iget-object v12, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->s:Lcom/google/android/youtube/api/jar/client/eo;

    move-object/from16 v1, p3

    move/from16 v13, p4

    invoke-interface/range {v1 .. v13}, Lcom/google/android/youtube/api/service/a/ah;->a(Lcom/google/android/youtube/api/jar/client/bm;Lcom/google/android/youtube/api/jar/client/cb;Lcom/google/android/youtube/api/jar/client/ch;Lcom/google/android/youtube/api/jar/client/ck;Lcom/google/android/youtube/api/jar/client/by;Lcom/google/android/youtube/api/jar/client/bj;Lcom/google/android/youtube/api/jar/client/bp;Lcom/google/android/youtube/api/jar/client/bs;Lcom/google/android/youtube/api/jar/client/bv;Lcom/google/android/youtube/api/jar/client/ce;Lcom/google/android/youtube/api/jar/client/cn;Z)Lcom/google/android/youtube/api/service/a/ak;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->i:Lcom/google/android/youtube/api/service/a/ak;

    return-void

    :cond_0
    new-instance v1, Lcom/google/android/youtube/api/jar/TextureApiPlayerSurface;

    new-instance v2, Lcom/google/android/youtube/api/jar/client/ds;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/android/youtube/api/jar/client/ds;-><init>(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;B)V

    invoke-direct {v1, p1, v2}, Lcom/google/android/youtube/api/jar/TextureApiPlayerSurface;-><init>(Landroid/content/Context;Lcom/google/android/youtube/api/jar/ag;)V

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->k:Lcom/google/android/youtube/api/jar/client/dy;

    new-instance v2, Lcom/google/android/youtube/api/jar/client/ek;

    iget-object v3, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    invoke-direct {v2, v1, p1, v3}, Lcom/google/android/youtube/api/jar/client/ek;-><init>(Lcom/google/android/youtube/api/jar/client/en;Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v2, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->l:Lcom/google/android/youtube/api/jar/client/ek;

    goto/16 :goto_0
.end method

.method public constructor <init>(Landroid/os/IBinder;Landroid/os/IBinder;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;-><init>(Landroid/os/IBinder;Landroid/os/IBinder;Z)V

    return-void
.end method

.method private constructor <init>(Landroid/os/IBinder;Landroid/os/IBinder;Z)V
    .locals 3

    invoke-static {p1}, Lcom/google/android/youtube/player/internal/dynamic/b;->a(Landroid/os/IBinder;)Lcom/google/android/youtube/player/internal/dynamic/a;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/player/internal/dynamic/d;->a(Lcom/google/android/youtube/player/internal/dynamic/a;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-static {p2}, Lcom/google/android/youtube/api/service/a/ai;->a(Landroid/os/IBinder;)Lcom/google/android/youtube/api/service/a/ah;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/api/service/a/ah;Z)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->w:I

    return p1
.end method

.method private static a(Landroid/app/Activity;)Landroid/app/Activity;
    .locals 6

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    invoke-virtual {v3}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v4

    const-class v5, Landroid/app/Activity;

    if-ne v4, v5, :cond_0

    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    :try_start_0
    invoke-virtual {v3, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Could not get the activity from the ActivityWrapper"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Failed to extract the wrapped activity"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static synthetic a(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->v:Z

    return p1
.end method

.method static synthetic b(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->x:I

    return p1
.end method

.method static synthetic b(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->u:Z

    return p1
.end method

.method static synthetic c(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->t:Z

    return p1
.end method


# virtual methods
.method public final A()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->v:Z

    return v0
.end method

.method public final B()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->i:Lcom/google/android/youtube/api/service/a/ak;

    invoke-interface {v0}, Lcom/google/android/youtube/api/service/a/ak;->f()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final C()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->i:Lcom/google/android/youtube/api/service/a/ak;

    invoke-interface {v0}, Lcom/google/android/youtube/api/service/a/ak;->g()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final D()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->w:I

    return v0
.end method

.method public final E()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->x:I

    return v0
.end method

.method public final F()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->i:Lcom/google/android/youtube/api/service/a/ak;

    invoke-interface {v0}, Lcom/google/android/youtube/api/service/a/ak;->i()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final G()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->i:Lcom/google/android/youtube/api/service/a/ak;

    invoke-interface {v0}, Lcom/google/android/youtube/api/service/a/ak;->j()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final H()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->i:Lcom/google/android/youtube/api/service/a/ak;

    invoke-interface {v0}, Lcom/google/android/youtube/api/service/a/ak;->h()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final I()Z
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->i:Lcom/google/android/youtube/api/service/a/ak;

    invoke-interface {v0}, Lcom/google/android/youtube/api/service/a/ak;->e()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final J()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->i:Lcom/google/android/youtube/api/service/a/ak;

    invoke-interface {v0}, Lcom/google/android/youtube/api/service/a/ak;->l()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method protected final a()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/youtube/api/jar/client/a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->i:Lcom/google/android/youtube/api/service/a/ak;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final a([B)Z
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->i:Lcom/google/android/youtube/api/service/a/ak;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/api/service/a/ak;->a([B)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final c(Ljava/lang/String;I)V
    .locals 2

    :try_start_0
    iput p2, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->w:I

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->i:Lcom/google/android/youtube/api/service/a/ak;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/api/service/a/ak;->a(Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final c(Ljava/lang/String;II)V
    .locals 2

    :try_start_0
    iput p3, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->w:I

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->i:Lcom/google/android/youtube/api/service/a/ak;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/youtube/api/service/a/ak;->a(Ljava/lang/String;II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final c(Ljava/util/List;II)V
    .locals 2

    :try_start_0
    iput p3, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->w:I

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->i:Lcom/google/android/youtube/api/service/a/ak;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/youtube/api/service/a/ak;->a(Ljava/util/List;II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final c(ILandroid/view/KeyEvent;)Z
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->i:Lcom/google/android/youtube/api/service/a/ak;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/api/service/a/ak;->a(ILandroid/view/KeyEvent;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x0

    return v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final d(Ljava/lang/String;I)V
    .locals 2

    :try_start_0
    iput p2, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->w:I

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->i:Lcom/google/android/youtube/api/service/a/ak;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/api/service/a/ak;->b(Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final d(Ljava/lang/String;II)V
    .locals 2

    :try_start_0
    iput p3, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->w:I

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->i:Lcom/google/android/youtube/api/service/a/ak;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/youtube/api/service/a/ak;->b(Ljava/lang/String;II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final d(Ljava/util/List;II)V
    .locals 2

    :try_start_0
    iput p3, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->w:I

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->i:Lcom/google/android/youtube/api/service/a/ak;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/youtube/api/service/a/ak;->b(Ljava/util/List;II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final d(ILandroid/view/KeyEvent;)Z
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->i:Lcom/google/android/youtube/api/service/a/ak;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/api/service/a/ak;->b(ILandroid/view/KeyEvent;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x0

    return v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final e(I)V
    .locals 2

    :try_start_0
    iput p1, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->w:I

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->i:Lcom/google/android/youtube/api/service/a/ak;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/api/service/a/ak;->a(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final f(I)V
    .locals 2

    :try_start_0
    iget v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->w:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->w:I

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->i:Lcom/google/android/youtube/api/service/a/ak;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/api/service/a/ak;->b(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final f(Z)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->i:Lcom/google/android/youtube/api/service/a/ak;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/api/service/a/ak;->b(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final g(Z)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->i:Lcom/google/android/youtube/api/service/a/ak;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/api/service/a/ak;->c(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final h(Z)V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->i:Lcom/google/android/youtube/api/service/a/ak;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/api/service/a/ak;->a(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->k:Lcom/google/android/youtube/api/jar/client/dy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->k:Lcom/google/android/youtube/api/jar/client/dy;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/jar/client/dy;->d()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->l:Lcom/google/android/youtube/api/jar/client/ek;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->l:Lcom/google/android/youtube/api/jar/client/ek;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/jar/client/ek;->a()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lcom/google/android/youtube/api/jar/client/da;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/jar/client/da;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->n:Lcom/google/android/youtube/api/jar/client/v;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/jar/client/v;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->o:Lcom/google/android/youtube/api/jar/client/ac;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/jar/client/ac;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->p:Lcom/google/android/youtube/api/jar/client/ai;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/jar/client/ai;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->q:Lcom/google/android/youtube/api/jar/client/cq;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/jar/client/cq;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->i:Lcom/google/android/youtube/api/service/a/ak;

    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected final u()[B
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->i:Lcom/google/android/youtube/api/service/a/ak;

    invoke-interface {v0}, Lcom/google/android/youtube/api/service/a/ak;->k()[B
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final v()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->i:Lcom/google/android/youtube/api/service/a/ak;

    invoke-interface {v0}, Lcom/google/android/youtube/api/service/a/ak;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final w()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->i:Lcom/google/android/youtube/api/service/a/ak;

    invoke-interface {v0}, Lcom/google/android/youtube/api/service/a/ak;->c()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final x()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->i:Lcom/google/android/youtube/api/service/a/ak;

    invoke-interface {v0}, Lcom/google/android/youtube/api/service/a/ak;->d()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final y()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->t:Z

    return v0
.end method

.method public final z()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->u:Z

    return v0
.end method
