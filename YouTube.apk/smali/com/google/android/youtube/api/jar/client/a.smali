.class public abstract Lcom/google/android/youtube/api/jar/client/a;
.super Lcom/google/android/youtube/player/internal/h;
.source "SourceFile"


# instance fields
.field protected final a:Landroid/os/Handler;

.field protected final b:Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;

.field protected final c:Lcom/google/android/youtube/api/jar/b;

.field protected final d:Lcom/google/android/youtube/core/player/overlay/a;

.field protected final e:Lcom/google/android/youtube/core/player/overlay/c;

.field protected final f:Lcom/google/android/youtube/core/player/overlay/i;

.field protected final g:Lcom/google/android/youtube/core/player/overlay/aa;

.field protected final h:Lcom/google/android/youtube/core/player/overlay/ab;

.field private final i:Landroid/content/Context;

.field private final j:Lcom/google/android/youtube/api/jar/a;

.field private final k:Lcom/google/android/youtube/api/jar/v;

.field private final l:Lcom/google/android/youtube/api/jar/t;

.field private final m:Lcom/google/android/youtube/api/jar/z;

.field private n:Lcom/google/android/youtube/player/internal/s;

.field private o:Lcom/google/android/youtube/player/internal/p;

.field private p:Lcom/google/android/youtube/player/internal/m;

.field private q:Lcom/google/android/youtube/player/internal/j;

.field private r:Ljava/lang/String;

.field private s:Lcom/google/android/youtube/player/YouTubePlayer$PlayerStyle;

.field private t:Z

.field private u:Z


# direct methods
.method protected constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/api/jar/a;Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;)V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/google/android/youtube/player/internal/h;-><init>()V

    const-string v0, "context cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->i:Landroid/content/Context;

    const-string v0, "activityProxy cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/api/jar/a;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->j:Lcom/google/android/youtube/api/jar/a;

    const-string v0, "playerOverlaysLayout cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->b:Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;

    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->a:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/youtube/api/jar/z;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/t;

    invoke-direct {v1, p0, v3}, Lcom/google/android/youtube/api/jar/client/t;-><init>(Lcom/google/android/youtube/api/jar/client/a;B)V

    invoke-direct {v0, p3, p2, v1}, Lcom/google/android/youtube/api/jar/z;-><init>(Landroid/view/View;Lcom/google/android/youtube/api/jar/a;Lcom/google/android/youtube/api/jar/ab;)V

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->m:Lcom/google/android/youtube/api/jar/z;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/pm/PackageManager;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/client/a;->m:Lcom/google/android/youtube/api/jar/z;

    invoke-direct {v0, p1, p3, v1}, Lcom/google/android/youtube/api/jar/ApiTvControllerOverlay;-><init>(Landroid/content/Context;Landroid/view/View;Lcom/google/android/youtube/api/jar/z;)V

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->c:Lcom/google/android/youtube/api/jar/b;

    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->c:Lcom/google/android/youtube/api/jar/b;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/u;

    invoke-direct {v1, p0, v3}, Lcom/google/android/youtube/api/jar/client/u;-><init>(Lcom/google/android/youtube/api/jar/client/a;B)V

    invoke-interface {v0, v1}, Lcom/google/android/youtube/api/jar/b;->setOnPlayInYouTubeListener(Lcom/google/android/youtube/api/jar/c;)V

    new-instance v0, Lcom/google/android/youtube/api/jar/client/s;

    invoke-direct {v0, p0, v3}, Lcom/google/android/youtube/api/jar/client/s;-><init>(Lcom/google/android/youtube/api/jar/client/a;B)V

    invoke-static {p1, p2, v0, p3}, Lcom/google/android/youtube/api/jar/v;->a(Landroid/content/Context;Lcom/google/android/youtube/api/jar/a;Lcom/google/android/youtube/api/jar/y;Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;)Lcom/google/android/youtube/api/jar/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->k:Lcom/google/android/youtube/api/jar/v;

    new-instance v0, Lcom/google/android/youtube/api/jar/t;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/r;

    invoke-direct {v1, p0, v3}, Lcom/google/android/youtube/api/jar/client/r;-><init>(Lcom/google/android/youtube/api/jar/client/a;B)V

    invoke-direct {v0, p1, v1, p3}, Lcom/google/android/youtube/api/jar/t;-><init>(Landroid/content/Context;Lcom/google/android/youtube/api/jar/u;Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->l:Lcom/google/android/youtube/api/jar/t;

    new-instance v0, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;

    new-instance v1, Lcom/google/android/youtube/api/jar/s;

    invoke-direct {v1}, Lcom/google/android/youtube/api/jar/s;-><init>()V

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/client/a;->c:Lcom/google/android/youtube/api/jar/b;

    invoke-interface {v2}, Lcom/google/android/youtube/api/jar/b;->a()I

    move-result v2

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/Analytics;I)V

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->d:Lcom/google/android/youtube/core/player/overlay/a;

    new-instance v0, Lcom/google/android/youtube/core/player/overlay/DefaultBrandingOverlay;

    invoke-direct {v0, p1}, Lcom/google/android/youtube/core/player/overlay/DefaultBrandingOverlay;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->e:Lcom/google/android/youtube/core/player/overlay/c;

    new-instance v0, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;

    const v1, 0x7f02000d

    invoke-direct {v0, p1, v1, v3}, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;-><init>(Landroid/content/Context;II)V

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->f:Lcom/google/android/youtube/core/player/overlay/i;

    new-instance v0, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;

    invoke-direct {v0, p1}, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->g:Lcom/google/android/youtube/core/player/overlay/aa;

    new-instance v0, Lcom/google/android/youtube/core/player/overlay/DefaultThumbnailOverlay;

    invoke-direct {v0, p1}, Lcom/google/android/youtube/core/player/overlay/DefaultThumbnailOverlay;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->h:Lcom/google/android/youtube/core/player/overlay/ab;

    sget-object v0, Lcom/google/android/youtube/player/YouTubePlayer$PlayerStyle;->DEFAULT:Lcom/google/android/youtube/player/YouTubePlayer$PlayerStyle;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->s:Lcom/google/android/youtube/player/YouTubePlayer$PlayerStyle;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/android/youtube/core/player/overlay/s;

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/client/a;->e:Lcom/google/android/youtube/core/player/overlay/c;

    aput-object v1, v0, v3

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/client/a;->h:Lcom/google/android/youtube/core/player/overlay/ab;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/client/a;->g:Lcom/google/android/youtube/core/player/overlay/aa;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/client/a;->c:Lcom/google/android/youtube/api/jar/b;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/client/a;->d:Lcom/google/android/youtube/core/player/overlay/a;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/client/a;->f:Lcom/google/android/youtube/core/player/overlay/i;

    aput-object v2, v0, v1

    invoke-virtual {p3, v0}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->a([Lcom/google/android/youtube/core/player/overlay/s;)V

    return-void

    :cond_0
    new-instance v0, Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/client/a;->m:Lcom/google/android/youtube/api/jar/z;

    invoke-direct {v0, p1, v1}, Lcom/google/android/youtube/api/jar/ApiMobileControllerOverlay;-><init>(Landroid/content/Context;Lcom/google/android/youtube/api/jar/z;)V

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->c:Lcom/google/android/youtube/api/jar/b;

    goto/16 :goto_0
.end method

.method static synthetic a(Lcom/google/android/youtube/api/jar/client/a;)Lcom/google/android/youtube/player/internal/s;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->n:Lcom/google/android/youtube/player/internal/s;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/api/jar/client/a;Ljava/lang/String;Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/a;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/a;->k()V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1, v0}, Lcom/google/android/youtube/player/internal/util/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p0, p2}, Lcom/google/android/youtube/api/jar/client/a;->a(Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/api/jar/client/a;Z)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/a;->I()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/a;->t:Z

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/a;->k()V

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->l:Lcom/google/android/youtube/api/jar/t;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/jar/t;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->l:Lcom/google/android/youtube/api/jar/t;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/jar/t;->show()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->l:Lcom/google/android/youtube/api/jar/t;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/jar/t;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->l:Lcom/google/android/youtube/api/jar/t;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/jar/t;->dismiss()V

    goto :goto_0
.end method

.method private aa()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This YouTubePlayer has been released"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/google/android/youtube/api/jar/client/a;)Lcom/google/android/youtube/player/internal/p;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->o:Lcom/google/android/youtube/player/internal/p;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/api/jar/client/a;)Lcom/google/android/youtube/player/internal/m;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->p:Lcom/google/android/youtube/player/internal/m;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/api/jar/client/a;)Lcom/google/android/youtube/player/internal/j;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->q:Lcom/google/android/youtube/player/internal/j;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/api/jar/client/a;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/a;->t:Z

    return v0
.end method

.method static synthetic f(Lcom/google/android/youtube/api/jar/client/a;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->i:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/youtube/api/jar/client/a;)Lcom/google/android/youtube/api/jar/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->j:Lcom/google/android/youtube/api/jar/a;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/youtube/api/jar/client/a;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->r:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected abstract A()Z
.end method

.method protected abstract B()V
.end method

.method protected abstract C()V
.end method

.method protected abstract D()I
.end method

.method protected abstract E()I
.end method

.method protected abstract F()V
.end method

.method protected abstract G()V
.end method

.method protected abstract H()V
.end method

.method protected abstract I()Z
.end method

.method protected abstract J()V
.end method

.method protected final K()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->k:Lcom/google/android/youtube/api/jar/v;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/api/jar/v;->d(Z)V

    goto :goto_0
.end method

.method protected final L()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->k:Lcom/google/android/youtube/api/jar/v;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/api/jar/v;->d(Z)V

    goto :goto_0
.end method

.method protected final M()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->k:Lcom/google/android/youtube/api/jar/v;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/jar/v;->f()V

    goto :goto_0
.end method

.method protected final N()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->k:Lcom/google/android/youtube/api/jar/v;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/jar/v;->g()V

    goto :goto_0
.end method

.method protected final O()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->m:Lcom/google/android/youtube/api/jar/z;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/jar/z;->a()V

    goto :goto_0
.end method

.method protected final P()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->m:Lcom/google/android/youtube/api/jar/z;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/jar/z;->b()V

    goto :goto_0
.end method

.method protected final Q()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/b;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/jar/client/b;-><init>(Lcom/google/android/youtube/api/jar/client/a;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected final R()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/j;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/jar/client/j;-><init>(Lcom/google/android/youtube/api/jar/client/a;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected final S()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/k;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/jar/client/k;-><init>(Lcom/google/android/youtube/api/jar/client/a;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected final T()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/l;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/jar/client/l;-><init>(Lcom/google/android/youtube/api/jar/client/a;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected final U()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/n;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/jar/client/n;-><init>(Lcom/google/android/youtube/api/jar/client/a;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected final V()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/o;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/jar/client/o;-><init>(Lcom/google/android/youtube/api/jar/client/a;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected final W()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/p;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/jar/client/p;-><init>(Lcom/google/android/youtube/api/jar/client/a;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected final X()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/c;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/jar/client/c;-><init>(Lcom/google/android/youtube/api/jar/client/a;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected final Y()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/d;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/jar/client/d;-><init>(Lcom/google/android/youtube/api/jar/client/a;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected final Z()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/e;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/jar/client/e;-><init>(Lcom/google/android/youtube/api/jar/client/a;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(I)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/client/a;->aa()V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->k:Lcom/google/android/youtube/api/jar/v;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/api/jar/v;->a(I)V

    return-void
.end method

.method public final a(Landroid/content/res/Configuration;)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->k:Lcom/google/android/youtube/api/jar/v;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/api/jar/v;->a(Landroid/content/res/Configuration;)V

    goto :goto_0
.end method

.method protected final a(Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/q;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/jar/client/q;-><init>(Lcom/google/android/youtube/api/jar/client/a;Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Lcom/google/android/youtube/player/internal/j;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/client/a;->aa()V

    iput-object p1, p0, Lcom/google/android/youtube/api/jar/client/a;->q:Lcom/google/android/youtube/player/internal/j;

    return-void
.end method

.method public final a(Lcom/google/android/youtube/player/internal/m;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/client/a;->aa()V

    iput-object p1, p0, Lcom/google/android/youtube/api/jar/client/a;->p:Lcom/google/android/youtube/player/internal/m;

    return-void
.end method

.method public final a(Lcom/google/android/youtube/player/internal/p;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/client/a;->aa()V

    iput-object p1, p0, Lcom/google/android/youtube/api/jar/client/a;->o:Lcom/google/android/youtube/player/internal/p;

    return-void
.end method

.method public final a(Lcom/google/android/youtube/player/internal/s;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/client/a;->aa()V

    iput-object p1, p0, Lcom/google/android/youtube/api/jar/client/a;->n:Lcom/google/android/youtube/player/internal/s;

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-static {p1}, Lcom/google/android/youtube/player/YouTubePlayer$PlayerStyle;->valueOf(Ljava/lang/String;)Lcom/google/android/youtube/player/YouTubePlayer$PlayerStyle;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/client/a;->aa()V

    sget-object v1, Lcom/google/android/youtube/api/jar/client/i;->a:[I

    invoke-virtual {v0}, Lcom/google/android/youtube/player/YouTubePlayer$PlayerStyle;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    const-string v0, "Unhandled PlayerStyle"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->s:Lcom/google/android/youtube/player/YouTubePlayer$PlayerStyle;

    :goto_0
    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->s:Lcom/google/android/youtube/player/YouTubePlayer$PlayerStyle;

    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/youtube/api/jar/client/a;->c:Lcom/google/android/youtube/api/jar/b;

    invoke-interface {v1, v3}, Lcom/google/android/youtube/api/jar/b;->setMinimal(Z)V

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/client/a;->c:Lcom/google/android/youtube/api/jar/b;

    invoke-interface {v1, v3}, Lcom/google/android/youtube/api/jar/b;->setControlsPermanentlyHidden(Z)V

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/client/a;->b:Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;

    invoke-virtual {v1, v4}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->setFocusable(Z)V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/google/android/youtube/api/jar/client/a;->c:Lcom/google/android/youtube/api/jar/b;

    invoke-interface {v1, v4}, Lcom/google/android/youtube/api/jar/b;->setMinimal(Z)V

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/client/a;->c:Lcom/google/android/youtube/api/jar/b;

    invoke-interface {v1, v3}, Lcom/google/android/youtube/api/jar/b;->setControlsPermanentlyHidden(Z)V

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/client/a;->b:Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;

    invoke-virtual {v1, v4}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->setFocusable(Z)V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/google/android/youtube/api/jar/client/a;->c:Lcom/google/android/youtube/api/jar/b;

    invoke-interface {v1, v4}, Lcom/google/android/youtube/api/jar/b;->setControlsPermanentlyHidden(Z)V

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/client/a;->b:Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;

    invoke-virtual {v1, v3}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->setFocusable(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/client/a;->aa()V

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/api/jar/client/a;->c(Ljava/lang/String;I)V

    return-void
.end method

.method public final a(Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/client/a;->aa()V

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/youtube/api/jar/client/a;->c(Ljava/lang/String;II)V

    return-void
.end method

.method protected final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/youtube/api/jar/client/a;->r:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->c:Lcom/google/android/youtube/api/jar/b;

    invoke-interface {v0, p2}, Lcom/google/android/youtube/api/jar/b;->setVideoTitle(Ljava/lang/String;)V

    return-void
.end method

.method public final a(Ljava/util/List;II)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/client/a;->aa()V

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/youtube/api/jar/client/a;->c(Ljava/util/List;II)V

    return-void
.end method

.method public final a(Z)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/api/jar/client/a;->c(Z)V

    return-void
.end method

.method protected a()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/a;->u:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(ILandroid/view/KeyEvent;)Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/api/jar/client/a;->c(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const-string v0, "playerStyle"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/api/jar/client/a;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->k:Lcom/google/android/youtube/api/jar/v;

    const-string v1, "fullscreenHelperState"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/api/jar/v;->a(Landroid/os/Bundle;)V

    const-string v0, "apiPlayerState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/api/jar/client/a;->a([B)Z

    move-result v0

    goto :goto_0
.end method

.method protected abstract a([B)Z
.end method

.method public final b()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->k:Lcom/google/android/youtube/api/jar/v;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/jar/v;->b()V

    goto :goto_0
.end method

.method public final b(I)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/client/a;->aa()V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->k:Lcom/google/android/youtube/api/jar/v;

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/a;->h()I

    move-result v1

    or-int/2addr v1, p1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/api/jar/v;->a(I)V

    return-void
.end method

.method protected final b(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/m;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/jar/client/m;-><init>(Lcom/google/android/youtube/api/jar/client/a;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final b(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/client/a;->aa()V

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/api/jar/client/a;->d(Ljava/lang/String;I)V

    return-void
.end method

.method public final b(Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/client/a;->aa()V

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/youtube/api/jar/client/a;->d(Ljava/lang/String;II)V

    return-void
.end method

.method public final b(Ljava/util/List;II)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/client/a;->aa()V

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/youtube/api/jar/client/a;->d(Ljava/util/List;II)V

    return-void
.end method

.method public final b(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->c:Lcom/google/android/youtube/api/jar/b;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/api/jar/b;->setShowFullscreen(Z)V

    return-void
.end method

.method public final b(ILandroid/view/KeyEvent;)Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/api/jar/client/a;->d(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/a;->F()V

    goto :goto_0
.end method

.method public final c(I)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/client/a;->aa()V

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/api/jar/client/a;->e(I)V

    return-void
.end method

.method protected abstract c(Ljava/lang/String;I)V
.end method

.method protected abstract c(Ljava/lang/String;II)V
.end method

.method protected abstract c(Ljava/util/List;II)V
.end method

.method public final c(Z)V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/a;->u:Z

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->k:Lcom/google/android/youtube/api/jar/v;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/jar/v;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->m:Lcom/google/android/youtube/api/jar/z;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/jar/z;->b()V

    iput-object v1, p0, Lcom/google/android/youtube/api/jar/client/a;->n:Lcom/google/android/youtube/player/internal/s;

    iput-object v1, p0, Lcom/google/android/youtube/api/jar/client/a;->o:Lcom/google/android/youtube/player/internal/p;

    iput-object v1, p0, Lcom/google/android/youtube/api/jar/client/a;->p:Lcom/google/android/youtube/player/internal/m;

    iput-object v1, p0, Lcom/google/android/youtube/api/jar/client/a;->q:Lcom/google/android/youtube/player/internal/j;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/api/jar/client/a;->h(Z)V

    goto :goto_0
.end method

.method protected abstract c(ILandroid/view/KeyEvent;)Z
.end method

.method public final d()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/a;->G()V

    goto :goto_0
.end method

.method public final d(I)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/client/a;->aa()V

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/api/jar/client/a;->f(I)V

    return-void
.end method

.method protected abstract d(Ljava/lang/String;I)V
.end method

.method protected abstract d(Ljava/lang/String;II)V
.end method

.method protected abstract d(Ljava/util/List;II)V
.end method

.method public final d(Z)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/client/a;->aa()V

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/api/jar/client/a;->f(Z)V

    return-void
.end method

.method protected abstract d(ILandroid/view/KeyEvent;)Z
.end method

.method public final e()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/a;->t:Z

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->l:Lcom/google/android/youtube/api/jar/t;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/jar/t;->dismiss()V

    goto :goto_0
.end method

.method protected abstract e(I)V
.end method

.method public final e(Z)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/client/a;->aa()V

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/api/jar/client/a;->g(Z)V

    return-void
.end method

.method public final f()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->o:Lcom/google/android/youtube/player/internal/p;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/a;->u:Z

    if-nez v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->o:Lcom/google/android/youtube/player/internal/p;

    sget-object v1, Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;->UNEXPECTED_SERVICE_DISCONNECTION:Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;

    invoke-virtual {v1}, Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;->name()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/player/internal/p;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/api/jar/client/a;->c(Z)V

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/youtube/player/internal/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/google/android/youtube/player/internal/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method protected abstract f(I)V
.end method

.method protected abstract f(Z)V
.end method

.method public final g()Lcom/google/android/youtube/player/internal/dynamic/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->b:Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;

    invoke-static {v0}, Lcom/google/android/youtube/player/internal/dynamic/d;->a(Ljava/lang/Object;)Lcom/google/android/youtube/player/internal/dynamic/a;

    move-result-object v0

    return-object v0
.end method

.method protected final g(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/g;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/jar/client/g;-><init>(Lcom/google/android/youtube/api/jar/client/a;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected abstract g(Z)V
.end method

.method public final h()I
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/client/a;->aa()V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->k:Lcom/google/android/youtube/api/jar/v;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/jar/v;->e()I

    move-result v0

    return v0
.end method

.method protected abstract h(Z)V
.end method

.method public final i()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/client/a;->aa()V

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/a;->v()V

    return-void
.end method

.method protected final i(Z)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->k:Lcom/google/android/youtube/api/jar/v;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/api/jar/v;->a(Z)V

    goto :goto_0
.end method

.method public final j()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/client/a;->aa()V

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/a;->w()V

    return-void
.end method

.method protected final j(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/f;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/jar/client/f;-><init>(Lcom/google/android/youtube/api/jar/client/a;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final k()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/client/a;->aa()V

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/a;->x()V

    return-void
.end method

.method protected final k(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/h;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/jar/client/h;-><init>(Lcom/google/android/youtube/api/jar/client/a;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final l()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/client/a;->aa()V

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/a;->y()Z

    move-result v0

    return v0
.end method

.method public final m()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/client/a;->aa()V

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/a;->z()Z

    move-result v0

    return v0
.end method

.method public final n()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/client/a;->aa()V

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/a;->A()Z

    move-result v0

    return v0
.end method

.method public final o()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/client/a;->aa()V

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/a;->m()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    const-string v1, "Called next at end of playlist"

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/a;->B()V

    return-void
.end method

.method public final p()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/client/a;->aa()V

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/a;->n()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    const-string v1, "Called previous at start of playlist"

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/a;->C()V

    return-void
.end method

.method public final q()I
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/client/a;->aa()V

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/a;->D()I

    move-result v0

    return v0
.end method

.method public final r()I
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/client/a;->aa()V

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/a;->E()I

    move-result v0

    return v0
.end method

.method public final s()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/client/a;->aa()V

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/a;->H()V

    return-void
.end method

.method public final t()Landroid/os/Bundle;
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "playerStyle"

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/client/a;->s:Lcom/google/android/youtube/player/YouTubePlayer$PlayerStyle;

    invoke-virtual {v2}, Lcom/google/android/youtube/player/YouTubePlayer$PlayerStyle;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "fullscreenHelperState"

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/client/a;->k:Lcom/google/android/youtube/api/jar/v;

    invoke-virtual {v2}, Lcom/google/android/youtube/api/jar/v;->d()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    const-string v1, "apiPlayerState"

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/a;->u()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    goto :goto_0
.end method

.method protected abstract u()[B
.end method

.method protected abstract v()V
.end method

.method protected abstract w()V
.end method

.method protected abstract x()V
.end method

.method protected abstract y()Z
.end method

.method protected abstract z()Z
.end method
