.class public final Lcom/google/android/youtube/api/jar/client/ac;
.super Lcom/google/android/youtube/api/jar/client/bq;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/youtube/core/player/overlay/c;

.field private final b:Landroid/os/Handler;

.field private c:Lcom/google/android/youtube/api/jar/client/ah;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/player/overlay/c;Landroid/os/Handler;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/client/bq;-><init>()V

    const-string v0, "target cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/overlay/c;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/ac;->a:Lcom/google/android/youtube/core/player/overlay/c;

    const-string v0, "uiHandler cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/ac;->b:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/api/jar/client/ac;)Lcom/google/android/youtube/api/jar/client/ah;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ac;->c:Lcom/google/android/youtube/api/jar/client/ah;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/api/jar/client/ac;Lcom/google/android/youtube/api/jar/client/ah;)Lcom/google/android/youtube/api/jar/client/ah;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/api/jar/client/ac;->c:Lcom/google/android/youtube/api/jar/client/ah;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/youtube/api/jar/client/ac;)Lcom/google/android/youtube/core/player/overlay/c;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ac;->a:Lcom/google/android/youtube/core/player/overlay/c;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ac;->c:Lcom/google/android/youtube/api/jar/client/ah;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ac;->c:Lcom/google/android/youtube/api/jar/client/ah;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/jar/client/ah;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/ac;->c:Lcom/google/android/youtube/api/jar/client/ah;

    :cond_0
    return-void
.end method

.method public final a(Landroid/graphics/Bitmap;Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ac;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/ag;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/youtube/api/jar/client/ag;-><init>(Lcom/google/android/youtube/api/jar/client/ac;Landroid/graphics/Bitmap;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Lcom/google/android/youtube/api/service/a/an;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ac;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/ad;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/jar/client/ad;-><init>(Lcom/google/android/youtube/api/jar/client/ac;Lcom/google/android/youtube/api/service/a/an;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ac;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/ae;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/jar/client/ae;-><init>(Lcom/google/android/youtube/api/jar/client/ac;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ac;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/af;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/jar/client/af;-><init>(Lcom/google/android/youtube/api/jar/client/ac;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
