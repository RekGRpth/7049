.class public final Lcom/google/android/youtube/api/jar/client/cv;
.super Lcom/google/android/youtube/api/jar/client/bz;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/youtube/api/jar/h;

.field private final b:Landroid/os/Handler;

.field private final c:Ljava/lang/Runnable;

.field private final d:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/api/jar/h;Landroid/os/Handler;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/client/bz;-><init>()V

    const-string v0, "target cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/api/jar/h;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/cv;->a:Lcom/google/android/youtube/api/jar/h;

    const-string v0, "uiHandler cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/cv;->b:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/youtube/api/jar/client/cw;

    invoke-direct {v0, p0, p1}, Lcom/google/android/youtube/api/jar/client/cw;-><init>(Lcom/google/android/youtube/api/jar/client/cv;Lcom/google/android/youtube/api/jar/h;)V

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/cv;->c:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/youtube/api/jar/client/cx;

    invoke-direct {v0, p0, p1}, Lcom/google/android/youtube/api/jar/client/cx;-><init>(Lcom/google/android/youtube/api/jar/client/cv;Lcom/google/android/youtube/api/jar/h;)V

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/cv;->d:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/api/jar/client/cv;)Lcom/google/android/youtube/api/jar/h;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/cv;->a:Lcom/google/android/youtube/api/jar/h;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/cv;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/cy;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/jar/client/cy;-><init>(Lcom/google/android/youtube/api/jar/client/cv;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(II)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/cv;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/cz;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/youtube/api/jar/client/cz;-><init>(Lcom/google/android/youtube/api/jar/client/cv;II)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/cv;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/client/cv;->c:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/cv;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/client/cv;->d:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
