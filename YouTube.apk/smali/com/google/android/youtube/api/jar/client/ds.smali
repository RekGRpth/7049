.class final Lcom/google/android/youtube/api/jar/client/ds;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/api/jar/ag;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/api/jar/client/ds;->a:Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/api/jar/client/ds;-><init>(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    const-string v0, "Cannot attach a YouTubePlayerView backed by a TextureView to a Window that is not hardware accelerated"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/youtube/player/internal/util/a;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/ds;->a:Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;

    sget-object v1, Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;->UNABLE_TO_USE_TEXTUREVIEW:Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a(Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;)V

    return-void
.end method
