.class final Lcom/google/android/youtube/api/jar/client/dz;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/api/service/a/az;

.field final synthetic b:Lcom/google/android/youtube/api/jar/client/dy;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/api/jar/client/dy;Lcom/google/android/youtube/api/service/a/az;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/api/jar/client/dz;->b:Lcom/google/android/youtube/api/jar/client/dy;

    iput-object p2, p0, Lcom/google/android/youtube/api/jar/client/dz;->a:Lcom/google/android/youtube/api/service/a/az;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/dz;->b:Lcom/google/android/youtube/api/jar/client/dy;

    new-instance v1, Lcom/google/android/youtube/api/jar/client/eh;

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/client/dz;->a:Lcom/google/android/youtube/api/service/a/az;

    invoke-direct {v1, v2}, Lcom/google/android/youtube/api/jar/client/eh;-><init>(Lcom/google/android/youtube/api/service/a/az;)V

    invoke-static {v0, v1}, Lcom/google/android/youtube/api/jar/client/dy;->a(Lcom/google/android/youtube/api/jar/client/dy;Lcom/google/android/youtube/api/jar/client/eh;)Lcom/google/android/youtube/api/jar/client/eh;

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/dz;->b:Lcom/google/android/youtube/api/jar/client/dy;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/client/dy;->a(Lcom/google/android/youtube/api/jar/client/dy;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/player/internal/util/b;->a(Landroid/content/pm/PackageManager;)Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/client/dy;->b(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    new-instance v0, Lcom/google/android/youtube/api/jar/client/ej;

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/client/dz;->b:Lcom/google/android/youtube/api/jar/client/dy;

    invoke-static {v1}, Lcom/google/android/youtube/api/jar/client/dy;->a(Lcom/google/android/youtube/api/jar/client/dy;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/client/dz;->b:Lcom/google/android/youtube/api/jar/client/dy;

    invoke-static {v2}, Lcom/google/android/youtube/api/jar/client/dy;->b(Lcom/google/android/youtube/api/jar/client/dy;)Lcom/google/android/youtube/api/jar/client/eh;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/api/jar/client/ej;-><init>(Landroid/content/Context;Lcom/google/android/youtube/api/jar/client/eh;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/api/jar/client/dz;->b:Lcom/google/android/youtube/api/jar/client/dy;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/youtube/api/jar/client/dy;->a(Lcom/google/android/youtube/api/jar/client/dy;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/client/dz;->b:Lcom/google/android/youtube/api/jar/client/dy;

    invoke-static {v1}, Lcom/google/android/youtube/api/jar/client/dy;->c(Lcom/google/android/youtube/api/jar/client/dy;)Landroid/view/SurfaceHolder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/api/jar/client/dz;->b:Lcom/google/android/youtube/api/jar/client/dy;

    invoke-static {v2}, Lcom/google/android/youtube/api/jar/client/dy;->b(Lcom/google/android/youtube/api/jar/client/dy;)Lcom/google/android/youtube/api/jar/client/eh;

    move-result-object v2

    invoke-interface {v1, v2}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/client/dz;->b:Lcom/google/android/youtube/api/jar/client/dy;

    invoke-static {v1}, Lcom/google/android/youtube/api/jar/client/dy;->d(Lcom/google/android/youtube/api/jar/client/dy;)Lcom/google/android/youtube/api/jar/client/ei;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/youtube/api/jar/client/ei;->a(Landroid/view/SurfaceView;)V

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Could not create ProxyRelayoutSurfaceView: "

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_0
    new-instance v0, Landroid/view/SurfaceView;

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/client/dz;->b:Lcom/google/android/youtube/api/jar/client/dy;

    invoke-static {v1}, Lcom/google/android/youtube/api/jar/client/dy;->a(Lcom/google/android/youtube/api/jar/client/dy;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    goto :goto_0
.end method
