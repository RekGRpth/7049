.class final Lcom/google/android/youtube/api/jar/client/t;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/api/jar/ab;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/api/jar/client/a;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/api/jar/client/a;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/api/jar/client/t;->a:Lcom/google/android/youtube/api/jar/client/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/api/jar/client/a;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/api/jar/client/t;-><init>(Lcom/google/android/youtube/api/jar/client/a;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/t;->a:Lcom/google/android/youtube/api/jar/client/a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "YouTube video playback stopped due to unauthorized overlay on top of player. "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;->UNAUTHORIZED_OVERLAY:Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/api/jar/client/a;->a(Lcom/google/android/youtube/api/jar/client/a;Ljava/lang/String;Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;)V

    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/t;->a:Lcom/google/android/youtube/api/jar/client/a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "YouTube video playback stopped due to the player\'s view being too small. "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;->PLAYER_VIEW_TOO_SMALL:Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/api/jar/client/a;->a(Lcom/google/android/youtube/api/jar/client/a;Ljava/lang/String;Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;)V

    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/t;->a:Lcom/google/android/youtube/api/jar/client/a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "YouTube video playback stopped because the player\'s view is not visible. "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;->PLAYER_VIEW_NOT_VISIBLE:Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/api/jar/client/a;->a(Lcom/google/android/youtube/api/jar/client/a;Ljava/lang/String;Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;)V

    return-void
.end method
