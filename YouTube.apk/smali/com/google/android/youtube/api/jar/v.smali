.class public abstract Lcom/google/android/youtube/api/jar/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/utils/i;


# static fields
.field private static final e:I


# instance fields
.field protected final a:Landroid/content/Context;

.field protected final b:Lcom/google/android/youtube/api/jar/a;

.field protected c:Z

.field protected d:Z

.field private final f:Lcom/google/android/youtube/api/jar/y;

.field private final g:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;

.field private final h:Z

.field private i:I

.field private j:Z

.field private k:Z

.field private l:I

.field private m:I

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget v0, Lcom/google/android/youtube/core/utils/Util;->a:I

    const/16 v1, 0x8

    if-gt v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    sput v0, Lcom/google/android/youtube/api/jar/v;->e:I

    return-void

    :cond_0
    const/4 v0, 0x6

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/api/jar/a;Lcom/google/android/youtube/api/jar/y;)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "context cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/v;->a:Landroid/content/Context;

    const-string v0, "activityProxy cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/api/jar/a;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/v;->b:Lcom/google/android/youtube/api/jar/a;

    const-string v0, "listener cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/api/jar/y;

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/v;->f:Lcom/google/android/youtube/api/jar/y;

    invoke-virtual {p2}, Lcom/google/android/youtube/api/jar/a;->c()I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/api/jar/v;->l:I

    invoke-virtual {p2}, Lcom/google/android/youtube/api/jar/a;->c()I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/api/jar/v;->m:I

    new-instance v0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;

    invoke-virtual {p2}, Lcom/google/android/youtube/api/jar/a;->e()Landroid/view/WindowManager;

    move-result-object v3

    invoke-direct {v0, p1, v3, p0}, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;-><init>(Landroid/content/Context;Landroid/view/WindowManager;Lcom/google/android/youtube/core/utils/i;)V

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/v;->g:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/google/android/youtube/api/jar/v;->i:I

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/youtube/api/jar/a;->b()Landroid/content/ComponentName;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/ActivityInfo;->configChanges:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/youtube/api/jar/v;->h:Z

    iput-boolean v1, p0, Lcom/google/android/youtube/api/jar/v;->n:Z

    iput-boolean v1, p0, Lcom/google/android/youtube/api/jar/v;->d:Z

    iput-boolean v2, p0, Lcom/google/android/youtube/api/jar/v;->o:Z

    iput-boolean v2, p0, Lcom/google/android/youtube/api/jar/v;->p:Z

    iput-boolean v1, p0, Lcom/google/android/youtube/api/jar/v;->j:Z

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :catch_0
    move-exception v0

    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/youtube/api/jar/a;Lcom/google/android/youtube/api/jar/y;Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;)Lcom/google/android/youtube/api/jar/v;
    .locals 2

    sget v0, Lcom/google/android/youtube/core/utils/Util;->a:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    new-instance v0, Lcom/google/android/youtube/api/jar/w;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/youtube/api/jar/w;-><init>(Landroid/content/Context;Lcom/google/android/youtube/api/jar/a;Lcom/google/android/youtube/api/jar/y;Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/youtube/api/jar/x;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/youtube/api/jar/x;-><init>(Landroid/content/Context;Lcom/google/android/youtube/api/jar/a;Lcom/google/android/youtube/api/jar/y;)V

    goto :goto_0
.end method

.method private b(I)V
    .locals 3

    iget v0, p0, Lcom/google/android/youtube/api/jar/v;->l:I

    if-ne p1, v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/api/jar/v;->b:Lcom/google/android/youtube/api/jar/a;

    invoke-virtual {v1}, Lcom/google/android/youtube/api/jar/a;->c()I

    move-result v1

    iget v2, p0, Lcom/google/android/youtube/api/jar/v;->m:I

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/api/jar/v;->b:Lcom/google/android/youtube/api/jar/a;

    invoke-virtual {v1}, Lcom/google/android/youtube/api/jar/a;->c()I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/api/jar/v;->l:I

    :cond_0
    if-eqz v0, :cond_1

    iget p1, p0, Lcom/google/android/youtube/api/jar/v;->l:I

    :cond_1
    iput p1, p0, Lcom/google/android/youtube/api/jar/v;->m:I

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/v;->b:Lcom/google/android/youtube/api/jar/a;

    iget v1, p0, Lcom/google/android/youtube/api/jar/v;->m:I

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/api/jar/a;->a(I)V

    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()Z
    .locals 2

    iget v0, p0, Lcom/google/android/youtube/api/jar/v;->m:I

    iget v1, p0, Lcom/google/android/youtube/api/jar/v;->l:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/v;->g:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->disable()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/api/jar/v;->j:Z

    return-void
.end method

.method public final a(I)V
    .locals 4

    const/4 v2, 0x0

    const/4 v1, 0x1

    and-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_0

    and-int/lit8 v0, p1, 0x1

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Can not set FULLSCREEN_FLAG_FULLSCREEN_WHEN_DEVICE_LANDSCAPE without setting FULLSCREEN_FLAG_CONTROL_ORIENTATION"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-boolean v3, p0, Lcom/google/android/youtube/api/jar/v;->o:Z

    and-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_3

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/youtube/api/jar/v;->n:Z

    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_4

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/youtube/api/jar/v;->d:Z

    and-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_5

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/android/youtube/api/jar/v;->o:Z

    and-int/lit8 v0, p1, 0x8

    if-eqz v0, :cond_1

    move v2, v1

    :cond_1
    iput-boolean v2, p0, Lcom/google/android/youtube/api/jar/v;->p:Z

    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/v;->o:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/v;->g:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->enable()V

    :cond_2
    :goto_3
    return-void

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_2

    :cond_6
    if-eqz v3, :cond_2

    iget v0, p0, Lcom/google/android/youtube/api/jar/v;->i:I

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/v;->g:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->disable()V

    goto :goto_3
.end method

.method public final a(Landroid/content/res/Configuration;)V
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    iget v3, p0, Lcom/google/android/youtube/api/jar/v;->i:I

    if-eq v2, v3, :cond_1

    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    iput v2, p0, Lcom/google/android/youtube/api/jar/v;->i:I

    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v2, v0, :cond_2

    :goto_0
    iget-boolean v2, p0, Lcom/google/android/youtube/api/jar/v;->c:Z

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lcom/google/android/youtube/api/jar/v;->n:Z

    if-eqz v2, :cond_3

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/v;->f:Lcom/google/android/youtube/api/jar/y;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/api/jar/y;->a(Z)V

    :cond_0
    :goto_1
    iput-boolean v1, p0, Lcom/google/android/youtube/api/jar/v;->q:Z

    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    iget-boolean v2, p0, Lcom/google/android/youtube/api/jar/v;->d:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/youtube/api/jar/v;->q:Z

    if-eqz v2, :cond_0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/v;->h()V

    goto :goto_1
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/youtube/api/jar/v;->k:Z

    const-string v0, "controlFlags"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/api/jar/v;->a(I)V

    const-string v0, "defaultRequestedOrientation"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/api/jar/v;->l:I

    const-string v0, "isFullscreen"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/v;->f:Lcom/google/android/youtube/api/jar/y;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/api/jar/y;->a(Z)V

    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iput-boolean p1, p0, Lcom/google/android/youtube/api/jar/v;->c:Z

    if-eqz p1, :cond_6

    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/v;->n:Z

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/v;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-eq v0, v3, :cond_4

    move v0, v1

    :goto_0
    sget v3, Lcom/google/android/youtube/api/jar/v;->e:I

    invoke-direct {p0, v3}, Lcom/google/android/youtube/api/jar/v;->b(I)V

    iget-object v3, p0, Lcom/google/android/youtube/api/jar/v;->g:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;

    invoke-virtual {v3}, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->a()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/youtube/api/jar/v;->g:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;

    invoke-virtual {v3}, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->enable()V

    :cond_0
    :goto_1
    iget-boolean v3, p0, Lcom/google/android/youtube/api/jar/v;->d:Z

    if-eqz v3, :cond_1

    if-eqz v0, :cond_5

    iput-boolean v1, p0, Lcom/google/android/youtube/api/jar/v;->q:Z

    :cond_1
    :goto_2
    iget-boolean v2, p0, Lcom/google/android/youtube/api/jar/v;->p:Z

    if-nez v2, :cond_3

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/v;->h:Z

    if-nez v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/v;->f:Lcom/google/android/youtube/api/jar/y;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/api/jar/y;->b(Z)V

    :cond_3
    :goto_3
    return-void

    :cond_4
    move v0, v2

    goto :goto_0

    :cond_5
    iput-boolean v2, p0, Lcom/google/android/youtube/api/jar/v;->q:Z

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/v;->h()V

    goto :goto_2

    :cond_6
    iput-boolean v2, p0, Lcom/google/android/youtube/api/jar/v;->q:Z

    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/v;->n:Z

    if-eqz v0, :cond_8

    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/v;->o:Z

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/v;->g:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->disable()V

    :cond_7
    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/v;->j()Z

    move-result v0

    if-eqz v0, :cond_8

    iget v0, p0, Lcom/google/android/youtube/api/jar/v;->l:I

    invoke-direct {p0, v0}, Lcom/google/android/youtube/api/jar/v;->b(I)V

    :cond_8
    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/v;->d:Z

    if-eqz v0, :cond_9

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/v;->i()V

    :cond_9
    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/v;->p:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/v;->f:Lcom/google/android/youtube/api/jar/y;

    invoke-interface {v0, v2}, Lcom/google/android/youtube/api/jar/y;->b(Z)V

    goto :goto_3

    :cond_a
    move v0, v2

    goto :goto_1
.end method

.method public final b()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/v;->c:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/v;->p:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/v;->f:Lcom/google/android/youtube/api/jar/y;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/api/jar/y;->b(Z)V

    :cond_0
    return-void
.end method

.method public final b(Z)V
    .locals 2

    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/v;->o:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/v;->c:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/v;->k:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/v;->f:Lcom/google/android/youtube/api/jar/y;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/api/jar/y;->a(Z)V

    :cond_0
    return-void
.end method

.method public final c(Z)V
    .locals 2

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/youtube/api/jar/v;->k:Z

    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/v;->j:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/v;->n:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/v;->o:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/v;->c:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/v;->f:Lcom/google/android/youtube/api/jar/y;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/api/jar/y;->a(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p1, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/api/jar/v;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/v;->h:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/v;->f:Lcom/google/android/youtube/api/jar/y;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/api/jar/y;->a(Z)V

    :cond_2
    iget v0, p0, Lcom/google/android/youtube/api/jar/v;->l:I

    invoke-direct {p0, v0}, Lcom/google/android/youtube/api/jar/v;->b(I)V

    goto :goto_0
.end method

.method public final d()Landroid/os/Bundle;
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "isFullscreen"

    iget-boolean v2, p0, Lcom/google/android/youtube/api/jar/v;->c:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "defaultRequestedOrientation"

    iget v2, p0, Lcom/google/android/youtube/api/jar/v;->l:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "controlFlags"

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/v;->e()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-object v0
.end method

.method public abstract d(Z)V
.end method

.method public final e()I
    .locals 3

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/v;->n:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iget-boolean v2, p0, Lcom/google/android/youtube/api/jar/v;->d:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x2

    :goto_1
    or-int/2addr v2, v0

    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/v;->o:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    :goto_2
    or-int/2addr v0, v2

    iget-boolean v2, p0, Lcom/google/android/youtube/api/jar/v;->p:Z

    if-eqz v2, :cond_0

    const/16 v1, 0x8

    :cond_0
    or-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v2, v1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public abstract f()V
.end method

.method public abstract g()V
.end method

.method abstract h()V
.end method

.method abstract i()V
.end method

.method public final k_()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/v;->j:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/v;->c:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/v;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/v;->f:Lcom/google/android/youtube/api/jar/y;

    invoke-interface {v0}, Lcom/google/android/youtube/api/jar/y;->a()V

    :cond_0
    return-void
.end method
