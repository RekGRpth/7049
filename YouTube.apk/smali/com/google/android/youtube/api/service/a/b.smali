.class public final Lcom/google/android/youtube/api/service/a/b;
.super Lcom/google/android/youtube/api/service/a/ai;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/os/Handler;

.field private final c:Lcom/google/android/youtube/api/service/k;

.field private final d:Lcom/google/android/youtube/api/j;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lcom/google/android/youtube/api/service/k;Lcom/google/android/youtube/api/j;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/api/service/a/ai;-><init>()V

    const-string v0, "context cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/b;->a:Landroid/content/Context;

    const-string v0, "uiHandler cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/b;->b:Landroid/os/Handler;

    const-string v0, "serviceDestroyedNotifier cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/api/service/k;

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/b;->c:Lcom/google/android/youtube/api/service/k;

    const-string v0, "apiEnvironment cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/api/j;

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/b;->d:Lcom/google/android/youtube/api/j;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/api/service/a/b;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/b;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/api/service/a/b;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/b;->b:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/api/service/a/b;)Lcom/google/android/youtube/api/service/k;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/b;->c:Lcom/google/android/youtube/api/service/k;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/api/service/a/b;)Lcom/google/android/youtube/api/j;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/b;->d:Lcom/google/android/youtube/api/j;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/api/jar/client/bm;Lcom/google/android/youtube/api/jar/client/cb;Lcom/google/android/youtube/api/jar/client/ch;Lcom/google/android/youtube/api/jar/client/ck;Lcom/google/android/youtube/api/jar/client/by;Lcom/google/android/youtube/api/jar/client/bj;Lcom/google/android/youtube/api/jar/client/bp;Lcom/google/android/youtube/api/jar/client/bs;Lcom/google/android/youtube/api/jar/client/bv;Lcom/google/android/youtube/api/jar/client/ce;Lcom/google/android/youtube/api/jar/client/cn;Z)Lcom/google/android/youtube/api/service/a/ak;
    .locals 18

    const-string v1, "apiPlayerClient cannot be null"

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "playerUiClient cannot be null"

    move-object/from16 v0, p2

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-nez p12, :cond_0

    const-string v1, "surfaceHolderClient cannot be null"

    move-object/from16 v0, p3

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    const-string v1, "playerSurfaceClient cannot be null"

    move-object/from16 v0, p5

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "adOverlayClient cannot be null"

    move-object/from16 v0, p6

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "brandingOverlayClient cannot be null"

    move-object/from16 v0, p7

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "controllerOverlayClient cannot be null"

    move-object/from16 v0, p8

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "liveOverlayClient cannot be null"

    move-object/from16 v0, p9

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "subtitlesOverlayClient cannot be null"

    move-object/from16 v0, p10

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "thumbnailOverlayClient cannot be null"

    move-object/from16 v0, p11

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v16, Landroid/os/ConditionVariable;

    invoke-direct/range {v16 .. v16}, Landroid/os/ConditionVariable;-><init>()V

    new-instance v15, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v15}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/api/service/a/b;->b:Landroid/os/Handler;

    move-object/from16 v17, v0

    new-instance v1, Lcom/google/android/youtube/api/service/a/c;

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    move-object/from16 v12, p10

    move-object/from16 v13, p11

    move/from16 v14, p12

    invoke-direct/range {v1 .. v16}, Lcom/google/android/youtube/api/service/a/c;-><init>(Lcom/google/android/youtube/api/service/a/b;Lcom/google/android/youtube/api/jar/client/bm;Lcom/google/android/youtube/api/jar/client/cb;Lcom/google/android/youtube/api/jar/client/ch;Lcom/google/android/youtube/api/jar/client/ck;Lcom/google/android/youtube/api/jar/client/by;Lcom/google/android/youtube/api/jar/client/bj;Lcom/google/android/youtube/api/jar/client/bp;Lcom/google/android/youtube/api/jar/client/bs;Lcom/google/android/youtube/api/jar/client/bv;Lcom/google/android/youtube/api/jar/client/ce;Lcom/google/android/youtube/api/jar/client/cn;ZLjava/util/concurrent/atomic/AtomicReference;Landroid/os/ConditionVariable;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    invoke-virtual/range {v16 .. v16}, Landroid/os/ConditionVariable;->block()V

    invoke-virtual {v15}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/api/service/a/ak;

    return-object v1

    :cond_0
    const-string v1, "surfaceTextureClient cannot be null"

    move-object/from16 v0, p4

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
