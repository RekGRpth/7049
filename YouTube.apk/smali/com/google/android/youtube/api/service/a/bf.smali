.class public final Lcom/google/android/youtube/api/service/a/bf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/player/overlay/a;


# instance fields
.field private final a:Lcom/google/android/youtube/api/service/a/bg;

.field private b:Lcom/google/android/youtube/api/jar/client/bj;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lcom/google/android/youtube/api/jar/client/bj;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "client cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/api/jar/client/bj;

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/bf;->b:Lcom/google/android/youtube/api/jar/client/bj;

    new-instance v0, Lcom/google/android/youtube/api/service/a/bg;

    invoke-direct {v0, p1}, Lcom/google/android/youtube/api/service/a/bg;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/bf;->a:Lcom/google/android/youtube/api/service/a/bg;

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bf;->a:Lcom/google/android/youtube/api/service/a/bg;

    invoke-interface {p2, v0}, Lcom/google/android/youtube/api/jar/client/bj;->a(Lcom/google/android/youtube/api/service/a/ae;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/bf;->b:Lcom/google/android/youtube/api/jar/client/bj;

    return-void
.end method

.method public final a(II)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bf;->b:Lcom/google/android/youtube/api/jar/client/bj;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bf;->b:Lcom/google/android/youtube/api/jar/client/bj;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/api/jar/client/bj;->a(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;ZZLjava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bf;->b:Lcom/google/android/youtube/api/jar/client/bj;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bf;->b:Lcom/google/android/youtube/api/jar/client/bj;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/youtube/api/jar/client/bj;->a(Ljava/lang/String;ZZLjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final b()Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bf;->b:Lcom/google/android/youtube/api/jar/client/bj;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bf;->b:Lcom/google/android/youtube/api/jar/client/bj;

    invoke-interface {v0}, Lcom/google/android/youtube/api/jar/client/bj;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setFullscreen(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bf;->b:Lcom/google/android/youtube/api/jar/client/bj;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bf;->b:Lcom/google/android/youtube/api/jar/client/bj;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/api/jar/client/bj;->a(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setListener(Lcom/google/android/youtube/core/player/overlay/b;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bf;->a:Lcom/google/android/youtube/api/service/a/bg;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/api/service/a/bg;->a(Lcom/google/android/youtube/core/player/overlay/b;)V

    return-void
.end method
