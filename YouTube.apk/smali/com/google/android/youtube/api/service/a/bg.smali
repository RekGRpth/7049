.class final Lcom/google/android/youtube/api/service/a/bg;
.super Lcom/google/android/youtube/api/service/a/af;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/player/overlay/b;


# instance fields
.field private final a:Landroid/os/Handler;

.field private b:Lcom/google/android/youtube/core/player/overlay/b;


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/api/service/a/af;-><init>()V

    const-string v0, "uiHandler cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/bg;->a:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/api/service/a/bg;)Lcom/google/android/youtube/core/player/overlay/b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bg;->b:Lcom/google/android/youtube/core/player/overlay/b;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/player/overlay/b;)V
    .locals 1

    const-string v0, "listener cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/overlay/b;

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/bg;->b:Lcom/google/android/youtube/core/player/overlay/b;

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bg;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/a/bh;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/service/a/bh;-><init>(Lcom/google/android/youtube/api/service/a/bg;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bg;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/a/bi;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/service/a/bi;-><init>(Lcom/google/android/youtube/api/service/a/bg;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final d()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bg;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/a/bj;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/service/a/bj;-><init>(Lcom/google/android/youtube/api/service/a/bg;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final e()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bg;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/a/bk;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/service/a/bk;-><init>(Lcom/google/android/youtube/api/service/a/bg;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
