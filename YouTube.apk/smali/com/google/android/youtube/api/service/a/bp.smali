.class final Lcom/google/android/youtube/api/service/a/bp;
.super Lcom/google/android/youtube/api/service/a/ar;
.source "SourceFile"


# instance fields
.field private final a:Landroid/os/Handler;

.field private b:Lcom/google/android/youtube/core/player/overlay/e;


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/api/service/a/ar;-><init>()V

    const-string v0, "uiHandler cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/bp;->a:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/api/service/a/bp;)Lcom/google/android/youtube/core/player/overlay/e;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bp;->b:Lcom/google/android/youtube/core/player/overlay/e;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bp;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/a/bq;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/service/a/bq;-><init>(Lcom/google/android/youtube/api/service/a/bp;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bp;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/a/bz;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/service/a/bz;-><init>(Lcom/google/android/youtube/api/service/a/bp;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/SubtitleTrack;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bp;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/a/bv;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/service/a/bv;-><init>(Lcom/google/android/youtube/api/service/a/bp;Lcom/google/android/youtube/core/model/SubtitleTrack;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/player/overlay/e;)V
    .locals 1

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/overlay/e;

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/bp;->b:Lcom/google/android/youtube/core/player/overlay/e;

    return-void
.end method

.method public final a(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bp;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/a/bs;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/service/a/bs;-><init>(Lcom/google/android/youtube/api/service/a/bp;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bp;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/a/bx;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/service/a/bx;-><init>(Lcom/google/android/youtube/api/service/a/bp;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bp;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/a/by;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/service/a/by;-><init>(Lcom/google/android/youtube/api/service/a/bp;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final d()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bp;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/a/ca;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/service/a/ca;-><init>(Lcom/google/android/youtube/api/service/a/bp;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final e()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bp;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/a/cb;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/service/a/cb;-><init>(Lcom/google/android/youtube/api/service/a/bp;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final f()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bp;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/a/cc;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/service/a/cc;-><init>(Lcom/google/android/youtube/api/service/a/bp;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final g()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bp;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/a/cd;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/service/a/cd;-><init>(Lcom/google/android/youtube/api/service/a/bp;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final h()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bp;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/a/ce;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/service/a/ce;-><init>(Lcom/google/android/youtube/api/service/a/bp;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final i()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bp;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/a/br;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/service/a/br;-><init>(Lcom/google/android/youtube/api/service/a/bp;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final j()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bp;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/a/bt;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/service/a/bt;-><init>(Lcom/google/android/youtube/api/service/a/bp;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final k()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bp;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/a/bu;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/service/a/bu;-><init>(Lcom/google/android/youtube/api/service/a/bp;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final l()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/bp;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/a/bw;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/service/a/bw;-><init>(Lcom/google/android/youtube/api/service/a/bp;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
