.class public final Lcom/google/android/youtube/api/service/a/cg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/player/overlay/i;


# instance fields
.field private final a:Lcom/google/android/youtube/api/service/a/ch;

.field private b:Lcom/google/android/youtube/api/jar/client/bv;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lcom/google/android/youtube/api/jar/client/bv;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "client cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/api/jar/client/bv;

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/cg;->b:Lcom/google/android/youtube/api/jar/client/bv;

    new-instance v0, Lcom/google/android/youtube/api/service/a/ch;

    invoke-direct {v0, p1}, Lcom/google/android/youtube/api/service/a/ch;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/cg;->a:Lcom/google/android/youtube/api/service/a/ch;

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/cg;->a:Lcom/google/android/youtube/api/service/a/ch;

    invoke-interface {p2, v0}, Lcom/google/android/youtube/api/jar/client/bv;->a(Lcom/google/android/youtube/api/service/a/at;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/cg;->b:Lcom/google/android/youtube/api/jar/client/bv;

    return-void
.end method

.method public final a(JZZ)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/cg;->b:Lcom/google/android/youtube/api/jar/client/bv;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/cg;->b:Lcom/google/android/youtube/api/jar/client/bv;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/youtube/api/jar/client/bv;->a(JZZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final b()Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/cg;->b:Lcom/google/android/youtube/api/jar/client/bv;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/cg;->b:Lcom/google/android/youtube/api/jar/client/bv;

    invoke-interface {v0}, Lcom/google/android/youtube/api/jar/client/bv;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setListener(Lcom/google/android/youtube/core/player/overlay/j;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/cg;->a:Lcom/google/android/youtube/api/service/a/ch;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/api/service/a/ch;->a(Lcom/google/android/youtube/core/player/overlay/j;)V

    return-void
.end method
