.class public final Lcom/google/android/youtube/api/service/a/cj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/player/ai;


# instance fields
.field private final a:Lcom/google/android/youtube/api/service/a/ck;

.field private b:Lcom/google/android/youtube/api/jar/client/cb;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lcom/google/android/youtube/api/jar/client/cb;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "client cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/api/jar/client/cb;

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/cj;->b:Lcom/google/android/youtube/api/jar/client/cb;

    new-instance v0, Lcom/google/android/youtube/api/service/a/ck;

    invoke-direct {v0, p1}, Lcom/google/android/youtube/api/service/a/ck;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/cj;->a:Lcom/google/android/youtube/api/service/a/ck;

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/cj;->a:Lcom/google/android/youtube/api/service/a/ck;

    invoke-interface {p2, v0}, Lcom/google/android/youtube/api/jar/client/cb;->a(Lcom/google/android/youtube/api/service/a/aw;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/cj;->b:Lcom/google/android/youtube/api/jar/client/cb;

    return-void
.end method

.method public final setKeepScreenOn(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/cj;->b:Lcom/google/android/youtube/api/jar/client/cb;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/cj;->b:Lcom/google/android/youtube/api/jar/client/cb;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/api/jar/client/cb;->a(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setListener(Lcom/google/android/youtube/core/player/aj;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/cj;->a:Lcom/google/android/youtube/api/service/a/ck;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/api/service/a/ck;->a(Lcom/google/android/youtube/core/player/aj;)V

    return-void
.end method
