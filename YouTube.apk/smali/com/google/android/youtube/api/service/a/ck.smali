.class final Lcom/google/android/youtube/api/service/a/ck;
.super Lcom/google/android/youtube/api/service/a/ax;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/player/aj;


# instance fields
.field private final a:Landroid/os/Handler;

.field private b:Lcom/google/android/youtube/core/player/aj;


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/api/service/a/ax;-><init>()V

    const-string v0, "uiHandler cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/ck;->a:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/api/service/a/ck;)Lcom/google/android/youtube/core/player/aj;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/ck;->b:Lcom/google/android/youtube/core/player/aj;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/player/aj;)V
    .locals 1

    const-string v0, "listener cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/aj;

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/ck;->b:Lcom/google/android/youtube/core/player/aj;

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/ck;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/a/cl;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/service/a/cl;-><init>(Lcom/google/android/youtube/api/service/a/ck;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
