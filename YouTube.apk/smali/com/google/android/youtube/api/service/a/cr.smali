.class final Lcom/google/android/youtube/api/service/a/cr;
.super Lcom/google/android/youtube/api/service/a/co;
.source "SourceFile"


# instance fields
.field final synthetic b:Lcom/google/android/youtube/api/service/a/cn;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/api/service/a/cn;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/youtube/api/service/a/cr;->b:Lcom/google/android/youtube/api/service/a/cn;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/api/service/a/co;-><init>(Lcom/google/android/youtube/api/service/a/cn;B)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/api/service/a/cn;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/api/service/a/cr;-><init>(Lcom/google/android/youtube/api/service/a/cn;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/IBinder;Landroid/os/IBinder;Landroid/view/WindowManager$LayoutParams;IIIZLandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/content/res/Configuration;Landroid/view/Surface;)I
    .locals 2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "remoteSurfaceViewRelayout should not be called post ICS"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Landroid/view/Surface;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/cr;->b:Lcom/google/android/youtube/api/service/a/cn;

    invoke-static {v0}, Lcom/google/android/youtube/api/service/a/cn;->a(Lcom/google/android/youtube/api/service/a/cn;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/api/service/a/cs;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/service/a/cs;-><init>(Lcom/google/android/youtube/api/service/a/cr;Landroid/view/Surface;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
