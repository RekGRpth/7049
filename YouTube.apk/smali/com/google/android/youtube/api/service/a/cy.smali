.class final Lcom/google/android/youtube/api/service/a/cy;
.super Lcom/google/android/youtube/api/service/a/bd;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/youtube/api/service/a/cw;

.field private final b:Landroid/os/Handler;

.field private c:Lcom/google/android/youtube/api/service/a/cx;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/api/service/a/cw;Landroid/os/Handler;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/youtube/api/service/a/cy;->a:Lcom/google/android/youtube/api/service/a/cw;

    invoke-direct {p0}, Lcom/google/android/youtube/api/service/a/bd;-><init>()V

    const-string v0, "uiHandler cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/cy;->b:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/api/service/a/cy;)Lcom/google/android/youtube/api/service/a/cx;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/cy;->c:Lcom/google/android/youtube/api/service/a/cx;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/cy;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/a/db;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/service/a/db;-><init>(Lcom/google/android/youtube/api/service/a/cy;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(II)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/cy;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/a/da;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/youtube/api/service/a/da;-><init>(Lcom/google/android/youtube/api/service/a/cy;II)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Landroid/view/Surface;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/cy;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/a/cz;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/service/a/cz;-><init>(Lcom/google/android/youtube/api/service/a/cy;Landroid/view/Surface;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Lcom/google/android/youtube/api/service/a/cx;)V
    .locals 1

    const-string v0, "listener cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/api/service/a/cx;

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/cy;->c:Lcom/google/android/youtube/api/service/a/cx;

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/cy;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/a/dc;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/service/a/dc;-><init>(Lcom/google/android/youtube/api/service/a/cy;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
