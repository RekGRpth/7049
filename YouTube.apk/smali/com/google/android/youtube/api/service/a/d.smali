.class public final Lcom/google/android/youtube/api/service/a/d;
.super Lcom/google/android/youtube/api/service/a/al;
.source "SourceFile"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;
.implements Lcom/google/android/youtube/api/service/i;


# instance fields
.field private final a:Landroid/os/Handler;

.field private final b:Lcom/google/android/youtube/api/ApiPlayer;

.field private final c:Lcom/google/android/youtube/api/service/k;

.field private final d:Lcom/google/android/youtube/api/service/a/ad;

.field private final e:Lcom/google/android/youtube/api/service/a/cn;

.field private final f:Lcom/google/android/youtube/api/service/a/cw;

.field private final g:Lcom/google/android/youtube/api/service/a/cj;

.field private final h:Lcom/google/android/youtube/api/service/a/bf;

.field private final i:Lcom/google/android/youtube/api/service/a/bl;

.field private final j:Lcom/google/android/youtube/api/service/a/bo;

.field private final k:Lcom/google/android/youtube/api/service/a/cg;

.field private final l:Lcom/google/android/youtube/api/service/a/cm;

.field private final m:Lcom/google/android/youtube/api/service/a/de;

.field private final n:Lcom/google/android/youtube/api/service/a/a;

.field private o:Lcom/google/android/youtube/api/jar/client/bm;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lcom/google/android/youtube/api/service/k;Lcom/google/android/youtube/api/j;Lcom/google/android/youtube/api/jar/client/bm;Lcom/google/android/youtube/api/jar/client/cb;Lcom/google/android/youtube/api/jar/client/ch;Lcom/google/android/youtube/api/jar/client/ck;Lcom/google/android/youtube/api/jar/client/by;Lcom/google/android/youtube/api/jar/client/bj;Lcom/google/android/youtube/api/jar/client/bp;Lcom/google/android/youtube/api/jar/client/bs;Lcom/google/android/youtube/api/jar/client/bv;Lcom/google/android/youtube/api/jar/client/ce;Lcom/google/android/youtube/api/jar/client/cn;Z)V
    .locals 13

    invoke-direct {p0}, Lcom/google/android/youtube/api/service/a/al;-><init>()V

    const-string v1, "context cannot be null"

    invoke-static {p1, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "uiHandler cannot be null"

    invoke-static {p2, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Handler;

    iput-object v1, p0, Lcom/google/android/youtube/api/service/a/d;->a:Landroid/os/Handler;

    const-string v1, "serviceDestroyedNotifier"

    move-object/from16 v0, p3

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/api/service/k;

    iput-object v1, p0, Lcom/google/android/youtube/api/service/a/d;->c:Lcom/google/android/youtube/api/service/k;

    const-string v1, "apiEnvironment cannot be null"

    move-object/from16 v0, p4

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "apiPlayerClient cannot be null"

    move-object/from16 v0, p5

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/api/jar/client/bm;

    iput-object v1, p0, Lcom/google/android/youtube/api/service/a/d;->o:Lcom/google/android/youtube/api/jar/client/bm;

    const-string v1, "playerUiClient cannot be null"

    move-object/from16 v0, p6

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-nez p16, :cond_0

    const-string v1, "surfaceHolderClient cannot be null"

    move-object/from16 v0, p7

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    const-string v1, "playerSurfaceClient cannot be null"

    move-object/from16 v0, p9

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "adOverlayClient cannot be null"

    move-object/from16 v0, p10

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "brandingOverlayClient cannot be null"

    move-object/from16 v0, p11

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "controllerOverlayClient cannot be null"

    move-object/from16 v0, p12

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "liveOverlayClient cannot be null"

    move-object/from16 v0, p13

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "subtitlesOverlayClient cannot be null"

    move-object/from16 v0, p14

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "thumbnailOverlayClient cannot be null"

    move-object/from16 v0, p15

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lcom/google/android/youtube/api/service/a/cj;

    move-object/from16 v0, p6

    invoke-direct {v1, p2, v0}, Lcom/google/android/youtube/api/service/a/cj;-><init>(Landroid/os/Handler;Lcom/google/android/youtube/api/jar/client/cb;)V

    iput-object v1, p0, Lcom/google/android/youtube/api/service/a/d;->g:Lcom/google/android/youtube/api/service/a/cj;

    new-instance v1, Lcom/google/android/youtube/api/service/a/bf;

    move-object/from16 v0, p10

    invoke-direct {v1, p2, v0}, Lcom/google/android/youtube/api/service/a/bf;-><init>(Landroid/os/Handler;Lcom/google/android/youtube/api/jar/client/bj;)V

    iput-object v1, p0, Lcom/google/android/youtube/api/service/a/d;->h:Lcom/google/android/youtube/api/service/a/bf;

    new-instance v1, Lcom/google/android/youtube/api/service/a/bl;

    move-object/from16 v0, p11

    invoke-direct {v1, p2, v0}, Lcom/google/android/youtube/api/service/a/bl;-><init>(Landroid/os/Handler;Lcom/google/android/youtube/api/jar/client/bp;)V

    iput-object v1, p0, Lcom/google/android/youtube/api/service/a/d;->i:Lcom/google/android/youtube/api/service/a/bl;

    new-instance v1, Lcom/google/android/youtube/api/service/a/bo;

    move-object/from16 v0, p12

    invoke-direct {v1, p1, p2, v0}, Lcom/google/android/youtube/api/service/a/bo;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/google/android/youtube/api/jar/client/bs;)V

    iput-object v1, p0, Lcom/google/android/youtube/api/service/a/d;->j:Lcom/google/android/youtube/api/service/a/bo;

    new-instance v1, Lcom/google/android/youtube/api/service/a/cg;

    move-object/from16 v0, p13

    invoke-direct {v1, p2, v0}, Lcom/google/android/youtube/api/service/a/cg;-><init>(Landroid/os/Handler;Lcom/google/android/youtube/api/jar/client/bv;)V

    iput-object v1, p0, Lcom/google/android/youtube/api/service/a/d;->k:Lcom/google/android/youtube/api/service/a/cg;

    new-instance v1, Lcom/google/android/youtube/api/service/a/cm;

    move-object/from16 v0, p14

    invoke-direct {v1, v0}, Lcom/google/android/youtube/api/service/a/cm;-><init>(Lcom/google/android/youtube/api/jar/client/ce;)V

    iput-object v1, p0, Lcom/google/android/youtube/api/service/a/d;->l:Lcom/google/android/youtube/api/service/a/cm;

    new-instance v1, Lcom/google/android/youtube/api/service/a/de;

    move-object/from16 v0, p15

    invoke-direct {v1, v0}, Lcom/google/android/youtube/api/service/a/de;-><init>(Lcom/google/android/youtube/api/jar/client/cn;)V

    iput-object v1, p0, Lcom/google/android/youtube/api/service/a/d;->m:Lcom/google/android/youtube/api/service/a/de;

    if-nez p16, :cond_1

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/youtube/api/service/a/d;->f:Lcom/google/android/youtube/api/service/a/cw;

    new-instance v1, Lcom/google/android/youtube/api/service/a/cn;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/pm/PackageManager;)Z

    move-result v2

    move-object/from16 v0, p7

    invoke-direct {v1, p2, v0, v2}, Lcom/google/android/youtube/api/service/a/cn;-><init>(Landroid/os/Handler;Lcom/google/android/youtube/api/jar/client/ch;Z)V

    iput-object v1, p0, Lcom/google/android/youtube/api/service/a/d;->e:Lcom/google/android/youtube/api/service/a/cn;

    new-instance v1, Lcom/google/android/youtube/api/service/a/cf;

    iget-object v2, p0, Lcom/google/android/youtube/api/service/a/d;->e:Lcom/google/android/youtube/api/service/a/cn;

    move-object/from16 v0, p9

    invoke-direct {v1, v2, v0}, Lcom/google/android/youtube/api/service/a/cf;-><init>(Landroid/view/SurfaceHolder;Lcom/google/android/youtube/api/jar/client/by;)V

    iput-object v1, p0, Lcom/google/android/youtube/api/service/a/d;->n:Lcom/google/android/youtube/api/service/a/a;

    :goto_1
    new-instance v1, Lcom/google/android/youtube/api/service/a/ad;

    move-object/from16 v0, p5

    invoke-direct {v1, v0}, Lcom/google/android/youtube/api/service/a/ad;-><init>(Lcom/google/android/youtube/api/jar/client/bm;)V

    iput-object v1, p0, Lcom/google/android/youtube/api/service/a/d;->d:Lcom/google/android/youtube/api/service/a/ad;

    new-instance v1, Lcom/google/android/youtube/api/ApiPlayer;

    iget-object v3, p0, Lcom/google/android/youtube/api/service/a/d;->d:Lcom/google/android/youtube/api/service/a/ad;

    iget-object v5, p0, Lcom/google/android/youtube/api/service/a/d;->g:Lcom/google/android/youtube/api/service/a/cj;

    iget-object v6, p0, Lcom/google/android/youtube/api/service/a/d;->n:Lcom/google/android/youtube/api/service/a/a;

    iget-object v7, p0, Lcom/google/android/youtube/api/service/a/d;->h:Lcom/google/android/youtube/api/service/a/bf;

    iget-object v8, p0, Lcom/google/android/youtube/api/service/a/d;->i:Lcom/google/android/youtube/api/service/a/bl;

    iget-object v9, p0, Lcom/google/android/youtube/api/service/a/d;->j:Lcom/google/android/youtube/api/service/a/bo;

    iget-object v10, p0, Lcom/google/android/youtube/api/service/a/d;->k:Lcom/google/android/youtube/api/service/a/cg;

    iget-object v11, p0, Lcom/google/android/youtube/api/service/a/d;->l:Lcom/google/android/youtube/api/service/a/cm;

    iget-object v12, p0, Lcom/google/android/youtube/api/service/a/d;->m:Lcom/google/android/youtube/api/service/a/de;

    move-object v2, p1

    move-object/from16 v4, p4

    invoke-direct/range {v1 .. v12}, Lcom/google/android/youtube/api/ApiPlayer;-><init>(Landroid/content/Context;Lcom/google/android/youtube/api/s;Lcom/google/android/youtube/api/j;Lcom/google/android/youtube/core/player/ai;Lcom/google/android/youtube/core/player/af;Lcom/google/android/youtube/core/player/overlay/a;Lcom/google/android/youtube/core/player/overlay/c;Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;Lcom/google/android/youtube/core/player/overlay/i;Lcom/google/android/youtube/core/player/overlay/aa;Lcom/google/android/youtube/core/player/overlay/ab;)V

    iput-object v1, p0, Lcom/google/android/youtube/api/service/a/d;->b:Lcom/google/android/youtube/api/ApiPlayer;

    move-object/from16 v0, p3

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/api/service/k;->a(Lcom/google/android/youtube/api/service/i;)V

    :try_start_0
    invoke-interface/range {p5 .. p5}, Lcom/google/android/youtube/api/jar/client/bm;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, p0, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    return-void

    :cond_0
    const-string v1, "surfaceTextureClient cannot be null"

    move-object/from16 v0, p8

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/youtube/api/service/a/d;->e:Lcom/google/android/youtube/api/service/a/cn;

    new-instance v1, Lcom/google/android/youtube/api/service/a/cw;

    move-object/from16 v0, p8

    invoke-direct {v1, p2, v0}, Lcom/google/android/youtube/api/service/a/cw;-><init>(Landroid/os/Handler;Lcom/google/android/youtube/api/jar/client/ck;)V

    iput-object v1, p0, Lcom/google/android/youtube/api/service/a/d;->f:Lcom/google/android/youtube/api/service/a/cw;

    new-instance v1, Lcom/google/android/youtube/api/service/a/dd;

    iget-object v2, p0, Lcom/google/android/youtube/api/service/a/d;->f:Lcom/google/android/youtube/api/service/a/cw;

    move-object/from16 v0, p9

    invoke-direct {v1, v2, v0}, Lcom/google/android/youtube/api/service/a/dd;-><init>(Lcom/google/android/youtube/api/service/a/cw;Lcom/google/android/youtube/api/jar/client/by;)V

    iput-object v1, p0, Lcom/google/android/youtube/api/service/a/d;->n:Lcom/google/android/youtube/api/service/a/a;

    goto :goto_1

    :catch_0
    move-exception v1

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/api/service/a/d;->a(Z)V

    goto :goto_2
.end method

.method static synthetic a(Lcom/google/android/youtube/api/service/a/d;)Lcom/google/android/youtube/api/ApiPlayer;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/d;->b:Lcom/google/android/youtube/api/ApiPlayer;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/api/service/a/d;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/api/service/a/d;->d(Z)V

    return-void
.end method

.method private d(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/d;->b:Lcom/google/android/youtube/api/ApiPlayer;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/api/ApiPlayer;->c(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/d;->c:Lcom/google/android/youtube/api/service/k;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/api/service/k;->b(Lcom/google/android/youtube/api/service/i;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/d;->o:Lcom/google/android/youtube/api/jar/client/bm;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/d;->o:Lcom/google/android/youtube/api/jar/client/bm;

    invoke-interface {v0}, Lcom/google/android/youtube/api/jar/client/bm;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/api/service/a/d;->o:Lcom/google/android/youtube/api/jar/client/bm;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/d;->d:Lcom/google/android/youtube/api/service/a/ad;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/service/a/ad;->l()V

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/d;->e:Lcom/google/android/youtube/api/service/a/cn;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/d;->e:Lcom/google/android/youtube/api/service/a/cn;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/service/a/cn;->a()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/d;->f:Lcom/google/android/youtube/api/service/a/cw;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/d;->f:Lcom/google/android/youtube/api/service/a/cw;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/service/a/cw;->a()V

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/d;->g:Lcom/google/android/youtube/api/service/a/cj;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/service/a/cj;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/d;->h:Lcom/google/android/youtube/api/service/a/bf;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/service/a/bf;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/d;->i:Lcom/google/android/youtube/api/service/a/bl;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/service/a/bl;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/d;->j:Lcom/google/android/youtube/api/service/a/bo;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/service/a/bo;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/d;->k:Lcom/google/android/youtube/api/service/a/cg;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/service/a/cg;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/d;->l:Lcom/google/android/youtube/api/service/a/cm;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/service/a/cm;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/d;->m:Lcom/google/android/youtube/api/service/a/de;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/service/a/de;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/d;->n:Lcom/google/android/youtube/api/service/a/a;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/service/a/a;->a()V

    invoke-static {}, Ljava/lang/System;->gc()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/api/service/a/d;->d(Z)V

    return-void
.end method

.method public final a(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/d;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/a/j;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/service/a/j;-><init>(Lcom/google/android/youtube/api/service/a/d;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(ILandroid/view/KeyEvent;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/d;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/a/r;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/youtube/api/service/a/r;-><init>(Lcom/google/android/youtube/api/service/a/d;ILandroid/view/KeyEvent;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/d;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/a/e;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/youtube/api/service/a/e;-><init>(Lcom/google/android/youtube/api/service/a/d;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Ljava/lang/String;II)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/d;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/a/w;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/youtube/api/service/a/w;-><init>(Lcom/google/android/youtube/api/service/a/d;Ljava/lang/String;II)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Ljava/util/List;II)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/d;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/a/y;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/youtube/api/service/a/y;-><init>(Lcom/google/android/youtube/api/service/a/d;Ljava/util/List;II)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/d;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/a/g;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/service/a/g;-><init>(Lcom/google/android/youtube/api/service/a/d;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a([B)Z
    .locals 5

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    array-length v0, p1

    invoke-virtual {v3, p1, v1, v0}, Landroid/os/Parcel;->unmarshall([BII)V

    invoke-virtual {v3, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    invoke-virtual {v3}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eq v0, v2, :cond_0

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    move v0, v1

    :goto_0
    return v0

    :cond_0
    :try_start_0
    const-class v0, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/api/ApiPlayer$PlayerState;
    :try_end_0
    .catch Landroid/os/BadParcelableException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    new-instance v1, Landroid/os/ConditionVariable;

    invoke-direct {v1}, Landroid/os/ConditionVariable;-><init>()V

    iget-object v3, p0, Lcom/google/android/youtube/api/service/a/d;->a:Landroid/os/Handler;

    new-instance v4, Lcom/google/android/youtube/api/service/a/u;

    invoke-direct {v4, p0, v0, v1}, Lcom/google/android/youtube/api/service/a/u;-><init>(Lcom/google/android/youtube/api/service/a/d;Lcom/google/android/youtube/api/ApiPlayer$PlayerState;Landroid/os/ConditionVariable;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->block()V

    move v0, v2

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    move v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/d;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/a/aa;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/service/a/aa;-><init>(Lcom/google/android/youtube/api/service/a/d;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final b(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/d;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/a/k;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/service/a/k;-><init>(Lcom/google/android/youtube/api/service/a/d;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final b(ILandroid/view/KeyEvent;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/d;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/a/s;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/youtube/api/service/a/s;-><init>(Lcom/google/android/youtube/api/service/a/d;ILandroid/view/KeyEvent;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final b(Ljava/lang/String;I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/d;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/a/p;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/youtube/api/service/a/p;-><init>(Lcom/google/android/youtube/api/service/a/d;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final b(Ljava/lang/String;II)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/d;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/a/x;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/youtube/api/service/a/x;-><init>(Lcom/google/android/youtube/api/service/a/d;Ljava/lang/String;II)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final b(Ljava/util/List;II)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/d;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/a/z;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/youtube/api/service/a/z;-><init>(Lcom/google/android/youtube/api/service/a/d;Ljava/util/List;II)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final b(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/d;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/a/l;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/service/a/l;-><init>(Lcom/google/android/youtube/api/service/a/d;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final binderDied()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/api/service/a/d;->a(Z)V

    return-void
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/d;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/a/ab;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/service/a/ab;-><init>(Lcom/google/android/youtube/api/service/a/d;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final c(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/d;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/a/n;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/service/a/n;-><init>(Lcom/google/android/youtube/api/service/a/d;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final d()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/d;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/a/ac;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/service/a/ac;-><init>(Lcom/google/android/youtube/api/service/a/d;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final e()Z
    .locals 4

    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iget-object v2, p0, Lcom/google/android/youtube/api/service/a/d;->a:Landroid/os/Handler;

    new-instance v3, Lcom/google/android/youtube/api/service/a/f;

    invoke-direct {v3, p0, v1, v0}, Lcom/google/android/youtube/api/service/a/f;-><init>(Lcom/google/android/youtube/api/service/a/d;Ljava/util/concurrent/atomic/AtomicBoolean;Landroid/os/ConditionVariable;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public final f()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/d;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/a/h;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/service/a/h;-><init>(Lcom/google/android/youtube/api/service/a/d;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final g()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/d;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/a/i;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/service/a/i;-><init>(Lcom/google/android/youtube/api/service/a/d;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final h()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/d;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/a/m;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/service/a/m;-><init>(Lcom/google/android/youtube/api/service/a/d;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final i()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/d;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/a/o;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/service/a/o;-><init>(Lcom/google/android/youtube/api/service/a/d;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final j()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/d;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/a/q;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/service/a/q;-><init>(Lcom/google/android/youtube/api/service/a/d;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final k()[B
    .locals 4

    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    new-instance v1, Landroid/os/ConditionVariable;

    invoke-direct {v1}, Landroid/os/ConditionVariable;-><init>()V

    iget-object v2, p0, Lcom/google/android/youtube/api/service/a/d;->a:Landroid/os/Handler;

    new-instance v3, Lcom/google/android/youtube/api/service/a/t;

    invoke-direct {v3, p0, v0, v1}, Lcom/google/android/youtube/api/service/a/t;-><init>(Lcom/google/android/youtube/api/service/a/d;Ljava/util/concurrent/atomic/AtomicReference;Landroid/os/ConditionVariable;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->block()V

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    move-result-object v0

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    return-object v0
.end method

.method public final l()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/a/d;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/a/v;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/service/a/v;-><init>(Lcom/google/android/youtube/api/service/a/d;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
