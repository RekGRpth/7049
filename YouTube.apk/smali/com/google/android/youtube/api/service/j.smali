.class final Lcom/google/android/youtube/api/service/j;
.super Lcom/google/android/youtube/player/internal/a;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/youtube/api/service/YouTubeService;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/api/service/YouTubeService;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/api/service/j;->a:Lcom/google/android/youtube/api/service/YouTubeService;

    invoke-direct {p0}, Lcom/google/android/youtube/player/internal/a;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/api/service/YouTubeService;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/api/service/j;-><init>(Lcom/google/android/youtube/api/service/YouTubeService;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/player/internal/d;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 8

    const/16 v0, 0x3e8

    if-ge p2, v0, :cond_0

    sget-object v0, Lcom/google/android/youtube/player/YouTubeInitializationResult;->CLIENT_LIBRARY_UPDATE_REQUIRED:Lcom/google/android/youtube/player/YouTubeInitializationResult;

    invoke-static {p1, v0}, Lcom/google/android/youtube/api/service/YouTubeService;->a(Lcom/google/android/youtube/player/internal/d;Lcom/google/android/youtube/player/YouTubeInitializationResult;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/j;->a:Lcom/google/android/youtube/api/service/YouTubeService;

    invoke-static {v0, p3}, Lcom/google/android/youtube/api/service/YouTubeService;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/android/youtube/player/YouTubeInitializationResult;->INTERNAL_ERROR:Lcom/google/android/youtube/player/YouTubeInitializationResult;

    invoke-static {p1, v0}, Lcom/google/android/youtube/api/service/YouTubeService;->a(Lcom/google/android/youtube/player/internal/d;Lcom/google/android/youtube/player/YouTubeInitializationResult;)V

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/youtube/api/service/l;

    iget-object v1, p0, Lcom/google/android/youtube/api/service/j;->a:Lcom/google/android/youtube/api/service/YouTubeService;

    iget-object v2, p0, Lcom/google/android/youtube/api/service/j;->a:Lcom/google/android/youtube/api/service/YouTubeService;

    invoke-static {v2}, Lcom/google/android/youtube/api/service/YouTubeService;->a(Lcom/google/android/youtube/api/service/YouTubeService;)Lcom/google/android/youtube/api/service/k;

    move-result-object v2

    invoke-static {p2}, Lcom/google/android/youtube/player/internal/util/b;->a(I)Ljava/lang/String;

    move-result-object v6

    move-object v3, p5

    move-object v4, p3

    move-object v5, p4

    move-object v7, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/youtube/api/service/l;-><init>(Landroid/content/Context;Lcom/google/android/youtube/api/service/k;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/player/internal/d;)V

    goto :goto_0
.end method
