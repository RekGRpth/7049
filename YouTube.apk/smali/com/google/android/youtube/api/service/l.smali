.class final Lcom/google/android/youtube/api/service/l;
.super Lcom/google/android/youtube/player/internal/af;
.source "SourceFile"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;
.implements Lcom/google/android/youtube/api/m;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/os/Handler;

.field private final c:Lcom/google/android/youtube/api/service/k;

.field private final d:Ljava/lang/String;

.field private volatile e:Lcom/google/android/youtube/player/internal/d;

.field private volatile f:Lcom/google/android/youtube/api/j;

.field private volatile g:Lcom/google/android/youtube/api/service/a/b;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/api/service/k;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/player/internal/d;)V
    .locals 7

    invoke-direct {p0}, Lcom/google/android/youtube/player/internal/af;-><init>()V

    const-string v0, "context cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/youtube/api/service/l;->a:Landroid/content/Context;

    const-string v0, "callbacks cannot be null"

    invoke-static {p7, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/player/internal/d;

    iput-object v0, p0, Lcom/google/android/youtube/api/service/l;->e:Lcom/google/android/youtube/player/internal/d;

    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/youtube/api/service/l;->b:Landroid/os/Handler;

    const-string v0, "serviceDestroyedNotifier"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/api/service/k;

    iput-object v0, p0, Lcom/google/android/youtube/api/service/l;->c:Lcom/google/android/youtube/api/service/k;

    const-string v0, "appPackage cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/api/service/l;->d:Ljava/lang/String;

    const-string v0, "appVersion cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "developerKey cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/youtube/api/service/l;->b:Landroid/os/Handler;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-static/range {v0 .. v6}, Lcom/google/android/youtube/api/j;->a(Lcom/google/android/youtube/api/m;Landroid/os/Handler;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/api/service/l;)Lcom/google/android/youtube/api/j;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/l;->f:Lcom/google/android/youtube/api/j;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/api/service/l;Lcom/google/android/youtube/api/j;)Lcom/google/android/youtube/api/j;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/api/service/l;->f:Lcom/google/android/youtube/api/j;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/api/service/l;Lcom/google/android/youtube/api/service/a/b;)Lcom/google/android/youtube/api/service/a/b;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/api/service/l;->g:Lcom/google/android/youtube/api/service/a/b;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/api/service/l;Lcom/google/android/youtube/player/internal/d;)Lcom/google/android/youtube/player/internal/d;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/api/service/l;->e:Lcom/google/android/youtube/player/internal/d;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/api/service/l;)Lcom/google/android/youtube/player/internal/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/service/l;->e:Lcom/google/android/youtube/player/internal/d;

    return-object v0
.end method

.method private b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/l;->f:Lcom/google/android/youtube/api/j;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "YouTubeServiceEntity not initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Landroid/os/IBinder;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/api/service/l;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/api/service/l;->g:Lcom/google/android/youtube/api/service/a/b;

    invoke-virtual {v0}, Lcom/google/android/youtube/api/service/a/b;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/youtube/player/internal/y;)Lcom/google/android/youtube/player/internal/ab;
    .locals 3

    invoke-direct {p0}, Lcom/google/android/youtube/api/service/l;->b()V

    new-instance v0, Lcom/google/android/youtube/api/service/a;

    iget-object v1, p0, Lcom/google/android/youtube/api/service/l;->b:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/youtube/api/service/l;->f:Lcom/google/android/youtube/api/j;

    invoke-direct {v0, v1, v2, p1}, Lcom/google/android/youtube/api/service/a;-><init>(Landroid/os/Handler;Lcom/google/android/youtube/api/j;Lcom/google/android/youtube/player/internal/y;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/youtube/api/j;)V
    .locals 4

    iput-object p1, p0, Lcom/google/android/youtube/api/service/l;->f:Lcom/google/android/youtube/api/j;

    new-instance v0, Lcom/google/android/youtube/api/service/a/b;

    iget-object v1, p0, Lcom/google/android/youtube/api/service/l;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/youtube/api/service/l;->b:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/youtube/api/service/l;->c:Lcom/google/android/youtube/api/service/k;

    invoke-direct {v0, v1, v2, v3, p1}, Lcom/google/android/youtube/api/service/a/b;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/google/android/youtube/api/service/k;Lcom/google/android/youtube/api/j;)V

    iput-object v0, p0, Lcom/google/android/youtube/api/service/l;->g:Lcom/google/android/youtube/api/service/a/b;

    iget-object v0, p0, Lcom/google/android/youtube/api/service/l;->e:Lcom/google/android/youtube/player/internal/d;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/service/l;->e:Lcom/google/android/youtube/player/internal/d;

    invoke-interface {v0}, Lcom/google/android/youtube/player/internal/d;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V

    iget-object v0, p0, Lcom/google/android/youtube/api/service/l;->e:Lcom/google/android/youtube/player/internal/d;

    sget-object v1, Lcom/google/android/youtube/player/YouTubeInitializationResult;->SUCCESS:Lcom/google/android/youtube/player/YouTubeInitializationResult;

    invoke-virtual {v1}, Lcom/google/android/youtube/player/YouTubeInitializationResult;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/youtube/api/service/l;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/player/internal/d;->a(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 2

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/api/service/l;->f:Lcom/google/android/youtube/api/j;

    const-string v0, "Error creating ApiEnvironment"

    invoke-static {v0, p1}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/service/l;->e:Lcom/google/android/youtube/player/internal/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/service/l;->e:Lcom/google/android/youtube/player/internal/d;

    invoke-static {p1}, Lcom/google/android/youtube/api/j;->a(Ljava/lang/Exception;)Lcom/google/android/youtube/player/YouTubeInitializationResult;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/youtube/api/service/YouTubeService;->a(Lcom/google/android/youtube/player/internal/d;Lcom/google/android/youtube/player/YouTubeInitializationResult;)V

    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/service/l;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/api/service/m;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/service/m;-><init>(Lcom/google/android/youtube/api/service/l;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final binderDied()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/api/service/l;->a(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/api/service/l;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/api/j;->a(Ljava/lang/String;)V

    return-void
.end method
