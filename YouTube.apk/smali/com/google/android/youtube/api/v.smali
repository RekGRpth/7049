.class public final Lcom/google/android/youtube/api/v;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/os/Handler;

.field private final b:Lcom/google/android/youtube/core/client/bc;

.field private final c:Lcom/google/android/youtube/core/client/be;

.field private final d:Lcom/google/android/youtube/core/utils/p;

.field private final e:Lcom/google/android/youtube/api/y;

.field private f:Lcom/google/android/youtube/core/async/p;

.field private g:Lcom/google/android/youtube/api/w;

.field private h:Lcom/google/android/youtube/core/async/a/a;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/api/w;Lcom/google/android/youtube/api/j;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "listener cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/api/w;

    iput-object v0, p0, Lcom/google/android/youtube/api/v;->g:Lcom/google/android/youtube/api/w;

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/youtube/api/v;->a:Landroid/os/Handler;

    invoke-virtual {p2}, Lcom/google/android/youtube/api/j;->a()Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/api/v;->b:Lcom/google/android/youtube/core/client/bc;

    invoke-virtual {p2}, Lcom/google/android/youtube/api/j;->e_()Lcom/google/android/youtube/core/client/be;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/api/v;->c:Lcom/google/android/youtube/core/client/be;

    invoke-virtual {p2}, Lcom/google/android/youtube/api/j;->l()Lcom/google/android/youtube/core/utils/p;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/api/v;->d:Lcom/google/android/youtube/core/utils/p;

    new-instance v0, Lcom/google/android/youtube/api/y;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/api/y;-><init>(Lcom/google/android/youtube/api/v;B)V

    iput-object v0, p0, Lcom/google/android/youtube/api/v;->e:Lcom/google/android/youtube/api/y;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/api/v;Lcom/google/android/youtube/core/async/p;)Lcom/google/android/youtube/core/async/p;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/api/v;->f:Lcom/google/android/youtube/core/async/p;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/youtube/api/v;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/api/v;->d:Lcom/google/android/youtube/core/utils/p;

    invoke-interface {v0}, Lcom/google/android/youtube/core/utils/p;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/youtube/player/YouTubeThumbnailLoader$ErrorReason;->NETWORK_ERROR:Lcom/google/android/youtube/player/YouTubeThumbnailLoader$ErrorReason;

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/api/v;->g:Lcom/google/android/youtube/api/w;

    invoke-virtual {v0}, Lcom/google/android/youtube/player/YouTubeThumbnailLoader$ErrorReason;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/v;->e()Z

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/youtube/api/v;->d()Z

    move-result v3

    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/youtube/api/w;->a(Ljava/lang/String;ZZ)V

    return-void

    :cond_0
    sget-object v0, Lcom/google/android/youtube/player/YouTubeThumbnailLoader$ErrorReason;->INTERNAL_ERROR:Lcom/google/android/youtube/player/YouTubeThumbnailLoader$ErrorReason;

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/youtube/api/v;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/v;->a:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/api/v;)Lcom/google/android/youtube/core/client/be;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/v;->c:Lcom/google/android/youtube/core/client/be;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/api/v;)Lcom/google/android/youtube/api/w;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/v;->g:Lcom/google/android/youtube/api/w;

    return-object v0
.end method

.method private f()Lcom/google/android/youtube/core/async/n;
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/v;->f:Lcom/google/android/youtube/core/async/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/v;->f:Lcom/google/android/youtube/core/async/p;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/p;->a()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/api/v;->e:Lcom/google/android/youtube/api/y;

    invoke-static {v0}, Lcom/google/android/youtube/core/async/p;->a(Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/p;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/api/v;->f:Lcom/google/android/youtube/core/async/p;

    iget-object v1, p0, Lcom/google/android/youtube/api/v;->a:Landroid/os/Handler;

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/async/am;->a(Landroid/os/Handler;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/am;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/api/v;->h:Lcom/google/android/youtube/core/async/a/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/v;->h:Lcom/google/android/youtube/core/async/a/a;

    invoke-interface {v0}, Lcom/google/android/youtube/core/async/a/a;->d()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/api/v;->h:Lcom/google/android/youtube/core/async/a/a;

    if-nez v0, :cond_1

    const-string v0, "due to no playlist being set."

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Ignoring call to next() on YouTubeThumbnailView "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/youtube/player/internal/util/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_1
    return-void

    :cond_1
    const-string v0, "as already at the end of the playlist."

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/api/v;->h:Lcom/google/android/youtube/core/async/a/a;

    invoke-interface {v0}, Lcom/google/android/youtube/core/async/a/a;->a()V

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    const-string v0, "videoId cannot be empty"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/api/v;->h:Lcom/google/android/youtube/core/async/a/a;

    iget-object v0, p0, Lcom/google/android/youtube/api/v;->b:Lcom/google/android/youtube/core/client/bc;

    invoke-direct {p0}, Lcom/google/android/youtube/api/v;->f()Lcom/google/android/youtube/core/async/n;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/youtube/core/client/bc;->a(Ljava/lang/String;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 2

    const-string v0, "playlistId cannot be null or empty"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/api/v;->h:Lcom/google/android/youtube/core/async/a/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/v;->h:Lcom/google/android/youtube/core/async/a/a;

    invoke-interface {v0}, Lcom/google/android/youtube/core/async/a/a;->f()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/api/v;->b:Lcom/google/android/youtube/core/client/bc;

    invoke-static {v0, p1, p2}, Lcom/google/android/youtube/core/async/a/b;->a(Lcom/google/android/youtube/core/client/bc;Ljava/lang/String;I)Lcom/google/android/youtube/core/async/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/api/v;->h:Lcom/google/android/youtube/core/async/a/a;

    iget-object v0, p0, Lcom/google/android/youtube/api/v;->h:Lcom/google/android/youtube/core/async/a/a;

    invoke-direct {p0}, Lcom/google/android/youtube/api/v;->f()Lcom/google/android/youtube/core/async/n;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/async/a/a;->a(Lcom/google/android/youtube/core/async/n;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/v;->h:Lcom/google/android/youtube/core/async/a/a;

    invoke-interface {v0}, Lcom/google/android/youtube/core/async/a/a;->a()V

    return-void
.end method

.method public final b()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/api/v;->h:Lcom/google/android/youtube/core/async/a/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/v;->h:Lcom/google/android/youtube/core/async/a/a;

    invoke-interface {v0}, Lcom/google/android/youtube/core/async/a/a;->e()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/api/v;->h:Lcom/google/android/youtube/core/async/a/a;

    if-nez v0, :cond_1

    const-string v0, "due to no playlist being set."

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Ignoring call to previous() on YouTubeThumbnailView "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/youtube/player/internal/util/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_1
    return-void

    :cond_1
    const-string v0, "as already at the start of the playlist."

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/api/v;->h:Lcom/google/android/youtube/core/async/a/a;

    invoke-interface {v0}, Lcom/google/android/youtube/core/async/a/a;->b()V

    goto :goto_1
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/v;->h:Lcom/google/android/youtube/core/async/a/a;

    if-nez v0, :cond_0

    const-string v0, "Ignoring call to first() on YouTubeThumbnailView due to no playlist being set."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/youtube/player/internal/util/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/api/v;->h:Lcom/google/android/youtube/core/async/a/a;

    invoke-interface {v0}, Lcom/google/android/youtube/core/async/a/a;->f()V

    iget-object v0, p0, Lcom/google/android/youtube/api/v;->h:Lcom/google/android/youtube/core/async/a/a;

    invoke-interface {v0}, Lcom/google/android/youtube/core/async/a/a;->g()Lcom/google/android/youtube/core/async/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/api/v;->h:Lcom/google/android/youtube/core/async/a/a;

    iget-object v0, p0, Lcom/google/android/youtube/api/v;->h:Lcom/google/android/youtube/core/async/a/a;

    invoke-direct {p0}, Lcom/google/android/youtube/api/v;->f()Lcom/google/android/youtube/core/async/n;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/async/a/a;->a(Lcom/google/android/youtube/core/async/n;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/v;->h:Lcom/google/android/youtube/core/async/a/a;

    invoke-interface {v0}, Lcom/google/android/youtube/core/async/a/a;->a()V

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/v;->h:Lcom/google/android/youtube/core/async/a/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/v;->h:Lcom/google/android/youtube/core/async/a/a;

    invoke-interface {v0}, Lcom/google/android/youtube/core/async/a/a;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/v;->h:Lcom/google/android/youtube/core/async/a/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/v;->h:Lcom/google/android/youtube/core/async/a/a;

    invoke-interface {v0}, Lcom/google/android/youtube/core/async/a/a;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
