.class final Lcom/google/android/youtube/api/x;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/api/v;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/api/v;Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/youtube/api/x;->a:Lcom/google/android/youtube/api/v;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "videoId cannot be null or empty"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/api/x;->b:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    const-string v0, "Error loading DefaultThumbnailLoader"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/youtube/api/x;->a:Lcom/google/android/youtube/api/v;

    invoke-static {v0}, Lcom/google/android/youtube/api/v;->a(Lcom/google/android/youtube/api/v;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    check-cast p2, Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/youtube/api/x;->a:Lcom/google/android/youtube/api/v;

    invoke-static {v0}, Lcom/google/android/youtube/api/v;->d(Lcom/google/android/youtube/api/v;)Lcom/google/android/youtube/api/w;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/api/x;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/api/x;->a:Lcom/google/android/youtube/api/v;

    invoke-virtual {v2}, Lcom/google/android/youtube/api/v;->e()Z

    move-result v2

    iget-object v3, p0, Lcom/google/android/youtube/api/x;->a:Lcom/google/android/youtube/api/v;

    invoke-virtual {v3}, Lcom/google/android/youtube/api/v;->d()Z

    move-result v3

    invoke-interface {v0, p2, v1, v2, v3}, Lcom/google/android/youtube/api/w;->a(Landroid/graphics/Bitmap;Ljava/lang/String;ZZ)V

    return-void
.end method
