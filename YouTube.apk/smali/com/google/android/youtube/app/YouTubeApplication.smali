.class public Lcom/google/android/youtube/app/YouTubeApplication;
.super Lcom/google/android/youtube/core/BaseApplication;
.source "SourceFile"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Lcom/google/android/youtube/core/async/bl;
.implements Lcom/google/android/youtube/core/client/bd;
.implements Lcom/google/android/youtube/core/client/bf;
.implements Lcom/google/android/youtube/core/client/bk;
.implements Lcom/google/android/youtube/core/client/bm;
.implements Lcom/google/android/youtube/core/client/c;


# instance fields
.field private A:Ljava/util/concurrent/atomic/AtomicReference;

.field private B:Ljava/util/concurrent/atomic/AtomicReference;

.field private C:I

.field private D:Lcom/google/android/youtube/app/a;

.field private final a:Ljava/lang/String;

.field private b:Lcom/google/android/youtube/app/k;

.field private c:Landroid/provider/SearchRecentSuggestions;

.field private d:Lcom/google/android/youtube/core/suggest/a;

.field private e:Lcom/google/android/youtube/core/async/by;

.field private f:Lcom/google/android/youtube/core/async/UserDelegator;

.field private g:Lcom/google/android/youtube/core/client/v;

.field private h:Lcom/google/android/youtube/core/client/am;

.field private i:Lcom/google/android/youtube/core/client/l;

.field private j:Lcom/google/android/youtube/core/client/b;

.field private k:Lcom/google/android/youtube/core/client/ap;

.field private l:Lcom/google/android/youtube/core/client/ak;

.field private m:Lcom/google/android/youtube/core/client/ai;

.field private n:Lcom/google/android/youtube/gmsplus1/f;

.field private o:Lcom/google/android/youtube/app/remote/bf;

.field private p:Lcom/google/android/youtube/app/remote/bq;

.field private q:Lcom/google/android/youtube/core/client/at;

.field private r:Lcom/google/android/youtube/app/remote/br;

.field private s:Lcom/google/android/youtube/app/remote/e;

.field private t:Lcom/google/android/youtube/app/remote/AtHomeConnection;

.field private u:Lcom/google/android/youtube/core/client/bo;

.field private v:Lcom/google/android/youtube/app/prefetch/d;

.field private w:Lcom/google/android/youtube/app/a/a;

.field private x:Lcom/google/android/youtube/app/player/a/e;

.field private y:Lcom/google/android/youtube/app/remote/ad;

.field private z:Ljava/util/concurrent/atomic/AtomicReference;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/core/BaseApplication;-><init>()V

    const-string v0, "android"

    iput-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->a:Ljava/lang/String;

    return-void
.end method

.method private ab()Z
    .locals 4

    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    const/16 v1, 0xce4

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->b:Lcom/google/android/youtube/app/k;

    iget-object v0, v0, Lcom/google/android/youtube/app/k;->a:Lcom/google/android/youtube/app/l;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return v3

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public final A()Lcom/google/android/youtube/app/remote/br;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->r:Lcom/google/android/youtube/app/remote/br;

    return-object v0
.end method

.method public final B()Lcom/google/android/youtube/app/remote/bq;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->p:Lcom/google/android/youtube/app/remote/bq;

    return-object v0
.end method

.method public final C()Lcom/google/android/youtube/app/remote/e;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->s:Lcom/google/android/youtube/app/remote/e;

    return-object v0
.end method

.method public final D()Lcom/google/android/youtube/app/remote/AtHomeConnection;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->t:Lcom/google/android/youtube/app/remote/AtHomeConnection;

    return-object v0
.end method

.method public final E()Lcom/google/android/youtube/app/prefetch/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->v:Lcom/google/android/youtube/app/prefetch/d;

    return-object v0
.end method

.method public final F()Ljava/lang/String;
    .locals 4

    sget-object v2, Lcom/google/android/youtube/core/async/GDataRequestFactory;->t:Ljava/util/Set;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/YouTubeApplication;->S()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "country"

    const-string v3, ""

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    move-object v1, v0

    const-string v0, "phone"

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/YouTubeApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {v1}, Lcom/google/android/youtube/core/utils/Util;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v0, v1

    goto :goto_0

    :cond_2
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->b:Lcom/google/android/youtube/app/k;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/k;->G()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_3
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final G()V
    .locals 2

    const/4 v1, 0x0

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->z:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->A:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->B:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    const/4 v0, -0x2

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/YouTubeApplication;->b(I)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/YouTubeApplication;->ab()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/google/android/youtube/app/autosync/a;->b(Landroid/content/Context;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Ljava/util/concurrent/Executor;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/app/j;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/j;-><init>(Lcom/google/android/youtube/app/YouTubeApplication;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    invoke-static {p0}, Lcom/google/android/youtube/app/prefetch/PrefetchService;->c(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/google/android/youtube/app/m;->a(Landroid/content/Context;)V

    return-void
.end method

.method public final H()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->B:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final I()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->C:I

    return v0
.end method

.method public final J()Lcom/google/android/youtube/app/player/a/e;
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->x:Lcom/google/android/youtube/app/player/a/e;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/YouTubeApplication;->S()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/g;->a(Landroid/content/SharedPreferences;)Ljava/security/Key;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Ljava/util/concurrent/Executor;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/youtube/app/YouTubeApplication;->S()Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/google/android/youtube/app/player/a/e;->a(Ljava/util/concurrent/Executor;Ljava/security/Key;Landroid/content/SharedPreferences;)Lcom/google/android/youtube/app/player/a/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->x:Lcom/google/android/youtube/app/player/a/e;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->x:Lcom/google/android/youtube/app/player/a/e;

    return-object v0
.end method

.method public final K()Lcom/google/android/youtube/app/remote/ad;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->y:Lcom/google/android/youtube/app/remote/ad;

    return-object v0
.end method

.method public final a()Lcom/google/android/youtube/core/client/bc;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->g:Lcom/google/android/youtube/core/client/v;

    return-object v0
.end method

.method public final a(I)V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/youtube/app/YouTubeApplication;->S()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "session_summary"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    invoke-static {v0}, Lcom/google/android/youtube/app/compat/ac;->a(Landroid/content/SharedPreferences;)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v0

    const-string v2, "session_summary"

    or-int/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Lcom/google/android/youtube/app/compat/ad;->a(Ljava/lang/String;I)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/compat/ad;->a()V

    return-void
.end method

.method public final a(Landroid/app/Activity;Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 2

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->z:Ljava/util/concurrent/atomic/AtomicReference;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/UserAuth;->username:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->A:Ljava/util/concurrent/atomic/AtomicReference;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/UserAuth;->channelId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->B:Ljava/util/concurrent/atomic/AtomicReference;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/UserAuth;->account:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/YouTubeApplication;->b(I)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/YouTubeApplication;->ab()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/google/android/youtube/app/autosync/a;->a(Lcom/google/android/youtube/app/YouTubeApplication;)V

    :cond_0
    invoke-static {p0}, Lcom/google/android/youtube/app/prefetch/PrefetchService;->c(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/google/android/youtube/app/m;->a(Landroid/content/Context;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/YouTubeApplication;->a(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->n:Lcom/google/android/youtube/gmsplus1/f;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/UserAuth;->account:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/youtube/gmsplus1/f;->a(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->q:Lcom/google/android/youtube/core/client/at;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/client/at;->f()V

    return-void
.end method

.method protected final b()V
    .locals 23

    new-instance v3, Lcom/google/android/youtube/core/utils/h;

    const-string v2, "connectivity"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/YouTubeApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/ConnectivityManager;

    invoke-direct {v3, v2}, Lcom/google/android/youtube/core/utils/h;-><init>(Landroid/net/ConnectivityManager;)V

    invoke-virtual {v3}, Lcom/google/android/youtube/core/utils/h;->g()Z

    move-result v2

    new-instance v3, Lcom/google/android/youtube/app/k;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-direct {v3, v4, v2}, Lcom/google/android/youtube/app/k;-><init>(Landroid/content/ContentResolver;Z)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/app/YouTubeApplication;->b:Lcom/google/android/youtube/app/k;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->b:Lcom/google/android/youtube/app/k;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/k;->m()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    new-instance v3, Landroid/content/ComponentName;

    const-class v4, Lcom/google/android/youtube/app/prefetch/PrefetchService;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v4, 0x1

    const/4 v5, 0x1

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    new-instance v3, Landroid/content/ComponentName;

    const-class v4, Lcom/google/android/youtube/app/prefetch/PrefetchService$DeviceStateReceiver;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v4, 0x1

    const/4 v5, 0x1

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    :cond_0
    invoke-super/range {p0 .. p0}, Lcom/google/android/youtube/core/BaseApplication;->b()V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->l()Lcom/google/android/youtube/core/utils/p;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/youtube/core/utils/p;->g()Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v2, Landroid/content/ComponentName;

    const-string v3, "com.google.android.youtube.ManageNetworkUsageActivity"

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v4

    const/4 v5, 0x1

    if-eq v4, v5, :cond_1

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    const/4 v4, 0x1

    const/4 v5, 0x1

    invoke-virtual {v3, v2, v4, v5}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->S()Landroid/content/SharedPreferences;

    move-result-object v20

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->aa()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static/range {v20 .. v20}, Lcom/google/android/youtube/app/compat/ac;->a(Landroid/content/SharedPreferences;)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v2

    const-string v3, "download_only_while_charging"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/google/android/youtube/app/compat/ad;->a(Ljava/lang/String;Z)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v2

    const-string v3, "transfer_max_connections"

    const/4 v4, 0x3

    invoke-virtual {v2, v3, v4}, Lcom/google/android/youtube/app/compat/ad;->a(Ljava/lang/String;I)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/youtube/app/compat/ad;->a()V

    :cond_2
    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    new-instance v2, Lcom/google/android/youtube/app/a;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-direct {v2, v3, v0}, Lcom/google/android/youtube/app/a;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->D:Lcom/google/android/youtube/app/a;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->X()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-static {v2, v0}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;Landroid/content/SharedPreferences;)Lcom/google/android/youtube/core/utils/Util$StartupType;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b00c4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->i()Lcom/google/android/youtube/core/Analytics;

    move-result-object v4

    const/4 v5, 0x3

    const-string v6, "FormFactor"

    const/4 v7, 0x2

    invoke-interface {v4, v5, v6, v3, v7}, Lcom/google/android/youtube/core/Analytics;->a(ILjava/lang/String;Ljava/lang/String;I)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->i()Lcom/google/android/youtube/core/Analytics;

    move-result-object v3

    const-string v4, "Startup"

    invoke-virtual {v2}, Lcom/google/android/youtube/core/utils/Util$StartupType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v4, v2}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "session_summary"

    const/4 v3, -0x1

    move-object/from16 v0, v20

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    const/4 v2, -0x1

    if-eq v3, v2, :cond_4

    const-string v2, "session_watch_count"

    const/4 v4, 0x0

    move-object/from16 v0, v20

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    and-int/lit8 v2, v3, 0x1

    if-nez v2, :cond_a

    const-string v2, "Out"

    :goto_0
    and-int/lit16 v5, v3, 0x2378

    if-nez v5, :cond_d

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    and-int/lit8 v2, v3, 0x2

    if-nez v2, :cond_b

    const-string v2, " NoWatch"

    :goto_1
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    and-int/lit8 v2, v3, 0x4

    if-nez v2, :cond_c

    const-string v2, " NoHome"

    :goto_2
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_3
    :goto_3
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->i()Lcom/google/android/youtube/core/Analytics;

    move-result-object v3

    const-string v5, "SessionSummary"

    invoke-interface {v3, v5, v2, v4}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;I)V

    :cond_4
    invoke-static/range {v20 .. v20}, Lcom/google/android/youtube/app/compat/ac;->a(Landroid/content/SharedPreferences;)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v2

    const-string v3, "session_watch_count"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/google/android/youtube/app/compat/ad;->a(Ljava/lang/String;I)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/youtube/app/compat/ad;->a()V

    invoke-static/range {v20 .. v20}, Lcom/google/android/youtube/app/compat/ac;->a(Landroid/content/SharedPreferences;)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v2

    const-string v3, "session_summary"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/google/android/youtube/app/compat/ad;->a(Ljava/lang/String;I)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/youtube/app/compat/ad;->a()V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->aa()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getCacheDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    new-instance v3, Lcom/google/android/youtube/app/i;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v2}, Lcom/google/android/youtube/app/i;-><init>(Lcom/google/android/youtube/app/YouTubeApplication;[Ljava/io/File;)V

    invoke-virtual {v3}, Lcom/google/android/youtube/app/i;->start()V

    :cond_5
    new-instance v5, Lcom/google/android/youtube/core/utils/aj;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-direct {v5, v2}, Lcom/google/android/youtube/core/utils/aj;-><init>(Landroid/content/ContentResolver;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "android_id"

    invoke-static {v2, v3}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v2, "%x"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    new-instance v6, Ljava/util/Random;

    invoke-direct {v6}, Ljava/util/Random;-><init>()V

    invoke-virtual {v6}, Ljava/util/Random;->nextLong()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    :cond_6
    new-instance v2, Lcom/google/android/youtube/core/client/u;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Ljava/util/concurrent/Executor;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->P()Lorg/apache/http/client/HttpClient;

    move-result-object v4

    sget-object v6, Lcom/google/android/youtube/app/c;->a:[B

    sget-object v7, Lcom/google/android/youtube/app/c;->b:[B

    invoke-direct/range {v2 .. v8}, Lcom/google/android/youtube/core/client/u;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/utils/aj;[B[BLjava/lang/String;)V

    new-instance v21, Lcom/google/android/youtube/core/async/v;

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-direct {v0, v2, v1}, Lcom/google/android/youtube/core/async/v;-><init>(Lcom/google/android/youtube/core/client/as;Landroid/content/SharedPreferences;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->U()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/google/android/youtube/core/async/UserAuthorizer;->d()Z

    move-result v2

    if-eqz v2, :cond_7

    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/YouTubeApplication;->a(I)V

    :cond_7
    new-instance v2, Ljava/util/concurrent/atomic/AtomicReference;

    const-string v3, "username"

    const/4 v4, 0x0

    move-object/from16 v0, v20

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->z:Ljava/util/concurrent/atomic/AtomicReference;

    new-instance v2, Ljava/util/concurrent/atomic/AtomicReference;

    const-string v3, "user_channel_id"

    const/4 v4, 0x0

    move-object/from16 v0, v20

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->A:Ljava/util/concurrent/atomic/AtomicReference;

    new-instance v2, Ljava/util/concurrent/atomic/AtomicReference;

    const-string v3, "user_account"

    const/4 v4, 0x0

    move-object/from16 v0, v20

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->B:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual/range {v22 .. v23}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bl;)V

    const/4 v2, -0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/YouTubeApplication;->b(I)V

    new-instance v2, Lcom/google/android/youtube/core/client/aj;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08002d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f08002e

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f080033

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    sget v6, Lcom/google/android/youtube/core/utils/Util;->a:I

    const/16 v7, 0xa

    if-le v6, v7, :cond_12

    const/4 v6, 0x1

    :goto_4
    const/4 v7, 0x1

    invoke-direct/range {v2 .. v7}, Lcom/google/android/youtube/core/client/aj;-><init>(IIIZZ)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Ljava/util/concurrent/Executor;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->O()Ljava/util/concurrent/Executor;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->P()Lorg/apache/http/client/HttpClient;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getCacheDir()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->L()Lcom/google/android/youtube/core/utils/ag;

    move-result-object v7

    invoke-static/range {p0 .. p0}, Lcom/google/android/youtube/core/utils/Util;->f(Landroid/content/Context;)Z

    move-result v9

    move-object v8, v2

    invoke-static/range {v3 .. v9}, Lcom/google/android/youtube/core/client/ai;->a(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Ljava/lang/String;Lcom/google/android/youtube/core/utils/e;Lcom/google/android/youtube/core/client/aj;Z)Lcom/google/android/youtube/core/client/ai;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->m:Lcom/google/android/youtube/core/client/ai;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0001

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    new-instance v8, Lcom/google/android/youtube/core/async/GDataRequestFactory;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->V()Lcom/google/android/youtube/core/utils/SafeSearch;

    move-result-object v3

    invoke-static/range {p0 .. p0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v8, v2, v3, v4}, Lcom/google/android/youtube/core/async/GDataRequestFactory;-><init>(ILcom/google/android/youtube/core/utils/SafeSearch;Ljava/lang/String;)V

    new-instance v2, Lcom/google/android/youtube/core/client/v;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Ljava/util/concurrent/Executor;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->P()Lorg/apache/http/client/HttpClient;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->L()Lcom/google/android/youtube/core/utils/ag;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->R()Lcom/google/android/youtube/core/converter/m;

    move-result-object v7

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v8}, Lcom/google/android/youtube/core/client/v;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/utils/e;Lcom/google/android/youtube/core/converter/m;Lcom/google/android/youtube/core/async/GDataRequestFactory;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->g:Lcom/google/android/youtube/core/client/v;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->b:Lcom/google/android/youtube/app/k;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/k;->r()Z

    move-result v2

    if-eqz v2, :cond_13

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->g:Lcom/google/android/youtube/core/client/v;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->T()Lcom/google/android/youtube/core/async/a;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/app/YouTubeApplication;->b:Lcom/google/android/youtube/app/k;

    invoke-virtual {v4}, Lcom/google/android/youtube/app/k;->D()Lcom/google/android/youtube/core/async/GDataRequest$Version;

    move-result-object v4

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v2, v0, v3, v4, v1}, Lcom/google/android/youtube/core/client/v;->a(Lcom/google/android/youtube/core/async/DeviceAuthorizer;Lcom/google/android/youtube/core/async/a;Lcom/google/android/youtube/core/async/GDataRequest$Version;Lcom/google/android/youtube/core/async/UserAuthorizer;)V

    :goto_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->g:Lcom/google/android/youtube/core/client/v;

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/client/bc;)V

    new-instance v2, Lcom/google/android/youtube/core/async/UserDelegator;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/YouTubeApplication;->g:Lcom/google/android/youtube/core/client/v;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/app/YouTubeApplication;->m:Lcom/google/android/youtube/core/client/ai;

    move-object/from16 v0, v20

    invoke-direct {v2, v3, v4, v0}, Lcom/google/android/youtube/core/async/UserDelegator;-><init>(Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/be;Landroid/content/SharedPreferences;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->f:Lcom/google/android/youtube/core/async/UserDelegator;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->T()Lcom/google/android/youtube/core/async/a;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/YouTubeApplication;->f:Lcom/google/android/youtube/core/async/UserDelegator;

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/async/a;->a(Lcom/google/android/youtube/core/async/UserDelegator;)V

    new-instance v2, Lcom/google/android/youtube/core/client/am;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Ljava/util/concurrent/Executor;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getCacheDir()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->L()Lcom/google/android/youtube/core/utils/ag;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->P()Lorg/apache/http/client/HttpClient;

    move-result-object v6

    const-string v7, "AIzaSyA8eiZmM1FaDVjRy-df2KTyQ_vz_yYM39w"

    invoke-direct/range {v2 .. v7}, Lcom/google/android/youtube/core/client/am;-><init>(Ljava/util/concurrent/Executor;Ljava/lang/String;Lcom/google/android/youtube/core/utils/e;Lorg/apache/http/client/HttpClient;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->h:Lcom/google/android/youtube/core/client/am;

    new-instance v2, Lcom/google/android/youtube/core/client/l;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Ljava/util/concurrent/Executor;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->L()Lcom/google/android/youtube/core/utils/ag;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->Q()Lorg/apache/http/client/HttpClient;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/youtube/app/YouTubeApplication;->g:Lcom/google/android/youtube/core/client/v;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->R()Lcom/google/android/youtube/core/converter/m;

    move-result-object v9

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->h()Lcom/google/android/youtube/core/player/ak;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/youtube/app/YouTubeApplication;->A:Ljava/util/concurrent/atomic/AtomicReference;

    const-string v12, "android"

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->X()Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    invoke-static/range {p0 .. p0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->m()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/YouTubeApplication;->b:Lcom/google/android/youtube/app/k;

    invoke-virtual {v3}, Lcom/google/android/youtube/app/k;->E()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/YouTubeApplication;->b:Lcom/google/android/youtube/app/k;

    invoke-virtual {v3}, Lcom/google/android/youtube/app/k;->F()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/YouTubeApplication;->b:Lcom/google/android/youtube/app/k;

    invoke-virtual {v3}, Lcom/google/android/youtube/app/k;->J()Z

    move-result v19

    move-object/from16 v3, p0

    move-object/from16 v6, v20

    invoke-direct/range {v2 .. v19}, Lcom/google/android/youtube/core/client/l;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/utils/e;Landroid/content/SharedPreferences;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/converter/m;Lcom/google/android/youtube/core/player/ak;Ljava/util/concurrent/atomic/AtomicReference;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->i:Lcom/google/android/youtube/core/client/l;

    new-instance v2, Lcom/google/android/youtube/core/client/k;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Ljava/util/concurrent/Executor;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->Q()Lorg/apache/http/client/HttpClient;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/google/android/youtube/core/client/k;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->j:Lcom/google/android/youtube/core/client/b;

    new-instance v2, Lcom/google/android/youtube/core/client/ap;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Ljava/util/concurrent/Executor;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->P()Lorg/apache/http/client/HttpClient;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->R()Lcom/google/android/youtube/core/converter/m;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getCacheDir()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->L()Lcom/google/android/youtube/core/utils/ag;

    move-result-object v7

    invoke-direct/range {v2 .. v7}, Lcom/google/android/youtube/core/client/ap;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/m;Ljava/lang/String;Lcom/google/android/youtube/core/utils/e;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->k:Lcom/google/android/youtube/core/client/ap;

    new-instance v2, Lcom/google/android/youtube/core/client/ak;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Ljava/util/concurrent/Executor;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getCacheDir()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->L()Lcom/google/android/youtube/core/utils/ag;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->P()Lorg/apache/http/client/HttpClient;

    move-result-object v6

    const-string v7, "AIzaSyA8eiZmM1FaDVjRy-df2KTyQ_vz_yYM39w"

    invoke-static/range {p0 .. p0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    invoke-direct/range {v2 .. v8}, Lcom/google/android/youtube/core/client/ak;-><init>(Ljava/util/concurrent/Executor;Ljava/lang/String;Lcom/google/android/youtube/core/utils/e;Lorg/apache/http/client/HttpClient;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->l:Lcom/google/android/youtube/core/client/ak;

    new-instance v2, Lcom/google/android/youtube/gmsplus1/f;

    invoke-direct {v2}, Lcom/google/android/youtube/gmsplus1/f;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->n:Lcom/google/android/youtube/gmsplus1/f;

    new-instance v12, Lcom/google/android/youtube/core/client/StatParams;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->X()Ljava/lang/String;

    move-result-object v4

    invoke-static/range {p0 .. p0}, Lcom/google/android/youtube/core/utils/j;->a(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_14

    sget-object v2, Lcom/google/android/youtube/core/client/StatParams$Platform;->TABLET:Lcom/google/android/youtube/core/client/StatParams$Platform;

    :goto_6
    sget-object v5, Lcom/google/android/youtube/core/client/StatParams$SoftwareInterface;->ANDROID:Lcom/google/android/youtube/core/client/StatParams$SoftwareInterface;

    invoke-direct {v12, v3, v4, v2, v5}, Lcom/google/android/youtube/core/client/StatParams;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/core/client/StatParams$Platform;Lcom/google/android/youtube/core/client/StatParams$SoftwareInterface;)V

    new-instance v2, Lcom/google/android/youtube/app/h;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Ljava/util/concurrent/Executor;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->P()Lorg/apache/http/client/HttpClient;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->L()Lcom/google/android/youtube/core/utils/ag;

    move-result-object v7

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->S()Landroid/content/SharedPreferences;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/youtube/app/YouTubeApplication;->g:Lcom/google/android/youtube/core/client/v;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/youtube/app/YouTubeApplication;->b:Lcom/google/android/youtube/app/k;

    move-object/from16 v3, p0

    move-object/from16 v4, p0

    move-object/from16 v10, v22

    invoke-direct/range {v2 .. v12}, Lcom/google/android/youtube/app/h;-><init>(Lcom/google/android/youtube/app/YouTubeApplication;Landroid/content/Context;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/utils/e;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/c;Lcom/google/android/youtube/core/client/StatParams;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->q:Lcom/google/android/youtube/core/client/at;

    new-instance v2, Lcom/google/android/youtube/core/async/by;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/app/YouTubeApplication;->g:Lcom/google/android/youtube/core/client/v;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/app/YouTubeApplication;->h:Lcom/google/android/youtube/core/client/am;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/app/YouTubeApplication;->m:Lcom/google/android/youtube/core/client/ai;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->T()Lcom/google/android/youtube/core/async/a;

    move-result-object v7

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->i()Lcom/google/android/youtube/core/Analytics;

    move-result-object v8

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/e;

    move-object/from16 v3, v22

    invoke-direct/range {v2 .. v8}, Lcom/google/android/youtube/core/async/by;-><init>(Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/bh;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/async/a;Lcom/google/android/youtube/core/Analytics;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->e:Lcom/google/android/youtube/core/async/by;

    new-instance v2, Lcom/google/android/youtube/app/remote/bf;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->l()Lcom/google/android/youtube/core/utils/p;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/YouTubeApplication;->b:Lcom/google/android/youtube/app/k;

    invoke-virtual {v3}, Lcom/google/android/youtube/app/k;->o()Z

    move-result v7

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/YouTubeApplication;->b:Lcom/google/android/youtube/app/k;

    invoke-virtual {v3}, Lcom/google/android/youtube/app/k;->q()Z

    move-result v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/youtube/app/YouTubeApplication;->D:Lcom/google/android/youtube/app/a;

    move-object/from16 v3, p0

    move-object/from16 v5, v20

    move-object/from16 v6, v22

    invoke-direct/range {v2 .. v9}, Lcom/google/android/youtube/app/remote/bf;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/utils/p;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/async/UserAuthorizer;ZZLcom/google/android/youtube/app/a;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->o:Lcom/google/android/youtube/app/remote/bf;

    new-instance v2, Lcom/google/android/youtube/app/remote/v;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Ljava/util/concurrent/Executor;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/app/YouTubeApplication;->o:Lcom/google/android/youtube/app/remote/bf;

    move-object/from16 v0, v20

    invoke-direct {v2, v3, v0, v4, v5}, Lcom/google/android/youtube/app/remote/v;-><init>(Ljava/util/concurrent/Executor;Landroid/content/SharedPreferences;Landroid/content/res/Resources;Lcom/google/android/youtube/app/remote/bf;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->p:Lcom/google/android/youtube/app/remote/bq;

    new-instance v2, Lcom/google/android/youtube/app/remote/br;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Ljava/util/concurrent/Executor;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/app/YouTubeApplication;->p:Lcom/google/android/youtube/app/remote/bq;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->l()Lcom/google/android/youtube/core/utils/p;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/app/YouTubeApplication;->b:Lcom/google/android/youtube/app/k;

    invoke-virtual {v6}, Lcom/google/android/youtube/app/k;->n()Z

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/youtube/app/YouTubeApplication;->o:Lcom/google/android/youtube/app/remote/bf;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/youtube/app/YouTubeApplication;->b:Lcom/google/android/youtube/app/k;

    invoke-virtual {v8}, Lcom/google/android/youtube/app/k;->q()Z

    move-result v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/youtube/app/YouTubeApplication;->D:Lcom/google/android/youtube/app/a;

    move-object/from16 v8, v20

    invoke-direct/range {v2 .. v10}, Lcom/google/android/youtube/app/remote/br;-><init>(Ljava/util/concurrent/Executor;Lcom/google/android/youtube/app/remote/bq;Lcom/google/android/youtube/core/utils/p;ZLcom/google/android/youtube/app/remote/bf;Landroid/content/SharedPreferences;ZLcom/google/android/youtube/app/a;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->r:Lcom/google/android/youtube/app/remote/br;

    new-instance v2, Lcom/google/android/youtube/app/remote/AtHomeConnection;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/google/android/youtube/app/remote/AtHomeConnection;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->t:Lcom/google/android/youtube/app/remote/AtHomeConnection;

    new-instance v2, Lcom/google/android/youtube/app/remote/e;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/YouTubeApplication;->t:Lcom/google/android/youtube/app/remote/AtHomeConnection;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Lcom/google/android/youtube/app/remote/e;-><init>(Landroid/content/Context;Lcom/google/android/youtube/app/remote/AtHomeConnection;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->s:Lcom/google/android/youtube/app/remote/e;

    new-instance v2, Landroid/provider/SearchRecentSuggestions;

    const-string v3, "com.google.android.youtube.SuggestionProvider"

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3, v4}, Landroid/provider/SearchRecentSuggestions;-><init>(Landroid/content/Context;Ljava/lang/String;I)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->c:Landroid/provider/SearchRecentSuggestions;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->ab()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->H()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_15

    invoke-static/range {p0 .. p0}, Lcom/google/android/youtube/app/autosync/a;->a(Lcom/google/android/youtube/app/YouTubeApplication;)V

    :cond_8
    :goto_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->b:Lcom/google/android/youtube/app/k;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/k;->m()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-static/range {p0 .. p0}, Lcom/google/android/youtube/app/prefetch/PrefetchService;->a(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-static/range {v20 .. v20}, Lcom/google/android/youtube/core/utils/g;->a(Landroid/content/SharedPreferences;)Ljava/security/Key;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Ljava/util/concurrent/Executor;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->S()Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-static {v3, v2, v4}, Lcom/google/android/youtube/app/player/a/e;->a(Ljava/util/concurrent/Executor;Ljava/security/Key;Landroid/content/SharedPreferences;)Lcom/google/android/youtube/app/player/a/e;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->x:Lcom/google/android/youtube/app/player/a/e;

    :cond_9
    new-instance v2, Lcom/google/android/youtube/app/remote/ad;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/app/YouTubeApplication;->t:Lcom/google/android/youtube/app/remote/AtHomeConnection;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/app/YouTubeApplication;->s:Lcom/google/android/youtube/app/remote/e;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/app/YouTubeApplication;->r:Lcom/google/android/youtube/app/remote/br;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/youtube/app/YouTubeApplication;->o:Lcom/google/android/youtube/app/remote/bf;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->i()Lcom/google/android/youtube/core/Analytics;

    move-result-object v8

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v8}, Lcom/google/android/youtube/app/remote/ad;-><init>(Landroid/content/Context;Lcom/google/android/youtube/app/remote/AtHomeConnection;Lcom/google/android/youtube/app/remote/e;Lcom/google/android/youtube/app/remote/br;Lcom/google/android/youtube/app/remote/bf;Lcom/google/android/youtube/core/Analytics;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->y:Lcom/google/android/youtube/app/remote/ad;

    new-instance v2, Lcom/google/android/youtube/app/remote/as;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/app/YouTubeApplication;->y:Lcom/google/android/youtube/app/remote/ad;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/app/YouTubeApplication;->g:Lcom/google/android/youtube/core/client/v;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/app/YouTubeApplication;->m:Lcom/google/android/youtube/core/client/ai;

    new-instance v8, Lcom/google/android/youtube/app/remote/ar;

    invoke-direct {v8}, Lcom/google/android/youtube/app/remote/ar;-><init>()V

    move-object/from16 v3, p0

    move-object/from16 v7, v22

    invoke-direct/range {v2 .. v8}, Lcom/google/android/youtube/app/remote/as;-><init>(Landroid/content/Context;Lcom/google/android/youtube/app/remote/ad;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/app/remote/aq;)V

    new-instance v2, Lcom/google/android/youtube/app/a/a;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/YouTubeApplication;->v:Lcom/google/android/youtube/app/prefetch/d;

    move-object/from16 v0, v20

    invoke-direct {v2, v0, v3}, Lcom/google/android/youtube/app/a/a;-><init>(Landroid/content/SharedPreferences;Lcom/google/android/youtube/app/prefetch/d;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->w:Lcom/google/android/youtube/app/a/a;

    new-instance v4, Lcom/google/android/youtube/app/a/c;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->L()Lcom/google/android/youtube/core/utils/ag;

    move-result-object v5

    new-instance v6, Ljava/security/SecureRandom;

    invoke-direct {v6}, Ljava/security/SecureRandom;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Ljava/util/concurrent/Executor;

    move-result-object v7

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->P()Lorg/apache/http/client/HttpClient;

    move-result-object v8

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->T()Lcom/google/android/youtube/core/async/a;

    move-result-object v10

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->U()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/youtube/app/YouTubeApplication;->v:Lcom/google/android/youtube/app/prefetch/d;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/youtube/app/YouTubeApplication;->w:Lcom/google/android/youtube/app/a/a;

    move-object/from16 v9, v21

    invoke-direct/range {v4 .. v14}, Lcom/google/android/youtube/app/a/c;-><init>(Lcom/google/android/youtube/core/utils/e;Ljava/util/Random;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/async/DeviceAuthorizer;Lcom/google/android/youtube/core/async/a;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/client/StatParams;Lcom/google/android/youtube/app/prefetch/d;Lcom/google/android/youtube/app/a/a;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/youtube/app/YouTubeApplication;->u:Lcom/google/android/youtube/core/client/bo;

    new-instance v2, Lcom/google/a/a/a;

    const-string v3, "1001680686"

    const-string v4, "8cu4CIrA3gMQrt7R3QM"

    const-string v5, "ConversionPing"

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3, v4, v5}, Lcom/google/a/a/a;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/google/a/a/a;->a()V

    return-void

    :cond_a
    const-string v2, "In"

    goto/16 :goto_0

    :cond_b
    const-string v2, " ShowWatch"

    goto/16 :goto_1

    :cond_c
    const-string v2, " ShowHome"

    goto/16 :goto_2

    :cond_d
    and-int/lit16 v5, v3, 0x2370

    if-nez v5, :cond_e

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " GuideExpanded"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_3

    :cond_e
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " GuideSelected"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    and-int/lit8 v5, v3, 0x20

    if-eqz v5, :cond_f

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " Account"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_f
    and-int/lit8 v5, v3, 0x10

    if-eqz v5, :cond_10

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " Channel"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_10
    and-int/lit16 v5, v3, 0x2000

    if-eqz v5, :cond_11

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " MySubscriptions"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_11
    and-int/lit16 v3, v3, 0x340

    if-eqz v3, :cond_3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Browse"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_3

    :cond_12
    const/4 v6, 0x0

    goto/16 :goto_4

    :cond_13
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->g:Lcom/google/android/youtube/core/client/v;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->T()Lcom/google/android/youtube/core/async/a;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/app/YouTubeApplication;->b:Lcom/google/android/youtube/app/k;

    invoke-virtual {v4}, Lcom/google/android/youtube/app/k;->D()Lcom/google/android/youtube/core/async/GDataRequest$Version;

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v2, v0, v3, v4, v5}, Lcom/google/android/youtube/core/client/v;->a(Lcom/google/android/youtube/core/async/DeviceAuthorizer;Lcom/google/android/youtube/core/async/a;Lcom/google/android/youtube/core/async/GDataRequest$Version;Lcom/google/android/youtube/core/async/UserAuthorizer;)V

    goto/16 :goto_5

    :cond_14
    sget-object v2, Lcom/google/android/youtube/core/client/StatParams$Platform;->MOBILE:Lcom/google/android/youtube/core/client/StatParams$Platform;

    goto/16 :goto_6

    :cond_15
    invoke-static/range {p0 .. p0}, Lcom/google/android/youtube/app/autosync/a;->b(Landroid/content/Context;)V

    goto/16 :goto_7
.end method

.method public final b(I)V
    .locals 4

    const/4 v3, 0x2

    iput p1, p0, Lcom/google/android/youtube/app/YouTubeApplication;->C:I

    const/4 v0, -0x2

    if-ne p1, v0, :cond_0

    const-string v0, "Signed out"

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/youtube/app/YouTubeApplication;->i()Lcom/google/android/youtube/core/Analytics;

    move-result-object v1

    const-string v2, "Engagement"

    invoke-interface {v1, v3, v2, v0, v3}, Lcom/google/android/youtube/core/Analytics;->a(ILjava/lang/String;Ljava/lang/String;I)V

    return-void

    :cond_0
    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    const-string v0, "Signed in"

    goto :goto_0

    :cond_1
    if-nez p1, :cond_2

    const-string v0, "No subscriptions"

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->b:Lcom/google/android/youtube/app/k;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/k;->m()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p0}, Lcom/google/android/youtube/app/prefetch/PrefetchService;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_4

    :cond_3
    const-string v0, "Subscriptions"

    goto :goto_0

    :cond_4
    const-string v0, "Prefetching"

    goto :goto_0
.end method

.method public final bridge synthetic c()Lcom/google/android/youtube/core/c;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->b:Lcom/google/android/youtube/app/k;

    return-object v0
.end method

.method protected final d()Lcom/google/android/youtube/core/async/a;
    .locals 4

    const-string v0, " "

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "https://www.googleapis.com/auth/youtube"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "https://www.googleapis.com/auth/pos"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "https://www.googleapis.com/auth/plus.me"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/gmsplus1/a;

    invoke-static {v0}, Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;->createOAuth(Ljava/lang/String;)Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/YouTubeApplication;->M()Ljava/util/concurrent/Executor;

    move-result-object v2

    invoke-direct {v1, p0, v0, v2}, Lcom/google/android/youtube/gmsplus1/a;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;Ljava/util/concurrent/Executor;)V

    return-object v1
.end method

.method protected final d_()Z
    .locals 7

    const/4 v2, 0x0

    const/4 v1, 0x1

    const-string v0, "activity"

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/YouTubeApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v4

    new-instance v3, Landroid/content/Intent;

    const-string v5, "com.google.android.youtube.api.service.START"

    invoke-direct {v3, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v5

    iget-object v5, v5, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {v5, v3, v2}, Landroid/content/pm/PackageManager;->resolveService(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v3

    iget-object v5, v3, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    if-eqz v5, :cond_0

    iget-object v3, v3, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v3, v3, Landroid/content/pm/ServiceInfo;->processName:Ljava/lang/String;

    :goto_0
    if-nez v3, :cond_1

    move v0, v1

    :goto_1
    return v0

    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v5

    iget-object v5, v5, Landroid/content/pm/ApplicationInfo;->processName:Ljava/lang/String;

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    iget v6, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    if-ne v6, v4, :cond_4

    iget-object v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_1

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method public final e()Lcom/google/android/youtube/core/client/b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->j:Lcom/google/android/youtube/core/client/b;

    return-object v0
.end method

.method public final e_()Lcom/google/android/youtube/core/client/be;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->m:Lcom/google/android/youtube/core/client/ai;

    return-object v0
.end method

.method public final f()Lcom/google/android/youtube/core/client/bj;
    .locals 4

    new-instance v0, Lcom/google/android/youtube/core/client/an;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/YouTubeApplication;->L()Lcom/google/android/youtube/core/utils/ag;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Ljava/util/concurrent/Executor;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/youtube/app/YouTubeApplication;->P()Lorg/apache/http/client/HttpClient;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/youtube/core/client/an;-><init>(Lcom/google/android/youtube/core/utils/e;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;)V

    return-object v0
.end method

.method public final g()Lcom/google/android/youtube/core/client/bl;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->k:Lcom/google/android/youtube/core/client/ap;

    return-object v0
.end method

.method protected final k()Lcom/google/android/youtube/core/player/ak;
    .locals 4

    new-instance v0, Lcom/google/android/youtube/app/prefetch/d;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/YouTubeApplication;->S()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/youtube/app/YouTubeApplication;->L()Lcom/google/android/youtube/core/utils/ag;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/app/YouTubeApplication;->b:Lcom/google/android/youtube/app/k;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/android/youtube/app/prefetch/d;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/utils/e;Lcom/google/android/youtube/app/k;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->v:Lcom/google/android/youtube/app/prefetch/d;

    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->v:Lcom/google/android/youtube/app/prefetch/d;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/prefetch/d;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->v:Lcom/google/android/youtube/app/prefetch/d;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/prefetch/d;->d()V

    invoke-super {p0}, Lcom/google/android/youtube/core/BaseApplication;->k()Lcom/google/android/youtube/core/player/ak;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/app/prefetch/j;

    iget-object v2, p0, Lcom/google/android/youtube/app/YouTubeApplication;->v:Lcom/google/android/youtube/app/prefetch/d;

    invoke-direct {v1, v2, v0}, Lcom/google/android/youtube/app/prefetch/j;-><init>(Lcom/google/android/youtube/app/prefetch/d;Lcom/google/android/youtube/core/player/ak;)V

    return-object v1
.end method

.method public final m()Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "youtube_client_id"

    const-string v2, "android-google"

    invoke-static {v0, v1, v2}, Lcom/google/android/gsf/e;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final n()Lcom/google/android/youtube/app/k;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->b:Lcom/google/android/youtube/app/k;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    const-string v0, "YouTube"

    return-object v0
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 1

    const-string v0, "prefetch_subscriptions"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "prefetch_watch_later"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-static {p0}, Lcom/google/android/youtube/app/prefetch/PrefetchService;->c(Landroid/content/Context;)V

    :cond_1
    return-void
.end method

.method public final p()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/youtube/app/YouTubeApplication;->S()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "session_watch_count"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    invoke-static {v0}, Lcom/google/android/youtube/app/compat/ac;->a(Landroid/content/SharedPreferences;)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v0

    const-string v2, "session_watch_count"

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/google/android/youtube/app/compat/ad;->a(Ljava/lang/String;I)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/compat/ad;->a()V

    return-void
.end method

.method public final q()Landroid/provider/SearchRecentSuggestions;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->c:Landroid/provider/SearchRecentSuggestions;

    return-object v0
.end method

.method public final r()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->d:Lcom/google/android/youtube/core/suggest/a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/youtube/core/suggest/a;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/core/suggest/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->d:Lcom/google/android/youtube/core/suggest/a;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->d:Lcom/google/android/youtube/core/suggest/a;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/suggest/a;->b()V

    return-void
.end method

.method public final s()Lcom/google/android/youtube/core/async/by;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->e:Lcom/google/android/youtube/core/async/by;

    return-object v0
.end method

.method public final t()Lcom/google/android/youtube/core/async/UserDelegator;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->f:Lcom/google/android/youtube/core/async/UserDelegator;

    return-object v0
.end method

.method public final u()Lcom/google/android/youtube/core/client/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->i:Lcom/google/android/youtube/core/client/l;

    return-object v0
.end method

.method public final v()Lcom/google/android/youtube/core/client/bo;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->u:Lcom/google/android/youtube/core/client/bo;

    return-object v0
.end method

.method public final w()Lcom/google/android/youtube/core/client/bg;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->l:Lcom/google/android/youtube/core/client/ak;

    return-object v0
.end method

.method public final x()Lcom/google/android/youtube/gmsplus1/f;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->n:Lcom/google/android/youtube/gmsplus1/f;

    return-object v0
.end method

.method public final y()Lcom/google/android/youtube/core/client/at;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->q:Lcom/google/android/youtube/core/client/at;

    return-object v0
.end method

.method public final z()Lcom/google/android/youtube/app/remote/bf;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->o:Lcom/google/android/youtube/app/remote/bf;

    return-object v0
.end method
