.class public final Lcom/google/android/youtube/app/a/c;
.super Lcom/google/android/youtube/core/client/i;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/youtube/app/a/a;

.field private final b:Lcom/google/android/youtube/app/prefetch/d;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/utils/e;Ljava/util/Random;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/async/DeviceAuthorizer;Lcom/google/android/youtube/core/async/a;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/client/StatParams;Lcom/google/android/youtube/app/prefetch/d;Lcom/google/android/youtube/app/a/a;)V
    .locals 1

    invoke-direct/range {p0 .. p8}, Lcom/google/android/youtube/core/client/i;-><init>(Lcom/google/android/youtube/core/utils/e;Ljava/util/Random;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/async/DeviceAuthorizer;Lcom/google/android/youtube/core/async/a;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/client/StatParams;)V

    const-string v0, "prefetchStore cannot be null"

    invoke-static {p9, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/prefetch/d;

    iput-object v0, p0, Lcom/google/android/youtube/app/a/c;->b:Lcom/google/android/youtube/app/prefetch/d;

    const-string v0, "prefetchPlaybacksTracker cannot be null"

    invoke-static {p10, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/a/a;

    iput-object v0, p0, Lcom/google/android/youtube/app/a/c;->a:Lcom/google/android/youtube/app/a/a;

    return-void
.end method


# virtual methods
.method protected final a(Ljava/lang/String;)Ljava/util/List;
    .locals 4

    invoke-super {p0, p1}, Lcom/google/android/youtube/core/client/i;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/youtube/app/a/c;->a:Lcom/google/android/youtube/app/a/a;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/a/a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/a/c;->b:Lcom/google/android/youtube/app/prefetch/d;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/prefetch/d;->b(Ljava/lang/String;)Lcom/google/android/youtube/core/transfer/Transfer$Status;

    move-result-object v0

    sget-object v2, Lcom/google/android/youtube/core/transfer/Transfer$Status;->COMPLETED:Lcom/google/android/youtube/core/transfer/Transfer$Status;

    if-ne v0, v2, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/app/a/c;->a:Lcom/google/android/youtube/app/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/a/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "2"

    :goto_0
    iget-object v2, p0, Lcom/google/android/youtube/app/a/c;->a:Lcom/google/android/youtube/app/a/a;

    invoke-virtual {v2, p1}, Lcom/google/android/youtube/app/a/a;->b(Ljava/lang/String;)V

    :goto_1
    new-instance v2, Landroid/util/Pair;

    const-string v3, "preloaded"

    invoke-direct {v2, v3, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object v1

    :cond_1
    const-string v0, "1"

    goto :goto_0

    :cond_2
    const-string v0, "0"

    goto :goto_1
.end method
