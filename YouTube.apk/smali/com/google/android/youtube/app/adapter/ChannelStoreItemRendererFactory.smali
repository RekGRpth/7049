.class public final Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;
.super Lcom/google/android/youtube/app/adapter/j;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/bk;
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field private final a:Landroid/os/Handler;

.field private final b:Ljava/util/WeakHashMap;

.field private final c:Landroid/content/Context;

.field private final d:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private final e:Lcom/google/android/youtube/app/YouTubeApplication;

.field private final f:Lcom/google/android/youtube/core/Analytics;

.field private final g:Lcom/google/android/youtube/core/client/bc;

.field private final h:Lcom/google/android/youtube/app/d;

.field private final i:Lcom/google/android/youtube/core/e;

.field private final j:Lcom/google/android/youtube/app/adapter/u;

.field private final k:Ljava/util/Map;

.field private final l:Ljava/util/Map;

.field private final m:Ljava/util/Map;

.field private final n:Lcom/google/android/youtube/app/ui/cw;

.field private final o:Lcom/google/android/youtube/core/async/h;

.field private final p:Landroid/app/Activity;

.field private q:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/app/YouTubeApplication;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/app/ui/cw;Landroid/app/Activity;)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/j;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->c:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->f:Lcom/google/android/youtube/core/Analytics;

    iput-object p3, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->g:Lcom/google/android/youtube/core/client/bc;

    iput-object p7, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->i:Lcom/google/android/youtube/core/e;

    iput-object p4, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->d:Lcom/google/android/youtube/core/async/UserAuthorizer;

    iput-object p5, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->e:Lcom/google/android/youtube/app/YouTubeApplication;

    iput-object p6, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->h:Lcom/google/android/youtube/app/d;

    new-instance v0, Lcom/google/android/youtube/app/adapter/u;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/adapter/u;-><init>(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;B)V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->j:Lcom/google/android/youtube/app/adapter/u;

    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->b:Ljava/util/WeakHashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->k:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->l:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->m:Ljava/util/Map;

    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->a:Landroid/os/Handler;

    iput-object p8, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->n:Lcom/google/android/youtube/app/ui/cw;

    invoke-static {p9, p0}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->o:Lcom/google/android/youtube/core/async/h;

    iput-object p9, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->p:Landroid/app/Activity;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)Lcom/google/android/youtube/app/adapter/u;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->j:Lcom/google/android/youtube/app/adapter/u;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;Lcom/google/android/youtube/app/adapter/z;Lcom/google/android/youtube/core/model/UserProfile;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->k:Ljava/util/Map;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/UserProfile;->uri:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->WORKING:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->n:Lcom/google/android/youtube/app/ui/cw;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/UserProfile;->uri:Landroid/net/Uri;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/ui/cw;->b(Landroid/net/Uri;)V

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->a(Lcom/google/android/youtube/app/adapter/z;Lcom/google/android/youtube/core/model/UserProfile;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;Lcom/google/android/youtube/app/adapter/z;Lcom/google/android/youtube/core/model/UserProfile;Landroid/net/Uri;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->k:Ljava/util/Map;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/UserProfile;->uri:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->SUBSCRIBED:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->l:Ljava/util/Map;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/UserProfile;->selfUri:Landroid/net/Uri;

    invoke-interface {v0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->a(Lcom/google/android/youtube/app/adapter/z;Lcom/google/android/youtube/core/model/UserProfile;)V

    return-void
.end method

.method private a(Lcom/google/android/youtube/app/adapter/z;Lcom/google/android/youtube/core/model/UserProfile;)V
    .locals 1

    invoke-static {p1}, Lcom/google/android/youtube/app/adapter/z;->b(Lcom/google/android/youtube/app/adapter/z;)Lcom/google/android/youtube/core/model/UserProfile;

    move-result-object v0

    if-ne v0, p2, :cond_0

    invoke-static {p1}, Lcom/google/android/youtube/app/adapter/z;->a(Lcom/google/android/youtube/app/adapter/z;)Landroid/view/View;

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->c()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;Z)Z
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->q:Z

    return v0
.end method

.method private b(Lcom/google/android/youtube/core/model/Subscription;)Landroid/net/Uri;
    .locals 1

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Subscription;->channelUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->c(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->k:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;Lcom/google/android/youtube/app/adapter/z;Lcom/google/android/youtube/core/model/UserProfile;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->k:Ljava/util/Map;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/UserProfile;->uri:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->UNSUBSCRIBED:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->l:Ljava/util/Map;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/UserProfile;->selfUri:Landroid/net/Uri;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->a(Lcom/google/android/youtube/app/adapter/z;Lcom/google/android/youtube/core/model/UserProfile;)V

    return-void
.end method

.method private static c(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "https"

    const-string v2, "http"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/z;

    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/z;->a(Lcom/google/android/youtube/app/adapter/z;)Landroid/view/View;

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->q:Z

    return v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)Lcom/google/android/youtube/core/async/UserAuthorizer;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->d:Lcom/google/android/youtube/core/async/UserAuthorizer;

    return-object v0
.end method

.method private d()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/z;

    iget-object v2, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->k:Ljava/util/Map;

    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/z;->b(Lcom/google/android/youtube/app/adapter/z;)Lcom/google/android/youtube/core/model/UserProfile;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/youtube/core/model/UserProfile;->uri:Landroid/net/Uri;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    sget-object v3, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->WORKING:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    if-eq v2, v3, :cond_0

    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/z;->b(Lcom/google/android/youtube/app/adapter/z;)Lcom/google/android/youtube/core/model/UserProfile;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->m:Ljava/util/Map;

    iget-object v4, v2, Lcom/google/android/youtube/core/model/UserProfile;->uri:Landroid/net/Uri;

    invoke-static {v4}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->c(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->k:Ljava/util/Map;

    iget-object v2, v2, Lcom/google/android/youtube/core/model/UserProfile;->uri:Landroid/net/Uri;

    sget-object v4, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->SUBSCRIBED:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    invoke-interface {v3, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/z;->a(Lcom/google/android/youtube/app/adapter/z;)Landroid/view/View;

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->k:Ljava/util/Map;

    iget-object v2, v2, Lcom/google/android/youtube/core/model/UserProfile;->uri:Landroid/net/Uri;

    sget-object v4, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->UNSUBSCRIBED:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    invoke-interface {v3, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->q:Z

    return-void
.end method

.method private d(Landroid/net/Uri;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/z;

    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/z;->b(Lcom/google/android/youtube/app/adapter/z;)Lcom/google/android/youtube/core/model/UserProfile;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/youtube/core/model/UserProfile;->uri:Landroid/net/Uri;

    invoke-virtual {v2, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/z;->a(Lcom/google/android/youtube/app/adapter/z;)Landroid/view/View;

    goto :goto_0

    :cond_1
    return-void
.end method

.method static synthetic e(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->c:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)Lcom/google/android/youtube/app/YouTubeApplication;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->e:Lcom/google/android/youtube/app/YouTubeApplication;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)Lcom/google/android/youtube/core/Analytics;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->f:Lcom/google/android/youtube/core/Analytics;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->p:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)Lcom/google/android/youtube/core/client/bc;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->g:Lcom/google/android/youtube/core/client/bc;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->a:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->l:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic l(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)Lcom/google/android/youtube/app/ui/cw;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->n:Lcom/google/android/youtube/app/ui/cw;

    return-object v0
.end method

.method static synthetic m(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)Lcom/google/android/youtube/core/e;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->i:Lcom/google/android/youtube/core/e;

    return-object v0
.end method

.method static synthetic n(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)Lcom/google/android/youtube/app/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->h:Lcom/google/android/youtube/app/d;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/youtube/app/adapter/bl;
    .locals 3

    new-instance v0, Lcom/google/android/youtube/app/adapter/z;

    invoke-direct {v0, p0, p1, p2, p0}, Lcom/google/android/youtube/app/adapter/z;-><init>(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;Landroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->b:Ljava/util/WeakHashMap;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-boolean v1, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->q:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->q:Z

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->d:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v1, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bk;)V

    :cond_0
    return-object v0
.end method

.method public final a()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/z;

    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/z;->b(Lcom/google/android/youtube/app/adapter/z;)Lcom/google/android/youtube/core/model/UserProfile;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/youtube/core/model/UserProfile;->uri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->k:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    sget-object v3, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->WORKING:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->k:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->c()V

    return-void
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->m:Ljava/util/Map;

    invoke-static {p1}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->c(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->k:Ljava/util/Map;

    sget-object v1, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->UNSUBSCRIBED:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->d(Landroid/net/Uri;)V

    return-void
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 2

    invoke-static {p2}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->valueOf(Ljava/lang/String;)Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->k:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->d(Landroid/net/Uri;)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/adapter/z;Lcom/google/android/youtube/core/model/UserProfile;Lcom/google/android/youtube/core/model/UserProfile;Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->g:Lcom/google/android/youtube/core/client/bc;

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->a:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/youtube/app/adapter/w;

    invoke-direct {v2, p0, p1, p2, p3}, Lcom/google/android/youtube/app/adapter/w;-><init>(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;Lcom/google/android/youtube/app/adapter/z;Lcom/google/android/youtube/core/model/UserProfile;Lcom/google/android/youtube/core/model/UserProfile;)V

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/async/am;->a(Landroid/os/Handler;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/am;

    move-result-object v1

    invoke-interface {v0, p4, p5, v1}, Lcom/google/android/youtube/core/client/bc;->g(Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Subscription;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->m:Ljava/util/Map;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->b(Lcom/google/android/youtube/core/model/Subscription;)Landroid/net/Uri;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->k:Ljava/util/Map;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->b(Lcom/google/android/youtube/core/model/Subscription;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->SUBSCRIBED:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->b(Lcom/google/android/youtube/core/model/Subscription;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->d(Landroid/net/Uri;)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 7

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->g:Lcom/google/android/youtube/core/client/bc;

    invoke-interface {v0}, Lcom/google/android/youtube/core/client/bc;->y()Lcom/google/android/youtube/core/async/au;

    move-result-object v2

    sget-object v6, Lcom/google/android/youtube/core/async/GDataRequestFactory;->i:Landroid/net/Uri;

    new-instance v0, Lcom/google/android/youtube/app/a/b;

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->g:Lcom/google/android/youtube/core/client/bc;

    iget-object v3, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->p:Landroid/app/Activity;

    iget-object v4, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->o:Lcom/google/android/youtube/core/async/h;

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/a/b;-><init>(Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/async/au;Landroid/app/Activity;Lcom/google/android/youtube/core/async/h;Lcom/google/android/youtube/core/model/UserAuth;)V

    invoke-virtual {v0, v6}, Lcom/google/android/youtube/app/a/b;->a(Landroid/net/Uri;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->d()V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    check-cast p2, Lcom/google/android/youtube/core/model/Page;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p2, Lcom/google/android/youtube/core/model/Page;->entries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Subscription;

    iget-object v2, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->m:Ljava/util/Map;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->b(Lcom/google/android/youtube/core/model/Subscription;)Landroid/net/Uri;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->d()V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->q:Z

    return-void
.end method

.method public final b(Landroid/net/Uri;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->k:Ljava/util/Map;

    sget-object v1, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->WORKING:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->d(Landroid/net/Uri;)V

    return-void
.end method

.method public final g_()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->q:Z

    return-void
.end method
