.class public final Lcom/google/android/youtube/app/adapter/a;
.super Lcom/google/android/youtube/app/adapter/j;
.source "SourceFile"


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/google/android/youtube/app/adapter/bv;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/client/be;)V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/j;-><init>()V

    const-string v0, "activity cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/a;->a:Landroid/app/Activity;

    const-string v0, "imageClient cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/youtube/app/adapter/b;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/adapter/b;-><init>(Lcom/google/android/youtube/app/adapter/a;)V

    sget-object v1, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;->SMALL:Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;

    const/4 v2, 0x0

    invoke-static {p1, p2, v1, v2}, Lcom/google/android/youtube/app/adapter/cm;->a(Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;Z)Lcom/google/android/youtube/app/adapter/cm;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/app/adapter/af;

    invoke-direct {v2}, Lcom/google/android/youtube/app/adapter/af;-><init>()V

    invoke-virtual {v2, v0}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/a;->b:Lcom/google/android/youtube/app/adapter/bv;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/adapter/a;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/a;->a:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/adapter/a;)Lcom/google/android/youtube/app/adapter/bv;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/a;->b:Lcom/google/android/youtube/app/adapter/bv;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/youtube/app/adapter/bl;
    .locals 1

    new-instance v0, Lcom/google/android/youtube/app/adapter/c;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/youtube/app/adapter/c;-><init>(Lcom/google/android/youtube/app/adapter/a;Landroid/view/View;Landroid/view/ViewGroup;)V

    return-object v0
.end method
