.class final Lcom/google/android/youtube/app/adapter/ag;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/adapter/bl;


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/view/View;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/app/adapter/ag;->a:Landroid/view/View;

    iput-object p2, p0, Lcom/google/android/youtube/app/adapter/ag;->b:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/Object;)Landroid/view/View;
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ag;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/bl;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/app/adapter/bl;->a(ILjava/lang/Object;)Landroid/view/View;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ag;->a:Landroid/view/View;

    return-object v0
.end method
