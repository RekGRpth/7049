.class public final Lcom/google/android/youtube/app/adapter/ak;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/adapter/bl;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/adapter/aj;

.field private final b:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/adapter/aj;Landroid/view/View;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/youtube/app/adapter/ak;->a:Lcom/google/android/youtube/app/adapter/aj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x7f070165

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    const v0, 0x7f070038

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    :cond_0
    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/ak;->b:Landroid/view/View;

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(ILjava/lang/Object;)Landroid/view/View;
    .locals 1

    check-cast p2, Lcom/google/android/youtube/core/model/Video;

    invoke-virtual {p0, p2}, Lcom/google/android/youtube/app/adapter/ak;->a(Lcom/google/android/youtube/core/model/Video;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/youtube/core/model/Video;)Landroid/view/View;
    .locals 6

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ak;->a:Lcom/google/android/youtube/app/adapter/aj;

    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/aj;->a(Lcom/google/android/youtube/app/adapter/aj;)Lcom/google/android/youtube/app/ui/v;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/ak;->b:Landroid/view/View;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/youtube/app/ui/v;->a(Landroid/view/View;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ak;->b:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setClickable(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ak;->b:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ak;->b:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/ak;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b026e

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p1, Lcom/google/android/youtube/core/model/Video;->title:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ak;->b:Landroid/view/View;

    return-object v0
.end method

.method public final a(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ak;->b:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method
