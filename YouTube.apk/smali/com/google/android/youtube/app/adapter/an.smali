.class public final Lcom/google/android/youtube/app/adapter/an;
.super Lcom/google/android/youtube/app/adapter/j;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:Lcom/google/android/youtube/app/adapter/bv;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Lcom/google/android/youtube/app/adapter/bv;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/j;-><init>()V

    const-string v0, "res cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/an;->a:Landroid/content/res/Resources;

    const-string v0, "videoRendererFactory cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/bv;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/an;->b:Lcom/google/android/youtube/app/adapter/bv;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/adapter/an;)Lcom/google/android/youtube/app/adapter/bv;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/an;->b:Lcom/google/android/youtube/app/adapter/bv;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/adapter/an;)Landroid/content/res/Resources;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/an;->a:Landroid/content/res/Resources;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/youtube/app/adapter/bl;
    .locals 2

    new-instance v0, Lcom/google/android/youtube/app/adapter/ap;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/google/android/youtube/app/adapter/ap;-><init>(Lcom/google/android/youtube/app/adapter/an;Landroid/view/View;Landroid/view/ViewGroup;B)V

    return-object v0
.end method

.method public final a(Ljava/lang/Iterable;)V
    .locals 3

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Event;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Event;->targetVideo:Lcom/google/android/youtube/core/model/Video;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/an;->b:Lcom/google/android/youtube/app/adapter/bv;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/adapter/bv;->a(Ljava/lang/Iterable;)V

    return-void
.end method
