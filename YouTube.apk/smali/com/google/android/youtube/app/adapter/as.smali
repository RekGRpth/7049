.class final Lcom/google/android/youtube/app/adapter/as;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/adapter/bl;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/adapter/ar;

.field private final b:Landroid/view/View;

.field private final c:Landroid/widget/TextView;

.field private final d:Lcom/google/android/youtube/app/adapter/bl;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/adapter/ar;Landroid/view/View;Landroid/view/ViewGroup;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/youtube/app/adapter/as;->a:Lcom/google/android/youtube/app/adapter/ar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x7f070149

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/as;->b:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/as;->b:Landroid/view/View;

    const v1, 0x7f070058

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/as;->c:Landroid/widget/TextView;

    invoke-static {p1}, Lcom/google/android/youtube/app/adapter/ar;->a(Lcom/google/android/youtube/app/adapter/ar;)Lcom/google/android/youtube/app/adapter/bv;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/as;->b:Landroid/view/View;

    invoke-interface {v0, v1, p3}, Lcom/google/android/youtube/app/adapter/bv;->a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/youtube/app/adapter/bl;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/as;->d:Lcom/google/android/youtube/app/adapter/bl;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/app/adapter/ar;Landroid/view/View;Landroid/view/ViewGroup;B)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/app/adapter/as;-><init>(Lcom/google/android/youtube/app/adapter/ar;Landroid/view/View;Landroid/view/ViewGroup;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(ILjava/lang/Object;)Landroid/view/View;
    .locals 3

    check-cast p2, Lcom/google/android/youtube/core/model/LiveEvent;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/as;->d:Lcom/google/android/youtube/app/adapter/bl;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/LiveEvent;->video:Lcom/google/android/youtube/core/model/Video;

    invoke-interface {v0, p1, v1}, Lcom/google/android/youtube/app/adapter/bl;->a(ILjava/lang/Object;)Landroid/view/View;

    iget-object v0, p2, Lcom/google/android/youtube/core/model/LiveEvent;->video:Lcom/google/android/youtube/core/model/Video;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/Video;->isLive()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/as;->c:Landroid/widget/TextView;

    const v1, 0x7f0b022a

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/as;->c:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/as;->a:Lcom/google/android/youtube/app/adapter/ar;

    invoke-static {v1}, Lcom/google/android/youtube/app/adapter/ar;->b(Lcom/google/android/youtube/app/adapter/ar;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d0043

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/as;->b:Landroid/view/View;

    return-object v0
.end method
