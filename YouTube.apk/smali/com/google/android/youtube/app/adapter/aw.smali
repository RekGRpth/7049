.class public final Lcom/google/android/youtube/app/adapter/aw;
.super Lcom/google/android/youtube/core/a/e;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field final a:Landroid/os/Handler;

.field private final c:Ljava/util/ArrayList;

.field private final d:Landroid/app/Activity;

.field private final e:Lcom/google/android/youtube/core/async/au;

.field private final f:Lcom/google/android/youtube/core/e;

.field private final g:Lcom/google/android/youtube/core/async/h;

.field private final h:I

.field private final i:Lcom/google/android/youtube/app/adapter/bv;

.field private final j:Ljava/util/LinkedList;

.field private k:Lcom/google/android/youtube/core/async/GDataRequest;

.field private l:Lcom/google/android/youtube/app/adapter/ay;

.field private final m:I

.field private final n:I

.field private final o:I

.field private p:I

.field private q:Z

.field private r:I

.field private final s:Lcom/google/android/youtube/core/a/g;

.field private t:Landroid/net/Uri;

.field private u:I

.field private v:Lcom/google/android/youtube/core/utils/t;


# direct methods
.method public constructor <init>(Landroid/app/Activity;ILcom/google/android/youtube/app/adapter/bv;Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/e;IIILcom/google/android/youtube/core/a/g;)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/youtube/core/a/e;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/aw;->c:Ljava/util/ArrayList;

    const-string v0, "activity cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/aw;->d:Landroid/app/Activity;

    new-instance v0, Lcom/google/android/youtube/app/adapter/ax;

    invoke-virtual {p1}, Landroid/app/Activity;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/adapter/ax;-><init>(Lcom/google/android/youtube/app/adapter/aw;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/aw;->a:Landroid/os/Handler;

    const-string v0, "requester cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/au;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/aw;->e:Lcom/google/android/youtube/core/async/au;

    const-string v0, "errorHelper cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/e;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/aw;->f:Lcom/google/android/youtube/core/e;

    invoke-static {p1, p0}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/aw;->g:Lcom/google/android/youtube/core/async/h;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/aw;->j:Ljava/util/LinkedList;

    iput p2, p0, Lcom/google/android/youtube/app/adapter/aw;->h:I

    const-string v0, "rendererFactory cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/bv;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/aw;->i:Lcom/google/android/youtube/app/adapter/bv;

    iput p6, p0, Lcom/google/android/youtube/app/adapter/aw;->m:I

    iput p7, p0, Lcom/google/android/youtube/app/adapter/aw;->n:I

    iput p8, p0, Lcom/google/android/youtube/app/adapter/aw;->o:I

    iput-object p9, p0, Lcom/google/android/youtube/app/adapter/aw;->s:Lcom/google/android/youtube/core/a/g;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/adapter/aw;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/aw;->o()V

    return-void
.end method

.method private a(Lcom/google/android/youtube/core/async/GDataRequest;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/youtube/app/adapter/aw;->k:Lcom/google/android/youtube/core/async/GDataRequest;

    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/aw;->f()V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/aw;->e:Lcom/google/android/youtube/core/async/au;

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/aw;->g:Lcom/google/android/youtube/core/async/h;

    invoke-interface {v0, p1, v1}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/aw;->l:Lcom/google/android/youtube/app/adapter/ay;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/aw;->l:Lcom/google/android/youtube/app/adapter/ay;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/app/adapter/ay;->a(Ljava/lang/String;Z)V

    :cond_0
    return-void
.end method

.method private f()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/aw;->l:Lcom/google/android/youtube/app/adapter/ay;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/aw;->l:Lcom/google/android/youtube/app/adapter/ay;

    invoke-interface {v0}, Lcom/google/android/youtube/app/adapter/ay;->c()V

    :cond_0
    return-void
.end method

.method private g()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/aw;->l:Lcom/google/android/youtube/app/adapter/ay;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/aw;->l:Lcom/google/android/youtube/app/adapter/ay;

    invoke-interface {v0}, Lcom/google/android/youtube/app/adapter/ay;->d()V

    :cond_0
    return-void
.end method

.method private h()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/aw;->l:Lcom/google/android/youtube/app/adapter/ay;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/aw;->l:Lcom/google/android/youtube/app/adapter/ay;

    invoke-interface {v0}, Lcom/google/android/youtube/app/adapter/ay;->f()V

    :cond_0
    return-void
.end method

.method private i()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/adapter/aw;->q:Z

    iput-object v1, p0, Lcom/google/android/youtube/app/adapter/aw;->t:Landroid/net/Uri;

    iput-object v1, p0, Lcom/google/android/youtube/app/adapter/aw;->k:Lcom/google/android/youtube/core/async/GDataRequest;

    iput v0, p0, Lcom/google/android/youtube/app/adapter/aw;->u:I

    iget v0, p0, Lcom/google/android/youtube/app/adapter/aw;->o:I

    iput v0, p0, Lcom/google/android/youtube/app/adapter/aw;->p:I

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/aw;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/aw;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/aw;->l:Lcom/google/android/youtube/app/adapter/ay;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/aw;->l:Lcom/google/android/youtube/app/adapter/ay;

    invoke-interface {v0}, Lcom/google/android/youtube/app/adapter/ay;->u_()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/app/adapter/aw;->k()V

    return-void
.end method

.method private j()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/aw;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/aw;->h()V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/aw;->t:Landroid/net/Uri;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/youtube/app/adapter/aw;->u:I

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/aw;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/GDataRequest;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/adapter/aw;->a(Lcom/google/android/youtube/core/async/GDataRequest;)V

    goto :goto_0
.end method

.method private o()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/aw;->t:Landroid/net/Uri;

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/aw;->j()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/aw;->k:Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v0, v0, Lcom/google/android/youtube/core/async/GDataRequest;->c:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/aw;->t:Landroid/net/Uri;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/aw;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/google/android/youtube/app/adapter/aw;->n:I

    if-ge v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/aw;->t:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/aw;->k:Lcom/google/android/youtube/core/async/GDataRequest;

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;Lcom/google/android/youtube/core/async/GDataRequest;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v0

    iput-object v2, p0, Lcom/google/android/youtube/app/adapter/aw;->t:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/adapter/aw;->a(Lcom/google/android/youtube/core/async/GDataRequest;)V

    goto :goto_0

    :cond_2
    iput-object v2, p0, Lcom/google/android/youtube/app/adapter/aw;->t:Landroid/net/Uri;

    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/aw;->h()V

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 2

    iget v0, p0, Lcom/google/android/youtube/app/adapter/aw;->p:I

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/aw;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/aw;->d:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iget v1, p0, Lcom/google/android/youtube/app/adapter/aw;->h:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/aw;->i:Lcom/google/android/youtube/app/adapter/bv;

    invoke-interface {v0, p2, p3}, Lcom/google/android/youtube/app/adapter/bv;->a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/youtube/app/adapter/bl;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/app/adapter/aw;->b(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/youtube/app/adapter/bl;->a(ILjava/lang/Object;)Landroid/view/View;

    return-object p2

    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/bl;

    goto :goto_0
.end method

.method public final a(I)Lcom/google/android/youtube/core/a/g;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/aw;->s:Lcom/google/android/youtube/core/a/g;

    return-object v0
.end method

.method public final a(ILjava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/aw;->c:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/adapter/aw;->k()V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/adapter/ay;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/adapter/aw;->l:Lcom/google/android/youtube/app/adapter/ay;

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/utils/t;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/adapter/aw;->v:Lcom/google/android/youtube/core/utils/t;

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2

    check-cast p1, Lcom/google/android/youtube/core/async/GDataRequest;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Error for request "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    instance-of v0, p2, Lorg/apache/http/client/HttpResponseException;

    if-eqz v0, :cond_0

    move-object v0, p2

    check-cast v0, Lorg/apache/http/client/HttpResponseException;

    invoke-virtual {v0}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v0

    const/16 v1, 0x193

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/aw;->d:Landroid/app/Activity;

    const v1, 0x7f0b0012

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/app/adapter/aw;->a(Ljava/lang/String;Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/aw;->f:Lcom/google/android/youtube/core/e;

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/core/e;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/app/adapter/aw;->a(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 10

    const/4 v8, 0x0

    const/4 v2, 0x0

    check-cast p1, Lcom/google/android/youtube/core/async/GDataRequest;

    check-cast p2, Lcom/google/android/youtube/core/model/Page;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/aw;->k:Lcom/google/android/youtube/core/async/GDataRequest;

    if-ne p1, v0, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/aw;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget-object v3, p2, Lcom/google/android/youtube/core/model/Page;->entries:Ljava/util/List;

    new-instance v4, Ljava/util/ArrayList;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    iget v0, p0, Lcom/google/android/youtube/app/adapter/aw;->u:I

    iget v5, p2, Lcom/google/android/youtube/core/model/Page;->startIndex:I

    sub-int/2addr v0, v5

    add-int/lit8 v0, v0, 0x1

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    move v9, v0

    move v0, v1

    move v1, v9

    :goto_0
    if-ge v1, v5, :cond_2

    iget v6, p0, Lcom/google/android/youtube/app/adapter/aw;->n:I

    if-ge v0, v6, :cond_2

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/youtube/app/adapter/aw;->v:Lcom/google/android/youtube/core/utils/t;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/google/android/youtube/app/adapter/aw;->v:Lcom/google/android/youtube/core/utils/t;

    invoke-interface {v7, v6}, Lcom/google/android/youtube/core/utils/t;->a(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    :cond_0
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    iget v1, p0, Lcom/google/android/youtube/app/adapter/aw;->u:I

    iget v5, p2, Lcom/google/android/youtube/core/model/Page;->startIndex:I

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/2addr v3, v5

    add-int/lit8 v3, v3, -0x1

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/app/adapter/aw;->u:I

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/aw;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {p0}, Lcom/google/android/youtube/app/adapter/aw;->k()V

    iget-object v1, p2, Lcom/google/android/youtube/core/model/Page;->nextUri:Landroid/net/Uri;

    iput-object v1, p0, Lcom/google/android/youtube/app/adapter/aw;->t:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/aw;->l:Lcom/google/android/youtube/app/adapter/ay;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/aw;->l:Lcom/google/android/youtube/app/adapter/ay;

    :cond_3
    :goto_1
    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/aw;->j:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/aw;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/aw;->l:Lcom/google/android/youtube/app/adapter/ay;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/aw;->l:Lcom/google/android/youtube/app/adapter/ay;

    invoke-interface {v0}, Lcom/google/android/youtube/app/adapter/ay;->e()V

    :cond_4
    :goto_2
    return-void

    :cond_5
    iput-object v8, p0, Lcom/google/android/youtube/app/adapter/aw;->t:Landroid/net/Uri;

    goto :goto_1

    :cond_6
    iget v1, p0, Lcom/google/android/youtube/app/adapter/aw;->r:I

    iget v3, p2, Lcom/google/android/youtube/core/model/Page;->totalResults:I

    add-int/2addr v1, v3

    iget v3, p0, Lcom/google/android/youtube/app/adapter/aw;->n:I

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    :goto_3
    iget-boolean v3, p0, Lcom/google/android/youtube/app/adapter/aw;->q:Z

    if-nez v3, :cond_7

    if-lt v0, v1, :cond_8

    :cond_7
    const/4 v2, 0x1

    :cond_8
    iput-boolean v2, p0, Lcom/google/android/youtube/app/adapter/aw;->q:Z

    iget-boolean v2, p0, Lcom/google/android/youtube/app/adapter/aw;->q:Z

    if-eqz v2, :cond_a

    iget v2, p0, Lcom/google/android/youtube/app/adapter/aw;->p:I

    if-lt v2, v1, :cond_a

    iput-object v8, p0, Lcom/google/android/youtube/app/adapter/aw;->t:Landroid/net/Uri;

    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/aw;->h()V

    goto :goto_2

    :cond_9
    iget v1, p0, Lcom/google/android/youtube/app/adapter/aw;->n:I

    iput v0, p0, Lcom/google/android/youtube/app/adapter/aw;->r:I

    goto :goto_3

    :cond_a
    iget v1, p0, Lcom/google/android/youtube/app/adapter/aw;->p:I

    if-ge v0, v1, :cond_b

    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/aw;->o()V

    goto :goto_2

    :cond_b
    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/aw;->g()V

    goto :goto_2
.end method

.method protected final a(Ljava/util/Set;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/aw;->s:Lcom/google/android/youtube/core/a/g;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final varargs a([Lcom/google/android/youtube/core/async/GDataRequest;)V
    .locals 5

    const/4 v1, 0x0

    const-string v0, "requests cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    array-length v0, p1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "requests cannot be empty"

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/aw;->i()V

    :goto_1
    array-length v0, p1

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/aw;->j:Ljava/util/LinkedList;

    aget-object v2, p1, v1

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "request "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " cannot be null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/aw;->j()V

    return-void
.end method

.method public final b(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/aw;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final b()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/aw;->i()V

    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/aw;->f()V

    return-void
.end method

.method public final c(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/aw;->k:Lcom/google/android/youtube/core/async/GDataRequest;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/aw;->k:Lcom/google/android/youtube/core/async/GDataRequest;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/adapter/aw;->a(Lcom/google/android/youtube/core/async/GDataRequest;)V

    :cond_0
    return-void
.end method

.method public final e()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/aw;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/google/android/youtube/app/adapter/aw;->p:I

    if-le v0, v1, :cond_1

    iget v0, p0, Lcom/google/android/youtube/app/adapter/aw;->n:I

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/aw;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget v1, p0, Lcom/google/android/youtube/app/adapter/aw;->p:I

    iget v2, p0, Lcom/google/android/youtube/app/adapter/aw;->m:I

    add-int/2addr v1, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/app/adapter/aw;->p:I

    invoke-virtual {p0}, Lcom/google/android/youtube/app/adapter/aw;->k()V

    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/aw;->g()V

    iget-boolean v1, p0, Lcom/google/android/youtube/app/adapter/aw;->q:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/youtube/app/adapter/aw;->p:I

    if-ne v1, v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/aw;->t:Landroid/net/Uri;

    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/aw;->h()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/google/android/youtube/app/adapter/aw;->p:I

    iget v1, p0, Lcom/google/android/youtube/app/adapter/aw;->m:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/youtube/app/adapter/aw;->p:I

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/aw;->a:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public final f_()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/aw;->i()V

    return-void
.end method
