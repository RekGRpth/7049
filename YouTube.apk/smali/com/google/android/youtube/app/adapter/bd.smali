.class final Lcom/google/android/youtube/app/adapter/bd;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/adapter/bl;
.implements Lcom/google/android/youtube/app/adapter/m;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/adapter/bb;

.field private final b:Landroid/view/View;

.field private final c:Lcom/google/android/youtube/app/adapter/k;

.field private d:Landroid/widget/ImageView;

.field private final e:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/adapter/bb;Landroid/view/View;Landroid/view/ViewGroup;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/youtube/app/adapter/bd;->a:Lcom/google/android/youtube/app/adapter/bb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/youtube/app/adapter/bd;->b:Landroid/view/View;

    invoke-static {p1}, Lcom/google/android/youtube/app/adapter/bb;->a(Lcom/google/android/youtube/app/adapter/bb;)Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;->a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/youtube/app/adapter/bl;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/k;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bd;->c:Lcom/google/android/youtube/app/adapter/k;

    const v0, 0x7f070036

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bd;->e:Landroid/widget/ImageView;

    const v0, 0x7f0700a1

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bd;->d:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bd;->e:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bd;->c:Lcom/google/android/youtube/app/adapter/k;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/adapter/k;->a(Lcom/google/android/youtube/app/adapter/m;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic a(ILjava/lang/Object;)Landroid/view/View;
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    check-cast p2, Lcom/google/android/youtube/core/model/Playlist;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bd;->e:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bd;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bd;->c:Lcom/google/android/youtube/app/adapter/k;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/app/adapter/k;->a(ILjava/lang/Object;)Landroid/view/View;

    iget-object v0, p2, Lcom/google/android/youtube/core/model/Playlist;->hqThumbnailUri:Landroid/net/Uri;

    if-nez v0, :cond_3

    move v0, v1

    :goto_0
    iget-object v3, p2, Lcom/google/android/youtube/core/model/Playlist;->thumbnailUri:Landroid/net/Uri;

    if-nez v3, :cond_4

    :goto_1
    and-int/2addr v0, v1

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bd;->d:Landroid/widget/ImageView;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bd;->b:Landroid/view/View;

    const v1, 0x7f070037

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bd;->d:Landroid/widget/ImageView;

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bd;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bd;->b:Landroid/view/View;

    return-object v0

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bd;->d:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bd;->d:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2
.end method

.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bd;->e:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method

.method public final b()V
    .locals 0

    return-void
.end method
