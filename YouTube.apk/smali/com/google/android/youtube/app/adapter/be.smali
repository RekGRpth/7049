.class public Lcom/google/android/youtube/app/adapter/be;
.super Lcom/google/android/youtube/core/a/a;
.source "SourceFile"


# instance fields
.field protected final a:Landroid/content/res/Resources;

.field private final b:Landroid/view/LayoutInflater;

.field private final d:Lcom/google/android/youtube/app/ui/ToolbarHelper;

.field private final e:I

.field private final f:Ljava/util/Map;

.field private final g:Landroid/graphics/Bitmap;

.field private final h:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/app/ui/ToolbarHelper;I)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/youtube/core/a/a;-><init>()V

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/be;->b:Landroid/view/LayoutInflater;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/be;->a:Landroid/content/res/Resources;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/be;->f:Ljava/util/Map;

    iput-object p2, p0, Lcom/google/android/youtube/app/adapter/be;->d:Lcom/google/android/youtube/app/ui/ToolbarHelper;

    iput p3, p0, Lcom/google/android/youtube/app/adapter/be;->e:I

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/be;->a:Landroid/content/res/Resources;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/cl;->a(Landroid/content/res/Resources;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/be;->h:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/be;->a:Landroid/content/res/Resources;

    const v1, 0x7f020159

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/be;->g:Landroid/graphics/Bitmap;

    return-void
.end method


# virtual methods
.method protected final a(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/be;->b:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/google/android/youtube/app/adapter/be;->e:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    new-instance v0, Lcom/google/android/youtube/app/adapter/bf;

    invoke-direct {v0, p1}, Lcom/google/android/youtube/app/adapter/bf;-><init>(Landroid/view/View;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :cond_0
    return-object p1
.end method

.method protected final a(Lcom/google/android/youtube/core/model/Playlist;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    const/4 v9, 0x1

    const/4 v8, 0x0

    invoke-virtual {p0, p2, p3}, Lcom/google/android/youtube/app/adapter/be;->a(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/bf;

    iget-object v1, v0, Lcom/google/android/youtube/app/adapter/bf;->e:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/youtube/app/adapter/bf;->e:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    iget-object v1, v0, Lcom/google/android/youtube/app/adapter/bf;->a:Landroid/widget/TextView;

    iget-object v2, p1, Lcom/google/android/youtube/core/model/Playlist;->title:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Lcom/google/android/youtube/app/adapter/bf;->b:Landroid/widget/TextView;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/google/android/youtube/app/adapter/bf;->b:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/youtube/app/adapter/be;->a:Landroid/content/res/Resources;

    const v4, 0x7f0c000c

    iget v5, p1, Lcom/google/android/youtube/core/model/Playlist;->size:I

    new-array v6, v9, [Ljava/lang/Object;

    iget v7, p1, Lcom/google/android/youtube/core/model/Playlist;->size:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v2, v4, v5, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v1, v0, Lcom/google/android/youtube/app/adapter/bf;->c:Landroid/widget/ImageView;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/be;->f:Ljava/util/Map;

    iget-object v2, p1, Lcom/google/android/youtube/core/model/Playlist;->contentUri:Landroid/net/Uri;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    if-eqz v1, :cond_2

    iget-object v4, v0, Lcom/google/android/youtube/app/adapter/bf;->c:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/google/android/youtube/app/adapter/be;->g:Landroid/graphics/Bitmap;

    if-ne v1, v2, :cond_4

    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    :goto_0
    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    iget-object v2, v0, Lcom/google/android/youtube/app/adapter/bf;->c:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_2
    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/be;->d:Lcom/google/android/youtube/app/ui/ToolbarHelper;

    if-eqz v1, :cond_3

    iget-object v1, v0, Lcom/google/android/youtube/app/adapter/bf;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setClickable(Z)V

    iget-object v1, v0, Lcom/google/android/youtube/app/adapter/bf;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/be;->d:Lcom/google/android/youtube/app/ui/ToolbarHelper;

    iget-object v0, v0, Lcom/google/android/youtube/app/adapter/bf;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v0, p1}, Lcom/google/android/youtube/app/ui/ToolbarHelper;->a(Landroid/view/View;Ljava/lang/Object;)V

    :cond_3
    return-object v3

    :cond_4
    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    goto :goto_0
.end method

.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/be;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    invoke-super {p0}, Lcom/google/android/youtube/core/a/a;->a()V

    return-void
.end method

.method protected final synthetic a(ILjava/lang/Object;)V
    .locals 3

    check-cast p2, Lcom/google/android/youtube/core/model/Playlist;

    const-string v0, "playlist can\'t be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/be;->f:Ljava/util/Map;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/Playlist;->contentUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/youtube/app/adapter/be;->h:Landroid/graphics/Bitmap;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/core/a/a;->a(ILjava/lang/Object;)V

    return-void
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 3

    check-cast p1, Lcom/google/android/youtube/core/model/Playlist;

    const-string v0, "playlist can\'t be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/be;->f:Ljava/util/Map;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Playlist;->contentUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/youtube/app/adapter/be;->h:Landroid/graphics/Bitmap;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-super {p0, p1}, Lcom/google/android/youtube/core/a/a;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/app/adapter/be;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Playlist;

    invoke-virtual {p0, v0, p2, p3}, Lcom/google/android/youtube/app/adapter/be;->a(Lcom/google/android/youtube/core/model/Playlist;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
