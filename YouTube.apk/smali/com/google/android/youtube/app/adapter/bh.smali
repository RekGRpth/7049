.class public final Lcom/google/android/youtube/app/adapter/bh;
.super Lcom/google/android/youtube/app/adapter/j;
.source "SourceFile"


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/google/android/youtube/app/remote/RemoteControl;

.field private final c:Lcom/google/android/youtube/app/adapter/aj;

.field private final d:Lcom/google/android/youtube/core/Analytics;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/app/remote/RemoteControl;Lcom/google/android/youtube/app/ui/v;Lcom/google/android/youtube/core/Analytics;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/j;-><init>()V

    const-string v0, "activity can not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bh;->a:Landroid/app/Activity;

    const-string v0, "remoteControl can not be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/remote/RemoteControl;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bh;->b:Lcom/google/android/youtube/app/remote/RemoteControl;

    new-instance v0, Lcom/google/android/youtube/app/adapter/aj;

    invoke-direct {v0, p3}, Lcom/google/android/youtube/app/adapter/aj;-><init>(Lcom/google/android/youtube/app/ui/v;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bh;->c:Lcom/google/android/youtube/app/adapter/aj;

    const-string v0, "analytics can not be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/Analytics;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bh;->d:Lcom/google/android/youtube/core/Analytics;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/adapter/bh;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bh;->a:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/adapter/bh;)Lcom/google/android/youtube/app/remote/RemoteControl;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bh;->b:Lcom/google/android/youtube/app/remote/RemoteControl;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/adapter/bh;)Lcom/google/android/youtube/core/Analytics;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bh;->d:Lcom/google/android/youtube/core/Analytics;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/adapter/bh;)Lcom/google/android/youtube/app/adapter/aj;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bh;->c:Lcom/google/android/youtube/app/adapter/aj;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/youtube/app/adapter/bl;
    .locals 2

    new-instance v0, Lcom/google/android/youtube/app/adapter/bi;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/google/android/youtube/app/adapter/bi;-><init>(Lcom/google/android/youtube/app/adapter/bh;Landroid/view/View;Landroid/view/ViewGroup;B)V

    return-object v0
.end method
