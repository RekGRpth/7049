.class final Lcom/google/android/youtube/app/adapter/bi;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/adapter/bl;
.implements Lcom/google/android/youtube/core/player/overlay/ac;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/adapter/bh;

.field private final b:Landroid/widget/RelativeLayout;

.field private final c:Lcom/google/android/youtube/core/player/overlay/TimeBar;

.field private final d:Landroid/widget/ImageView;

.field private final e:Landroid/widget/LinearLayout;

.field private final f:Landroid/view/View;

.field private final g:Landroid/view/View;

.field private final h:Landroid/os/Handler;

.field private final i:Landroid/view/View;

.field private final j:Landroid/widget/ImageView;

.field private final k:Landroid/widget/TextView;

.field private final l:Lcom/google/android/youtube/app/adapter/ak;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/adapter/bh;Landroid/view/View;Landroid/view/ViewGroup;)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, -0x1

    const/4 v3, -0x2

    iput-object p1, p0, Lcom/google/android/youtube/app/adapter/bi;->a:Lcom/google/android/youtube/app/adapter/bh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x7f070090

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bi;->b:Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bi;->b:Landroid/widget/RelativeLayout;

    const v1, 0x7f070036

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bi;->d:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bi;->b:Landroid/widget/RelativeLayout;

    const v1, 0x7f07008e

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bi;->e:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bi;->b:Landroid/widget/RelativeLayout;

    const v1, 0x7f07002c

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bi;->k:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bi;->k:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->setSingleLine()V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bi;->b:Landroid/widget/RelativeLayout;

    const v1, 0x7f070058

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bi;->f:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bi;->b:Landroid/widget/RelativeLayout;

    const v1, 0x7f070091

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bi;->g:Landroid/view/View;

    new-instance v0, Landroid/view/View;

    invoke-static {p1}, Lcom/google/android/youtube/app/adapter/bh;->a(Lcom/google/android/youtube/app/adapter/bh;)Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bi;->i:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bi;->i:Landroid/view/View;

    const v1, 0x7f020090

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/bi;->b:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/google/android/youtube/app/adapter/bi;->i:Landroid/view/View;

    invoke-virtual {v1, v2, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Landroid/widget/ImageView;

    invoke-static {p1}, Lcom/google/android/youtube/app/adapter/bh;->a(Lcom/google/android/youtube/app/adapter/bh;)Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bi;->j:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bi;->j:Landroid/widget/ImageView;

    const v1, 0x7f0201a5

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bi;->j:Landroid/widget/ImageView;

    const v1, 0x7f020065

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/bi;->b:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/google/android/youtube/app/adapter/bi;->j:Landroid/widget/ImageView;

    invoke-virtual {v1, v2, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bi;->j:Landroid/widget/ImageView;

    new-instance v1, Lcom/google/android/youtube/app/adapter/bj;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/app/adapter/bj;-><init>(Lcom/google/android/youtube/app/adapter/bi;Lcom/google/android/youtube/app/adapter/bh;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/google/android/youtube/core/player/overlay/TimeBar;

    invoke-static {p1}, Lcom/google/android/youtube/app/adapter/bh;->a(Lcom/google/android/youtube/app/adapter/bh;)Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/player/overlay/ac;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bi;->c:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bi;->c:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    invoke-virtual {v0, v5}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->setShowScrubber(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bi;->c:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    invoke-virtual {v0, v5}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bi;->c:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    invoke-static {p1}, Lcom/google/android/youtube/app/adapter/bh;->a(Lcom/google/android/youtube/app/adapter/bh;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090048

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->setProgressColor(I)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v4, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/bi;->b:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/google/android/youtube/app/adapter/bi;->c:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    invoke-virtual {v1, v2, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Lcom/google/android/youtube/app/adapter/bk;

    invoke-static {p1}, Lcom/google/android/youtube/app/adapter/bh;->a(Lcom/google/android/youtube/app/adapter/bh;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1}, Lcom/google/android/youtube/app/adapter/bk;-><init>(Lcom/google/android/youtube/app/adapter/bi;Landroid/os/Looper;Lcom/google/android/youtube/app/adapter/bh;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bi;->h:Landroid/os/Handler;

    invoke-static {p1}, Lcom/google/android/youtube/app/adapter/bh;->d(Lcom/google/android/youtube/app/adapter/bh;)Lcom/google/android/youtube/app/adapter/aj;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/google/android/youtube/app/adapter/aj;->a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/youtube/app/adapter/bl;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/ak;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bi;->l:Lcom/google/android/youtube/app/adapter/ak;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bi;->l:Lcom/google/android/youtube/app/adapter/ak;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/ak;->a(Z)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/app/adapter/bh;Landroid/view/View;Landroid/view/ViewGroup;B)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/app/adapter/bi;-><init>(Lcom/google/android/youtube/app/adapter/bh;Landroid/view/View;Landroid/view/ViewGroup;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/adapter/bi;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bi;->h:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/adapter/bi;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/adapter/bi;->b(I)V

    return-void
.end method

.method private b(I)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bi;->a:Lcom/google/android/youtube/app/adapter/bh;

    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/bh;->b(Lcom/google/android/youtube/app/adapter/bh;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->f()Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->ENDED:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bi;->a:Lcom/google/android/youtube/app/adapter/bh;

    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/bh;->b(Lcom/google/android/youtube/app/adapter/bh;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->i()D

    move-result-wide v0

    double-to-int v0, v0

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/bi;->c:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    const/16 v2, 0x64

    invoke-virtual {v1, v0, p1, v2}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->setTime(III)V

    return-void

    :cond_0
    move v0, p1

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a(ILjava/lang/Object;)Landroid/view/View;
    .locals 8

    const/4 v7, -0x1

    const/4 v6, -0x2

    const v5, 0x7f070056

    const/16 v4, 0x8

    const/4 v3, 0x0

    check-cast p2, Lcom/google/android/youtube/core/model/Video;

    iget-object v0, p2, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/bi;->a:Lcom/google/android/youtube/app/adapter/bh;

    invoke-static {v1}, Lcom/google/android/youtube/app/adapter/bh;->b(Lcom/google/android/youtube/app/adapter/bh;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bi;->i:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bi;->c:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->setVisibility(I)V

    iget v0, p2, Lcom/google/android/youtube/core/model/Video;->duration:I

    mul-int/lit16 v0, v0, 0x3e8

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/adapter/bi;->b(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bi;->h:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/bi;->h:Landroid/os/Handler;

    iget v2, p2, Lcom/google/android/youtube/core/model/Video;->duration:I

    mul-int/lit16 v2, v2, 0x3e8

    invoke-static {v1, v3, v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bi;->d:Landroid/widget/ImageView;

    const v1, 0x7f020215

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v7, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/4 v1, 0x6

    invoke-virtual {v0, v1, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    const/4 v1, 0x5

    invoke-virtual {v0, v1, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/bi;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bi;->f:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bi;->g:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bi;->j:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bi;->l:Lcom/google/android/youtube/app/adapter/ak;

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/app/adapter/ak;->a(Lcom/google/android/youtube/core/model/Video;)Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bi;->b:Landroid/widget/RelativeLayout;

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bi;->i:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bi;->c:Lcom/google/android/youtube/core/player/overlay/TimeBar;

    invoke-virtual {v0, v4}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bi;->h:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bi;->d:Landroid/widget/ImageView;

    const v1, 0x7f020214

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v7, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    const/4 v1, 0x5

    invoke-virtual {v0, v1, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/bi;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bi;->f:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bi;->g:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bi;->j:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bi;->j:Landroid/widget/ImageView;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a()V
    .locals 0

    return-void
.end method

.method public final a(I)V
    .locals 0

    return-void
.end method
