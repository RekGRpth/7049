.class public final Lcom/google/android/youtube/app/adapter/bn;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Landroid/content/Context;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/app/YouTubeApplication;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/app/ui/cw;Landroid/app/Activity;)Lcom/google/android/youtube/app/adapter/ab;
    .locals 12

    new-instance v11, Lcom/google/android/youtube/app/adapter/bp;

    const v1, 0x7f070070

    move-object/from16 v0, p7

    invoke-direct {v11, p0, v1, v0}, Lcom/google/android/youtube/app/adapter/bp;-><init>(Landroid/content/Context;ILcom/google/android/youtube/core/client/be;)V

    new-instance v1, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v1 .. v10}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/app/YouTubeApplication;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/app/ui/cw;Landroid/app/Activity;)V

    new-instance v2, Lcom/google/android/youtube/app/adapter/af;

    invoke-direct {v2}, Lcom/google/android/youtube/app/adapter/af;-><init>()V

    invoke-virtual {v2, v11}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v2

    new-instance v3, Lcom/google/android/youtube/app/adapter/ab;

    const v4, 0x7f040022

    invoke-direct {v3, p0, v4, v2, v1}, Lcom/google/android/youtube/app/adapter/ab;-><init>(Landroid/content/Context;ILcom/google/android/youtube/app/adapter/bv;Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)V

    return-object v3
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/be;)Lcom/google/android/youtube/app/adapter/bm;
    .locals 3

    new-instance v0, Lcom/google/android/youtube/app/adapter/bm;

    const v1, 0x7f040036

    new-instance v2, Lcom/google/android/youtube/app/adapter/p;

    invoke-direct {v2, p0, p1, p2}, Lcom/google/android/youtube/app/adapter/p;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/be;)V

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/youtube/app/adapter/bm;-><init>(Landroid/content/Context;ILcom/google/android/youtube/app/adapter/bv;)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/bg;Lcom/google/android/youtube/core/utils/p;Lcom/google/android/youtube/app/prefetch/d;)Lcom/google/android/youtube/app/adapter/bm;
    .locals 7

    new-instance v0, Lcom/google/android/youtube/app/adapter/bz;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/adapter/bz;-><init>(Landroid/content/Context;)V

    new-instance v1, Lcom/google/android/youtube/app/adapter/cj;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/adapter/cj;-><init>(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/google/android/youtube/core/utils/Util;->g(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/adapter/cj;->a(Landroid/graphics/Typeface;)V

    const/4 v2, 0x1

    invoke-static {p0, p2, p4, v2}, Lcom/google/android/youtube/app/adapter/cm;->a(Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/utils/p;Z)Lcom/google/android/youtube/app/adapter/cm;

    move-result-object v2

    new-instance v3, Lcom/google/android/youtube/app/adapter/h;

    invoke-static {p0}, Lcom/google/android/youtube/core/utils/j;->d(Landroid/content/Context;)Z

    move-result v4

    const/16 v5, 0x8

    const/4 v6, 0x0

    invoke-direct {v3, p5, v4, v5, v6}, Lcom/google/android/youtube/app/adapter/h;-><init>(Lcom/google/android/youtube/app/prefetch/d;ZIZ)V

    new-instance v4, Lcom/google/android/youtube/app/adapter/bo;

    invoke-direct {v4, p0, p1, p2}, Lcom/google/android/youtube/app/adapter/bo;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/be;)V

    new-instance v5, Lcom/google/android/youtube/app/adapter/af;

    invoke-direct {v5}, Lcom/google/android/youtube/app/adapter/af;-><init>()V

    invoke-virtual {v5, v0}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/app/adapter/bm;

    const v2, 0x7f04009b

    invoke-direct {v1, p0, v2, v0}, Lcom/google/android/youtube/app/adapter/bm;-><init>(Landroid/content/Context;ILcom/google/android/youtube/app/adapter/bv;)V

    return-object v1
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/utils/p;)Lcom/google/android/youtube/app/adapter/bm;
    .locals 5

    new-instance v0, Lcom/google/android/youtube/app/adapter/bz;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/adapter/bz;-><init>(Landroid/content/Context;)V

    new-instance v1, Lcom/google/android/youtube/app/adapter/cj;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/adapter/cj;-><init>(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/google/android/youtube/core/utils/Util;->g(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/adapter/cj;->a(Landroid/graphics/Typeface;)V

    invoke-static {p0, p2, p3, p1}, Lcom/google/android/youtube/app/adapter/at;->a(Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/utils/p;Lcom/google/android/youtube/core/client/bc;)Lcom/google/android/youtube/app/adapter/at;

    move-result-object v2

    new-instance v3, Lcom/google/android/youtube/app/adapter/bq;

    invoke-direct {v3, p0, p1, p2}, Lcom/google/android/youtube/app/adapter/bq;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/be;)V

    new-instance v4, Lcom/google/android/youtube/app/adapter/af;

    invoke-direct {v4}, Lcom/google/android/youtube/app/adapter/af;-><init>()V

    invoke-virtual {v4, v0}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/app/adapter/ar;

    invoke-direct {v1, p0, v0}, Lcom/google/android/youtube/app/adapter/ar;-><init>(Landroid/content/Context;Lcom/google/android/youtube/app/adapter/bv;)V

    new-instance v0, Lcom/google/android/youtube/app/adapter/bm;

    const v2, 0x7f04009b

    invoke-direct {v0, p0, v2, v1}, Lcom/google/android/youtube/app/adapter/bm;-><init>(Landroid/content/Context;ILcom/google/android/youtube/app/adapter/bv;)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/youtube/core/client/be;)Lcom/google/android/youtube/app/adapter/bm;
    .locals 3

    new-instance v0, Lcom/google/android/youtube/app/adapter/br;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/adapter/br;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;->LARGE:Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;

    invoke-static {p0, p1, v1}, Lcom/google/android/youtube/app/adapter/bb;->a(Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)Lcom/google/android/youtube/app/adapter/bb;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/app/adapter/af;

    invoke-direct {v2}, Lcom/google/android/youtube/app/adapter/af;-><init>()V

    invoke-virtual {v2, v0}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/app/adapter/bm;

    const v2, 0x7f040032

    invoke-direct {v1, p0, v2, v0}, Lcom/google/android/youtube/app/adapter/bm;-><init>(Landroid/content/Context;ILcom/google/android/youtube/app/adapter/bv;)V

    return-object v1
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/app/ui/v;)Lcom/google/android/youtube/app/adapter/bm;
    .locals 4

    new-instance v0, Lcom/google/android/youtube/app/adapter/bt;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/adapter/bt;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;->SMALL:Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;

    invoke-static {p0, p1, v1}, Lcom/google/android/youtube/app/adapter/bb;->a(Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)Lcom/google/android/youtube/app/adapter/bb;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/app/adapter/ah;

    invoke-direct {v2, p2}, Lcom/google/android/youtube/app/adapter/ah;-><init>(Lcom/google/android/youtube/app/ui/v;)V

    new-instance v3, Lcom/google/android/youtube/app/adapter/af;

    invoke-direct {v3}, Lcom/google/android/youtube/app/adapter/af;-><init>()V

    invoke-virtual {v3, v0}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/app/adapter/bm;

    const v2, 0x7f040032

    invoke-direct {v1, p0, v2, v0}, Lcom/google/android/youtube/app/adapter/bm;-><init>(Landroid/content/Context;ILcom/google/android/youtube/app/adapter/bv;)V

    return-object v1
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/bg;)Lcom/google/android/youtube/app/adapter/bm;
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0, v0}, Lcom/google/android/youtube/app/adapter/bn;->a(Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/bg;Lcom/google/android/youtube/app/prefetch/d;Lcom/google/android/gms/plus/a;)Lcom/google/android/youtube/app/adapter/bm;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/bg;Lcom/google/android/youtube/app/prefetch/d;)Lcom/google/android/youtube/app/adapter/bm;
    .locals 1

    const-string v0, "prefetchStore cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lcom/google/android/youtube/app/adapter/bn;->a(Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/bg;Lcom/google/android/youtube/app/prefetch/d;Lcom/google/android/gms/plus/a;)Lcom/google/android/youtube/app/adapter/bm;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/bg;Lcom/google/android/youtube/app/prefetch/d;Lcom/google/android/gms/plus/a;)Lcom/google/android/youtube/app/adapter/bm;
    .locals 6

    new-instance v0, Lcom/google/android/youtube/app/adapter/cj;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/adapter/cj;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;->SMALL:Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;

    const/4 v2, 0x0

    invoke-static {p0, p1, v1, v2}, Lcom/google/android/youtube/app/adapter/cm;->a(Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;Z)Lcom/google/android/youtube/app/adapter/cm;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/app/adapter/h;

    invoke-static {p0}, Lcom/google/android/youtube/core/utils/j;->d(Landroid/content/Context;)Z

    move-result v3

    const/16 v4, 0xf

    const/4 v5, 0x1

    invoke-direct {v2, p3, v3, v4, v5}, Lcom/google/android/youtube/app/adapter/h;-><init>(Lcom/google/android/youtube/app/prefetch/d;ZIZ)V

    new-instance v3, Lcom/google/android/youtube/app/adapter/af;

    invoke-direct {v3}, Lcom/google/android/youtube/app/adapter/af;-><init>()V

    invoke-virtual {v3, v0}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/app/adapter/bm;

    const v2, 0x7f040037

    invoke-direct {v1, p0, v2, v0}, Lcom/google/android/youtube/app/adapter/bm;-><init>(Landroid/content/Context;ILcom/google/android/youtube/app/adapter/bv;)V

    return-object v1
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/bg;Lcom/google/android/youtube/app/prefetch/d;Lcom/google/android/youtube/app/ui/v;I)Lcom/google/android/youtube/app/adapter/bm;
    .locals 6

    const-string v0, "prefetchStore cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const v5, 0x7f0400bc

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/youtube/app/adapter/bn;->b(Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/bg;Lcom/google/android/youtube/app/prefetch/d;Lcom/google/android/youtube/app/ui/v;I)Lcom/google/android/youtube/app/adapter/bm;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/bg;Lcom/google/android/youtube/app/ui/v;I)Lcom/google/android/youtube/app/adapter/bm;
    .locals 6

    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move v5, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/youtube/app/adapter/bn;->b(Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/bg;Lcom/google/android/youtube/app/prefetch/d;Lcom/google/android/youtube/app/ui/v;I)Lcom/google/android/youtube/app/adapter/bm;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/bg;Lcom/google/android/youtube/app/prefetch/d;Lcom/google/android/youtube/app/ui/v;Lcom/google/android/youtube/app/ui/v;)Lcom/google/android/youtube/app/adapter/cd;
    .locals 4

    new-instance v0, Lcom/google/android/youtube/app/adapter/cj;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/adapter/cj;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;->SMALL:Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;

    const/4 v2, 0x0

    invoke-static {p0, p1, v1, v2}, Lcom/google/android/youtube/app/adapter/cm;->a(Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;Z)Lcom/google/android/youtube/app/adapter/cm;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/app/adapter/aj;

    invoke-direct {v2, p4}, Lcom/google/android/youtube/app/adapter/aj;-><init>(Lcom/google/android/youtube/app/ui/v;)V

    new-instance v3, Lcom/google/android/youtube/app/adapter/af;

    invoke-direct {v3}, Lcom/google/android/youtube/app/adapter/af;-><init>()V

    invoke-virtual {v3, v0}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/app/adapter/cb;

    invoke-direct {v1, p0, p5}, Lcom/google/android/youtube/app/adapter/cb;-><init>(Landroid/content/Context;Lcom/google/android/youtube/app/ui/v;)V

    new-instance v2, Lcom/google/android/youtube/app/adapter/ce;

    invoke-direct {v2, v0, v1}, Lcom/google/android/youtube/app/adapter/ce;-><init>(Lcom/google/android/youtube/app/adapter/bv;Lcom/google/android/youtube/app/adapter/cb;)V

    new-instance v0, Lcom/google/android/youtube/app/adapter/cd;

    const v1, 0x7f0400a7

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/youtube/app/adapter/cd;-><init>(Landroid/content/Context;ILcom/google/android/youtube/app/adapter/ce;)V

    return-object v0
.end method

.method private static b(Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/bg;Lcom/google/android/youtube/app/prefetch/d;Lcom/google/android/youtube/app/ui/v;I)Lcom/google/android/youtube/app/adapter/bm;
    .locals 7

    const/4 v6, 0x0

    new-instance v0, Lcom/google/android/youtube/app/adapter/cj;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/adapter/cj;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;->SMALL:Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;

    invoke-static {p0, p1, v1, v6}, Lcom/google/android/youtube/app/adapter/cm;->a(Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;Z)Lcom/google/android/youtube/app/adapter/cm;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/app/adapter/aj;

    invoke-direct {v2, p4}, Lcom/google/android/youtube/app/adapter/aj;-><init>(Lcom/google/android/youtube/app/ui/v;)V

    new-instance v3, Lcom/google/android/youtube/app/adapter/h;

    invoke-static {p0}, Lcom/google/android/youtube/core/utils/j;->d(Landroid/content/Context;)Z

    move-result v4

    const/4 v5, 0x2

    invoke-direct {v3, p3, v4, v5, v6}, Lcom/google/android/youtube/app/adapter/h;-><init>(Lcom/google/android/youtube/app/prefetch/d;ZIZ)V

    new-instance v4, Lcom/google/android/youtube/app/adapter/af;

    invoke-direct {v4}, Lcom/google/android/youtube/app/adapter/af;-><init>()V

    invoke-virtual {v4, v0}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/app/adapter/bm;

    invoke-direct {v1, p0, p5, v0}, Lcom/google/android/youtube/app/adapter/bm;-><init>(Landroid/content/Context;ILcom/google/android/youtube/app/adapter/bv;)V

    return-object v1
.end method
