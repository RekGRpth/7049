.class public final Lcom/google/android/youtube/app/adapter/bw;
.super Lcom/google/android/youtube/app/adapter/f;
.source "SourceFile"


# instance fields
.field private final a:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/be;)V
    .locals 2

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/app/adapter/f;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/be;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02021a

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bw;->a:Landroid/graphics/Bitmap;

    return-void
.end method


# virtual methods
.method protected final bridge synthetic a(Ljava/lang/Object;)Landroid/net/Uri;
    .locals 1

    check-cast p1, Lcom/google/android/youtube/core/model/Event;

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Event;->subjectUri:Landroid/net/Uri;

    return-object v0
.end method

.method protected final bridge synthetic a(Ljava/lang/Object;Landroid/view/View;Lcom/google/android/youtube/core/async/n;)V
    .locals 2

    check-cast p1, Lcom/google/android/youtube/core/model/Event;

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Event;->action:Lcom/google/android/youtube/core/model/Event$Action;

    sget-object v1, Lcom/google/android/youtube/core/model/Event$Action;->VIDEO_RECOMMENDED:Lcom/google/android/youtube/core/model/Event$Action;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/bw;->a:Landroid/graphics/Bitmap;

    invoke-interface {p3, v0, v1}, Lcom/google/android/youtube/core/async/n;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/youtube/app/adapter/f;->a(Ljava/lang/Object;Landroid/view/View;Lcom/google/android/youtube/core/async/n;)V

    goto :goto_0
.end method
