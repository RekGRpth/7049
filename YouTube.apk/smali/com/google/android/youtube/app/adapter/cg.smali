.class public final Lcom/google/android/youtube/app/adapter/cg;
.super Lcom/google/android/youtube/core/a/b;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private final c:Landroid/view/LayoutInflater;

.field private final d:I

.field private final e:Lcom/google/android/youtube/app/adapter/ch;

.field private final f:Lcom/google/android/youtube/app/adapter/ci;

.field private final g:I

.field private final h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:I


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/a/e;Landroid/view/LayoutInflater;Lcom/google/android/youtube/core/a/g;IILcom/google/android/youtube/app/adapter/ch;Lcom/google/android/youtube/app/adapter/ci;)V
    .locals 9

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v8}, Lcom/google/android/youtube/app/adapter/cg;-><init>(Lcom/google/android/youtube/core/a/e;Landroid/view/LayoutInflater;Lcom/google/android/youtube/core/a/g;IILcom/google/android/youtube/app/adapter/ch;Lcom/google/android/youtube/app/adapter/ci;I)V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/youtube/core/a/e;Landroid/view/LayoutInflater;Lcom/google/android/youtube/core/a/g;IILcom/google/android/youtube/app/adapter/ch;Lcom/google/android/youtube/app/adapter/ci;I)V
    .locals 1

    invoke-direct {p0, p1, p3, p4}, Lcom/google/android/youtube/core/a/b;-><init>(Lcom/google/android/youtube/core/a/e;Lcom/google/android/youtube/core/a/g;I)V

    const-string v0, "inflater cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/cg;->c:Landroid/view/LayoutInflater;

    const-string v0, "clickListener cannot be null"

    invoke-static {p6, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/ch;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/cg;->e:Lcom/google/android/youtube/app/adapter/ch;

    iput-object p7, p0, Lcom/google/android/youtube/app/adapter/cg;->f:Lcom/google/android/youtube/app/adapter/ci;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/youtube/app/adapter/cg;->h:I

    iput p5, p0, Lcom/google/android/youtube/app/adapter/cg;->d:I

    iput p4, p0, Lcom/google/android/youtube/app/adapter/cg;->g:I

    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9

    const v3, 0x7f070028

    const/4 v2, 0x0

    if-eqz p2, :cond_2

    check-cast p2, Landroid/widget/LinearLayout;

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/youtube/app/adapter/cg;->b()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p2, v0}, Landroid/widget/LinearLayout;->setWeightSum(F)V

    invoke-virtual {p2, v3}, Landroid/widget/LinearLayout;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/view/View;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/adapter/cg;->b()I

    move-result v0

    new-array v0, v0, [Landroid/view/View;

    invoke-virtual {p2, v3, v0}, Landroid/widget/LinearLayout;->setTag(ILjava/lang/Object;)V

    :cond_0
    invoke-virtual {p2}, Landroid/widget/LinearLayout;->removeAllViews()V

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/app/adapter/cg;->e(I)I

    move-result v4

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/app/adapter/cg;->f(I)I

    move-result v1

    sub-int v5, v1, v4

    move v3, v2

    :goto_1
    if-ge v3, v5, :cond_5

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/cg;->a:Lcom/google/android/youtube/core/a/e;

    add-int v6, v4, v3

    aget-object v7, v0, v3

    invoke-virtual {v1, v6, v7, p2}, Lcom/google/android/youtube/core/a/e;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    new-instance v7, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v7, v2, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/high16 v1, 0x3f800000

    iput v1, v7, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    if-nez v3, :cond_3

    iget v1, p0, Lcom/google/android/youtube/app/adapter/cg;->i:I

    :goto_2
    iput v1, v7, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    iget v1, p0, Lcom/google/android/youtube/app/adapter/cg;->j:I

    iput v1, v7, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    iget v1, p0, Lcom/google/android/youtube/app/adapter/cg;->g:I

    add-int/lit8 v1, v1, -0x1

    if-ne v3, v1, :cond_4

    iget v1, p0, Lcom/google/android/youtube/app/adapter/cg;->k:I

    :goto_3
    iput v1, v7, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    iget v1, p0, Lcom/google/android/youtube/app/adapter/cg;->l:I

    iput v1, v7, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    invoke-virtual {v6, v7}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    add-int v1, v4, v3

    const v7, 0x7f070029

    iget v8, p0, Lcom/google/android/youtube/app/adapter/cg;->h:I

    add-int/2addr v1, v8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v6, v7, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    invoke-virtual {v6, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-virtual {v6, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/cg;->f:Lcom/google/android/youtube/app/adapter/ci;

    if-eqz v1, :cond_1

    invoke-virtual {v6, p0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    :cond_1
    const/4 v1, 0x1

    invoke-virtual {v6, v1}, Landroid/view/View;->setFocusable(Z)V

    aput-object v6, v0, v3

    invoke-virtual {p2, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cg;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f0400a9

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    move-object p2, v0

    goto/16 :goto_0

    :cond_3
    iget v1, p0, Lcom/google/android/youtube/app/adapter/cg;->d:I

    goto :goto_2

    :cond_4
    move v1, v2

    goto :goto_3

    :cond_5
    return-object p2
.end method

.method public final a(IIII)V
    .locals 0

    iput p1, p0, Lcom/google/android/youtube/app/adapter/cg;->i:I

    iput p2, p0, Lcom/google/android/youtube/app/adapter/cg;->j:I

    iput p3, p0, Lcom/google/android/youtube/app/adapter/cg;->k:I

    iput p4, p0, Lcom/google/android/youtube/app/adapter/cg;->l:I

    return-void
.end method

.method public final b(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cg;->a:Lcom/google/android/youtube/core/a/e;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/a/e;->b(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 3

    const v2, 0x7f07002a

    invoke-virtual {p1, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->isInTouchMode()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->b(Landroid/content/pm/PackageManager;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const v0, 0x7f070029

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p1, v2, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/cg;->e:Lcom/google/android/youtube/app/adapter/ch;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, p0, v0}, Lcom/google/android/youtube/app/adapter/ch;->a(Lcom/google/android/youtube/app/adapter/cg;I)V

    :cond_2
    return-void
.end method

.method public final onLongClick(Landroid/view/View;)Z
    .locals 4

    const v3, 0x7f07002a

    const/4 v2, 0x1

    invoke-virtual {p1, v3}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return v2

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p1, v3, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/cg;->f:Lcom/google/android/youtube/app/adapter/ci;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    goto :goto_0
.end method

.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2

    const v1, 0x7f07002a

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    const/4 v0, 0x0

    return v0

    :pswitch_1
    const v0, 0x7f070029

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x0

    invoke-virtual {p1, v1, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
