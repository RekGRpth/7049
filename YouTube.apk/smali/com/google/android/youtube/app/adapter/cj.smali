.class public final Lcom/google/android/youtube/app/adapter/cj;
.super Lcom/google/android/youtube/app/adapter/j;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private b:Landroid/graphics/Typeface;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/j;-><init>()V

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/cj;->a:Landroid/content/res/Resources;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/adapter/cj;)Landroid/graphics/Typeface;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cj;->b:Landroid/graphics/Typeface;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/adapter/cj;)Landroid/content/res/Resources;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cj;->a:Landroid/content/res/Resources;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/youtube/app/adapter/bl;
    .locals 1

    new-instance v0, Lcom/google/android/youtube/app/adapter/ck;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/youtube/app/adapter/ck;-><init>(Lcom/google/android/youtube/app/adapter/cj;Landroid/view/View;Landroid/view/ViewGroup;)V

    return-object v0
.end method

.method public final a(Landroid/graphics/Typeface;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/adapter/cj;->b:Landroid/graphics/Typeface;

    return-void
.end method
