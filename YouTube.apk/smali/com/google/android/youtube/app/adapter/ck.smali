.class final Lcom/google/android/youtube/app/adapter/ck;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/adapter/bl;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/adapter/cj;

.field private final b:Landroid/view/View;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/TextView;

.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/view/View;

.field private final g:Landroid/widget/TextView;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/adapter/cj;Landroid/view/View;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/youtube/app/adapter/ck;->a:Lcom/google/android/youtube/app/adapter/cj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x7f070090

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/ck;->b:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ck;->b:Landroid/view/View;

    const v1, 0x7f07002c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/ck;->c:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ck;->b:Landroid/view/View;

    const v1, 0x7f070058

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/ck;->d:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ck;->d:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/google/android/youtube/app/adapter/cj;->a(Lcom/google/android/youtube/app/adapter/cj;)Landroid/graphics/Typeface;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ck;->d:Landroid/widget/TextView;

    invoke-static {p1}, Lcom/google/android/youtube/app/adapter/cj;->a(Lcom/google/android/youtube/app/adapter/cj;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ck;->b:Landroid/view/View;

    const v1, 0x7f070059

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/ck;->e:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ck;->b:Landroid/view/View;

    const v1, 0x7f07005a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/ck;->g:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ck;->b:Landroid/view/View;

    const v1, 0x7f0700ce

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/ck;->f:Landroid/view/View;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/app/adapter/cj;Landroid/view/View;Landroid/view/ViewGroup;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/app/adapter/ck;-><init>(Lcom/google/android/youtube/app/adapter/cj;Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(ILjava/lang/Object;)Landroid/view/View;
    .locals 10

    const/4 v9, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    check-cast p2, Lcom/google/android/youtube/core/model/Video;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ck;->c:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ck;->c:Landroid/widget/TextView;

    iget-object v3, p2, Lcom/google/android/youtube/core/model/Video;->title:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ck;->d:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ck;->d:Landroid/widget/TextView;

    iget v3, p2, Lcom/google/android/youtube/core/model/Video;->duration:I

    invoke-static {v3}, Lcom/google/android/youtube/core/utils/Util;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ck;->e:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ck;->e:Landroid/widget/TextView;

    iget-object v3, p2, Lcom/google/android/youtube/core/model/Video;->ownerDisplayName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ck;->g:Landroid/widget/TextView;

    if-eqz v0, :cond_4

    iget-object v0, p2, Lcom/google/android/youtube/core/model/Video;->state:Lcom/google/android/youtube/core/model/Video$State;

    sget-object v3, Lcom/google/android/youtube/core/model/Video$State;->PLAYABLE:Lcom/google/android/youtube/core/model/Video$State;

    if-eq v0, v3, :cond_3

    iget-object v0, p2, Lcom/google/android/youtube/core/model/Video;->state:Lcom/google/android/youtube/core/model/Video$State;

    sget-object v3, Lcom/google/android/youtube/core/model/Video$State;->PROCESSING:Lcom/google/android/youtube/core/model/Video$State;

    if-ne v0, v3, :cond_6

    invoke-virtual {p2}, Lcom/google/android/youtube/core/model/Video;->isLive()Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_3
    move v0, v2

    :goto_0
    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ck;->g:Landroid/widget/TextView;

    sget-object v3, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v0, v3, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    iget-object v0, p2, Lcom/google/android/youtube/core/model/Video;->uploadedDate:Ljava/util/Date;

    iget-object v3, p0, Lcom/google/android/youtube/app/adapter/ck;->a:Lcom/google/android/youtube/app/adapter/cj;

    invoke-static {v3}, Lcom/google/android/youtube/app/adapter/cj;->b(Lcom/google/android/youtube/app/adapter/cj;)Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/ah;->a(Ljava/util/Date;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/youtube/app/adapter/ck;->g:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/android/youtube/app/adapter/ck;->a:Lcom/google/android/youtube/app/adapter/cj;

    invoke-static {v4}, Lcom/google/android/youtube/app/adapter/cj;->b(Lcom/google/android/youtube/app/adapter/cj;)Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b01e0

    new-array v6, v9, [Ljava/lang/Object;

    if-eqz v0, :cond_7

    :goto_1
    aput-object v0, v6, v1

    iget-wide v7, p2, Lcom/google/android/youtube/core/model/Video;->viewCount:J

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v6, v2

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ck;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setMaxLines(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ck;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMaxLines(I)V

    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ck;->f:Landroid/view/View;

    if-eqz v0, :cond_5

    iget-object v2, p0, Lcom/google/android/youtube/app/adapter/ck;->f:Landroid/view/View;

    iget-object v0, p2, Lcom/google/android/youtube/core/model/Video;->uploadedDate:Ljava/util/Date;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/util/Date;)Z

    move-result v0

    if-eqz v0, :cond_9

    move v0, v1

    :goto_3
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_5
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ck;->b:Landroid/view/View;

    return-object v0

    :cond_6
    move v0, v1

    goto :goto_0

    :cond_7
    const-string v0, ""

    goto :goto_1

    :cond_8
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ck;->g:Landroid/widget/TextView;

    sget-object v3, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v0, v3, v9}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ck;->g:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/youtube/app/adapter/ck;->a:Lcom/google/android/youtube/app/adapter/cj;

    invoke-static {v3}, Lcom/google/android/youtube/app/adapter/cj;->b(Lcom/google/android/youtube/app/adapter/cj;)Landroid/content/res/Resources;

    move-result-object v3

    iget-object v4, p2, Lcom/google/android/youtube/core/model/Video;->state:Lcom/google/android/youtube/core/model/Video$State;

    iget v4, v4, Lcom/google/android/youtube/core/model/Video$State;->explanationId:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ck;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMaxLines(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ck;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setMaxLines(I)V

    goto :goto_2

    :cond_9
    const/16 v0, 0x8

    goto :goto_3
.end method
