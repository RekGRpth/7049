.class public final Lcom/google/android/youtube/app/adapter/cl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/adapter/bl;
.implements Lcom/google/android/youtube/app/adapter/m;


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Lcom/google/android/youtube/app/adapter/k;

.field private c:Landroid/widget/ImageView;

.field private final d:Landroid/widget/ImageView;

.field private final e:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/adapter/k;Landroid/view/View;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/youtube/app/adapter/cl;->a:Landroid/view/View;

    iput-object p1, p0, Lcom/google/android/youtube/app/adapter/cl;->b:Lcom/google/android/youtube/app/adapter/k;

    const v0, 0x7f070099

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/cl;->c:Landroid/widget/ImageView;

    const v0, 0x7f070036

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/cl;->d:Landroid/widget/ImageView;

    const v0, 0x7f070056

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/cl;->e:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cl;->d:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cl;->e:Landroid/view/View;

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p1, p0}, Lcom/google/android/youtube/app/adapter/k;->a(Lcom/google/android/youtube/app/adapter/m;)V

    :cond_1
    return-void
.end method


# virtual methods
.method public final synthetic a(ILjava/lang/Object;)Landroid/view/View;
    .locals 4

    const/16 v3, 0x8

    const/4 v1, 0x0

    check-cast p2, Lcom/google/android/youtube/core/model/Video;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cl;->d:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cl;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cl;->e:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cl;->e:Landroid/view/View;

    const v2, 0x7f020068

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cl;->b:Lcom/google/android/youtube/app/adapter/k;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/app/adapter/k;->a(ILjava/lang/Object;)Landroid/view/View;

    iget-object v0, p2, Lcom/google/android/youtube/core/model/Video;->state:Lcom/google/android/youtube/core/model/Video$State;

    sget-object v2, Lcom/google/android/youtube/core/model/Video$State;->PLAYABLE:Lcom/google/android/youtube/core/model/Video$State;

    if-eq v0, v2, :cond_2

    iget-object v0, p2, Lcom/google/android/youtube/core/model/Video;->state:Lcom/google/android/youtube/core/model/Video$State;

    sget-object v2, Lcom/google/android/youtube/core/model/Video$State;->PROCESSING:Lcom/google/android/youtube/core/model/Video$State;

    if-ne v0, v2, :cond_4

    invoke-virtual {p2}, Lcom/google/android/youtube/core/model/Video;->isLive()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_2
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cl;->c:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cl;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cl;->a:Landroid/view/View;

    return-object v0

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cl;->c:Landroid/widget/ImageView;

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cl;->a:Landroid/view/View;

    const v2, 0x7f070057

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/cl;->c:Landroid/widget/ImageView;

    :cond_6
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cl;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cl;->d:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cl;->d:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cl;->e:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cl;->e:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_0
    return-void
.end method
