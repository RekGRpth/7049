.class public Lcom/google/android/youtube/app/adapter/cm;
.super Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/youtube/core/client/be;

.field private final b:Z


# direct methods
.method protected constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/utils/p;Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)V
    .locals 1

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/utils/p;Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/adapter/cm;->b:Z

    invoke-static {p2}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/client/be;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/cm;->a:Lcom/google/android/youtube/core/client/be;

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;Z)Lcom/google/android/youtube/app/adapter/cm;
    .locals 2

    const-string v0, "thumbnailSize cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/youtube/app/adapter/cm;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1, p2}, Lcom/google/android/youtube/app/adapter/cm;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/utils/p;Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/utils/p;Z)Lcom/google/android/youtube/app/adapter/cm;
    .locals 2

    const-string v0, "networkStatus cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/youtube/app/adapter/cm;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/google/android/youtube/app/adapter/cm;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/utils/p;Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)V

    return-object v0
.end method


# virtual methods
.method protected final bridge synthetic a(Ljava/lang/Object;)Landroid/net/Uri;
    .locals 1

    check-cast p1, Lcom/google/android/youtube/core/model/Video;

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->defaultThumbnailUri:Landroid/net/Uri;

    return-object v0
.end method

.method public final a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/youtube/app/adapter/bl;
    .locals 2

    new-instance v1, Lcom/google/android/youtube/app/adapter/cl;

    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;->a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/youtube/app/adapter/bl;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/k;

    invoke-direct {v1, v0, p1}, Lcom/google/android/youtube/app/adapter/cl;-><init>(Lcom/google/android/youtube/app/adapter/k;Landroid/view/View;)V

    return-object v1
.end method

.method protected final a(Landroid/net/Uri;Lcom/google/android/youtube/core/async/n;)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/adapter/cm;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cm;->a:Lcom/google/android/youtube/core/client/be;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/client/be;->b(Landroid/net/Uri;Lcom/google/android/youtube/core/async/n;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;->a(Landroid/net/Uri;Lcom/google/android/youtube/core/async/n;)V

    goto :goto_0
.end method

.method protected a(Lcom/google/android/youtube/core/model/Video;Landroid/view/View;Lcom/google/android/youtube/core/async/n;)V
    .locals 2

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->state:Lcom/google/android/youtube/core/model/Video$State;

    sget-object v1, Lcom/google/android/youtube/core/model/Video$State;->PLAYABLE:Lcom/google/android/youtube/core/model/Video$State;

    if-eq v0, v1, :cond_0

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->state:Lcom/google/android/youtube/core/model/Video$State;

    sget-object v1, Lcom/google/android/youtube/core/model/Video$State;->PROCESSING:Lcom/google/android/youtube/core/model/Video$State;

    if-ne v0, v1, :cond_1

    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/Video;->isLive()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_3

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->mqThumbnailUri:Landroid/net/Uri;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->mqThumbnailUri:Landroid/net/Uri;

    :goto_1
    const/4 v1, 0x0

    invoke-interface {p3, v0, v1}, Lcom/google/android/youtube/core/async/n;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_2
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->defaultThumbnailUri:Landroid/net/Uri;

    goto :goto_1

    :cond_3
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;->a(Ljava/lang/Object;Landroid/view/View;Lcom/google/android/youtube/core/async/n;)V

    goto :goto_2
.end method

.method protected bridge synthetic a(Ljava/lang/Object;Landroid/view/View;Lcom/google/android/youtube/core/async/n;)V
    .locals 0

    check-cast p1, Lcom/google/android/youtube/core/model/Video;

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/youtube/app/adapter/cm;->a(Lcom/google/android/youtube/core/model/Video;Landroid/view/View;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method

.method protected final bridge synthetic b(Ljava/lang/Object;)Landroid/net/Uri;
    .locals 1

    check-cast p1, Lcom/google/android/youtube/core/model/Video;

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->sdThumbnailUri:Landroid/net/Uri;

    return-object v0
.end method

.method protected final bridge synthetic c(Ljava/lang/Object;)Landroid/net/Uri;
    .locals 1

    check-cast p1, Lcom/google/android/youtube/core/model/Video;

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->hqThumbnailUri:Landroid/net/Uri;

    return-object v0
.end method

.method protected final bridge synthetic d(Ljava/lang/Object;)Landroid/net/Uri;
    .locals 1

    check-cast p1, Lcom/google/android/youtube/core/model/Video;

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->mqThumbnailUri:Landroid/net/Uri;

    return-object v0
.end method
