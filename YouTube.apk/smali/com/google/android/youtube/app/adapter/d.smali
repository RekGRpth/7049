.class final Lcom/google/android/youtube/app/adapter/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/adapter/bl;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/adapter/a;

.field private final b:Landroid/view/View;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/adapter/a;Landroid/view/View;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/youtube/app/adapter/d;->a:Lcom/google/android/youtube/app/adapter/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/youtube/app/adapter/d;->b:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/d;->b:Landroid/view/View;

    const v1, 0x7f07002c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/d;->c:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/d;->b:Landroid/view/View;

    const v1, 0x7f070058

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/d;->d:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public final synthetic a(ILjava/lang/Object;)Landroid/view/View;
    .locals 2

    check-cast p2, Lcom/google/android/youtube/core/model/MusicVideo;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/d;->c:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/d;->c:Landroid/widget/TextView;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/MusicVideo;->trackName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/d;->d:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/d;->d:Landroid/widget/TextView;

    iget v1, p2, Lcom/google/android/youtube/core/model/MusicVideo;->duration:I

    invoke-static {v1}, Lcom/google/android/youtube/core/utils/Util;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/d;->b:Landroid/view/View;

    return-object v0
.end method
