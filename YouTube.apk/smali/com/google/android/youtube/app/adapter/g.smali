.class final Lcom/google/android/youtube/app/adapter/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/adapter/f;

.field private final b:Lcom/google/android/youtube/core/async/n;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/adapter/f;Lcom/google/android/youtube/core/async/n;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/adapter/g;->a:Lcom/google/android/youtube/app/adapter/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/youtube/app/adapter/g;->b:Lcom/google/android/youtube/core/async/n;

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2

    const-string v0, "Error retrieving user profile"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/g;->b:Lcom/google/android/youtube/core/async/n;

    const/4 v1, 0x0

    invoke-interface {v0, v1, p2}, Lcom/google/android/youtube/core/async/n;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    check-cast p2, Lcom/google/android/youtube/core/model/UserProfile;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/g;->a:Lcom/google/android/youtube/app/adapter/f;

    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/f;->a(Lcom/google/android/youtube/app/adapter/f;)Lcom/google/android/youtube/core/client/be;

    move-result-object v0

    iget-object v1, p2, Lcom/google/android/youtube/core/model/UserProfile;->thumbnailUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/youtube/app/adapter/g;->b:Lcom/google/android/youtube/core/async/n;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/be;->d(Landroid/net/Uri;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method
