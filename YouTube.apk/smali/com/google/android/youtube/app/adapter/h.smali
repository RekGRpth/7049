.class public final Lcom/google/android/youtube/app/adapter/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/adapter/bv;


# instance fields
.field private final a:Ljava/util/WeakHashMap;

.field private final b:Ljava/util/Set;

.field private final c:Lcom/google/android/youtube/app/prefetch/d;

.field private final d:Z

.field private final e:Z

.field private final f:Z

.field private final g:Z

.field private final h:Z

.field private final i:Z


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/prefetch/d;ZIZ)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/app/adapter/h;->c:Lcom/google/android/youtube/app/prefetch/d;

    iput-boolean p2, p0, Lcom/google/android/youtube/app/adapter/h;->d:Z

    if-eqz p3, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Omit this renderer all together instead of using it with no badges"

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    and-int/lit8 v0, p3, 0x1

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/youtube/app/adapter/h;->f:Z

    and-int/lit8 v0, p3, 0x2

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/android/youtube/app/adapter/h;->g:Z

    and-int/lit8 v0, p3, 0x4

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/google/android/youtube/app/adapter/h;->h:Z

    and-int/lit8 v0, p3, 0x8

    if-eqz v0, :cond_4

    :goto_4
    iput-boolean v1, p0, Lcom/google/android/youtube/app/adapter/h;->i:Z

    iput-boolean p4, p0, Lcom/google/android/youtube/app/adapter/h;->e:Z

    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/h;->a:Ljava/util/WeakHashMap;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/h;->b:Ljava/util/Set;

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v1, v2

    goto :goto_4
.end method

.method static synthetic a(Lcom/google/android/youtube/app/adapter/h;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/adapter/h;->f:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/adapter/h;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/adapter/h;->g:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/adapter/h;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/adapter/h;->h:Z

    return v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/adapter/h;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/adapter/h;->i:Z

    return v0
.end method

.method static synthetic e(Lcom/google/android/youtube/app/adapter/h;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/adapter/h;->d:Z

    return v0
.end method

.method static synthetic f(Lcom/google/android/youtube/app/adapter/h;)Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/h;->b:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/youtube/app/adapter/h;)Lcom/google/android/youtube/app/prefetch/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/h;->c:Lcom/google/android/youtube/app/prefetch/d;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/youtube/app/adapter/h;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/adapter/h;->e:Z

    return v0
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/youtube/app/adapter/bl;
    .locals 3

    new-instance v0, Lcom/google/android/youtube/app/adapter/i;

    invoke-direct {v0, p0, p1}, Lcom/google/android/youtube/app/adapter/i;-><init>(Lcom/google/android/youtube/app/adapter/h;Landroid/view/View;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/h;->a:Ljava/util/WeakHashMap;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

.method public final a(Ljava/lang/Iterable;)V
    .locals 0

    return-void
.end method
