.class final Lcom/google/android/youtube/app/adapter/t;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/bk;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;

.field private final b:Lcom/google/android/youtube/app/adapter/z;

.field private final c:Lcom/google/android/youtube/core/model/UserProfile;

.field private final d:Z


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;Lcom/google/android/youtube/app/adapter/z;Lcom/google/android/youtube/core/model/UserProfile;Z)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/adapter/t;->a:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/youtube/app/adapter/t;->b:Lcom/google/android/youtube/app/adapter/z;

    iput-object p3, p0, Lcom/google/android/youtube/app/adapter/t;->c:Lcom/google/android/youtube/core/model/UserProfile;

    iput-boolean p4, p0, Lcom/google/android/youtube/app/adapter/t;->d:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;Lcom/google/android/youtube/app/adapter/z;Lcom/google/android/youtube/core/model/UserProfile;ZB)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/youtube/app/adapter/t;-><init>(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;Lcom/google/android/youtube/app/adapter/z;Lcom/google/android/youtube/core/model/UserProfile;Z)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 9

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/t;->a:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->i(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)Lcom/google/android/youtube/core/client/bc;

    move-result-object v6

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/t;->c:Lcom/google/android/youtube/core/model/UserProfile;

    iget-object v7, v0, Lcom/google/android/youtube/core/model/UserProfile;->uri:Landroid/net/Uri;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/t;->a:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->h(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)Landroid/app/Activity;

    move-result-object v8

    new-instance v0, Lcom/google/android/youtube/app/adapter/y;

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/t;->a:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;

    iget-object v3, p0, Lcom/google/android/youtube/app/adapter/t;->c:Lcom/google/android/youtube/core/model/UserProfile;

    iget-object v4, p0, Lcom/google/android/youtube/app/adapter/t;->b:Lcom/google/android/youtube/app/adapter/z;

    iget-boolean v5, p0, Lcom/google/android/youtube/app/adapter/t;->d:Z

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/adapter/y;-><init>(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/model/UserProfile;Lcom/google/android/youtube/app/adapter/z;Z)V

    invoke-static {v8, v0}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v0

    invoke-interface {v6, v7, v0}, Lcom/google/android/youtube/core/client/bc;->c(Landroid/net/Uri;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    const-string v0, "Ignoring click due to authentication error"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    return-void
.end method

.method public final g_()V
    .locals 1

    const-string v0, "Ignoring click due to not authenticated"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    return-void
.end method
