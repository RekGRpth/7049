.class public final Lcom/google/android/youtube/app/b/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/au;


# instance fields
.field private final a:I

.field private final b:I

.field private final c:Lcom/google/android/youtube/core/async/au;

.field private final d:Ljava/util/concurrent/ConcurrentMap;

.field private final e:Lcom/google/android/youtube/core/client/be;

.field private final f:Z


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/client/be;Ljava/util/concurrent/ConcurrentMap;IIZ)V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/au;

    iput-object v0, p0, Lcom/google/android/youtube/app/b/b;->c:Lcom/google/android/youtube/core/async/au;

    invoke-static {p2}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/client/be;

    iput-object v0, p0, Lcom/google/android/youtube/app/b/b;->e:Lcom/google/android/youtube/core/client/be;

    invoke-static {p3}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ConcurrentMap;

    iput-object v0, p0, Lcom/google/android/youtube/app/b/b;->d:Ljava/util/concurrent/ConcurrentMap;

    const-string v0, "minDesiredTeasers must be > 0"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    const-string v0, "maxTeasers must be > minDesiredTeasers"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    const/16 v0, 0x8

    iput v0, p0, Lcom/google/android/youtube/app/b/b;->a:I

    const/16 v0, 0x18

    iput v0, p0, Lcom/google/android/youtube/app/b/b;->b:I

    iput-boolean v1, p0, Lcom/google/android/youtube/app/b/b;->f:Z

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/b/b;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/b/b;->f:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/b/b;)Ljava/util/concurrent/ConcurrentMap;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/b/b;->d:Ljava/util/concurrent/ConcurrentMap;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/b/b;)I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/app/b/b;->b:I

    return v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/b/b;)I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/app/b/b;->a:I

    return v0
.end method

.method static synthetic e(Lcom/google/android/youtube/app/b/b;)Lcom/google/android/youtube/core/async/au;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/b/b;->c:Lcom/google/android/youtube/core/async/au;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/youtube/app/b/b;)Lcom/google/android/youtube/core/client/be;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/b/b;->e:Lcom/google/android/youtube/core/client/be;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/async/GDataRequest;Lcom/google/android/youtube/core/async/n;)V
    .locals 2

    invoke-static {}, Lcom/google/android/youtube/core/L;->e()V

    iget-object v0, p0, Lcom/google/android/youtube/app/b/b;->c:Lcom/google/android/youtube/core/async/au;

    new-instance v1, Lcom/google/android/youtube/app/b/c;

    invoke-direct {v1, p0, p2}, Lcom/google/android/youtube/app/b/c;-><init>(Lcom/google/android/youtube/app/b/b;Lcom/google/android/youtube/core/async/n;)V

    invoke-interface {v0, p1, v1}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V
    .locals 0

    check-cast p1, Lcom/google/android/youtube/core/async/GDataRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/app/b/b;->a(Lcom/google/android/youtube/core/async/GDataRequest;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method
