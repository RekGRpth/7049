.class public final Lcom/google/android/youtube/app/compat/o;
.super Landroid/app/Dialog;
.source "SourceFile"


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Ljava/util/List;

.field private final c:Landroid/widget/LinearLayout;

.field private final d:Landroid/widget/ListView;

.field private final e:Lcom/google/android/youtube/app/compat/q;


# direct methods
.method private constructor <init>(Landroid/app/Activity;)V
    .locals 3

    const v0, 0x7f0d0037

    invoke-direct {p0, p1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    iput-object p1, p0, Lcom/google/android/youtube/app/compat/o;->a:Landroid/app/Activity;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/o;->b:Ljava/util/List;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040091

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/o;->c:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/o;->c:Landroid/widget/LinearLayout;

    const v1, 0x7f07013c

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/o;->d:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/o;->d:Landroid/widget/ListView;

    new-instance v1, Lcom/google/android/youtube/app/compat/p;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/compat/p;-><init>(Lcom/google/android/youtube/app/compat/o;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    new-instance v0, Lcom/google/android/youtube/app/compat/q;

    iget-object v1, p0, Lcom/google/android/youtube/app/compat/o;->b:Ljava/util/List;

    invoke-direct {v0, p1, v1}, Lcom/google/android/youtube/app/compat/q;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/o;->e:Lcom/google/android/youtube/app/compat/q;

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/o;->d:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/youtube/app/compat/o;->e:Lcom/google/android/youtube/app/compat/q;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/o;->c:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/compat/o;->setContentView(Landroid/view/View;)V

    return-void
.end method

.method public static a(Landroid/app/Activity;)Lcom/google/android/youtube/app/compat/o;
    .locals 1

    new-instance v0, Lcom/google/android/youtube/app/compat/o;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/compat/o;-><init>(Landroid/app/Activity;)V

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/compat/o;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/o;->b:Ljava/util/List;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/compat/o;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/o;->a:Landroid/app/Activity;

    return-object v0
.end method

.method private b()V
    .locals 7

    const/4 v6, 0x1

    invoke-virtual {p0}, Lcom/google/android/youtube/app/compat/o;->getWindow()Landroid/view/Window;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/compat/o;->a:Landroid/app/Activity;

    const-string v0, "window"

    invoke-virtual {v2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    new-instance v3, Landroid/util/DisplayMetrics;

    invoke-direct {v3}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v6, :cond_0

    iget v0, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    :goto_0
    int-to-double v2, v0

    const-wide v4, 0x3fe999999999999aL

    mul-double/2addr v2, v4

    double-to-int v0, v2

    const/4 v2, -0x2

    invoke-virtual {v1, v0, v2}, Landroid/view/Window;->setLayout(II)V

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/o;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v6, :cond_1

    invoke-virtual {p0}, Lcom/google/android/youtube/app/compat/o;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x50

    invoke-virtual {v0, v1}, Landroid/view/Window;->setGravity(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/o;->d:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, 0x0

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    :goto_1
    return-void

    :cond_0
    iget v0, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/youtube/app/compat/o;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x53

    invoke-virtual {v0, v1}, Landroid/view/Window;->setGravity(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/o;->d:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v1, p0, Lcom/google/android/youtube/app/compat/o;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    const-string v2, "metrics cannot be null"

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/high16 v2, 0x41200000

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v1, v2

    float-to-double v1, v1

    const-wide/high16 v3, 0x3fe0000000000000L

    add-double/2addr v1, v3

    double-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/compat/o;->b()V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/compat/m;)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/youtube/app/compat/o;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/o;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/o;->b:Ljava/util/List;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/compat/o;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/google/android/youtube/app/compat/c;->a(Landroid/content/Context;Lcom/google/android/youtube/app/compat/m;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/o;->e:Lcom/google/android/youtube/app/compat/q;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/compat/q;->notifyDataSetChanged()V

    return-void
.end method

.method public final onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    const/16 v0, 0x52

    if-ne p1, v0, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/compat/o;->dismiss()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method
