.class public Lcom/google/android/youtube/app/compat/z;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/compat/t;


# instance fields
.field protected final a:Landroid/view/MenuItem;

.field private final b:Landroid/content/Context;

.field private c:I

.field private d:Lcom/google/android/youtube/app/compat/ab;

.field private e:Landroid/view/View;

.field private f:Lcom/google/android/youtube/app/compat/v;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/MenuItem;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "context cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/z;->b:Landroid/content/Context;

    const-string v0, "target cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/z;->a:Landroid/view/MenuItem;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/youtube/app/compat/z;->c:I

    return-void
.end method


# virtual methods
.method public final a()Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/z;->a:Landroid/view/MenuItem;

    return-object v0
.end method

.method public final a(I)Lcom/google/android/youtube/app/compat/t;
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/z;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    new-instance v1, Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/google/android/youtube/app/compat/z;->b:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/z;->e:Landroid/view/View;

    return-object p0
.end method

.method public a(Lcom/google/android/youtube/app/compat/u;)Lcom/google/android/youtube/app/compat/t;
    .locals 0

    return-object p0
.end method

.method public final a(Lcom/google/android/youtube/app/compat/v;)Lcom/google/android/youtube/app/compat/t;
    .locals 2

    iput-object p1, p0, Lcom/google/android/youtube/app/compat/z;->f:Lcom/google/android/youtube/app/compat/v;

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/z;->a:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    :goto_0
    return-object p0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/z;->a:Landroid/view/MenuItem;

    new-instance v1, Lcom/google/android/youtube/app/compat/aa;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/app/compat/aa;-><init>(Lcom/google/android/youtube/app/compat/z;Lcom/google/android/youtube/app/compat/v;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public final a(Z)Lcom/google/android/youtube/app/compat/t;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/z;->a:Landroid/view/MenuItem;

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    return-object p0
.end method

.method public final a(Lcom/google/android/youtube/app/compat/ab;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/compat/z;->d:Lcom/google/android/youtube/app/compat/ab;

    return-void
.end method

.method final a(Lcom/google/android/youtube/app/compat/t;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/z;->f:Lcom/google/android/youtube/app/compat/v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/z;->f:Lcom/google/android/youtube/app/compat/v;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/app/compat/v;->a(Lcom/google/android/youtube/app/compat/t;)Z

    :cond_0
    return-void
.end method

.method public final b(Z)Lcom/google/android/youtube/app/compat/t;
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/z;->a:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->isVisible()Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/app/compat/z;->a:Landroid/view/MenuItem;

    invoke-interface {v1, p1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/z;->d:Lcom/google/android/youtube/app/compat/ab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/z;->d:Lcom/google/android/youtube/app/compat/ab;

    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/ab;->d()V

    :cond_0
    return-object p0
.end method

.method public b(I)V
    .locals 0

    iput p1, p0, Lcom/google/android/youtube/app/compat/z;->c:I

    return-void
.end method

.method public b()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final c(I)Lcom/google/android/youtube/app/compat/t;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/z;->a:Landroid/view/MenuItem;

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    return-object p0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public d()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/z;->e:Landroid/view/View;

    return-object v0
.end method

.method public final e()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/z;->a:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/z;->a:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    return v0
.end method

.method public final g()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/z;->a:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final h()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/z;->a:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->isVisible()Z

    move-result v0

    return v0
.end method

.method public final i()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/app/compat/z;->c:I

    return v0
.end method
