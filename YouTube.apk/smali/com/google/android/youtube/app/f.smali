.class final Lcom/google/android/youtube/app/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/e;

.field private final b:Lcom/google/android/youtube/core/async/GDataRequest;

.field private final c:Lcom/google/android/youtube/core/async/n;

.field private final d:Ljava/util/HashMap;

.field private e:I

.field private final f:Ljava/util/ArrayList;

.field private g:Ljava/lang/Exception;

.field private h:Ljava/lang/Exception;

.field private final i:Ljava/util/ArrayList;

.field private final j:I


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/e;Lcom/google/android/youtube/core/async/n;Lcom/google/android/youtube/core/async/GDataRequest;Ljava/util/HashMap;I)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/youtube/app/f;->a:Lcom/google/android/youtube/app/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/n;

    iput-object v0, p0, Lcom/google/android/youtube/app/f;->c:Lcom/google/android/youtube/core/async/n;

    invoke-static {p3}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/GDataRequest;

    iput-object v0, p0, Lcom/google/android/youtube/app/f;->b:Lcom/google/android/youtube/core/async/GDataRequest;

    invoke-static {p4}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    iput-object v0, p0, Lcom/google/android/youtube/app/f;->d:Ljava/util/HashMap;

    iput p5, p0, Lcom/google/android/youtube/app/f;->j:I

    invoke-virtual {p4}, Ljava/util/HashMap;->size()I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/f;->e:I

    new-instance v0, Ljava/util/ArrayList;

    iget v1, p0, Lcom/google/android/youtube/app/f;->e:I

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/youtube/app/f;->f:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/f;->i:Ljava/util/ArrayList;

    return-void
.end method

.method private a()V
    .locals 9

    const/4 v4, 0x0

    const/4 v2, 0x0

    iget v0, p0, Lcom/google/android/youtube/app/f;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/app/f;->e:I

    iget v0, p0, Lcom/google/android/youtube/app/f;->e:I

    if-nez v0, :cond_3

    iget-object v5, p0, Lcom/google/android/youtube/app/f;->f:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/google/android/youtube/app/f;->i:Ljava/util/ArrayList;

    move v3, v2

    move v1, v2

    :goto_0
    if-nez v1, :cond_2

    const/4 v0, 0x1

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v1, v0

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v8

    if-ge v3, v8, :cond_6

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Video;

    invoke-interface {v6, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    move v0, v2

    :goto_2
    move v1, v0

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/app/f;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/google/android/youtube/app/f;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4

    iget-object v7, p0, Lcom/google/android/youtube/app/f;->c:Lcom/google/android/youtube/core/async/n;

    iget-object v8, p0, Lcom/google/android/youtube/app/f;->b:Lcom/google/android/youtube/core/async/GDataRequest;

    new-instance v0, Lcom/google/android/youtube/core/model/Page;

    iget-object v1, p0, Lcom/google/android/youtube/app/f;->i:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/youtube/app/f;->i:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    iget v3, p0, Lcom/google/android/youtube/app/f;->j:I

    iget-object v6, p0, Lcom/google/android/youtube/app/f;->i:Ljava/util/ArrayList;

    move-object v5, v4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/core/model/Page;-><init>(IIILandroid/net/Uri;Landroid/net/Uri;Ljava/util/List;)V

    invoke-interface {v7, v8, v0}, Lcom/google/android/youtube/core/async/n;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/f;->g:Ljava/lang/Exception;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/app/f;->c:Lcom/google/android/youtube/core/async/n;

    iget-object v1, p0, Lcom/google/android/youtube/app/f;->b:Lcom/google/android/youtube/core/async/GDataRequest;

    new-instance v2, Ljava/lang/Exception;

    const-string v3, "some feed sources failed"

    iget-object v4, p0, Lcom/google/android/youtube/app/f;->g:Ljava/lang/Exception;

    invoke-direct {v2, v3, v4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/async/n;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    :cond_3
    :goto_3
    return-void

    :cond_4
    iget-object v1, p0, Lcom/google/android/youtube/app/f;->c:Lcom/google/android/youtube/core/async/n;

    iget-object v2, p0, Lcom/google/android/youtube/app/f;->b:Lcom/google/android/youtube/core/async/GDataRequest;

    new-instance v3, Ljava/lang/Exception;

    const-string v4, "all feed sources failed"

    iget-object v0, p0, Lcom/google/android/youtube/app/f;->g:Ljava/lang/Exception;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/youtube/app/f;->g:Ljava/lang/Exception;

    :goto_4
    invoke-direct {v3, v4, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-interface {v1, v2, v3}, Lcom/google/android/youtube/core/async/n;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_3

    :cond_5
    iget-object v0, p0, Lcom/google/android/youtube/app/f;->h:Ljava/lang/Exception;

    goto :goto_4

    :cond_6
    move v0, v1

    goto :goto_2
.end method

.method private declared-synchronized a(Lcom/google/android/youtube/core/async/GDataRequest;Lcom/google/android/youtube/core/model/Page;)V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/app/f;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/aj;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/Page;->entries:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/model/Video;

    iget-object v3, p0, Lcom/google/android/youtube/app/f;->a:Lcom/google/android/youtube/app/e;

    invoke-static {v3}, Lcom/google/android/youtube/app/e;->a(Lcom/google/android/youtube/app/e;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v3

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-virtual {v3, v1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/youtube/app/f;->f:Ljava/util/ArrayList;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/Page;->entries:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lcom/google/android/youtube/app/f;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method

.method private declared-synchronized a(Ljava/lang/Exception;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    instance-of v1, p1, Lcom/google/android/youtube/core/async/GDataResponseException;

    if-eqz v1, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/android/youtube/core/async/GDataResponseException;

    move-object v1, v0

    invoke-virtual {v1}, Lcom/google/android/youtube/core/async/GDataResponseException;->containsYouTubeSignupRequiredError()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onError in the feed: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " but it\'s youtube_signup_required (or equivalent), ignoring"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    iput-object p1, p0, Lcom/google/android/youtube/app/f;->h:Ljava/lang/Exception;

    :goto_0
    invoke-direct {p0}, Lcom/google/android/youtube/app/f;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onError in the feed: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    iput-object p1, p0, Lcom/google/android/youtube/app/f;->g:Ljava/lang/Exception;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0

    invoke-direct {p0, p2}, Lcom/google/android/youtube/app/f;->a(Ljava/lang/Exception;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/google/android/youtube/core/async/GDataRequest;

    check-cast p2, Lcom/google/android/youtube/core/model/Page;

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/app/f;->a(Lcom/google/android/youtube/core/async/GDataRequest;Lcom/google/android/youtube/core/model/Page;)V

    return-void
.end method
