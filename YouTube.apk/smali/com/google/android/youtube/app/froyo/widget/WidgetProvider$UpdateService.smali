.class public Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;
.super Landroid/app/Service;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/bk;
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field private a:Landroid/appwidget/AppWidgetManager;

.field private b:Lcom/google/android/youtube/app/froyo/widget/c;

.field private c:Landroid/content/ComponentName;

.field private d:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private e:Lcom/google/android/youtube/core/async/GDataRequestFactory;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;->b:Lcom/google/android/youtube/app/froyo/widget/c;

    iget-object v1, p0, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;->e:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    invoke-static {}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->a()Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lcom/google/android/youtube/app/froyo/widget/c;->a(Lcom/google/android/youtube/core/async/GDataRequest;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;->b:Lcom/google/android/youtube/app/froyo/widget/c;

    iget-object v1, p0, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;->e:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    invoke-virtual {v1, p1}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->a(Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lcom/google/android/youtube/app/froyo/widget/c;->a(Lcom/google/android/youtube/core/async/GDataRequest;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 3

    const-string v0, "Widget error"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;->a:Landroid/appwidget/AppWidgetManager;

    iget-object v1, p0, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;->c:Landroid/content/ComponentName;

    invoke-static {p0}, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider;->c(Landroid/content/Context;)Landroid/widget/RemoteViews;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(Landroid/content/ComponentName;Landroid/widget/RemoteViews;)V

    invoke-static {p0}, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider;->d(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;->stopSelf()V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    check-cast p2, Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;->a:Landroid/appwidget/AppWidgetManager;

    iget-object v1, p0, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;->c:Landroid/content/ComponentName;

    invoke-static {p0}, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider;->c(Landroid/content/Context;)Landroid/widget/RemoteViews;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(Landroid/content/ComponentName;Landroid/widget/RemoteViews;)V

    invoke-static {p0}, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider;->d(Landroid/content/Context;)V

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;->stopSelf()V

    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;->a:Landroid/appwidget/AppWidgetManager;

    iget-object v1, p0, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;->c:Landroid/content/ComponentName;

    invoke-static {p0, p2}, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider;->a(Landroid/content/Context;Ljava/util/List;)Landroid/widget/RemoteViews;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(Landroid/content/ComponentName;Landroid/widget/RemoteViews;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;->a()V

    return-void
.end method

.method public final g_()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;->a()V

    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 7

    invoke-virtual {p0}, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;->getApplication()Landroid/app/Application;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->U()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;->d:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->a()Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/client/bc;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;->e:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->F()Ljava/lang/String;

    move-result-object v3

    new-instance v1, Lcom/google/android/youtube/app/e;

    invoke-direct {v1, v0, v3}, Lcom/google/android/youtube/app/e;-><init>(Lcom/google/android/youtube/core/client/bc;Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/youtube/app/froyo/widget/c;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->e_()Lcom/google/android/youtube/core/client/be;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/youtube/app/e;->a()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v3

    const/16 v4, 0x8

    const/16 v5, 0xf

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/app/froyo/widget/c;-><init>(Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/client/be;Ljava/util/concurrent/ConcurrentMap;IIZ)V

    iput-object v0, p0, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;->b:Lcom/google/android/youtube/app/froyo/widget/c;

    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;->a:Landroid/appwidget/AppWidgetManager;

    new-instance v0, Landroid/content/ComponentName;

    const-class v1, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider;

    invoke-direct {v0, p0, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;->c:Landroid/content/ComponentName;

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;->a:Landroid/appwidget/AppWidgetManager;

    iget-object v1, p0, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;->c:Landroid/content/ComponentName;

    invoke-static {p0}, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider;->b(Landroid/content/Context;)Landroid/widget/RemoteViews;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(Landroid/content/ComponentName;Landroid/widget/RemoteViews;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;->d:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bk;)V

    const/4 v0, 0x1

    return v0
.end method
