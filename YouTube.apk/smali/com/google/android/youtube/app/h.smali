.class final Lcom/google/android/youtube/app/h;
.super Lcom/google/android/youtube/core/client/at;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/YouTubeApplication;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/YouTubeApplication;Landroid/content/Context;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/utils/e;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/c;Lcom/google/android/youtube/core/client/StatParams;)V
    .locals 10

    iput-object p1, p0, Lcom/google/android/youtube/app/h;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    move-object/from16 v8, p9

    move-object/from16 v9, p10

    invoke-direct/range {v0 .. v9}, Lcom/google/android/youtube/core/client/at;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/utils/e;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/c;Lcom/google/android/youtube/core/client/StatParams;)V

    return-void
.end method


# virtual methods
.method protected final a()Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/h;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->S()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "dev_retention_enabled"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method
