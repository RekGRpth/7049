.class public Lcom/google/android/youtube/app/honeycomb/SettingsActivity;
.super Landroid/preference/PreferenceActivity;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/compat/j;


# instance fields
.field private a:Lcom/google/android/youtube/app/YouTubeApplication;

.field private b:Lcom/google/android/youtube/app/ui/da;

.field private c:Lcom/google/android/youtube/app/honeycomb/SettingsActivity$PrefetchPrefsFragment;

.field private d:Lcom/google/android/youtube/app/honeycomb/l;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/SettingsActivity;)Lcom/google/android/youtube/app/YouTubeApplication;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivity;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    return-object v0
.end method


# virtual methods
.method protected final a()Lcom/google/android/youtube/app/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivity;->d:Lcom/google/android/youtube/app/honeycomb/l;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/l;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/honeycomb/l;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivity;->d:Lcom/google/android/youtube/app/honeycomb/l;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivity;->d:Lcom/google/android/youtube/app/honeycomb/l;

    return-object v0
.end method

.method public final a(Lcom/google/android/youtube/app/honeycomb/SettingsActivity$PrefetchPrefsFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivity;->c:Lcom/google/android/youtube/app/honeycomb/SettingsActivity$PrefetchPrefsFragment;

    return-void
.end method

.method public final l()Lcom/google/android/youtube/app/compat/SupportActionBar;
    .locals 3

    invoke-static {p0}, Lcom/google/android/youtube/app/compat/SupportActionBar;->b(Landroid/app/Activity;)Lcom/google/android/youtube/app/compat/SupportActionBar;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02005a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(Landroid/graphics/drawable/Drawable;)V

    return-object v0
.end method

.method public onBuildHeaders(Ljava/util/List;)V
    .locals 4

    const v0, 0x7f060004

    invoke-virtual {p0, v0, p1}, Lcom/google/android/youtube/app/honeycomb/SettingsActivity;->loadHeadersFromResource(ILjava/util/List;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivity;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivity;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->n()Lcom/google/android/youtube/app/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/k;->m()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    iget-object v2, v0, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    const-class v3, Lcom/google/android/youtube/app/honeycomb/SettingsActivity$PrefetchPrefsFragment;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    const/16 v1, 0xc

    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(Landroid/app/Activity;)Lcom/google/android/youtube/app/compat/SupportActionBar;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(II)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivity;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    new-instance v0, Lcom/google/android/youtube/app/ui/da;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/ui/da;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivity;->b:Lcom/google/android/youtube/app/ui/da;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivity;->a()Lcom/google/android/youtube/app/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/d;->b()V

    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4

    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    :sswitch_0
    new-instance v0, Lcom/google/android/youtube/core/ui/w;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/core/ui/w;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0b00ce

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/w;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    new-instance v2, Lcom/google/android/youtube/app/honeycomb/r;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/app/honeycomb/r;-><init>(Lcom/google/android/youtube/app/honeycomb/SettingsActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :sswitch_1
    new-instance v0, Lcom/google/android/youtube/core/ui/w;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/core/ui/w;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0b0142

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/w;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f100001

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivity;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->V()Lcom/google/android/youtube/core/utils/SafeSearch;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/youtube/core/utils/SafeSearch;->a()Lcom/google/android/youtube/core/utils/SafeSearch$SafeSearchMode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/youtube/core/utils/SafeSearch$SafeSearchMode;->ordinal()I

    move-result v2

    new-instance v3, Lcom/google/android/youtube/app/honeycomb/s;

    invoke-direct {v3, p0}, Lcom/google/android/youtube/app/honeycomb/s;-><init>(Lcom/google/android/youtube/app/honeycomb/SettingsActivity;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(IILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivity;->b:Lcom/google/android/youtube/app/ui/da;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/da;->a()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivity;->c:Lcom/google/android/youtube/app/honeycomb/SettingsActivity$PrefetchPrefsFragment;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivity$PrefetchPrefsFragment;->a()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x3ea -> :sswitch_0
        0x3f6 -> :sswitch_1
        0x3fd -> :sswitch_2
        0x401 -> :sswitch_3
    .end sparse-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivity;->finish()V

    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V
    .locals 1

    const/16 v0, 0x401

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivity;->c:Lcom/google/android/youtube/app/honeycomb/SettingsActivity$PrefetchPrefsFragment;

    invoke-virtual {v0, p2, p3}, Lcom/google/android/youtube/app/honeycomb/SettingsActivity$PrefetchPrefsFragment;->a(Landroid/app/Dialog;Landroid/os/Bundle;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/preference/PreferenceActivity;->onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivity;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->i()Lcom/google/android/youtube/core/Analytics;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;)V

    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected onStart()V
    .locals 1

    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onStart()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivity;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->y()Lcom/google/android/youtube/core/client/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/client/at;->e()V

    return-void
.end method
