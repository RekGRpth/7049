.class public Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;
.super Landroid/preference/PreferenceActivity;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/compat/i;


# instance fields
.field private a:Lcom/google/android/youtube/app/YouTubeApplication;

.field private b:Lcom/google/android/youtube/app/honeycomb/p;

.field private c:Lcom/google/android/youtube/app/honeycomb/l;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;)Lcom/google/android/youtube/app/YouTubeApplication;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    return-object v0
.end method


# virtual methods
.method public final i()Z
    .locals 1

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/l;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/honeycomb/l;-><init>(Landroid/app/Activity;)V

    invoke-interface {v0}, Lcom/google/android/youtube/app/d;->c()V

    const/4 v0, 0x1

    return v0
.end method

.method public final j()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final k()I
    .locals 1

    const v0, 0x102000a

    return v0
.end method

.method public final l_()V
    .locals 0

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 9

    const/4 v1, 0x4

    const/4 v4, -0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f040093

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->setContentView(I)V

    invoke-static {p0}, Lcom/google/android/youtube/app/compat/SupportActionBar;->b(Landroid/app/Activity;)Lcom/google/android/youtube/app/compat/SupportActionBar;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/compat/SupportActionBar;->b(I)V

    invoke-virtual {v0, v1, v1}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(II)V

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/l;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/honeycomb/l;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->c:Lcom/google/android/youtube/app/honeycomb/l;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->c:Lcom/google/android/youtube/app/honeycomb/l;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/l;->b()V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    const-string v1, "youtube"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceManager;->setSharedPreferencesName(Ljava/lang/String;)V

    const v0, 0x7f06000a

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->i()Lcom/google/android/youtube/core/Analytics;

    const-string v0, "version"

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->X()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/e;

    const-string v0, "safe_search"

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->V()Lcom/google/android/youtube/core/utils/SafeSearch;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/SafeSearch;->b()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->n()Lcom/google/android/youtube/app/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/k;->m()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "prefetch_category"

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_0
    const-string v0, "general_category"

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-static {v1}, Lcom/google/android/youtube/core/utils/n;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "default_hq"

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->S()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "default_hq"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "default_hq"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    :cond_1
    const-string v0, "country"

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    sget-object v1, Lcom/google/android/youtube/core/async/GDataRequestFactory;->t:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    new-instance v6, Ljava/util/Locale;

    const-string v7, ""

    invoke-direct {v6, v7, v1}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v1

    new-instance v3, Lcom/google/android/youtube/app/honeycomb/u;

    invoke-direct {v3, p0, v1}, Lcom/google/android/youtube/app/honeycomb/u;-><init>(Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;Ljava/text/Collator;)V

    invoke-static {v5, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v6, v1, [Ljava/lang/CharSequence;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v7, v1, [Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->F()Ljava/lang/String;

    move-result-object v8

    move v3, v4

    :goto_2
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v2, v1, :cond_4

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getDisplayCountry()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v2

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v7, v2

    aget-object v1, v7, v2

    invoke-virtual {v1, v8}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    move v1, v2

    :goto_3
    add-int/lit8 v2, v2, 0x1

    move v3, v1

    goto :goto_2

    :cond_4
    invoke-virtual {v0, v6}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v7}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    if-eq v3, v4, :cond_5

    invoke-virtual {v0, v3}, Landroid/preference/ListPreference;->setValueIndex(I)V

    :cond_5
    new-instance v2, Lcom/google/android/youtube/app/honeycomb/p;

    const-string v0, "prefetch_subscriptions"

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    const-string v1, "prefetch_watch_later"

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    invoke-direct {v2, p0, v0, v1}, Lcom/google/android/youtube/app/honeycomb/p;-><init>(Landroid/app/Activity;Landroid/preference/CheckBoxPreference;Landroid/preference/CheckBoxPreference;)V

    iput-object v2, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->b:Lcom/google/android/youtube/app/honeycomb/p;

    return-void

    :cond_6
    move v1, v3

    goto :goto_3
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4

    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    :sswitch_0
    new-instance v0, Lcom/google/android/youtube/core/ui/w;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/core/ui/w;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0b00ce

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/w;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    new-instance v2, Lcom/google/android/youtube/app/honeycomb/v;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/app/honeycomb/v;-><init>(Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :sswitch_1
    new-instance v0, Lcom/google/android/youtube/core/ui/w;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/core/ui/w;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0b0142

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/w;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f100001

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->V()Lcom/google/android/youtube/core/utils/SafeSearch;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/youtube/core/utils/SafeSearch;->a()Lcom/google/android/youtube/core/utils/SafeSearch$SafeSearchMode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/youtube/core/utils/SafeSearch$SafeSearchMode;->ordinal()I

    move-result v2

    new-instance v3, Lcom/google/android/youtube/app/honeycomb/w;

    invoke-direct {v3, p0}, Lcom/google/android/youtube/app/honeycomb/w;-><init>(Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(IILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->b:Lcom/google/android/youtube/app/honeycomb/p;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/p;->a()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x3ea -> :sswitch_0
        0x3f6 -> :sswitch_1
        0x401 -> :sswitch_2
    .end sparse-switch
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v3, 0x0

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    const-string v2, "clear_history"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x3ea

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->showDialog(I)V

    :goto_0
    return v0

    :cond_0
    const-string v2, "safe_search"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x3f6

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->showDialog(I)V

    goto :goto_0

    :cond_1
    const-string v0, "mobile_terms"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x7f0b0221

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/utils/m;->a(Landroid/app/Activity;Landroid/net/Uri;)V

    :cond_2
    :goto_1
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceActivity;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    goto :goto_0

    :cond_3
    const-string v0, "youtube_terms"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const v0, 0x7f0b0222

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/utils/m;->a(Landroid/app/Activity;Landroid/net/Uri;)V

    goto :goto_1

    :cond_4
    const-string v0, "mobile_privacy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const v0, 0x7f0b0223

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/utils/m;->a(Landroid/app/Activity;Landroid/net/Uri;)V

    goto :goto_1

    :cond_5
    const-string v0, "youtube_privacy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const v0, 0x7f0b0224

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/utils/m;->a(Landroid/app/Activity;Landroid/net/Uri;)V

    goto :goto_1

    :cond_6
    const-string v0, "open_source_licenses"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-static {p0}, Lcom/google/android/youtube/core/LicensesActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    :cond_7
    const-string v0, "feedback"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->X()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0b0220

    invoke-static {p0, v0, v1}, Lcom/google/android/youtube/core/utils/m;->a(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/utils/m;->a(Landroid/app/Activity;Landroid/net/Uri;)V

    goto :goto_1

    :cond_8
    const-string v0, "help"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    const v0, 0x7f0b021e

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/utils/m;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/utils/m;->a(Landroid/app/Activity;Landroid/net/Uri;)V

    goto/16 :goto_1

    :cond_9
    const-string v0, "dev_retention_enabled"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    const-string v0, "dev_retention_enabled"

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->y()Lcom/google/android/youtube/core/client/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/client/at;->b()V

    goto/16 :goto_1

    :cond_a
    const-string v0, "pair_with_youtube_tv"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->c:Lcom/google/android/youtube/app/honeycomb/l;

    const-string v1, ""

    invoke-virtual {v0, v1, v3}, Lcom/google/android/youtube/app/honeycomb/l;->a(Ljava/lang/String;I)V

    const-string v0, ""

    invoke-static {p0, v0, v3}, Lcom/google/android/youtube/app/honeycomb/phone/ScreenPairingActivity;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    :cond_b
    const-string v0, "edit_tvs"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->c:Lcom/google/android/youtube/app/honeycomb/l;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/l;->j()V

    goto/16 :goto_1
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V
    .locals 1

    const/16 v0, 0x401

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->b:Lcom/google/android/youtube/app/honeycomb/p;

    invoke-virtual {v0, p3}, Lcom/google/android/youtube/app/honeycomb/p;->a(Landroid/os/Bundle;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/preference/PreferenceActivity;->onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->i()Lcom/google/android/youtube/core/Analytics;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;)V

    return-void
.end method
