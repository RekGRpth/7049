.class public final Lcom/google/android/youtube/app/honeycomb/Shell;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Z

.field private static b:Z

.field private static c:Z

.field private static d:J

.field private static e:J


# direct methods
.method static synthetic a()J
    .locals 2

    sget-wide v0, Lcom/google/android/youtube/app/honeycomb/Shell;->d:J

    return-wide v0
.end method

.method static synthetic a(J)J
    .locals 2

    const-wide v0, 0x7fffffffffffffffL

    sput-wide v0, Lcom/google/android/youtube/app/honeycomb/Shell;->d:J

    return-wide v0
.end method

.method public static a(Lcom/google/android/youtube/app/YouTubeApplication;Lcom/google/android/youtube/app/k;Landroid/content/SharedPreferences;)V
    .locals 8

    const/4 v2, 0x0

    const/4 v1, 0x1

    sget-boolean v0, Lcom/google/android/youtube/app/honeycomb/Shell;->a:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/youtube/app/YouTubeApplication;->Z()I

    move-result v3

    invoke-virtual {p1}, Lcom/google/android/youtube/app/k;->a()I

    move-result v4

    invoke-virtual {p1}, Lcom/google/android/youtube/app/k;->b()I

    move-result v5

    invoke-virtual {p1}, Lcom/google/android/youtube/app/k;->c()J

    move-result-wide v6

    sput-wide v6, Lcom/google/android/youtube/app/honeycomb/Shell;->e:J

    const-string v0, "upgrade_prompt_shown_millis"

    const-wide/16 v6, 0x0

    invoke-interface {p2, v0, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    sput-wide v6, Lcom/google/android/youtube/app/honeycomb/Shell;->d:J

    if-ge v3, v4, :cond_2

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/google/android/youtube/app/honeycomb/Shell;->b:Z

    if-ge v3, v5, :cond_0

    move v2, v1

    :cond_0
    sput-boolean v2, Lcom/google/android/youtube/app/honeycomb/Shell;->c:Z

    sput-boolean v1, Lcom/google/android/youtube/app/honeycomb/Shell;->a:Z

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "App version = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Min app version = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Target app version = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Prompt shown ago = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    sget-wide v3, Lcom/google/android/youtube/app/honeycomb/Shell;->d:J

    sub-long/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method static synthetic b()Z
    .locals 1

    sget-boolean v0, Lcom/google/android/youtube/app/honeycomb/Shell;->b:Z

    return v0
.end method

.method static synthetic c()Z
    .locals 1

    sget-boolean v0, Lcom/google/android/youtube/app/honeycomb/Shell;->c:Z

    return v0
.end method

.method static synthetic d()J
    .locals 2

    sget-wide v0, Lcom/google/android/youtube/app/honeycomb/Shell;->e:J

    return-wide v0
.end method
