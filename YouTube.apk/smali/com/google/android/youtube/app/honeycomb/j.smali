.class final Lcom/google/android/youtube/app/honeycomb/j;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;

.field private b:Landroid/os/Handler;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/j;->a:Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/honeycomb/j;-><init>(Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/j;->a:Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;

    invoke-virtual {v1, p0, v0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/j;->b:Landroid/os/Handler;

    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/j;->a:Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    const-string v0, "android.intent.action.SCREEN_OFF"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/j;->a:Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->f(Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;)Lcom/google/android/youtube/core/player/as;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/as;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/j;->a:Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->f(Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;)Lcom/google/android/youtube/core/player/as;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/as;->c()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/j;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/k;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/honeycomb/k;-><init>(Lcom/google/android/youtube/app/honeycomb/j;)V

    const-wide/32 v2, 0x2bf20

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/j;->b:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    goto :goto_0
.end method
