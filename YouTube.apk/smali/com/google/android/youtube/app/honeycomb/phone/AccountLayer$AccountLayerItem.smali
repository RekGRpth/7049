.class final enum Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;

.field public static final enum FAVORITES:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;

.field public static final enum HISTORY:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;

.field public static final enum PLAYLISTS:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;

.field public static final enum UPLOADS:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;

.field public static final enum WATCH_LATER:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;


# instance fields
.field public final defaultThumbnailId:I

.field public final labelStringId:I

.field public final position:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;

    const-string v1, "HISTORY"

    const v4, 0x7f0b0192

    const v5, 0x7f02018c

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;->HISTORY:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;

    new-instance v3, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;

    const-string v4, "UPLOADS"

    const v7, 0x7f0b018e

    const v8, 0x7f02018e

    move v5, v9

    move v6, v9

    invoke-direct/range {v3 .. v8}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;->UPLOADS:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;

    new-instance v3, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;

    const-string v4, "FAVORITES"

    const v7, 0x7f0b0190

    const v8, 0x7f02018a

    move v5, v10

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;->FAVORITES:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;

    new-instance v3, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;

    const-string v4, "PLAYLISTS"

    const v7, 0x7f0b0193

    const v8, 0x7f02018d

    move v5, v11

    move v6, v11

    invoke-direct/range {v3 .. v8}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;->PLAYLISTS:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;

    new-instance v3, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;

    const-string v4, "WATCH_LATER"

    const v7, 0x7f0b0191

    const v8, 0x7f02018f

    move v5, v12

    move v6, v12

    invoke-direct/range {v3 .. v8}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;->WATCH_LATER:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;

    sget-object v1, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;->HISTORY:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;->UPLOADS:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;

    aput-object v1, v0, v9

    sget-object v1, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;->FAVORITES:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;

    aput-object v1, v0, v10

    sget-object v1, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;->PLAYLISTS:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;

    aput-object v1, v0, v11

    sget-object v1, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;->WATCH_LATER:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;

    aput-object v1, v0, v12

    sput-object v0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;->$VALUES:[Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIII)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;->position:I

    iput p4, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;->labelStringId:I

    iput p5, p0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;->defaultThumbnailId:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;
    .locals 1

    const-class v0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;

    return-object v0
.end method

.method public static values()[Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;->$VALUES:[Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;

    invoke-virtual {v0}, [Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;

    return-object v0
.end method
