.class public Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;
.super Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/bk;


# instance fields
.field private n:Landroid/content/res/Resources;

.field private o:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private p:Lcom/google/android/youtube/core/client/bc;

.field private q:Lcom/google/android/youtube/core/async/au;

.field private r:Lcom/google/android/youtube/core/client/be;

.field private s:Lcom/google/android/youtube/core/e;

.field private t:Lcom/google/android/youtube/core/Analytics;

.field private u:Lcom/google/android/youtube/app/ui/bx;

.field private v:Lcom/google/android/youtube/core/ui/j;

.field private w:Landroid/net/Uri;

.field private x:Ljava/lang/String;

.field private y:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;)Landroid/content/Intent;
    .locals 4

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "category"

    invoke-static {p2}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;->toString(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private f()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->u:Lcom/google/android/youtube/app/ui/bx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->u:Lcom/google/android/youtube/app/ui/bx;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->n:Landroid/content/res/Resources;

    const v2, 0x7f0a000a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/bx;->a(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/youtube/app/YouTubeApplication;)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->n:Landroid/content/res/Resources;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->U()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->o:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->a()Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->p:Lcom/google/android/youtube/core/client/bc;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->e_()Lcom/google/android/youtube/core/client/be;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->r:Lcom/google/android/youtube/core/client/be;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->s:Lcom/google/android/youtube/core/e;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->i()Lcom/google/android/youtube/core/Analytics;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->t:Lcom/google/android/youtube/core/Analytics;

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 5

    const/4 v1, 0x1

    const/4 v4, 0x0

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->y:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->v:Lcom/google/android/youtube/core/ui/j;

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->p:Lcom/google/android/youtube/core/client/bc;

    invoke-interface {v2}, Lcom/google/android/youtube/core/client/bc;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v2

    const/16 v3, 0x18

    invoke-virtual {v2, p1, v3}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->f(Lcom/google/android/youtube/core/model/UserAuth;I)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/j;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->v:Lcom/google/android/youtube/core/ui/j;

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->p:Lcom/google/android/youtube/core/client/bc;

    invoke-interface {v2}, Lcom/google/android/youtube/core/client/bc;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->w:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->d(Landroid/net/Uri;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/j;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->finish()V

    return-void
.end method

.method public final g_()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->finish()V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->f()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 11

    const v10, 0x7f08004b

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->w:Landroid/net/Uri;

    const-string v1, "category"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->x:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->x:Ljava/lang/String;

    sget-object v1, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;->RECOMMENDED:Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->n:Landroid/content/res/Resources;

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;->toString(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->y:Z

    const v0, 0x7f04008a

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->setContentView(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->x:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->b(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->getApplication()Landroid/app/Application;

    move-result-object v4

    check-cast v4, Lcom/google/android/youtube/app/YouTubeApplication;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->t:Lcom/google/android/youtube/core/Analytics;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->p:Lcom/google/android/youtube/core/client/bc;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->o:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->F()Lcom/google/android/youtube/app/d;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->s:Lcom/google/android/youtube/core/e;

    iget-object v7, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->r:Lcom/google/android/youtube/core/client/be;

    new-instance v8, Lcom/google/android/youtube/app/honeycomb/phone/v;

    invoke-direct {v8, p0}, Lcom/google/android/youtube/app/honeycomb/phone/v;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;)V

    move-object v0, p0

    move-object v9, p0

    invoke-static/range {v0 .. v9}, Lcom/google/android/youtube/app/adapter/bn;->a(Landroid/content/Context;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/app/YouTubeApplication;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/app/ui/cw;Landroid/app/Activity;)Lcom/google/android/youtube/app/adapter/ab;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/app/ui/bx;

    invoke-direct {v1, p0, v0}, Lcom/google/android/youtube/app/ui/bx;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/a/a;)V

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->u:Lcom/google/android/youtube/app/ui/bx;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->u:Lcom/google/android/youtube/app/ui/bx;

    invoke-virtual {v0, v10}, Lcom/google/android/youtube/app/ui/bx;->c(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->u:Lcom/google/android/youtube/app/ui/bx;

    invoke-virtual {v0, v10}, Lcom/google/android/youtube/app/ui/bx;->d(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->u:Lcom/google/android/youtube/app/ui/bx;

    const v1, 0x7f080052

    const v2, 0x7f080054

    const v3, 0x7f080053

    const v4, 0x7f080055

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/youtube/app/ui/bx;->a(IIII)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->f()V

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->y:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->p:Lcom/google/android/youtube/core/client/bc;

    invoke-interface {v0}, Lcom/google/android/youtube/core/client/bc;->h()Lcom/google/android/youtube/core/async/au;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->q:Lcom/google/android/youtube/core/async/au;

    new-instance v0, Lcom/google/android/youtube/core/ui/j;

    const v1, 0x7f070124

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/core/ui/PagedListView;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->u:Lcom/google/android/youtube/app/ui/bx;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->q:Lcom/google/android/youtube/core/async/au;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->s:Lcom/google/android/youtube/core/e;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/core/ui/j;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/PagedView;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/e;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->v:Lcom/google/android/youtube/core/ui/j;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->o:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0, p0, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bk;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->p:Lcom/google/android/youtube/core/client/bc;

    invoke-interface {v0}, Lcom/google/android/youtube/core/client/bc;->g()Lcom/google/android/youtube/core/async/au;

    move-result-object v0

    goto :goto_0
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onResume()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->o:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bk;)V

    return-void
.end method

.method protected onStop()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onStop()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->v:Lcom/google/android/youtube/core/ui/j;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ChannelStoreCategoryActivity;->v:Lcom/google/android/youtube/core/ui/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/ui/j;->e()V

    :cond_0
    return-void
.end method

.method protected final w()Ljava/lang/String;
    .locals 1

    const-string v0, "yt_channel"

    return-object v0
.end method
