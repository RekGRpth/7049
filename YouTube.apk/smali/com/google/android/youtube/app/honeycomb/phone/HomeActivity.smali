.class public Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;
.super Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/honeycomb/phone/aq;
.implements Lcom/google/android/youtube/app/honeycomb/phone/da;
.implements Lcom/google/android/youtube/app/ui/cw;
.implements Lcom/google/android/youtube/core/async/bk;


# instance fields
.field private A:Landroid/app/ProgressDialog;

.field private B:Lcom/google/android/youtube/core/model/UserAuth;

.field private C:Lcom/google/android/youtube/core/async/au;

.field private D:Lcom/google/android/youtube/core/async/au;

.field private E:Lcom/google/android/youtube/core/async/GDataRequestFactory;

.field private F:Z

.field private G:Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;

.field private H:Z

.field private I:Z

.field private J:Ljava/lang/String;

.field private K:Lcom/google/android/youtube/app/honeycomb/phone/by;

.field private n:Lcom/google/android/youtube/app/YouTubeApplication;

.field private o:Landroid/content/SharedPreferences;

.field private p:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private q:Lcom/google/android/youtube/core/e;

.field private r:Lcom/google/android/youtube/core/Analytics;

.field private s:Lcom/google/android/youtube/app/ui/Slider;

.field private t:Landroid/view/View;

.field private u:Lcom/google/android/youtube/app/honeycomb/phone/ab;

.field private v:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;

.field private w:Lcom/google/android/youtube/app/honeycomb/phone/w;

.field private x:Lcom/google/android/youtube/app/honeycomb/phone/x;

.field private y:Lcom/google/android/youtube/app/compat/t;

.field private z:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "android.intent.action.MAIN"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;)Landroid/content/SharedPreferences;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->o:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;)Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->G:Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;ZZ)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->a(ZZ)V

    return-void
.end method

.method private a(ZZ)V
    .locals 2

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->s:Lcom/google/android/youtube/app/ui/Slider;

    if-eqz p1, :cond_0

    sget-object v0, Lcom/google/android/youtube/app/ui/Slider$Order;->FIRST:Lcom/google/android/youtube/app/ui/Slider$Order;

    :goto_0
    invoke-interface {v1, v0, p2}, Lcom/google/android/youtube/app/ui/Slider;->a(Lcom/google/android/youtube/app/ui/Slider$Order;Z)V

    return-void

    :cond_0
    sget-object v0, Lcom/google/android/youtube/app/ui/Slider$Order;->SECOND:Lcom/google/android/youtube/app/ui/Slider$Order;

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    invoke-static {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.youtube.action.search"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private c(I)V
    .locals 3

    const-string v0, ""

    sparse-switch p1, :sswitch_data_0

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->r:Lcom/google/android/youtube/core/Analytics;

    const-string v2, "GuideSelection"

    invoke-interface {v1, v2, v0}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->n:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/YouTubeApplication;->a(I)V

    return-void

    :sswitch_0
    const-string v0, "Channel"

    goto :goto_0

    :sswitch_1
    const-string v0, "Account"

    goto :goto_0

    :sswitch_2
    const-string v0, "Category"

    goto :goto_0

    :sswitch_3
    const-string v0, "ChannelStore"

    goto :goto_0

    :sswitch_4
    const-string v0, "Recommended"

    goto :goto_0

    :sswitch_5
    const-string v0, "Trending"

    goto :goto_0

    :sswitch_6
    const-string v0, "TheFeed"

    goto :goto_0

    :sswitch_7
    const-string v0, "MySubscriptions"

    goto :goto_0

    :sswitch_8
    const-string v0, "Live"

    goto :goto_0

    :sswitch_9
    const-string v0, "RemoteQueue"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_0
        0x20 -> :sswitch_1
        0x40 -> :sswitch_2
        0x80 -> :sswitch_3
        0x100 -> :sswitch_4
        0x200 -> :sswitch_5
        0x400 -> :sswitch_6
        0x800 -> :sswitch_8
        0x1000 -> :sswitch_9
        0x2000 -> :sswitch_7
    .end sparse-switch
.end method

.method private c(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->J:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->o()V

    return-void
.end method

.method private g()V
    .locals 3

    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->z:Ljava/lang/Boolean;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->l_()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->J:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->u:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    if-nez v0, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->h()V

    :cond_1
    const-string v0, "REMOTE"

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->J:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->u:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->J:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->a(Ljava/lang/String;Z)V

    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->J:Ljava/lang/String;

    return-void
.end method

.method private h()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->s:Lcom/google/android/youtube/app/ui/Slider;

    sget-object v1, Lcom/google/android/youtube/app/ui/Slider$Order;->SECOND:Lcom/google/android/youtube/app/ui/Slider$Order;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/app/ui/Slider;->setLayer(Lcom/google/android/youtube/app/ui/Slider$Order;Lcom/google/android/youtube/app/ui/at;)V

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/ab;

    invoke-direct {v0, p0, p0}, Lcom/google/android/youtube/app/honeycomb/phone/ab;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/youtube/app/honeycomb/phone/aq;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->u:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->s:Lcom/google/android/youtube/app/ui/Slider;

    sget-object v1, Lcom/google/android/youtube/app/ui/Slider$Order;->FIRST:Lcom/google/android/youtube/app/ui/Slider$Order;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->u:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/app/ui/Slider;->setLayer(Lcom/google/android/youtube/app/ui/Slider$Order;Lcom/google/android/youtube/app/ui/at;)V

    return-void
.end method

.method private m()V
    .locals 7

    const/4 v6, 0x0

    const/4 v5, 0x1

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0005

    invoke-virtual {v0, v1, v5, v5}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v0

    float-to-double v1, v0

    const-wide v3, 0x3fa999999999999aL

    cmpl-double v1, v1, v3

    if-lez v1, :cond_0

    float-to-double v1, v0

    const-wide v3, 0x3fee666666666666L

    cmpg-double v1, v1, v3

    if-gez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->s:Lcom/google/android/youtube/app/ui/Slider;

    invoke-interface {v1, v0}, Lcom/google/android/youtube/app/ui/Slider;->a(F)V

    iput-boolean v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->I:Z

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;->AUTO:Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->G:Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->a()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->s:Lcom/google/android/youtube/app/ui/Slider;

    invoke-interface {v0}, Lcom/google/android/youtube/app/ui/Slider;->e()V

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->I:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;->AUTO:Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->G:Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;

    invoke-direct {p0, v5, v6}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->a(ZZ)V

    :cond_1
    iput-boolean v6, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->I:Z

    goto :goto_0
.end method

.method private n()V
    .locals 4

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/x;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0004

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/x;

    const-string v2, "context cannot be null"

    invoke-static {p0, v2}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v3, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v3, v3

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    div-float v2, v3, v2

    float-to-int v2, v2

    int-to-float v2, v2

    mul-float/2addr v0, v2

    float-to-int v0, v0

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/honeycomb/phone/x;->a(I)V

    :cond_0
    return-void
.end method

.method private o()V
    .locals 5

    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->J:Ljava/lang/String;

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->s:Lcom/google/android/youtube/app/ui/Slider;

    sget-object v3, Lcom/google/android/youtube/app/ui/Slider$Order;->FIRST:Lcom/google/android/youtube/app/ui/Slider$Order;

    invoke-interface {v2, v3, v4}, Lcom/google/android/youtube/app/ui/Slider;->setLayer(Lcom/google/android/youtube/app/ui/Slider$Order;Lcom/google/android/youtube/app/ui/at;)V

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->s:Lcom/google/android/youtube/app/ui/Slider;

    sget-object v3, Lcom/google/android/youtube/app/ui/Slider$Order;->SECOND:Lcom/google/android/youtube/app/ui/Slider$Order;

    invoke-interface {v2, v3, v4}, Lcom/google/android/youtube/app/ui/Slider;->setLayer(Lcom/google/android/youtube/app/ui/Slider$Order;Lcom/google/android/youtube/app/ui/at;)V

    :cond_0
    iput-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->z:Ljava/lang/Boolean;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->l_()V

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->A:Landroid/app/ProgressDialog;

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->show()V

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->p:Lcom/google/android/youtube/core/async/UserAuthorizer;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->o:Landroid/content/SharedPreferences;

    const-string v4, "user_signed_out"

    invoke-interface {v3, v4, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_1

    move v0, v1

    :cond_1
    new-instance v3, Lcom/google/android/youtube/app/honeycomb/phone/aw;

    invoke-direct {v3, p0}, Lcom/google/android/youtube/app/honeycomb/phone/aw;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;)V

    invoke-virtual {v2, p0, v0, v1, v3}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;ZZLcom/google/android/youtube/core/async/bk;)V

    return-void
.end method


# virtual methods
.method public final G()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->g_()V

    return-void
.end method

.method protected final a(I)Landroid/app/Dialog;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->K:Lcom/google/android/youtube/app/honeycomb/phone/by;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->K:Lcom/google/android/youtube/app/honeycomb/phone/by;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/by;->b(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(I)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_1
.end method

.method public final a()V
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->F:Z

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->l()Lcom/google/android/youtube/app/compat/SupportActionBar;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v2, v1}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(II)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->l()Lcom/google/android/youtube/app/compat/SupportActionBar;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/compat/SupportActionBar;->b(Z)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->D()V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->l_()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->r:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "GuideExpanded"

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->G:Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->G:Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;

    sget-object v1, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;->AUTO:Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->n:Lcom/google/android/youtube/app/YouTubeApplication;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/YouTubeApplication;->a(I)V

    :cond_0
    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;->SWIPE:Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->G:Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;

    return-void
.end method

.method public final a(Landroid/app/Activity;Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 0

    invoke-virtual {p0, p2}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->a(Lcom/google/android/youtube/core/model/UserAuth;)V

    return-void
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->u:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->a(Landroid/net/Uri;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/x;

    instance-of v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/w;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/x;

    check-cast v0, Lcom/google/android/youtube/app/honeycomb/phone/w;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/w;->a(Landroid/net/Uri;)V

    :cond_0
    return-void
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/x;

    instance-of v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/w;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/x;

    check-cast v0, Lcom/google/android/youtube/app/honeycomb/phone/w;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/app/honeycomb/phone/w;->a(Landroid/net/Uri;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Subscription;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->u:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->a(Lcom/google/android/youtube/core/model/Subscription;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/x;

    instance-of v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/w;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/x;

    check-cast v0, Lcom/google/android/youtube/app/honeycomb/phone/w;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/w;->a(Lcom/google/android/youtube/core/model/Subscription;)V

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 3

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->B:Lcom/google/android/youtube/core/model/UserAuth;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->A:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->z:Ljava/lang/Boolean;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->l_()V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->h()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->J:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->u:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->J:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->a(Ljava/lang/String;Z)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->J:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->A:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->q:Lcom/google/android/youtube/core/e;

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/core/e;->b(Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->B:Lcom/google/android/youtube/core/model/UserAuth;

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->g()V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/compat/m;)Z
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/youtube/app/compat/m;)Z

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->E()Lcom/google/android/youtube/app/compat/r;

    move-result-object v0

    const v1, 0x7f110001

    invoke-virtual {v0, v1, p1}, Lcom/google/android/youtube/app/compat/r;->a(ILcom/google/android/youtube/app/compat/m;)V

    const v0, 0x7f070181

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/app/compat/m;->c(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->y:Lcom/google/android/youtube/app/compat/t;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->y:Lcom/google/android/youtube/app/compat/t;

    invoke-interface {v0, v2}, Lcom/google/android/youtube/app/compat/t;->b(Z)Lcom/google/android/youtube/app/compat/t;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->s:Lcom/google/android/youtube/app/ui/Slider;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/app/ui/Slider;->a(Lcom/google/android/youtube/app/compat/m;)V

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->H:Z

    if-eqz v0, :cond_0

    iput-boolean v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->H:Z

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->onSearchRequested()Z

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public final a(Lcom/google/android/youtube/app/compat/t;)Z
    .locals 4

    const/4 v0, 0x1

    invoke-interface {p1}, Lcom/google/android/youtube/app/compat/t;->f()I

    move-result v1

    const v2, 0x7f070181

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->z:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->r:Lcom/google/android/youtube/core/Analytics;

    const-string v2, "SignOut"

    const-string v3, "Menu"

    invoke-interface {v1, v2, v3}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->o:Landroid/content/SharedPreferences;

    invoke-static {v1}, Lcom/google/android/youtube/app/compat/ac;->a(Landroid/content/SharedPreferences;)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v1

    const-string v2, "user_signed_out"

    invoke-virtual {v1, v2, v0}, Lcom/google/android/youtube/app/compat/ad;->a(Ljava/lang/String;Z)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/youtube/app/compat/ad;->a()V

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->p:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a()V

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->r:Lcom/google/android/youtube/core/Analytics;

    const-string v2, "SignIn"

    const-string v3, "Menu"

    invoke-interface {v1, v2, v3}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "WHAT_TO_WATCH"

    invoke-direct {p0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->s:Lcom/google/android/youtube/app/ui/Slider;

    invoke-interface {v1, p1}, Lcom/google/android/youtube/app/ui/Slider;->a(Lcom/google/android/youtube/app/compat/t;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/youtube/app/compat/t;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Z)Z
    .locals 12

    const/4 v11, 0x0

    const/4 v3, 0x1

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->H()Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    move-result-object v0

    invoke-virtual {v0, v11}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Ljava/lang/String;)V

    const-string v0, "WHAT_TO_WATCH"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    if-eqz p2, :cond_0

    const/16 v0, 0x400

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->c(I)V

    :cond_0
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;

    sget-object v1, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer$DisplayMode;->WHAT_TO_WATCH:Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer$DisplayMode;

    invoke-direct {v0, p0, v1, p0}, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer$DisplayMode;Lcom/google/android/youtube/app/honeycomb/phone/da;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/x;

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->n()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->s:Lcom/google/android/youtube/app/ui/Slider;

    sget-object v1, Lcom/google/android/youtube/app/ui/Slider$Order;->SECOND:Lcom/google/android/youtube/app/ui/Slider$Order;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/x;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/app/ui/Slider;->setLayer(Lcom/google/android/youtube/app/ui/Slider$Order;Lcom/google/android/youtube/app/ui/at;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/x;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/ax;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/honeycomb/phone/ax;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/x;->a(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->o:Landroid/content/SharedPreferences;

    const-string v1, "show_channel_store_turorial"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez p2, :cond_1

    if-nez v1, :cond_2

    :cond_1
    if-eqz p2, :cond_16

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;->SELECTION:Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;

    :goto_1
    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->G:Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;

    invoke-direct {p0, v7, p2}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->a(ZZ)V

    :cond_2
    if-nez p2, :cond_3

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->F:Z

    if-eqz v0, :cond_17

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/x;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/x;->k()V

    :cond_3
    :goto_2
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->t:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->setContentView(Landroid/view/View;)V

    if-eqz v1, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->u:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->n()V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->D()V

    :cond_4
    move v7, v3

    :goto_3
    return v7

    :cond_5
    const-string v0, "MY_SUBSCRIPTIONS"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    if-eqz p2, :cond_6

    const/16 v0, 0x2000

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->c(I)V

    :cond_6
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;

    sget-object v1, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer$DisplayMode;->MY_SUBSCRIPTIONS:Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer$DisplayMode;

    invoke-direct {v0, p0, v1, p0}, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer$DisplayMode;Lcom/google/android/youtube/app/honeycomb/phone/da;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/x;

    goto :goto_0

    :cond_7
    const-string v0, "ACCOUNT"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    const/16 v0, 0x20

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->c(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->B:Lcom/google/android/youtube/core/model/UserAuth;

    if-nez v0, :cond_8

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->c(Ljava/lang/String;)V

    goto :goto_3

    :cond_8
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->v:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;

    if-nez v0, :cond_9

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->v:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;

    :cond_9
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->v:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/x;

    goto/16 :goto_0

    :cond_a
    const-string v0, "CHANNEL_STORE"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    const/16 v0, 0x80

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->c(I)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->H()Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    move-result-object v0

    const-string v1, "is:channel"

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->B:Lcom/google/android/youtube/core/model/UserAuth;

    if-nez v0, :cond_b

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->c(Ljava/lang/String;)V

    goto :goto_3

    :cond_b
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->w:Lcom/google/android/youtube/app/honeycomb/phone/w;

    if-nez v0, :cond_c

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/w;

    invoke-direct {v0, p0, p0}, Lcom/google/android/youtube/app/honeycomb/phone/w;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/youtube/app/ui/cw;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->w:Lcom/google/android/youtube/app/honeycomb/phone/w;

    :cond_c
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->w:Lcom/google/android/youtube/app/honeycomb/phone/w;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/x;

    goto/16 :goto_0

    :cond_d
    const-string v0, "REMOTE"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    const/16 v0, 0x1000

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->c(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->K:Lcom/google/android/youtube/app/honeycomb/phone/by;

    if-nez v0, :cond_e

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/by;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->F()Lcom/google/android/youtube/app/d;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->r:Lcom/google/android/youtube/core/Analytics;

    invoke-static {p0}, Lcom/google/android/youtube/core/utils/Util;->g(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-direct {v0, p0, v1, v2, v4}, Lcom/google/android/youtube/app/honeycomb/phone/by;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/Analytics;Landroid/graphics/Typeface;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->K:Lcom/google/android/youtube/app/honeycomb/phone/by;

    :cond_e
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->K:Lcom/google/android/youtube/app/honeycomb/phone/by;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/x;

    goto/16 :goto_0

    :cond_f
    const-string v0, "RECOMMENDED_GUIDE_ITEM"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    const/16 v0, 0x100

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->c(I)V

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->B:Lcom/google/android/youtube/core/model/UserAuth;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->E:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->B:Lcom/google/android/youtube/core/model/UserAuth;

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->l(Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_10
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->E:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    sget-object v2, Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;->MOST_POPULAR:Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->n:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v4}, Lcom/google/android/youtube/app/YouTubeApplication;->F()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, v11, v4, v11}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->a(Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/aa;

    const v2, 0x7f0b0182

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v4, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_RECOMMENDED:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->C:Lcom/google/android/youtube/core/async/au;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v6

    new-array v6, v6, [Lcom/google/android/youtube/core/async/GDataRequest;

    invoke-interface {v1, v6}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Lcom/google/android/youtube/core/async/GDataRequest;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/app/honeycomb/phone/aa;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Ljava/lang/String;ZLcom/google/android/youtube/core/client/VideoStats2Client$Feature;Lcom/google/android/youtube/core/async/au;[Lcom/google/android/youtube/core/async/GDataRequest;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/x;

    goto/16 :goto_0

    :cond_11
    const-string v0, "TRENDING_GUIDE_ITEM"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    if-eqz p2, :cond_12

    const/16 v0, 0x200

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->c(I)V

    :cond_12
    new-instance v4, Lcom/google/android/youtube/app/honeycomb/phone/aa;

    const v0, 0x7f0b0183

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    sget-object v8, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_TRENDING:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    iget-object v9, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->D:Lcom/google/android/youtube/core/async/au;

    new-array v10, v3, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->E:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    sget-object v1, Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;->MOST_POPULAR:Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->n:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->F()Ljava/lang/String;

    move-result-object v2

    sget-object v5, Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;->TODAY:Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    invoke-virtual {v0, v1, v11, v2, v5}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->a(Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v0

    aput-object v0, v10, v7

    move-object v5, p0

    invoke-direct/range {v4 .. v10}, Lcom/google/android/youtube/app/honeycomb/phone/aa;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Ljava/lang/String;ZLcom/google/android/youtube/core/client/VideoStats2Client$Feature;Lcom/google/android/youtube/core/async/au;[Lcom/google/android/youtube/core/async/GDataRequest;)V

    iput-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/x;

    goto/16 :goto_0

    :cond_13
    const-string v0, "LIVE_GUIDE_ITEM"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    const/16 v0, 0x800

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->c(I)V

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/ay;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/honeycomb/phone/ay;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/x;

    goto/16 :goto_0

    :cond_14
    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/l;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    const/16 v0, 0x40

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->c(I)V

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/l;

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/l;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-direct {v1, p0, p1, v0}, Lcom/google/android/youtube/app/honeycomb/phone/l;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Ljava/lang/String;Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;)V

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/x;

    goto/16 :goto_0

    :cond_15
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->c(I)V

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/p;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, p0, v1, p0}, Lcom/google/android/youtube/app/honeycomb/phone/p;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Landroid/net/Uri;Lcom/google/android/youtube/app/ui/cw;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/x;

    goto/16 :goto_0

    :cond_16
    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;->AUTO:Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;

    goto/16 :goto_1

    :cond_17
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/x;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/x;->j()V

    goto/16 :goto_2
.end method

.method protected final b(I)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->I:Z

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->b(I)V

    :cond_0
    return-void
.end method

.method public final b(Landroid/net/Uri;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/x;

    instance-of v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/w;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/x;

    check-cast v0, Lcom/google/android/youtube/app/honeycomb/phone/w;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/w;->b(Landroid/net/Uri;)V

    :cond_0
    return-void
.end method

.method protected final b(Ljava/lang/String;)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->I:Z

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->b(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final b(Lcom/google/android/youtube/app/compat/m;)Z
    .locals 3

    const/4 v2, 0x1

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->b(Lcom/google/android/youtube/app/compat/m;)Z

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->s:Lcom/google/android/youtube/app/ui/Slider;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/app/ui/Slider;->b(Lcom/google/android/youtube/app/compat/m;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->z:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->y:Lcom/google/android/youtube/app/compat/t;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->z:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f0b00e2

    :goto_0
    invoke-interface {v1, v0}, Lcom/google/android/youtube/app/compat/t;->c(I)Lcom/google/android/youtube/app/compat/t;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->y:Lcom/google/android/youtube/app/compat/t;

    invoke-interface {v0, v2}, Lcom/google/android/youtube/app/compat/t;->b(Z)Lcom/google/android/youtube/app/compat/t;

    :cond_0
    return v2

    :cond_1
    const v0, 0x7f0b00e1

    goto :goto_0
.end method

.method protected final e()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final f()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->u:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    const-string v1, "CHANNEL_STORE"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->a(Ljava/lang/String;Z)V

    return-void
.end method

.method public final g_()V
    .locals 3

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->B:Lcom/google/android/youtube/core/model/UserAuth;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->A:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->o:Landroid/content/SharedPreferences;

    const-string v1, "user_signed_out"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->g()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->o()V

    goto :goto_0
.end method

.method public final i()Z
    .locals 2

    const/4 v1, 0x1

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;->UP:Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->G:Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;

    invoke-direct {p0, v1, v1}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->a(ZZ)V

    return v1
.end method

.method public final o_()V
    .locals 3

    const/4 v1, 0x4

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->F:Z

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->l()Lcom/google/android/youtube/app/compat/SupportActionBar;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(II)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->l()Lcom/google/android/youtube/app/compat/SupportActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/compat/SupportActionBar;->b(Z)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->l_()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->r:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "GuideCollapsed"

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->G:Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;->SWIPE:Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->G:Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;

    return-void
.end method

.method public onBackPressed()V
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->F:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->u:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->I:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->u:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->u:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->q()V

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;->BACK:Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->G:Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->s:Lcom/google/android/youtube/app/ui/Slider;

    sget-object v1, Lcom/google/android/youtube/app/ui/Slider$Order;->SECOND:Lcom/google/android/youtube/app/ui/Slider$Order;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/app/ui/Slider;->a(Lcom/google/android/youtube/app/ui/Slider$Order;Z)V

    goto :goto_0

    :cond_1
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->n()V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->m()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->s:Lcom/google/android/youtube/app/ui/Slider;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/app/ui/Slider;->a(Landroid/content/res/Configuration;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    const/4 v5, 0x0

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04008d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f07003e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/app/honeycomb/phone/av;

    invoke-direct {v2, p0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/av;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;Landroid/view/View;)V

    const-wide/16 v3, 0x3e8

    invoke-virtual {v1, v2, v3, v4}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->setContentView(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->n:Lcom/google/android/youtube/app/YouTubeApplication;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->n:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->S()Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->o:Landroid/content/SharedPreferences;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->n:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->U()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->p:Lcom/google/android/youtube/core/async/UserAuthorizer;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->n:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->q:Lcom/google/android/youtube/core/e;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->n:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->i()Lcom/google/android/youtube/core/Analytics;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->r:Lcom/google/android/youtube/core/Analytics;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->n:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->a()Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/client/bc;->u()Lcom/google/android/youtube/core/async/au;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->C:Lcom/google/android/youtube/core/async/au;

    invoke-interface {v0}, Lcom/google/android/youtube/core/client/bc;->k()Lcom/google/android/youtube/core/async/au;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->D:Lcom/google/android/youtube/core/async/au;

    invoke-interface {v0}, Lcom/google/android/youtube/core/client/bc;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->E:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    invoke-static {p0}, Lcom/google/android/youtube/app/ui/ck;->a(Landroid/app/Activity;)Lcom/google/android/youtube/app/ui/Slider;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->s:Lcom/google/android/youtube/app/ui/Slider;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->s:Lcom/google/android/youtube/app/ui/Slider;

    sget-object v1, Lcom/google/android/youtube/app/ui/Slider$Order;->FIRST:Lcom/google/android/youtube/app/ui/Slider$Order;

    sget-object v2, Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;->OCCLUDE:Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/app/ui/Slider;->setCollapseStrategy(Lcom/google/android/youtube/app/ui/Slider$Order;Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->s:Lcom/google/android/youtube/app/ui/Slider;

    sget-object v1, Lcom/google/android/youtube/app/ui/Slider$Order;->SECOND:Lcom/google/android/youtube/app/ui/Slider$Order;

    sget-object v2, Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;->DISPLACE:Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/app/ui/Slider;->setCollapseStrategy(Lcom/google/android/youtube/app/ui/Slider$Order;Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->m()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->s:Lcom/google/android/youtube/app/ui/Slider;

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->t:Landroid/view/View;

    iput-boolean v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->F:Z

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;->SWIPE:Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->G:Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity$SlideTrigger;

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->A:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->A:Landroid/app/ProgressDialog;

    const v1, 0x7f0b01c2

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->A:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v5}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->A:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v5}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->p:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->p:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0, p0, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bk;)V

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "com.google.android.youtube.action.search"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->H:Z

    :cond_0
    :goto_1
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->g_()V

    goto :goto_0

    :cond_2
    const-string v1, "guide_selection"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "guide_selection"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->J:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->r:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "GuideSelectionByIntent"

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->J:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 3

    const-string v0, "com.google.android.youtube.action.search"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->onSearchRequested()Z

    :goto_0
    return-void

    :cond_0
    const-string v0, "guide_selection"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "guide_selection"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->r:Lcom/google/android/youtube/core/Analytics;

    const-string v2, "GuideSelectionByIntent"

    invoke-interface {v1, v2, v0}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->u:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->u:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->a(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_1
    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->J:Ljava/lang/String;

    goto :goto_0

    :cond_2
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onNewIntent(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onPause()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->s:Lcom/google/android/youtube/app/ui/Slider;

    invoke-interface {v0}, Lcom/google/android/youtube/app/ui/Slider;->c()V

    return-void
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onResume()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->s:Lcom/google/android/youtube/app/ui/Slider;

    invoke-interface {v0}, Lcom/google/android/youtube/app/ui/Slider;->d()V

    return-void
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onStart()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->n:Lcom/google/android/youtube/app/YouTubeApplication;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/YouTubeApplication;->a(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->u:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->u:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->r()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->u:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->s()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/x;

    instance-of v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/p;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/x;

    check-cast v0, Lcom/google/android/youtube/app/honeycomb/phone/p;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/p;->n()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/x;

    instance-of v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/w;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->x:Lcom/google/android/youtube/app/honeycomb/phone/x;

    check-cast v0, Lcom/google/android/youtube/app/honeycomb/phone/w;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/w;->b()V

    goto :goto_0
.end method

.method protected final w()Ljava/lang/String;
    .locals 1

    const-string v0, "yt_home"

    return-object v0
.end method
