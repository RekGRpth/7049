.class public Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;
.super Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/ui/ab;
.implements Lcom/google/android/youtube/core/async/bk;


# instance fields
.field private A:I

.field private B:Lcom/google/android/youtube/core/model/Video;

.field private C:Lcom/google/android/youtube/app/honeycomb/phone/bq;

.field private D:Z

.field private n:Lcom/google/android/youtube/core/async/au;

.field private o:Lcom/google/android/youtube/core/client/bc;

.field private p:Lcom/google/android/youtube/core/client/be;

.field private q:Lcom/google/android/youtube/core/client/bg;

.field private r:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private s:Lcom/google/android/youtube/core/e;

.field private t:Landroid/content/res/Resources;

.field private u:Lcom/google/android/youtube/app/ui/dc;

.field private v:Lcom/google/android/youtube/app/adapter/cd;

.field private w:Lcom/google/android/youtube/app/ui/bx;

.field private x:Lcom/google/android/youtube/app/ui/v;

.field private y:Lcom/google/android/youtube/app/ui/v;

.field private z:I


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;-><init>()V

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/bq;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/bq;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;B)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->C:Lcom/google/android/youtube/app/honeycomb/phone/bq;

    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;)Lcom/google/android/youtube/app/ui/dc;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->u:Lcom/google/android/youtube/app/ui/dc;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;)V
    .locals 4

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/bo;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/honeycomb/phone/bo;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;)V

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->r:Lcom/google/android/youtube/core/async/UserAuthorizer;

    new-instance v2, Lcom/google/android/youtube/app/honeycomb/phone/bp;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->s:Lcom/google/android/youtube/core/e;

    invoke-direct {v2, p0, v3, v0}, Lcom/google/android/youtube/app/honeycomb/phone/bp;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/core/async/n;)V

    invoke-virtual {v1, p0, v2}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bk;)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;)Lcom/google/android/youtube/app/honeycomb/phone/bq;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->C:Lcom/google/android/youtube/app/honeycomb/phone/bq;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;)Lcom/google/android/youtube/app/adapter/cd;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->v:Lcom/google/android/youtube/app/adapter/cd;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;)Lcom/google/android/youtube/core/e;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->s:Lcom/google/android/youtube/core/e;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;)Lcom/google/android/youtube/core/model/Video;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->B:Lcom/google/android/youtube/core/model/Video;

    return-object v0
.end method

.method private f()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->w:Lcom/google/android/youtube/app/ui/bx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->w:Lcom/google/android/youtube/app/ui/bx;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->t:Landroid/content/res/Resources;

    const v2, 0x7f0a0012

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/bx;->a(I)V

    :cond_0
    return-void
.end method

.method static synthetic g(Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;)Lcom/google/android/youtube/core/client/bc;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->o:Lcom/google/android/youtube/core/client/bc;

    return-object v0
.end method


# virtual methods
.method protected final a(I)Landroid/app/Dialog;
    .locals 3

    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    :sswitch_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->x:Lcom/google/android/youtube/app/ui/v;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/v;->b()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->y:Lcom/google/android/youtube/app/ui/v;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/v;->b()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    :sswitch_2
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/bn;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/honeycomb/phone/bn;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;)V

    new-instance v1, Lcom/google/android/youtube/core/ui/w;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/core/ui/w;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0b01ff

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/core/ui/w;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x1040013

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x1040009

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x3ef -> :sswitch_2
        0x3f2 -> :sswitch_1
        0x3fc -> :sswitch_0
    .end sparse-switch
.end method

.method protected final a(Lcom/google/android/youtube/app/YouTubeApplication;)V
    .locals 1

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->U()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->r:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->a()Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->o:Lcom/google/android/youtube/core/client/bc;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->e_()Lcom/google/android/youtube/core/client/be;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->p:Lcom/google/android/youtube/core/client/be;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->w()Lcom/google/android/youtube/core/client/bg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->q:Lcom/google/android/youtube/core/client/bg;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->s:Lcom/google/android/youtube/core/e;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->o:Lcom/google/android/youtube/core/client/bc;

    invoke-interface {v0}, Lcom/google/android/youtube/core/client/bc;->m()Lcom/google/android/youtube/core/async/au;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->n:Lcom/google/android/youtube/core/async/au;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->t:Landroid/content/res/Resources;

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 4

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->D:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->u:Lcom/google/android/youtube/app/ui/dc;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->o:Lcom/google/android/youtube/core/client/bc;

    invoke-interface {v3}, Lcom/google/android/youtube/core/client/bc;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->d(Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/dc;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->D:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->s:Lcom/google/android/youtube/core/e;

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/core/e;->b(Ljava/lang/Throwable;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->finish()V

    return-void
.end method

.method public final synthetic a(ILjava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x1

    check-cast p2, Lcom/google/android/youtube/core/model/Video;

    iput-object p2, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->B:Lcom/google/android/youtube/core/model/Video;

    iget v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->z:I

    if-ne p1, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->I()Lcom/google/android/youtube/core/Analytics;

    move-result-object v1

    const-string v2, "DeleteUpload"

    invoke-interface {v1, v2}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    const/16 v1, 0x3ef

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->showDialog(I)V

    :goto_0
    return v0

    :cond_0
    iget v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->A:I

    if-ne p1, v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->I()Lcom/google/android/youtube/core/Analytics;

    move-result-object v1

    const-string v2, "EditMetadata"

    invoke-interface {v1, v2}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->F()Lcom/google/android/youtube/app/d;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->B:Lcom/google/android/youtube/core/model/Video;

    invoke-interface {v1, v2}, Lcom/google/android/youtube/app/d;->a(Lcom/google/android/youtube/core/model/Video;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/youtube/app/compat/m;)Z
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/youtube/app/compat/m;)Z

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->E()Lcom/google/android/youtube/app/compat/r;

    move-result-object v0

    const v1, 0x7f110005

    invoke-virtual {v0, v1, p1}, Lcom/google/android/youtube/app/compat/r;->a(ILcom/google/android/youtube/app/compat/m;)V

    const/4 v0, 0x1

    return v0
.end method

.method public final g_()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->finish()V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->f()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 11

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f04005d

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->setContentView(I)V

    const v0, 0x7f0b018e

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->b(I)V

    new-instance v0, Lcom/google/android/youtube/app/ui/v;

    const/16 v1, 0x3fc

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/ui/v;-><init>(Landroid/app/Activity;I)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->x:Lcom/google/android/youtube/app/ui/v;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->x:Lcom/google/android/youtube/app/ui/v;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/ui/v;->a(Lcom/google/android/youtube/app/ui/ab;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->x:Lcom/google/android/youtube/app/ui/v;

    const v1, 0x7f0b0203

    const v2, 0x7f0200d1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/ui/v;->a(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->A:I

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->x:Lcom/google/android/youtube/app/ui/v;

    const v1, 0x7f0b01fe

    const v2, 0x7f0200d0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/ui/v;->a(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->z:I

    new-instance v0, Lcom/google/android/youtube/app/ui/v;

    const/16 v1, 0x3f2

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/ui/v;-><init>(Landroid/app/Activity;I)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->y:Lcom/google/android/youtube/app/ui/v;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->y:Lcom/google/android/youtube/app/ui/v;

    const v1, 0x7f0b0201

    const v2, 0x7f0200cf

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/ui/v;->a(II)I

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->y:Lcom/google/android/youtube/app/ui/v;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/bm;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/honeycomb/phone/bm;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/v;->a(Lcom/google/android/youtube/app/ui/ab;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->p:Lcom/google/android/youtube/core/client/be;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->q:Lcom/google/android/youtube/core/client/bg;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->x:Lcom/google/android/youtube/app/ui/v;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->y:Lcom/google/android/youtube/app/ui/v;

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/youtube/app/adapter/bn;->a(Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/bg;Lcom/google/android/youtube/app/prefetch/d;Lcom/google/android/youtube/app/ui/v;Lcom/google/android/youtube/app/ui/v;)Lcom/google/android/youtube/app/adapter/cd;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->v:Lcom/google/android/youtube/app/adapter/cd;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->v:Lcom/google/android/youtube/app/adapter/cd;

    invoke-static {p0, v0}, Lcom/google/android/youtube/app/ui/bx;->a(Landroid/content/Context;Lcom/google/android/youtube/core/a/a;)Lcom/google/android/youtube/app/ui/bx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->w:Lcom/google/android/youtube/app/ui/bx;

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->f()V

    new-instance v0, Lcom/google/android/youtube/app/ui/dc;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->r:Lcom/google/android/youtube/core/async/UserAuthorizer;

    const v1, 0x7f07006b

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/youtube/core/ui/PagedView;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->v:Lcom/google/android/youtube/app/adapter/cd;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->w:Lcom/google/android/youtube/app/ui/bx;

    iget-object v6, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->n:Lcom/google/android/youtube/core/async/au;

    iget-object v7, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->o:Lcom/google/android/youtube/core/client/bc;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->I()Lcom/google/android/youtube/core/Analytics;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->s:Lcom/google/android/youtube/core/e;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->F()Lcom/google/android/youtube/app/d;

    move-result-object v10

    move-object v1, p0

    invoke-direct/range {v0 .. v10}, Lcom/google/android/youtube/app/ui/dc;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/ui/PagedView;Lcom/google/android/youtube/app/adapter/cd;Lcom/google/android/youtube/app/ui/bx;Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/app/d;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->u:Lcom/google/android/youtube/app/ui/dc;

    return-void
.end method

.method protected onPause()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onPause()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->D:Z

    return-void
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onResume()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->D:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->r:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0, p0, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bk;)V

    return-void
.end method

.method protected onStop()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onStop()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->u:Lcom/google/android/youtube/app/ui/dc;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/dc;->e()V

    return-void
.end method

.method protected final w()Ljava/lang/String;
    .locals 1

    const-string v0, "yt_upload"

    return-object v0
.end method
