.class public Lcom/google/android/youtube/app/honeycomb/phone/NewVersionAvailableActivity;
.super Landroid/app/Activity;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Landroid/content/SharedPreferences;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/content/Intent;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/NewVersionAvailableActivity;->d:Landroid/content/Intent;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/NewVersionAvailableActivity;->d:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/NewVersionAvailableActivity;->startActivity(Landroid/content/Intent;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/NewVersionAvailableActivity;->finish()V

    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/NewVersionAvailableActivity;->a()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/NewVersionAvailableActivity;->b:Landroid/widget/TextView;

    if-ne p1, v0, :cond_2

    const-string v2, "app"

    const-string v3, "prompt"

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/NewVersionAvailableActivity;->d:Landroid/content/Intent;

    if-eqz v0, :cond_1

    const-string v0, "suggest"

    move-object v1, v0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/NewVersionAvailableActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->X()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v2, v3, v1, v0}, Lcom/google/android/youtube/core/utils/m;->a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/NewVersionAvailableActivity;->finish()V

    :cond_0
    :goto_1
    return-void

    :cond_1
    const-string v0, "force"

    move-object v1, v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/NewVersionAvailableActivity;->c:Landroid/widget/TextView;

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/NewVersionAvailableActivity;->a()V

    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    const/4 v3, -0x2

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    const v0, 0x7f04005e

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/NewVersionAvailableActivity;->setContentView(I)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/NewVersionAvailableActivity;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/NewVersionAvailableActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->S()Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/NewVersionAvailableActivity;->a:Landroid/content/SharedPreferences;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/NewVersionAvailableActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "forward_intent"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/NewVersionAvailableActivity;->d:Landroid/content/Intent;

    const v0, 0x7f0700ea

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/NewVersionAvailableActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0700eb

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/NewVersionAvailableActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-static {p0}, Lcom/google/android/youtube/core/utils/Util;->g(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    invoke-static {p0}, Lcom/google/android/youtube/core/utils/Util;->g(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    const v0, 0x7f0700ed

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/NewVersionAvailableActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/NewVersionAvailableActivity;->b:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/NewVersionAvailableActivity;->b:Landroid/widget/TextView;

    new-instance v1, Lcom/google/android/youtube/app/ui/dg;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/ui/dg;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/NewVersionAvailableActivity;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0700ec

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/NewVersionAvailableActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/NewVersionAvailableActivity;->c:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/NewVersionAvailableActivity;->d:Landroid/content/Intent;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/NewVersionAvailableActivity;->c:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/NewVersionAvailableActivity;->b:Landroid/widget/TextView;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, 0x0

    invoke-direct {v1, v3, v3, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/NewVersionAvailableActivity;->c:Landroid/widget/TextView;

    new-instance v1, Lcom/google/android/youtube/app/ui/dg;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/ui/dg;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/NewVersionAvailableActivity;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method
