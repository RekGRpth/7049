.class final enum Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

.field public static final enum FAVORITES:Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

.field public static final enum HISTORY:Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

.field public static final enum MUSIC:Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

.field public static final enum TRENDING:Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

.field public static final enum TRENDING_WW:Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

.field public static final enum UPLOADS:Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

.field public static final enum WATCH_LATER:Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;


# instance fields
.field final feature:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field final isAccountFeed:Z

.field final labelStringId:I

.field final position:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    const-string v1, "WATCH_LATER"

    const/4 v2, 0x0

    const/4 v3, 0x0

    const v4, 0x7f0b0191

    sget-object v5, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->WATCH_LATER:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const/4 v6, 0x1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;-><init>(Ljava/lang/String;IIILcom/google/android/youtube/core/client/VideoStats2Client$Feature;Z)V

    sput-object v0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;->WATCH_LATER:Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    const-string v1, "FAVORITES"

    const/4 v2, 0x1

    const/4 v3, 0x1

    const v4, 0x7f0b0190

    sget-object v5, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->MY_FAVORITES:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const/4 v6, 0x1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;-><init>(Ljava/lang/String;IIILcom/google/android/youtube/core/client/VideoStats2Client$Feature;Z)V

    sput-object v0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;->FAVORITES:Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    const-string v1, "UPLOADS"

    const/4 v2, 0x2

    const/4 v3, 0x2

    const v4, 0x7f0b018e

    sget-object v5, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->MY_UPLOADS:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const/4 v6, 0x1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;-><init>(Ljava/lang/String;IIILcom/google/android/youtube/core/client/VideoStats2Client$Feature;Z)V

    sput-object v0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;->UPLOADS:Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    const-string v1, "HISTORY"

    const/4 v2, 0x3

    const/4 v3, 0x3

    const v4, 0x7f0b0192

    sget-object v5, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->WATCH_HISTORY:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const/4 v6, 0x1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;-><init>(Ljava/lang/String;IIILcom/google/android/youtube/core/client/VideoStats2Client$Feature;Z)V

    sput-object v0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;->HISTORY:Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    const-string v1, "TRENDING"

    const/4 v2, 0x4

    const/4 v3, 0x4

    const v4, 0x7f0b0183

    sget-object v5, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_TRENDING:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;-><init>(Ljava/lang/String;IIILcom/google/android/youtube/core/client/VideoStats2Client$Feature;Z)V

    sput-object v0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;->TRENDING:Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    const-string v1, "MUSIC"

    const/4 v2, 0x5

    const/4 v3, 0x5

    const v4, 0x7f0b0105

    sget-object v5, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_MUSIC:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;-><init>(Ljava/lang/String;IIILcom/google/android/youtube/core/client/VideoStats2Client$Feature;Z)V

    sput-object v0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;->MUSIC:Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    const-string v1, "TRENDING_WW"

    const/4 v2, 0x6

    const/4 v3, 0x6

    const v4, 0x7f0b0183

    sget-object v5, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_TRENDING:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;-><init>(Ljava/lang/String;IIILcom/google/android/youtube/core/client/VideoStats2Client$Feature;Z)V

    sput-object v0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;->TRENDING_WW:Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;->WATCH_LATER:Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;->FAVORITES:Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;->UPLOADS:Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;->HISTORY:Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;->TRENDING:Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;->MUSIC:Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;->TRENDING_WW:Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;->$VALUES:[Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIILcom/google/android/youtube/core/client/VideoStats2Client$Feature;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;->position:I

    iput p4, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;->labelStringId:I

    iput-object p5, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;->feature:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    iput-boolean p6, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;->isAccountFeed:Z

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;
    .locals 1

    const-class v0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    return-object v0
.end method

.method public static values()[Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;->$VALUES:[Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    invoke-virtual {v0}, [Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    return-object v0
.end method
