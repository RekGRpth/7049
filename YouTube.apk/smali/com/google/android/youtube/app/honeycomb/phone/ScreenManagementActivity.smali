.class public Lcom/google/android/youtube/app/honeycomb/phone/ScreenManagementActivity;
.super Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;
.source "SourceFile"


# instance fields
.field private n:Lcom/google/android/youtube/app/remote/bq;

.field private o:Lcom/google/android/youtube/core/ui/PagedListView;

.field private p:Landroid/widget/ArrayAdapter;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/ScreenManagementActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ScreenManagementActivity;->f()V

    return-void
.end method

.method static synthetic b(Lcom/google/android/youtube/app/honeycomb/phone/ScreenManagementActivity;)Landroid/widget/ArrayAdapter;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ScreenManagementActivity;->p:Landroid/widget/ArrayAdapter;

    return-object v0
.end method

.method private f()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ScreenManagementActivity;->n:Lcom/google/android/youtube/app/remote/bq;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/ch;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/honeycomb/phone/ch;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/ScreenManagementActivity;)V

    invoke-static {p0, v1}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/remote/bq;->b(Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/youtube/app/compat/m;)Z
    .locals 1

    invoke-virtual {p1}, Lcom/google/android/youtube/app/compat/m;->a()V

    const/4 v0, 0x1

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    const/4 v6, 0x0

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ScreenManagementActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->B()Lcom/google/android/youtube/app/remote/bq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ScreenManagementActivity;->n:Lcom/google/android/youtube/app/remote/bq;

    const v0, 0x7f040084

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/ScreenManagementActivity;->setContentView(I)V

    const v0, 0x7f0b015c

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/ScreenManagementActivity;->b(I)V

    new-instance v5, Lcom/google/android/youtube/app/honeycomb/phone/cd;

    invoke-direct {v5, p0}, Lcom/google/android/youtube/app/honeycomb/phone/cd;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/ScreenManagementActivity;)V

    const v0, 0x7f070128

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/ScreenManagementActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/ui/PagedListView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ScreenManagementActivity;->o:Lcom/google/android/youtube/core/ui/PagedListView;

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/ct;

    const v3, 0x7f040083

    const v4, 0x7f070127

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/app/honeycomb/phone/ct;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/ScreenManagementActivity;Landroid/content/Context;IILandroid/view/View$OnClickListener;B)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ScreenManagementActivity;->p:Landroid/widget/ArrayAdapter;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ScreenManagementActivity;->o:Lcom/google/android/youtube/core/ui/PagedListView;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ScreenManagementActivity;->p:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/PagedListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ScreenManagementActivity;->o:Lcom/google/android/youtube/core/ui/PagedListView;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/cf;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/honeycomb/phone/cf;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/ScreenManagementActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/PagedListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-virtual {p0, v6}, Lcom/google/android/youtube/app/honeycomb/phone/ScreenManagementActivity;->k(Z)V

    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onResume()V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ScreenManagementActivity;->f()V

    return-void
.end method

.method protected final w()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method
