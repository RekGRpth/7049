.class public Lcom/google/android/youtube/app/honeycomb/phone/ScreenPairingActivity;
.super Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;
.source "SourceFile"


# instance fields
.field private n:Lcom/google/android/youtube/app/ui/bz;

.field private o:Ljava/lang/String;

.field private p:I

.field private q:Z

.field private r:Lcom/google/android/youtube/app/remote/ad;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;-><init>()V

    return-void
.end method

.method private static a(Landroid/net/Uri;)I
    .locals 3

    const-string v0, "videoPosition"

    invoke-virtual {p0, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    :goto_0
    return v0

    :catch_0
    move-exception v1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid value for video positon "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;I)Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/youtube/app/honeycomb/phone/ScreenPairingActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    if-eqz p1, :cond_0

    const-string v1, "video_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "video_position_ms"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :cond_0
    return-object v0
.end method

.method private a(III)V
    .locals 4

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/ScreenPairingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const v1, 0x7f0700ef

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {p0}, Lcom/google/android/youtube/core/utils/Util;->g(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    const v1, 0x7f0700f0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/ScreenPairingActivity;Z)Z
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ScreenPairingActivity;->q:Z

    return v0
.end method


# virtual methods
.method protected final a(I)Landroid/app/Dialog;
    .locals 1

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ScreenPairingActivity;->n:Lcom/google/android/youtube/app/ui/bz;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/bz;->a(I)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x403
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/youtube/app/compat/m;)Z
    .locals 1

    invoke-virtual {p1}, Lcom/google/android/youtube/app/compat/m;->a()V

    const/4 v0, 0x1

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ScreenPairingActivity;->n:Lcom/google/android/youtube/app/ui/bz;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/youtube/app/ui/bz;->a(IILandroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12

    const/4 v11, 0x1

    const/4 v10, 0x0

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ScreenPairingActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->K()Lcom/google/android/youtube/app/remote/ad;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ScreenPairingActivity;->r:Lcom/google/android/youtube/app/remote/ad;

    new-instance v7, Lcom/google/android/youtube/app/honeycomb/phone/cv;

    invoke-direct {v7, p0}, Lcom/google/android/youtube/app/honeycomb/phone/cv;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/ScreenPairingActivity;)V

    new-instance v0, Lcom/google/android/youtube/app/ui/bz;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->K()Lcom/google/android/youtube/app/remote/ad;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->B()Lcom/google/android/youtube/app/remote/bq;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->A()Lcom/google/android/youtube/app/remote/br;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->z()Lcom/google/android/youtube/app/remote/bf;

    move-result-object v5

    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->i()Lcom/google/android/youtube/core/Analytics;

    move-result-object v6

    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/e;

    move-result-object v8

    const v9, 0x7f040086

    move-object v1, p0

    invoke-direct/range {v0 .. v9}, Lcom/google/android/youtube/app/ui/bz;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/app/remote/ad;Lcom/google/android/youtube/app/remote/bq;Lcom/google/android/youtube/app/remote/br;Lcom/google/android/youtube/app/remote/bf;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/app/ui/cj;Lcom/google/android/youtube/core/e;I)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ScreenPairingActivity;->n:Lcom/google/android/youtube/app/ui/bz;

    if-nez p1, :cond_1

    move v0, v10

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ScreenPairingActivity;->q:Z

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ScreenPairingActivity;->q:Z

    if-eqz v0, :cond_2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/ScreenPairingActivity;->startActivity(Landroid/content/Intent;)V

    :cond_0
    :goto_1
    const v0, 0x7f0b015a

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/ScreenPairingActivity;->b(I)V

    const v0, 0x7f070129

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/ScreenPairingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {p0}, Lcom/google/android/youtube/core/utils/Util;->g(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1, v11}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    const v0, 0x7f07012a

    const v1, 0x7f0b0230

    invoke-direct {p0, v0, v11, v1}, Lcom/google/android/youtube/app/honeycomb/phone/ScreenPairingActivity;->a(III)V

    const v0, 0x7f07012b

    const/4 v1, 0x2

    const v2, 0x7f0b0231

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/youtube/app/honeycomb/phone/ScreenPairingActivity;->a(III)V

    const v0, 0x7f07012c

    const/4 v1, 0x3

    const v2, 0x7f0b0232

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/youtube/app/honeycomb/phone/ScreenPairingActivity;->a(III)V

    invoke-virtual {p0, v10}, Lcom/google/android/youtube/app/honeycomb/phone/ScreenPairingActivity;->k(Z)V

    return-void

    :cond_1
    const-string v0, "paired"

    invoke-virtual {p1, v0, v10}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0

    :cond_2
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ScreenPairingActivity;->n:Lcom/google/android/youtube/app/ui/bz;

    const-string v1, "pairing_code"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/bz;->b(Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected onPause()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onPause()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ScreenPairingActivity;->n:Lcom/google/android/youtube/app/ui/bz;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/bz;->b()V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "paired"

    iget-boolean v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ScreenPairingActivity;->q:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "pairing_code"

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ScreenPairingActivity;->n:Lcom/google/android/youtube/app/ui/bz;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/ui/bz;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected onStart()V
    .locals 4

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onStart()V

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ScreenPairingActivity;->q:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ScreenPairingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "video_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ScreenPairingActivity;->o:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "video_position_ms"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ScreenPairingActivity;->p:I

    :cond_2
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ScreenPairingActivity;->n:Lcom/google/android/youtube/app/ui/bz;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ScreenPairingActivity;->o:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/ui/bz;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ScreenPairingActivity;->n:Lcom/google/android/youtube/app/ui/bz;

    iget v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ScreenPairingActivity;->p:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/youtube/app/ui/bz;->a(J)V

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    const-string v2, "remote"

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ScreenPairingActivity;->n:Lcom/google/android/youtube/app/ui/bz;

    const-string v2, "pairingCode"

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/ui/bz;->b(Ljava/lang/String;)V

    const-string v1, "videoId"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ScreenPairingActivity;->o:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/ScreenPairingActivity;->a(Landroid/net/Uri;)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ScreenPairingActivity;->p:I

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ScreenPairingActivity;->n:Lcom/google/android/youtube/app/ui/bz;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ScreenPairingActivity;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/bz;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ScreenPairingActivity;->n:Lcom/google/android/youtube/app/ui/bz;

    iget v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ScreenPairingActivity;->p:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/ui/bz;->a(J)V

    :cond_3
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/ScreenPairingActivity;->setIntent(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onStop()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onStop()V

    return-void
.end method

.method protected final w()Ljava/lang/String;
    .locals 1

    const-string v0, "yt_add_screen"

    return-object v0
.end method
