.class public abstract Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;
.super Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/ui/b;


# instance fields
.field private n:Landroid/app/ActionBar;

.field private o:Ljava/util/ArrayList;

.field private p:Lcom/google/android/youtube/app/honeycomb/phone/cw;

.field private q:Z

.field private r:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->n:Landroid/app/ActionBar;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->n:Landroid/app/ActionBar;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setNavigationMode(I)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->o:Ljava/util/ArrayList;

    return-void
.end method

.method protected onPause()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onPause()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->p:Lcom/google/android/youtube/app/honeycomb/phone/cw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->p:Lcom/google/android/youtube/app/honeycomb/phone/cw;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/cw;->a()V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->r:Z

    return-void
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onResume()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->r:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->p:Lcom/google/android/youtube/app/honeycomb/phone/cw;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->q:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/TabbedActivity;->p:Lcom/google/android/youtube/app/honeycomb/phone/cw;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/cw;->b()V

    :cond_0
    return-void
.end method
