.class public final enum Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer$DisplayMode;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer$DisplayMode;

.field public static final enum MY_SUBSCRIPTIONS:Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer$DisplayMode;

.field public static final enum WHAT_TO_WATCH:Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer$DisplayMode;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer$DisplayMode;

    const-string v1, "WHAT_TO_WATCH"

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer$DisplayMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer$DisplayMode;->WHAT_TO_WATCH:Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer$DisplayMode;

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer$DisplayMode;

    const-string v1, "MY_SUBSCRIPTIONS"

    invoke-direct {v0, v1, v3}, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer$DisplayMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer$DisplayMode;->MY_SUBSCRIPTIONS:Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer$DisplayMode;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer$DisplayMode;

    sget-object v1, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer$DisplayMode;->WHAT_TO_WATCH:Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer$DisplayMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer$DisplayMode;->MY_SUBSCRIPTIONS:Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer$DisplayMode;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer$DisplayMode;->$VALUES:[Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer$DisplayMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer$DisplayMode;
    .locals 1

    const-class v0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer$DisplayMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer$DisplayMode;

    return-object v0
.end method

.method public static values()[Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer$DisplayMode;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer$DisplayMode;->$VALUES:[Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer$DisplayMode;

    invoke-virtual {v0}, [Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer$DisplayMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer$DisplayMode;

    return-object v0
.end method
