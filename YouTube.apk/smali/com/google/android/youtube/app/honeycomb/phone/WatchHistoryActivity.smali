.class public Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;
.super Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/bk;


# instance fields
.field private n:Landroid/content/res/Resources;

.field private o:Lcom/google/android/youtube/core/async/au;

.field private p:Lcom/google/android/youtube/core/client/bc;

.field private q:Lcom/google/android/youtube/core/client/be;

.field private r:Lcom/google/android/youtube/core/client/bg;

.field private s:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private t:Lcom/google/android/youtube/core/model/UserAuth;

.field private u:Lcom/google/android/youtube/core/e;

.field private v:Lcom/google/android/youtube/app/ui/bx;

.field private w:Lcom/google/android/youtube/app/ui/ea;

.field private x:Lcom/google/android/youtube/core/a/a;

.field private y:Lcom/google/android/youtube/app/ui/v;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;)Lcom/google/android/youtube/core/a/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->x:Lcom/google/android/youtube/core/a/a;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;Lcom/google/android/youtube/core/model/Video;)V
    .locals 4

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/dc;

    invoke-direct {v0, p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/dc;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;Lcom/google/android/youtube/core/model/Video;)V

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->p:Lcom/google/android/youtube/core/client/bc;

    iget-object v2, p1, Lcom/google/android/youtube/core/model/Video;->editUri:Landroid/net/Uri;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->t:Lcom/google/android/youtube/core/model/UserAuth;

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/youtube/core/client/bc;->e(Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;)Lcom/google/android/youtube/core/e;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->u:Lcom/google/android/youtube/core/e;

    return-object v0
.end method

.method private f()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->v:Lcom/google/android/youtube/app/ui/bx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->v:Lcom/google/android/youtube/app/ui/bx;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->n:Landroid/content/res/Resources;

    const v2, 0x7f0a001d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/bx;->a(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected final a(I)Landroid/app/Dialog;
    .locals 1

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->y:Lcom/google/android/youtube/app/ui/v;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/v;->b()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x402
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/youtube/app/YouTubeApplication;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/youtube/app/YouTubeApplication;)V

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->n:Landroid/content/res/Resources;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->U()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->s:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->a()Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->p:Lcom/google/android/youtube/core/client/bc;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->e_()Lcom/google/android/youtube/core/client/be;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->q:Lcom/google/android/youtube/core/client/be;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->w()Lcom/google/android/youtube/core/client/bg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->r:Lcom/google/android/youtube/core/client/bg;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->u:Lcom/google/android/youtube/core/e;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->p:Lcom/google/android/youtube/core/client/bc;

    invoke-interface {v0}, Lcom/google/android/youtube/core/client/bc;->s()Lcom/google/android/youtube/core/async/au;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->o:Lcom/google/android/youtube/core/async/au;

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 4

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->t:Lcom/google/android/youtube/core/model/UserAuth;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->w:Lcom/google/android/youtube/app/ui/ea;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->p:Lcom/google/android/youtube/core/client/bc;

    invoke-interface {v3}, Lcom/google/android/youtube/core/client/bc;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->g(Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/ea;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->finish()V

    return-void
.end method

.method public final g_()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->finish()V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->y:Lcom/google/android/youtube/app/ui/v;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/v;->a()V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->f()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12

    const/4 v6, 0x1

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f0400ab

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->setContentView(I)V

    const v0, 0x7f0b0192

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->b(I)V

    new-instance v0, Lcom/google/android/youtube/app/ui/v;

    const/16 v1, 0x402

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/ui/v;-><init>(Landroid/app/Activity;I)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->y:Lcom/google/android/youtube/app/ui/v;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->y:Lcom/google/android/youtube/app/ui/v;

    const v1, 0x7f0b01f1

    const v2, 0x7f0200d0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/ui/v;->a(II)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->y:Lcom/google/android/youtube/app/ui/v;

    new-instance v2, Lcom/google/android/youtube/app/honeycomb/phone/db;

    invoke-direct {v2, p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/db;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;I)V

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/ui/v;->a(Lcom/google/android/youtube/app/ui/ab;)V

    new-instance v0, Lcom/google/android/youtube/app/adapter/bx;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->q:Lcom/google/android/youtube/core/client/be;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->r:Lcom/google/android/youtube/core/client/bg;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->y:Lcom/google/android/youtube/app/ui/v;

    const v4, 0x7f0400ba

    invoke-static {p0, v1, v2, v3, v4}, Lcom/google/android/youtube/app/adapter/bn;->a(Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/bg;Lcom/google/android/youtube/app/ui/v;I)Lcom/google/android/youtube/app/adapter/bm;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/youtube/app/adapter/bx;-><init>(Lcom/google/android/youtube/core/a/a;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->x:Lcom/google/android/youtube/core/a/a;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->x:Lcom/google/android/youtube/core/a/a;

    invoke-static {p0, v0}, Lcom/google/android/youtube/app/ui/bx;->a(Landroid/content/Context;Lcom/google/android/youtube/core/a/a;)Lcom/google/android/youtube/app/ui/bx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->v:Lcom/google/android/youtube/app/ui/bx;

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->f()V

    new-instance v0, Lcom/google/android/youtube/app/ui/ea;

    const v1, 0x7f070169

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/core/ui/PagedView;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->v:Lcom/google/android/youtube/app/ui/bx;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->o:Lcom/google/android/youtube/core/async/au;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->u:Lcom/google/android/youtube/core/e;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->F()Lcom/google/android/youtube/app/d;

    move-result-object v7

    sget-object v9, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->WATCH_HISTORY:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->I()Lcom/google/android/youtube/core/Analytics;

    move-result-object v10

    sget-object v11, Lcom/google/android/youtube/core/Analytics$VideoCategory;->WatchHistory:Lcom/google/android/youtube/core/Analytics$VideoCategory;

    move-object v1, p0

    move v8, v6

    invoke-direct/range {v0 .. v11}, Lcom/google/android/youtube/app/ui/ea;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/PagedView;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/e;ZLcom/google/android/youtube/app/d;ZLcom/google/android/youtube/core/client/VideoStats2Client$Feature;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/Analytics$VideoCategory;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->w:Lcom/google/android/youtube/app/ui/ea;

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onResume()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->s:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0, p0, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bk;)V

    return-void
.end method

.method protected onStop()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onStop()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchHistoryActivity;->w:Lcom/google/android/youtube/app/ui/ea;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/ea;->e()V

    return-void
.end method

.method protected final w()Ljava/lang/String;
    .locals 1

    const-string v0, "yt_your_channel"

    return-object v0
.end method
