.class public Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;
.super Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/prefetch/f;
.implements Lcom/google/android/youtube/core/async/bk;


# instance fields
.field private n:Landroid/content/res/Resources;

.field private o:Lcom/google/android/youtube/core/async/au;

.field private p:Lcom/google/android/youtube/core/client/bc;

.field private q:Lcom/google/android/youtube/core/client/be;

.field private r:Lcom/google/android/youtube/core/client/bg;

.field private s:Lcom/google/android/youtube/app/prefetch/d;

.field private t:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private u:Lcom/google/android/youtube/core/model/UserAuth;

.field private v:Lcom/google/android/youtube/core/e;

.field private w:Lcom/google/android/youtube/app/ui/ea;

.field private x:Lcom/google/android/youtube/app/adapter/bm;

.field private y:Lcom/google/android/youtube/app/ui/bx;

.field private z:Lcom/google/android/youtube/app/ui/v;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;)Lcom/google/android/youtube/app/adapter/bm;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->x:Lcom/google/android/youtube/app/adapter/bm;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;Lcom/google/android/youtube/core/model/Video;)V
    .locals 4

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/df;

    invoke-direct {v0, p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/df;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;Lcom/google/android/youtube/core/model/Video;)V

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->p:Lcom/google/android/youtube/core/client/bc;

    iget-object v2, p1, Lcom/google/android/youtube/core/model/Video;->editUri:Landroid/net/Uri;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->u:Lcom/google/android/youtube/core/model/UserAuth;

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/youtube/core/client/bc;->d(Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;)Lcom/google/android/youtube/core/e;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->v:Lcom/google/android/youtube/core/e;

    return-object v0
.end method

.method private f()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->y:Lcom/google/android/youtube/app/ui/bx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->y:Lcom/google/android/youtube/app/ui/bx;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->n:Landroid/content/res/Resources;

    const v2, 0x7f0a001f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/bx;->a(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected final a(I)Landroid/app/Dialog;
    .locals 1

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->z:Lcom/google/android/youtube/app/ui/v;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/v;->b()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3fe
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/youtube/app/YouTubeApplication;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/youtube/app/YouTubeApplication;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->n:Landroid/content/res/Resources;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->U()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->t:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->a()Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->p:Lcom/google/android/youtube/core/client/bc;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->e_()Lcom/google/android/youtube/core/client/be;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->q:Lcom/google/android/youtube/core/client/be;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->w()Lcom/google/android/youtube/core/client/bg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->r:Lcom/google/android/youtube/core/client/bg;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->E()Lcom/google/android/youtube/app/prefetch/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->s:Lcom/google/android/youtube/app/prefetch/d;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->v:Lcom/google/android/youtube/core/e;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->p:Lcom/google/android/youtube/core/client/bc;

    invoke-interface {v0}, Lcom/google/android/youtube/core/client/bc;->r()Lcom/google/android/youtube/core/async/au;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->o:Lcom/google/android/youtube/core/async/au;

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 4

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->u:Lcom/google/android/youtube/core/model/UserAuth;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->w:Lcom/google/android/youtube/app/ui/ea;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->p:Lcom/google/android/youtube/core/client/bc;

    invoke-interface {v3}, Lcom/google/android/youtube/core/client/bc;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->f(Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/ea;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->finish()V

    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->x:Lcom/google/android/youtube/app/adapter/bm;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/bm;->notifyDataSetChanged()V

    return-void
.end method

.method public final g_()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->finish()V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->f()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 13

    const v9, 0x7f070178

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f0400b1

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->setContentView(I)V

    const v0, 0x7f0b0191

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->b(I)V

    new-instance v0, Lcom/google/android/youtube/app/ui/v;

    const/16 v1, 0x3fe

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/ui/v;-><init>(Landroid/app/Activity;I)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->z:Lcom/google/android/youtube/app/ui/v;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->z:Lcom/google/android/youtube/app/ui/v;

    const v1, 0x7f0b01ef

    const v2, 0x7f0200d0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/ui/v;->a(II)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->z:Lcom/google/android/youtube/app/ui/v;

    new-instance v2, Lcom/google/android/youtube/app/honeycomb/phone/dd;

    invoke-direct {v2, p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/dd;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;I)V

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/ui/v;->a(Lcom/google/android/youtube/app/ui/ab;)V

    invoke-virtual {p0, v9}, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->findViewById(I)Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->q:Lcom/google/android/youtube/core/client/be;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->r:Lcom/google/android/youtube/core/client/bg;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->s:Lcom/google/android/youtube/app/prefetch/d;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->z:Lcom/google/android/youtube/app/ui/v;

    new-instance v3, Lcom/google/android/youtube/app/adapter/cj;

    invoke-direct {v3, p0}, Lcom/google/android/youtube/app/adapter/cj;-><init>(Landroid/content/Context;)V

    sget-object v4, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;->SMALL:Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;

    invoke-static {p0, v0, v4, v8}, Lcom/google/android/youtube/app/adapter/cm;->a(Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;Z)Lcom/google/android/youtube/app/adapter/cm;

    move-result-object v0

    new-instance v4, Lcom/google/android/youtube/app/adapter/h;

    invoke-static {p0}, Lcom/google/android/youtube/core/utils/j;->d(Landroid/content/Context;)Z

    move-result v5

    const/16 v6, 0x8

    invoke-direct {v4, v1, v5, v6, v8}, Lcom/google/android/youtube/app/adapter/h;-><init>(Lcom/google/android/youtube/app/prefetch/d;ZIZ)V

    new-instance v1, Lcom/google/android/youtube/app/adapter/aj;

    invoke-direct {v1, v2}, Lcom/google/android/youtube/app/adapter/aj;-><init>(Lcom/google/android/youtube/app/ui/v;)V

    new-instance v2, Lcom/google/android/youtube/app/adapter/af;

    invoke-direct {v2}, Lcom/google/android/youtube/app/adapter/af;-><init>()V

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/app/adapter/bm;

    const v2, 0x7f0400bd

    invoke-direct {v1, p0, v2, v0}, Lcom/google/android/youtube/app/adapter/bm;-><init>(Landroid/content/Context;ILcom/google/android/youtube/app/adapter/bv;)V

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->x:Lcom/google/android/youtube/app/adapter/bm;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->x:Lcom/google/android/youtube/app/adapter/bm;

    invoke-static {p0, v0}, Lcom/google/android/youtube/app/ui/bx;->a(Landroid/content/Context;Lcom/google/android/youtube/core/a/a;)Lcom/google/android/youtube/app/ui/bx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->y:Lcom/google/android/youtube/app/ui/bx;

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->f()V

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/de;

    invoke-virtual {p0, v9}, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/youtube/core/ui/PagedView;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->y:Lcom/google/android/youtube/app/ui/bx;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->o:Lcom/google/android/youtube/core/async/au;

    iget-object v6, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->v:Lcom/google/android/youtube/core/e;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->F()Lcom/google/android/youtube/app/d;

    move-result-object v8

    sget-object v10, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->WATCH_LATER:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->I()Lcom/google/android/youtube/core/Analytics;

    move-result-object v11

    sget-object v12, Lcom/google/android/youtube/core/Analytics$VideoCategory;->WatchLater:Lcom/google/android/youtube/core/Analytics$VideoCategory;

    move-object v1, p0

    move-object v2, p0

    move v9, v7

    invoke-direct/range {v0 .. v12}, Lcom/google/android/youtube/app/honeycomb/phone/de;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;Landroid/app/Activity;Lcom/google/android/youtube/core/ui/PagedView;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/e;ZLcom/google/android/youtube/app/d;ZLcom/google/android/youtube/core/client/VideoStats2Client$Feature;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/Analytics$VideoCategory;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->w:Lcom/google/android/youtube/app/ui/ea;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->s:Lcom/google/android/youtube/app/prefetch/d;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/prefetch/d;->a(Lcom/google/android/youtube/app/prefetch/f;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->s:Lcom/google/android/youtube/app/prefetch/d;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/prefetch/d;->b(Lcom/google/android/youtube/app/prefetch/f;)V

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onDestroy()V

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onResume()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->t:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0, p0, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bk;)V

    return-void
.end method

.method protected onStop()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onStop()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchLaterActivity;->w:Lcom/google/android/youtube/app/ui/ea;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/ea;->e()V

    return-void
.end method

.method protected final w()Ljava/lang/String;
    .locals 1

    const-string v0, "yt_your_channel"

    return-object v0
.end method
