.class public abstract Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/compat/i;
.implements Lcom/google/android/youtube/app/compat/j;
.implements Lcom/google/android/youtube/app/remote/ag;
.implements Lcom/google/android/youtube/core/async/bl;


# instance fields
.field private A:Lcom/google/android/youtube/app/remote/ad;

.field private B:Lcom/google/android/youtube/app/honeycomb/phone/bb;

.field private C:Z

.field private D:Lcom/google/android/youtube/app/compat/o;

.field private E:Lcom/google/android/youtube/app/honeycomb/ui/j;

.field private F:Lcom/google/android/youtube/app/honeycomb/ui/r;

.field private n:Lcom/google/android/youtube/app/YouTubeApplication;

.field private o:Landroid/content/SharedPreferences;

.field private p:Lcom/google/android/youtube/core/Analytics;

.field private q:Lcom/google/android/youtube/app/d;

.field private r:Lcom/google/android/youtube/app/compat/SupportActionBar;

.field private s:Lcom/google/android/youtube/app/compat/r;

.field private t:Lcom/google/android/youtube/app/compat/m;

.field private u:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

.field private v:Z

.field private w:Lcom/google/android/youtube/app/ui/ak;

.field private x:Z

.field private y:Lcom/google/android/youtube/app/ui/bw;

.field private z:Lcom/google/android/youtube/core/async/UserAuthorizer;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->f()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Z)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->v:Z

    return v0
.end method

.method private f()V
    .locals 2

    new-instance v0, Lcom/google/android/youtube/app/compat/f;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/compat/f;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    return-void
.end method


# virtual methods
.method protected final D()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->r:Lcom/google/android/youtube/app/compat/SupportActionBar;

    const/4 v1, 0x0

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(II)V

    return-void
.end method

.method public final E()Lcom/google/android/youtube/app/compat/r;
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->s:Lcom/google/android/youtube/app/compat/r;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/youtube/app/compat/r;

    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/compat/r;-><init>(Landroid/content/Context;Landroid/view/MenuInflater;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->s:Lcom/google/android/youtube/app/compat/r;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->s:Lcom/google/android/youtube/app/compat/r;

    return-object v0
.end method

.method protected final F()Lcom/google/android/youtube/app/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->q:Lcom/google/android/youtube/app/d;

    return-object v0
.end method

.method public G()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->finish()V

    return-void
.end method

.method protected final H()Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->u:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    return-object v0
.end method

.method public final I()Lcom/google/android/youtube/core/Analytics;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->p:Lcom/google/android/youtube/core/Analytics;

    return-object v0
.end method

.method protected final J()Lcom/google/android/youtube/app/honeycomb/ui/j;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->E:Lcom/google/android/youtube/app/honeycomb/ui/j;

    return-object v0
.end method

.method protected final K()Lcom/google/android/youtube/app/honeycomb/phone/bb;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->B:Lcom/google/android/youtube/app/honeycomb/phone/bb;

    return-object v0
.end method

.method protected final L()V
    .locals 2

    sget v0, Lcom/google/android/youtube/core/utils/Util;->a:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/view/Window;->requestFeature(I)Z

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->C:Z

    goto :goto_0
.end method

.method protected final M()Lcom/google/android/youtube/app/honeycomb/ui/r;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->F:Lcom/google/android/youtube/app/honeycomb/ui/r;

    return-object v0
.end method

.method protected a(I)Landroid/app/Dialog;
    .locals 1

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->n:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->s()Lcom/google/android/youtube/core/async/by;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/async/by;->a(Landroid/app/Activity;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method public a(Landroid/app/Activity;Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 0

    return-void
.end method

.method protected a(Lcom/google/android/youtube/app/YouTubeApplication;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/android/youtube/app/remote/RemoteControl;Z)V
    .locals 1

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->y:Lcom/google/android/youtube/app/ui/bw;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/bw;->a(Lcom/google/android/youtube/app/remote/RemoteControl;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->E:Lcom/google/android/youtube/app/honeycomb/ui/j;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->E:Lcom/google/android/youtube/app/honeycomb/ui/j;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/honeycomb/ui/j;->a(Lcom/google/android/youtube/app/remote/RemoteControl;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->y:Lcom/google/android/youtube/app/ui/bw;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/bw;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->E:Lcom/google/android/youtube/app/honeycomb/ui/j;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->E:Lcom/google/android/youtube/app/honeycomb/ui/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/ui/j;->a()V

    goto :goto_0
.end method

.method protected a(Lcom/google/android/youtube/app/compat/m;)Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->u:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Lcom/google/android/youtube/app/compat/m;)Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->B:Lcom/google/android/youtube/app/honeycomb/phone/bb;

    invoke-virtual {v1, p1}, Lcom/google/android/youtube/app/honeycomb/phone/bb;->a(Lcom/google/android/youtube/app/compat/m;)V

    return v0
.end method

.method public a(Lcom/google/android/youtube/app/compat/t;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->u:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Lcom/google/android/youtube/app/compat/t;)Z

    move-result v0

    return v0
.end method

.method protected b(I)V
    .locals 2

    const/16 v1, 0x8

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->r:Lcom/google/android/youtube/app/compat/SupportActionBar;

    invoke-virtual {v0, v1, v1}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(II)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->r:Lcom/google/android/youtube/app/compat/SupportActionBar;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(I)V

    return-void
.end method

.method protected b(Ljava/lang/String;)V
    .locals 2

    const/16 v1, 0x8

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->r:Lcom/google/android/youtube/app/compat/SupportActionBar;

    invoke-virtual {v0, v1, v1}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(II)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->r:Lcom/google/android/youtube/app/compat/SupportActionBar;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected b(Lcom/google/android/youtube/app/compat/m;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->B:Lcom/google/android/youtube/app/honeycomb/phone/bb;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/bb;->d()V

    const/4 v0, 0x1

    return v0
.end method

.method protected e()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getMenuInflater()Landroid/view/MenuInflater;
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    return-object v0
.end method

.method public i()Z
    .locals 3

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "ancestor_classname"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    :try_start_0
    new-instance v1, Landroid/content/Intent;

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v0, 0x24000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->finish()V

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v2

    :catch_0
    move-exception v0

    const-string v1, "Target Activity class for Up event not found"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->q:Lcom/google/android/youtube/app/d;

    invoke-interface {v0}, Lcom/google/android/youtube/app/d;->c()V

    goto :goto_0
.end method

.method public final j()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->C:Z

    return v0
.end method

.method public final k()I
    .locals 1

    const v0, 0x7f07013e

    return v0
.end method

.method protected final k(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->E:Lcom/google/android/youtube/app/honeycomb/ui/j;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/honeycomb/ui/j;->a(Z)V

    :cond_0
    return-void
.end method

.method public final l()Lcom/google/android/youtube/app/compat/SupportActionBar;
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->r:Lcom/google/android/youtube/app/compat/SupportActionBar;

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/google/android/youtube/app/compat/SupportActionBar;->b(Landroid/app/Activity;)Lcom/google/android/youtube/app/compat/SupportActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->r:Lcom/google/android/youtube/app/compat/SupportActionBar;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->r:Lcom/google/android/youtube/app/compat/SupportActionBar;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02005a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->r:Lcom/google/android/youtube/app/compat/SupportActionBar;

    return-object v0
.end method

.method public final l_()V
    .locals 2

    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->l_()V

    sget v0, Lcom/google/android/youtube/core/utils/Util;->a:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->v:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->v:Z

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/dh;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/honeycomb/phone/dh;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;)V

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->n:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->U()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;IILandroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/youtube/core/utils/m;->a(Landroid/app/Activity;IILandroid/content/Intent;)Z

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->D:Lcom/google/android/youtube/app/compat/o;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->D:Lcom/google/android/youtube/app/compat/o;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/compat/o;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->D:Lcom/google/android/youtube/app/compat/o;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/compat/o;->a()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->F:Lcom/google/android/youtube/app/honeycomb/ui/r;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->B:Lcom/google/android/youtube/app/honeycomb/phone/bb;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/honeycomb/phone/bb;->a()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/honeycomb/ui/r;->b(Landroid/view/View;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9

    const/4 v2, 0x4

    const/4 v0, 0x3

    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->setDefaultKeyMode(I)V

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->setVolumeControlStream(I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->x:Z

    const v0, 0x7f040094

    invoke-super {p0, v0}, Landroid/support/v4/app/FragmentActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->l()Lcom/google/android/youtube/app/compat/SupportActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->r:Lcom/google/android/youtube/app/compat/SupportActionBar;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->r:Lcom/google/android/youtube/app/compat/SupportActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/compat/SupportActionBar;->b(I)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->e()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->r:Lcom/google/android/youtube/app/compat/SupportActionBar;

    invoke-virtual {v0, v2, v2}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->n:Lcom/google/android/youtube/app/YouTubeApplication;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->n:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->i()Lcom/google/android/youtube/core/Analytics;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->p:Lcom/google/android/youtube/core/Analytics;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->n:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/youtube/app/YouTubeApplication;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->n:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->S()Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->o:Landroid/content/SharedPreferences;

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/l;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/honeycomb/l;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->q:Lcom/google/android/youtube/app/d;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->q:Lcom/google/android/youtube/app/d;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->w()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->E()Lcom/google/android/youtube/app/compat/r;

    move-result-object v2

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/youtube/app/d;Ljava/lang/String;Lcom/google/android/youtube/app/compat/r;)Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->u:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    new-instance v0, Lcom/google/android/youtube/app/ui/ak;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/ui/ak;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->w:Lcom/google/android/youtube/app/ui/ak;

    new-instance v0, Lcom/google/android/youtube/app/ui/bw;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->p:Lcom/google/android/youtube/core/Analytics;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->n:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->n()Lcom/google/android/youtube/app/k;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/youtube/app/k;->t()I

    move-result v2

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/youtube/app/ui/bw;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/Analytics;I)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->y:Lcom/google/android/youtube/app/ui/bw;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->n:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->U()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->z:Lcom/google/android/youtube/core/async/UserAuthorizer;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->z:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bl;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->n:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->K()Lcom/google/android/youtube/app/remote/ad;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->A:Lcom/google/android/youtube/app/remote/ad;

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/bb;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->A:Lcom/google/android/youtube/app/remote/ad;

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/bb;-><init>(Landroid/content/Context;Lcom/google/android/youtube/app/remote/ad;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->B:Lcom/google/android/youtube/app/honeycomb/phone/bb;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->B:Lcom/google/android/youtube/app/honeycomb/phone/bb;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/bb;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->B:Lcom/google/android/youtube/app/honeycomb/phone/bb;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e0009

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/bb;->b(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->u:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->B:Lcom/google/android/youtube/app/honeycomb/phone/bb;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Lcom/google/android/youtube/app/honeycomb/ui/i;)V

    invoke-static {p0}, Lcom/google/android/youtube/core/utils/Util;->g(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v8

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->n:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->n()Lcom/google/android/youtube/app/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/k;->p()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/ui/j;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->n:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->a()Lcom/google/android/youtube/core/client/bc;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->n:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->e_()Lcom/google/android/youtube/core/client/be;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->z:Lcom/google/android/youtube/core/async/UserAuthorizer;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->q:Lcom/google/android/youtube/app/d;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->n:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/e;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->p:Lcom/google/android/youtube/core/Analytics;

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/youtube/app/honeycomb/ui/j;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/core/Analytics;Landroid/graphics/Typeface;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->E:Lcom/google/android/youtube/app/honeycomb/ui/j;

    :cond_1
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/ui/r;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->o:Landroid/content/SharedPreferences;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->n:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->A()Lcom/google/android/youtube/app/remote/br;

    move-result-object v2

    invoke-direct {v0, p0, v1, v8, v2}, Lcom/google/android/youtube/app/honeycomb/ui/r;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Landroid/content/SharedPreferences;Landroid/graphics/Typeface;Lcom/google/android/youtube/app/remote/br;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->F:Lcom/google/android/youtube/app/honeycomb/ui/r;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->B:Lcom/google/android/youtube/app/honeycomb/phone/bb;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->F:Lcom/google/android/youtube/app/honeycomb/ui/r;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/bb;->a(Lcom/google/android/youtube/app/honeycomb/phone/bc;)V

    return-void
.end method

.method protected final onCreateDialog(I)Landroid/app/Dialog;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method protected final onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    sparse-switch p1, :sswitch_data_0

    if-eqz p2, :cond_1

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->n:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->s()Lcom/google/android/youtube/core/async/by;

    move-result-object v0

    invoke-virtual {v0, p0, p2}, Lcom/google/android/youtube/core/async/by;->a(Landroid/app/Activity;Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    :sswitch_0
    invoke-static {p0}, Lcom/google/android/youtube/app/compat/o;->a(Landroid/app/Activity;)Lcom/google/android/youtube/app/compat/o;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->D:Lcom/google/android/youtube/app/compat/o;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->D:Lcom/google/android/youtube/app/compat/o;

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->w:Lcom/google/android/youtube/app/ui/ak;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/ak;->a()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->n:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->t()Lcom/google/android/youtube/core/async/UserDelegator;

    move-result-object v0

    invoke-virtual {v0, p0, p2}, Lcom/google/android/youtube/core/async/UserDelegator;->a(Landroid/app/Activity;Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(I)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x3f1 -> :sswitch_1
        0x405 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    new-instance v0, Lcom/google/android/youtube/app/compat/m;

    invoke-direct {v0, p0, p1}, Lcom/google/android/youtube/app/compat/m;-><init>(Landroid/content/Context;Landroid/view/Menu;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->t:Lcom/google/android/youtube/app/compat/m;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->t:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/youtube/app/compat/m;)Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->r:Lcom/google/android/youtube/app/compat/SupportActionBar;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->t:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(Lcom/google/android/youtube/app/compat/m;)Z

    move-result v1

    or-int/2addr v0, v1

    return v0
.end method

.method protected onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->z:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->b(Lcom/google/android/youtube/core/async/bl;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->B:Lcom/google/android/youtube/app/honeycomb/phone/bb;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/bb;->c()V

    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->y:Lcom/google/android/youtube/app/ui/bw;

    invoke-virtual {v1, p1}, Lcom/google/android/youtube/app/ui/bw;->b(I)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    sget v1, Lcom/google/android/youtube/core/utils/Util;->a:I

    const/16 v2, 0xb

    if-gt v1, v2, :cond_1

    const/16 v1, 0x52

    if-ne p1, v1, :cond_1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_1

    const/16 v1, 0x405

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->showDialog(I)V

    goto :goto_0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/FragmentActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->y:Lcom/google/android/youtube/app/ui/bw;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/bw;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/FragmentActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    sget v0, Lcom/google/android/youtube/core/utils/Util;->a:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->i()Z

    move-result v0

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->t:Lcom/google/android/youtube/app/compat/m;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/compat/m;->c(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/youtube/app/compat/t;)Z

    move-result v0

    goto :goto_1
.end method

.method protected onPause()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->x:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->u:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->b()V

    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onPause()V

    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onPostCreate(Landroid/os/Bundle;)V

    sget v0, Lcom/google/android/youtube/core/utils/Util;->a:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->f()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->l_()V

    return-void
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-super {p0, p1, p2}, Landroid/support/v4/app/FragmentActivity;->onPrepareDialog(ILandroid/app/Dialog;)V

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->t:Lcom/google/android/youtube/app/compat/m;

    if-eqz v0, :cond_0

    check-cast p2, Lcom/google/android/youtube/app/compat/o;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->t:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {p2, v0}, Lcom/google/android/youtube/app/compat/o;->a(Lcom/google/android/youtube/app/compat/m;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x405
        :pswitch_0
    .end packed-switch
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->t:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->b(Lcom/google/android/youtube/app/compat/m;)Z

    move-result v1

    or-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->r:Lcom/google/android/youtube/app/compat/SupportActionBar;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->t:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/compat/SupportActionBar;->b(Lcom/google/android/youtube/app/compat/m;)Z

    move-result v1

    or-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->D:Lcom/google/android/youtube/app/compat/o;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->D:Lcom/google/android/youtube/app/compat/o;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/compat/o;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->D:Lcom/google/android/youtube/app/compat/o;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->t:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/compat/o;->a(Lcom/google/android/youtube/app/compat/m;)V

    :cond_0
    return v0
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onResume()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->p:Lcom/google/android/youtube/core/Analytics;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;)V

    return-void
.end method

.method public onSearchRequested()Z
    .locals 3

    const/4 v0, 0x1

    sget v1, Lcom/google/android/youtube/core/utils/Util;->a:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->u:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Z)V

    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onSearchRequested()Z

    move-result v0

    goto :goto_0
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStart()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->A:Lcom/google/android/youtube/app/remote/ad;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/ad;->c()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->A:Lcom/google/android/youtube/app/remote/ad;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/remote/ad;->a(Lcom/google/android/youtube/app/remote/ag;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->A:Lcom/google/android/youtube/app/remote/ad;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/ad;->b()Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->y:Lcom/google/android/youtube/app/ui/bw;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/ui/bw;->a(Lcom/google/android/youtube/app/remote/RemoteControl;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->E:Lcom/google/android/youtube/app/honeycomb/ui/j;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->E:Lcom/google/android/youtube/app/honeycomb/ui/j;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/honeycomb/ui/j;->a(Lcom/google/android/youtube/app/remote/RemoteControl;)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->F:Lcom/google/android/youtube/app/honeycomb/ui/r;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->B:Lcom/google/android/youtube/app/honeycomb/phone/bb;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/honeycomb/phone/bb;->a()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/honeycomb/ui/r;->b(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->n:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->y()Lcom/google/android/youtube/core/client/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/client/at;->e()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->n:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->A()Lcom/google/android/youtube/app/remote/br;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->F:Lcom/google/android/youtube/app/honeycomb/ui/r;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/remote/br;->a(Lcom/google/android/youtube/app/remote/bd;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->E:Lcom/google/android/youtube/app/honeycomb/ui/j;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->E:Lcom/google/android/youtube/app/honeycomb/ui/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/ui/j;->a()V

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->y:Lcom/google/android/youtube/app/ui/bw;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/bw;->a()V

    goto :goto_0
.end method

.method protected onStop()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->A:Lcom/google/android/youtube/app/remote/ad;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/ad;->d()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->A:Lcom/google/android/youtube/app/remote/ad;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/remote/ad;->b(Lcom/google/android/youtube/app/remote/ag;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->n:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->A()Lcom/google/android/youtube/app/remote/br;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->F:Lcom/google/android/youtube/app/honeycomb/ui/r;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/remote/br;->b(Lcom/google/android/youtube/app/remote/bd;)V

    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStop()V

    return-void
.end method

.method public final setContentView(I)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f07013e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    return-void
.end method

.method public final setContentView(Landroid/view/View;)V
    .locals 2

    const/4 v1, -0x1

    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, p1, v0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public final setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f07013e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    invoke-virtual {v0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method protected abstract w()Ljava/lang/String;
.end method
