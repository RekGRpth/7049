.class public final Lcom/google/android/youtube/app/honeycomb/phone/aa;
.super Lcom/google/android/youtube/app/honeycomb/phone/x;
.source "SourceFile"


# instance fields
.field private final b:Lcom/google/android/youtube/app/ui/bx;

.field private final c:Landroid/view/View;

.field private final d:Ljava/lang/String;


# direct methods
.method public varargs constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Ljava/lang/String;ZLcom/google/android/youtube/core/client/VideoStats2Client$Feature;Lcom/google/android/youtube/core/async/au;[Lcom/google/android/youtube/core/async/GDataRequest;)V
    .locals 13

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/x;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;)V

    const-string v1, "requester cannot be null"

    move-object/from16 v0, p5

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "request cannot be null"

    move-object/from16 v0, p6

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f040041

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/aa;->o()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v2, v3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/aa;->c:Landroid/view/View;

    iput-object p2, p0, Lcom/google/android/youtube/app/honeycomb/phone/aa;->d:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getApplication()Landroid/app/Application;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v9}, Lcom/google/android/youtube/app/YouTubeApplication;->getResources()Landroid/content/res/Resources;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/aa;->c:Landroid/view/View;

    const v2, 0x7f070067

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/google/android/youtube/core/ui/PagedListView;

    invoke-virtual {v9}, Lcom/google/android/youtube/app/YouTubeApplication;->a()Lcom/google/android/youtube/core/client/bc;

    move-result-object v2

    invoke-virtual {v9}, Lcom/google/android/youtube/app/YouTubeApplication;->e_()Lcom/google/android/youtube/core/client/be;

    move-result-object v3

    invoke-virtual {v9}, Lcom/google/android/youtube/app/YouTubeApplication;->w()Lcom/google/android/youtube/core/client/bg;

    move-result-object v4

    invoke-virtual {v9}, Lcom/google/android/youtube/app/YouTubeApplication;->l()Lcom/google/android/youtube/core/utils/p;

    move-result-object v5

    const/4 v6, 0x0

    move-object v1, p1

    invoke-static/range {v1 .. v6}, Lcom/google/android/youtube/app/adapter/bn;->a(Landroid/content/Context;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/bg;Lcom/google/android/youtube/core/utils/p;Lcom/google/android/youtube/app/prefetch/d;)Lcom/google/android/youtube/app/adapter/bm;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/google/android/youtube/app/ui/bx;->a(Landroid/content/Context;Lcom/google/android/youtube/core/a/a;)Lcom/google/android/youtube/app/ui/bx;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/aa;->b:Lcom/google/android/youtube/app/ui/bx;

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/aa;->b()V

    new-instance v1, Lcom/google/android/youtube/app/ui/ea;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/aa;->b:Lcom/google/android/youtube/app/ui/bx;

    invoke-virtual {v9}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/e;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->F()Lcom/google/android/youtube/app/d;

    move-result-object v8

    invoke-virtual {v9}, Lcom/google/android/youtube/app/YouTubeApplication;->i()Lcom/google/android/youtube/core/Analytics;

    move-result-object v11

    sget-object v12, Lcom/google/android/youtube/core/Analytics$VideoCategory;->HomeFeed:Lcom/google/android/youtube/core/Analytics$VideoCategory;

    move-object v2, p1

    move-object v3, v10

    move-object/from16 v5, p5

    move/from16 v9, p3

    move-object/from16 v10, p4

    invoke-direct/range {v1 .. v12}, Lcom/google/android/youtube/app/ui/ea;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/PagedView;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/e;ZLcom/google/android/youtube/app/d;ZLcom/google/android/youtube/core/client/VideoStats2Client$Feature;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/Analytics$VideoCategory;)V

    move-object/from16 v0, p6

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/ui/ea;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    return-void
.end method

.method private b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/aa;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/aa;->b:Lcom/google/android/youtube/app/ui/bx;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/ui/bx;->a(I)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/x;->a(Landroid/content/res/Configuration;)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/aa;->b()V

    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/aa;->d:Ljava/lang/String;

    return-object v0
.end method
