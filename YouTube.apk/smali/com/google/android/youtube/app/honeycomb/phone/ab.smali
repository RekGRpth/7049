.class public final Lcom/google/android/youtube/app/honeycomb/phone/ab;
.super Lcom/google/android/youtube/app/honeycomb/phone/j;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/bk;
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field private A:I

.field private B:I

.field private C:I

.field private D:I

.field private E:I

.field private F:I

.field private G:Z

.field private H:Z

.field private I:Ljava/lang/String;

.field private J:Lcom/google/android/youtube/app/ui/TutorialView;

.field private K:Z

.field private L:Z

.field private M:Landroid/view/View;

.field private N:Lcom/google/android/youtube/core/model/UserAuth;

.field private O:Lcom/google/android/youtube/app/remote/ad;

.field private P:Lcom/google/android/youtube/app/remote/ag;

.field private Q:Ljava/lang/String;

.field private final b:Landroid/util/SparseArray;

.field private final c:Landroid/view/LayoutInflater;

.field private final d:Landroid/view/View;

.field private final e:Lcom/google/android/youtube/core/ui/PagedListView;

.field private final f:Landroid/widget/ListView;

.field private final g:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private final h:Lcom/google/android/youtube/app/YouTubeApplication;

.field private final i:Lcom/google/android/youtube/core/client/bc;

.field private final j:Lcom/google/android/youtube/core/client/be;

.field private final k:Lcom/google/android/youtube/core/e;

.field private final l:Lcom/google/android/youtube/core/Analytics;

.field private m:Lcom/google/android/youtube/app/remote/RemoteControl;

.field private final n:Lcom/google/android/youtube/app/honeycomb/phone/ar;

.field private final o:Lcom/google/android/youtube/app/honeycomb/phone/at;

.field private final p:Lcom/google/android/youtube/core/async/n;

.field private final q:Landroid/content/SharedPreferences;

.field private r:Ljava/util/Set;

.field private s:Lcom/google/android/youtube/app/adapter/bm;

.field private t:Lcom/google/android/youtube/core/ui/j;

.field private u:Lcom/google/android/youtube/app/honeycomb/phone/ak;

.field private v:Lcom/google/android/youtube/app/honeycomb/phone/ai;

.field private final w:Landroid/os/Handler;

.field private final x:Lcom/google/android/youtube/app/honeycomb/phone/aq;

.field private y:I

.field private z:I


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/youtube/app/honeycomb/phone/aq;)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v0, -0x1

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/j;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;)V

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->y:I

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->z:I

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->A:I

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->B:I

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->C:I

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->D:I

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->E:I

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->F:I

    iput-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->I:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->K:Z

    iput-boolean v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->L:Z

    iput-object p2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->x:Lcom/google/android/youtube/app/honeycomb/phone/aq;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->c:Landroid/view/LayoutInflater;

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/ac;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/ac;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/ab;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->w:Landroid/os/Handler;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->h:Lcom/google/android/youtube/app/YouTubeApplication;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->h:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->U()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->g:Lcom/google/android/youtube/core/async/UserAuthorizer;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->h:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->a()Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->i:Lcom/google/android/youtube/core/client/bc;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->h:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->e_()Lcom/google/android/youtube/core/client/be;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->j:Lcom/google/android/youtube/core/client/be;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->h:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->k:Lcom/google/android/youtube/core/e;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->h:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->i()Lcom/google/android/youtube/core/Analytics;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->l:Lcom/google/android/youtube/core/Analytics;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->h:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->K()Lcom/google/android/youtube/app/remote/ad;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->O:Lcom/google/android/youtube/app/remote/ad;

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/ad;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/honeycomb/phone/ad;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/ab;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->P:Lcom/google/android/youtube/app/remote/ag;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f040047

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->d:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->d:Landroid/view/View;

    const v1, 0x7f0700bf

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/ui/PagedListView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->e:Lcom/google/android/youtube/core/ui/PagedListView;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->e:Lcom/google/android/youtube/core/ui/PagedListView;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/ui/PagedListView;->i()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->f:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->f:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setDividerHeight(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->h:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->S()Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->q:Landroid/content/SharedPreferences;

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/ar;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/honeycomb/phone/ar;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/ab;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->n:Lcom/google/android/youtube/app/honeycomb/phone/ar;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->h:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->F()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->Q:Ljava/lang/String;

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/at;

    invoke-direct {v0, p0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/at;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/ab;B)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->o:Lcom/google/android/youtube/app/honeycomb/phone/at;

    new-instance v0, Landroid/util/SparseArray;

    sget-object v1, Lcom/google/android/youtube/app/honeycomb/phone/l;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->b:Landroid/util/SparseArray;

    invoke-static {p1, p0}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->p:Lcom/google/android/youtube/core/async/n;

    return-void
.end method

.method static synthetic A(Lcom/google/android/youtube/app/honeycomb/phone/ab;)Lcom/google/android/youtube/core/model/UserAuth;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->N:Lcom/google/android/youtube/core/model/UserAuth;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/ab;Landroid/view/View;)Landroid/view/View;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->M:Landroid/view/View;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/ab;)Lcom/google/android/youtube/app/honeycomb/phone/aq;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->x:Lcom/google/android/youtube/app/honeycomb/phone/aq;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/ab;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->I:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/ab;Landroid/view/View;Landroid/widget/TextView;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->I:Ljava/lang/String;

    invoke-static {v0, p3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const v0, 0x7f020051

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    if-eqz v1, :cond_1

    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    :goto_1
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    return-void

    :cond_0
    const v0, 0x7f020050

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->g(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v0

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/ab;Lcom/google/android/youtube/app/remote/RemoteControl;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->a(Lcom/google/android/youtube/app/remote/RemoteControl;)V

    return-void
.end method

.method private a(Lcom/google/android/youtube/app/remote/RemoteControl;)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->m:Lcom/google/android/youtube/app/remote/RemoteControl;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->m:Lcom/google/android/youtube/app/remote/RemoteControl;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->m:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->k()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->n:Lcom/google/android/youtube/app/honeycomb/phone/ar;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->m:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v3}, Lcom/google/android/youtube/app/remote/RemoteControl;->t()Lcom/google/android/youtube/app/remote/bb;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/app/honeycomb/phone/ar;->a(Lcom/google/android/youtube/app/remote/bb;)V

    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->C:I

    if-gez v0, :cond_5

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->G:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->s:Lcom/google/android/youtube/app/adapter/bm;

    iget v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->B:I

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->n:Lcom/google/android/youtube/app/honeycomb/phone/ar;

    invoke-virtual {v0, v3, v4}, Lcom/google/android/youtube/app/adapter/bm;->a(ILcom/google/android/youtube/app/adapter/by;)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->C:I

    iget v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->B:I

    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->B:I

    if-ltz v0, :cond_2

    move v0, v1

    :goto_0
    add-int/2addr v0, v3

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->B:I

    iget v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->z:I

    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->z:I

    if-ltz v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v0, v3

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->z:I

    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->A:I

    iget v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->A:I

    if-ltz v3, :cond_4

    :goto_2
    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->A:I

    :cond_0
    :goto_3
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->w()V

    :cond_1
    :goto_4
    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    move v1, v2

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->s:Lcom/google/android/youtube/app/adapter/bm;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/bm;->notifyDataSetChanged()V

    goto :goto_4

    :cond_6
    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->C:I

    if-ltz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->s:Lcom/google/android/youtube/app/adapter/bm;

    iget v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->C:I

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/app/adapter/bm;->a(I)Lcom/google/android/youtube/app/adapter/by;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->C:I

    iget v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->B:I

    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->B:I

    if-ltz v0, :cond_7

    move v0, v1

    :goto_5
    sub-int v0, v3, v0

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->B:I

    iget v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->z:I

    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->z:I

    if-ltz v0, :cond_8

    move v0, v1

    :goto_6
    sub-int v0, v3, v0

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->z:I

    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->A:I

    iget v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->A:I

    if-ltz v3, :cond_9

    :goto_7
    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->A:I

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->n:Lcom/google/android/youtube/app/honeycomb/phone/ar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/ar;->a(Lcom/google/android/youtube/app/remote/bb;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->I:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->I:Ljava/lang/String;

    const-string v1, "REMOTE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->K:Z

    if-eqz v0, :cond_a

    const-string v0, "WHAT_TO_WATCH"

    invoke-virtual {p0, v0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->a(Ljava/lang/String;Z)V

    goto :goto_3

    :cond_7
    move v0, v2

    goto :goto_5

    :cond_8
    move v0, v2

    goto :goto_6

    :cond_9
    move v1, v2

    goto :goto_7

    :cond_a
    const-string v0, "TRENDING_GUIDE_ITEM"

    invoke-virtual {p0, v0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->a(Ljava/lang/String;Z)V

    goto :goto_3
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/ab;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->K:Z

    return p1
.end method

.method static synthetic b(Lcom/google/android/youtube/app/honeycomb/phone/ab;)Lcom/google/android/youtube/app/adapter/bm;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->s:Lcom/google/android/youtube/app/adapter/bm;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/honeycomb/phone/ab;Z)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->L:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/honeycomb/phone/ab;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->t()V

    return-void
.end method

.method static synthetic d(Lcom/google/android/youtube/app/honeycomb/phone/ab;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->I:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/app/honeycomb/phone/ab;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->u()V

    return-void
.end method

.method static synthetic f(Lcom/google/android/youtube/app/honeycomb/phone/ab;)Lcom/google/android/youtube/core/Analytics;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->l:Lcom/google/android/youtube/core/Analytics;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/youtube/app/honeycomb/phone/ab;)Lcom/google/android/youtube/app/YouTubeApplication;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->h:Lcom/google/android/youtube/app/YouTubeApplication;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/youtube/app/honeycomb/phone/ab;)V
    .locals 4

    const/4 v3, 0x0

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->L:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->K:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->M:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->J:Lcom/google/android/youtube/app/ui/TutorialView;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->h:Lcom/google/android/youtube/app/YouTubeApplication;

    const v2, 0x7f0b01c4

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/YouTubeApplication;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/TutorialView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->J:Lcom/google/android/youtube/app/ui/TutorialView;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->M:Landroid/view/View;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/ui/TutorialView;->setTargetView(Landroid/view/View;Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->J:Lcom/google/android/youtube/app/ui/TutorialView;

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/app/ui/TutorialView;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->u:Lcom/google/android/youtube/app/honeycomb/phone/ak;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->u:Lcom/google/android/youtube/app/honeycomb/phone/ak;

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/ak;->b:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->J:Lcom/google/android/youtube/app/ui/TutorialView;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->h:Lcom/google/android/youtube/app/YouTubeApplication;

    const v2, 0x7f0b01c3

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/YouTubeApplication;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/TutorialView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->J:Lcom/google/android/youtube/app/ui/TutorialView;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->u:Lcom/google/android/youtube/app/honeycomb/phone/ak;

    iget-object v2, v2, Lcom/google/android/youtube/app/honeycomb/phone/ak;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/ui/TutorialView;->setTargetView(Landroid/view/View;Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->J:Lcom/google/android/youtube/app/ui/TutorialView;

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/app/ui/TutorialView;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic i(Lcom/google/android/youtube/app/honeycomb/phone/ab;)Landroid/util/SparseArray;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->b:Landroid/util/SparseArray;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/youtube/app/honeycomb/phone/ab;)I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->C:I

    return v0
.end method

.method static synthetic k(Lcom/google/android/youtube/app/honeycomb/phone/ab;)I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->y:I

    return v0
.end method

.method static synthetic l(Lcom/google/android/youtube/app/honeycomb/phone/ab;)I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->z:I

    return v0
.end method

.method static synthetic m(Lcom/google/android/youtube/app/honeycomb/phone/ab;)I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->A:I

    return v0
.end method

.method static synthetic n(Lcom/google/android/youtube/app/honeycomb/phone/ab;)I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->B:I

    return v0
.end method

.method static synthetic o(Lcom/google/android/youtube/app/honeycomb/phone/ab;)I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->D:I

    return v0
.end method

.method static synthetic p(Lcom/google/android/youtube/app/honeycomb/phone/ab;)I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->E:I

    return v0
.end method

.method static synthetic q(Lcom/google/android/youtube/app/honeycomb/phone/ab;)I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->F:I

    return v0
.end method

.method static synthetic r(Lcom/google/android/youtube/app/honeycomb/phone/ab;)Landroid/content/SharedPreferences;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->q:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic s(Lcom/google/android/youtube/app/honeycomb/phone/ab;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->K:Z

    return v0
.end method

.method static synthetic t(Lcom/google/android/youtube/app/honeycomb/phone/ab;)Landroid/widget/ListView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->f:Landroid/widget/ListView;

    return-object v0
.end method

.method private t()V
    .locals 5

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->G:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->s:Lcom/google/android/youtube/app/adapter/bm;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/ap;

    const v2, 0x7f0b0184

    invoke-direct {v1, p0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/ap;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/ab;I)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/bm;->a(Lcom/google/android/youtube/app/adapter/by;)I

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->N:Lcom/google/android/youtube/core/model/UserAuth;

    if-eqz v0, :cond_2

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/ai;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->N:Lcom/google/android/youtube/core/model/UserAuth;

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/ai;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/ab;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->v:Lcom/google/android/youtube/app/honeycomb/phone/ai;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->s:Lcom/google/android/youtube/app/adapter/bm;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->v:Lcom/google/android/youtube/app/honeycomb/phone/ai;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/bm;->a(Lcom/google/android/youtube/app/adapter/by;)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->y:I

    :goto_1
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/as;

    const-string v1, "CHANNEL_STORE"

    const v2, 0x7f0b0181

    const v3, 0x7f0200d9

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/android/youtube/app/honeycomb/phone/as;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/ab;Ljava/lang/String;II)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->u:Lcom/google/android/youtube/app/honeycomb/phone/ak;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->s:Lcom/google/android/youtube/app/adapter/bm;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->u:Lcom/google/android/youtube/app/honeycomb/phone/ak;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/bm;->a(Lcom/google/android/youtube/app/adapter/by;)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->B:I

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->N:Lcom/google/android/youtube/core/model/UserAuth;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->s:Lcom/google/android/youtube/app/adapter/bm;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/ap;

    const v2, 0x7f0b0185

    invoke-direct {v1, p0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/ap;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/ab;I)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/bm;->a(Lcom/google/android/youtube/app/adapter/by;)I

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->s:Lcom/google/android/youtube/app/adapter/bm;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/as;

    const-string v2, "WHAT_TO_WATCH"

    const v3, 0x7f0b017f

    const v4, 0x7f0200e4

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/google/android/youtube/app/honeycomb/phone/as;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/ab;Ljava/lang/String;II)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/bm;->a(Lcom/google/android/youtube/app/adapter/by;)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->z:I

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->s:Lcom/google/android/youtube/app/adapter/bm;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/as;

    const-string v2, "MY_SUBSCRIPTIONS"

    const v3, 0x7f0b0180

    const v4, 0x7f0200dc

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/google/android/youtube/app/honeycomb/phone/as;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/ab;Ljava/lang/String;II)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/bm;->a(Lcom/google/android/youtube/app/adapter/by;)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->A:I

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->G:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->m:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->a(Lcom/google/android/youtube/app/remote/RemoteControl;)V

    goto/16 :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->s:Lcom/google/android/youtube/app/adapter/bm;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/as;

    const-string v2, "ACCOUNT"

    const v3, 0x7f0b00e1

    const v4, 0x7f0200df

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/google/android/youtube/app/honeycomb/phone/as;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/ab;Ljava/lang/String;II)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/bm;->a(Lcom/google/android/youtube/app/adapter/by;)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->y:I

    goto :goto_1
.end method

.method private u()V
    .locals 8

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->H:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->s:Lcom/google/android/youtube/app/adapter/bm;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/ap;

    const v2, 0x7f0b0186

    invoke-direct {v1, p0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/ap;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/ab;I)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/bm;->b(Lcom/google/android/youtube/app/adapter/by;)I

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->g:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->s:Lcom/google/android/youtube/app/adapter/bm;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/as;

    const-string v2, "RECOMMENDED_GUIDE_ITEM"

    const v3, 0x7f0b0182

    const v4, 0x7f02010e

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/google/android/youtube/app/honeycomb/phone/as;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/ab;Ljava/lang/String;II)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/bm;->b(Lcom/google/android/youtube/app/adapter/by;)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->D:I

    :goto_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->s:Lcom/google/android/youtube/app/adapter/bm;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/as;

    const-string v2, "TRENDING_GUIDE_ITEM"

    const v3, 0x7f0b0183

    const v4, 0x7f02011d

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/google/android/youtube/app/honeycomb/phone/as;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/ab;Ljava/lang/String;II)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/bm;->b(Lcom/google/android/youtube/app/adapter/by;)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->E:I

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->h:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->n()Lcom/google/android/youtube/app/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/k;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->s:Lcom/google/android/youtube/app/adapter/bm;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/as;

    const-string v2, "LIVE_GUIDE_ITEM"

    const v3, 0x7f0b0015

    const v4, 0x7f0200f9

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/google/android/youtube/app/honeycomb/phone/as;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/ab;Ljava/lang/String;II)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/bm;->b(Lcom/google/android/youtube/app/adapter/by;)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->F:I

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->i:Lcom/google/android/youtube/core/client/bc;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-static {v2}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->p:Lcom/google/android/youtube/core/async/n;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/youtube/core/client/bc;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/core/async/n;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->r:Ljava/util/Set;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->r:Ljava/util/Set;

    move-object v3, v0

    :goto_2
    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/l;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v3, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->s:Lcom/google/android/youtube/app/adapter/bm;

    new-instance v6, Lcom/google/android/youtube/app/honeycomb/phone/al;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    iget-object v2, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {v6, p0, v1, v7, v2}, Lcom/google/android/youtube/app/honeycomb/phone/al;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/ab;Ljava/lang/String;II)V

    invoke-virtual {v5, v6}, Lcom/google/android/youtube/app/adapter/bm;->b(Lcom/google/android/youtube/app/adapter/by;)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->b:Landroid/util/SparseArray;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_3

    :cond_3
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->D:I

    goto/16 :goto_1

    :cond_4
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->q:Landroid/content/SharedPreferences;

    const-string v1, "youtube_categories"

    sget-object v2, Lcom/google/android/youtube/app/honeycomb/phone/l;->b:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/app/compat/ac;->a(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    move-object v3, v0

    goto :goto_2

    :cond_5
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->H:Z

    goto/16 :goto_0
.end method

.method static synthetic u(Lcom/google/android/youtube/app/honeycomb/phone/ab;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->v()V

    return-void
.end method

.method static synthetic v(Lcom/google/android/youtube/app/honeycomb/phone/ab;)Lcom/google/android/youtube/core/client/bc;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->i:Lcom/google/android/youtube/core/client/bc;

    return-object v0
.end method

.method private v()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->H:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->s:Lcom/google/android/youtube/app/adapter/bm;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/bm;->b()V

    return-void
.end method

.method static synthetic w(Lcom/google/android/youtube/app/honeycomb/phone/ab;)Lcom/google/android/youtube/core/ui/j;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->t:Lcom/google/android/youtube/core/ui/j;

    return-object v0
.end method

.method private w()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->H:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->v()V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->u()V

    :cond_0
    return-void
.end method

.method static synthetic x(Lcom/google/android/youtube/app/honeycomb/phone/ab;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->w()V

    return-void
.end method

.method static synthetic y(Lcom/google/android/youtube/app/honeycomb/phone/ab;)Lcom/google/android/youtube/core/client/be;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->j:Lcom/google/android/youtube/core/client/be;

    return-object v0
.end method

.method static synthetic z(Lcom/google/android/youtube/app/honeycomb/phone/ab;)Landroid/view/LayoutInflater;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->c:Landroid/view/LayoutInflater;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 7

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/j;->a()V

    new-instance v0, Lcom/google/android/youtube/app/adapter/bm;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    const v2, 0x7f040045

    new-instance v3, Lcom/google/android/youtube/app/honeycomb/phone/ao;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/google/android/youtube/app/honeycomb/phone/ao;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/ab;B)V

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/youtube/app/adapter/bm;-><init>(Landroid/content/Context;ILcom/google/android/youtube/app/adapter/bv;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->s:Lcom/google/android/youtube/app/adapter/bm;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->I:Ljava/lang/String;

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/ae;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->e:Lcom/google/android/youtube/core/ui/PagedListView;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->s:Lcom/google/android/youtube/app/adapter/bm;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->i:Lcom/google/android/youtube/core/client/bc;

    invoke-interface {v1}, Lcom/google/android/youtube/core/client/bc;->y()Lcom/google/android/youtube/core/async/au;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->k:Lcom/google/android/youtube/core/e;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/app/honeycomb/phone/ae;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/ab;Landroid/app/Activity;Lcom/google/android/youtube/core/ui/PagedView;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/e;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->t:Lcom/google/android/youtube/core/ui/j;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->e:Lcom/google/android/youtube/core/ui/PagedListView;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/af;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/honeycomb/phone/af;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/ab;)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/PagedListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->g:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bk;)V

    return-void
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->s:Lcom/google/android/youtube/app/adapter/bm;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/au;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/au;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/ab;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/bm;->a(Lcom/google/android/youtube/core/utils/t;)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->w()V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Subscription;)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->s:Lcom/google/android/youtube/app/adapter/bm;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/adapter/bm;->d(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->s:Lcom/google/android/youtube/app/adapter/bm;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/bm;->notifyDataSetChanged()V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->w()V

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 4

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->N:Lcom/google/android/youtube/core/model/UserAuth;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->t:Lcom/google/android/youtube/core/ui/j;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->i:Lcom/google/android/youtube/core/client/bc;

    invoke-interface {v3}, Lcom/google/android/youtube/core/client/bc;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->i(Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/j;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    check-cast p2, Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/converter/http/i;->a(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->r:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Category;

    sget-object v2, Lcom/google/android/youtube/app/honeycomb/phone/l;->b:Ljava/util/Map;

    iget-object v3, v0, Lcom/google/android/youtube/core/model/Category;->term:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->r:Ljava/util/Set;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Category;->term:Ljava/lang/String;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->q:Landroid/content/SharedPreferences;

    invoke-static {v0}, Lcom/google/android/youtube/app/compat/ac;->a(Landroid/content/SharedPreferences;)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v0

    const-string v1, "youtube_categories"

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->r:Ljava/util/Set;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/compat/ad;->a(Ljava/lang/String;Ljava/util/Set;)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/compat/ad;->a()V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->g_()V

    return-void
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->x:Lcom/google/android/youtube/app/honeycomb/phone/aq;

    if-eqz v0, :cond_0

    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->x:Lcom/google/android/youtube/app/honeycomb/phone/aq;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/app/honeycomb/phone/aq;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->I:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->s:Lcom/google/android/youtube/app/adapter/bm;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/bm;->notifyDataSetChanged()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->w:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0
.end method

.method public final c()V
    .locals 3

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/j;->c()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->O:Lcom/google/android/youtube/app/remote/ad;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->P:Lcom/google/android/youtube/app/remote/ag;

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/remote/ad;->a(Lcom/google/android/youtube/app/remote/ag;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->O:Lcom/google/android/youtube/app/remote/ad;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/ad;->b()Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->a(Lcom/google/android/youtube/app/remote/RemoteControl;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->I:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->h:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->F()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->Q:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->Q:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->I:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->a(Ljava/lang/String;Z)V

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final e()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final g_()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->t()V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->u()V

    const-string v0, "TRENDING_GUIDE_ITEM"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->a(Ljava/lang/String;Z)V

    return-void
.end method

.method public final h()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/j;->h()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->O:Lcom/google/android/youtube/app/remote/ad;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->P:Lcom/google/android/youtube/app/remote/ag;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/remote/ad;->b(Lcom/google/android/youtube/app/remote/ag;)V

    return-void
.end method

.method public final j()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->x:Lcom/google/android/youtube/app/honeycomb/phone/aq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->x:Lcom/google/android/youtube/app/honeycomb/phone/aq;

    invoke-interface {v0}, Lcom/google/android/youtube/app/honeycomb/phone/aq;->a()V

    :cond_0
    return-void
.end method

.method public final k()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->x:Lcom/google/android/youtube/app/honeycomb/phone/aq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->x:Lcom/google/android/youtube/app/honeycomb/phone/aq;

    invoke-interface {v0}, Lcom/google/android/youtube/app/honeycomb/phone/aq;->o_()V

    :cond_0
    return-void
.end method

.method public final n()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->c:Landroid/view/LayoutInflater;

    const v2, 0x7f04009f

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f07014b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/TutorialView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->J:Lcom/google/android/youtube/app/ui/TutorialView;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->J:Lcom/google/android/youtube/app/ui/TutorialView;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-static {v1}, Lcom/google/android/youtube/core/utils/Util;->g(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/TutorialView;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->J:Lcom/google/android/youtube/app/ui/TutorialView;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/ag;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/honeycomb/phone/ag;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/ab;)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/TutorialView;->setDismissListener(Lcom/google/android/youtube/app/ui/cy;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->L:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->f:Landroid/widget/ListView;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/ah;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/honeycomb/phone/ah;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/ab;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final o()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->d:Landroid/view/View;

    return-object v0
.end method

.method public final p()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->L:Z

    return v0
.end method

.method public final q()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->J:Lcom/google/android/youtube/app/ui/TutorialView;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/TutorialView;->a()V

    return-void
.end method

.method public final r()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->v:Lcom/google/android/youtube/app/honeycomb/phone/ai;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->v:Lcom/google/android/youtube/app/honeycomb/phone/ai;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/ai;->b()V

    :cond_0
    return-void
.end method

.method public final s()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->g:Lcom/google/android/youtube/core/async/UserAuthorizer;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ab;->o:Lcom/google/android/youtube/app/honeycomb/phone/at;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bk;)V

    return-void
.end method
