.class final Lcom/google/android/youtube/app/honeycomb/phone/am;
.super Lcom/google/android/youtube/app/adapter/k;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

.field private final b:Landroid/view/View;

.field private final c:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/ab;Landroid/view/View;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/am;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    iget-object v0, p1, Lcom/google/android/youtube/app/honeycomb/phone/ab;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    const v1, 0x7f070035

    invoke-direct {p0, v0, p2, v1}, Lcom/google/android/youtube/app/adapter/k;-><init>(Landroid/content/Context;Landroid/view/View;I)V

    iput-object p2, p0, Lcom/google/android/youtube/app/honeycomb/phone/am;->b:Landroid/view/View;

    const v0, 0x7f07002c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/am;->c:Landroid/widget/TextView;

    const v0, 0x7f070056

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const v1, 0x7f020049

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    return-void
.end method


# virtual methods
.method public final synthetic a(ILjava/lang/Object;)Landroid/view/View;
    .locals 4

    check-cast p2, Lcom/google/android/youtube/core/model/Subscription;

    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/app/adapter/k;->a(ILjava/lang/Object;)Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/am;->c:Landroid/widget/TextView;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/Subscription;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/am;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/am;->b:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/am;->c:Landroid/widget/TextView;

    iget-object v3, p2, Lcom/google/android/youtube/core/model/Subscription;->channelUri:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->a(Lcom/google/android/youtube/app/honeycomb/phone/ab;Landroid/view/View;Landroid/widget/TextView;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/am;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->m(Lcom/google/android/youtube/app/honeycomb/phone/ab;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/am;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/am;->b:Landroid/view/View;

    const v2, 0x7f070056

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->a(Lcom/google/android/youtube/app/honeycomb/phone/ab;Landroid/view/View;)Landroid/view/View;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/am;->b:Landroid/view/View;

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;Landroid/view/View;Lcom/google/android/youtube/core/async/n;)V
    .locals 3

    check-cast p1, Lcom/google/android/youtube/core/model/Subscription;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/am;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->v(Lcom/google/android/youtube/app/honeycomb/phone/ab;)Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Subscription;->channelUri:Landroid/net/Uri;

    new-instance v2, Lcom/google/android/youtube/app/honeycomb/phone/an;

    invoke-direct {v2, p0, p3}, Lcom/google/android/youtube/app/honeycomb/phone/an;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/am;Lcom/google/android/youtube/core/async/n;)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/bc;->c(Landroid/net/Uri;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method
