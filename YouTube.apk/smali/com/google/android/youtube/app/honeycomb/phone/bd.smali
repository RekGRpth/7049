.class public abstract Lcom/google/android/youtube/app/honeycomb/phone/bd;
.super Lcom/google/android/youtube/app/honeycomb/phone/x;
.source "SourceFile"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;
.implements Lcom/google/android/youtube/app/prefetch/f;


# instance fields
.field private final b:Landroid/view/View;

.field private final c:Landroid/view/LayoutInflater;

.field private d:[Lcom/google/android/youtube/core/ui/PagedListView;

.field private e:[Lcom/google/android/youtube/core/a/a;

.field private f:I

.field private g:Landroid/view/View;

.field private h:Z

.field private i:Lcom/google/android/youtube/app/prefetch/d;

.field private j:Lcom/google/android/youtube/app/compat/SupportActionBar;

.field private k:Landroid/widget/Spinner;

.field private l:Landroid/widget/ArrayAdapter;

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Landroid/view/animation/Animation;

.field private q:J


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;)V
    .locals 3

    const/4 v1, 0x2

    const/4 v2, -0x1

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/x;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;)V

    iput v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->f:I

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->c:Landroid/view/LayoutInflater;

    new-array v0, v1, [Lcom/google/android/youtube/core/ui/PagedListView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->d:[Lcom/google/android/youtube/core/ui/PagedListView;

    new-array v0, v1, [Lcom/google/android/youtube/core/a/a;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->e:[Lcom/google/android/youtube/core/a/a;

    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->b:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->b:Landroid/view/View;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/bd;->o()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    const v0, 0x10a0001

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->p:Landroid/view/animation/Animation;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->p:Landroid/view/animation/Animation;

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    invoke-virtual {p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x10e0001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->q:J

    return-void
.end method

.method private n()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->m:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->n:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->o:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->j:Lcom/google/android/youtube/app/compat/SupportActionBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->j:Lcom/google/android/youtube/app/compat/SupportActionBar;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->k:Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(Landroid/view/View;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->l_()V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->j:Lcom/google/android/youtube/app/compat/SupportActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(Z)V

    goto :goto_0
.end method

.method private s()V
    .locals 4

    const/4 v1, 0x0

    move v0, v1

    :goto_0
    const/4 v2, 0x2

    if-ge v0, v2, :cond_1

    iget-boolean v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->h:Z

    if-nez v2, :cond_0

    iget v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->f:I

    if-ne v0, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->d:[Lcom/google/android/youtube/core/ui/PagedListView;

    aget-object v2, v2, v0

    invoke-virtual {v2, v1}, Lcom/google/android/youtube/core/ui/PagedListView;->setVisibility(I)V

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->d:[Lcom/google/android/youtube/core/ui/PagedListView;

    aget-object v2, v2, v0

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/ui/PagedListView;->setVisibility(I)V

    goto :goto_1

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->h:Z

    if-nez v0, :cond_2

    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->f:I

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/bd;->b(I)V

    :cond_2
    return-void
.end method

.method private t()V
    .locals 4

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->d:[Lcom/google/android/youtube/core/ui/PagedListView;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->e:[Lcom/google/android/youtube/core/a/a;

    aget-object v2, v2, v0

    invoke-virtual {v1}, Lcom/google/android/youtube/core/ui/PagedListView;->getTag()Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v1}, Lcom/google/android/youtube/core/ui/PagedListView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v2}, Lcom/google/android/youtube/core/a/a;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v2}, Lcom/google/android/youtube/core/a/a;->notifyDataSetChanged()V

    :cond_0
    return-void

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private u()[Ljava/lang/String;
    .locals 4

    const/4 v3, 0x2

    new-array v1, v3, [Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/bd;->c(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method


# virtual methods
.method protected abstract a(Lcom/google/android/youtube/core/ui/PagedListView;I)Lcom/google/android/youtube/core/a/a;
.end method

.method public a()V
    .locals 12

    const v11, 0x7f090035

    const/16 v10, 0x8

    const/4 v9, -0x1

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/x;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->b:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    move v1, v2

    :goto_0
    const/4 v4, 0x2

    if-ge v1, v4, :cond_0

    new-instance v4, Lcom/google/android/youtube/core/ui/PagedListView;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v3, v11}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    const v7, 0x7f04004e

    const v8, 0x7f0b010e

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/google/android/youtube/core/ui/PagedListView;-><init>(Landroid/content/Context;IILjava/lang/String;)V

    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v5, v9, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5}, Lcom/google/android/youtube/core/ui/PagedListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v3, v11}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/google/android/youtube/core/ui/PagedListView;->setBackgroundColor(I)V

    invoke-virtual {v4, v10}, Lcom/google/android/youtube/core/ui/PagedListView;->setVisibility(I)V

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->d:[Lcom/google/android/youtube/core/ui/PagedListView;

    aput-object v4, v5, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->c:Landroid/view/LayoutInflater;

    const v3, 0x7f04002b

    invoke-virtual {v1, v3, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->g:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->g:Landroid/view/View;

    invoke-virtual {v1, v10}, Landroid/view/View;->setVisibility(I)V

    iput-boolean v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->h:Z

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->g:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->E()Lcom/google/android/youtube/app/prefetch/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->i:Lcom/google/android/youtube/app/prefetch/d;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->l()Lcom/google/android/youtube/app/compat/SupportActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->j:Lcom/google/android/youtube/app/compat/SupportActionBar;

    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    const v2, 0x1090008

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/bd;->u()[Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->l:Landroid/widget/ArrayAdapter;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->l:Landroid/widget/ArrayAdapter;

    const v1, 0x1090009

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f040007

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->k:Landroid/widget/Spinner;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->k:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->l:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->k:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v1, "com.google.android.youtube.action.search"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onSearchRequested()Z

    :cond_1
    return-void
.end method

.method public final b()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/bd;->t()V

    return-void
.end method

.method protected b(I)V
    .locals 0

    return-void
.end method

.method protected abstract c(I)Ljava/lang/String;
.end method

.method protected final d(I)V
    .locals 3

    if-ltz p1, :cond_2

    const/4 v0, 0x2

    if-ge p1, v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Feed index out of range."

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->f:I

    if-eq p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->d:[Lcom/google/android/youtube/core/ui/PagedListView;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/google/android/youtube/core/ui/PagedListView;->getTag()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->e:[Lcom/google/android/youtube/core/a/a;

    invoke-virtual {p0, v0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/bd;->a(Lcom/google/android/youtube/core/ui/PagedListView;I)Lcom/google/android/youtube/core/a/a;

    move-result-object v2

    aput-object v2, v1, p1

    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/PagedListView;->setTag(Ljava/lang/Object;)V

    :cond_0
    iput p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->f:I

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->h:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/bd;->s()V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/x;->g()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->i:Lcom/google/android/youtube/app/prefetch/d;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/prefetch/d;->a(Lcom/google/android/youtube/app/prefetch/f;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->m:Z

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/bd;->n()V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/bd;->t()V

    return-void
.end method

.method public final i()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/x;->i()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->i:Lcom/google/android/youtube/app/prefetch/d;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/prefetch/d;->b(Lcom/google/android/youtube/app/prefetch/f;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->m:Z

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/bd;->n()V

    return-void
.end method

.method public final j()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/x;->j()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->n:Z

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/bd;->n()V

    return-void
.end method

.method public final k()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/x;->k()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->n:Z

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/bd;->n()V

    return-void
.end method

.method public final l()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/x;->l()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->o:Z

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/bd;->n()V

    return-void
.end method

.method public final m()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/x;->m()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->o:Z

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/bd;->n()V

    return-void
.end method

.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->h:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->g:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->f:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->f:I

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->d:[Lcom/google/android/youtube/core/ui/PagedListView;

    iget v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->f:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/android/youtube/core/ui/PagedListView;->invalidate()V

    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0

    invoke-virtual {p0, p3}, Lcom/google/android/youtube/app/honeycomb/phone/bd;->d(I)V

    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    return-void
.end method

.method protected final p()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->h:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->g:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/bd;->s()V

    return-void
.end method

.method protected final q()V
    .locals 3

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->h:Z

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->h:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->g:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->p:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->reset()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->p:Landroid/view/animation/Animation;

    iget-wide v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->q:J

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->g:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->p:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/bd;->s()V

    :cond_0
    return-void
.end method

.method protected final r()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bd;->f:I

    return v0
.end method
