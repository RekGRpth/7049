.class final Lcom/google/android/youtube/app/honeycomb/phone/bu;
.super Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/utils/p;Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bu;->a:Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;

    const/4 v0, 0x0

    invoke-direct {p0, p2, p3, p4, v0}, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/utils/p;Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)V

    return-void
.end method


# virtual methods
.method protected final bridge synthetic a(Ljava/lang/Object;)Landroid/net/Uri;
    .locals 1

    check-cast p1, Lcom/google/android/youtube/core/model/Video;

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->defaultThumbnailUri:Landroid/net/Uri;

    return-object v0
.end method

.method protected final bridge synthetic b(Ljava/lang/Object;)Landroid/net/Uri;
    .locals 1

    check-cast p1, Lcom/google/android/youtube/core/model/Video;

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->sdThumbnailUri:Landroid/net/Uri;

    return-object v0
.end method

.method protected final bridge synthetic c(Ljava/lang/Object;)Landroid/net/Uri;
    .locals 1

    check-cast p1, Lcom/google/android/youtube/core/model/Video;

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->hqThumbnailUri:Landroid/net/Uri;

    return-object v0
.end method

.method protected final bridge synthetic d(Ljava/lang/Object;)Landroid/net/Uri;
    .locals 1

    check-cast p1, Lcom/google/android/youtube/core/model/Video;

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->mqThumbnailUri:Landroid/net/Uri;

    return-object v0
.end method
