.class final Lcom/google/android/youtube/app/honeycomb/phone/bw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;

.field private final b:Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->a:Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->b:Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/bw;)Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->b:Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->a:Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->b:Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->a(Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    const/4 v2, 0x0

    check-cast p2, Lcom/google/android/youtube/core/model/Page;

    iget-object v0, p2, Lcom/google/android/youtube/core/model/Page;->entries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p2, Lcom/google/android/youtube/core/model/Page;->entries:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Video;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->a:Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->b(Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;)Lcom/google/android/youtube/app/adapter/k;

    move-result-object v1

    invoke-virtual {v1, v2, v0}, Lcom/google/android/youtube/app/adapter/k;->a(ILjava/lang/Object;)Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->a:Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->c(Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->a:Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->c(Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->b:Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    iget v1, v1, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;->labelStringId:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/bx;

    invoke-direct {v0, p0, p2, v2}, Lcom/google/android/youtube/app/honeycomb/phone/bx;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/bw;Lcom/google/android/youtube/core/model/Page;B)V

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->a:Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->d(Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->a:Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->e(Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->a:Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->b:Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->a(Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;)V

    goto :goto_0
.end method
