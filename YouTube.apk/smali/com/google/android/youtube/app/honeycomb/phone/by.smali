.class public final Lcom/google/android/youtube/app/honeycomb/phone/by;
.super Lcom/google/android/youtube/app/honeycomb/phone/x;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/remote/al;
.implements Lcom/google/android/youtube/app/ui/ab;
.implements Lcom/google/android/youtube/core/async/bk;


# instance fields
.field private final b:Lcom/google/android/youtube/core/client/bc;

.field private final c:Lcom/google/android/youtube/core/client/be;

.field private final d:Lcom/google/android/youtube/app/remote/RemoteControl;

.field private final e:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private final f:Landroid/view/View;

.field private final g:Lcom/google/android/youtube/core/ui/PagedListView;

.field private final h:Landroid/content/res/Resources;

.field private final i:Lcom/google/android/youtube/app/ui/bx;

.field private j:Lcom/google/android/youtube/core/model/UserAuth;

.field private k:Ljava/lang/String;

.field private l:Lcom/google/android/youtube/app/adapter/bm;

.field private m:Lcom/google/android/youtube/app/ui/bu;

.field private n:Lcom/google/android/youtube/app/ui/v;

.field private o:I

.field private final p:Lcom/google/android/youtube/core/Analytics;

.field private q:Landroid/view/View;

.field private final r:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/Analytics;Landroid/graphics/Typeface;)V
    .locals 8

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/x;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;)V

    const-string v0, "analytics can not be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/Analytics;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->p:Lcom/google/android/youtube/core/Analytics;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->h:Landroid/content/res/Resources;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->a()Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->b:Lcom/google/android/youtube/core/client/bc;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->e_()Lcom/google/android/youtube/core/client/be;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->c:Lcom/google/android/youtube/core/client/be;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->z()Lcom/google/android/youtube/app/remote/bf;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->d:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->U()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->e:Lcom/google/android/youtube/core/async/UserAuthorizer;

    new-instance v0, Lcom/google/android/youtube/app/ui/v;

    const/16 v2, 0x3ec

    invoke-direct {v0, p1, v2}, Lcom/google/android/youtube/app/ui/v;-><init>(Landroid/app/Activity;I)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->n:Lcom/google/android/youtube/app/ui/v;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->n:Lcom/google/android/youtube/app/ui/v;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/ui/v;->a(Lcom/google/android/youtube/app/ui/ab;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->n:Lcom/google/android/youtube/app/ui/v;

    const v2, 0x7f0b023f

    const v3, 0x7f0200d0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/youtube/app/ui/v;->a(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->o:I

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->c:Lcom/google/android/youtube/core/client/be;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->l()Lcom/google/android/youtube/core/utils/p;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->d:Lcom/google/android/youtube/app/remote/RemoteControl;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->n:Lcom/google/android/youtube/app/ui/v;

    new-instance v5, Lcom/google/android/youtube/app/adapter/cj;

    invoke-direct {v5, p1}, Lcom/google/android/youtube/app/adapter/cj;-><init>(Landroid/content/Context;)V

    new-instance v6, Lcom/google/android/youtube/app/adapter/bh;

    invoke-direct {v6, p1, v3, v4, p3}, Lcom/google/android/youtube/app/adapter/bh;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/app/remote/RemoteControl;Lcom/google/android/youtube/app/ui/v;Lcom/google/android/youtube/core/Analytics;)V

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/Util;->g(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v3

    invoke-virtual {v5, v3}, Lcom/google/android/youtube/app/adapter/cj;->a(Landroid/graphics/Typeface;)V

    const/4 v3, 0x0

    invoke-static {p1, v0, v2, v3}, Lcom/google/android/youtube/app/adapter/cm;->a(Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/utils/p;Z)Lcom/google/android/youtube/app/adapter/cm;

    move-result-object v0

    new-instance v2, Lcom/google/android/youtube/app/adapter/af;

    invoke-direct {v2}, Lcom/google/android/youtube/app/adapter/af;-><init>()V

    invoke-virtual {v2, v5}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v0

    new-instance v2, Lcom/google/android/youtube/app/adapter/bm;

    const v3, 0x7f040033

    invoke-direct {v2, p1, v3, v0}, Lcom/google/android/youtube/app/adapter/bm;-><init>(Landroid/content/Context;ILcom/google/android/youtube/app/adapter/bv;)V

    iput-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->l:Lcom/google/android/youtube/app/adapter/bm;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->l:Lcom/google/android/youtube/app/adapter/bm;

    invoke-static {p1, v0}, Lcom/google/android/youtube/app/ui/bx;->a(Landroid/content/Context;Lcom/google/android/youtube/core/a/a;)Lcom/google/android/youtube/app/ui/bx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->i:Lcom/google/android/youtube/app/ui/bx;

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/by;->p()V

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f04007f

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/by;->o()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v2, v3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->f:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->f:Landroid/view/View;

    const v2, 0x7f070120

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/ui/PagedListView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->g:Lcom/google/android/youtube/core/ui/PagedListView;

    new-instance v0, Lcom/google/android/youtube/app/ui/bu;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->g:Lcom/google/android/youtube/core/ui/PagedListView;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->i:Lcom/google/android/youtube/app/ui/bx;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->b:Lcom/google/android/youtube/core/client/bc;

    invoke-interface {v4}, Lcom/google/android/youtube/core/client/bc;->A()Lcom/google/android/youtube/core/async/au;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/e;

    move-result-object v5

    move-object v1, p1

    move-object v6, p2

    move-object v7, p3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/youtube/app/ui/bu;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/PagedView;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/Analytics;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->m:Lcom/google/android/youtube/app/ui/bu;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->m:Lcom/google/android/youtube/app/ui/bu;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/bu;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->f:Landroid/view/View;

    const v1, 0x7f070121

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->q:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->q:Landroid/view/View;

    const v1, 0x7f07010a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->q:Landroid/view/View;

    const v1, 0x7f07010b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->h:Landroid/content/res/Resources;

    const v2, 0x7f0b0267

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, p4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/bz;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/honeycomb/phone/bz;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/by;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->r:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/by;)Lcom/google/android/youtube/app/remote/RemoteControl;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->d:Lcom/google/android/youtube/app/remote/RemoteControl;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/by;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/by;->b(Ljava/util/List;)V

    return-void
.end method

.method private b(Ljava/util/List;)V
    .locals 5

    const/16 v2, 0x8

    const/4 v1, 0x0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->q:Landroid/view/View;

    if-eqz v3, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->g:Lcom/google/android/youtube/core/ui/PagedListView;

    if-nez v3, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/PagedListView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->m:Lcom/google/android/youtube/app/ui/bu;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/bu;->a(Ljava/util/List;)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method private p()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->i:Lcom/google/android/youtube/app/ui/bx;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->h:Landroid/content/res/Resources;

    const v2, 0x7f0a000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/bx;->a(I)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/x;->a(Landroid/content/res/Configuration;)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/by;->p()V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->l:Lcom/google/android/youtube/app/adapter/bm;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/bm;->notifyDataSetChanged()V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/remote/RemoteControl$State;)V
    .locals 4

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/ca;->a:[I

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->d:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->v()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/youtube/app/remote/RemoteControl$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->q:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->g:Lcom/google/android/youtube/core/ui/PagedListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/PagedListView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->g:Lcom/google/android/youtube/core/ui/PagedListView;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/ui/PagedListView;->e()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->m:Lcom/google/android/youtube/app/ui/bu;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/bu;->b()V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->r:Landroid/os/Handler;

    const/4 v1, 0x1

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lcom/google/android/youtube/core/model/SubtitleTrack;)V
    .locals 0

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->j:Lcom/google/android/youtube/core/model/UserAuth;

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->k:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->k:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->d:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->j()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->l:Lcom/google/android/youtube/app/adapter/bm;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/bm;->notifyDataSetChanged()V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->j:Lcom/google/android/youtube/core/model/UserAuth;

    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->r:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/by;->b(Ljava/util/List;)V

    return-void
.end method

.method public final synthetic a(ILjava/lang/Object;)Z
    .locals 2

    check-cast p2, Lcom/google/android/youtube/core/model/Video;

    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->o:I

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->d:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->d:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->g()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p2, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->d:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->e()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->d:Lcom/google/android/youtube/app/remote/RemoteControl;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->p:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "RemoteQueueDeleteQueueLayer"

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(I)Landroid/app/Dialog;
    .locals 1

    const/16 v0, 0x3ec

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->n:Lcom/google/android/youtube/app/ui/v;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/v;->b()Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/x;->c()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->e:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bk;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->d:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->v()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-eq v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->d:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->j()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/by;->b(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->d:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/app/remote/RemoteControl;->a(Lcom/google/android/youtube/app/remote/al;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->d:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/app/remote/RemoteControl;->c(Lcom/google/android/youtube/app/remote/al;)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/by;->p()V

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 5

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    const v1, 0x7f0b023d

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->d:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v4}, Lcom/google/android/youtube/app/remote/RemoteControl;->t()Lcom/google/android/youtube/app/remote/bb;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/youtube/app/remote/bb;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final g_()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/by;->j:Lcom/google/android/youtube/core/model/UserAuth;

    return-void
.end method

.method public final n()V
    .locals 0

    return-void
.end method
