.class public final Lcom/google/android/youtube/app/honeycomb/phone/ci;
.super Landroid/support/v4/app/d;
.source "SourceFile"


# instance fields
.field private Y:Lcom/google/android/youtube/app/honeycomb/phone/cl;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/d;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/ci;)Lcom/google/android/youtube/app/honeycomb/phone/cl;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ci;->Y:Lcom/google/android/youtube/app/honeycomb/phone/cl;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/app/honeycomb/phone/cl;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ci;->Y:Lcom/google/android/youtube/app/honeycomb/phone/cl;

    return-void
.end method

.method public final c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    const v0, 0x7f0b0264

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ci;->i()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "screenName"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/ci;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ci;->j()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f0b0265

    new-instance v3, Lcom/google/android/youtube/app/honeycomb/phone/ck;

    invoke-direct {v3, p0}, Lcom/google/android/youtube/app/honeycomb/phone/ck;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/ci;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f0b0017

    new-instance v3, Lcom/google/android/youtube/app/honeycomb/phone/cj;

    invoke-direct {v3, p0}, Lcom/google/android/youtube/app/honeycomb/phone/cj;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/ci;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
