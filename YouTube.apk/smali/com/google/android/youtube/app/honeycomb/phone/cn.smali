.class public final Lcom/google/android/youtube/app/honeycomb/phone/cn;
.super Landroid/support/v4/app/d;
.source "SourceFile"


# instance fields
.field private Y:Lcom/google/android/youtube/app/honeycomb/phone/cr;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/d;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/cn;)Lcom/google/android/youtube/app/honeycomb/phone/cr;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/cn;->Y:Lcom/google/android/youtube/app/honeycomb/phone/cr;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/app/honeycomb/phone/cr;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/cn;->Y:Lcom/google/android/youtube/app/honeycomb/phone/cr;

    return-void
.end method

.method public final c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7

    const/4 v6, 0x0

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/cn;->j()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/cn;->j()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f040080

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/cn;->i()Landroid/os/Bundle;

    move-result-object v2

    new-instance v3, Lcom/google/android/ytremote/model/ScreenId;

    const-string v4, "screenId"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/android/ytremote/model/ScreenId;-><init>(Ljava/lang/String;)V

    const-string v4, "screenName"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    const v4, 0x7f0b0263

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v2, v5, v6

    invoke-virtual {p0, v4, v5}, Lcom/google/android/youtube/app/honeycomb/phone/cn;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v4, 0x7f0b0016

    new-instance v5, Lcom/google/android/youtube/app/honeycomb/phone/cp;

    invoke-direct {v5, p0, v0, v3}, Lcom/google/android/youtube/app/honeycomb/phone/cp;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/cn;Landroid/widget/EditText;Lcom/google/android/ytremote/model/ScreenId;)V

    invoke-virtual {v2, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0b0017

    new-instance v4, Lcom/google/android/youtube/app/honeycomb/phone/co;

    invoke-direct {v4, p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/co;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/cn;Landroid/widget/EditText;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/app/honeycomb/phone/cs;

    invoke-direct {v2, p0, v6}, Lcom/google/android/youtube/app/honeycomb/phone/cs;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/cn;B)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    return-object v1
.end method

.method public final e()V
    .locals 2

    invoke-super {p0}, Landroid/support/v4/app/d;->e()V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/cn;->b()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method
