.class final Lcom/google/android/youtube/app/honeycomb/phone/cx;
.super Lcom/google/android/youtube/app/ui/a;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;Landroid/app/Activity;Lcom/google/android/youtube/core/ui/PagedView;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/e;Z)V
    .locals 7

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/cx;->a:Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;

    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/app/ui/a;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/PagedView;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/e;Z)V

    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/youtube/app/ui/a;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/cx;->a:Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->b(Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;)V

    return-void
.end method

.method protected final a(Lcom/google/android/youtube/core/async/GDataRequest;Ljava/util/List;)V
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/app/ui/a;->a(Lcom/google/android/youtube/core/async/GDataRequest;Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/cx;->a:Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->a(Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;)Lcom/google/android/youtube/core/ui/PagedListView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/PagedListView;->setVisibility(I)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Event;I)V
    .locals 4

    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/Event;->targetIsVideo()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Event;->targetVideo:Lcom/google/android/youtube/core/model/Video;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/cx;->a:Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->d(Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;)Lcom/google/android/youtube/core/Analytics;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/core/Analytics$VideoCategory;->HomeFeed:Lcom/google/android/youtube/core/Analytics$VideoCategory;

    invoke-interface {v0, v1, p2}, Lcom/google/android/youtube/core/Analytics;->a(Lcom/google/android/youtube/core/Analytics$VideoCategory;I)V

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/cz;->a:[I

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Event;->action:Lcom/google/android/youtube/core/model/Event$Action;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/model/Event$Action;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    sget-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_RIVER_ACTIVTY:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/cx;->a:Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->e(Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;)Lcom/google/android/youtube/app/d;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/youtube/core/model/Event;->target:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/youtube/app/d;->a(Ljava/lang/String;ZLcom/google/android/youtube/core/client/VideoStats2Client$Feature;)V

    :cond_0
    :goto_1
    return-void

    :pswitch_0
    sget-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_RIVER_ACTIVTY_UPLOAD:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_RIVER_RECOMMENDED:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/cx;->a:Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->e(Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;)Lcom/google/android/youtube/app/d;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Event;->target:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/d;->a(Ljava/lang/String;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lcom/google/android/youtube/core/model/Event;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/cx;->a:Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;->c(Lcom/google/android/youtube/app/honeycomb/phone/TheFeedLayer;)Lcom/google/android/youtube/app/ui/ar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/ar;->a(Lcom/google/android/youtube/core/model/Event;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    check-cast p1, Lcom/google/android/youtube/core/model/Event;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/cx;->a(Lcom/google/android/youtube/core/model/Event;)Z

    move-result v0

    return v0
.end method
