.class final Lcom/google/android/youtube/app/honeycomb/phone/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/adapter/bl;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/honeycomb/phone/c;

.field private final b:Landroid/view/View;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/ImageView;

.field private final e:Landroid/widget/ImageView;

.field private final f:Lcom/google/android/youtube/app/adapter/k;

.field private g:Lcom/google/android/youtube/core/async/p;

.field private h:Lcom/google/android/youtube/core/async/p;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/c;Landroid/view/View;Landroid/view/ViewGroup;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->a:Lcom/google/android/youtube/app/honeycomb/phone/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x7f070032

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->b:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->b:Landroid/view/View;

    const v1, 0x7f07002c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->c:Landroid/widget/TextView;

    const v0, 0x7f070034

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->d:Landroid/widget/ImageView;

    const v0, 0x7f070035

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->e:Landroid/widget/ImageView;

    invoke-static {p1}, Lcom/google/android/youtube/app/honeycomb/phone/c;->a(Lcom/google/android/youtube/app/honeycomb/phone/c;)Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;->a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/youtube/app/adapter/bl;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/k;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->f:Lcom/google/android/youtube/app/adapter/k;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/c;Landroid/view/View;Landroid/view/ViewGroup;B)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/app/honeycomb/phone/e;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/c;Landroid/view/View;Landroid/view/ViewGroup;)V

    return-void
.end method

.method private a()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->e:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->g:Lcom/google/android/youtube/core/async/p;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->g:Lcom/google/android/youtube/core/async/p;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/p;->a()V

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->g:Lcom/google/android/youtube/core/async/p;

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->h:Lcom/google/android/youtube/core/async/p;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->h:Lcom/google/android/youtube/core/async/p;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/p;->a()V

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->h:Lcom/google/android/youtube/core/async/p;

    :cond_2
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/e;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/e;->a()V

    return-void
.end method

.method static synthetic b(Lcom/google/android/youtube/app/honeycomb/phone/e;)Lcom/google/android/youtube/app/adapter/k;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->f:Lcom/google/android/youtube/app/adapter/k;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(ILjava/lang/Object;)Landroid/view/View;
    .locals 4

    const/4 v3, 0x1

    check-cast p2, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->c:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->c:Landroid/widget/TextView;

    iget v1, p2, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;->labelStringId:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->d:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->d:Landroid/widget/ImageView;

    iget v1, p2, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;->defaultThumbnailId:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_1
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/e;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->a:Lcom/google/android/youtube/app/honeycomb/phone/c;

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/c;->a:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->g(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;)Lcom/google/android/youtube/core/model/UserAuth;

    move-result-object v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;->PLAYLISTS:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;

    if-ne p2, v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->a:Lcom/google/android/youtube/app/honeycomb/phone/c;

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/c;->a:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/f;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/f;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/e;B)V

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/async/p;->a(Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/p;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->h:Lcom/google/android/youtube/core/async/p;

    :goto_0
    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/b;->a:[I

    invoke-virtual {p2}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->b:Landroid/view/View;

    return-object v0

    :cond_3
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->a:Lcom/google/android/youtube/app/honeycomb/phone/c;

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/c;->a:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/g;

    invoke-direct {v1, p0, p2}, Lcom/google/android/youtube/app/honeycomb/phone/g;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/e;Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;)V

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/async/p;->a(Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/p;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->g:Lcom/google/android/youtube/core/async/p;

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->a:Lcom/google/android/youtube/app/honeycomb/phone/c;

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/c;->a:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->h(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;)Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->a:Lcom/google/android/youtube/app/honeycomb/phone/c;

    iget-object v1, v1, Lcom/google/android/youtube/app/honeycomb/phone/c;->a:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->g(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;)Lcom/google/android/youtube/core/model/UserAuth;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->g:Lcom/google/android/youtube/core/async/p;

    invoke-interface {v0, v1, v3, v2}, Lcom/google/android/youtube/core/client/bc;->a(Lcom/google/android/youtube/core/model/UserAuth;ILcom/google/android/youtube/core/async/n;)V

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->a:Lcom/google/android/youtube/app/honeycomb/phone/c;

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/c;->a:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->h(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;)Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->a:Lcom/google/android/youtube/app/honeycomb/phone/c;

    iget-object v1, v1, Lcom/google/android/youtube/app/honeycomb/phone/c;->a:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->g(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;)Lcom/google/android/youtube/core/model/UserAuth;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->g:Lcom/google/android/youtube/core/async/p;

    invoke-interface {v0, v1, v3, v2}, Lcom/google/android/youtube/core/client/bc;->d(Lcom/google/android/youtube/core/model/UserAuth;ILcom/google/android/youtube/core/async/n;)V

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->a:Lcom/google/android/youtube/app/honeycomb/phone/c;

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/c;->a:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->h(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;)Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->a:Lcom/google/android/youtube/app/honeycomb/phone/c;

    iget-object v1, v1, Lcom/google/android/youtube/app/honeycomb/phone/c;->a:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->g(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;)Lcom/google/android/youtube/core/model/UserAuth;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->g:Lcom/google/android/youtube/core/async/p;

    invoke-interface {v0, v1, v3, v2}, Lcom/google/android/youtube/core/client/bc;->b(Lcom/google/android/youtube/core/model/UserAuth;ILcom/google/android/youtube/core/async/n;)V

    goto :goto_1

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->a:Lcom/google/android/youtube/app/honeycomb/phone/c;

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/c;->a:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->h(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;)Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->a:Lcom/google/android/youtube/app/honeycomb/phone/c;

    iget-object v1, v1, Lcom/google/android/youtube/app/honeycomb/phone/c;->a:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->g(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;)Lcom/google/android/youtube/core/model/UserAuth;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->g:Lcom/google/android/youtube/core/async/p;

    invoke-interface {v0, v1, v3, v2}, Lcom/google/android/youtube/core/client/bc;->c(Lcom/google/android/youtube/core/model/UserAuth;ILcom/google/android/youtube/core/async/n;)V

    goto :goto_1

    :pswitch_4
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->a:Lcom/google/android/youtube/app/honeycomb/phone/c;

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/c;->a:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->h(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;)Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->a:Lcom/google/android/youtube/app/honeycomb/phone/c;

    iget-object v1, v1, Lcom/google/android/youtube/app/honeycomb/phone/c;->a:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->g(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;)Lcom/google/android/youtube/core/model/UserAuth;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/e;->h:Lcom/google/android/youtube/core/async/p;

    invoke-interface {v0, v1, v3, v2}, Lcom/google/android/youtube/core/client/bc;->e(Lcom/google/android/youtube/core/model/UserAuth;ILcom/google/android/youtube/core/async/n;)V

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method
