.class public final Lcom/google/android/youtube/app/honeycomb/phone/p;
.super Lcom/google/android/youtube/app/honeycomb/phone/bd;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/ui/cq;
.implements Lcom/google/android/youtube/app/ui/j;


# instance fields
.field private A:Lcom/google/android/youtube/app/ui/a;

.field private B:Lcom/google/android/youtube/app/ui/SubscribeHelper;

.field private final C:Lcom/google/android/youtube/app/ui/cw;

.field private D:Z

.field private final b:Landroid/net/Uri;

.field private final c:Landroid/view/LayoutInflater;

.field private final d:Landroid/content/res/Resources;

.field private final e:Lcom/google/android/youtube/core/client/be;

.field private final f:Lcom/google/android/youtube/core/client/bg;

.field private final g:Lcom/google/android/youtube/core/e;

.field private final h:Lcom/google/android/youtube/app/prefetch/d;

.field private final i:Lcom/google/android/youtube/core/utils/p;

.field private final j:Lcom/google/android/youtube/core/client/bc;

.field private final k:Lcom/google/android/youtube/core/async/au;

.field private final l:Lcom/google/android/youtube/core/Analytics;

.field private final m:Lcom/google/android/youtube/app/d;

.field private final n:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private final o:Lcom/google/android/youtube/app/honeycomb/phone/t;

.field private p:Lcom/google/android/youtube/core/model/Branding;

.field private q:Z

.field private r:Z

.field private s:Lcom/google/android/youtube/app/ui/bx;

.field private t:Lcom/google/android/youtube/app/ui/bx;

.field private u:Lcom/google/android/youtube/core/model/UserProfile;

.field private v:Landroid/view/View;

.field private w:Lcom/google/android/youtube/app/ui/g;

.field private x:Lcom/google/android/youtube/app/ui/ea;

.field private y:Landroid/view/View;

.field private z:Lcom/google/android/youtube/app/ui/g;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Landroid/net/Uri;Lcom/google/android/youtube/app/ui/cw;)V
    .locals 9

    const/4 v8, 0x0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/bd;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;)V

    const-string v0, "channeluri cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->b:Landroid/net/Uri;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->c:Landroid/view/LayoutInflater;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->d:Landroid/content/res/Resources;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->e_()Lcom/google/android/youtube/core/client/be;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->e:Lcom/google/android/youtube/core/client/be;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->w()Lcom/google/android/youtube/core/client/bg;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->f:Lcom/google/android/youtube/core/client/bg;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->l()Lcom/google/android/youtube/core/utils/p;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->i:Lcom/google/android/youtube/core/utils/p;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->E()Lcom/google/android/youtube/app/prefetch/d;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->h:Lcom/google/android/youtube/app/prefetch/d;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->a()Lcom/google/android/youtube/core/client/bc;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->j:Lcom/google/android/youtube/core/client/bc;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->j:Lcom/google/android/youtube/core/client/bc;

    invoke-interface {v1}, Lcom/google/android/youtube/core/client/bc;->l()Lcom/google/android/youtube/core/async/au;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->k:Lcom/google/android/youtube/core/async/au;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/e;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->g:Lcom/google/android/youtube/core/e;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->i()Lcom/google/android/youtube/core/Analytics;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->l:Lcom/google/android/youtube/core/Analytics;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->F()Lcom/google/android/youtube/app/d;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->m:Lcom/google/android/youtube/app/d;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->U()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->n:Lcom/google/android/youtube/core/async/UserAuthorizer;

    new-instance v0, Lcom/google/android/youtube/app/ui/SubscribeHelper;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->l:Lcom/google/android/youtube/core/Analytics;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->n:Lcom/google/android/youtube/core/async/UserAuthorizer;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->j:Lcom/google/android/youtube/core/client/bc;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->g:Lcom/google/android/youtube/core/e;

    const-string v7, "ChannelLayer"

    move-object v1, p1

    move-object v6, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/youtube/app/ui/SubscribeHelper;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/app/ui/cq;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->B:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    iput-object p3, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->C:Lcom/google/android/youtube/app/ui/cw;

    iput-boolean v8, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->D:Z

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/t;

    invoke-direct {v0, p0, v8}, Lcom/google/android/youtube/app/honeycomb/phone/t;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/p;B)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->o:Lcom/google/android/youtube/app/honeycomb/phone/t;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->p:Lcom/google/android/youtube/core/model/Branding;

    iput-boolean v8, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->q:Z

    iput-boolean v8, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->r:Z

    return-void
.end method

.method private a(Landroid/view/ViewGroup;)Landroid/util/Pair;
    .locals 6

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f040044

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    new-instance v0, Lcom/google/android/youtube/app/ui/g;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->e:Lcom/google/android/youtube/core/client/be;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->g:Lcom/google/android/youtube/core/e;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->m:Lcom/google/android/youtube/app/d;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/ui/g;-><init>(Landroid/app/Activity;Landroid/view/View;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/app/d;)V

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/ui/g;->a(Lcom/google/android/youtube/app/ui/j;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/g;->a(Z)V

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/p;->b(Lcom/google/android/youtube/app/ui/g;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->u:Lcom/google/android/youtube/core/model/UserProfile;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->u:Lcom/google/android/youtube/core/model/UserProfile;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/g;->a(Lcom/google/android/youtube/core/model/UserProfile;)V

    iget-boolean v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->q:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->p:Lcom/google/android/youtube/core/model/Branding;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/g;->a(Lcom/google/android/youtube/core/model/Branding;)V

    :cond_0
    invoke-static {v2, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/p;)Lcom/google/android/youtube/core/Analytics;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->l:Lcom/google/android/youtube/core/Analytics;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/p;Lcom/google/android/youtube/core/model/UserProfile;)Lcom/google/android/youtube/core/model/UserProfile;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->u:Lcom/google/android/youtube/core/model/UserProfile;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/p;Lcom/google/android/youtube/core/model/Branding;)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->q:Z

    if-nez v0, :cond_2

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->p:Lcom/google/android/youtube/core/model/Branding;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->q:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->w:Lcom/google/android/youtube/app/ui/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->w:Lcom/google/android/youtube/app/ui/g;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/g;->a(Lcom/google/android/youtube/core/model/Branding;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->z:Lcom/google/android/youtube/app/ui/g;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->z:Lcom/google/android/youtube/app/ui/g;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/g;->a(Lcom/google/android/youtube/core/model/Branding;)V

    :cond_1
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/p;->w()V

    :cond_2
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/p;Lcom/google/android/youtube/core/ui/PagedView$State;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/p;->a(Lcom/google/android/youtube/core/ui/PagedView$State;)V

    return-void
.end method

.method private a(Lcom/google/android/youtube/app/ui/g;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->B:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->d()Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->B:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->c()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/youtube/app/ui/g;->a(Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;Z)V

    return-void
.end method

.method private a(Lcom/google/android/youtube/core/ui/PagedView$State;)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->r:Z

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/youtube/core/ui/PagedView$State;->LOADING:Lcom/google/android/youtube/core/ui/PagedView$State;

    if-eq v0, p1, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->r:Z

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/p;->w()V

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/google/android/youtube/app/honeycomb/phone/p;)Lcom/google/android/youtube/app/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->m:Lcom/google/android/youtube/app/d;

    return-object v0
.end method

.method private b(Lcom/google/android/youtube/app/ui/g;)V
    .locals 4

    const/4 v3, 0x1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->d:Landroid/content/res/Resources;

    const v1, 0x7f0e0005

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->d:Landroid/content/res/Resources;

    const v2, 0x7f0f0006

    invoke-virtual {v1, v2, v3, v3}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/youtube/app/ui/g;->a(ZF)V

    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/google/android/youtube/app/honeycomb/phone/p;)Lcom/google/android/youtube/core/model/UserProfile;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->u:Lcom/google/android/youtube/core/model/UserProfile;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/honeycomb/phone/p;)Lcom/google/android/youtube/app/ui/SubscribeHelper;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->B:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/app/honeycomb/phone/p;)Lcom/google/android/youtube/app/ui/ea;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->x:Lcom/google/android/youtube/app/ui/ea;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/youtube/app/honeycomb/phone/p;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/p;->t()V

    return-void
.end method

.method static synthetic g(Lcom/google/android/youtube/app/honeycomb/phone/p;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->v:Landroid/view/View;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/youtube/app/honeycomb/phone/p;)Lcom/google/android/youtube/app/ui/g;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->w:Lcom/google/android/youtube/app/ui/g;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/youtube/app/honeycomb/phone/p;)Lcom/google/android/youtube/app/ui/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->A:Lcom/google/android/youtube/app/ui/a;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/youtube/app/honeycomb/phone/p;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/p;->u()V

    return-void
.end method

.method static synthetic k(Lcom/google/android/youtube/app/honeycomb/phone/p;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->y:Landroid/view/View;

    return-object v0
.end method

.method static synthetic l(Lcom/google/android/youtube/app/honeycomb/phone/p;)Lcom/google/android/youtube/app/ui/g;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->z:Lcom/google/android/youtube/app/ui/g;

    return-object v0
.end method

.method static synthetic m(Lcom/google/android/youtube/app/honeycomb/phone/p;)Lcom/google/android/youtube/app/honeycomb/phone/t;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->o:Lcom/google/android/youtube/app/honeycomb/phone/t;

    return-object v0
.end method

.method static synthetic n(Lcom/google/android/youtube/app/honeycomb/phone/p;)Lcom/google/android/youtube/core/client/bc;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->j:Lcom/google/android/youtube/core/client/bc;

    return-object v0
.end method

.method private s()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->j:Lcom/google/android/youtube/core/client/bc;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->b:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    new-instance v3, Lcom/google/android/youtube/app/honeycomb/phone/u;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/google/android/youtube/app/honeycomb/phone/u;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/p;B)V

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/bc;->c(Landroid/net/Uri;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method

.method private t()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->x:Lcom/google/android/youtube/app/ui/ea;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->j:Lcom/google/android/youtube/core/client/bc;

    invoke-interface {v3}, Lcom/google/android/youtube/core/client/bc;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->u:Lcom/google/android/youtube/core/model/UserProfile;

    iget-object v4, v4, Lcom/google/android/youtube/core/model/UserProfile;->uploadsUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->b(Landroid/net/Uri;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/ea;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->x:Lcom/google/android/youtube/app/ui/ea;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/q;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/honeycomb/phone/q;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/p;)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/ea;->a(Lcom/google/android/youtube/core/ui/g;)V

    return-void
.end method

.method private u()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->A:Lcom/google/android/youtube/app/ui/a;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->j:Lcom/google/android/youtube/core/client/bc;

    invoke-interface {v3}, Lcom/google/android/youtube/core/client/bc;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->u:Lcom/google/android/youtube/core/model/UserProfile;

    iget-object v4, v4, Lcom/google/android/youtube/core/model/UserProfile;->activityUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->d(Landroid/net/Uri;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/a;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->A:Lcom/google/android/youtube/app/ui/a;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/s;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/honeycomb/phone/s;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/p;)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/a;->a(Lcom/google/android/youtube/core/ui/g;)V

    return-void
.end method

.method private v()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->d:Landroid/content/res/Resources;

    const v1, 0x7f0a000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->s:Lcom/google/android/youtube/app/ui/bx;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->s:Lcom/google/android/youtube/app/ui/bx;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/ui/bx;->a(I)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->t:Lcom/google/android/youtube/app/ui/bx;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->t:Lcom/google/android/youtube/app/ui/bx;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/ui/bx;->a(I)V

    :cond_1
    return-void
.end method

.method private w()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->q:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->r:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/p;->q()V

    :cond_0
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/youtube/core/ui/PagedListView;I)Lcom/google/android/youtube/core/a/a;
    .locals 13

    const v10, 0x7f080062

    const v9, 0x7f080061

    const/16 v8, 0x8

    const/4 v7, 0x1

    const/4 v6, 0x0

    packed-switch p2, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    const v1, 0x7f0b0114

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/core/ui/PagedListView;->setEmptyText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->e:Lcom/google/android/youtube/core/client/be;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->f:Lcom/google/android/youtube/core/client/bg;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->i:Lcom/google/android/youtube/core/utils/p;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->h:Lcom/google/android/youtube/app/prefetch/d;

    new-instance v4, Lcom/google/android/youtube/app/adapter/cj;

    invoke-direct {v4, v0}, Lcom/google/android/youtube/app/adapter/cj;-><init>(Landroid/content/Context;)V

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->g(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/youtube/app/adapter/cj;->a(Landroid/graphics/Typeface;)V

    invoke-static {v0, v1, v2, v7}, Lcom/google/android/youtube/app/adapter/cm;->a(Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/utils/p;Z)Lcom/google/android/youtube/app/adapter/cm;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/app/adapter/h;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/j;->d(Landroid/content/Context;)Z

    move-result v5

    invoke-direct {v2, v3, v5, v8, v6}, Lcom/google/android/youtube/app/adapter/h;-><init>(Lcom/google/android/youtube/app/prefetch/d;ZIZ)V

    new-instance v3, Lcom/google/android/youtube/app/adapter/af;

    invoke-direct {v3}, Lcom/google/android/youtube/app/adapter/af;-><init>()V

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v1

    new-instance v12, Lcom/google/android/youtube/app/adapter/bm;

    const v2, 0x7f040033

    invoke-direct {v12, v0, v2, v1}, Lcom/google/android/youtube/app/adapter/bm;-><init>(Landroid/content/Context;ILcom/google/android/youtube/app/adapter/bv;)V

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/p;->a(Landroid/view/ViewGroup;)Landroid/util/Pair;

    move-result-object v1

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->v:Landroid/view/View;

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/youtube/app/ui/g;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->w:Lcom/google/android/youtube/app/ui/g;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->v:Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/core/ui/PagedListView;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-static {v0, v12}, Lcom/google/android/youtube/app/ui/bx;->a(Landroid/content/Context;Lcom/google/android/youtube/core/a/a;)Lcom/google/android/youtube/app/ui/bx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->s:Lcom/google/android/youtube/app/ui/bx;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->s:Lcom/google/android/youtube/app/ui/bx;

    const v1, 0x7f080067

    const v2, 0x7f080064

    invoke-virtual {v0, v9, v1, v10, v2}, Lcom/google/android/youtube/app/ui/bx;->a(IIII)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/p;->v()V

    new-instance v0, Lcom/google/android/youtube/app/ui/ea;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->s:Lcom/google/android/youtube/app/ui/bx;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->k:Lcom/google/android/youtube/core/async/au;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->g:Lcom/google/android/youtube/core/e;

    iget-object v7, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->m:Lcom/google/android/youtube/app/d;

    sget-object v9, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CHANNEL_UPLOADS:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    iget-object v10, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->l:Lcom/google/android/youtube/core/Analytics;

    sget-object v11, Lcom/google/android/youtube/core/Analytics$VideoCategory;->ChannelUploads:Lcom/google/android/youtube/core/Analytics$VideoCategory;

    move-object v2, p1

    move v8, v6

    invoke-direct/range {v0 .. v11}, Lcom/google/android/youtube/app/ui/ea;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/PagedView;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/e;ZLcom/google/android/youtube/app/d;ZLcom/google/android/youtube/core/client/VideoStats2Client$Feature;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/Analytics$VideoCategory;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->x:Lcom/google/android/youtube/app/ui/ea;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->u:Lcom/google/android/youtube/core/model/UserProfile;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/p;->t()V

    :goto_0
    move-object v0, v12

    :goto_1
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->x:Lcom/google/android/youtube/app/ui/ea;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/ea;->e()V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    const v1, 0x7f0b0113

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/core/ui/PagedListView;->setEmptyText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->e:Lcom/google/android/youtube/core/client/be;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->f:Lcom/google/android/youtube/core/client/bg;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->i:Lcom/google/android/youtube/core/utils/p;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->h:Lcom/google/android/youtube/app/prefetch/d;

    new-instance v4, Lcom/google/android/youtube/app/adapter/cj;

    invoke-direct {v4, v0}, Lcom/google/android/youtube/app/adapter/cj;-><init>(Landroid/content/Context;)V

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->g(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/youtube/app/adapter/cj;->a(Landroid/graphics/Typeface;)V

    invoke-static {v0, v1, v2, v6}, Lcom/google/android/youtube/app/adapter/cm;->a(Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/utils/p;Z)Lcom/google/android/youtube/app/adapter/cm;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/app/adapter/h;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/j;->d(Landroid/content/Context;)Z

    move-result v5

    invoke-direct {v2, v3, v5, v8, v6}, Lcom/google/android/youtube/app/adapter/h;-><init>(Lcom/google/android/youtube/app/prefetch/d;ZIZ)V

    new-instance v3, Lcom/google/android/youtube/app/adapter/af;

    invoke-direct {v3}, Lcom/google/android/youtube/app/adapter/af;-><init>()V

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/app/adapter/an;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Lcom/google/android/youtube/app/adapter/an;-><init>(Landroid/content/res/Resources;Lcom/google/android/youtube/app/adapter/bv;)V

    new-instance v8, Lcom/google/android/youtube/app/adapter/bm;

    const v1, 0x7f040031

    invoke-direct {v8, v0, v1, v2}, Lcom/google/android/youtube/app/adapter/bm;-><init>(Landroid/content/Context;ILcom/google/android/youtube/app/adapter/bv;)V

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/p;->a(Landroid/view/ViewGroup;)Landroid/util/Pair;

    move-result-object v1

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->y:Landroid/view/View;

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/youtube/app/ui/g;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->z:Lcom/google/android/youtube/app/ui/g;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->y:Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/core/ui/PagedListView;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-static {v0, v8}, Lcom/google/android/youtube/app/ui/bx;->a(Landroid/content/Context;Lcom/google/android/youtube/core/a/a;)Lcom/google/android/youtube/app/ui/bx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->t:Lcom/google/android/youtube/app/ui/bx;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->t:Lcom/google/android/youtube/app/ui/bx;

    const v1, 0x7f080067

    const v2, 0x7f080064

    invoke-virtual {v0, v9, v1, v10, v2}, Lcom/google/android/youtube/app/ui/bx;->a(IIII)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/p;->v()V

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/r;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->t:Lcom/google/android/youtube/app/ui/bx;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->j:Lcom/google/android/youtube/core/client/bc;

    iget-object v6, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->g:Lcom/google/android/youtube/core/e;

    move-object v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/youtube/app/honeycomb/phone/r;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/p;Landroid/app/Activity;Lcom/google/android/youtube/core/ui/PagedView;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/e;Z)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->A:Lcom/google/android/youtube/app/ui/a;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->u:Lcom/google/android/youtube/core/model/UserProfile;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/p;->u()V

    :goto_2
    move-object v0, v8

    goto/16 :goto_1

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->A:Lcom/google/android/youtube/app/ui/a;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/a;->e()V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/bd;->a()V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/p;->s()V

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->q:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/p;->p()V

    :cond_0
    return-void
.end method

.method public final a(Landroid/content/res/Configuration;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/bd;->a(Landroid/content/res/Configuration;)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/p;->v()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->w:Lcom/google/android/youtube/app/ui/g;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/p;->b(Lcom/google/android/youtube/app/ui/g;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->z:Lcom/google/android/youtube/app/ui/g;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/p;->b(Lcom/google/android/youtube/app/ui/g;)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;)V
    .locals 2

    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->SUBSCRIBED:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    if-eq p1, v1, :cond_0

    sget-object v1, Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->NOT_SUBSCRIBED:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    if-ne p1, v1, :cond_1

    :cond_0
    iget-boolean v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->D:Z

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->D:Z

    move v0, v1

    :cond_1
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->w:Lcom/google/android/youtube/app/ui/g;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->w:Lcom/google/android/youtube/app/ui/g;

    invoke-direct {p0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/p;->a(Lcom/google/android/youtube/app/ui/g;)V

    :cond_2
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->z:Lcom/google/android/youtube/app/ui/g;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->z:Lcom/google/android/youtube/app/ui/g;

    invoke-direct {p0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/p;->a(Lcom/google/android/youtube/app/ui/g;)V

    :cond_3
    sget-object v1, Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->WORKING:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    if-ne p1, v1, :cond_5

    :cond_4
    :goto_0
    return-void

    :cond_5
    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->C:Lcom/google/android/youtube/app/ui/cw;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->SUBSCRIBED:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    if-ne p1, v0, :cond_6

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->C:Lcom/google/android/youtube/app/ui/cw;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->B:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->e()Lcom/google/android/youtube/core/model/Subscription;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/ui/cw;->a(Lcom/google/android/youtube/core/model/Subscription;)V

    goto :goto_0

    :cond_6
    sget-object v0, Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->NOT_SUBSCRIBED:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    if-ne p1, v0, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->C:Lcom/google/android/youtube/app/ui/cw;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->u:Lcom/google/android/youtube/core/model/UserProfile;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/UserProfile;->uri:Landroid/net/Uri;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/ui/cw;->a(Landroid/net/Uri;)V

    goto :goto_0
.end method

.method protected final b(I)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/bd;->b(I)V

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->x:Lcom/google/android/youtube/app/ui/ea;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/ea;->g()Lcom/google/android/youtube/core/ui/PagedView$State;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/p;->a(Lcom/google/android/youtube/core/ui/PagedView$State;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->w:Lcom/google/android/youtube/app/ui/g;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/p;->a(Lcom/google/android/youtube/app/ui/g;)V

    :goto_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->A:Lcom/google/android/youtube/app/ui/a;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/a;->g()Lcom/google/android/youtube/core/ui/PagedView$State;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/p;->a(Lcom/google/android/youtube/core/ui/PagedView$State;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->z:Lcom/google/android/youtube/app/ui/g;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/p;->a(Lcom/google/android/youtube/app/ui/g;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected final c(I)Ljava/lang/String;
    .locals 2

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    const v1, 0x7f0b017d

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    const v1, 0x7f0b017c

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final n()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->u:Lcom/google/android/youtube/core/model/UserProfile;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->B:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->a()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/p;->s()V

    goto :goto_0
.end method

.method public final n_()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->D:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/p;->B:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->b()V

    return-void
.end method
