.class final Lcom/google/android/youtube/app/honeycomb/phone/r;
.super Lcom/google/android/youtube/app/ui/a;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/honeycomb/phone/p;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/p;Landroid/app/Activity;Lcom/google/android/youtube/core/ui/PagedView;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/e;Z)V
    .locals 7

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/r;->a:Lcom/google/android/youtube/app/honeycomb/phone/p;

    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/app/ui/a;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/PagedView;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/e;Z)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/Event;I)V
    .locals 4

    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/Event;->targetIsVideo()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Event;->targetVideo:Lcom/google/android/youtube/core/model/Video;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/r;->a:Lcom/google/android/youtube/app/honeycomb/phone/p;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/p;->a(Lcom/google/android/youtube/app/honeycomb/phone/p;)Lcom/google/android/youtube/core/Analytics;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/core/Analytics$VideoCategory;->ChannelActivity:Lcom/google/android/youtube/core/Analytics$VideoCategory;

    invoke-interface {v0, v1, p2}, Lcom/google/android/youtube/core/Analytics;->a(Lcom/google/android/youtube/core/Analytics$VideoCategory;I)V

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Event;->action:Lcom/google/android/youtube/core/model/Event$Action;

    sget-object v1, Lcom/google/android/youtube/core/model/Event$Action;->VIDEO_UPLOADED:Lcom/google/android/youtube/core/model/Event$Action;

    if-ne v0, v1, :cond_1

    sget-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CHANNEL_ACTIVITY_UPLOAD:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/r;->a:Lcom/google/android/youtube/app/honeycomb/phone/p;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/phone/p;->b(Lcom/google/android/youtube/app/honeycomb/phone/p;)Lcom/google/android/youtube/app/d;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/youtube/core/model/Event;->target:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/youtube/app/d;->a(Ljava/lang/String;ZLcom/google/android/youtube/core/client/VideoStats2Client$Feature;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    sget-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CHANNEL_ACTIVITY:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/r;->a:Lcom/google/android/youtube/app/honeycomb/phone/p;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/p;->b(Lcom/google/android/youtube/app/honeycomb/phone/p;)Lcom/google/android/youtube/app/d;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Event;->target:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/d;->a(Ljava/lang/String;)V

    goto :goto_1
.end method
