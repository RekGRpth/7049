.class public Lcom/google/android/youtube/app/honeycomb/phone/x;
.super Lcom/google/android/youtube/app/honeycomb/phone/j;
.source "SourceFile"


# instance fields
.field private final b:Landroid/view/View;

.field private final c:Landroid/view/View;

.field private d:Z

.field private e:Z


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;)V
    .locals 3

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/j;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/x;->e:Z

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04002a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/x;->b:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/x;->b:Landroid/view/View;

    const v1, 0x7f070082

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/x;->c:Landroid/view/View;

    return-void
.end method

.method private b()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/x;->e:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/x;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/x;->c:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/x;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->bringToFront()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/x;->c:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/x;->c:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public j()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/j;->j()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/x;->e:Z

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/x;->b()V

    return-void
.end method

.method public k()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/j;->k()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/x;->e:Z

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/x;->b()V

    return-void
.end method

.method public l()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/j;->l()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/x;->d:Z

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/x;->b()V

    return-void
.end method

.method public m()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/j;->m()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/x;->d:Z

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/x;->b()V

    return-void
.end method

.method public final o()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/x;->b:Landroid/view/View;

    return-object v0
.end method
