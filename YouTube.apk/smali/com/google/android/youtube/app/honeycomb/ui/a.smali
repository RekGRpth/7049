.class Lcom/google/android/youtube/app/honeycomb/ui/a;
.super Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field protected a:Lcom/google/android/youtube/app/compat/t;

.field protected b:Landroid/widget/SearchView;

.field protected c:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

.field protected d:Ljava/lang/String;

.field protected e:Z

.field protected final f:Ljava/util/List;


# direct methods
.method protected constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/youtube/app/d;Ljava/lang/String;Lcom/google/android/youtube/app/compat/r;)V
    .locals 1

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/youtube/app/d;Ljava/lang/String;Lcom/google/android/youtube/app/compat/r;)V

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->ICONIFIED:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->c:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->f:Ljava/util/List;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/ui/a;Ljava/lang/String;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->d:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->d:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/ui/a;->b()V

    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->c:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    return-object v0
.end method

.method public final a(Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->c:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->g:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/honeycomb/ui/i;)V
    .locals 1

    const-string v0, "listener can\'t be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    if-nez p1, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->d:Ljava/lang/String;

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->d:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->a:Lcom/google/android/youtube/app/compat/t;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->h:Lcom/google/android/youtube/core/Analytics;

    const-string v2, "Search"

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->g:Landroid/app/Activity;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    if-nez p1, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/SearchView;->setSubmitButtonEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    invoke-virtual {v0, v1}, Landroid/widget/SearchView;->setIconified(Z)V

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->clearFocus()V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public a(Lcom/google/android/youtube/app/compat/m;)Z
    .locals 5

    const v4, 0x7f070185

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Lcom/google/android/youtube/app/compat/m;)Z

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->c:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    sget-object v3, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->DISABLED:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    if-ne v0, v3, :cond_0

    invoke-virtual {p1, v4}, Lcom/google/android/youtube/app/compat/m;->f(I)V

    :goto_0
    return v1

    :cond_0
    invoke-virtual {p1, v4}, Lcom/google/android/youtube/app/compat/m;->c(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->a:Lcom/google/android/youtube/app/compat/t;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->a:Lcom/google/android/youtube/app/compat/t;

    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->d()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SearchView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->g:Landroid/app/Activity;

    const-string v3, "search"

    invoke-virtual {v0, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/SearchManager;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->g:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/app/SearchManager;->getSearchableInfo(Landroid/content/ComponentName;)Landroid/app/SearchableInfo;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/SearchView;->setSearchableInfo(Landroid/app/SearchableInfo;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    invoke-virtual {v0, v1}, Landroid/widget/SearchView;->setQueryRefinementEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    new-instance v3, Lcom/google/android/youtube/app/honeycomb/ui/b;

    invoke-direct {v3, p0}, Lcom/google/android/youtube/app/honeycomb/ui/b;-><init>(Lcom/google/android/youtube/app/honeycomb/ui/a;)V

    invoke-virtual {v0, v3}, Landroid/widget/SearchView;->setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    new-instance v3, Lcom/google/android/youtube/app/honeycomb/ui/c;

    invoke-direct {v3, p0}, Lcom/google/android/youtube/app/honeycomb/ui/c;-><init>(Lcom/google/android/youtube/app/honeycomb/ui/a;)V

    invoke-virtual {v0, v3}, Landroid/widget/SearchView;->setOnSuggestionListener(Landroid/widget/SearchView$OnSuggestionListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->c:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    sget-object v3, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->CUSTOM:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->a:Lcom/google/android/youtube/app/compat/t;

    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->c()Z

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->clearFocus()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    invoke-virtual {v0, v2}, Landroid/widget/SearchView;->setFocusable(Z)V

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->c:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    sget-object v4, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->ICONIFIED:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    if-ne v0, v4, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/SearchView;->setIconifiedByDefault(Z)V

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->a:Lcom/google/android/youtube/app/compat/t;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->c:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    sget-object v3, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->ICONIFIED:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    if-ne v0, v3, :cond_3

    const/16 v0, 0xa

    :goto_2
    invoke-interface {v2, v0}, Lcom/google/android/youtube/app/compat/t;->b(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->hasFocus()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->e:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    invoke-virtual {v0, p0}, Landroid/widget/SearchView;->setOnQueryTextFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    const/4 v0, 0x2

    goto :goto_2
.end method

.method public b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->a:Lcom/google/android/youtube/app/compat/t;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->c:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    sget-object v1, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->CUSTOM:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/SearchView;->setIconified(Z)V

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->e:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/ui/a;->b()V

    :cond_0
    return-void
.end method

.method protected final d()V
    .locals 0

    return-void
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 0

    iput-boolean p2, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->e:Z

    return-void
.end method
