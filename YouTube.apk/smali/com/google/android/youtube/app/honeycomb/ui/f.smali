.class final Lcom/google/android/youtube/app/honeycomb/ui/f;
.super Lcom/google/android/youtube/app/honeycomb/ui/a;
.source "SourceFile"


# instance fields
.field private final j:Lcom/google/android/youtube/app/honeycomb/ui/g;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/youtube/app/d;Ljava/lang/String;Lcom/google/android/youtube/app/compat/r;)V
    .locals 2

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/youtube/app/honeycomb/ui/a;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/youtube/app/d;Ljava/lang/String;Lcom/google/android/youtube/app/compat/r;)V

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/ui/g;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/honeycomb/ui/g;-><init>(Lcom/google/android/youtube/app/honeycomb/ui/f;B)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/f;->j:Lcom/google/android/youtube/app/honeycomb/ui/g;

    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/ui/a;->a(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/f;->a:Lcom/google/android/youtube/app/compat/t;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/f;->b:Landroid/widget/SearchView;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/f;->c:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    sget-object v1, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->DISABLED:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/f;->c:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    sget-object v1, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->CUSTOM:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/f;->a:Lcom/google/android/youtube/app/compat/t;

    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->c()Z

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/f;->b:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->clearFocus()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/youtube/app/compat/m;)Z
    .locals 3

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/ui/a;->a(Lcom/google/android/youtube/app/compat/m;)Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/f;->c:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    sget-object v2, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->DISABLED:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/f;->j:Lcom/google/android/youtube/app/honeycomb/ui/g;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/f;->a:Lcom/google/android/youtube/app/compat/t;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/f;->j:Lcom/google/android/youtube/app/honeycomb/ui/g;

    invoke-interface {v1, v2}, Lcom/google/android/youtube/app/compat/t;->a(Lcom/google/android/youtube/app/compat/u;)Lcom/google/android/youtube/app/compat/t;

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/f;->c:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    sget-object v2, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->CUSTOM:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/f;->b:Landroid/widget/SearchView;

    const v2, 0x2000003

    invoke-virtual {v1, v2}, Landroid/widget/SearchView;->setImeOptions(I)V

    :cond_1
    return v0
.end method

.method public final b()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/ui/a;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/f;->a:Lcom/google/android/youtube/app/compat/t;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/f;->b:Landroid/widget/SearchView;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/f;->c:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    sget-object v1, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->DISABLED:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/f;->c:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    sget-object v1, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->CUSTOM:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/f;->a:Lcom/google/android/youtube/app/compat/t;

    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->b()Z

    goto :goto_0
.end method
