.class public final Lcom/google/android/youtube/app/honeycomb/ui/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/remote/al;


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/google/android/youtube/core/client/bc;

.field private final c:Lcom/google/android/youtube/core/async/n;

.field private final d:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private final e:Landroid/view/ViewGroup;

.field private final f:Landroid/widget/ImageView;

.field private final g:Landroid/widget/TextView;

.field private final h:Landroid/widget/ImageView;

.field private final i:Landroid/widget/ImageView;

.field private final j:Landroid/widget/TextView;

.field private final k:Landroid/view/View;

.field private final l:Landroid/view/View;

.field private m:Lcom/google/android/youtube/app/remote/RemoteControl;

.field private n:Z

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private final q:Lcom/google/android/youtube/core/e;

.field private r:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/core/Analytics;Landroid/graphics/Typeface;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->r:Z

    const-string v0, "activity can not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->a:Landroid/app/Activity;

    const-string v0, "gDataClient can not be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/client/bc;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->b:Lcom/google/android/youtube/core/client/bc;

    const-string v0, "userAuthorizer can not be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/UserAuthorizer;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->d:Lcom/google/android/youtube/core/async/UserAuthorizer;

    const-string v0, "errorHelper can not be null"

    invoke-static {p6, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/e;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->q:Lcom/google/android/youtube/core/e;

    const v0, 0x7f07013d

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->e:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->e:Landroid/view/ViewGroup;

    const-string v1, "activity must contain a remote_control_bar layout"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->e:Landroid/view/ViewGroup;

    const v1, 0x7f070114

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->k:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->e:Landroid/view/ViewGroup;

    const v1, 0x7f070112

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->l:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->e:Landroid/view/ViewGroup;

    const v1, 0x7f07002c

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->g:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p8}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->e:Landroid/view/ViewGroup;

    const v1, 0x7f070035

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->f:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->e:Landroid/view/ViewGroup;

    const v1, 0x7f070113

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->j:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->j:Landroid/widget/TextView;

    invoke-virtual {v0, p8}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/ui/p;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p3, v1}, Lcom/google/android/youtube/app/honeycomb/ui/p;-><init>(Lcom/google/android/youtube/app/honeycomb/ui/j;Lcom/google/android/youtube/core/client/be;B)V

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->c:Lcom/google/android/youtube/core/async/n;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->e:Landroid/view/ViewGroup;

    const v1, 0x7f07010e

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->h:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->e:Landroid/view/ViewGroup;

    const v1, 0x7f070110

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->i:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->e:Landroid/view/ViewGroup;

    const v1, 0x7f070115

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/ui/k;

    invoke-direct {v1, p0, p5, p7}, Lcom/google/android/youtube/app/honeycomb/ui/k;-><init>(Lcom/google/android/youtube/app/honeycomb/ui/j;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/Analytics;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->h:Landroid/widget/ImageView;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/ui/l;

    invoke-direct {v1, p0, p7}, Lcom/google/android/youtube/app/honeycomb/ui/l;-><init>(Lcom/google/android/youtube/app/honeycomb/ui/j;Lcom/google/android/youtube/core/Analytics;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->i:Landroid/widget/ImageView;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/ui/m;

    invoke-direct {v1, p0, p7}, Lcom/google/android/youtube/app/honeycomb/ui/m;-><init>(Lcom/google/android/youtube/app/honeycomb/ui/j;Lcom/google/android/youtube/core/Analytics;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/ui/j;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->o:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/ui/j;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->n:Z

    return p1
.end method

.method static synthetic b(Lcom/google/android/youtube/app/honeycomb/ui/j;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->p:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/honeycomb/ui/j;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->n:Z

    return v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/honeycomb/ui/j;)Lcom/google/android/youtube/app/remote/RemoteControl;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->m:Lcom/google/android/youtube/app/remote/RemoteControl;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/app/honeycomb/ui/j;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->h:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/youtube/app/honeycomb/ui/j;)Lcom/google/android/youtube/core/async/n;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->c:Lcom/google/android/youtube/core/async/n;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/youtube/app/honeycomb/ui/j;)Lcom/google/android/youtube/core/client/bc;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->b:Lcom/google/android/youtube/core/client/bc;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/youtube/app/honeycomb/ui/j;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->a:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/youtube/app/honeycomb/ui/j;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->f:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/youtube/app/honeycomb/ui/j;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->g:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/youtube/app/honeycomb/ui/j;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->k:Landroid/view/View;

    return-object v0
.end method

.method static synthetic l(Lcom/google/android/youtube/app/honeycomb/ui/j;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->l:Landroid/view/View;

    return-object v0
.end method

.method static synthetic m(Lcom/google/android/youtube/app/honeycomb/ui/j;)Lcom/google/android/youtube/core/e;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->q:Lcom/google/android/youtube/core/e;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->m:Lcom/google/android/youtube/app/remote/RemoteControl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->m:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/app/remote/RemoteControl;->b(Lcom/google/android/youtube/app/remote/al;)V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->m:Lcom/google/android/youtube/app/remote/RemoteControl;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->e:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;Ljava/lang/String;)V
    .locals 5

    const v4, 0x7f0201c8

    const/4 v3, 0x0

    const/4 v2, 0x1

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/ui/n;->a:[I

    invoke-virtual {p1}, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iput-boolean v3, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->n:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->h:Landroid/widget/ImageView;

    const v1, 0x7f0201c7

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto :goto_0

    :pswitch_1
    iput-boolean v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->n:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto :goto_0

    :pswitch_2
    iput-boolean v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->n:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->h:Landroid/widget/ImageView;

    const v1, 0x7f0201c9

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Lcom/google/android/youtube/app/remote/RemoteControl$State;)V
    .locals 0

    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/remote/RemoteControl;)V
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->m:Lcom/google/android/youtube/app/remote/RemoteControl;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->m:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v1, p0}, Lcom/google/android/youtube/app/remote/RemoteControl;->b(Lcom/google/android/youtube/app/remote/al;)V

    :cond_0
    iget-boolean v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->r:Z

    if-nez v1, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->m:Lcom/google/android/youtube/app/remote/RemoteControl;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->m:Lcom/google/android/youtube/app/remote/RemoteControl;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->m:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v1, p0}, Lcom/google/android/youtube/app/remote/RemoteControl;->a(Lcom/google/android/youtube/app/remote/al;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->e:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->m:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/ui/j;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->i:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->m:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v2}, Lcom/google/android/youtube/app/remote/RemoteControl;->k()Z

    move-result v2

    if-eqz v2, :cond_3

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_3
    const/16 v0, 0x8

    goto :goto_1
.end method

.method public final a(Lcom/google/android/youtube/core/model/SubtitleTrack;)V
    .locals 0

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 6

    const/4 v1, 0x0

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->o:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->o:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->k:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->l:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->m:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->t()Lcom/google/android/youtube/app/remote/bb;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->j:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->a:Landroid/app/Activity;

    const v2, 0x7f0b0242

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->m:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v4}, Lcom/google/android/youtube/app/remote/RemoteControl;->t()Lcom/google/android/youtube/app/remote/bb;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/youtube/app/remote/bb;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->j:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->d:Lcom/google/android/youtube/core/async/UserAuthorizer;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/ui/o;

    invoke-direct {v1, p0, p1, v5}, Lcom/google/android/youtube/app/honeycomb/ui/o;-><init>(Lcom/google/android/youtube/app/honeycomb/ui/j;Ljava/lang/String;B)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bk;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->i:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->m:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->o()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->m:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->f()Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->m:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/app/honeycomb/ui/j;->a(Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/util/List;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->i:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->m:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->o()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    return-void
.end method

.method public final a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->r:Z

    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/ui/j;->p:Ljava/lang/String;

    return-void
.end method

.method public final n()V
    .locals 0

    return-void
.end method
