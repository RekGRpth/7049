.class final Lcom/google/android/youtube/app/honeycomb/ui/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/Analytics;

.field final synthetic b:Lcom/google/android/youtube/app/honeycomb/ui/j;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/honeycomb/ui/j;Lcom/google/android/youtube/core/Analytics;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/ui/l;->b:Lcom/google/android/youtube/app/honeycomb/ui/j;

    iput-object p2, p0, Lcom/google/android/youtube/app/honeycomb/ui/l;->a:Lcom/google/android/youtube/core/Analytics;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/l;->b:Lcom/google/android/youtube/app/honeycomb/ui/j;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/j;->c(Lcom/google/android/youtube/app/honeycomb/ui/j;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/l;->b:Lcom/google/android/youtube/app/honeycomb/ui/j;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/j;->d(Lcom/google/android/youtube/app/honeycomb/ui/j;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->c()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/l;->b:Lcom/google/android/youtube/app/honeycomb/ui/j;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/j;->e(Lcom/google/android/youtube/app/honeycomb/ui/j;)Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x7f0201c7

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/l;->b:Lcom/google/android/youtube/app/honeycomb/ui/j;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/honeycomb/ui/j;->a(Lcom/google/android/youtube/app/honeycomb/ui/j;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/l;->a:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "RemoteControlBarPlay"

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/l;->b:Lcom/google/android/youtube/app/honeycomb/ui/j;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/j;->d(Lcom/google/android/youtube/app/honeycomb/ui/j;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->d()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/l;->b:Lcom/google/android/youtube/app/honeycomb/ui/j;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/j;->e(Lcom/google/android/youtube/app/honeycomb/ui/j;)Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x7f0201c8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/l;->b:Lcom/google/android/youtube/app/honeycomb/ui/j;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/honeycomb/ui/j;->a(Lcom/google/android/youtube/app/honeycomb/ui/j;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/l;->a:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "RemoteControlBarPause"

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    goto :goto_0
.end method
