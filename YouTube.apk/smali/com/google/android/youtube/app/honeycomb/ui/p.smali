.class final Lcom/google/android/youtube/app/honeycomb/ui/p;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/honeycomb/ui/j;

.field private final b:Lcom/google/android/youtube/core/client/be;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/honeycomb/ui/j;Lcom/google/android/youtube/core/client/be;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->a:Lcom/google/android/youtube/app/honeycomb/ui/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->b:Lcom/google/android/youtube/core/client/be;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/app/honeycomb/ui/j;Lcom/google/android/youtube/core/client/be;B)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/app/honeycomb/ui/p;-><init>(Lcom/google/android/youtube/app/honeycomb/ui/j;Lcom/google/android/youtube/core/client/be;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 3

    const/16 v2, 0x8

    const-string v0, "Error downloading video info"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->a:Lcom/google/android/youtube/app/honeycomb/ui/j;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/j;->i(Lcom/google/android/youtube/app/honeycomb/ui/j;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->a:Lcom/google/android/youtube/app/honeycomb/ui/j;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/j;->j(Lcom/google/android/youtube/app/honeycomb/ui/j;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->a:Lcom/google/android/youtube/app/honeycomb/ui/j;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/ui/j;->m(Lcom/google/android/youtube/app/honeycomb/ui/j;)Lcom/google/android/youtube/core/e;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/android/youtube/core/e;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->a:Lcom/google/android/youtube/app/honeycomb/ui/j;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/j;->k(Lcom/google/android/youtube/app/honeycomb/ui/j;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->a:Lcom/google/android/youtube/app/honeycomb/ui/j;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/j;->l(Lcom/google/android/youtube/app/honeycomb/ui/j;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    check-cast p2, Lcom/google/android/youtube/core/model/Video;

    iget-object v0, p2, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->a:Lcom/google/android/youtube/app/honeycomb/ui/j;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/ui/j;->a(Lcom/google/android/youtube/app/honeycomb/ui/j;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/google/android/youtube/core/model/Video;->defaultThumbnailUri:Landroid/net/Uri;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->b:Lcom/google/android/youtube/core/client/be;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/Video;->defaultThumbnailUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->a:Lcom/google/android/youtube/app/honeycomb/ui/j;

    invoke-static {v2}, Lcom/google/android/youtube/app/honeycomb/ui/j;->h(Lcom/google/android/youtube/app/honeycomb/ui/j;)Landroid/app/Activity;

    move-result-object v2

    new-instance v3, Lcom/google/android/youtube/app/honeycomb/ui/q;

    invoke-direct {v3, p0, p2}, Lcom/google/android/youtube/app/honeycomb/ui/q;-><init>(Lcom/google/android/youtube/app/honeycomb/ui/p;Lcom/google/android/youtube/core/model/Video;)V

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/be;->a(Landroid/net/Uri;Lcom/google/android/youtube/core/async/n;)V

    goto :goto_0
.end method
