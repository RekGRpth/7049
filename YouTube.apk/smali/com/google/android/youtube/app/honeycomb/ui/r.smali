.class public final Lcom/google/android/youtube/app/honeycomb/ui/r;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/honeycomb/phone/bc;
.implements Lcom/google/android/youtube/app/remote/bd;


# instance fields
.field private final a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

.field private final b:Landroid/content/SharedPreferences;

.field private final c:Lcom/google/android/youtube/app/remote/br;

.field private final d:Landroid/graphics/Typeface;

.field private e:Lcom/google/android/youtube/app/ui/TutorialView;

.field private f:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Landroid/content/SharedPreferences;Landroid/graphics/Typeface;Lcom/google/android/youtube/app/remote/br;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "activity can not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/r;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    const-string v0, "preferences can not be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/r;->b:Landroid/content/SharedPreferences;

    const-string v0, "typeface can not be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Typeface;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/r;->d:Landroid/graphics/Typeface;

    const-string v0, "youTubeTvScreensMonitor can not be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/remote/br;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/r;->c:Lcom/google/android/youtube/app/remote/br;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/ui/r;)Landroid/content/SharedPreferences;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/r;->b:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method private b()V
    .locals 4

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/r;->b:Landroid/content/SharedPreferences;

    const-string v3, "show_dial_screen_tutorial"

    invoke-interface {v0, v3, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/r;->b:Landroid/content/SharedPreferences;

    const-string v3, "show_channel_store_turorial"

    invoke-interface {v0, v3, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/r;->c:Lcom/google/android/youtube/app/remote/br;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/br;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/remote/bp;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/bp;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/r;->f:Landroid/view/View;

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/r;->e:Lcom/google/android/youtube/app/ui/TutorialView;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/r;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/r;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v3, 0x7f04009f

    invoke-virtual {v1, v3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f07014b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/TutorialView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/r;->e:Lcom/google/android/youtube/app/ui/TutorialView;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/r;->e:Lcom/google/android/youtube/app/ui/TutorialView;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/r;->d:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/TutorialView;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/r;->e:Lcom/google/android/youtube/app/ui/TutorialView;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/ui/s;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/honeycomb/ui/s;-><init>(Lcom/google/android/youtube/app/honeycomb/ui/r;)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/TutorialView;->setDismissListener(Lcom/google/android/youtube/app/ui/cy;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/r;->e:Lcom/google/android/youtube/app/ui/TutorialView;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/r;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    const v3, 0x7f0b025f

    invoke-virtual {v1, v3}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/TutorialView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/r;->e:Lcom/google/android/youtube/app/ui/TutorialView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/TutorialView;->setAlignTextToCling(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/r;->e:Lcom/google/android/youtube/app/ui/TutorialView;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/TutorialView;->b()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/r;->e:Lcom/google/android/youtube/app/ui/TutorialView;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/r;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/ui/r;->f:Landroid/view/View;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/youtube/app/ui/TutorialView;->setTargetView(Landroid/view/View;Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/r;->e:Lcom/google/android/youtube/app/ui/TutorialView;

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/ui/TutorialView;->setVisibility(I)V

    :goto_2
    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/ui/r;->a()V

    goto :goto_2
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/r;->e:Lcom/google/android/youtube/app/ui/TutorialView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/r;->e:Lcom/google/android/youtube/app/ui/TutorialView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/TutorialView;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/app/honeycomb/ui/r;->b(Landroid/view/View;)V

    return-void
.end method

.method public final synthetic a(Lcom/google/android/youtube/app/remote/bb;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/ui/r;->b()V

    return-void
.end method

.method public final b(Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/ui/r;->f:Landroid/view/View;

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/ui/r;->b()V

    return-void
.end method

.method public final bridge synthetic b(Lcom/google/android/youtube/app/remote/bb;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/ui/r;->b()V

    return-void
.end method
