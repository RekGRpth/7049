.class public final Lcom/google/android/youtube/app/k;
.super Lcom/google/android/youtube/core/g;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/youtube/app/l;

.field public final b:Ljava/lang/String;

.field private final e:Z


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;Z)V
    .locals 6

    const/4 v4, 0x0

    const-string v0, "youtube"

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/core/g;-><init>(Landroid/content/ContentResolver;Ljava/lang/String;)V

    iput-boolean p2, p0, Lcom/google/android/youtube/app/k;->e:Z

    new-instance v0, Lcom/google/android/youtube/app/l;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/l;-><init>(Lcom/google/android/youtube/app/k;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/k;->a:Lcom/google/android/youtube/app/l;

    iget-object v0, p0, Lcom/google/android/youtube/app/k;->c:Landroid/content/ContentResolver;

    const-string v1, "content://com.google.settings/partner"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "value"

    aput-object v5, v2, v3

    const-string v3, "name=\'youtube_client_id\'"

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "value"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_1
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v4, "mvapp-android-google"

    :cond_2
    :goto_0
    iput-object v4, p0, Lcom/google/android/youtube/app/k;->b:Ljava/lang/String;

    return-void

    :cond_3
    const-string v0, "mvapp-android-"

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "mvapp-android-"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 2

    const-string v0, "min_app_version"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/app/k;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final b()I
    .locals 2

    const-string v0, "target_app_version"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/app/k;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final c()J
    .locals 3

    const-string v0, "time_between_upgrade_prompts_millis"

    const-wide/32 v1, 0x240c8400

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/youtube/app/k;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final d()Z
    .locals 2

    const-string v0, "enable_playlists_for_lw"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/app/k;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final e()Z
    .locals 2

    const-string v0, "enable_favorites_for_lw"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/app/k;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final f()Z
    .locals 2

    const-string v0, "enable_comments_for_lw"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/app/k;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final g()Z
    .locals 2

    const-string v0, "enable_uploads_for_lw"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/app/k;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Lcom/google/android/youtube/core/g;->h()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-super {p0}, Lcom/google/android/youtube/core/g;->h()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "UA-20803990-1"

    goto :goto_0
.end method

.method protected final i()I
    .locals 1

    const/16 v0, 0x64

    return v0
.end method

.method public final j()Z
    .locals 3

    const/4 v0, 0x0

    sget v1, Lcom/google/android/youtube/core/utils/Util;->a:I

    const/16 v2, 0xb

    if-ge v1, v2, :cond_0

    const-string v1, "enable_awful_player"

    invoke-virtual {p0, v1, v0}, Lcom/google/android/youtube/app/k;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final k()Z
    .locals 2

    const-string v0, "disconnect_at_highwatermark"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/app/k;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final l()Z
    .locals 4

    const/4 v1, 0x0

    const/4 v0, 0x1

    const-string v2, "enable_live_category_from_ics_mr0"

    invoke-virtual {p0, v2, v0}, Lcom/google/android/youtube/app/k;->a(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    sget v2, Lcom/google/android/youtube/core/utils/Util;->a:I

    const/16 v3, 0xe

    if-lt v2, v3, :cond_0

    move v2, v0

    :goto_0
    if-eqz v2, :cond_1

    :goto_1
    return v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final m()Z
    .locals 2

    const-string v0, "enable_prefetch"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/app/k;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final n()Z
    .locals 2

    const-string v0, "enable_dial_borsch"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/app/k;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final o()Z
    .locals 2

    const-string v0, "enable_remote_control_queue"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/app/k;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final p()Z
    .locals 2

    const-string v0, "enable_remote_control_bar"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/app/k;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final q()Z
    .locals 2

    const-string v0, "enable_mdx_logs"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/app/k;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final r()Z
    .locals 2

    const-string v0, "authenticate_all_requests"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/app/k;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final s()J
    .locals 3

    const-string v0, "prefetch_not_used_notification_frequency"

    const-wide/32 v1, 0x240c8400

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/youtube/app/k;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final t()I
    .locals 2

    const-string v0, "remote_volume_step_percent"

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/app/k;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method
