.class public final Lcom/google/android/youtube/app/player/a/i;
.super Lcom/google/android/youtube/core/player/l;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/youtube/app/player/a/e;

.field private final b:Lcom/google/android/youtube/app/player/a/j;

.field private c:Z


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/player/ad;Lcom/google/android/youtube/app/player/a/e;)V
    .locals 3

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/l;-><init>(Lcom/google/android/youtube/core/player/ad;)V

    const-string v0, "mediaServer cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/player/a/e;

    iput-object v0, p0, Lcom/google/android/youtube/app/player/a/i;->a:Lcom/google/android/youtube/app/player/a/e;

    new-instance v0, Lcom/google/android/youtube/app/player/a/j;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/youtube/app/player/a/j;-><init>(Lcom/google/android/youtube/app/player/a/i;Landroid/os/Looper;B)V

    iput-object v0, p0, Lcom/google/android/youtube/app/player/a/i;->b:Lcom/google/android/youtube/app/player/a/j;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/player/a/i;I)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/app/player/a/i;->e(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/player/a/i;II)Z
    .locals 2

    const/4 v0, 0x1

    const/16 v1, -0x3eb

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/app/player/a/i;->b(II)Z

    move-result v0

    return v0
.end method

.method private h()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a/i;->a:Lcom/google/android/youtube/app/player/a/e;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/player/a/e;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a/i;->b:Lcom/google/android/youtube/app/player/a/j;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/player/a/j;->sendEmptyMessage(I)Z

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "MediaServer is not started"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method private k()V
    .locals 4

    iget-boolean v0, p0, Lcom/google/android/youtube/app/player/a/i;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a/i;->b:Lcom/google/android/youtube/app/player/a/j;

    const/4 v1, 0x2

    const/16 v2, 0x64

    const/4 v3, -0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/youtube/app/player/a/j;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a/i;->h()V

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a/i;->a:Lcom/google/android/youtube/app/player/a/e;

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/app/player/a/e;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    invoke-super {p0, p1, v0}, Lcom/google/android/youtube/core/player/l;->a(Landroid/content/Context;Landroid/net/Uri;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/player/a/i;->c:Z

    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a/i;->k()V

    return-void
.end method

.method public final a(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a/i;->h()V

    iget-object v0, p0, Lcom/google/android/youtube/app/player/a/i;->a:Lcom/google/android/youtube/app/player/a/e;

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/app/player/a/e;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    invoke-super {p0, p1, v0, p3}, Lcom/google/android/youtube/core/player/l;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/player/a/i;->c:Z

    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a/i;->k()V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/player/ae;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/google/android/youtube/core/player/l;->a(Lcom/google/android/youtube/core/player/ae;)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a/i;->k()V

    return-void
.end method

.method public final b(I)V
    .locals 0

    return-void
.end method
