.class final Lcom/google/android/youtube/app/player/f;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/player/a;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/player/a;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/player/f;->a:Lcom/google/android/youtube/app/player/a;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    const/4 v4, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/app/player/f;->a:Lcom/google/android/youtube/app/player/a;

    invoke-static {v0}, Lcom/google/android/youtube/app/player/a;->c(Lcom/google/android/youtube/app/player/a;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "The file was deleted"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catch_0
    move-exception v0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/youtube/app/player/f;->a:Lcom/google/android/youtube/app/player/a;

    invoke-static {v0}, Lcom/google/android/youtube/app/player/a;->d(Lcom/google/android/youtube/app/player/a;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/player/f;->a:Lcom/google/android/youtube/app/player/a;

    invoke-static {v0}, Lcom/google/android/youtube/app/player/a;->f(Lcom/google/android/youtube/app/player/a;)Ljava/util/concurrent/atomic/AtomicLong;

    move-result-object v0

    const-wide/32 v1, 0x40000

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    const-string v0, "failed to read offsets, will retry"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lcom/google/android/youtube/app/player/f;->a:Lcom/google/android/youtube/app/player/a;

    invoke-static {v0}, Lcom/google/android/youtube/app/player/a;->i(Lcom/google/android/youtube/app/player/a;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    :goto_0
    return-void

    :cond_0
    :try_start_2
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/player/f;->a:Lcom/google/android/youtube/app/player/a;

    invoke-static {v0}, Lcom/google/android/youtube/app/player/a;->d(Lcom/google/android/youtube/app/player/a;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v0

    invoke-static {v1}, Lcom/google/android/youtube/app/player/mp4/e;->a(Ljava/io/InputStream;)Ljava/util/SortedMap;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    iget-object v0, p0, Lcom/google/android/youtube/app/player/f;->a:Lcom/google/android/youtube/app/player/a;

    invoke-static {v0}, Lcom/google/android/youtube/app/player/a;->f(Lcom/google/android/youtube/app/player/a;)Ljava/util/concurrent/atomic/AtomicLong;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/player/f;->a:Lcom/google/android/youtube/app/player/a;

    iget-object v2, p0, Lcom/google/android/youtube/app/player/f;->a:Lcom/google/android/youtube/app/player/a;

    invoke-static {v2}, Lcom/google/android/youtube/app/player/a;->e(Lcom/google/android/youtube/app/player/a;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/android/youtube/app/player/a;->a(Lcom/google/android/youtube/app/player/a;I)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    iget-object v0, p0, Lcom/google/android/youtube/app/player/f;->a:Lcom/google/android/youtube/app/player/a;

    invoke-static {v0}, Lcom/google/android/youtube/app/player/a;->g(Lcom/google/android/youtube/app/player/a;)J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/youtube/app/player/f;->a:Lcom/google/android/youtube/app/player/a;

    invoke-static {v2}, Lcom/google/android/youtube/app/player/a;->f(Lcom/google/android/youtube/app/player/a;)Ljava/util/concurrent/atomic/AtomicLong;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "loaded offsets, will prepare at offset "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/player/f;->a:Lcom/google/android/youtube/app/player/a;

    invoke-static {v1}, Lcom/google/android/youtube/app/player/a;->f(Lcom/google/android/youtube/app/player/a;)Ljava/util/concurrent/atomic/AtomicLong;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_1
    iget-object v0, p0, Lcom/google/android/youtube/app/player/f;->a:Lcom/google/android/youtube/app/player/a;

    invoke-static {v0}, Lcom/google/android/youtube/app/player/a;->i(Lcom/google/android/youtube/app/player/a;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_0

    :cond_1
    :try_start_3
    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/player/f;->a:Lcom/google/android/youtube/app/player/a;

    invoke-static {v0}, Lcom/google/android/youtube/app/player/a;->h(Lcom/google/android/youtube/app/player/a;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/youtube/app/player/f;->a:Lcom/google/android/youtube/app/player/a;

    invoke-static {v1}, Lcom/google/android/youtube/app/player/a;->i(Lcom/google/android/youtube/app/player/a;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    throw v0
.end method
