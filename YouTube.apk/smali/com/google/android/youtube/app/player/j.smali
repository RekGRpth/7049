.class public final Lcom/google/android/youtube/app/player/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/player/i;
.implements Lcom/google/android/youtube/core/player/ax;


# instance fields
.field private final a:Lcom/google/android/youtube/app/YouTubeApplication;

.field private final b:Lcom/google/android/youtube/core/utils/p;

.field private final c:I

.field private final d:I

.field private e:Z


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/YouTubeApplication;Lcom/google/android/youtube/app/k;Lcom/google/android/youtube/core/utils/p;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/app/player/j;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    const-string v0, "networkStatus cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/utils/p;

    iput-object v0, p0, Lcom/google/android/youtube/app/player/j;->b:Lcom/google/android/youtube/core/utils/p;

    sget v0, Lcom/google/android/youtube/core/utils/Util;->a:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    invoke-virtual {p2}, Lcom/google/android/youtube/app/k;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/youtube/app/player/j;->e:Z

    invoke-virtual {p2}, Lcom/google/android/youtube/app/k;->H()I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/player/j;->c:I

    invoke-virtual {p2}, Lcom/google/android/youtube/app/k;->I()I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/player/j;->d:I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/Stream;Z)Lcom/google/android/youtube/core/player/ad;
    .locals 6

    new-instance v1, Lcom/google/android/youtube/core/player/aa;

    invoke-direct {v1}, Lcom/google/android/youtube/core/player/aa;-><init>()V

    iget-boolean v0, p1, Lcom/google/android/youtube/core/model/Stream;->isOffline:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/player/j;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->J()Lcom/google/android/youtube/app/player/a/e;

    move-result-object v2

    if-eqz v2, :cond_0

    new-instance v0, Lcom/google/android/youtube/app/player/a/i;

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/app/player/a/i;-><init>(Lcom/google/android/youtube/core/player/ad;Lcom/google/android/youtube/app/player/a/e;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/InstantiationException;

    const-string v1, "Cannot create ProxyPlayer because MediaServer is null"

    invoke-direct {v0, v1}, Ljava/lang/InstantiationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/youtube/app/player/j;->e:Z

    if-eqz v0, :cond_2

    if-nez p2, :cond_2

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Stream;->uri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->b(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lcom/google/android/youtube/app/player/a;

    iget v2, p0, Lcom/google/android/youtube/app/player/j;->c:I

    iget v3, p0, Lcom/google/android/youtube/app/player/j;->d:I

    iget-object v4, p0, Lcom/google/android/youtube/app/player/j;->b:Lcom/google/android/youtube/core/utils/p;

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/player/a;-><init>(Lcom/google/android/youtube/core/player/ad;IILcom/google/android/youtube/core/utils/p;Lcom/google/android/youtube/app/player/i;)V

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public final a()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/player/j;->e:Z

    return-void
.end method
