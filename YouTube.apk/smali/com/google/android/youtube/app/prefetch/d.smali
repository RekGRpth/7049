.class public final Lcom/google/android/youtube/app/prefetch/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/transfer/i;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/content/SharedPreferences;

.field private final c:J

.field private final d:Z

.field private final e:J

.field private final f:Ljava/util/Set;

.field private g:Lcom/google/android/youtube/core/utils/aa;

.field private h:Lcom/google/android/youtube/core/transfer/l;

.field private volatile i:Ljava/util/Map;

.field private final j:Landroid/app/NotificationManager;

.field private k:Z

.field private l:Lcom/google/android/youtube/app/prefetch/g;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/utils/e;Lcom/google/android/youtube/app/k;)V
    .locals 5

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "context can\'t be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/youtube/app/prefetch/d;->a:Landroid/content/Context;

    const-string v0, "preferences can\'t be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/google/android/youtube/app/prefetch/d;->b:Landroid/content/SharedPreferences;

    const-string v0, "youtubeConfig can\'t be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p4}, Lcom/google/android/youtube/app/k;->s()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/youtube/app/prefetch/d;->e:J

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/prefetch/d;->f:Ljava/util/Set;

    const-string v0, "prefetch_session_id"

    invoke-interface {p2, v0, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    invoke-interface {p3}, Lcom/google/android/youtube/core/utils/e;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/youtube/app/prefetch/d;->c:J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/prefetch/d;->d:Z

    invoke-static {p2}, Lcom/google/android/youtube/app/compat/ac;->a(Landroid/content/SharedPreferences;)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v0

    const-string v1, "prefetch_session_id"

    iget-wide v2, p0, Lcom/google/android/youtube/app/prefetch/d;->c:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/youtube/app/compat/ad;->a(Ljava/lang/String;J)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/compat/ad;->a()V

    :goto_0
    const-string v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/google/android/youtube/app/prefetch/d;->j:Landroid/app/NotificationManager;

    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/youtube/app/prefetch/d;->g()V

    :goto_1
    new-instance v0, Lcom/google/android/youtube/app/prefetch/g;

    invoke-direct {v0, p0, v4}, Lcom/google/android/youtube/app/prefetch/g;-><init>(Lcom/google/android/youtube/app/prefetch/d;B)V

    iput-object v0, p0, Lcom/google/android/youtube/app/prefetch/d;->l:Lcom/google/android/youtube/app/prefetch/g;

    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/d;->l:Lcom/google/android/youtube/app/prefetch/g;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/prefetch/g;->a()V

    return-void

    :cond_0
    iput-wide v0, p0, Lcom/google/android/youtube/app/prefetch/d;->c:J

    iput-boolean v4, p0, Lcom/google/android/youtube/app/prefetch/d;->d:Z

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/youtube/app/prefetch/d;->h()V

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/youtube/app/prefetch/d;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/d;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/prefetch/d;Ljava/io/File;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/prefetch/d;->a(Ljava/io/File;)V

    return-void
.end method

.method private a(Ljava/io/File;)V
    .locals 6

    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    iget-wide v4, p0, Lcom/google/android/youtube/app/prefetch/d;->c:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-direct {p0, v3}, Lcom/google/android/youtube/app/prefetch/d;->a(Ljava/io/File;)V

    :cond_0
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method static synthetic b(Lcom/google/android/youtube/app/prefetch/d;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/prefetch/d;->g()V

    return-void
.end method

.method static synthetic c(Lcom/google/android/youtube/app/prefetch/d;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/prefetch/d;->h()V

    return-void
.end method

.method private e()V
    .locals 8

    const-wide/16 v6, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/d;->g:Lcom/google/android/youtube/core/utils/aa;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/aa;->a()Landroid/os/Binder;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/transfer/l;

    iput-object v0, p0, Lcom/google/android/youtube/app/prefetch/d;->h:Lcom/google/android/youtube/core/transfer/l;

    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/d;->h:Lcom/google/android/youtube/core/transfer/l;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/transfer/l;->a()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/prefetch/d;->i:Ljava/util/Map;

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/d;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/transfer/Transfer;

    iget-object v0, v0, Lcom/google/android/youtube/core/transfer/Transfer;->c:Lcom/google/android/youtube/core/transfer/Transfer$Status;

    sget-object v3, Lcom/google/android/youtube/core/transfer/Transfer$Status;->RUNNING:Lcom/google/android/youtube/core/transfer/Transfer$Status;

    if-ne v0, v3, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/youtube/app/prefetch/d;->k:Z

    iget-boolean v0, p0, Lcom/google/android/youtube/app/prefetch/d;->k:Z

    if-eqz v0, :cond_1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/youtube/app/prefetch/d;->b:Landroid/content/SharedPreferences;

    const-string v3, "prefetch_last_not_used_notification_time"

    invoke-interface {v2, v3, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    cmp-long v4, v2, v6

    if-nez v4, :cond_2

    iget-object v2, p0, Lcom/google/android/youtube/app/prefetch/d;->b:Landroid/content/SharedPreferences;

    invoke-static {v2}, Lcom/google/android/youtube/app/compat/ac;->a(Landroid/content/SharedPreferences;)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v2

    const-string v3, "prefetch_last_not_used_notification_time"

    invoke-virtual {v2, v3, v0, v1}, Lcom/google/android/youtube/app/compat/ad;->a(Ljava/lang/String;J)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/compat/ad;->a()V

    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/d;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/prefetch/f;

    invoke-interface {v0}, Lcom/google/android/youtube/app/prefetch/f;->b()V

    goto :goto_2

    :cond_2
    sub-long v2, v0, v2

    iget-wide v4, p0, Lcom/google/android/youtube/app/prefetch/d;->e:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/youtube/app/prefetch/d;->b:Landroid/content/SharedPreferences;

    const-string v3, "prefetch_last_local_playback_time"

    invoke-interface {v2, v3, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    sub-long/2addr v0, v2

    iget-wide v2, p0, Lcom/google/android/youtube/app/prefetch/d;->e:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/youtube/app/prefetch/d;->f()V

    goto :goto_1

    :cond_3
    return-void

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method private f()V
    .locals 7

    const/4 v6, 0x0

    new-instance v1, Landroid/app/Notification;

    const v0, 0x7f020173

    iget-object v2, p0, Lcom/google/android/youtube/app/prefetch/d;->a:Landroid/content/Context;

    const v3, 0x7f0b0153

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-direct {v1, v0, v2, v3, v4}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    iget v0, v1, Landroid/app/Notification;->flags:I

    or-int/lit8 v0, v0, 0x10

    iput v0, v1, Landroid/app/Notification;->flags:I

    new-instance v0, Landroid/widget/RemoteViews;

    iget-object v2, p0, Lcom/google/android/youtube/app/prefetch/d;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f040070

    invoke-direct {v0, v2, v3}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    iput-object v0, v1, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    :try_start_0
    const-string v0, "com.google.android.youtube.app.honeycomb.SettingsActivity$PrefetchPrefsFragment"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-string v2, "createIntent"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Landroid/content/Context;

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/youtube/app/prefetch/d;->a:Landroid/content/Context;

    aput-object v5, v3, v4

    invoke-virtual {v2, v0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v2, p0, Lcom/google/android/youtube/app/prefetch/d;->a:Landroid/content/Context;

    invoke-static {v2, v6, v0, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, v1, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/d;->j:Landroid/app/NotificationManager;

    const/16 v2, 0x3e9

    invoke-virtual {v0, v2, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/d;->b:Landroid/content/SharedPreferences;

    invoke-static {v0}, Lcom/google/android/youtube/app/compat/ac;->a(Landroid/content/SharedPreferences;)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v0

    const-string v1, "prefetch_last_not_used_notification_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/youtube/app/compat/ad;->a(Ljava/lang/String;J)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/compat/ad;->a()V

    return-void

    :catch_0
    move-exception v0

    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.MAIN"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/youtube/app/prefetch/d;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method private g()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/d;->b:Landroid/content/SharedPreferences;

    invoke-static {v0}, Lcom/google/android/youtube/app/compat/ac;->a(Landroid/content/SharedPreferences;)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v0

    const-string v1, "download_max_rate"

    const/high16 v2, 0x20000

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/compat/ad;->a(Ljava/lang/String;I)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/compat/ad;->a()V

    return-void
.end method

.method private h()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/d;->b:Landroid/content/SharedPreferences;

    invoke-static {v0}, Lcom/google/android/youtube/app/compat/ac;->a(Landroid/content/SharedPreferences;)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v0

    const-string v1, "download_max_rate"

    const/high16 v2, 0x40000

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/compat/ad;->a(Ljava/lang/String;I)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/compat/ad;->a()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/android/youtube/core/model/ak;
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/d;->i:Ljava/util/Map;

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/app/prefetch/d;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/youtube/app/prefetch/d;->i:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/transfer/Transfer;

    if-eqz v0, :cond_1

    iget-object v2, v0, Lcom/google/android/youtube/core/transfer/Transfer;->c:Lcom/google/android/youtube/core/transfer/Transfer$Status;

    sget-object v3, Lcom/google/android/youtube/core/transfer/Transfer$Status;->COMPLETED:Lcom/google/android/youtube/core/transfer/Transfer$Status;

    if-ne v2, v3, :cond_1

    iget-object v2, v0, Lcom/google/android/youtube/core/transfer/Transfer;->a:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/youtube/app/prefetch/Prefetch;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    move-object v0, v1

    goto :goto_0

    :cond_2
    new-instance v1, Ljava/io/File;

    iget-object v2, v0, Lcom/google/android/youtube/core/transfer/Transfer;->a:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/core/model/Stream$Builder;

    invoke-direct {v2}, Lcom/google/android/youtube/core/model/Stream$Builder;-><init>()V

    invoke-virtual {v2, v1}, Lcom/google/android/youtube/core/model/Stream$Builder;->uri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Stream$Builder;

    move-result-object v1

    iget-wide v2, v0, Lcom/google/android/youtube/core/transfer/Transfer;->f:J

    invoke-virtual {v1, v2, v3}, Lcom/google/android/youtube/core/model/Stream$Builder;->sizeInBytes(J)Lcom/google/android/youtube/core/model/Stream$Builder;

    move-result-object v0

    const-string v1, "unknown"

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/Stream$Builder;->mimeType(Ljava/lang/String;)Lcom/google/android/youtube/core/model/Stream$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/core/model/Stream$Quality;->UNKNOWN:Lcom/google/android/youtube/core/model/Stream$Quality;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/Stream$Builder;->quality(Lcom/google/android/youtube/core/model/Stream$Quality;)Lcom/google/android/youtube/core/model/Stream$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/Stream$Builder;->build()Lcom/google/android/youtube/core/model/Stream;

    move-result-object v1

    new-instance v0, Lcom/google/android/youtube/core/model/ak;

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/model/ak;-><init>(Lcom/google/android/youtube/core/model/Stream;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/youtube/app/prefetch/f;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/d;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/d;->i:Ljava/util/Map;

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/google/android/youtube/app/prefetch/f;->b()V

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/transfer/Transfer;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/prefetch/d;->e()V

    return-void
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/youtube/core/transfer/Transfer$Status;
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/youtube/app/prefetch/d;->i:Ljava/util/Map;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/app/prefetch/d;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/prefetch/d;->i:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v1}, Lcom/google/android/youtube/app/prefetch/Prefetch;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/d;->i:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/transfer/Transfer;

    iget-object v0, v0, Lcom/google/android/youtube/core/transfer/Transfer;->c:Lcom/google/android/youtube/core/transfer/Transfer$Status;

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/d;->g:Lcom/google/android/youtube/core/utils/aa;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/d;->a:Landroid/content/Context;

    invoke-static {v0, p0}, Lcom/google/android/youtube/core/transfer/DownloadService;->a(Landroid/content/Context;Lcom/google/android/youtube/core/transfer/i;)Lcom/google/android/youtube/core/utils/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/prefetch/d;->g:Lcom/google/android/youtube/core/utils/aa;

    :cond_0
    return-void
.end method

.method public final b(Lcom/google/android/youtube/app/prefetch/f;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/d;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public final b(Lcom/google/android/youtube/core/transfer/Transfer;)V
    .locals 0

    return-void
.end method

.method public final c(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/d;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/youtube/app/prefetch/PrefetchService;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/youtube/app/prefetch/d;->c:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final c(Lcom/google/android/youtube/core/transfer/Transfer;)V
    .locals 0

    return-void
.end method

.method public final c()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/prefetch/d;->k:Z

    return v0
.end method

.method public final d()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/d;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/youtube/app/prefetch/PrefetchService;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v1, p0, Lcom/google/android/youtube/app/prefetch/d;->d:Z

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    new-instance v1, Lcom/google/android/youtube/app/prefetch/e;

    invoke-direct {v1, p0, v0}, Lcom/google/android/youtube/app/prefetch/e;-><init>(Lcom/google/android/youtube/app/prefetch/d;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/youtube/app/prefetch/e;->start()V

    goto :goto_0
.end method

.method public final d(Lcom/google/android/youtube/core/transfer/Transfer;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/prefetch/d;->e()V

    return-void
.end method

.method public final e(Lcom/google/android/youtube/core/transfer/Transfer;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/prefetch/d;->e()V

    return-void
.end method

.method public final t_()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/prefetch/d;->e()V

    return-void
.end method
