.class public final Lcom/google/android/youtube/app/prefetch/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/player/ak;


# instance fields
.field private final a:Lcom/google/android/youtube/core/player/ak;

.field private final b:Lcom/google/android/youtube/app/prefetch/d;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/prefetch/d;Lcom/google/android/youtube/core/player/ak;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "target cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/ak;

    iput-object v0, p0, Lcom/google/android/youtube/app/prefetch/j;->a:Lcom/google/android/youtube/core/player/ak;

    const-string v0, "prefetchStore cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/prefetch/d;

    iput-object v0, p0, Lcom/google/android/youtube/app/prefetch/j;->b:Lcom/google/android/youtube/app/prefetch/d;

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Set;Z)Lcom/google/android/youtube/core/model/Stream$Quality;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/j;->a:Lcom/google/android/youtube/core/player/ak;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/player/ak;->a(Ljava/util/Set;Z)Lcom/google/android/youtube/core/model/Stream$Quality;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/youtube/core/model/Video;Ljava/util/Set;Lcom/google/android/youtube/core/player/al;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/j;->b:Lcom/google/android/youtube/app/prefetch/d;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/prefetch/d;->a(Ljava/lang/String;)Lcom/google/android/youtube/core/model/ak;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p3, v0}, Lcom/google/android/youtube/core/player/al;->a(Lcom/google/android/youtube/core/model/ak;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/j;->a:Lcom/google/android/youtube/core/player/ak;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/youtube/core/player/ak;->a(Lcom/google/android/youtube/core/model/Video;Ljava/util/Set;Lcom/google/android/youtube/core/player/al;)V

    goto :goto_0
.end method

.method public final a(Ljava/util/Collection;Ljava/util/Set;Lcom/google/android/youtube/core/player/al;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/j;->a:Lcom/google/android/youtube/core/player/ak;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/youtube/core/player/ak;->a(Ljava/util/Collection;Ljava/util/Set;Lcom/google/android/youtube/core/player/al;)V

    return-void
.end method

.method public final a()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/j;->a:Lcom/google/android/youtube/core/player/ak;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ak;->a()Z

    move-result v0

    return v0
.end method
