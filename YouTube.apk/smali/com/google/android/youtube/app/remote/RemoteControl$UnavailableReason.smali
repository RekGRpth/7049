.class public final enum Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

.field public static final enum LIMITED_SYNDICATION:Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

.field public static final enum LIVE_NOT_SUPPORTED:Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

.field public static final enum PRIVATE:Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

.field public static final enum REGION:Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

.field public static final enum UNSPECIFIED:Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;


# instance fields
.field private final canRetry:Z

.field private final stringId:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

    const-string v1, "REGION"

    const v2, 0x7f0b0255

    invoke-direct {v0, v1, v3, v2, v3}, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;->REGION:Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

    new-instance v0, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

    const-string v1, "PRIVATE"

    const v2, 0x7f0b0256

    invoke-direct {v0, v1, v4, v2, v4}, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;->PRIVATE:Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

    new-instance v0, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

    const-string v1, "LIMITED_SYNDICATION"

    const v2, 0x7f0b0257

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;->LIMITED_SYNDICATION:Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

    new-instance v0, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

    const-string v1, "UNSPECIFIED"

    const v2, 0x7f0b0253

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;->UNSPECIFIED:Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

    new-instance v0, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

    const-string v1, "LIVE_NOT_SUPPORTED"

    const v2, 0x7f0b0253

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;->LIVE_NOT_SUPPORTED:Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;->REGION:Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;->PRIVATE:Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;->LIMITED_SYNDICATION:Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;->UNSPECIFIED:Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;->LIVE_NOT_SUPPORTED:Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

    aput-object v1, v0, v7

    sput-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;->$VALUES:[Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIZ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;->stringId:I

    iput-boolean p4, p0, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;->canRetry:Z

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;
    .locals 1

    const-class v0, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

    return-object v0
.end method

.method public static values()[Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;->$VALUES:[Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

    invoke-virtual {v0}, [Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

    return-object v0
.end method


# virtual methods
.method public final canRetry()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;->canRetry:Z

    return v0
.end method

.method public final getStringId()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;->stringId:I

    return v0
.end method
