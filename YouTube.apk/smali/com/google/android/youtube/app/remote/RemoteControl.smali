.class public interface abstract Lcom/google/android/youtube/app/remote/RemoteControl;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract a(I)V
.end method

.method public abstract a(Lcom/google/android/youtube/app/remote/al;)V
.end method

.method public abstract a(Lcom/google/android/youtube/app/remote/ap;)V
.end method

.method public abstract a(Lcom/google/android/youtube/app/remote/bb;Ljava/lang/String;J)V
.end method

.method public abstract a(Lcom/google/android/youtube/core/model/SubtitleTrack;)V
.end method

.method public abstract a(Ljava/lang/String;)V
.end method

.method public abstract a(Ljava/util/List;)V
.end method

.method public abstract a(Z)V
.end method

.method public abstract a(Lcom/google/android/youtube/core/model/Video;)Z
.end method

.method public abstract b(Lcom/google/android/youtube/core/model/Video;)Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;
.end method

.method public abstract b(I)V
.end method

.method public abstract b(Lcom/google/android/youtube/app/remote/al;)V
.end method

.method public abstract b(Lcom/google/android/youtube/app/remote/ap;)V
.end method

.method public abstract b(Ljava/lang/String;)V
.end method

.method public abstract c()V
.end method

.method public abstract c(Lcom/google/android/youtube/app/remote/al;)V
.end method

.method public abstract c(Ljava/lang/String;)V
.end method

.method public abstract d()V
.end method

.method public abstract e()V
.end method

.method public abstract f()Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;
.end method

.method public abstract g()Ljava/lang/String;
.end method

.method public abstract h()I
.end method

.method public abstract i()D
.end method

.method public abstract j()Ljava/util/List;
.end method

.method public abstract k()Z
.end method

.method public abstract l()Lcom/google/android/youtube/app/remote/am;
.end method

.method public abstract m()I
.end method

.method public abstract n()Z
.end method

.method public abstract o()Z
.end method

.method public abstract p()V
.end method

.method public abstract q()Z
.end method

.method public abstract r()V
.end method

.method public abstract s()Lcom/google/android/youtube/core/model/SubtitleTrack;
.end method

.method public abstract t()Lcom/google/android/youtube/app/remote/bb;
.end method

.method public abstract v()Lcom/google/android/youtube/app/remote/RemoteControl$State;
.end method

.method public abstract w()Lcom/google/android/youtube/app/remote/an;
.end method
