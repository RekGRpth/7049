.class final Lcom/google/android/youtube/app/remote/ac;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/ytremote/model/CloudScreen;

.field final synthetic b:Lcom/google/android/youtube/app/remote/v;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/remote/v;Lcom/google/android/ytremote/model/CloudScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/remote/ac;->b:Lcom/google/android/youtube/app/remote/v;

    iput-object p2, p0, Lcom/google/android/youtube/app/remote/ac;->a:Lcom/google/android/ytremote/model/CloudScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    const/4 v3, 0x5

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ac;->b:Lcom/google/android/youtube/app/remote/v;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/v;->h(Lcom/google/android/youtube/app/remote/v;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ac;->a:Lcom/google/android/ytremote/model/CloudScreen;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/ac;->b:Lcom/google/android/youtube/app/remote/v;

    invoke-virtual {v0}, Lcom/google/android/ytremote/model/CloudScreen;->getScreenId()Lcom/google/android/ytremote/model/ScreenId;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/youtube/app/remote/v;->a(Lcom/google/android/youtube/app/remote/v;Lcom/google/android/ytremote/model/ScreenId;)Lcom/google/android/ytremote/model/CloudScreen;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/ac;->b:Lcom/google/android/youtube/app/remote/v;

    invoke-static {v1}, Lcom/google/android/youtube/app/remote/v;->c(Lcom/google/android/youtube/app/remote/v;)Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ac;->b:Lcom/google/android/youtube/app/remote/v;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/v;->c(Lcom/google/android/youtube/app/remote/v;)Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/ac;->a:Lcom/google/android/ytremote/model/CloudScreen;

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ac;->b:Lcom/google/android/youtube/app/remote/v;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/v;->c(Lcom/google/android/youtube/app/remote/v;)Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v0

    if-le v0, v3, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ac;->b:Lcom/google/android/youtube/app/remote/v;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/v;->c(Lcom/google/android/youtube/app/remote/v;)Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(I)Ljava/lang/Object;

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ac;->b:Lcom/google/android/youtube/app/remote/v;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/v;->i(Lcom/google/android/youtube/app/remote/v;)V

    return-void
.end method
