.class public final Lcom/google/android/youtube/app/remote/ad;
.super Lcom/android/athome/picker/media/c;
.source "SourceFile"


# instance fields
.field public a:Z

.field private final c:Ljava/lang/Object;

.field private final d:Ljava/util/List;

.field private e:I

.field private final f:Lcom/google/android/youtube/app/remote/e;

.field private final g:Lcom/google/android/youtube/app/remote/AtHomeConnection;

.field private final h:Lcom/google/android/youtube/app/remote/c;

.field private final i:Lcom/google/android/youtube/app/remote/bf;

.field private final j:Lcom/google/android/youtube/app/remote/br;

.field private final k:Lcom/google/android/youtube/app/remote/aj;

.field private final l:Ljava/util/Map;

.field private final m:Ljava/lang/Object;

.field private final n:Lcom/google/android/youtube/app/remote/ai;

.field private final o:Lcom/google/android/youtube/app/remote/ap;

.field private final p:Lcom/google/android/youtube/core/Analytics;

.field private final q:Landroid/os/Handler;

.field private r:Lcom/google/android/youtube/app/remote/RemoteControl;

.field private s:Ljava/lang/Object;

.field private t:Lcom/google/android/youtube/app/remote/ah;

.field private final u:Ljava/util/Set;

.field private v:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/app/remote/AtHomeConnection;Lcom/google/android/youtube/app/remote/e;Lcom/google/android/youtube/app/remote/br;Lcom/google/android/youtube/app/remote/bf;Lcom/google/android/youtube/core/Analytics;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/android/athome/picker/media/c;-><init>()V

    iput v2, p0, Lcom/google/android/youtube/app/remote/ad;->e:I

    const-string v0, "context cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "atHomeConnection cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/remote/AtHomeConnection;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->g:Lcom/google/android/youtube/app/remote/AtHomeConnection;

    const-string v0, "atHomeRemoteControl cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/remote/e;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->f:Lcom/google/android/youtube/app/remote/e;

    const-string v0, "youTubeTvRemoteControl cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/remote/bf;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->i:Lcom/google/android/youtube/app/remote/bf;

    const-string v0, "youTubeTvScreensMonitor cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/remote/br;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->j:Lcom/google/android/youtube/app/remote/br;

    const-string v0, "analytics cannot be null"

    invoke-static {p6, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/Analytics;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->p:Lcom/google/android/youtube/core/Analytics;

    invoke-static {p1}, Lcom/android/athome/picker/media/k;->a(Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->c:Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->c:Ljava/lang/Object;

    const v1, 0x800001

    invoke-static {v0, v1, p0}, Lcom/android/athome/picker/media/k;->a(Ljava/lang/Object;ILcom/android/athome/picker/media/b;)V

    new-instance v0, Lcom/google/android/youtube/app/remote/af;

    invoke-direct {v0, p0, v2}, Lcom/google/android/youtube/app/remote/af;-><init>(Lcom/google/android/youtube/app/remote/ad;B)V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->h:Lcom/google/android/youtube/app/remote/c;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->d:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->c:Ljava/lang/Object;

    const v1, 0x7f0b025e

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lcom/android/athome/picker/media/k;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Z)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->m:Ljava/lang/Object;

    new-instance v0, Lcom/google/android/youtube/app/remote/aj;

    invoke-direct {v0, p0, v2}, Lcom/google/android/youtube/app/remote/aj;-><init>(Lcom/google/android/youtube/app/remote/ad;B)V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->k:Lcom/google/android/youtube/app/remote/aj;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->l:Ljava/util/Map;

    new-instance v0, Lcom/google/android/youtube/app/remote/ai;

    invoke-direct {v0, p0, v2}, Lcom/google/android/youtube/app/remote/ai;-><init>(Lcom/google/android/youtube/app/remote/ad;B)V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->n:Lcom/google/android/youtube/app/remote/ai;

    new-instance v0, Lcom/google/android/youtube/app/remote/ak;

    invoke-direct {v0, p0, v2}, Lcom/google/android/youtube/app/remote/ak;-><init>(Lcom/google/android/youtube/app/remote/ad;B)V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->o:Lcom/google/android/youtube/app/remote/ap;

    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/app/remote/ae;

    invoke-direct {v1, p0, v2}, Lcom/google/android/youtube/app/remote/ae;-><init>(Lcom/google/android/youtube/app/remote/ad;B)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->q:Landroid/os/Handler;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->u:Ljava/util/Set;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/ad;Ljava/lang/Object;)Lcom/google/android/youtube/app/remote/bp;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/remote/ad;->d(Ljava/lang/Object;)Lcom/google/android/youtube/app/remote/bp;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/ad;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->l:Ljava/util/Map;

    return-object v0
.end method

.method private a(Lcom/google/android/youtube/app/remote/RemoteControl;Lcom/google/android/youtube/app/remote/bb;)V
    .locals 5

    const-string v1, ""

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/ad;->t:Lcom/google/android/youtube/app/remote/ah;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/ad;->t:Lcom/google/android/youtube/app/remote/ah;

    invoke-interface {v2}, Lcom/google/android/youtube/app/remote/ah;->y()Lcom/google/android/youtube/core/model/Video;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v3, v2, Lcom/google/android/youtube/core/model/Video;->state:Lcom/google/android/youtube/core/model/Video$State;

    sget-object v4, Lcom/google/android/youtube/core/model/Video$State;->NOT_AVAILABLE_ON_MOBILE:Lcom/google/android/youtube/core/model/Video$State;

    if-eq v3, v4, :cond_0

    iget-object v1, v2, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->t:Lcom/google/android/youtube/app/remote/ah;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/ah;->z()I

    move-result v0

    :cond_0
    int-to-long v2, v0

    invoke-interface {p1, p2, v1, v2, v3}, Lcom/google/android/youtube/app/remote/RemoteControl;->a(Lcom/google/android/youtube/app/remote/bb;Ljava/lang/String;J)V

    iput-object p1, p0, Lcom/google/android/youtube/app/remote/ad;->r:Lcom/google/android/youtube/app/remote/RemoteControl;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/ad;Lcom/google/android/youtube/app/remote/bp;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/remote/ad;->b(Lcom/google/android/youtube/app/remote/bp;)V

    return-void
.end method

.method private declared-synchronized a(Z)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/remote/ag;

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/ad;->r:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0, v2, p1}, Lcom/google/android/youtube/app/remote/ag;->a(Lcom/google/android/youtube/app/remote/RemoteControl;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    monitor-exit p0

    return-void
.end method

.method static synthetic b(Lcom/google/android/youtube/app/remote/ad;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->s:Ljava/lang/Object;

    return-object v0
.end method

.method private b(Lcom/google/android/youtube/app/remote/bp;)V
    .locals 3

    const/4 v2, 0x1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "YouTubeTvScreen added: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->l:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Already know about the screen - ignoring"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->c:Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/ad;->m:Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/android/athome/picker/media/k;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/youtube/app/remote/bp;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/athome/picker/media/r;->a(Ljava/lang/Object;Ljava/lang/CharSequence;)V

    invoke-static {v0, v2}, Lcom/android/athome/picker/media/r;->e(Ljava/lang/Object;I)V

    invoke-static {v0, v2}, Lcom/android/athome/picker/media/r;->f(Ljava/lang/Object;I)V

    const/16 v1, 0x64

    invoke-static {v0, v1}, Lcom/android/athome/picker/media/r;->b(Ljava/lang/Object;I)V

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/ad;->n:Lcom/google/android/youtube/app/remote/ai;

    invoke-static {v0, v1}, Lcom/android/athome/picker/media/r;->a(Ljava/lang/Object;Lcom/android/athome/picker/media/d;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/ad;->c:Ljava/lang/Object;

    invoke-static {v1, v0}, Lcom/android/athome/picker/media/k;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/ad;->l:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/remote/ad;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->c:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/remote/ad;)Lcom/google/android/youtube/app/remote/RemoteControl;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->r:Lcom/google/android/youtube/app/remote/RemoteControl;

    return-object v0
.end method

.method private d(Ljava/lang/Object;)Lcom/google/android/youtube/app/remote/bp;
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->l:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/remote/bp;

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic e(Lcom/google/android/youtube/app/remote/ad;)Lcom/google/android/youtube/app/remote/bf;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->i:Lcom/google/android/youtube/app/remote/bf;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/youtube/app/remote/ad;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->q:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/youtube/app/remote/ad;)Lcom/google/android/youtube/core/Analytics;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->p:Lcom/google/android/youtube/core/Analytics;

    return-object v0
.end method

.method private h()V
    .locals 5

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/ad;->c:Ljava/lang/Object;

    invoke-static {v1}, Lcom/android/athome/picker/media/k;->a(Ljava/lang/Object;)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/ad;->u:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    iput v0, p0, Lcom/google/android/youtube/app/remote/ad;->v:I

    :goto_0
    if-ge v0, v1, :cond_2

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/ad;->c:Ljava/lang/Object;

    invoke-static {v2, v0}, Lcom/android/athome/picker/media/k;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lcom/android/athome/picker/media/q;->d(Ljava/lang/Object;)I

    move-result v3

    const v4, 0x800001

    and-int/2addr v4, v3

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/youtube/app/remote/ad;->u:Ljava/util/Set;

    invoke-static {v2}, Lcom/android/athome/picker/media/q;->f(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v4, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    and-int/lit8 v2, v3, 0x1

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/google/android/youtube/app/remote/ad;->v:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/android/youtube/app/remote/ad;->v:I

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/Object;)V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onRouteUnselected: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    const/high16 v0, 0x800000

    and-int/2addr v0, p1

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->r:Lcom/google/android/youtube/app/remote/RemoteControl;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->r:Lcom/google/android/youtube/app/remote/RemoteControl;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/ad;->f:Lcom/google/android/youtube/app/remote/e;

    if-ne v0, v1, :cond_3

    iget v0, p0, Lcom/google/android/youtube/app/remote/ad;->e:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->r:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->v()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->r:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->e()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->p:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "AtHomeDisconnect"

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/app/remote/ad;->r:Lcom/google/android/youtube/app/remote/RemoteControl;

    iget-boolean v0, p0, Lcom/google/android/youtube/app/remote/ad;->a:Z

    if-nez v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    invoke-interface {v1, v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->a(Z)V

    iput-object v2, p0, Lcom/google/android/youtube/app/remote/ad;->r:Lcom/google/android/youtube/app/remote/RemoteControl;

    :cond_2
    iput-object v2, p0, Lcom/google/android/youtube/app/remote/ad;->s:Ljava/lang/Object;

    return-void

    :cond_3
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->r:Lcom/google/android/youtube/app/remote/RemoteControl;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/ad;->i:Lcom/google/android/youtube/app/remote/bf;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->r:Lcom/google/android/youtube/app/remote/RemoteControl;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/ad;->o:Lcom/google/android/youtube/app/remote/ap;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->b(Lcom/google/android/youtube/app/remote/ap;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->p:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "YouTubeTvDisconnect"

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final declared-synchronized a(Lcom/google/android/youtube/app/remote/ag;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/google/android/youtube/app/remote/ah;)V
    .locals 1

    const-string v0, "provider cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/remote/ah;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->t:Lcom/google/android/youtube/app/remote/ah;

    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/remote/bp;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->l:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "Trying to select an unknown route - will ignore"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/app/remote/ad;->c:Ljava/lang/Object;

    const v2, 0x800001

    invoke-static {v1, v2, v0}, Lcom/android/athome/picker/media/k;->a(Ljava/lang/Object;ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/remote/ad;->h()V

    return-void
.end method

.method public final a(Ljava/lang/Object;ILjava/lang/Object;)V
    .locals 3

    const/4 v1, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "onRouteSelected: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->s:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->s:Ljava/lang/Object;

    invoke-virtual {p0, p2, v0}, Lcom/google/android/youtube/app/remote/ad;->a(ILjava/lang/Object;)V

    :cond_0
    const/high16 v0, 0x800000

    and-int/2addr v0, p2

    if-eqz v0, :cond_5

    invoke-direct {p0, p3}, Lcom/google/android/youtube/app/remote/ad;->d(Ljava/lang/Object;)Lcom/google/android/youtube/app/remote/bp;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/ad;->i:Lcom/google/android/youtube/app/remote/bf;

    invoke-direct {p0, v2, v0}, Lcom/google/android/youtube/app/remote/ad;->a(Lcom/google/android/youtube/app/remote/RemoteControl;Lcom/google/android/youtube/app/remote/bb;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->i:Lcom/google/android/youtube/app/remote/bf;

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/ad;->o:Lcom/google/android/youtube/app/remote/ap;

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/youtube/app/remote/ap;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->i:Lcom/google/android/youtube/app/remote/bf;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/bf;->h()I

    move-result v0

    invoke-static {p3, v0}, Lcom/android/athome/picker/media/r;->a(Ljava/lang/Object;I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->p:Lcom/google/android/youtube/core/Analytics;

    const-string v2, "YouTubeTvConnect"

    invoke-interface {v0, v2}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    :cond_1
    :goto_0
    iput-object p3, p0, Lcom/google/android/youtube/app/remote/ad;->s:Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->r:Lcom/google/android/youtube/app/remote/RemoteControl;

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/youtube/app/remote/ad;->a:Z

    if-nez v0, :cond_6

    :cond_2
    const/4 v0, 0x1

    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/remote/ad;->a(Z)V

    iput-boolean v1, p0, Lcom/google/android/youtube/app/remote/ad;->a:Z

    :goto_2
    return-void

    :cond_3
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->g:Lcom/google/android/youtube/app/remote/AtHomeConnection;

    invoke-virtual {v0, p3}, Lcom/google/android/youtube/app/remote/AtHomeConnection;->a(Ljava/lang/Object;)Lcom/google/android/youtube/app/remote/m;

    move-result-object v0

    if-nez v0, :cond_4

    const-string v0, "Cannot obtain a screen for a selected @Home route."

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    goto :goto_2

    :cond_4
    iget-object v2, p0, Lcom/google/android/youtube/app/remote/ad;->f:Lcom/google/android/youtube/app/remote/e;

    invoke-direct {p0, v2, v0}, Lcom/google/android/youtube/app/remote/ad;->a(Lcom/google/android/youtube/app/remote/RemoteControl;Lcom/google/android/youtube/app/remote/bb;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->p:Lcom/google/android/youtube/core/Analytics;

    const-string v2, "AtHomeConnect"

    invoke-interface {v0, v2}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    and-int/lit8 v0, p2, 0x1

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->r:Lcom/google/android/youtube/app/remote/RemoteControl;

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method public final b()Lcom/google/android/youtube/app/remote/RemoteControl;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->r:Lcom/google/android/youtube/app/remote/RemoteControl;

    return-object v0
.end method

.method public final declared-synchronized b(Lcom/google/android/youtube/app/remote/ag;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Lcom/google/android/youtube/app/remote/ah;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->t:Lcom/google/android/youtube/app/remote/ah;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->t:Lcom/google/android/youtube/app/remote/ah;

    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/youtube/app/remote/ad;->h()V

    invoke-static {p1}, Lcom/android/athome/picker/media/q;->f(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/ad;->m:Ljava/lang/Object;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->l:Ljava/util/Map;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/remote/ad;->d(Ljava/lang/Object;)Lcom/google/android/youtube/app/remote/bp;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public final c()V
    .locals 4

    iget v0, p0, Lcom/google/android/youtube/app/remote/ad;->e:I

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->j:Lcom/google/android/youtube/app/remote/br;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/ad;->k:Lcom/google/android/youtube/app/remote/aj;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/remote/br;->a(Lcom/google/android/youtube/app/remote/bd;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->j:Lcom/google/android/youtube/app/remote/br;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/br;->c()V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->g:Lcom/google/android/youtube/app/remote/AtHomeConnection;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/ad;->h:Lcom/google/android/youtube/app/remote/c;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/remote/AtHomeConnection;->a(Lcom/google/android/youtube/app/remote/c;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->j:Lcom/google/android/youtube/app/remote/br;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/br;->a()Ljava/util/List;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->l:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/youtube/app/remote/ad;->c:Ljava/lang/Object;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/android/athome/picker/media/k;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/remote/bp;

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/ad;->l:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/remote/ad;->b(Lcom/google/android/youtube/app/remote/bp;)V

    goto :goto_1

    :cond_3
    iget v0, p0, Lcom/google/android/youtube/app/remote/ad;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/youtube/app/remote/ad;->e:I

    return-void
.end method

.method public final d()V
    .locals 2

    iget v0, p0, Lcom/google/android/youtube/app/remote/ad;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/app/remote/ad;->e:I

    iget v0, p0, Lcom/google/android/youtube/app/remote/ad;->e:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->j:Lcom/google/android/youtube/app/remote/br;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/ad;->k:Lcom/google/android/youtube/app/remote/aj;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/remote/br;->b(Lcom/google/android/youtube/app/remote/bd;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->g:Lcom/google/android/youtube/app/remote/AtHomeConnection;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/ad;->h:Lcom/google/android/youtube/app/remote/c;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/remote/AtHomeConnection;->b(Lcom/google/android/youtube/app/remote/c;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->j:Lcom/google/android/youtube/app/remote/br;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/br;->b()V

    :cond_0
    return-void
.end method

.method public final e()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->s:Ljava/lang/Object;

    if-nez v0, :cond_1

    const-string v0, "Trying to unselect an unknown route - will ignore"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->s:Ljava/lang/Object;

    invoke-static {v0}, Lcom/android/athome/picker/media/q;->f(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/ad;->m:Ljava/lang/Object;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->s:Ljava/lang/Object;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/remote/ad;->d(Ljava/lang/Object;)Lcom/google/android/youtube/app/remote/bp;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/ad;->c:Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/ad;->s:Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/android/athome/picker/media/k;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/remote/ad;->b(Lcom/google/android/youtube/app/remote/bp;)V

    goto :goto_0
.end method

.method public final f()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ad;->m:Ljava/lang/Object;

    return-object v0
.end method

.method public final g()Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/ad;->u:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/ad;->u:Ljava/util/Set;

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/ad;->m:Ljava/lang/Object;

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/youtube/app/remote/ad;->v:I

    if-gt v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
