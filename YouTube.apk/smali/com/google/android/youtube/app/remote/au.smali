.class final Lcom/google/android/youtube/app/remote/au;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/remote/as;

.field private final b:Lcom/google/android/youtube/core/model/Video;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/remote/as;Lcom/google/android/youtube/core/model/Video;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/remote/au;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/youtube/app/remote/au;->b:Lcom/google/android/youtube/core/model/Video;

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 6

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/au;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/as;->k(Lcom/google/android/youtube/app/remote/as;)Lcom/google/android/youtube/app/remote/av;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/au;->b:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/au;->b:Lcom/google/android/youtube/core/model/Video;

    iget-object v2, v2, Lcom/google/android/youtube/core/model/Video;->title:Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/youtube/app/remote/au;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v4}, Lcom/google/android/youtube/app/remote/as;->a(Lcom/google/android/youtube/app/remote/as;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/youtube/app/remote/RemoteControl;->f()Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/youtube/app/remote/au;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v5}, Lcom/google/android/youtube/app/remote/as;->a(Lcom/google/android/youtube/app/remote/as;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/youtube/app/remote/RemoteControl;->t()Lcom/google/android/youtube/app/remote/bb;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/youtube/app/remote/bb;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/youtube/app/remote/av;->a(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/au;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/as;->e(Lcom/google/android/youtube/app/remote/as;)Lcom/google/android/youtube/app/remote/aq;

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/au;->b:Lcom/google/android/youtube/core/model/Video;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Video;->title:Ljava/lang/String;

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 6

    check-cast p2, Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/au;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v0, p2}, Lcom/google/android/youtube/app/remote/as;->a(Lcom/google/android/youtube/app/remote/as;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/au;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/as;->k(Lcom/google/android/youtube/app/remote/as;)Lcom/google/android/youtube/app/remote/av;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/au;->b:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/au;->b:Lcom/google/android/youtube/core/model/Video;

    iget-object v2, v2, Lcom/google/android/youtube/core/model/Video;->title:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/youtube/app/remote/au;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v4}, Lcom/google/android/youtube/app/remote/as;->a(Lcom/google/android/youtube/app/remote/as;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/youtube/app/remote/RemoteControl;->f()Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/youtube/app/remote/au;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v5}, Lcom/google/android/youtube/app/remote/as;->a(Lcom/google/android/youtube/app/remote/as;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/youtube/app/remote/RemoteControl;->t()Lcom/google/android/youtube/app/remote/bb;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/youtube/app/remote/bb;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/youtube/app/remote/av;->a(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/au;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/as;->e(Lcom/google/android/youtube/app/remote/as;)Lcom/google/android/youtube/app/remote/aq;

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/au;->b:Lcom/google/android/youtube/core/model/Video;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Video;->title:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/au;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/as;->e(Lcom/google/android/youtube/app/remote/as;)Lcom/google/android/youtube/app/remote/aq;

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/au;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/as;->a(Lcom/google/android/youtube/app/remote/as;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/au;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/as;->a(Lcom/google/android/youtube/app/remote/as;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->q()Z

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/au;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/as;->a(Lcom/google/android/youtube/app/remote/as;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->o()Z

    return-void
.end method
