.class final Lcom/google/android/youtube/app/remote/av;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/remote/as;

.field private final b:Landroid/app/NotificationManager;

.field private c:Ljava/lang/String;

.field private d:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/remote/as;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/youtube/app/remote/av;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/youtube/app/remote/as;->l(Lcom/google/android/youtube/app/remote/as;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/av;->b:Landroid/app/NotificationManager;

    return-void
.end method

.method private a(Landroid/widget/RemoteViews;ILjava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/av;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/as;->l(Lcom/google/android/youtube/app/remote/as;)Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2, p3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v3, 0x8000000

    invoke-static {v0, v1, v2, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/av;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/av;->d:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/av;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/av;->e:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/av;->b:Landroid/app/NotificationManager;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;Ljava/lang/String;)V
    .locals 8

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/av;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/av;->d:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    invoke-virtual {p4, v0}, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/av;->e:Ljava/lang/String;

    invoke-virtual {p5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/google/android/youtube/app/remote/av;->c:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/youtube/app/remote/av;->f:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/youtube/app/remote/av;->g:Landroid/graphics/Bitmap;

    iput-object p4, p0, Lcom/google/android/youtube/app/remote/av;->d:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    iput-object p5, p0, Lcom/google/android/youtube/app/remote/av;->e:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/av;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/as;->m(Lcom/google/android/youtube/app/remote/as;)Lcom/google/android/youtube/app/remote/ad;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/ad;->g()Z

    move-result v0

    if-eqz v0, :cond_5

    const v0, 0x7f02015c

    :goto_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    new-instance v1, Landroid/widget/RemoteViews;

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/av;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v2}, Lcom/google/android/youtube/app/remote/as;->l(Lcom/google/android/youtube/app/remote/as;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f04005f

    invoke-direct {v1, v2, v3}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    const v2, 0x7f07002c

    invoke-virtual {v1, v2, p2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v2, 0x7f0700ee

    iget-object v3, p0, Lcom/google/android/youtube/app/remote/av;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v3, p5}, Lcom/google/android/youtube/app/remote/as;->a(Lcom/google/android/youtube/app/remote/as;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    if-eqz p3, :cond_1

    const v2, 0x7f070035

    invoke-virtual {v1, v2, p3}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    :cond_1
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "http://www.youtube.com/watch?v="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    iget-object v3, p0, Lcom/google/android/youtube/app/remote/av;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v3}, Lcom/google/android/youtube/app/remote/as;->l(Lcom/google/android/youtube/app/remote/as;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getFlags()I

    move-result v3

    const/high16 v4, 0x20000000

    or-int/2addr v3, v4

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v3, p0, Lcom/google/android/youtube/app/remote/av;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v3}, Lcom/google/android/youtube/app/remote/as;->l(Lcom/google/android/youtube/app/remote/as;)Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x0

    const/high16 v5, 0x10000000

    invoke-static {v3, v4, v2, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    new-instance v3, Landroid/support/v4/app/af;

    iget-object v4, p0, Lcom/google/android/youtube/app/remote/av;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v4}, Lcom/google/android/youtube/app/remote/as;->l(Lcom/google/android/youtube/app/remote/as;)Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/support/v4/app/af;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v0}, Landroid/support/v4/app/af;->a(I)Landroid/support/v4/app/af;

    move-result-object v0

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/support/v4/app/af;->a(Z)Landroid/support/v4/app/af;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/af;->a(Landroid/widget/RemoteViews;)Landroid/support/v4/app/af;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/support/v4/app/af;->a(Landroid/app/PendingIntent;)Landroid/support/v4/app/af;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/af;->a()Landroid/app/Notification;

    move-result-object v0

    sget v2, Lcom/google/android/youtube/core/utils/Util;->a:I

    const/16 v3, 0x10

    if-lt v2, v3, :cond_3

    new-instance v2, Landroid/widget/RemoteViews;

    iget-object v3, p0, Lcom/google/android/youtube/app/remote/av;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v3}, Lcom/google/android/youtube/app/remote/as;->l(Lcom/google/android/youtube/app/remote/as;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f04007a

    invoke-direct {v2, v3, v4}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    const v3, 0x7f07002c

    invoke-virtual {v2, v3, p2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v3, 0x7f0700ee

    iget-object v4, p0, Lcom/google/android/youtube/app/remote/av;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v4, p5}, Lcom/google/android/youtube/app/remote/as;->a(Lcom/google/android/youtube/app/remote/as;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v3, 0x7f070110

    const-string v4, "com.google.android.youtube.action.remote_next"

    invoke-direct {p0, v2, v3, v4}, Lcom/google/android/youtube/app/remote/av;->a(Landroid/widget/RemoteViews;ILjava/lang/String;)V

    const v3, 0x7f07010f

    const-string v4, "com.google.android.youtube.action.remote_prev"

    invoke-direct {p0, v2, v3, v4}, Lcom/google/android/youtube/app/remote/av;->a(Landroid/widget/RemoteViews;ILjava/lang/String;)V

    const v3, 0x7f07010e

    const-string v4, "com.google.android.youtube.action.remote_playpause"

    invoke-direct {p0, v2, v3, v4}, Lcom/google/android/youtube/app/remote/av;->a(Landroid/widget/RemoteViews;ILjava/lang/String;)V

    sget-object v3, Lcom/google/android/youtube/app/remote/at;->a:[I

    invoke-virtual {p4}, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    const v3, 0x7f07010e

    const v4, 0x7f020161

    invoke-virtual {v2, v3, v4}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    :goto_2
    iget-object v3, p0, Lcom/google/android/youtube/app/remote/av;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v3}, Lcom/google/android/youtube/app/remote/as;->a(Lcom/google/android/youtube/app/remote/as;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/youtube/app/remote/RemoteControl;->k()Z

    move-result v3

    if-eqz v3, :cond_6

    const v3, 0x7f070111

    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    const-string v5, "android.intent.category.LAUNCHER"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const-string v5, "guide_selection"

    const-string v6, "REMOTE"

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v5, p0, Lcom/google/android/youtube/app/remote/av;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v5}, Lcom/google/android/youtube/app/remote/as;->l(Lcom/google/android/youtube/app/remote/as;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v4}, Landroid/content/Intent;->getFlags()I

    move-result v5

    const/high16 v6, 0x20000000

    or-int/2addr v5, v6

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v5, p0, Lcom/google/android/youtube/app/remote/av;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v5}, Lcom/google/android/youtube/app/remote/as;->l(Lcom/google/android/youtube/app/remote/as;)Landroid/content/Context;

    move-result-object v5

    const/4 v6, 0x0

    const/high16 v7, 0x10000000

    invoke-static {v5, v6, v4, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    const v3, 0x7f070111

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    :goto_3
    if-eqz p3, :cond_2

    const v3, 0x7f070035

    invoke-virtual {v2, v3, p3}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    :cond_2
    iput-object v2, v0, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    :cond_3
    sget v2, Lcom/google/android/youtube/core/utils/Util;->a:I

    const/16 v3, 0xa

    if-gt v2, v3, :cond_4

    iput-object v1, v0, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    :cond_4
    iget-object v1, p0, Lcom/google/android/youtube/app/remote/av;->b:Landroid/app/NotificationManager;

    const/16 v2, 0x3e8

    invoke-virtual {v1, v2, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto/16 :goto_0

    :cond_5
    const v0, 0x7f02021c

    goto/16 :goto_1

    :pswitch_0
    const v3, 0x7f07010e

    const v4, 0x7f020160

    invoke-virtual {v2, v3, v4}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto :goto_2

    :pswitch_1
    const v3, 0x7f07010e

    const v4, 0x7f020161

    invoke-virtual {v2, v3, v4}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto/16 :goto_2

    :cond_6
    const v3, 0x7f070111

    const/16 v4, 0x8

    invoke-virtual {v2, v3, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
