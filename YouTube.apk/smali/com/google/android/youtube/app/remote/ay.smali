.class final Lcom/google/android/youtube/app/remote/ay;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/bk;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/remote/as;

.field private final b:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/remote/as;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/remote/ay;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/youtube/app/remote/ay;->b:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/app/remote/as;Ljava/lang/String;B)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/app/remote/ay;-><init>(Lcom/google/android/youtube/app/remote/as;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ay;->a:Lcom/google/android/youtube/app/remote/as;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/remote/as;->a(Lcom/google/android/youtube/app/remote/as;Lcom/google/android/youtube/core/async/o;)Lcom/google/android/youtube/core/async/o;

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ay;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/as;->j(Lcom/google/android/youtube/app/remote/as;)Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/ay;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/ay;->a:Lcom/google/android/youtube/app/remote/as;

    iget-object v3, p0, Lcom/google/android/youtube/app/remote/ay;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v3}, Lcom/google/android/youtube/app/remote/as;->i(Lcom/google/android/youtube/app/remote/as;)Lcom/google/android/youtube/core/async/n;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/youtube/app/remote/as;->a(Lcom/google/android/youtube/app/remote/as;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/n;

    move-result-object v2

    invoke-interface {v0, v1, p1, v2}, Lcom/google/android/youtube/core/client/bc;->a(Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 4

    const-string v0, "Error while authenticating"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ay;->a:Lcom/google/android/youtube/app/remote/as;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/remote/as;->a(Lcom/google/android/youtube/app/remote/as;Lcom/google/android/youtube/core/async/o;)Lcom/google/android/youtube/core/async/o;

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ay;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/as;->j(Lcom/google/android/youtube/app/remote/as;)Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/ay;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/ay;->a:Lcom/google/android/youtube/app/remote/as;

    iget-object v3, p0, Lcom/google/android/youtube/app/remote/ay;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v3}, Lcom/google/android/youtube/app/remote/as;->i(Lcom/google/android/youtube/app/remote/as;)Lcom/google/android/youtube/core/async/n;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/youtube/app/remote/as;->a(Lcom/google/android/youtube/app/remote/as;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/n;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/bc;->a(Ljava/lang/String;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method

.method public final g_()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ay;->a:Lcom/google/android/youtube/app/remote/as;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/remote/as;->a(Lcom/google/android/youtube/app/remote/as;Lcom/google/android/youtube/core/async/o;)Lcom/google/android/youtube/core/async/o;

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/ay;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/as;->j(Lcom/google/android/youtube/app/remote/as;)Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/ay;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/ay;->a:Lcom/google/android/youtube/app/remote/as;

    iget-object v3, p0, Lcom/google/android/youtube/app/remote/ay;->a:Lcom/google/android/youtube/app/remote/as;

    invoke-static {v3}, Lcom/google/android/youtube/app/remote/as;->i(Lcom/google/android/youtube/app/remote/as;)Lcom/google/android/youtube/core/async/n;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/youtube/app/remote/as;->a(Lcom/google/android/youtube/app/remote/as;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/n;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/bc;->a(Ljava/lang/String;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method
