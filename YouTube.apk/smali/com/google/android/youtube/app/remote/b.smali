.class final Lcom/google/android/youtube/app/remote/b;
.super Landroid/support/place/api/broker/BrokerManager$ConnectionListener;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/remote/AtHomeConnection;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/remote/AtHomeConnection;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/remote/b;->a:Lcom/google/android/youtube/app/remote/AtHomeConnection;

    invoke-direct {p0}, Landroid/support/place/api/broker/BrokerManager$ConnectionListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/app/remote/AtHomeConnection;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/remote/b;-><init>(Lcom/google/android/youtube/app/remote/AtHomeConnection;)V

    return-void
.end method


# virtual methods
.method public final onBrokerConnected(Landroid/support/place/connector/Broker;)V
    .locals 1

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/b;->a:Lcom/google/android/youtube/app/remote/AtHomeConnection;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/AtHomeConnection;->b(Lcom/google/android/youtube/app/remote/AtHomeConnection;)V

    return-void
.end method

.method public final onBrokerDisconnected()V
    .locals 1

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/b;->a:Lcom/google/android/youtube/app/remote/AtHomeConnection;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/AtHomeConnection;->b(Lcom/google/android/youtube/app/remote/AtHomeConnection;)V

    return-void
.end method

.method public final onPlaceAdded(Landroid/support/place/connector/PlaceInfo;)V
    .locals 1

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/b;->a:Lcom/google/android/youtube/app/remote/AtHomeConnection;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/AtHomeConnection;->b(Lcom/google/android/youtube/app/remote/AtHomeConnection;)V

    return-void
.end method

.method public final onPlaceConnected(Landroid/support/place/connector/PlaceInfo;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onPlaceConnnected - "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/support/place/connector/PlaceInfo;->getPlaceName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/b;->a:Lcom/google/android/youtube/app/remote/AtHomeConnection;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/AtHomeConnection;->b(Lcom/google/android/youtube/app/remote/AtHomeConnection;)V

    return-void
.end method

.method public final onPlaceDisconnected()V
    .locals 1

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/b;->a:Lcom/google/android/youtube/app/remote/AtHomeConnection;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/AtHomeConnection;->b(Lcom/google/android/youtube/app/remote/AtHomeConnection;)V

    return-void
.end method

.method public final onPlaceRemoved(Landroid/support/place/connector/PlaceInfo;)V
    .locals 1

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/b;->a:Lcom/google/android/youtube/app/remote/AtHomeConnection;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/AtHomeConnection;->b(Lcom/google/android/youtube/app/remote/AtHomeConnection;)V

    return-void
.end method
