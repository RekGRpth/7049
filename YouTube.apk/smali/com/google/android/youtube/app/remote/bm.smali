.class final Lcom/google/android/youtube/app/remote/bm;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/ytremote/logic/e;


# instance fields
.field final synthetic a:Lcom/google/android/ytremote/model/d;

.field final synthetic b:Lcom/google/android/youtube/app/remote/bi;

.field final synthetic c:Lcom/google/android/youtube/app/remote/bl;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/remote/bl;Lcom/google/android/ytremote/model/d;Lcom/google/android/youtube/app/remote/bi;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/remote/bm;->c:Lcom/google/android/youtube/app/remote/bl;

    iput-object p2, p0, Lcom/google/android/youtube/app/remote/bm;->a:Lcom/google/android/ytremote/model/d;

    iput-object p3, p0, Lcom/google/android/youtube/app/remote/bm;->b:Lcom/google/android/youtube/app/remote/bi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bm;->c:Lcom/google/android/youtube/app/remote/bl;

    iget-object v0, v0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-static {v0, p1}, Lcom/google/android/youtube/app/remote/bf;->b(Lcom/google/android/youtube/app/remote/bf;I)Lcom/google/android/youtube/app/remote/an;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bm;->c:Lcom/google/android/youtube/app/remote/bl;

    iget-object v1, v1, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not find cloud screen corresponding to DIAL device  "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/youtube/app/remote/bm;->a:Lcom/google/android/ytremote/model/d;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/youtube/app/remote/bf;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bm;->c:Lcom/google/android/youtube/app/remote/bl;

    iget-object v1, v1, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/remote/bf;->x()Lcom/google/android/youtube/app/remote/bp;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/youtube/app/remote/bp;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/google/android/youtube/app/remote/bp;->b()Lcom/google/android/ytremote/model/d;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bm;->a:Lcom/google/android/ytremote/model/d;

    invoke-virtual {v1, v2}, Lcom/google/android/ytremote/model/d;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bm;->c:Lcom/google/android/youtube/app/remote/bl;

    iget-object v0, v0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Connection to DIAL device "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bm;->a:Lcom/google/android/ytremote/model/d;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " was canceled in the meantime. Will not move to error state."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/youtube/app/remote/bf;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bm;->c:Lcom/google/android/youtube/app/remote/bl;

    iget-object v1, v1, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/google/android/youtube/app/remote/bf;->c(Lcom/google/android/youtube/app/remote/bf;Z)Z

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bm;->c:Lcom/google/android/youtube/app/remote/bl;

    iget-object v1, v1, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/youtube/app/remote/an;)V

    goto :goto_0
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bm;->c:Lcom/google/android/youtube/app/remote/bl;

    iget-object v0, v0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Successfully launched YouTube TV on DIAL device "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bm;->a:Lcom/google/android/ytremote/model/d;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/youtube/app/remote/bf;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bm;->c:Lcom/google/android/youtube/app/remote/bl;

    iget-object v0, v0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bf;->c(Lcom/google/android/youtube/app/remote/bf;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bm;->a:Lcom/google/android/ytremote/model/d;

    invoke-virtual {v1}, Lcom/google/android/ytremote/model/d;->e()Lcom/google/android/ytremote/model/SsdpId;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/ytremote/model/CloudScreen;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bm;->c:Lcom/google/android/youtube/app/remote/bl;

    iget-object v0, v0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Found corresponding cloud screen "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for DIAL device "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bm;->a:Lcom/google/android/ytremote/model/d;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/youtube/app/remote/bf;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bm;->c:Lcom/google/android/youtube/app/remote/bl;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bm;->a:Lcom/google/android/ytremote/model/d;

    invoke-static {v0, v1, p1}, Lcom/google/android/youtube/app/remote/bl;->a(Lcom/google/android/youtube/app/remote/bl;Lcom/google/android/ytremote/model/d;Lcom/google/android/ytremote/model/CloudScreen;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bm;->c:Lcom/google/android/youtube/app/remote/bl;

    iget-object v0, v0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/bf;->x()Lcom/google/android/youtube/app/remote/bp;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/bp;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/bp;->b()Lcom/google/android/ytremote/model/d;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bm;->a:Lcom/google/android/ytremote/model/d;

    invoke-virtual {v0, v1}, Lcom/google/android/ytremote/model/d;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bm;->c:Lcom/google/android/youtube/app/remote/bl;

    iget-object v0, v0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Connection to DIAL device "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bm;->a:Lcom/google/android/ytremote/model/d;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " was canceled. Will not connect to the cloud"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/youtube/app/remote/bf;Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bm;->c:Lcom/google/android/youtube/app/remote/bl;

    iget-object v0, v0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/remote/bf;->c(Lcom/google/android/youtube/app/remote/bf;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bm;->c:Lcom/google/android/youtube/app/remote/bl;

    iget-object v0, v0, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/bf;->v()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bm;->c:Lcom/google/android/youtube/app/remote/bl;

    iget-object v1, v1, Lcom/google/android/youtube/app/remote/bl;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-static {v1}, Lcom/google/android/youtube/app/remote/bf;->m(Lcom/google/android/youtube/app/remote/bf;)Lcom/google/android/ytremote/model/CloudScreen;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/ytremote/model/CloudScreen;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-eq v0, v1, :cond_1

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->CONNECTING:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-eq v0, v1, :cond_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bm;->c:Lcom/google/android/youtube/app/remote/bl;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bm;->b:Lcom/google/android/youtube/app/remote/bi;

    invoke-static {v0, p1, v1}, Lcom/google/android/youtube/app/remote/bl;->a(Lcom/google/android/youtube/app/remote/bl;Lcom/google/android/ytremote/model/CloudScreen;Lcom/google/android/youtube/app/remote/bi;)V

    goto :goto_0
.end method
