.class final Lcom/google/android/youtube/app/remote/bo;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/remote/bf;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/remote/bf;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/remote/bo;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/app/remote/bf;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/remote/bo;-><init>(Lcom/google/android/youtube/app/remote/bf;)V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    const-string v0, "android.intent.action.SCREEN_OFF"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bo;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/bf;->v()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->OFFLINE:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bo;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bf;->B(Lcom/google/android/youtube/app/remote/bf;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "android.intent.action.SCREEN_ON"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bo;->a:Lcom/google/android/youtube/app/remote/bf;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/bf;->a()V

    goto :goto_0
.end method
