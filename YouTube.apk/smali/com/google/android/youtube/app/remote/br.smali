.class public final Lcom/google/android/youtube/app/remote/br;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/remote/bc;


# instance fields
.field private final a:Ljava/util/concurrent/CopyOnWriteArrayList;

.field private final b:Ljava/util/concurrent/CopyOnWriteArrayList;

.field private final c:Ljava/util/concurrent/CopyOnWriteArrayList;

.field private final d:Ljava/util/concurrent/CopyOnWriteArrayList;

.field private final e:Lcom/google/android/youtube/core/async/n;

.field private final f:Lcom/google/android/youtube/app/remote/bq;

.field private final g:Lcom/google/android/youtube/app/remote/bf;

.field private final h:Landroid/content/SharedPreferences;

.field private final i:Ljava/util/Map;

.field private final j:Ljava/util/Map;

.field private final k:Ljava/util/concurrent/CopyOnWriteArrayList;

.field private final l:Landroid/os/Handler;

.field private final m:Lcom/google/android/youtube/core/utils/p;

.field private final n:Z

.field private final o:Lcom/google/android/ytremote/logic/b;

.field private p:Z

.field private q:Z

.field private final r:Ljava/util/concurrent/Executor;

.field private final s:Lcom/google/android/youtube/app/a;

.field private final t:Z


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Lcom/google/android/youtube/app/remote/bq;Lcom/google/android/youtube/core/utils/p;ZLcom/google/android/youtube/app/remote/bf;Landroid/content/SharedPreferences;ZLcom/google/android/youtube/app/a;)V
    .locals 6

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "screensClient can not be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/remote/bq;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/br;->f:Lcom/google/android/youtube/app/remote/bq;

    const-string v0, "networkStatus can not be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/utils/p;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/br;->m:Lcom/google/android/youtube/core/utils/p;

    const-string v0, "youTubeTvRemoteControl can not be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/remote/bf;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/br;->g:Lcom/google/android/youtube/app/remote/bf;

    const-string v0, "preferences can not be null"

    invoke-static {p6, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/br;->h:Landroid/content/SharedPreferences;

    const-string v0, "executor can not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/br;->r:Ljava/util/concurrent/Executor;

    iput-boolean p7, p0, Lcom/google/android/youtube/app/remote/br;->t:Z

    iput-boolean p4, p0, Lcom/google/android/youtube/app/remote/br;->n:Z

    const-string v0, "flags can not be null"

    invoke-static {p8, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/a;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/br;->s:Lcom/google/android/youtube/app/a;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/br;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/br;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/br;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/br;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v0, Lcom/google/android/youtube/app/remote/cg;

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/remote/cg;-><init>(Lcom/google/android/youtube/app/remote/br;B)V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/br;->e:Lcom/google/android/youtube/core/async/n;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/br;->i:Ljava/util/Map;

    new-instance v0, Lcom/google/android/ytremote/b/k;

    invoke-direct {v0}, Lcom/google/android/ytremote/b/k;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/br;->o:Lcom/google/android/ytremote/logic/b;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/br;->j:Ljava/util/Map;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/br;->k:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v0, Lcom/google/android/youtube/app/remote/bs;

    invoke-direct {v0, p0, p6}, Lcom/google/android/youtube/app/remote/bs;-><init>(Lcom/google/android/youtube/app/remote/br;Landroid/content/SharedPreferences;)V

    invoke-interface {p1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    new-instance v0, Lcom/google/android/youtube/app/remote/ce;

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/remote/ce;-><init>(Lcom/google/android/youtube/app/remote/br;B)V

    invoke-virtual {p5, v0}, Lcom/google/android/youtube/app/remote/bf;->a(Lcom/google/android/youtube/app/remote/bn;)V

    new-instance v0, Lcom/google/android/ytremote/b/c;

    invoke-direct {v0}, Lcom/google/android/ytremote/b/c;-><init>()V

    new-instance v1, Lcom/google/android/youtube/app/remote/bt;

    invoke-direct {v1, p0, v0}, Lcom/google/android/youtube/app/remote/bt;-><init>(Lcom/google/android/youtube/app/remote/br;Lcom/google/android/ytremote/b/c;)V

    invoke-static {p1, v1}, Lcom/google/android/youtube/core/async/i;->a(Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/async/au;)Lcom/google/android/youtube/core/async/i;

    move-result-object v5

    new-instance v0, Lcom/google/android/youtube/app/remote/bu;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p2

    move-object v4, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/remote/bu;-><init>(Lcom/google/android/youtube/app/remote/br;Lcom/google/android/youtube/core/utils/p;Lcom/google/android/youtube/app/remote/bq;Lcom/google/android/youtube/app/remote/bf;Lcom/google/android/youtube/core/async/i;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/br;->l:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/br;Lcom/google/android/ytremote/model/d;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/remote/br;->a(Lcom/google/android/ytremote/model/d;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized a(Lcom/google/android/ytremote/model/d;)Ljava/lang/String;
    .locals 6

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->h:Landroid/content/SharedPreferences;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/remote/br;->a(Landroid/content/SharedPreferences;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->j:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/android/ytremote/model/d;->e()Lcom/google/android/ytremote/model/SsdpId;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->j:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/android/ytremote/model/d;->e()Lcom/google/android/ytremote/model/SsdpId;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/ytremote/model/d;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/youtube/app/remote/br;->j:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->k:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {p1}, Lcom/google/android/ytremote/model/d;->e()Lcom/google/android/ytremote/model/SsdpId;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/ytremote/model/d;->c()Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x2

    move v2, v0

    move-object v0, v1

    :goto_1
    iget-object v3, p0, Lcom/google/android/youtube/app/remote/br;->j:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->containsValue(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/google/android/youtube/app/remote/br;->j:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/android/ytremote/model/d;->e()Lcom/google/android/ytremote/model/SsdpId;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/br;->k:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {p1}, Lcom/google/android/ytremote/model/d;->e()Lcom/google/android/ytremote/model/SsdpId;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/br;->r:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/google/android/youtube/app/remote/cf;

    iget-object v3, p0, Lcom/google/android/youtube/app/remote/br;->h:Landroid/content/SharedPreferences;

    iget-object v4, p0, Lcom/google/android/youtube/app/remote/br;->j:Ljava/util/Map;

    iget-object v5, p0, Lcom/google/android/youtube/app/remote/br;->k:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/youtube/app/remote/cf;-><init>(Landroid/content/SharedPreferences;Ljava/util/Map;Ljava/util/concurrent/CopyOnWriteArrayList;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Landroid/content/SharedPreferences;)V
    .locals 6

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/youtube/app/remote/br;->q:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    const-string v0, "dial_device_ids"

    invoke-interface {p1, v0}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "dial_device_ids"

    const-string v1, ""

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const-string v0, "dial_device_names"

    const-string v2, ""

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x0

    :goto_1
    array-length v3, v1

    if-ge v0, v3, :cond_3

    aget-object v3, v1, v0

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    new-instance v4, Lcom/google/android/ytremote/model/SsdpId;

    invoke-direct {v4, v3}, Lcom/google/android/ytremote/model/SsdpId;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/youtube/app/remote/br;->k:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3, v4}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/google/android/youtube/app/remote/br;->j:Ljava/util/Map;

    aget-object v5, v2, v0

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/remote/br;->q:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(Lcom/google/android/youtube/app/remote/bp;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->l:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/app/remote/bx;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/app/remote/bx;-><init>(Lcom/google/android/youtube/app/remote/br;Lcom/google/android/youtube/app/remote/bp;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/br;Landroid/content/SharedPreferences;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/remote/br;->a(Landroid/content/SharedPreferences;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/br;Lcom/google/android/youtube/app/remote/bp;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/remote/bp;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/bp;->b()Lcom/google/android/ytremote/model/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/ytremote/model/d;->e()Lcom/google/android/ytremote/model/SsdpId;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/youtube/app/remote/bp;->b()Lcom/google/android/ytremote/model/d;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/ytremote/model/d;->e()Lcom/google/android/ytremote/model/SsdpId;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/ytremote/model/SsdpId;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Removing duplicate device "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/youtube/app/remote/br;->a(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/remote/br;->c(Lcom/google/android/youtube/app/remote/bp;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/remote/br;->a(Lcom/google/android/youtube/app/remote/bp;)V

    :cond_2
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/br;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/remote/br;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    iget-boolean v0, p0, Lcom/google/android/youtube/app/remote/br;->t:Z

    if-nez v0, :cond_0

    const-string v0, "true"

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/br;->s:Lcom/google/android/youtube/app/a;

    const-string v2, "enable_mdx_logs"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/google/android/youtube/app/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, "YouTube"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/br;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/remote/br;->p:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/remote/br;)Lcom/google/android/youtube/core/async/n;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->e:Lcom/google/android/youtube/core/async/n;

    return-object v0
.end method

.method private b(Lcom/google/android/youtube/app/remote/bp;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->l:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/app/remote/by;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/app/remote/by;-><init>(Lcom/google/android/youtube/app/remote/br;Lcom/google/android/youtube/app/remote/bp;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method static synthetic b(Lcom/google/android/youtube/app/remote/br;Lcom/google/android/youtube/app/remote/bp;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/remote/bp;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/bp;->c()Lcom/google/android/ytremote/model/CloudScreen;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/ytremote/model/CloudScreen;->getScreenId()Lcom/google/android/ytremote/model/ScreenId;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/youtube/app/remote/bp;->c()Lcom/google/android/ytremote/model/CloudScreen;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/ytremote/model/CloudScreen;->getScreenId()Lcom/google/android/ytremote/model/ScreenId;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/ytremote/model/ScreenId;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Removing duplicate screen "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/youtube/app/remote/br;->a(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/remote/br;->d(Lcom/google/android/youtube/app/remote/bp;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/remote/br;->a(Lcom/google/android/youtube/app/remote/bp;)V

    :cond_2
    return-void
.end method

.method static synthetic c(Lcom/google/android/youtube/app/remote/br;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method private c(Lcom/google/android/youtube/app/remote/bp;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Removing dial device "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/remote/br;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/remote/br;->b(Lcom/google/android/youtube/app/remote/bp;)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/youtube/app/remote/br;Lcom/google/android/youtube/app/remote/bp;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/remote/br;->b(Lcom/google/android/youtube/app/remote/bp;)V

    return-void
.end method

.method private d()V
    .locals 5

    const/4 v3, 0x2

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->l:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->m:Lcom/google/android/youtube/core/utils/p;

    invoke-interface {v0}, Lcom/google/android/youtube/core/utils/p;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->m:Lcom/google/android/youtube/core/utils/p;

    invoke-interface {v0}, Lcom/google/android/youtube/core/utils/p;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->m:Lcom/google/android/youtube/core/utils/p;

    invoke-interface {v0}, Lcom/google/android/youtube/core/utils/p;->a()Z

    move-result v0

    if-nez v0, :cond_4

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "Not connected to wifi/eth or network not available. Will mark all devices as unavailable."

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/remote/br;->a(Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/remote/bp;

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/br;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/remote/br;->b(Lcom/google/android/youtube/app/remote/bp;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    :goto_1
    return-void

    :cond_4
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/ac;->a(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/br;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/br;->l:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/br;->l:Landroid/os/Handler;

    invoke-static {v2, v3, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    const-wide/16 v3, 0x251c

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    const-string v1, "Starting new DIAL search."

    invoke-direct {p0, v1}, Lcom/google/android/youtube/app/remote/br;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/br;->o:Lcom/google/android/ytremote/logic/b;

    new-instance v2, Lcom/google/android/youtube/app/remote/cc;

    invoke-direct {v2, p0, v0}, Lcom/google/android/youtube/app/remote/cc;-><init>(Lcom/google/android/youtube/app/remote/br;Ljava/util/Set;)V

    invoke-interface {v1, v2}, Lcom/google/android/ytremote/logic/b;->a(Lcom/google/android/ytremote/logic/c;)V

    goto :goto_1
.end method

.method private d(Lcom/google/android/youtube/app/remote/bp;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Removing cloud screen "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/remote/br;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/remote/br;->b(Lcom/google/android/youtube/app/remote/bp;)V

    return-void
.end method

.method static synthetic d(Lcom/google/android/youtube/app/remote/br;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/remote/bp;

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/br;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/remote/br;->b(Lcom/google/android/youtube/app/remote/bp;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    return-void
.end method

.method static synthetic d(Lcom/google/android/youtube/app/remote/br;Lcom/google/android/youtube/app/remote/bp;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/remote/br;->d(Lcom/google/android/youtube/app/remote/bp;)V

    return-void
.end method

.method static synthetic e(Lcom/google/android/youtube/app/remote/br;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->l:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/app/remote/br;Lcom/google/android/youtube/app/remote/bp;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/remote/br;->c(Lcom/google/android/youtube/app/remote/bp;)V

    return-void
.end method

.method static synthetic f(Lcom/google/android/youtube/app/remote/br;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/remote/br;->d()V

    return-void
.end method

.method static synthetic g(Lcom/google/android/youtube/app/remote/br;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->i:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/youtube/app/remote/br;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/youtube/app/remote/br;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/youtube/app/remote/br;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/youtube/app/remote/br;)Lcom/google/android/youtube/app/remote/bf;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->g:Lcom/google/android/youtube/app/remote/bf;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method public final a(Lcom/google/android/youtube/app/remote/bd;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final a(Lcom/google/android/ytremote/model/SsdpId;Lcom/google/android/youtube/app/remote/cd;)V
    .locals 4

    iget-boolean v0, p0, Lcom/google/android/youtube/app/remote/br;->n:Z

    if-nez v0, :cond_0

    invoke-interface {p2}, Lcom/google/android/youtube/app/remote/cd;->a()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/remote/bp;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/bp;->b()Lcom/google/android/ytremote/model/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/ytremote/model/d;->e()Lcom/google/android/ytremote/model/SsdpId;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/android/ytremote/model/SsdpId;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p2, v0}, Lcom/google/android/youtube/app/remote/cd;->a(Lcom/google/android/youtube/app/remote/bp;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->i:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->o:Lcom/google/android/ytremote/logic/b;

    new-instance v1, Lcom/google/android/youtube/app/remote/bv;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/youtube/app/remote/bv;-><init>(Lcom/google/android/youtube/app/remote/br;Lcom/google/android/ytremote/model/SsdpId;Lcom/google/android/youtube/app/remote/cd;)V

    invoke-interface {v0, v1}, Lcom/google/android/ytremote/logic/b;->a(Lcom/google/android/ytremote/logic/c;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->l:Landroid/os/Handler;

    const/4 v1, 0x3

    new-instance v2, Landroid/util/Pair;

    invoke-direct {v2, p1, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {v0, v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/br;->l:Landroid/os/Handler;

    const-wide/16 v2, 0x251c

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/ytremote/model/PairingCode;Lcom/google/android/youtube/core/async/n;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->f:Lcom/google/android/youtube/app/remote/bq;

    new-instance v1, Lcom/google/android/youtube/app/remote/bz;

    invoke-direct {v1, p0, p3}, Lcom/google/android/youtube/app/remote/bz;-><init>(Lcom/google/android/youtube/app/remote/br;Lcom/google/android/youtube/core/async/n;)V

    invoke-interface {v0, p1, p2, v1}, Lcom/google/android/youtube/app/remote/bq;->a(Ljava/lang/String;Lcom/google/android/ytremote/model/PairingCode;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method

.method public final b()V
    .locals 2

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/youtube/app/remote/br;->p:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->l:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->l:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    return-void
.end method

.method public final b(Lcom/google/android/youtube/app/remote/bd;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public final c()V
    .locals 5

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->f:Lcom/google/android/youtube/app/remote/bq;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/br;->e:Lcom/google/android/youtube/core/async/n;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/remote/bq;->a(Lcom/google/android/youtube/core/async/n;)V

    iget-boolean v0, p0, Lcom/google/android/youtube/app/remote/br;->n:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/app/remote/br;->d()V

    :cond_0
    iput-boolean v4, p0, Lcom/google/android/youtube/app/remote/br;->p:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->l:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    iget-boolean v0, p0, Lcom/google/android/youtube/app/remote/br;->n:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/br;->l:Landroid/os/Handler;

    const-wide/16 v1, 0x2710

    invoke-virtual {v0, v4, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_1
    return-void
.end method
