.class final Lcom/google/android/youtube/app/remote/bu;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/utils/p;

.field final synthetic b:Lcom/google/android/youtube/app/remote/bq;

.field final synthetic c:Lcom/google/android/youtube/app/remote/bf;

.field final synthetic d:Lcom/google/android/youtube/core/async/i;

.field final synthetic e:Lcom/google/android/youtube/app/remote/br;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/remote/br;Lcom/google/android/youtube/core/utils/p;Lcom/google/android/youtube/app/remote/bq;Lcom/google/android/youtube/app/remote/bf;Lcom/google/android/youtube/core/async/i;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/remote/bu;->e:Lcom/google/android/youtube/app/remote/br;

    iput-object p2, p0, Lcom/google/android/youtube/app/remote/bu;->a:Lcom/google/android/youtube/core/utils/p;

    iput-object p3, p0, Lcom/google/android/youtube/app/remote/bu;->b:Lcom/google/android/youtube/app/remote/bq;

    iput-object p4, p0, Lcom/google/android/youtube/app/remote/bu;->c:Lcom/google/android/youtube/app/remote/bf;

    iput-object p5, p0, Lcom/google/android/youtube/app/remote/bu;->d:Lcom/google/android/youtube/core/async/i;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 6

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bu;->e:Lcom/google/android/youtube/app/remote/br;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/br;->a(Lcom/google/android/youtube/app/remote/br;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bu;->a:Lcom/google/android/youtube/core/utils/p;

    invoke-interface {v0}, Lcom/google/android/youtube/core/utils/p;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bu;->b:Lcom/google/android/youtube/app/remote/bq;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bu;->e:Lcom/google/android/youtube/app/remote/br;

    invoke-static {v1}, Lcom/google/android/youtube/app/remote/br;->b(Lcom/google/android/youtube/app/remote/br;)Lcom/google/android/youtube/core/async/n;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/remote/bq;->a(Lcom/google/android/youtube/core/async/n;)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bu;->e:Lcom/google/android/youtube/app/remote/br;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/br;->e(Lcom/google/android/youtube/app/remote/br;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x0

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bu;->e:Lcom/google/android/youtube/app/remote/br;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/br;->c(Lcom/google/android/youtube/app/remote/br;)Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bu;->e:Lcom/google/android/youtube/app/remote/br;

    const-string v1, "Network unavailable. Removing all screens."

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/remote/br;->a(Lcom/google/android/youtube/app/remote/br;Ljava/lang/String;)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bu;->e:Lcom/google/android/youtube/app/remote/br;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/br;->d(Lcom/google/android/youtube/app/remote/br;)V

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bu;->e:Lcom/google/android/youtube/app/remote/br;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/br;->f(Lcom/google/android/youtube/app/remote/br;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bu;->e:Lcom/google/android/youtube/app/remote/br;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/br;->e(Lcom/google/android/youtube/app/remote/br;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bu;->e:Lcom/google/android/youtube/app/remote/br;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/br;->e(Lcom/google/android/youtube/app/remote/br;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v1, 0x2710

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    :pswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/remote/bp;

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bu;->c:Lcom/google/android/youtube/app/remote/bf;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/remote/bf;->v()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v2

    sget-object v3, Lcom/google/android/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-eq v2, v3, :cond_4

    sget-object v3, Lcom/google/android/youtube/app/remote/RemoteControl$State;->CONNECTING:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-ne v2, v3, :cond_5

    :cond_4
    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bu;->c:Lcom/google/android/youtube/app/remote/bf;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/remote/bf;->x()Lcom/google/android/youtube/app/remote/bp;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/remote/bp;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bu;->e:Lcom/google/android/youtube/app/remote/br;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "RemoteControl connected/connecting to "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/bp;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " . Will not remove the screen from the list of available devices even though it timed out."

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/youtube/app/remote/br;->a(Lcom/google/android/youtube/app/remote/br;Ljava/lang/String;)V

    goto :goto_2

    :cond_5
    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bu;->e:Lcom/google/android/youtube/app/remote/br;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Screen "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/bp;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " timed out. Will check the app status."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/youtube/app/remote/br;->a(Lcom/google/android/youtube/app/remote/br;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bu;->d:Lcom/google/android/youtube/core/async/i;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/bp;->b()Lcom/google/android/ytremote/model/d;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/ytremote/model/d;->b()Landroid/net/Uri;

    move-result-object v3

    new-instance v4, Lcom/google/android/youtube/app/remote/cb;

    iget-object v5, p0, Lcom/google/android/youtube/app/remote/bu;->e:Lcom/google/android/youtube/app/remote/br;

    invoke-direct {v4, v5, v0}, Lcom/google/android/youtube/app/remote/cb;-><init>(Lcom/google/android/youtube/app/remote/br;Lcom/google/android/youtube/app/remote/bp;)V

    invoke-virtual {v2, v3, v4}, Lcom/google/android/youtube/core/async/i;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    goto :goto_2

    :pswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/util/Pair;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/ytremote/model/SsdpId;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/youtube/app/remote/cd;

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bu;->e:Lcom/google/android/youtube/app/remote/br;

    invoke-static {v2}, Lcom/google/android/youtube/app/remote/br;->g(Lcom/google/android/youtube/app/remote/br;)Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_6

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/cd;->a()V

    :cond_6
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bu;->e:Lcom/google/android/youtube/app/remote/br;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/br;->g(Lcom/google/android/youtube/app/remote/br;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
