.class final Lcom/google/android/youtube/app/remote/cc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/ytremote/logic/c;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/remote/br;

.field private final b:Ljava/util/Set;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/remote/br;Ljava/util/Set;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/remote/cc;->a:Lcom/google/android/youtube/app/remote/br;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/youtube/app/remote/cc;->b:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/ytremote/model/d;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/cc;->a:Lcom/google/android/youtube/app/remote/br;

    invoke-static {v0, p1}, Lcom/google/android/youtube/app/remote/br;->a(Lcom/google/android/youtube/app/remote/br;Lcom/google/android/ytremote/model/d;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/ytremote/model/d;->a(Ljava/lang/String;)Lcom/google/android/ytremote/model/d;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/app/remote/bp;

    invoke-direct {v1, v0}, Lcom/google/android/youtube/app/remote/bp;-><init>(Lcom/google/android/ytremote/model/d;)V

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/cc;->a:Lcom/google/android/youtube/app/remote/br;

    invoke-static {v2}, Lcom/google/android/youtube/app/remote/br;->k(Lcom/google/android/youtube/app/remote/br;)Lcom/google/android/youtube/app/remote/bf;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/youtube/app/remote/bf;->v()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v2

    sget-object v3, Lcom/google/android/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/cc;->a:Lcom/google/android/youtube/app/remote/br;

    invoke-static {v2}, Lcom/google/android/youtube/app/remote/br;->k(Lcom/google/android/youtube/app/remote/br;)Lcom/google/android/youtube/app/remote/bf;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/youtube/app/remote/bf;->x()Lcom/google/android/youtube/app/remote/bp;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/remote/bp;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/google/android/ytremote/model/d;->f()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/google/android/ytremote/model/d;->a()Lcom/google/android/ytremote/model/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/ytremote/model/a;->a()I

    move-result v0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/cc;->a:Lcom/google/android/youtube/app/remote/br;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The app status for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is STOPPED. Will remove the route!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/youtube/app/remote/br;->a(Lcom/google/android/youtube/app/remote/br;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/cc;->a:Lcom/google/android/youtube/app/remote/br;

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/remote/br;->e(Lcom/google/android/youtube/app/remote/br;Lcom/google/android/youtube/app/remote/bp;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/cc;->b:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/cc;->a:Lcom/google/android/youtube/app/remote/br;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/br;->i(Lcom/google/android/youtube/app/remote/br;)Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/cc;->a:Lcom/google/android/youtube/app/remote/br;

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/remote/br;->a(Lcom/google/android/youtube/app/remote/br;Lcom/google/android/youtube/app/remote/bp;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/cc;->b:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method
