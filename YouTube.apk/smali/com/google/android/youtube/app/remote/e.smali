.class public final Lcom/google/android/youtube/app/remote/e;
.super Lcom/google/android/youtube/app/remote/n;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/remote/c;


# instance fields
.field private final a:Lcom/google/android/youtube/app/remote/AtHomeConnection;

.field private final b:Landroid/support/place/rpc/RpcErrorHandler;

.field private final c:Lcom/google/android/youtube/athome/app/common/l;

.field private final d:Lcom/google/android/youtube/athome/app/common/j;

.field private final e:Lcom/google/android/youtube/athome/app/common/k;

.field private final f:Landroid/os/Handler;

.field private g:Lcom/google/android/youtube/athome/app/common/h;

.field private h:Lcom/google/android/youtube/app/remote/m;

.field private i:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

.field private j:Ljava/lang/String;

.field private k:I

.field private l:Lcom/google/android/youtube/app/remote/am;

.field private m:I

.field private n:Z

.field private o:Z

.field private p:I

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Lcom/google/android/youtube/core/model/SubtitleTrack;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/app/remote/AtHomeConnection;)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/remote/n;-><init>(Landroid/content/Context;)V

    const-string v0, "connection cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/remote/AtHomeConnection;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/e;->a:Lcom/google/android/youtube/app/remote/AtHomeConnection;

    new-instance v0, Lcom/google/android/youtube/app/remote/l;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/remote/l;-><init>(Lcom/google/android/youtube/app/remote/e;B)V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/e;->b:Landroid/support/place/rpc/RpcErrorHandler;

    sget-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->UNCONFIRMED:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/e;->i:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    new-instance v0, Lcom/google/android/youtube/app/remote/f;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/remote/f;-><init>(Lcom/google/android/youtube/app/remote/e;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/e;->f:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/youtube/app/remote/g;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/remote/g;-><init>(Lcom/google/android/youtube/app/remote/e;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/e;->d:Lcom/google/android/youtube/athome/app/common/j;

    new-instance v0, Lcom/google/android/youtube/app/remote/h;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/remote/h;-><init>(Lcom/google/android/youtube/app/remote/e;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/e;->c:Lcom/google/android/youtube/athome/app/common/l;

    new-instance v0, Lcom/google/android/youtube/app/remote/i;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/remote/i;-><init>(Lcom/google/android/youtube/app/remote/e;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/e;->e:Lcom/google/android/youtube/athome/app/common/k;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/e;)I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/app/remote/e;->m:I

    return v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/e;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/youtube/app/remote/e;->m:I

    return p1
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/e;Lcom/google/android/youtube/athome/app/common/AtHomePlayerState;)V
    .locals 5

    iget-object v0, p1, Lcom/google/android/youtube/athome/app/common/AtHomePlayerState;->videoId:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/e;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    if-nez v0, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/e;->j:Ljava/lang/String;

    if-eqz v1, :cond_2

    :cond_1
    iput-object v0, p0, Lcom/google/android/youtube/app/remote/e;->j:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->j:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/remote/e;->e(Ljava/lang/String;)V

    :cond_2
    iget v0, p1, Lcom/google/android/youtube/athome/app/common/AtHomePlayerState;->state:I

    packed-switch v0, :pswitch_data_0

    sget-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->UNCONFIRMED:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    :cond_3
    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/app/remote/e;->i:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-eq v0, v1, :cond_4

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/e;->i:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->i:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/e;->j:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/app/remote/e;->a(Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;Ljava/lang/String;)V

    :cond_4
    iget-boolean v0, p0, Lcom/google/android/youtube/app/remote/e;->r:Z

    iget-boolean v1, p1, Lcom/google/android/youtube/athome/app/common/AtHomePlayerState;->subtitlesEnabled:Z

    if-eq v0, v1, :cond_5

    iget-boolean v0, p1, Lcom/google/android/youtube/athome/app/common/AtHomePlayerState;->subtitlesEnabled:Z

    iput-boolean v0, p0, Lcom/google/android/youtube/app/remote/e;->r:Z

    iget-boolean v0, p0, Lcom/google/android/youtube/app/remote/e;->r:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->t:Lcom/google/android/youtube/core/model/SubtitleTrack;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/remote/e;->b(Lcom/google/android/youtube/core/model/SubtitleTrack;)V

    :cond_5
    return-void

    :pswitch_0
    sget-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->PLAYING:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/youtube/app/remote/e;->l:Lcom/google/android/youtube/app/remote/am;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->PAUSED:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->ENDED:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    goto :goto_0

    :pswitch_3
    sget-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->ENDED:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    goto :goto_0

    :pswitch_4
    sget-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->BUFFERING:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    goto :goto_0

    :pswitch_5
    sget-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->ADVERTISEMENT:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    iget-object v1, p1, Lcom/google/android/youtube/athome/app/common/AtHomePlayerState;->ad:Lcom/google/android/youtube/athome/app/common/a;

    if-eqz v1, :cond_3

    iget-object v1, p1, Lcom/google/android/youtube/athome/app/common/AtHomePlayerState;->ad:Lcom/google/android/youtube/athome/app/common/a;

    new-instance v2, Lcom/google/android/youtube/app/remote/am;

    iget-object v3, v1, Lcom/google/android/youtube/athome/app/common/a;->a:Ljava/lang/String;

    iget-object v4, v1, Lcom/google/android/youtube/athome/app/common/a;->b:Landroid/net/Uri;

    iget-boolean v1, v1, Lcom/google/android/youtube/athome/app/common/a;->c:Z

    invoke-direct {v2, v3, v4, v1}, Lcom/google/android/youtube/app/remote/am;-><init>(Ljava/lang/String;Landroid/net/Uri;Z)V

    iput-object v2, p0, Lcom/google/android/youtube/app/remote/e;->l:Lcom/google/android/youtube/app/remote/am;

    goto :goto_0

    :pswitch_6
    sget-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->ERROR:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_6
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private a(Lcom/google/android/youtube/app/remote/m;)V
    .locals 6

    const/4 v5, 0x4

    const/4 v2, 0x1

    const/4 v3, 0x0

    sget-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$State;->CONNECTING:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/remote/e;->a(Lcom/google/android/youtube/app/remote/RemoteControl$State;)V

    iput-object p1, p0, Lcom/google/android/youtube/app/remote/e;->h:Lcom/google/android/youtube/app/remote/m;

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->a:Lcom/google/android/youtube/app/remote/AtHomeConnection;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/AtHomeConnection;->a()Lcom/google/android/youtube/app/remote/AtHomeConnection$ConnectionState;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/AtHomeConnection$ConnectionState;->DISCONNECTED:Lcom/google/android/youtube/app/remote/AtHomeConnection$ConnectionState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->a:Lcom/google/android/youtube/app/remote/AtHomeConnection;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/AtHomeConnection;->a()Lcom/google/android/youtube/app/remote/AtHomeConnection$ConnectionState;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/AtHomeConnection$ConnectionState;->CONNECTING_TO_BROKER:Lcom/google/android/youtube/app/remote/AtHomeConnection$ConnectionState;

    if-ne v0, v1, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->f:Landroid/os/Handler;

    const-wide/16 v1, 0x2710

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/youtube/app/remote/m;->b()Landroid/support/place/connector/ConnectorInfo;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/athome/common/d;

    invoke-virtual {v0}, Landroid/support/place/connector/ConnectorInfo;->getExtras()Landroid/support/place/rpc/RpcData;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/youtube/athome/common/d;-><init>(Landroid/support/place/rpc/RpcData;)V

    const-string v0, "versionInfo"

    sget-object v4, Lcom/google/android/youtube/athome/common/e;->e:Landroid/support/place/rpc/Flattenable$Creator;

    invoke-virtual {v1, v0, v4}, Lcom/google/android/youtube/athome/common/d;->a(Ljava/lang/String;Landroid/support/place/rpc/Flattenable$Creator;)Landroid/support/place/rpc/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/athome/common/e;

    if-eqz v0, :cond_7

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Version information: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    iget v1, v0, Lcom/google/android/youtube/athome/common/e;->b:I

    if-gtz v1, :cond_3

    sget-object v0, Lcom/google/android/youtube/app/remote/d;->b:Lcom/google/android/youtube/app/remote/an;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/remote/e;->a(Lcom/google/android/youtube/app/remote/an;)V

    goto :goto_0

    :cond_3
    iget v1, v0, Lcom/google/android/youtube/athome/common/e;->a:I

    if-le v1, v5, :cond_4

    sget-object v0, Lcom/google/android/youtube/app/remote/d;->a:Lcom/google/android/youtube/app/remote/an;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/remote/e;->a(Lcom/google/android/youtube/app/remote/an;)V

    goto :goto_0

    :cond_4
    iget v1, v0, Lcom/google/android/youtube/athome/common/e;->b:I

    const/4 v4, 0x2

    if-lt v1, v4, :cond_5

    move v1, v2

    :goto_1
    iput-boolean v1, p0, Lcom/google/android/youtube/app/remote/e;->q:Z

    iget v0, v0, Lcom/google/android/youtube/athome/common/e;->b:I

    if-lt v0, v5, :cond_6

    :goto_2
    iput-boolean v2, p0, Lcom/google/android/youtube/app/remote/e;->o:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->a:Lcom/google/android/youtube/app/remote/AtHomeConnection;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/AtHomeConnection;->b()Landroid/support/place/connector/Broker;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/app/remote/m;->a(Landroid/support/place/connector/Broker;)Lcom/google/android/youtube/athome/app/common/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/e;->g:Lcom/google/android/youtube/athome/app/common/h;

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->g:Lcom/google/android/youtube/athome/app/common/h;

    new-instance v1, Lcom/google/android/youtube/app/remote/k;

    invoke-direct {v1, p0, v3}, Lcom/google/android/youtube/app/remote/k;-><init>(Lcom/google/android/youtube/app/remote/e;B)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/athome/app/common/h;->a(Lcom/google/android/youtube/athome/app/common/i;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->g:Lcom/google/android/youtube/athome/app/common/h;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/e;->c:Lcom/google/android/youtube/athome/app/common/l;

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/e;->b:Landroid/support/place/rpc/RpcErrorHandler;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/athome/app/common/h;->a(Lcom/google/android/youtube/athome/app/common/l;Landroid/support/place/rpc/RpcErrorHandler;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->g:Lcom/google/android/youtube/athome/app/common/h;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/e;->d:Lcom/google/android/youtube/athome/app/common/j;

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/e;->b:Landroid/support/place/rpc/RpcErrorHandler;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/athome/app/common/h;->a(Lcom/google/android/youtube/athome/app/common/j;Landroid/support/place/rpc/RpcErrorHandler;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->g:Lcom/google/android/youtube/athome/app/common/h;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/e;->e:Lcom/google/android/youtube/athome/app/common/k;

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/e;->b:Landroid/support/place/rpc/RpcErrorHandler;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/athome/app/common/h;->a(Lcom/google/android/youtube/athome/app/common/k;Landroid/support/place/rpc/RpcErrorHandler;)V

    iget-boolean v0, p0, Lcom/google/android/youtube/app/remote/e;->n:Z

    if-eqz v0, :cond_1

    iput-boolean v3, p0, Lcom/google/android/youtube/app/remote/e;->n:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->j:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/remote/e;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    move v1, v3

    goto :goto_1

    :cond_6
    move v2, v3

    goto :goto_2

    :cond_7
    const-string v0, "Cannot retrieve version info from the connector"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/youtube/app/remote/d;->b:Lcom/google/android/youtube/app/remote/an;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/remote/e;->a(Lcom/google/android/youtube/app/remote/an;)V

    goto/16 :goto_0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/remote/e;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/youtube/app/remote/e;->k:I

    return p1
.end method

.method static synthetic b(Lcom/google/android/youtube/app/remote/e;)Lcom/google/android/youtube/app/remote/m;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->h:Lcom/google/android/youtube/app/remote/m;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/remote/e;)Landroid/support/place/rpc/RpcErrorHandler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->b:Landroid/support/place/rpc/RpcErrorHandler;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/remote/e;)Lcom/google/android/youtube/athome/app/common/l;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->c:Lcom/google/android/youtube/athome/app/common/l;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/app/remote/e;)Lcom/google/android/youtube/athome/app/common/k;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->e:Lcom/google/android/youtube/athome/app/common/k;

    return-object v0
.end method


# virtual methods
.method protected final a()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/youtube/app/remote/e;->v()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->SLEEP:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/remote/e;->v()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->CONNECTING:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-ne v0, v1, :cond_1

    :cond_0
    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    sget-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$State;->CONNECTING:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/remote/e;->a(Lcom/google/android/youtube/app/remote/RemoteControl$State;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->f:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->a:Lcom/google/android/youtube/app/remote/AtHomeConnection;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/AtHomeConnection;->a()Lcom/google/android/youtube/app/remote/AtHomeConnection$ConnectionState;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/remote/e;->a(Lcom/google/android/youtube/app/remote/AtHomeConnection$ConnectionState;)V

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/youtube/app/remote/e;->s:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->a:Lcom/google/android/youtube/app/remote/AtHomeConnection;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/remote/AtHomeConnection;->a(Lcom/google/android/youtube/app/remote/c;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/remote/e;->s:Z

    :cond_2
    return-void
.end method

.method public final a(I)V
    .locals 3

    const/16 v0, 0x64

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/youtube/app/remote/e;->m:I

    add-int/2addr v2, p1

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/e;->g:Lcom/google/android/youtube/athome/app/common/h;

    if-eqz v1, :cond_0

    iput v0, p0, Lcom/google/android/youtube/app/remote/e;->m:I

    mul-int/lit8 v0, v0, 0x64

    div-int/lit8 v0, v0, 0x64

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/e;->g:Lcom/google/android/youtube/athome/app/common/h;

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/e;->b:Landroid/support/place/rpc/RpcErrorHandler;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/youtube/athome/app/common/h;->b(ILandroid/support/place/rpc/RpcErrorHandler;)V

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/remote/AtHomeConnection$ConnectionState;)V
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/google/android/youtube/app/remote/j;->a:[I

    invoke-virtual {p1}, Lcom/google/android/youtube/app/remote/AtHomeConnection$ConnectionState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->h:Lcom/google/android/youtube/app/remote/m;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    sget-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$State;->CONNECTING:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/remote/e;->a(Lcom/google/android/youtube/app/remote/RemoteControl$State;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->f:Landroid/os/Handler;

    const-wide/16 v1, 0x2710

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/youtube/app/remote/e;->v()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->CONNECTING:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-eq v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/youtube/app/remote/e;->v()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->ERROR:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/remote/e;->w()Lcom/google/android/youtube/app/remote/an;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/ao;->b:Lcom/google/android/youtube/app/remote/an;

    if-ne v0, v1, :cond_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->h:Lcom/google/android/youtube/app/remote/m;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->f:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->h:Lcom/google/android/youtube/app/remote/m;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/remote/e;->a(Lcom/google/android/youtube/app/remote/m;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final synthetic a(Lcom/google/android/youtube/app/remote/bb;Ljava/lang/String;J)V
    .locals 1

    check-cast p1, Lcom/google/android/youtube/app/remote/m;

    const-string v0, "screen cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/remote/e;->n:Z

    long-to-int v0, p3

    iput v0, p0, Lcom/google/android/youtube/app/remote/e;->p:I

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/remote/e;->a(Lcom/google/android/youtube/app/remote/m;)V

    invoke-virtual {p0, p2}, Lcom/google/android/youtube/app/remote/e;->d(Ljava/lang/String;)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/SubtitleTrack;)V
    .locals 3

    iput-object p1, p0, Lcom/google/android/youtube/app/remote/e;->t:Lcom/google/android/youtube/core/model/SubtitleTrack;

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->g:Lcom/google/android/youtube/athome/app/common/h;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/google/android/youtube/athome/app/common/g;

    invoke-direct {v1}, Lcom/google/android/youtube/athome/app/common/g;-><init>()V

    iget-object v0, p1, Lcom/google/android/youtube/core/model/SubtitleTrack;->videoId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p1, Lcom/google/android/youtube/core/model/SubtitleTrack;->videoId:Ljava/lang/String;

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/youtube/athome/app/common/g;->a(Ljava/lang/String;)Lcom/google/android/youtube/athome/app/common/g;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/youtube/core/model/SubtitleTrack;->languageCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/athome/app/common/g;->b(Ljava/lang/String;)Lcom/google/android/youtube/athome/app/common/g;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/youtube/core/model/SubtitleTrack;->trackName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/athome/app/common/g;->c(Ljava/lang/String;)Lcom/google/android/youtube/athome/app/common/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/athome/app/common/g;->a()Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/e;->g:Lcom/google/android/youtube/athome/app/common/h;

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/e;->b:Landroid/support/place/rpc/RpcErrorHandler;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/youtube/athome/app/common/h;->a(Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;Landroid/support/place/rpc/RpcErrorHandler;)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->j:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/app/remote/e;->d(Ljava/lang/String;)V

    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 0

    return-void
.end method

.method public final a(Z)V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/remote/e;->d(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$State;->OFFLINE:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/remote/e;->a(Lcom/google/android/youtube/app/remote/RemoteControl$State;)V

    iput-object v1, p0, Lcom/google/android/youtube/app/remote/e;->h:Lcom/google/android/youtube/app/remote/m;

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->g:Lcom/google/android/youtube/athome/app/common/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->g:Lcom/google/android/youtube/athome/app/common/h;

    invoke-virtual {v0}, Lcom/google/android/youtube/athome/app/common/h;->a()V

    iput-object v1, p0, Lcom/google/android/youtube/app/remote/e;->g:Lcom/google/android/youtube/athome/app/common/h;

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/remote/e;->q:Z

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Video;)Z
    .locals 2

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->privacy:Lcom/google/android/youtube/core/model/Video$Privacy;

    sget-object v1, Lcom/google/android/youtube/core/model/Video$Privacy;->PRIVATE:Lcom/google/android/youtube/core/model/Video$Privacy;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/google/android/youtube/core/model/Video;)Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;
    .locals 2

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->privacy:Lcom/google/android/youtube/core/model/Video$Privacy;

    sget-object v1, Lcom/google/android/youtube/core/model/Video$Privacy;->PRIVATE:Lcom/google/android/youtube/core/model/Video$Privacy;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;->PRIVATE:Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;->UNSPECIFIED:Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

    goto :goto_0
.end method

.method protected final b()V
    .locals 3

    const/4 v2, 0x0

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->a:Lcom/google/android/youtube/app/remote/AtHomeConnection;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/remote/AtHomeConnection;->b(Lcom/google/android/youtube/app/remote/c;)V

    iput-boolean v2, p0, Lcom/google/android/youtube/app/remote/e;->s:Z

    invoke-virtual {p0}, Lcom/google/android/youtube/app/remote/e;->v()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->OFFLINE:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-eq v0, v1, :cond_0

    sget-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$State;->SLEEP:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/remote/e;->a(Lcom/google/android/youtube/app/remote/RemoteControl$State;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->g:Lcom/google/android/youtube/athome/app/common/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->g:Lcom/google/android/youtube/athome/app/common/h;

    invoke-virtual {v0}, Lcom/google/android/youtube/athome/app/common/h;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/e;->g:Lcom/google/android/youtube/athome/app/common/h;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->f:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    return-void
.end method

.method public final b(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->g:Lcom/google/android/youtube/athome/app/common/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->g:Lcom/google/android/youtube/athome/app/common/h;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/e;->b:Landroid/support/place/rpc/RpcErrorHandler;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/youtube/athome/app/common/h;->a(ILandroid/support/place/rpc/RpcErrorHandler;)V

    iput p1, p0, Lcom/google/android/youtube/app/remote/e;->k:I

    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->g:Lcom/google/android/youtube/athome/app/common/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->g:Lcom/google/android/youtube/athome/app/common/h;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/e;->b:Landroid/support/place/rpc/RpcErrorHandler;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/athome/app/common/h;->a(Landroid/support/place/rpc/RpcErrorHandler;)V

    :cond_0
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final d()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->g:Lcom/google/android/youtube/athome/app/common/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->g:Lcom/google/android/youtube/athome/app/common/h;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/e;->b:Landroid/support/place/rpc/RpcErrorHandler;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/athome/app/common/h;->b(Landroid/support/place/rpc/RpcErrorHandler;)V

    :cond_0
    return-void
.end method

.method protected final d(Ljava/lang/String;)V
    .locals 3

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->g:Lcom/google/android/youtube/athome/app/common/h;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->UNCONFIRMED:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/e;->i:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->i:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/e;->j:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/app/remote/e;->a(Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/youtube/app/remote/e;->o:Z

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/android/youtube/athome/app/common/PlayRequest;

    iget v1, p0, Lcom/google/android/youtube/app/remote/e;->p:I

    invoke-direct {v0, p1, v1}, Lcom/google/android/youtube/athome/app/common/PlayRequest;-><init>(Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/e;->g:Lcom/google/android/youtube/athome/app/common/h;

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/e;->b:Landroid/support/place/rpc/RpcErrorHandler;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/youtube/athome/app/common/h;->a(Lcom/google/android/youtube/athome/app/common/PlayRequest;Landroid/support/place/rpc/RpcErrorHandler;)V

    :goto_0
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/app/remote/e;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/youtube/app/remote/e;->p:I

    iput v0, p0, Lcom/google/android/youtube/app/remote/e;->k:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/youtube/app/remote/e;->p:I

    :cond_0
    iput-object p1, p0, Lcom/google/android/youtube/app/remote/e;->j:Ljava/lang/String;

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->g:Lcom/google/android/youtube/athome/app/common/h;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/e;->b:Landroid/support/place/rpc/RpcErrorHandler;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/youtube/athome/app/common/h;->a(Ljava/lang/String;Landroid/support/place/rpc/RpcErrorHandler;)V

    goto :goto_0
.end method

.method public final e()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->g:Lcom/google/android/youtube/athome/app/common/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->g:Lcom/google/android/youtube/athome/app/common/h;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/e;->b:Landroid/support/place/rpc/RpcErrorHandler;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/athome/app/common/h;->c(Landroid/support/place/rpc/RpcErrorHandler;)V

    :cond_0
    return-void
.end method

.method public final f()Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->i:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final h()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/app/remote/e;->m:I

    return v0
.end method

.method public final i()D
    .locals 2

    iget v0, p0, Lcom/google/android/youtube/app/remote/e;->k:I

    int-to-double v0, v0

    return-wide v0
.end method

.method public final j()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->j:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final k()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final l()Lcom/google/android/youtube/app/remote/am;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->l:Lcom/google/android/youtube/app/remote/am;

    return-object v0
.end method

.method public final m()I
    .locals 1

    const v0, 0x7f0b025d

    return v0
.end method

.method public final n()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/remote/e;->q:Z

    return v0
.end method

.method public final o()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final p()V
    .locals 0

    return-void
.end method

.method public final q()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final r()V
    .locals 0

    return-void
.end method

.method public final s()Lcom/google/android/youtube/core/model/SubtitleTrack;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->t:Lcom/google/android/youtube/core/model/SubtitleTrack;

    return-object v0
.end method

.method public final bridge synthetic t()Lcom/google/android/youtube/app/remote/bb;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/e;->h:Lcom/google/android/youtube/app/remote/m;

    return-object v0
.end method
