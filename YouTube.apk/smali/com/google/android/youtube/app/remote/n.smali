.class public abstract Lcom/google/android/youtube/app/remote/n;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/remote/RemoteControl;


# instance fields
.field private final a:Ljava/util/Set;

.field private final b:Ljava/util/Set;

.field private final c:Ljava/util/Set;

.field private final d:Landroid/os/Handler;

.field private e:Lcom/google/android/youtube/app/remote/RemoteControl$State;

.field private f:Lcom/google/android/youtube/app/remote/an;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/n;->a:Ljava/util/Set;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/n;->b:Ljava/util/Set;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/n;->c:Ljava/util/Set;

    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/n;->d:Landroid/os/Handler;

    sget-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$State;->OFFLINE:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/n;->e:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/n;)Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/n;->a:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/remote/n;)Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/n;->c:Ljava/util/Set;

    return-object v0
.end method

.method private b(Lcom/google/android/youtube/app/remote/RemoteControl$State;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/n;->d:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/app/remote/p;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/app/remote/p;-><init>(Lcom/google/android/youtube/app/remote/n;Lcom/google/android/youtube/app/remote/RemoteControl$State;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method


# virtual methods
.method protected abstract a()V
.end method

.method protected final a(Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/n;->d:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/app/remote/r;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/youtube/app/remote/r;-><init>(Lcom/google/android/youtube/app/remote/n;Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected final a(Lcom/google/android/youtube/app/remote/RemoteControl$State;)V
    .locals 2

    sget-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$State;->ERROR:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-eq p1, v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    const-string v1, "use toErrorState for ERROR state"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/n;->e:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    iput-object p1, p0, Lcom/google/android/youtube/app/remote/n;->e:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/n;->e:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/remote/n;->b(Lcom/google/android/youtube/app/remote/RemoteControl$State;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/n;->e:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->SLEEP:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/n;->e:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->OFFLINE:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-ne v0, v1, :cond_2

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/remote/n;->d(Ljava/lang/String;)V

    :cond_2
    return-void

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final declared-synchronized a(Lcom/google/android/youtube/app/remote/al;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    const-string v0, "listener can not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/n;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final a(Lcom/google/android/youtube/app/remote/an;)V
    .locals 2

    const-string v0, "error can not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/n;->e:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->OFFLINE:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-ne v0, v1, :cond_0

    const-string v0, "Remote control trying to move to ERROR state while OFFLINE"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/google/android/youtube/app/remote/n;->f:Lcom/google/android/youtube/app/remote/an;

    sget-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$State;->ERROR:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/n;->e:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/n;->e:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/remote/n;->b(Lcom/google/android/youtube/app/remote/RemoteControl$State;)V

    goto :goto_0
.end method

.method public final declared-synchronized a(Lcom/google/android/youtube/app/remote/ap;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    const-string v0, "listener can not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/n;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected abstract b()V
.end method

.method public final declared-synchronized b(Lcom/google/android/youtube/app/remote/al;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    const-string v0, "listener can not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/n;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/n;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/n;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/remote/n;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Lcom/google/android/youtube/app/remote/ap;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    const-string v0, "listener can not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/n;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final b(Lcom/google/android/youtube/core/model/SubtitleTrack;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/n;->d:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/app/remote/u;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/app/remote/u;-><init>(Lcom/google/android/youtube/app/remote/n;Lcom/google/android/youtube/core/model/SubtitleTrack;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected final b(Ljava/util/List;)V
    .locals 3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/n;->d:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/youtube/app/remote/q;

    invoke-direct {v2, p0, v0}, Lcom/google/android/youtube/app/remote/q;-><init>(Lcom/google/android/youtube/app/remote/n;Ljava/util/List;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected final c(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/n;->d:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/app/remote/s;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/app/remote/s;-><init>(Lcom/google/android/youtube/app/remote/n;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final declared-synchronized c(Lcom/google/android/youtube/app/remote/al;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    const-string v0, "listener can not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/n;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "listener not added, cannot be made active"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/n;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/n;->b:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/remote/n;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected abstract d(Ljava/lang/String;)V
.end method

.method protected final e(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/n;->d:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/app/remote/o;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/app/remote/o;-><init>(Lcom/google/android/youtube/app/remote/n;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected final u()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/n;->d:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/app/remote/t;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/remote/t;-><init>(Lcom/google/android/youtube/app/remote/n;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final v()Lcom/google/android/youtube/app/remote/RemoteControl$State;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/n;->e:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    return-object v0
.end method

.method public final w()Lcom/google/android/youtube/app/remote/an;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/n;->f:Lcom/google/android/youtube/app/remote/an;

    return-object v0
.end method
