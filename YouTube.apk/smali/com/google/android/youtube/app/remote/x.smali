.class final Lcom/google/android/youtube/app/remote/x;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/google/android/youtube/core/async/n;

.field final synthetic c:Lcom/google/android/youtube/app/remote/v;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/remote/v;Ljava/lang/String;Lcom/google/android/youtube/core/async/n;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/remote/x;->c:Lcom/google/android/youtube/app/remote/v;

    iput-object p2, p0, Lcom/google/android/youtube/app/remote/x;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/youtube/app/remote/x;->b:Lcom/google/android/youtube/core/async/n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    check-cast p1, Lcom/google/android/ytremote/model/PairingCode;

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/x;->b:Lcom/google/android/youtube/core/async/n;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/async/n;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    check-cast p1, Lcom/google/android/ytremote/model/PairingCode;

    check-cast p2, Lcom/google/android/ytremote/model/CloudScreen;

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/x;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/x;->a:Ljava/lang/String;

    invoke-virtual {p2, v0}, Lcom/google/android/ytremote/model/CloudScreen;->withName(Ljava/lang/String;)Lcom/google/android/ytremote/model/CloudScreen;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/app/remote/x;->c:Lcom/google/android/youtube/app/remote/v;

    invoke-static {v1, v0}, Lcom/google/android/youtube/app/remote/v;->b(Lcom/google/android/youtube/app/remote/v;Lcom/google/android/ytremote/model/CloudScreen;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/x;->b:Lcom/google/android/youtube/core/async/n;

    new-instance v2, Lcom/google/android/youtube/app/remote/bp;

    invoke-direct {v2, v0}, Lcom/google/android/youtube/app/remote/bp;-><init>(Lcom/google/android/ytremote/model/CloudScreen;)V

    invoke-interface {v1, p1, v2}, Lcom/google/android/youtube/core/async/n;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/x;->c:Lcom/google/android/youtube/app/remote/v;

    invoke-static {v0, p2}, Lcom/google/android/youtube/app/remote/v;->a(Lcom/google/android/youtube/app/remote/v;Lcom/google/android/ytremote/model/CloudScreen;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/google/android/ytremote/model/CloudScreen;->withName(Ljava/lang/String;)Lcom/google/android/ytremote/model/CloudScreen;

    move-result-object v0

    goto :goto_0
.end method
