.class public Lcom/google/android/youtube/app/ui/DefaultSlider;
.super Landroid/view/ViewGroup;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/ui/Slider;


# instance fields
.field protected final a:Landroid/app/Activity;

.field private b:I

.field private final c:Landroid/util/DisplayMetrics;

.field private d:Z

.field private e:I

.field private f:I

.field private g:[Lcom/google/android/youtube/app/ui/at;

.field private h:[Landroid/view/View;

.field private i:[Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

.field private j:[I

.field private k:I

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:F

.field private p:I

.field private q:Landroid/widget/Scroller;

.field private r:Z

.field private s:Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    const-string v0, "activity must not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {p0, v0}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    iput-boolean v2, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->l:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->p:I

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->a:Landroid/app/Activity;

    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->c:Landroid/util/DisplayMetrics;

    invoke-virtual {p1}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->c:Landroid/util/DisplayMetrics;

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    new-instance v0, Landroid/widget/Scroller;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-direct {v0, p1, v1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->q:Landroid/widget/Scroller;

    new-instance v0, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;

    invoke-direct {v0, p1, p0}, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/app/ui/DefaultSlider;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->s:Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;

    sget-object v0, Lcom/google/android/youtube/app/ui/Slider$Orientation;->HORIZONTAL:Lcom/google/android/youtube/app/ui/Slider$Orientation;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->setOrientation(Lcom/google/android/youtube/app/ui/Slider$Orientation;)V

    new-array v0, v4, [Lcom/google/android/youtube/app/ui/at;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/at;

    iput v3, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    new-array v0, v4, [Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->i:[Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->i:[Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    sget-object v1, Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;->DISPLACE:Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    aput-object v1, v0, v3

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->i:[Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    sget-object v1, Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;->DISPLACE:Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    aput-object v1, v0, v2

    new-array v0, v4, [I

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->j:[I

    new-array v0, v4, [Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    new-instance v1, Landroid/view/View;

    invoke-direct {v1, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    aput-object v1, v0, v3

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    new-instance v1, Landroid/view/View;

    invoke-direct {v1, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    aput-object v1, v0, v2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    aget-object v0, v0, v3

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    aget-object v0, v0, v2

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->addView(Landroid/view/View;)V

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/app/ui/DefaultSlider;->setChildrenDrawingOrderEnabled(Z)V

    return-void
.end method

.method private b(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->r:Z

    if-eq v0, p1, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->a()V

    :cond_0
    iput-boolean p1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->r:Z

    return-void
.end method

.method private c(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->q:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->q:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->g()I

    move-result v0

    mul-int/2addr v0, p1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->e(I)V

    iget v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    if-eq v0, p1, :cond_1

    iput p1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/ui/DefaultSlider;->d(I)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->invalidate()V

    return-void
.end method

.method private d(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/at;

    aget-object v0, v0, p1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/at;

    aget-object v0, v0, p1

    invoke-interface {v0}, Lcom/google/android/youtube/app/ui/at;->j()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/at;

    rsub-int/lit8 v1, p1, 0x1

    aget-object v0, v0, v1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/at;

    rsub-int/lit8 v1, p1, 0x1

    aget-object v0, v0, v1

    invoke-interface {v0}, Lcom/google/android/youtube/app/ui/at;->k()V

    :cond_1
    return-void
.end method

.method private e(I)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/app/ui/DefaultSlider;->b(I)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->requestLayout()V

    return-void
.end method

.method private f(I)V
    .locals 6

    const/4 v2, 0x0

    const/4 v0, 0x1

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->p:I

    iget-boolean v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->l:Z

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    sub-int v1, v0, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->getFocusedChild()Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_1

    if-eqz v1, :cond_1

    iget-object v4, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    iget v5, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    aget-object v4, v4, v5

    if-ne v3, v4, :cond_1

    invoke-virtual {v3}, Landroid/view/View;->clearFocus()V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->g()I

    move-result v3

    mul-int/2addr v0, v3

    iget v3, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->b:I

    sub-int v3, v0, v3

    mul-int/lit16 v5, v1, 0xc8

    if-nez v5, :cond_2

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v5

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->q:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->q:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    :cond_3
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->q:Landroid/widget/Scroller;

    iget v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->b:I

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->invalidate()V

    goto :goto_0
.end method

.method private i()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->g()I

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x2

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/at;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    iget v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->b:I

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->b:I

    goto :goto_1

    :cond_2
    return-void
.end method

.method private j()V
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    aget-object v1, v1, v0

    iget v2, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->b:I

    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->g()I

    move-result v3

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->n:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->j:[I

    aget v2, v2, v0

    if-nez v2, :cond_0

    const/16 v0, 0x8

    :cond_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/app/ui/Slider$Order;)Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->i:[Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/ui/Slider$Order;->ordinal()I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method protected a()V
    .locals 0

    return-void
.end method

.method public final a(F)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    const/high16 v0, 0x3f800000

    cmpg-float v0, p1, v0

    if-gez v0, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "split must be in the range (0,1)"

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/s;->b(ZLjava/lang/Object;)V

    iput p1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->o:F

    iput-boolean v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->n:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/at;

    array-length v1, v0

    :goto_1
    if-ge v2, v1, :cond_2

    aget-object v3, v0, v2

    if-eqz v3, :cond_0

    invoke-interface {v3}, Lcom/google/android/youtube/app/ui/at;->l()V

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    return-void
.end method

.method protected a(I)V
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->b:I

    add-int/2addr v0, p1

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->b(I)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->requestLayout()V

    return-void
.end method

.method public final a(Landroid/content/res/Configuration;)V
    .locals 5

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/at;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    invoke-interface {v1, p1}, Lcom/google/android/youtube/app/ui/at;->a(Landroid/content/res/Configuration;)V

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->j:[I

    invoke-interface {v1}, Lcom/google/android/youtube/app/ui/at;->e()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->c:Landroid/util/DisplayMetrics;

    iget v4, v4, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    aput v3, v2, v0

    invoke-interface {v1}, Lcom/google/android/youtube/app/ui/at;->o()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    aget-object v2, v2, v0

    if-eq v1, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    aget-object v2, v2, v0

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/app/ui/DefaultSlider;->removeView(Landroid/view/View;)V

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    aput-object v1, v2, v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    aget-object v1, v1, v0

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/ui/DefaultSlider;->addView(Landroid/view/View;)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/compat/m;)V
    .locals 4

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/at;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    if-eqz v3, :cond_0

    invoke-interface {v3, p1}, Lcom/google/android/youtube/app/ui/at;->a(Lcom/google/android/youtube/app/compat/m;)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/ui/Slider$Order;Z)V
    .locals 1

    const-string v0, "order may not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->n:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p1}, Lcom/google/android/youtube/app/ui/Slider$Order;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->f(I)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/youtube/app/ui/Slider$Order;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->c(I)V

    goto :goto_0
.end method

.method protected a(Z)V
    .locals 0

    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/compat/t;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/at;

    const/4 v0, 0x0

    return v0
.end method

.method public addFocusables(Ljava/util/ArrayList;II)V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    iget v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    aget-object v0, v0, v1

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;I)V

    iget v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->e:I

    if-ne p2, v0, :cond_1

    iget v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->f:I

    if-ne p2, v0, :cond_0

    iget v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    aget-object v0, v0, v2

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;I)V

    goto :goto_0
.end method

.method public addView(Landroid/view/View;)V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setClickable(Z)V

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method

.method protected final b(Lcom/google/android/youtube/app/ui/Slider$Order;)Landroid/view/View;
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/ui/Slider$Order;->ordinal()I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public final b()Lcom/google/android/youtube/app/ui/Slider$Orientation;
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->d:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/youtube/app/ui/Slider$Orientation;->VERTICAL:Lcom/google/android/youtube/app/ui/Slider$Orientation;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/youtube/app/ui/Slider$Orientation;->HORIZONTAL:Lcom/google/android/youtube/app/ui/Slider$Orientation;

    goto :goto_0
.end method

.method protected final b(I)V
    .locals 0

    iput p1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->b:I

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->j()V

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->i()V

    return-void
.end method

.method public final b(Lcom/google/android/youtube/app/compat/m;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/at;

    return-void
.end method

.method public final c()V
    .locals 4

    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->m:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/at;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    if-eqz v3, :cond_1

    invoke-interface {v3}, Lcom/google/android/youtube/app/ui/at;->h()V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->m:Z

    goto :goto_0
.end method

.method public computeScroll()V
    .locals 4

    const/4 v3, -0x1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->q:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->q:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->e(I)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->postInvalidate()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->p:I

    if-eq v0, v3, :cond_0

    const/4 v0, 0x0

    iget v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->p:I

    const/4 v2, 0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v3, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->p:I

    iget v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    if-eq v0, v1, :cond_0

    iput v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    iget v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->d(I)V

    goto :goto_0
.end method

.method public final d()V
    .locals 5

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->m:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/at;

    array-length v3, v2

    move v0, v1

    :goto_1
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    if-eqz v4, :cond_1

    invoke-interface {v4}, Lcom/google/android/youtube/app/ui/at;->c()V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iput-boolean v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->m:Z

    goto :goto_0
.end method

.method public dispatchUnhandledMove(Landroid/view/View;I)Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->e:I

    if-ne p2, v1, :cond_0

    iget v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    if-ne v1, v0, :cond_0

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/youtube/app/ui/DefaultSlider;->f(I)V

    :goto_0
    return v0

    :cond_0
    iget v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->f:I

    if-ne p2, v1, :cond_1

    iget v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    if-nez v1, :cond_1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->f(I)V

    goto :goto_0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->dispatchUnhandledMove(Landroid/view/View;I)Z

    move-result v0

    goto :goto_0
.end method

.method public final e()V
    .locals 4

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->n:Z

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/at;

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    if-eqz v3, :cond_0

    invoke-interface {v3}, Lcom/google/android/youtube/app/ui/at;->m()V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method protected final f()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    return v0
.end method

.method protected final g()I
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->d:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->getHeight()I

    move-result v0

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->j:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->j:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    sub-int/2addr v0, v1

    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->getWidth()I

    move-result v0

    goto :goto_0
.end method

.method protected getChildDrawingOrder(II)I
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->i:[Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    aget-object v1, v1, v0

    sget-object v2, Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;->OCCLUDE:Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x0

    :cond_0
    if-nez p2, :cond_1

    rsub-int/lit8 v0, v0, 0x1

    :cond_1
    return v0
.end method

.method protected final h()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->b:I

    return v0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->n:Z

    if-eqz v2, :cond_0

    :goto_0
    return v1

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    and-int/lit16 v2, v2, 0xff

    packed-switch v2, :pswitch_data_0

    :cond_1
    :goto_1
    :pswitch_0
    iget-boolean v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->r:Z

    goto :goto_0

    :pswitch_1
    iget-boolean v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->r:Z

    if-eqz v1, :cond_2

    move v1, v0

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->s:Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;

    invoke-virtual {v1, p1}, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->d(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->b(Z)V

    goto :goto_1

    :pswitch_2
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->s:Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;

    invoke-virtual {v2, p1}, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->b(Landroid/view/MotionEvent;)V

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->q:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->isFinished()Z

    move-result v2

    if-nez v2, :cond_3

    :goto_2
    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->b(Z)V

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    :pswitch_3
    invoke-direct {p0, v1}, Lcom/google/android/youtube/app/ui/DefaultSlider;->b(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->s:Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->a()V

    goto :goto_1

    :pswitch_4
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->s:Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->f(Landroid/view/MotionEvent;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 10

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->j:[I

    const/4 v1, 0x0

    aget v2, v0, v1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->j:[I

    const/4 v1, 0x1

    aget v3, v0, v1

    const/4 v0, 0x2

    new-array v4, v0, [I

    const/4 v0, 0x2

    new-array v5, v0, [I

    const/4 v0, 0x2

    new-array v6, v0, [I

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    aput v1, v4, v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    aput v1, v5, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    sub-int v1, p4, p2

    sub-int v0, p5, p3

    iget-boolean v7, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->d:Z

    if-eqz v7, :cond_1

    :goto_1
    iget-boolean v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->n:Z

    if-eqz v1, :cond_3

    const/4 v0, 0x0

    const/4 v1, 0x0

    aput v1, v6, v0

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->d:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    aget v0, v5, v0

    :goto_2
    aput v0, v6, v1

    :goto_3
    const/4 v0, 0x0

    :goto_4
    const/4 v1, 0x2

    if-ge v0, v1, :cond_7

    iget-boolean v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->d:Z

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    aget-object v1, v1, v0

    const/4 v2, 0x0

    aget v3, v6, v0

    aget v7, v4, v0

    aget v8, v6, v0

    aget v9, v5, v0

    add-int/2addr v8, v9

    invoke-virtual {v1, v2, v3, v7, v8}, Landroid/view/View;->layout(IIII)V

    :goto_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    aget v0, v4, v0

    goto :goto_2

    :cond_3
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->i:[Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    const/4 v7, 0x0

    aget-object v1, v1, v7

    sget-object v7, Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;->DISPLACE:Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    if-ne v1, v7, :cond_4

    const/4 v1, 0x0

    iget v7, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->b:I

    neg-int v7, v7

    aput v7, v6, v1

    :goto_6
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->i:[Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    const/4 v7, 0x1

    aget-object v1, v1, v7

    sget-object v7, Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;->OCCLUDE:Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    if-ne v1, v7, :cond_5

    const/4 v0, 0x1

    aput v2, v6, v0

    goto :goto_3

    :cond_4
    const/4 v1, 0x0

    const/4 v7, 0x0

    aput v7, v6, v1

    goto :goto_6

    :cond_5
    const/4 v1, 0x1

    sub-int/2addr v0, v3

    iget v2, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->b:I

    sub-int/2addr v0, v2

    aput v0, v6, v1

    goto :goto_3

    :cond_6
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    aget-object v1, v1, v0

    aget v2, v6, v0

    const/4 v3, 0x0

    aget v7, v6, v0

    aget v8, v4, v0

    add-int/2addr v7, v8

    aget v8, v5, v0

    invoke-virtual {v1, v2, v3, v7, v8}, Landroid/view/View;->layout(IIII)V

    goto :goto_5

    :cond_7
    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->l:Z

    if-eqz v0, :cond_8

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->l:Z

    iget v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->p:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_9

    iget v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->p:I

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->f(I)V

    :cond_8
    :goto_7
    return-void

    :cond_9
    iget v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->g()I

    move-result v1

    mul-int/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->e(I)V

    goto :goto_7
.end method

.method protected onMeasure(II)V
    .locals 12

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/high16 v9, 0x40000000

    const/4 v3, 0x0

    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    if-eq v0, v9, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Slider can only be used in EXACTLY mode."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    if-eq v0, v9, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Slider can only be used in EXACTLY mode."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->j:[I

    aget v4, v0, v3

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->j:[I

    aget v5, v0, v10

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->d:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    new-array v6, v11, [I

    iget-boolean v7, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->n:Z

    if-eqz v7, :cond_3

    int-to-float v4, v0

    iget v5, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->o:F

    mul-float/2addr v4, v5

    float-to-int v4, v4

    aput v4, v6, v3

    aget v4, v6, v3

    sub-int/2addr v0, v4

    aput v0, v6, v10

    :goto_1
    move v0, v3

    :goto_2
    if-ge v0, v11, :cond_7

    iget-boolean v3, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->d:Z

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    aget-object v3, v3, v0

    invoke-static {v2, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    aget v5, v6, v0

    invoke-static {v5, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/view/View;->measure(II)V

    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    iget-object v7, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->i:[Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    aget-object v7, v7, v3

    sget-object v8, Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;->RESIZE:Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    if-ne v7, v8, :cond_4

    sub-int v7, v0, v5

    iget v8, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->b:I

    sub-int/2addr v7, v8

    aput v7, v6, v3

    :goto_4
    iget-object v7, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->i:[Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    aget-object v7, v7, v10

    sget-object v8, Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;->RESIZE:Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    if-ne v7, v8, :cond_5

    iget v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->b:I

    add-int/2addr v0, v5

    aput v0, v6, v10

    goto :goto_1

    :cond_4
    sub-int v7, v0, v5

    aput v7, v6, v3

    goto :goto_4

    :cond_5
    sub-int/2addr v0, v4

    aput v0, v6, v10

    goto :goto_1

    :cond_6
    iget-object v3, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    aget-object v3, v3, v0

    aget v4, v6, v0

    invoke-static {v4, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-static {v1, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/view/View;->measure(II)V

    goto :goto_3

    :cond_7
    return-void
.end method

.method protected onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z
    .locals 2

    iget v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->p:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->p:I

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    aget-object v0, v1, v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v0

    :goto_1
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    check-cast p1, Lcom/google/android/youtube/app/ui/DefaultSliderSavedState;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/ui/DefaultSliderSavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget v0, p1, Lcom/google/android/youtube/app/ui/DefaultSliderSavedState;->expandedLayer:I

    iput v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    iget v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    :cond_1
    iget v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->c(I)V

    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    new-instance v0, Lcom/google/android/youtube/app/ui/DefaultSliderSavedState;

    invoke-super {p0}, Landroid/view/ViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/youtube/app/ui/DefaultSliderSavedState;-><init>(Landroid/os/Parcelable;)V

    iget v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    iput v1, v0, Lcom/google/android/youtube/app/ui/DefaultSliderSavedState;->expandedLayer:I

    return-object v0
.end method

.method protected onSizeChanged(IIII)V
    .locals 1

    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->onSizeChanged(IIII)V

    iget v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->c(I)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->s:Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->a(Landroid/view/MotionEvent;)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return v4

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->q:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->q:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->s:Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->b(Landroid/view/MotionEvent;)V

    goto :goto_0

    :pswitch_2
    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->r:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->s:Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->c(Landroid/view/MotionEvent;)I

    move-result v0

    if-gez v0, :cond_2

    iget v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->b:I

    if-lez v1, :cond_0

    iget v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->b:I

    neg-int v1, v1

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->a(I)V

    goto :goto_0

    :cond_2
    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->g()I

    move-result v1

    iget v2, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->b:I

    sub-int/2addr v1, v2

    if-lez v1, :cond_0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->a(I)V

    goto :goto_0

    :pswitch_3
    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->r:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->s:Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->e(Landroid/view/MotionEvent;)Lcom/google/android/youtube/app/ui/DefaultSliderDynamics$Fling;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/ui/ah;->a:[I

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics$Fling;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_1

    :cond_3
    :goto_1
    invoke-direct {p0, v3}, Lcom/google/android/youtube/app/ui/DefaultSlider;->b(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->s:Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->a()V

    goto :goto_0

    :pswitch_4
    invoke-direct {p0, v3}, Lcom/google/android/youtube/app/ui/DefaultSlider;->f(I)V

    goto :goto_1

    :pswitch_5
    invoke-direct {p0, v4}, Lcom/google/android/youtube/app/ui/DefaultSlider;->f(I)V

    goto :goto_1

    :pswitch_6
    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->g()I

    move-result v0

    iget v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->b:I

    div-int/lit8 v2, v0, 0x2

    add-int/2addr v1, v2

    div-int v0, v1, v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->f(I)V

    goto :goto_1

    :pswitch_7
    invoke-direct {p0, v3}, Lcom/google/android/youtube/app/ui/DefaultSlider;->b(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->s:Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->a()V

    goto :goto_0

    :pswitch_8
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->s:Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/DefaultSliderDynamics;->f(Landroid/view/MotionEvent;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_8
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public requestChildFocus(Landroid/view/View;Landroid/view/View;)V
    .locals 2

    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->requestChildFocus(Landroid/view/View;Landroid/view/View;)V

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/app/ui/DefaultSlider;->indexOfChild(Landroid/view/View;)I

    move-result v0

    if-ltz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->isInTouchMode()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->f(I)V

    :cond_0
    return-void
.end method

.method public requestChildRectangleOnScreen(Landroid/view/View;Landroid/graphics/Rect;Z)Z
    .locals 2

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/app/ui/DefaultSlider;->indexOfChild(Landroid/view/View;)I

    move-result v0

    iget v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->q:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->isFinished()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->f(I)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setCollapseStrategy(Lcom/google/android/youtube/app/ui/Slider$Order;Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;)V
    .locals 4

    const-string v0, "order may not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "strategy may not be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/ui/Slider$Order;->ordinal()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->i:[Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    aput-object p2, v1, v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->i:[Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    rsub-int/lit8 v2, v0, 0x1

    aget-object v1, v1, v2

    sget-object v2, Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;->OCCLUDE:Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    if-ne v1, v2, :cond_0

    sget-object v1, Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;->OCCLUDE:Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    if-ne p2, v1, :cond_0

    const-string v1, "Both collapse strategies cannot be OCCLUDE"

    invoke-static {v1}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->i:[Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    rsub-int/lit8 v2, v0, 0x1

    sget-object v3, Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;->DISPLACE:Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    aput-object v3, v1, v2

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/at;

    aget-object v0, v1, v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/at;

    :cond_1
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->i()V

    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->l:Z

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->requestLayout()V

    :cond_2
    return-void
.end method

.method public setLayer(Lcom/google/android/youtube/app/ui/Slider$Order;Lcom/google/android/youtube/app/ui/at;)V
    .locals 4

    const/4 v1, 0x0

    const-string v0, "order may not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->getChildCount()I

    move-result v0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_2

    const/4 v0, 0x1

    :goto_0
    const-string v2, "there must be two children"

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/utils/s;->b(ZLjava/lang/Object;)V

    invoke-virtual {p1}, Lcom/google/android/youtube/app/ui/Slider$Order;->ordinal()I

    move-result v0

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/at;

    aget-object v2, v2, v0

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->m:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/at;

    aget-object v2, v2, v0

    invoke-interface {v2}, Lcom/google/android/youtube/app/ui/at;->h()V

    :cond_0
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/at;

    aget-object v2, v2, v0

    invoke-interface {v2}, Lcom/google/android/youtube/app/ui/at;->i()V

    :cond_1
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/at;

    aput-object p2, v2, v0

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    rsub-int/lit8 v3, v0, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    aget-object v2, v2, v0

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/app/ui/DefaultSlider;->removeView(Landroid/view/View;)V

    if-nez p2, :cond_3

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->j:[I

    aput v1, v2, v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    new-instance v2, Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->a:Landroid/app/Activity;

    invoke-direct {v2, v3}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    aput-object v2, v1, v0

    :goto_1
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    aget-object v0, v1, v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->addView(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->j()V

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->i()V

    return-void

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/at;

    aget-object v1, v1, v0

    invoke-interface {v1}, Lcom/google/android/youtube/app/ui/at;->f()Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/at;

    aget-object v1, v1, v0

    invoke-interface {v1}, Lcom/google/android/youtube/app/ui/at;->a()V

    :cond_4
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/at;

    aget-object v1, v1, v0

    invoke-interface {v1}, Lcom/google/android/youtube/app/ui/at;->g()V

    iget-boolean v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->m:Z

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/at;

    aget-object v1, v1, v0

    invoke-interface {v1}, Lcom/google/android/youtube/app/ui/at;->c()V

    :cond_5
    iget-boolean v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->n:Z

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/at;

    aget-object v1, v1, v0

    invoke-interface {v1}, Lcom/google/android/youtube/app/ui/at;->l()V

    :goto_2
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->j:[I

    invoke-interface {p2}, Lcom/google/android/youtube/app/ui/at;->e()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->c:Landroid/util/DisplayMetrics;

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    aput v2, v1, v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->h:[Landroid/view/View;

    invoke-interface {p2}, Lcom/google/android/youtube/app/ui/at;->o()Landroid/view/View;

    move-result-object v2

    aput-object v2, v1, v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/at;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->i:[Lcom/google/android/youtube/app/ui/Slider$CollapseStrategy;

    goto :goto_1

    :cond_6
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->g:[Lcom/google/android/youtube/app/ui/at;

    aget-object v1, v1, v0

    invoke-interface {v1}, Lcom/google/android/youtube/app/ui/at;->m()V

    goto :goto_2
.end method

.method public setOrientation(Lcom/google/android/youtube/app/ui/Slider$Orientation;)V
    .locals 2

    const/4 v1, 0x0

    sget-object v0, Lcom/google/android/youtube/app/ui/Slider$Orientation;->VERTICAL:Lcom/google/android/youtube/app/ui/Slider$Orientation;

    if-ne p1, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->d:Z

    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->d:Z

    if-eqz v0, :cond_2

    const/16 v0, 0x21

    :goto_1
    iput v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->e:I

    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->d:Z

    if-eqz v0, :cond_3

    const/16 v0, 0x82

    :goto_2
    iput v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->f:I

    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->l:Z

    if-nez v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/ui/DefaultSlider;->b(I)V

    iput v1, p0, Lcom/google/android/youtube/app/ui/DefaultSlider;->k:I

    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/DefaultSlider;->requestLayout()V

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    const/16 v0, 0x11

    goto :goto_1

    :cond_3
    const/16 v0, 0x42

    goto :goto_2
.end method
