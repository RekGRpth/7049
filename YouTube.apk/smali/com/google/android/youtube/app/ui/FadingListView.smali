.class public Lcom/google/android/youtube/app/ui/FadingListView;
.super Landroid/widget/ListView;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:Z

.field private c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/FadingListView;->a()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/FadingListView;->a()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/FadingListView;->a()V

    return-void
.end method

.method private a()V
    .locals 2

    const/4 v1, 0x1

    const v0, 0x555555

    iput v0, p0, Lcom/google/android/youtube/app/ui/FadingListView;->a:I

    iput-boolean v1, p0, Lcom/google/android/youtube/app/ui/FadingListView;->b:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/ui/FadingListView;->c:Z

    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/ui/FadingListView;->setFadingEdgeLength(I)V

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/ui/FadingListView;->setVerticalFadingEdgeEnabled(Z)V

    return-void
.end method


# virtual methods
.method protected getBottomFadingEdgeStrength()F
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/FadingListView;->c:Z

    if-eqz v0, :cond_0

    invoke-super {p0}, Landroid/widget/ListView;->getBottomFadingEdgeStrength()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSolidColor()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/app/ui/FadingListView;->a:I

    return v0
.end method

.method protected getTopFadingEdgeStrength()F
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/FadingListView;->b:Z

    if-eqz v0, :cond_0

    invoke-super {p0}, Landroid/widget/ListView;->getTopFadingEdgeStrength()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setBottomFadingEdgeVisibility(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/app/ui/FadingListView;->c:Z

    return-void
.end method

.method public setFadeColor(I)V
    .locals 0

    iput p1, p0, Lcom/google/android/youtube/app/ui/FadingListView;->a:I

    return-void
.end method

.method public setTopFadingEdgeVisibility(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/app/ui/FadingListView;->b:Z

    return-void
.end method
