.class public Lcom/google/android/youtube/app/ui/RemoteControlView;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/view/View;

.field private d:Landroid/widget/TextView;

.field private e:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

.field private f:Z

.field private final g:Landroid/view/LayoutInflater;

.field private h:Landroid/view/View$OnClickListener;

.field private i:Landroid/view/View;

.field private j:Landroid/widget/ProgressBar;

.field private k:Lcom/google/android/youtube/core/ui/p;

.field private l:Landroid/view/View;

.field private m:Landroid/widget/ImageView;

.field private n:Z

.field private o:Landroid/widget/Button;

.field private p:Z

.field private q:Landroid/widget/TextView;

.field private r:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;Lcom/google/android/youtube/core/ui/q;Landroid/view/View$OnClickListener;)V
    .locals 4

    const/4 v3, -0x1

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->g:Landroid/view/LayoutInflater;

    const-string v0, "controllerOverlay can not be null"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/a/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->e:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    const-string v0, "retryListener can not be null"

    invoke-static {p3, v0}, Lcom/google/android/gms/common/a/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "addToQueueListener can not be null"

    invoke-static {p4, v0}, Lcom/google/android/gms/common/a/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/RemoteControlView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->m:Landroid/widget/ImageView;

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->m:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->m:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->m:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/ui/RemoteControlView;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->g:Landroid/view/LayoutInflater;

    const v1, 0x7f040079

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->c:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->c:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/ui/RemoteControlView;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->c:Landroid/view/View;

    const v1, 0x7f07010c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->d:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->c:Landroid/view/View;

    const v1, 0x7f07010d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->i:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->c:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->g:Landroid/view/LayoutInflater;

    const v1, 0x7f040066

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->q:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->q:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/RemoteControlView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/utils/Util;->g(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->q:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/ui/RemoteControlView;->addView(Landroid/view/View;)V

    iput-object p2, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->e:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    move-object v0, p2

    check-cast v0, Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/ui/RemoteControlView;->addView(Landroid/view/View;)V

    invoke-interface {p2, v2}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->setShowFullscreen(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->g:Landroid/view/LayoutInflater;

    const v1, 0x7f04007c

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->l:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/RemoteControlView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->l:Landroid/view/View;

    invoke-static {v0, v1, p3}, Lcom/google/android/youtube/core/ui/p;->a(Landroid/content/Context;Landroid/view/View;Lcom/google/android/youtube/core/ui/q;)Lcom/google/android/youtube/core/ui/p;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->k:Lcom/google/android/youtube/core/ui/p;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->k:Lcom/google/android/youtube/core/ui/p;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/ui/p;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->l:Landroid/view/View;

    const v1, 0x7f070117

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->o:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->l:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/ui/RemoteControlView;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->g:Landroid/view/LayoutInflater;

    const v1, 0x7f04000c

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->a:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->a:Landroid/view/View;

    const v1, 0x7f07003d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->b:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->a:Landroid/view/View;

    const v1, 0x7f07003e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->j:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->b:Landroid/widget/TextView;

    new-instance v1, Lcom/google/android/youtube/app/ui/bs;

    invoke-direct {v1, p0, p4}, Lcom/google/android/youtube/app/ui/bs;-><init>(Lcom/google/android/youtube/app/ui/RemoteControlView;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->a:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/ui/RemoteControlView;->addView(Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/RemoteControlView;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->b:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/ui/RemoteControlView;)Landroid/widget/ProgressBar;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->j:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method private b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->e:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->b()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->e:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->g()V

    return-void
.end method

.method private c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->e:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->b()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private d()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/RemoteControlView;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->c:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->k:Lcom/google/android/youtube/core/ui/p;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/ui/p;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->e:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    iget-boolean v1, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->n:Z

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->setHasCc(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->e:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    iget-boolean v1, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->p:Z

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->setCcEnabled(Z)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    const/16 v3, 0x8

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->k:Lcom/google/android/youtube/core/ui/p;

    const v1, 0x7f0b0239

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/ui/p;->a(IZ)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/RemoteControlView;->c()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->c:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->a:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/youtube/app/ui/RemoteControlView;->a(Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;Lcom/google/android/youtube/app/remote/am;)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;Lcom/google/android/youtube/app/remote/am;)V
    .locals 5

    const v4, 0x7f0b0258

    const/16 v3, 0x8

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->e:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    sget-object v2, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->YOUTUBE:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    invoke-interface {v0, v2}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->setStyle(Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->a:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    sget-object v0, Lcom/google/android/youtube/app/ui/bt;->a:[I

    invoke-virtual {p1}, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/RemoteControlView;->d()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->e:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->setPlaying()V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/RemoteControlView;->d()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->e:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->d()V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/RemoteControlView;->d()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->e:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->e()V

    goto :goto_0

    :pswitch_3
    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->k:Lcom/google/android/youtube/core/ui/p;

    invoke-virtual {v0, v4, v1}, Lcom/google/android/youtube/core/ui/p;->a(IZ)V

    :goto_1
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/RemoteControlView;->c()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->a:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->k:Lcom/google/android/youtube/core/ui/p;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/ui/p;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->e:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->setPlaying()V

    iget-object v0, p2, Lcom/google/android/youtube/app/remote/am;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->d:Landroid/widget/TextView;

    iget-object v2, p2, Lcom/google/android/youtube/app/remote/am;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    iget-object v0, p2, Lcom/google/android/youtube/app/remote/am;->b:Landroid/net/Uri;

    if-eqz v0, :cond_2

    sget-object v0, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    iget-object v2, p2, Lcom/google/android/youtube/app/remote/am;->b:Landroid/net/Uri;

    invoke-virtual {v0, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_3
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->i:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->i:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->h:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_4
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2

    :cond_2
    move v0, v1

    goto :goto_3

    :cond_3
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->i:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4

    :pswitch_4
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->e:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->setLoading()V

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/RemoteControlView;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->k:Lcom/google/android/youtube/core/ui/p;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/ui/p;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->c:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->e:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    iget-boolean v1, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->n:Z

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->setHasCc(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->e:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    iget-boolean v1, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->p:Z

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->setCcEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->a:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method public final a(Lcom/google/android/youtube/app/remote/an;Ljava/lang/String;)V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/RemoteControlView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p1, Lcom/google/android/youtube/app/remote/an;->b:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, p1, Lcom/google/android/youtube/app/remote/an;->c:Z

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/app/ui/RemoteControlView;->a(Ljava/lang/String;Z)V

    return-void
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 3

    const/16 v2, 0x8

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->o:Landroid/widget/Button;

    if-eqz p2, :cond_0

    const v0, 0x7f0b0019

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->k:Lcom/google/android/youtube/core/ui/p;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/youtube/core/ui/p;->a(Ljava/lang/CharSequence;Z)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/RemoteControlView;->c()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->c:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->a:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    return-void

    :cond_0
    const v0, 0x7f0b0017

    goto :goto_0
.end method

.method public final a(Ljava/util/List;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->e:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->a(Ljava/util/List;)V

    return-void
.end method

.method public final a(Z)V
    .locals 4

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/RemoteControlView;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->e:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    sget-object v2, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->REMOTE:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;

    invoke-interface {v0, v2}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->setStyle(Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->e:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->d()V

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->a:Landroid/view/View;

    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->f:Z

    iget-boolean v3, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->r:Z

    and-int/2addr v0, v3

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/app/ui/RemoteControlView;->b(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->e:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->setHasCc(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->k:Lcom/google/android/youtube/core/ui/p;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/ui/p;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->q:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void

    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->j:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->b:Landroid/widget/TextView;

    const v1, 0x7f02017a

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->b:Landroid/widget/TextView;

    const v1, 0x7f0b023e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->b:Landroid/widget/TextView;

    const v1, 0x7f02017b

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->b:Landroid/widget/TextView;

    const v1, 0x7f0b0240

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method public setAreQueueOpsEnabled(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->r:Z

    return-void
.end method

.method public setCcEnabled(Z)V
    .locals 1

    iput-boolean p1, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->p:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->e:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->setCcEnabled(Z)V

    return-void
.end method

.method public setHasCc(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->n:Z

    return-void
.end method

.method public setHasQueueButton(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->f:Z

    return-void
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->m:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public setPlayingOnMarginTop(I)V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->q:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v0, v2, p1, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->q:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/RemoteControlView;->invalidate()V

    return-void
.end method

.method public setPlayingOnText(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->q:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setTimes(III)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/RemoteControlView;->e:Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->setTimes(III)V

    return-void
.end method
