.class public final enum Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

.field public static final enum ERROR:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

.field public static final enum NOT_SUBSCRIBED:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

.field public static final enum SUBSCRIBED:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

.field public static final enum WORKING:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    const-string v1, "SUBSCRIBED"

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->SUBSCRIBED:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    new-instance v0, Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    const-string v1, "NOT_SUBSCRIBED"

    invoke-direct {v0, v1, v3}, Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->NOT_SUBSCRIBED:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    new-instance v0, Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    const-string v1, "WORKING"

    invoke-direct {v0, v1, v4}, Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->WORKING:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    new-instance v0, Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v5}, Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->ERROR:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    sget-object v1, Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->SUBSCRIBED:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->NOT_SUBSCRIBED:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->WORKING:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->ERROR:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->$VALUES:[Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;
    .locals 1

    const-class v0, Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    return-object v0
.end method

.method public static values()[Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->$VALUES:[Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    invoke-virtual {v0}, [Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    return-object v0
.end method
