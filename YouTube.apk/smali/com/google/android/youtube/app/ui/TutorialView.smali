.class public Lcom/google/android/youtube/app/ui/TutorialView;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/youtube/app/ui/u;


# instance fields
.field private a:Lcom/google/android/youtube/app/ui/ClingView;

.field private b:Landroid/view/View;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/LinearLayout;

.field private e:Lcom/google/android/youtube/app/ui/cy;

.field private f:I

.field private g:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x7

    iput v0, p0, Lcom/google/android/youtube/app/ui/TutorialView;->f:I

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/TutorialView;I)V
    .locals 0

    invoke-super {p0, p1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/ui/TutorialView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/TutorialView;->e:Lcom/google/android/youtube/app/ui/cy;

    invoke-interface {v0}, Lcom/google/android/youtube/app/ui/cy;->a()V

    return-void
.end method

.method public final a(Landroid/graphics/Rect;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/TutorialView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget v1, p0, Lcom/google/android/youtube/app/ui/TutorialView;->f:I

    const/4 v2, 0x7

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/TutorialView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setMinimumHeight(I)V

    iget v1, p1, Landroid/graphics/Rect;->top:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget v1, p1, Landroid/graphics/Rect;->right:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/TutorialView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/TutorialView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/TutorialView;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    return-void

    :cond_1
    iget v1, p0, Lcom/google/android/youtube/app/ui/TutorialView;->f:I

    const/16 v2, 0x8

    if-ne v1, v2, :cond_0

    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/TutorialView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0800b5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    const/16 v1, 0xc

    invoke-virtual {v0, v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/TutorialView;->c:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v2, 0x31

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/TutorialView;->c:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/TutorialView;->g:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/TutorialView;->b:Landroid/view/View;

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/TutorialView;->a()V

    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    const v0, 0x7f07014d

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/ui/TutorialView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/TutorialView;->b:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/TutorialView;->b:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f07014c

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/ui/TutorialView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/ClingView;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/TutorialView;->a:Lcom/google/android/youtube/app/ui/ClingView;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/TutorialView;->a:Lcom/google/android/youtube/app/ui/ClingView;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/ui/ClingView;->setHighlightBoundsListener(Lcom/google/android/youtube/app/ui/u;)V

    const v0, 0x7f07008c

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/ui/TutorialView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/TutorialView;->c:Landroid/widget/TextView;

    const v0, 0x7f07014e

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/ui/TutorialView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/TutorialView;->d:Landroid/widget/LinearLayout;

    const v0, 0x7f07002c

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/ui/TutorialView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/TutorialView;->g:Landroid/view/View;

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/TutorialView;->a:Lcom/google/android/youtube/app/ui/ClingView;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/ClingView;->a()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/TutorialView;->a()V

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setAlignTextToCling(I)V
    .locals 2

    const/16 v0, 0x8

    if-eq p1, v0, :cond_0

    const/4 v0, 0x7

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "Only  RelativeLayout.ALIGN_BOTTOM or RelativeLayout.ALIGN_RIGHT are supported at the moment"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    iput p1, p0, Lcom/google/android/youtube/app/ui/TutorialView;->f:I

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setDismissListener(Lcom/google/android/youtube/app/ui/cy;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/TutorialView;->e:Lcom/google/android/youtube/app/ui/cy;

    return-void
.end method

.method public setTargetView(Landroid/view/View;Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/TutorialView;->a:Lcom/google/android/youtube/app/ui/ClingView;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/app/ui/ClingView;->setViewToCling(Landroid/view/View;Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/TutorialView;->postInvalidate()V

    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/TutorialView;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setTypeface(Landroid/graphics/Typeface;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/TutorialView;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    return-void
.end method

.method public setVisibility(I)V
    .locals 6

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/TutorialView;->getVisibility()I

    move-result v0

    if-ne p1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-nez p1, :cond_2

    const/4 v0, 0x1

    :goto_1
    new-instance v5, Landroid/view/animation/AlphaAnimation;

    if-eqz v0, :cond_3

    move v4, v3

    :goto_2
    if-eqz v0, :cond_4

    :goto_3
    invoke-direct {v5, v4, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    if-eqz v0, :cond_5

    const-wide/16 v2, 0x3e8

    :goto_4
    invoke-virtual {v5, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    new-instance v2, Lcom/google/android/youtube/app/ui/cx;

    invoke-direct {v2, p0, p1}, Lcom/google/android/youtube/app/ui/cx;-><init>(Lcom/google/android/youtube/app/ui/TutorialView;I)V

    invoke-virtual {v5, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    if-eqz v0, :cond_1

    invoke-super {p0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    :cond_1
    invoke-virtual {p0, v5}, Lcom/google/android/youtube/app/ui/TutorialView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    move v4, v2

    goto :goto_2

    :cond_4
    move v2, v3

    goto :goto_3

    :cond_5
    const-wide/16 v2, 0x1f4

    goto :goto_4
.end method
