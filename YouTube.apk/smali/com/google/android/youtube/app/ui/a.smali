.class public abstract Lcom/google/android/youtube/app/ui/a;
.super Lcom/google/android/youtube/core/ui/j;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field private final a:Lcom/google/android/youtube/core/a/a;

.field private final g:Lcom/google/android/youtube/app/ui/bx;

.field private final h:Z

.field private final i:Lcom/google/android/youtube/app/ui/az;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/PagedView;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/e;Z)V
    .locals 6

    invoke-interface {p4}, Lcom/google/android/youtube/core/client/bc;->n()Lcom/google/android/youtube/core/async/au;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/core/ui/j;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/PagedView;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/e;)V

    iput-boolean p6, p0, Lcom/google/android/youtube/app/ui/a;->h:Z

    new-instance v0, Lcom/google/android/youtube/app/ui/az;

    invoke-direct {v0}, Lcom/google/android/youtube/app/ui/az;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/a;->i:Lcom/google/android/youtube/app/ui/az;

    instance-of v0, p3, Lcom/google/android/youtube/app/ui/bx;

    if-eqz v0, :cond_0

    move-object v0, p3

    check-cast v0, Lcom/google/android/youtube/app/ui/bx;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/ui/bx;->a(Landroid/widget/AdapterView$OnItemClickListener;)V

    check-cast p3, Lcom/google/android/youtube/app/ui/bx;

    iput-object p3, p0, Lcom/google/android/youtube/app/ui/a;->g:Lcom/google/android/youtube/app/ui/bx;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/a;->g:Lcom/google/android/youtube/app/ui/bx;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/bx;->d()Lcom/google/android/youtube/core/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/a;->a:Lcom/google/android/youtube/core/a/a;

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/a;->g:Lcom/google/android/youtube/app/ui/bx;

    iput-object p3, p0, Lcom/google/android/youtube/app/ui/a;->a:Lcom/google/android/youtube/core/a/a;

    invoke-interface {p2, p0}, Lcom/google/android/youtube/core/ui/PagedView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_0
.end method


# virtual methods
.method protected abstract a(Lcom/google/android/youtube/core/model/Event;I)V
.end method

.method public a(Lcom/google/android/youtube/core/model/Event;)Z
    .locals 1

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Event;->action:Lcom/google/android/youtube/core/model/Event$Action;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/a;->h:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/Event;->targetIsVideo()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/a;->i:Lcom/google/android/youtube/app/ui/az;

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Event;->targetVideo:Lcom/google/android/youtube/core/model/Video;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/az;->a(Lcom/google/android/youtube/core/model/Video;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    check-cast p1, Lcom/google/android/youtube/core/model/Event;

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/app/ui/a;->a(Lcom/google/android/youtube/core/model/Event;)Z

    move-result v0

    return v0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v1

    if-ge p3, v1, :cond_0

    invoke-interface {v0, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Event;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0, p3}, Lcom/google/android/youtube/app/ui/a;->a(Lcom/google/android/youtube/core/model/Event;I)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/a;->a:Lcom/google/android/youtube/core/a/a;

    goto :goto_0
.end method
