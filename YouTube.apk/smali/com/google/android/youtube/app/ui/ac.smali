.class public final Lcom/google/android/youtube/app/ui/ac;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private final c:Lcom/google/android/youtube/core/client/bc;

.field private final d:Lcom/google/android/youtube/core/e;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/e;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/ac;->a:Landroid/app/Activity;

    iput-object p2, p0, Lcom/google/android/youtube/app/ui/ac;->b:Lcom/google/android/youtube/core/async/UserAuthorizer;

    iput-object p3, p0, Lcom/google/android/youtube/app/ui/ac;->c:Lcom/google/android/youtube/core/client/bc;

    iput-object p4, p0, Lcom/google/android/youtube/app/ui/ac;->d:Lcom/google/android/youtube/core/e;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/ac;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ac;->a:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/ui/ac;)Lcom/google/android/youtube/core/async/UserAuthorizer;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ac;->b:Lcom/google/android/youtube/core/async/UserAuthorizer;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/ui/ac;)Lcom/google/android/youtube/core/e;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ac;->d:Lcom/google/android/youtube/core/e;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/ui/ac;)Lcom/google/android/youtube/core/client/bc;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ac;->c:Lcom/google/android/youtube/core/client/bc;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/app/ui/af;)Landroid/app/Dialog;
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ac;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040030

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    const v0, 0x7f070054

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    const v1, 0x7f07008a

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    new-instance v3, Lcom/google/android/youtube/app/ui/ad;

    invoke-direct {v3, p0, v0, v1, p1}, Lcom/google/android/youtube/app/ui/ad;-><init>(Lcom/google/android/youtube/app/ui/ac;Landroid/widget/EditText;Landroid/widget/CheckBox;Lcom/google/android/youtube/app/ui/af;)V

    new-instance v0, Lcom/google/android/youtube/core/ui/w;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ac;->a:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/ui/w;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0b020d

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/w;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ac;->a:Landroid/app/Activity;

    const v2, 0x104000a

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ac;->a:Landroid/app/Activity;

    const/high16 v2, 0x1040000

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    return-object v0
.end method
