.class final Lcom/google/android/youtube/app/ui/ad;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Landroid/widget/EditText;

.field final synthetic b:Landroid/widget/CheckBox;

.field final synthetic c:Lcom/google/android/youtube/app/ui/af;

.field final synthetic d:Lcom/google/android/youtube/app/ui/ac;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/ui/ac;Landroid/widget/EditText;Landroid/widget/CheckBox;Lcom/google/android/youtube/app/ui/af;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/ad;->d:Lcom/google/android/youtube/app/ui/ac;

    iput-object p2, p0, Lcom/google/android/youtube/app/ui/ad;->a:Landroid/widget/EditText;

    iput-object p3, p0, Lcom/google/android/youtube/app/ui/ad;->b:Landroid/widget/CheckBox;

    iput-object p4, p0, Lcom/google/android/youtube/app/ui/ad;->c:Lcom/google/android/youtube/app/ui/af;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 7

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ad;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ad;->d:Lcom/google/android/youtube/app/ui/ac;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/ac;->b(Lcom/google/android/youtube/app/ui/ac;)Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/ad;->d:Lcom/google/android/youtube/app/ui/ac;

    invoke-static {v2}, Lcom/google/android/youtube/app/ui/ac;->a(Lcom/google/android/youtube/app/ui/ac;)Landroid/app/Activity;

    move-result-object v2

    new-instance v3, Lcom/google/android/youtube/app/ui/ae;

    iget-object v4, p0, Lcom/google/android/youtube/app/ui/ad;->d:Lcom/google/android/youtube/app/ui/ac;

    iget-object v5, p0, Lcom/google/android/youtube/app/ui/ad;->b:Landroid/widget/CheckBox;

    invoke-virtual {v5}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v5

    iget-object v6, p0, Lcom/google/android/youtube/app/ui/ad;->c:Lcom/google/android/youtube/app/ui/af;

    invoke-direct {v3, v4, v0, v5, v6}, Lcom/google/android/youtube/app/ui/ae;-><init>(Lcom/google/android/youtube/app/ui/ac;Ljava/lang/String;ZLcom/google/android/youtube/app/ui/af;)V

    invoke-virtual {v1, v2, v3}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bk;)V

    goto :goto_0
.end method
