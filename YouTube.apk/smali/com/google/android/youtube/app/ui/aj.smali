.class public final Lcom/google/android/youtube/app/ui/aj;
.super Lcom/google/android/youtube/core/a/l;
.source "SourceFile"


# instance fields
.field private final a:Landroid/widget/TextView;

.field private c:Lcom/google/android/youtube/app/adapter/cn;


# direct methods
.method private constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/app/adapter/ac;Lcom/google/android/youtube/app/adapter/cn;)V
    .locals 4

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/youtube/core/a/e;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    const/4 v1, 0x1

    aput-object p3, v0, v1

    const/4 v1, 0x2

    invoke-virtual {p1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f0400b4

    invoke-static {v2, v3}, Lcom/google/android/youtube/app/adapter/cn;->a(Landroid/view/LayoutInflater;I)Lcom/google/android/youtube/app/adapter/cn;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/a/l;-><init>([Lcom/google/android/youtube/core/a/e;)V

    const-string v0, "bodyOutline cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/cn;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/aj;->c:Lcom/google/android/youtube/app/adapter/cn;

    invoke-virtual {p3}, Lcom/google/android/youtube/app/adapter/cn;->b()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    const v1, 0x7f070096

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/aj;->a:Landroid/widget/TextView;

    return-void
.end method

.method public static a(Landroid/app/Activity;)Lcom/google/android/youtube/app/ui/aj;
    .locals 6

    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040035

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/adapter/cn;->a(Landroid/view/LayoutInflater;I)Lcom/google/android/youtube/app/adapter/cn;

    move-result-object v5

    new-instance v0, Lcom/google/android/youtube/app/adapter/ac;

    sget-object v2, Lcom/google/android/youtube/app/ui/ee;->a:Lcom/google/android/youtube/core/a/g;

    const v1, 0x7f0b026d

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f040027

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/adapter/ac;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/a/g;Ljava/lang/String;ILcom/google/android/youtube/core/a/e;)V

    new-instance v1, Lcom/google/android/youtube/app/ui/aj;

    invoke-direct {v1, p0, v0, v5}, Lcom/google/android/youtube/app/ui/aj;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/app/adapter/ac;Lcom/google/android/youtube/app/adapter/cn;)V

    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/aj;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz p1, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->getTrimmedLength(Ljava/lang/CharSequence;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/ui/aj;->c(Z)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/ui/aj;->c(Z)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/aj;->c:Lcom/google/android/youtube/app/adapter/cn;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/cn;->c(Z)V

    return-void
.end method

.method public final d(I)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
