.class public final Lcom/google/android/youtube/app/ui/ak;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/app/Activity;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "activity may not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ak;->a:Landroid/app/Activity;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/ak;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ak;->a:Landroid/app/Activity;

    return-object v0
.end method


# virtual methods
.method public final a()Landroid/app/Dialog;
    .locals 6

    new-instance v0, Landroid/app/Dialog;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ak;->a:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0b00f6

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(I)V

    new-instance v1, Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/ak;->a:Landroid/app/Activity;

    invoke-direct {v1, v2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    new-instance v2, Lcom/google/android/youtube/app/ui/an;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/ak;->a:Landroid/app/Activity;

    invoke-direct {v2, p0, v3}, Lcom/google/android/youtube/app/ui/an;-><init>(Lcom/google/android/youtube/app/ui/ak;Landroid/content/Context;)V

    new-instance v3, Lcom/google/android/youtube/app/ui/am;

    const v4, 0x7f0b00f7

    const v5, 0x7f0b00f8

    invoke-direct {v3, v4, v5}, Lcom/google/android/youtube/app/ui/am;-><init>(II)V

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/app/ui/an;->d(Ljava/lang/Object;)V

    new-instance v3, Lcom/google/android/youtube/app/ui/am;

    const v4, 0x7f0b00f9

    const v5, 0x7f0b00fa

    invoke-direct {v3, v4, v5}, Lcom/google/android/youtube/app/ui/am;-><init>(II)V

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/app/ui/an;->d(Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v2, Lcom/google/android/youtube/app/ui/al;

    invoke-direct {v2, p0, v0}, Lcom/google/android/youtube/app/ui/al;-><init>(Lcom/google/android/youtube/app/ui/ak;Landroid/app/Dialog;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    return-object v0
.end method
