.class public final Lcom/google/android/youtube/app/ui/ao;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# static fields
.field private static final i:[Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;

.field private static final j:[I

.field private static final k:[Lcom/google/android/youtube/core/async/GDataRequestFactory$LiveFeed;

.field private static final l:[I


# instance fields
.field protected final a:Landroid/app/Activity;

.field private final b:Lcom/google/android/youtube/app/ui/aq;

.field private c:Landroid/widget/Button;

.field private d:Landroid/app/Dialog;

.field private e:I

.field private f:Landroid/widget/Spinner;

.field private final g:Ljava/util/List;

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x4

    new-array v0, v2, [Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;

    sget-object v1, Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;->TOP_RATED:Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;->TOP_FAVORITES:Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;->MOST_VIEWED:Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;->MOST_DISCUSSED:Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/youtube/app/ui/ao;->i:[Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;

    new-array v0, v2, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/youtube/app/ui/ao;->j:[I

    new-array v0, v2, [Lcom/google/android/youtube/core/async/GDataRequestFactory$LiveFeed;

    sget-object v1, Lcom/google/android/youtube/core/async/GDataRequestFactory$LiveFeed;->FEATURED:Lcom/google/android/youtube/core/async/GDataRequestFactory$LiveFeed;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/youtube/core/async/GDataRequestFactory$LiveFeed;->LIVE_NOW:Lcom/google/android/youtube/core/async/GDataRequestFactory$LiveFeed;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/youtube/core/async/GDataRequestFactory$LiveFeed;->UPCOMING:Lcom/google/android/youtube/core/async/GDataRequestFactory$LiveFeed;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/youtube/core/async/GDataRequestFactory$LiveFeed;->RECENTLY_BROADCASTED:Lcom/google/android/youtube/core/async/GDataRequestFactory$LiveFeed;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/youtube/app/ui/ao;->k:[Lcom/google/android/youtube/core/async/GDataRequestFactory$LiveFeed;

    new-array v0, v2, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/youtube/app/ui/ao;->l:[I

    return-void

    :array_0
    .array-data 4
        0x7f0b00fb
        0x7f0b00fc
        0x7f0b00fd
        0x7f0b00fe
    .end array-data

    :array_1
    .array-data 4
        0x7f0b0229
        0x7f0b0226
        0x7f0b0227
        0x7f0b0228
    .end array-data
.end method

.method private constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/app/ui/aq;Ljava/util/List;I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-ltz p4, :cond_0

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-ge p4, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/youtube/core/utils/s;->a(Z)V

    invoke-static {p2}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/aq;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ao;->b:Lcom/google/android/youtube/app/ui/aq;

    invoke-static {p3}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ao;->g:Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/ao;->a:Landroid/app/Activity;

    iput p4, p0, Lcom/google/android/youtube/app/ui/ao;->h:I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/app/ui/aq;Ljava/util/List;ILandroid/widget/Spinner;)V
    .locals 3

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/youtube/app/ui/ao;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/app/ui/aq;Ljava/util/List;I)V

    invoke-static {p5}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ao;->f:Landroid/widget/Spinner;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ao;->f:Landroid/widget/Spinner;

    new-instance v1, Lcom/google/android/youtube/app/ui/ap;

    invoke-virtual {p1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-direct {v1, v2, p3}, Lcom/google/android/youtube/app/ui/ap;-><init>(Landroid/view/LayoutInflater;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ao;->f:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ao;->f:Landroid/widget/Spinner;

    invoke-virtual {v0, p4}, Landroid/widget/Spinner;->setSelection(I)V

    return-void
.end method

.method public static a(Landroid/app/Activity;Lcom/google/android/youtube/app/ui/aq;Lcom/google/android/youtube/core/async/GDataRequestFactory$StatisticFilter;Landroid/widget/Spinner;)Lcom/google/android/youtube/app/ui/ao;
    .locals 6

    invoke-static {}, Lcom/google/android/youtube/core/async/GDataRequestFactory$StatisticFilter;->values()[Lcom/google/android/youtube/core/async/GDataRequestFactory$StatisticFilter;

    move-result-object v1

    new-instance v3, Ljava/util/ArrayList;

    array-length v0, v1

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v4, v1, v0

    iget v5, v4, Lcom/google/android/youtube/core/async/GDataRequestFactory$StatisticFilter;->stringId:I

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/google/android/youtube/app/ui/ao;

    invoke-virtual {p2}, Lcom/google/android/youtube/core/async/GDataRequestFactory$StatisticFilter;->ordinal()I

    move-result v4

    move-object v1, p0

    move-object v2, p1

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/ui/ao;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/app/ui/aq;Ljava/util/List;ILandroid/widget/Spinner;)V

    return-object v0
.end method

.method public static a(Landroid/app/Activity;Lcom/google/android/youtube/app/ui/aq;Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;Landroid/widget/Spinner;I)Lcom/google/android/youtube/app/ui/ao;
    .locals 6

    invoke-static {}, Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;->values()[Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    move-result-object v1

    new-instance v3, Ljava/util/ArrayList;

    array-length v0, v1

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v4, v1, v0

    iget v5, v4, Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;->stringId:I

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/google/android/youtube/app/ui/ao;

    invoke-virtual {p2}, Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;->ordinal()I

    move-result v4

    move-object v1, p0

    move-object v2, p1

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/ui/ao;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/app/ui/aq;Ljava/util/List;ILandroid/widget/Spinner;)V

    return-object v0
.end method


# virtual methods
.method public final a()Landroid/app/Dialog;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ao;->d:Landroid/app/Dialog;

    return-object v0
.end method

.method public final b()Ljava/lang/Enum;
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ao;->g:Ljava/util/List;

    iget v1, p0, Lcom/google/android/youtube/app/ui/ao;->h:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Enum;

    return-object v0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ao;->a:Landroid/app/Activity;

    iget v1, p0, Lcom/google/android/youtube/app/ui/ao;->e:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    return-void
.end method

.method public final onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    iget v0, p0, Lcom/google/android/youtube/app/ui/ao;->h:I

    if-eq p3, v0, :cond_1

    iput p3, p0, Lcom/google/android/youtube/app/ui/ao;->h:I

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ao;->c:Landroid/widget/Button;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ao;->c:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ao;->g:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ao;->b:Lcom/google/android/youtube/app/ui/aq;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ao;->g:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Enum;

    invoke-interface {v1, v0}, Lcom/google/android/youtube/app/ui/aq;->a(Ljava/lang/Enum;)V

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ao;->f:Landroid/widget/Spinner;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ao;->f:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    if-eq v0, p3, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ao;->f:Landroid/widget/Spinner;

    invoke-virtual {v0, p3}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_0
.end method

.method public final onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    return-void
.end method
