.class public final Lcom/google/android/youtube/app/ui/au;
.super Lcom/google/android/youtube/core/ui/j;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field private final a:Lcom/google/android/youtube/core/a/a;

.field private final g:Lcom/google/android/youtube/core/utils/t;

.field private final h:Lcom/google/android/youtube/app/d;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/PagedView;Lcom/google/android/youtube/app/ui/bx;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/utils/t;)V
    .locals 6

    invoke-interface {p4}, Lcom/google/android/youtube/core/client/bc;->v()Lcom/google/android/youtube/core/async/au;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/core/ui/j;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/PagedView;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/e;)V

    iput-object p6, p0, Lcom/google/android/youtube/app/ui/au;->h:Lcom/google/android/youtube/app/d;

    iput-object p7, p0, Lcom/google/android/youtube/app/ui/au;->g:Lcom/google/android/youtube/core/utils/t;

    invoke-virtual {p3, p0}, Lcom/google/android/youtube/app/ui/bx;->a(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-virtual {p3}, Lcom/google/android/youtube/app/ui/bx;->d()Lcom/google/android/youtube/core/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/au;->a:Lcom/google/android/youtube/core/a/a;

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    check-cast p1, Lcom/google/android/youtube/core/model/LiveEvent;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/au;->g:Lcom/google/android/youtube/core/utils/t;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/utils/t;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v1

    if-ge p3, v1, :cond_0

    invoke-interface {v0, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/LiveEvent;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/au;->h:Lcom/google/android/youtube/app/d;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/LiveEvent;->video:Lcom/google/android/youtube/core/model/Video;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->LIVE:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/youtube/app/d;->a(Ljava/lang/String;ZLcom/google/android/youtube/core/client/VideoStats2Client$Feature;)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/au;->a:Lcom/google/android/youtube/core/a/a;

    goto :goto_0
.end method
