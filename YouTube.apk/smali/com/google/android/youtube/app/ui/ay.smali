.class final Lcom/google/android/youtube/app/ui/ay;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/ui/aw;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/ui/aw;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/ay;->a:Lcom/google/android/youtube/app/ui/aw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2

    check-cast p1, Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "failed to get disco results for artist "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ay;->a:Lcom/google/android/youtube/app/ui/aw;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/aw;->a(Lcom/google/android/youtube/app/ui/aw;)Lcom/google/android/youtube/app/ui/ep;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/ui/ep;->A()V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    check-cast p2, Lcom/google/android/youtube/core/model/Artist;

    if-nez p2, :cond_0

    const-string v0, "got null artist disco result"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ay;->a:Lcom/google/android/youtube/app/ui/aw;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/aw;->a(Lcom/google/android/youtube/app/ui/aw;)Lcom/google/android/youtube/app/ui/ep;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/ui/ep;->A()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ay;->a:Lcom/google/android/youtube/app/ui/aw;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/aw;->d(Lcom/google/android/youtube/app/ui/aw;)Lcom/google/android/youtube/core/model/MusicVideo;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p2, Lcom/google/android/youtube/core/model/Artist;->id:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ay;->a:Lcom/google/android/youtube/app/ui/aw;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/aw;->d(Lcom/google/android/youtube/app/ui/aw;)Lcom/google/android/youtube/core/model/MusicVideo;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/youtube/core/model/MusicVideo;->artistId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    const-string v0, "got stale result for artist disco, ignoring"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ay;->a:Lcom/google/android/youtube/app/ui/aw;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/aw;->a(Lcom/google/android/youtube/app/ui/aw;)Lcom/google/android/youtube/app/ui/ep;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/ui/ep;->A()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ay;->a:Lcom/google/android/youtube/app/ui/aw;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/aw;->b(Lcom/google/android/youtube/app/ui/aw;)Lcom/google/android/youtube/app/ui/ef;

    move-result-object v0

    iget-object v1, p2, Lcom/google/android/youtube/core/model/Artist;->bio:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/ef;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ay;->a:Lcom/google/android/youtube/app/ui/aw;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/aw;->c(Lcom/google/android/youtube/app/ui/aw;)Lcom/google/android/youtube/app/ui/eg;

    move-result-object v0

    iget-object v1, p2, Lcom/google/android/youtube/core/model/Artist;->artistTape:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/eg;->a(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ay;->a:Lcom/google/android/youtube/app/ui/aw;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/aw;->e(Lcom/google/android/youtube/app/ui/aw;)Lcom/google/android/youtube/app/ui/eq;

    move-result-object v0

    iget-object v1, p2, Lcom/google/android/youtube/core/model/Artist;->relatedArtists:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/eq;->a(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ay;->a:Lcom/google/android/youtube/app/ui/aw;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/ui/aw;->a(Lcom/google/android/youtube/app/ui/aw;Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ay;->a:Lcom/google/android/youtube/app/ui/aw;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/aw;->a(Lcom/google/android/youtube/app/ui/aw;)Lcom/google/android/youtube/app/ui/ep;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/ui/ep;->A()V

    goto :goto_0
.end method
