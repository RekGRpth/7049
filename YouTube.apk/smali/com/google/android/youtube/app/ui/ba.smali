.class public final Lcom/google/android/youtube/app/ui/ba;
.super Lcom/google/android/youtube/core/ui/j;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# instance fields
.field private final a:Lcom/google/android/youtube/core/a/a;

.field private final g:Lcom/google/android/youtube/app/ui/bx;

.field private final h:Lcom/google/android/youtube/app/ui/bb;

.field private i:Lcom/google/android/youtube/app/ui/bc;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/PagedView;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/app/ui/bb;)V
    .locals 1

    invoke-direct/range {p0 .. p5}, Lcom/google/android/youtube/core/ui/j;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/PagedView;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/e;)V

    const-string v0, "onPlaylistClickListener can\'t be null"

    invoke-static {p6, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/bb;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ba;->h:Lcom/google/android/youtube/app/ui/bb;

    instance-of v0, p3, Lcom/google/android/youtube/app/ui/bx;

    if-eqz v0, :cond_0

    move-object v0, p3

    check-cast v0, Lcom/google/android/youtube/app/ui/bx;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/ui/bx;->a(Landroid/widget/AdapterView$OnItemClickListener;)V

    check-cast p3, Lcom/google/android/youtube/app/ui/bx;

    iput-object p3, p0, Lcom/google/android/youtube/app/ui/ba;->g:Lcom/google/android/youtube/app/ui/bx;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ba;->g:Lcom/google/android/youtube/app/ui/bx;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/bx;->d()Lcom/google/android/youtube/core/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ba;->a:Lcom/google/android/youtube/core/a/a;

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ba;->g:Lcom/google/android/youtube/app/ui/bx;

    iput-object p3, p0, Lcom/google/android/youtube/app/ui/ba;->a:Lcom/google/android/youtube/core/a/a;

    invoke-interface {p2, p0}, Lcom/google/android/youtube/core/ui/PagedView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/async/GDataRequest;Ljava/lang/Exception;)V
    .locals 1

    iget-object v0, p1, Lcom/google/android/youtube/core/async/GDataRequest;->d:Lcom/google/android/youtube/core/model/UserAuth;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/youtube/core/async/GDataRequest;->d:Lcom/google/android/youtube/core/model/UserAuth;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/UserAuth;->username:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    instance-of v0, p2, Lcom/google/android/youtube/core/async/GDataResponseException;

    if-eqz v0, :cond_0

    move-object v0, p2

    check-cast v0, Lcom/google/android/youtube/core/async/GDataResponseException;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/GDataResponseException;->containsYouTubeSignupRequiredError()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/youtube/core/model/Page$Builder;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/Page$Builder;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/Page$Builder;->build()Lcom/google/android/youtube/core/model/Page;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/youtube/app/ui/ba;->a(Lcom/google/android/youtube/core/async/GDataRequest;Lcom/google/android/youtube/core/model/Page;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/core/ui/j;->a(Lcom/google/android/youtube/core/async/GDataRequest;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0

    check-cast p1, Lcom/google/android/youtube/core/async/GDataRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/app/ui/ba;->a(Lcom/google/android/youtube/core/async/GDataRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v1

    if-ge p3, v1, :cond_0

    invoke-interface {v0, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Playlist;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ba;->h:Lcom/google/android/youtube/app/ui/bb;

    invoke-interface {v1, v0}, Lcom/google/android/youtube/app/ui/bb;->a(Lcom/google/android/youtube/core/model/Playlist;)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ba;->a:Lcom/google/android/youtube/core/a/a;

    goto :goto_0
.end method

.method public final onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    :goto_0
    invoke-interface {v0, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Playlist;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ba;->i:Lcom/google/android/youtube/app/ui/bc;

    invoke-interface {v0}, Lcom/google/android/youtube/app/ui/bc;->a()Z

    move-result v0

    :goto_1
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ba;->a:Lcom/google/android/youtube/core/a/a;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
