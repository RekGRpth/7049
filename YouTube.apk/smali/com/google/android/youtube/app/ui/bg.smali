.class Lcom/google/android/youtube/app/ui/bg;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public c:Landroid/widget/ImageView;

.field public d:Landroid/widget/TextView;

.field final synthetic e:Lcom/google/android/youtube/app/ui/be;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/ui/be;Landroid/view/View;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/bg;->e:Lcom/google/android/youtube/app/ui/be;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x7f070083

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->c:Landroid/widget/ImageView;

    const v0, 0x7f070109

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->d:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/youtube/core/model/Video$Privacy;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->c:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/youtube/core/model/Video$Privacy;->PRIVATE:Lcom/google/android/youtube/core/model/Video$Privacy;

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->c:Landroid/widget/ImageView;

    const v1, 0x7f0201b0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->d:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->d:Landroid/widget/TextView;

    iget v1, p1, Lcom/google/android/youtube/core/model/Video$Privacy;->nameId:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->c:Landroid/widget/ImageView;

    const v1, 0x7f0201b1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method
