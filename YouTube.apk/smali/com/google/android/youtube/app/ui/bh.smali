.class public final Lcom/google/android/youtube/app/ui/bh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/remote/al;
.implements Lcom/google/android/youtube/core/player/overlay/e;


# static fields
.field private static final a:Ljava/util/Set;


# instance fields
.field private final A:Lcom/google/android/youtube/app/adapter/cn;

.field private final B:Landroid/widget/FrameLayout;

.field private final C:Landroid/view/View;

.field private final D:Landroid/view/View;

.field private final b:Landroid/app/Activity;

.field private final c:Lcom/google/android/youtube/core/client/be;

.field private final d:Lcom/google/android/youtube/core/client/bl;

.field private final e:Lcom/google/android/youtube/core/Analytics;

.field private final f:Lcom/google/android/youtube/core/async/n;

.field private g:Lcom/google/android/youtube/app/remote/RemoteControl;

.field private final h:Lcom/google/android/youtube/core/player/as;

.field private final i:Landroid/os/Handler;

.field private final j:Lcom/google/android/youtube/app/ui/bq;

.field private final k:Lcom/google/android/youtube/app/ui/bp;

.field private final l:Z

.field private m:Lcom/google/android/youtube/core/model/Video;

.field private n:Z

.field private o:Ljava/lang/String;

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:Z

.field private v:I

.field private w:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

.field private final x:Lcom/google/android/youtube/app/ui/RemoteControlView;

.field private final y:Lcom/google/android/youtube/app/ui/RemoteControlView;

.field private z:Lcom/google/android/youtube/app/ui/RemoteControlView;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v1, 0x0

    const/4 v0, 0x3

    new-array v2, v0, [Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    sget-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->PLAYING:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    aput-object v0, v2, v1

    const/4 v0, 0x1

    sget-object v3, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->PAUSED:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    aput-object v3, v2, v0

    const/4 v0, 0x2

    sget-object v3, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->ENDED:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    aput-object v3, v2, v0

    if-eqz v2, :cond_0

    array-length v0, v2

    if-nez v0, :cond_2

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    :cond_1
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/app/ui/bh;->a:Ljava/util/Set;

    return-void

    :cond_2
    new-instance v0, Ljava/util/HashSet;

    array-length v3, v2

    invoke-direct {v0, v3}, Ljava/util/HashSet;-><init>(I)V

    array-length v3, v2

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private constructor <init>(Lcom/google/android/youtube/core/player/as;Landroid/app/Activity;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/bl;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;Landroid/view/View;Lcom/google/android/youtube/app/ui/bp;Lcom/google/android/youtube/app/ui/bq;Z)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/ui/bh;->t:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/ui/bh;->u:Z

    iput-object p8, p0, Lcom/google/android/youtube/app/ui/bh;->C:Landroid/view/View;

    const-string v0, "player can not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/as;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->h:Lcom/google/android/youtube/core/player/as;

    const-string v0, "activity can not be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->b:Landroid/app/Activity;

    const-string v0, "imageClient can not be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/client/be;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->c:Lcom/google/android/youtube/core/client/be;

    const-string v0, "subtitlesClient cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/client/bl;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->d:Lcom/google/android/youtube/core/client/bl;

    const-string v0, "analytics can not be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/Analytics;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->e:Lcom/google/android/youtube/core/Analytics;

    const-string v0, "fullscreenListener can not be null"

    invoke-static {p9, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/bp;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->k:Lcom/google/android/youtube/app/ui/bp;

    const-string v0, "listener can not be null"

    invoke-static {p10, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/bq;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->j:Lcom/google/android/youtube/app/ui/bq;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/ui/bh;->l:Z

    new-instance v0, Lcom/google/android/youtube/app/ui/bm;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/ui/bm;-><init>(Lcom/google/android/youtube/app/ui/bh;)V

    new-instance v1, Lcom/google/android/youtube/app/ui/bl;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/ui/bl;-><init>(Lcom/google/android/youtube/app/ui/bh;)V

    new-instance v2, Lcom/google/android/youtube/app/ui/RemoteControlView;

    invoke-direct {v2, p2, p6, v0, v1}, Lcom/google/android/youtube/app/ui/RemoteControlView;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;Lcom/google/android/youtube/core/ui/q;Landroid/view/View$OnClickListener;)V

    iput-object v2, p0, Lcom/google/android/youtube/app/ui/bh;->x:Lcom/google/android/youtube/app/ui/RemoteControlView;

    new-instance v2, Lcom/google/android/youtube/app/ui/FixedAspectRatioFrameLayout;

    invoke-direct {v2, p2}, Lcom/google/android/youtube/app/ui/FixedAspectRatioFrameLayout;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/bh;->x:Lcom/google/android/youtube/app/ui/RemoteControlView;

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/app/ui/FixedAspectRatioFrameLayout;->addView(Landroid/view/View;)V

    const/high16 v3, 0x40000000

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/app/ui/FixedAspectRatioFrameLayout;->setAspectRatio(F)V

    invoke-interface {p6, p0}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->setListener(Lcom/google/android/youtube/core/player/overlay/e;)V

    new-instance v3, Lcom/google/android/youtube/app/adapter/cn;

    invoke-direct {v3, v2}, Lcom/google/android/youtube/app/adapter/cn;-><init>(Landroid/view/View;)V

    iput-object v3, p0, Lcom/google/android/youtube/app/ui/bh;->A:Lcom/google/android/youtube/app/adapter/cn;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bh;->A:Lcom/google/android/youtube/app/adapter/cn;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/app/adapter/cn;->c(Z)V

    invoke-interface {p7, p0}, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;->setListener(Lcom/google/android/youtube/core/player/overlay/e;)V

    new-instance v2, Lcom/google/android/youtube/app/ui/RemoteControlView;

    invoke-direct {v2, p2, p7, v0, v1}, Lcom/google/android/youtube/app/ui/RemoteControlView;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;Lcom/google/android/youtube/core/ui/q;Landroid/view/View$OnClickListener;)V

    iput-object v2, p0, Lcom/google/android/youtube/app/ui/bh;->y:Lcom/google/android/youtube/app/ui/RemoteControlView;

    const v0, 0x7f0700f3

    invoke-virtual {p2, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->B:Landroid/widget/FrameLayout;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->B:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bh;->y:Lcom/google/android/youtube/app/ui/RemoteControlView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->B:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    const v0, 0x7f07013f

    invoke-virtual {p2, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->D:Landroid/view/View;

    new-instance v0, Lcom/google/android/youtube/app/ui/bi;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/ui/bi;-><init>(Lcom/google/android/youtube/app/ui/bh;)V

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->f:Lcom/google/android/youtube/core/async/n;

    new-instance v0, Lcom/google/android/youtube/app/ui/bj;

    invoke-virtual {p2}, Landroid/app/Activity;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1, p2}, Lcom/google/android/youtube/app/ui/bj;-><init>(Lcom/google/android/youtube/app/ui/bh;Landroid/os/Looper;Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->i:Landroid/os/Handler;

    return-void
.end method

.method private A()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->v()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/ui/bh;->a(Lcom/google/android/youtube/app/remote/RemoteControl$State;)V

    return-void
.end method

.method private B()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->m:Lcom/google/android/youtube/core/model/Video;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->z:Lcom/google/android/youtube/app/ui/RemoteControlView;

    invoke-virtual {v0, v3, v3, v3}, Lcom/google/android/youtube/app/ui/RemoteControlView;->setTimes(III)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->m:Lcom/google/android/youtube/core/model/Video;

    iget v0, v0, Lcom/google/android/youtube/core/model/Video;->duration:I

    mul-int/lit16 v0, v0, 0x3e8

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bh;->m:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v2}, Lcom/google/android/youtube/app/remote/RemoteControl;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->f()Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v1

    sget-object v2, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->ENDED:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->i()D

    move-result-wide v1

    double-to-int v1, v1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/app/ui/bh;->v:I

    :goto_1
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bh;->z:Lcom/google/android/youtube/app/ui/RemoteControlView;

    iget v2, p0, Lcom/google/android/youtube/app/ui/bh;->v:I

    const/16 v3, 0x64

    invoke-virtual {v1, v2, v0, v3}, Lcom/google/android/youtube/app/ui/RemoteControlView;->setTimes(III)V

    goto :goto_0

    :cond_1
    iput v0, p0, Lcom/google/android/youtube/app/ui/bh;->v:I

    goto :goto_1

    :cond_2
    iput v3, p0, Lcom/google/android/youtube/app/ui/bh;->v:I

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/app/ui/RemoteControlView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->x:Lcom/google/android/youtube/app/ui/RemoteControlView;

    return-object v0
.end method

.method public static a(Lcom/google/android/youtube/core/player/as;Landroid/app/Activity;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/bl;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;Landroid/view/View;Lcom/google/android/youtube/app/ui/bp;Lcom/google/android/youtube/app/ui/bq;)Lcom/google/android/youtube/app/ui/bh;
    .locals 12

    new-instance v0, Lcom/google/android/youtube/app/ui/bh;

    const/4 v11, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v11}, Lcom/google/android/youtube/app/ui/bh;-><init>(Lcom/google/android/youtube/core/player/as;Landroid/app/Activity;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/bl;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;Landroid/view/View;Lcom/google/android/youtube/app/ui/bp;Lcom/google/android/youtube/app/ui/bq;Z)V

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/bh;Z)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/ui/bh;->q:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/app/ui/RemoteControlView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->y:Lcom/google/android/youtube/app/ui/RemoteControlView;

    return-object v0
.end method

.method private b(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->B:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->D:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->D:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->f()Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->ENDED:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bh;->y:Lcom/google/android/youtube/app/ui/RemoteControlView;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/ui/RemoteControlView;->setAreQueueOpsEnabled(Z)V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bh;->x:Lcom/google/android/youtube/app/ui/RemoteControlView;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/ui/RemoteControlView;->setAreQueueOpsEnabled(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/ui/bh;)Z
    .locals 2

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bh;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->m:Lcom/google/android/youtube/core/model/Video;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->m:Lcom/google/android/youtube/core/model/Video;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->f()Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/youtube/app/ui/bh;->a:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->f()Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/ui/bh;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bh;->B()V

    return-void
.end method

.method static synthetic e(Lcom/google/android/youtube/app/ui/bh;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->i:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/youtube/app/ui/bh;)Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bh;->z()Z

    move-result v0

    return v0
.end method

.method static synthetic g(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/core/model/Video;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->m:Lcom/google/android/youtube/core/model/Video;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/app/remote/RemoteControl;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/youtube/app/ui/bh;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->b:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/app/ui/RemoteControlView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->z:Lcom/google/android/youtube/app/ui/RemoteControlView;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/core/Analytics;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->e:Lcom/google/android/youtube/core/Analytics;

    return-object v0
.end method

.method static synthetic l(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/app/ui/bq;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->j:Lcom/google/android/youtube/app/ui/bq;

    return-object v0
.end method

.method static synthetic m(Lcom/google/android/youtube/app/ui/bh;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/bh;->q:Z

    return v0
.end method

.method static synthetic n(Lcom/google/android/youtube/app/ui/bh;)Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->v()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->OFFLINE:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic o(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/core/player/as;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->h:Lcom/google/android/youtube/core/player/as;

    return-object v0
.end method

.method private w()V
    .locals 4

    const/16 v0, 0x8

    const/4 v3, 0x1

    iget-boolean v1, p0, Lcom/google/android/youtube/app/ui/bh;->t:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bh;->h:Lcom/google/android/youtube/core/player/as;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/as;->g()V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bh;->h:Lcom/google/android/youtube/core/player/as;

    new-instance v2, Lcom/google/android/youtube/app/ui/bn;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/app/ui/bn;-><init>(Lcom/google/android/youtube/app/ui/bh;)V

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/core/player/as;->a(Landroid/os/Handler;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bh;->C:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bh;->A:Lcom/google/android/youtube/app/adapter/cn;

    invoke-virtual {v1, v3}, Lcom/google/android/youtube/app/adapter/cn;->c(Z)V

    iget-boolean v1, p0, Lcom/google/android/youtube/app/ui/bh;->u:Z

    if-eqz v1, :cond_1

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/bh;->b(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->j:Lcom/google/android/youtube/app/ui/bq;

    invoke-interface {v0, v3}, Lcom/google/android/youtube/app/ui/bq;->i(Z)V

    iput-boolean v3, p0, Lcom/google/android/youtube/app/ui/bh;->t:Z

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private x()Ljava/lang/String;
    .locals 5

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bh;->m:Lcom/google/android/youtube/core/model/Video;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->b(Lcom/google/android/youtube/core/model/Video;)Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->t()Lcom/google/android/youtube/app/remote/bb;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->b:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bh;->m:Lcom/google/android/youtube/core/model/Video;

    invoke-interface {v1, v2}, Lcom/google/android/youtube/app/remote/RemoteControl;->b(Lcom/google/android/youtube/core/model/Video;)Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;->getStringId()I

    move-result v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v4}, Lcom/google/android/youtube/app/remote/RemoteControl;->t()Lcom/google/android/youtube/app/remote/bb;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/youtube/app/remote/bb;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->b:Landroid/app/Activity;

    const v1, 0x7f0b0013

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private y()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->t()Lcom/google/android/youtube/app/remote/bb;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->t()Lcom/google/android/youtube/app/remote/bb;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/bb;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private z()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->v()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bh;->z()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->m:Lcom/google/android/youtube/core/model/Video;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bh;->m:Lcom/google/android/youtube/core/model/Video;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->a(Lcom/google/android/youtube/core/model/Video;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->e:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "RemotePlay"

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->c()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->z:Lcom/google/android/youtube/app/ui/RemoteControlView;

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->PLAYING:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/RemoteControlView;->a(Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bh;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->m:Lcom/google/android/youtube/core/model/Video;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->e:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "RemotePlayVideo"

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bh;->m:Lcom/google/android/youtube/core/model/Video;

    iget-object v2, v2, Lcom/google/android/youtube/core/model/Video;->categoryLabel:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/ui/bh;->q:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bh;->m:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->z:Lcom/google/android/youtube/app/ui/RemoteControlView;

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->UNCONFIRMED:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/RemoteControlView;->a(Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;)V

    goto :goto_0
.end method

.method public final a(I)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bh;->z()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/app/remote/RemoteControl;->b(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->m:Lcom/google/android/youtube/core/model/Video;

    iget v0, v0, Lcom/google/android/youtube/core/model/Video;->duration:I

    mul-int/lit16 v0, v0, 0x3e8

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->c()V

    goto :goto_0
.end method

.method public final a(Landroid/content/res/Configuration;I)V
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v2, v0, :cond_3

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/youtube/app/ui/bh;->u:Z

    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/bh;->u:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->x:Lcom/google/android/youtube/app/ui/RemoteControlView;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->z:Lcom/google/android/youtube/app/ui/RemoteControlView;

    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/bh;->u:Z

    if-eqz v0, :cond_1

    const/16 v1, 0x8

    :cond_1
    invoke-direct {p0, v1}, Lcom/google/android/youtube/app/ui/bh;->b(I)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bh;->A()V

    :cond_2
    return-void

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->y:Lcom/google/android/youtube/app/ui/RemoteControlView;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->z:Lcom/google/android/youtube/app/ui/RemoteControlView;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0e000a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->y:Lcom/google/android/youtube/app/ui/RemoteControlView;

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/app/ui/RemoteControlView;->setPlayingOnMarginTop(I)V

    goto :goto_1
.end method

.method public final a(Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;Ljava/lang/String;)V
    .locals 5

    const/4 v4, 0x2

    const/4 v1, 0x1

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/bh;->w:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->m:Lcom/google/android/youtube/core/model/Video;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->m:Lcom/google/android/youtube/core/model/Video;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/bh;->n:Z

    if-nez v0, :cond_0

    invoke-direct {p0, p2}, Lcom/google/android/youtube/app/ui/bh;->c(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bh;->z:Lcom/google/android/youtube/app/ui/RemoteControlView;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->j()Ljava/util/List;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/bh;->m:Lcom/google/android/youtube/core/model/Video;

    iget-object v3, v3, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Lcom/google/android/youtube/app/ui/RemoteControlView;->a(Z)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->m:Lcom/google/android/youtube/core/model/Video;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->m:Lcom/google/android/youtube/core/model/Video;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->w:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    :cond_2
    :goto_1
    return-void

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bh;->z()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bh;->m:Lcom/google/android/youtube/core/model/Video;

    invoke-interface {v0, v2}, Lcom/google/android/youtube/app/remote/RemoteControl;->a(Lcom/google/android/youtube/core/model/Video;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->z:Lcom/google/android/youtube/app/ui/RemoteControlView;

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bh;->x()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/bh;->m:Lcom/google/android/youtube/core/model/Video;

    invoke-interface {v2, v3}, Lcom/google/android/youtube/app/remote/RemoteControl;->b(Lcom/google/android/youtube/core/model/Video;)Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;->canRetry()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/ui/RemoteControlView;->a(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->i:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_1

    :cond_5
    sget-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->ERROR:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-eq p1, v0, :cond_9

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->z:Lcom/google/android/youtube/app/ui/RemoteControlView;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v2}, Lcom/google/android/youtube/app/remote/RemoteControl;->l()Lcom/google/android/youtube/app/remote/am;

    move-result-object v2

    invoke-virtual {v0, p1, v2}, Lcom/google/android/youtube/app/ui/RemoteControlView;->a(Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;Lcom/google/android/youtube/app/remote/am;)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bh;->B()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->m:Lcom/google/android/youtube/core/model/Video;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v2}, Lcom/google/android/youtube/app/remote/RemoteControl;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->i:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->i:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bh;->i:Landroid/os/Handler;

    invoke-static {v2, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_6
    :goto_2
    sget-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->BUFFERING:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-eq p1, v0, :cond_7

    sget-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->UNSTARTED:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-eq p1, v0, :cond_7

    sget-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->UNCONFIRMED:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-ne p1, v0, :cond_a

    :cond_7
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->i:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->i:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bh;->i:Landroid/os/Handler;

    invoke-static {v1, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_1

    :cond_8
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->i:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_2

    :cond_9
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->b:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v2}, Lcom/google/android/youtube/app/remote/RemoteControl;->m()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bh;->z:Lcom/google/android/youtube/app/ui/RemoteControlView;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/youtube/app/ui/RemoteControlView;->a(Ljava/lang/String;Z)V

    goto :goto_2

    :cond_a
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->i:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    goto/16 :goto_1
.end method

.method public final a(Lcom/google/android/youtube/app/remote/RemoteControl$State;)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->m:Lcom/google/android/youtube/core/model/Video;

    if-nez v0, :cond_1

    const-string v0, "RemoteControlHelper was not initialized with a video"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->m:Lcom/google/android/youtube/core/model/Video;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->m:Lcom/google/android/youtube/core/model/Video;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Video;->captionTracksUri:Landroid/net/Uri;

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    iget-object v3, p0, Lcom/google/android/youtube/app/ui/bh;->x:Lcom/google/android/youtube/app/ui/RemoteControlView;

    invoke-virtual {v3, v0}, Lcom/google/android/youtube/app/ui/RemoteControlView;->setHasCc(Z)V

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/bh;->y:Lcom/google/android/youtube/app/ui/RemoteControlView;

    invoke-virtual {v3, v0}, Lcom/google/android/youtube/app/ui/RemoteControlView;->setHasCc(Z)V

    :cond_2
    sget-object v0, Lcom/google/android/youtube/app/ui/bo;->a:[I

    invoke-virtual {p1}, Lcom/google/android/youtube/app/remote/RemoteControl$State;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    :goto_2
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->m:Lcom/google/android/youtube/core/model/Video;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/bh;->p:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->m:Lcom/google/android/youtube/core/model/Video;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Video;->hqThumbnailUri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lcom/google/android/youtube/app/ui/bh;->p:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->c:Lcom/google/android/youtube/core/client/be;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bh;->m:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->hqThumbnailUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bh;->f:Lcom/google/android/youtube/core/async/n;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/be;->a(Landroid/net/Uri;Lcom/google/android/youtube/core/async/n;)V

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :pswitch_1
    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/bh;->t:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$State;->ERROR:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-eq p1, v0, :cond_5

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->e:Lcom/google/android/youtube/core/Analytics;

    const-string v3, "RemoteStateChange"

    invoke-virtual {p1}, Lcom/google/android/youtube/app/remote/RemoteControl$State;->name()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_3
    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/bh;->t:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->A:Lcom/google/android/youtube/app/adapter/cn;

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/adapter/cn;->c(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->B:Landroid/widget/FrameLayout;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->D:Landroid/view/View;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->D:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_4
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->C:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iput-boolean v2, p0, Lcom/google/android/youtube/app/ui/bh;->t:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->j:Lcom/google/android/youtube/app/ui/bq;

    invoke-interface {v0, v2}, Lcom/google/android/youtube/app/ui/bq;->i(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->i:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->e:Lcom/google/android/youtube/core/Analytics;

    const-string v3, "RemoteError"

    iget-object v4, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v4}, Lcom/google/android/youtube/app/remote/RemoteControl;->w()Lcom/google/android/youtube/app/remote/an;

    move-result-object v4

    iget-object v4, v4, Lcom/google/android/youtube/app/remote/an;->a:Ljava/lang/String;

    invoke-interface {v0, v3, v4}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bh;->w()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->z:Lcom/google/android/youtube/app/ui/RemoteControlView;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/RemoteControlView;->a()V

    goto :goto_2

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/ui/bh;->a(Ljava/lang/String;)V

    goto :goto_2

    :pswitch_4
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bh;->w()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->z:Lcom/google/android/youtube/app/ui/RemoteControlView;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v2}, Lcom/google/android/youtube/app/remote/RemoteControl;->w()Lcom/google/android/youtube/app/remote/an;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v3}, Lcom/google/android/youtube/app/remote/RemoteControl;->t()Lcom/google/android/youtube/app/remote/bb;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/youtube/app/remote/bb;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/youtube/app/ui/RemoteControlView;->a(Lcom/google/android/youtube/app/remote/an;Ljava/lang/String;)V

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final a(Lcom/google/android/youtube/app/remote/RemoteControl;)V
    .locals 2

    const-string v0, "remoteControl cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    if-ne v0, p1, :cond_0

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/bh;->m()V

    :cond_1
    iput-object p1, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {p1}, Lcom/google/android/youtube/app/remote/RemoteControl;->k()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/ui/bh;->r:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->x:Lcom/google/android/youtube/app/ui/RemoteControlView;

    iget-boolean v1, p0, Lcom/google/android/youtube/app/ui/bh;->r:Z

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/RemoteControlView;->setHasQueueButton(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->y:Lcom/google/android/youtube/app/ui/RemoteControlView;

    iget-boolean v1, p0, Lcom/google/android/youtube/app/ui/bh;->r:Z

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/RemoteControlView;->setHasQueueButton(Z)V

    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/bh;->s:Z

    if-eqz v0, :cond_2

    invoke-interface {p1, p0}, Lcom/google/android/youtube/app/remote/RemoteControl;->a(Lcom/google/android/youtube/app/remote/al;)V

    invoke-interface {p1, p0}, Lcom/google/android/youtube/app/remote/RemoteControl;->c(Lcom/google/android/youtube/app/remote/al;)V

    :cond_2
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bh;->A()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/youtube/core/model/SubtitleTrack;)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/SubtitleTrack;->isDisableOption()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bh;->x:Lcom/google/android/youtube/app/ui/RemoteControlView;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/ui/RemoteControlView;->setCcEnabled(Z)V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bh;->y:Lcom/google/android/youtube/app/ui/RemoteControlView;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/ui/RemoteControlView;->setCcEnabled(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/youtube/core/model/Video;)V
    .locals 2

    const-string v0, "video can not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->m:Lcom/google/android/youtube/core/model/Video;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bh;->m:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/ui/bh;->p:Z

    :cond_0
    iput-object p1, p0, Lcom/google/android/youtube/app/ui/bh;->m:Lcom/google/android/youtube/core/model/Video;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bh;->A()V

    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bh;->m:Lcom/google/android/youtube/core/model/Video;

    if-nez v2, :cond_0

    const-string v0, "Video is null"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bh;->z()Z

    move-result v2

    if-nez v2, :cond_1

    const-string v0, "Video changed received for a non connected remote. Will ignore"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bh;->w()V

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bh;->m:Lcom/google/android/youtube/core/model/Video;

    iget-object v2, v2, Lcom/google/android/youtube/core/model/Video;->state:Lcom/google/android/youtube/core/model/Video$State;

    sget-object v3, Lcom/google/android/youtube/core/model/Video$State;->NOT_AVAILABLE_ON_MOBILE:Lcom/google/android/youtube/core/model/Video$State;

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bh;->b:Landroid/app/Activity;

    const v3, 0x7f0b0257

    new-array v0, v0, [Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bh;->y()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v1

    invoke-virtual {v2, v3, v0}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/ui/bh;->b(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v2}, Lcom/google/android/youtube/app/remote/RemoteControl;->t()Lcom/google/android/youtube/app/remote/bb;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bh;->b:Landroid/app/Activity;

    const v3, 0x7f0b0262

    new-array v4, v0, [Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bh;->y()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-virtual {v2, v3, v4}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/bh;->y:Lcom/google/android/youtube/app/ui/RemoteControlView;

    invoke-virtual {v3, v2}, Lcom/google/android/youtube/app/ui/RemoteControlView;->setPlayingOnText(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/bh;->x:Lcom/google/android/youtube/app/ui/RemoteControlView;

    invoke-virtual {v3, v2}, Lcom/google/android/youtube/app/ui/RemoteControlView;->setPlayingOnText(Ljava/lang/String;)V

    :cond_3
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v2}, Lcom/google/android/youtube/app/remote/RemoteControl;->s()Lcom/google/android/youtube/core/model/SubtitleTrack;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/app/ui/bh;->a(Lcom/google/android/youtube/core/model/SubtitleTrack;)V

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/ui/bh;->c(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bh;->m:Lcom/google/android/youtube/core/model/Video;

    iget-object v2, v2, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->f()Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/google/android/youtube/app/ui/bh;->a(Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    iget-boolean v2, p0, Lcom/google/android/youtube/app/ui/bh;->n:Z

    if-eqz v2, :cond_5

    iput v1, p0, Lcom/google/android/youtube/app/ui/bh;->v:I

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->z:Lcom/google/android/youtube/app/ui/RemoteControlView;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bh;->o:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/youtube/app/ui/RemoteControlView;->a(Ljava/lang/String;Z)V

    goto/16 :goto_0

    :cond_5
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/youtube/app/ui/bh;->w:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    iput v1, p0, Lcom/google/android/youtube/app/ui/bh;->v:I

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bh;->z:Lcom/google/android/youtube/app/ui/RemoteControlView;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v3}, Lcom/google/android/youtube/app/remote/RemoteControl;->j()Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/app/ui/bh;->m:Lcom/google/android/youtube/core/model/Video;

    iget-object v4, v4, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    :goto_1
    invoke-virtual {v2, v0}, Lcom/google/android/youtube/app/ui/RemoteControlView;->a(Z)V

    goto/16 :goto_0

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method public final a(Ljava/util/List;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->m:Lcom/google/android/youtube/core/model/Video;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/bh;->r:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->m:Lcom/google/android/youtube/core/model/Video;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bh;->z:Lcom/google/android/youtube/app/ui/RemoteControlView;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->m:Lcom/google/android/youtube/core/model/Video;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/ui/RemoteControlView;->b(Z)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a_(Lcom/google/android/youtube/core/model/SubtitleTrack;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/app/remote/RemoteControl;->a(Lcom/google/android/youtube/core/model/SubtitleTrack;)V

    return-void
.end method

.method public final b()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bh;->z()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->e:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "RemotePause"

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->d()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->z:Lcom/google/android/youtube/app/ui/RemoteControlView;

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->PAUSED:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/RemoteControlView;->a(Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/ui/bh;->n:Z

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/bh;->o:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bh;->z()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->z:Lcom/google/android/youtube/app/ui/RemoteControlView;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/google/android/youtube/app/ui/RemoteControlView;->a(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->k:Lcom/google/android/youtube/app/ui/bp;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/app/ui/bp;->a_(Z)V

    return-void
.end method

.method public final c()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bh;->z()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->d()V

    goto :goto_0
.end method

.method public final d()V
    .locals 0

    return-void
.end method

.method public final e()V
    .locals 0

    return-void
.end method

.method public final f()V
    .locals 0

    return-void
.end method

.method public final g()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->d:Lcom/google/android/youtube/core/client/bl;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bh;->m:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bh;->b:Landroid/app/Activity;

    new-instance v3, Lcom/google/android/youtube/app/ui/bk;

    invoke-direct {v3, p0}, Lcom/google/android/youtube/app/ui/bk;-><init>(Lcom/google/android/youtube/app/ui/bh;)V

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/bl;->a(Ljava/lang/String;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method

.method public final h()V
    .locals 0

    return-void
.end method

.method public final i()V
    .locals 0

    return-void
.end method

.method public final j()V
    .locals 0

    return-void
.end method

.method public final k()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bh;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->c()V

    :cond_0
    return-void
.end method

.method public final l()V
    .locals 0

    return-void
.end method

.method public final m()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/app/remote/RemoteControl;->b(Lcom/google/android/youtube/app/remote/al;)V

    sget-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$State;->OFFLINE:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/ui/bh;->a(Lcom/google/android/youtube/app/remote/RemoteControl$State;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    :cond_0
    return-void
.end method

.method public final n()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->v()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->m:Lcom/google/android/youtube/core/model/Video;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bh;->m:Lcom/google/android/youtube/core/model/Video;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->a(Lcom/google/android/youtube/core/model/Video;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->z:Lcom/google/android/youtube/app/ui/RemoteControlView;

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bh;->x()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/bh;->m:Lcom/google/android/youtube/core/model/Video;

    invoke-interface {v2, v3}, Lcom/google/android/youtube/app/remote/RemoteControl;->b(Lcom/google/android/youtube/core/model/Video;)Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;->canRetry()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/ui/RemoteControlView;->a(Ljava/lang/String;Z)V

    :cond_0
    return-void
.end method

.method public final o()V
    .locals 3

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/bh;->s:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    const-string v2, "cannot call onResume() multiple times, need to call onPause() first"

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/utils/s;->b(ZLjava/lang/Object;)V

    iput-boolean v1, p0, Lcom/google/android/youtube/app/ui/bh;->s:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/app/remote/RemoteControl;->a(Lcom/google/android/youtube/app/remote/al;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/app/remote/RemoteControl;->c(Lcom/google/android/youtube/app/remote/al;)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bh;->A()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final p()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/bh;->s:Z

    const-string v1, "cannot call onPause() multiple times, need to call onResume() first"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->b(ZLjava/lang/Object;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/ui/bh;->s:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/app/remote/RemoteControl;->b(Lcom/google/android/youtube/app/remote/al;)V

    :cond_0
    return-void
.end method

.method public final q()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->m:Lcom/google/android/youtube/core/model/Video;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->m:Lcom/google/android/youtube/core/model/Video;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final r()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/bh;->t:Z

    return v0
.end method

.method public final s()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final t()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/app/ui/bh;->v:I

    return v0
.end method

.method public final u()Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->w:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    return-object v0
.end method

.method public final v()Lcom/google/android/youtube/app/adapter/cn;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bh;->A:Lcom/google/android/youtube/app/adapter/cn;

    return-object v0
.end method
