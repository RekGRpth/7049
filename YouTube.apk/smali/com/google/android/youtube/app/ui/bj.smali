.class final Lcom/google/android/youtube/app/ui/bj;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/app/Activity;

.field final synthetic b:Lcom/google/android/youtube/app/ui/bh;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/ui/bh;Landroid/os/Looper;Landroid/app/Activity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/bj;->b:Lcom/google/android/youtube/app/ui/bh;

    iput-object p3, p0, Lcom/google/android/youtube/app/ui/bj;->a:Landroid/app/Activity;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 5

    const/4 v2, 0x1

    const/4 v4, 0x0

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bj;->b:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/bh;->c(Lcom/google/android/youtube/app/ui/bh;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bj;->b:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/bh;->d(Lcom/google/android/youtube/app/ui/bh;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bj;->b:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/bh;->e(Lcom/google/android/youtube/app/ui/bh;)Landroid/os/Handler;

    move-result-object v0

    invoke-static {p0, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bj;->b:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/bh;->f(Lcom/google/android/youtube/app/ui/bh;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bj;->b:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/bh;->g(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/core/model/Video;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bj;->b:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/bh;->g(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/core/model/Video;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bj;->b:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/bh;->h(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bj;->b:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/bh;->h(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->f()Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->UNCONFIRMED:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bj;->b:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/bh;->h(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->f()Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->UNSTARTED:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bj;->b:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/bh;->h(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->f()Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->BUFFERING:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-ne v0, v1, :cond_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bj;->a:Landroid/app/Activity;

    const v1, 0x7f0b0252

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/bj;->b:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v3}, Lcom/google/android/youtube/app/ui/bh;->h(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/youtube/app/remote/RemoteControl;->t()Lcom/google/android/youtube/app/remote/bb;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/youtube/app/remote/bb;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bj;->b:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/bh;->a(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/app/ui/RemoteControlView;

    move-result-object v1

    invoke-virtual {v1, v0, v4}, Lcom/google/android/youtube/app/ui/RemoteControlView;->a(Ljava/lang/String;Z)V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bj;->b:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/bh;->b(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/app/ui/RemoteControlView;

    move-result-object v1

    invoke-virtual {v1, v0, v4}, Lcom/google/android/youtube/app/ui/RemoteControlView;->a(Ljava/lang/String;Z)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
