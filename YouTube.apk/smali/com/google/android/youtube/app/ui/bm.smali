.class final Lcom/google/android/youtube/app/ui/bm;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/ui/q;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/ui/bh;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/ui/bh;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v2}, Lcom/google/android/youtube/app/ui/bh;->h(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v2

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v2}, Lcom/google/android/youtube/app/ui/bh;->h(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/youtube/app/remote/RemoteControl;->v()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v2

    sget-object v3, Lcom/google/android/youtube/app/remote/RemoteControl$State;->ERROR:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-eq v2, v3, :cond_1

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v2}, Lcom/google/android/youtube/app/ui/bh;->h(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/youtube/app/remote/RemoteControl;->f()Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v2

    sget-object v3, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->ERROR:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-ne v2, v3, :cond_4

    :cond_1
    move v2, v1

    :goto_1
    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v2}, Lcom/google/android/youtube/app/ui/bh;->h(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/youtube/app/remote/RemoteControl;->f()Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v2

    sget-object v3, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->ERROR:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-eq v2, v3, :cond_2

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v2}, Lcom/google/android/youtube/app/ui/bh;->h(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/youtube/app/remote/RemoteControl;->w()Lcom/google/android/youtube/app/remote/an;

    move-result-object v2

    iget-boolean v2, v2, Lcom/google/android/youtube/app/remote/an;->c:Z

    if-eqz v2, :cond_3

    :cond_2
    move v0, v1

    :cond_3
    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/bh;->h(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/bh;->g(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/core/model/Video;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    move v2, v0

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/bh;->l(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/app/ui/bq;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/ui/bq;->x()V

    goto :goto_0

    :cond_6
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/bh;->h(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v2}, Lcom/google/android/youtube/app/ui/bh;->g(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/core/model/Video;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/youtube/app/remote/RemoteControl;->a(Lcom/google/android/youtube/core/model/Video;)Z

    move-result v1

    if-nez v1, :cond_8

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/bh;->h(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/bh;->g(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/core/model/Video;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->b(Lcom/google/android/youtube/core/model/Video;)Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;->canRetry()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/bh;->h(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/bh;->g(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/core/model/Video;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/bh;->l(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/app/ui/bq;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/ui/bq;->x()V

    goto/16 :goto_0

    :cond_8
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/bh;->h(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->v()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v1

    sget-object v2, Lcom/google/android/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-eq v1, v2, :cond_9

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bh;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v2}, Lcom/google/android/youtube/app/ui/bh;->h(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/youtube/app/remote/RemoteControl;->v()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/ui/bh;->a(Lcom/google/android/youtube/app/remote/RemoteControl$State;)V

    :cond_9
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/bh;->g(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/core/model/Video;

    move-result-object v1

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/bh;->g(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/core/model/Video;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v2}, Lcom/google/android/youtube/app/ui/bh;->h(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/youtube/app/remote/RemoteControl;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    :cond_a
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bh;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v2}, Lcom/google/android/youtube/app/ui/bh;->h(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/youtube/app/remote/RemoteControl;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/ui/bh;->a(Ljava/lang/String;)V

    :cond_b
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/bh;->g(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/core/model/Video;

    move-result-object v1

    if-eqz v1, :cond_c

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/bh;->h(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v2}, Lcom/google/android/youtube/app/ui/bh;->g(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/core/model/Video;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/youtube/app/remote/RemoteControl;->a(Lcom/google/android/youtube/core/model/Video;)Z

    move-result v1

    if-nez v1, :cond_c

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/bh;->h(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/bh;->g(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/core/model/Video;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_c
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/bh;->h(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->f()Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v1

    sget-object v2, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->UNCONFIRMED:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-eq v1, v2, :cond_d

    sget-object v2, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->UNSTARTED:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-eq v1, v2, :cond_d

    sget-object v2, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->BUFFERING:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-ne v1, v2, :cond_f

    :cond_d
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/bh;->m(Lcom/google/android/youtube/app/ui/bh;)Z

    move-result v1

    if-eqz v1, :cond_e

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/bh;->h(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->e()V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v1, v0}, Lcom/google/android/youtube/app/ui/bh;->a(Lcom/google/android/youtube/app/ui/bh;Z)Z

    goto/16 :goto_0

    :cond_e
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/bh;->l(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/app/ui/bq;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/ui/bq;->x()V

    goto/16 :goto_0

    :cond_f
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bh;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v2}, Lcom/google/android/youtube/app/ui/bh;->h(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/youtube/app/remote/RemoteControl;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/ui/bh;->a(Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
