.class public final Lcom/google/android/youtube/app/ui/bu;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/google/android/youtube/core/async/n;
.implements Lcom/google/android/youtube/core/ui/h;


# instance fields
.field private final a:Lcom/google/android/youtube/core/ui/PagedView;

.field private final b:Lcom/google/android/youtube/core/async/au;

.field private final c:Lcom/google/android/youtube/core/e;

.field private final d:Lcom/google/android/youtube/core/a/j;

.field private final e:Lcom/google/android/youtube/core/ui/i;

.field private final f:Lcom/google/android/youtube/core/async/h;

.field private final g:Landroid/os/Handler;

.field private h:Ljava/util/List;

.field private i:I

.field private j:Ljava/util/List;

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:Z

.field private final p:Lcom/google/android/youtube/app/d;

.field private final q:Lcom/google/android/youtube/core/Analytics;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/PagedView;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/Analytics;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "activity cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "targetAdapter cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "analytics can not be null"

    invoke-static {p7, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/Analytics;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->q:Lcom/google/android/youtube/core/Analytics;

    const-string v0, "view cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/ui/PagedView;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->a:Lcom/google/android/youtube/core/ui/PagedView;

    const-string v0, "requester cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/au;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->b:Lcom/google/android/youtube/core/async/au;

    const-string v0, "errorHelper cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/e;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->c:Lcom/google/android/youtube/core/e;

    const-string v0, "navigation cannot be null"

    invoke-static {p6, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/d;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->p:Lcom/google/android/youtube/app/d;

    invoke-interface {p2}, Lcom/google/android/youtube/core/ui/PagedView;->h()Lcom/google/android/youtube/core/ui/i;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->e:Lcom/google/android/youtube/core/ui/i;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->e:Lcom/google/android/youtube/core/ui/i;

    invoke-interface {v0}, Lcom/google/android/youtube/core/ui/i;->a()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p3, v0, v1}, Lcom/google/android/youtube/core/a/j;->a(Lcom/google/android/youtube/core/a/a;Landroid/view/View;Z)Lcom/google/android/youtube/core/a/j;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->d:Lcom/google/android/youtube/core/a/j;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->a:Lcom/google/android/youtube/core/ui/PagedView;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bu;->d:Lcom/google/android/youtube/core/a/j;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/ui/PagedView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->a:Lcom/google/android/youtube/core/ui/PagedView;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/core/ui/PagedView;->setOnScrollListener(Lcom/google/android/youtube/core/ui/h;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->a:Lcom/google/android/youtube/core/ui/PagedView;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/core/ui/PagedView;->setOnRetryClickListener(Landroid/view/View$OnClickListener;)V

    instance-of v0, p3, Lcom/google/android/youtube/app/ui/bx;

    if-eqz v0, :cond_0

    check-cast p3, Lcom/google/android/youtube/app/ui/bx;

    invoke-virtual {p3, p0}, Lcom/google/android/youtube/app/ui/bx;->a(Landroid/widget/AdapterView$OnItemClickListener;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->a:Lcom/google/android/youtube/core/ui/PagedView;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/core/ui/PagedView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->e:Lcom/google/android/youtube/core/ui/i;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/core/ui/i;->a(Landroid/view/View$OnClickListener;)V

    invoke-static {p1, p0}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->f:Lcom/google/android/youtube/core/async/h;

    new-instance v0, Lcom/google/android/youtube/app/ui/bv;

    invoke-virtual {p1}, Landroid/app/Activity;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/ui/bv;-><init>(Lcom/google/android/youtube/app/ui/bu;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->g:Landroid/os/Handler;

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->a:Lcom/google/android/youtube/core/ui/PagedView;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/core/ui/PagedView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/bu;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bu;->c()V

    return-void
.end method

.method private c()V
    .locals 5

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/google/android/youtube/app/ui/bu;->o:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/youtube/app/ui/bu;->i:I

    iget v2, p0, Lcom/google/android/youtube/app/ui/bu;->l:I

    if-lt v1, v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-boolean v0, p0, Lcom/google/android/youtube/app/ui/bu;->o:Z

    iget v1, p0, Lcom/google/android/youtube/app/ui/bu;->i:I

    mul-int/lit8 v1, v1, 0xf

    iput v1, p0, Lcom/google/android/youtube/app/ui/bu;->k:I

    iget v1, p0, Lcom/google/android/youtube/app/ui/bu;->k:I

    add-int/lit8 v1, v1, 0xf

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bu;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    iget v2, p0, Lcom/google/android/youtube/app/ui/bu;->k:I

    if-ge v2, v1, :cond_0

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/bu;->h:Ljava/util/List;

    iget v4, p0, Lcom/google/android/youtube/app/ui/bu;->k:I

    invoke-interface {v3, v4, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/android/youtube/app/ui/bu;->j:Ljava/util/List;

    iget v1, p0, Lcom/google/android/youtube/app/ui/bu;->i:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/youtube/app/ui/bu;->i:I

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bu;->d:Lcom/google/android/youtube/core/a/j;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/a/j;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v1}, Landroid/widget/ListAdapter;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v0, 0x1

    :cond_2
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->e:Lcom/google/android/youtube/core/ui/i;

    invoke-interface {v0}, Lcom/google/android/youtube/core/ui/i;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->d:Lcom/google/android/youtube/core/a/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/j;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->a:Lcom/google/android/youtube/core/ui/PagedView;

    invoke-interface {v0}, Lcom/google/android/youtube/core/ui/PagedView;->d()V

    :goto_1
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->b:Lcom/google/android/youtube/core/async/au;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bu;->j:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bu;->f:Lcom/google/android/youtube/core/async/h;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->a:Lcom/google/android/youtube/core/ui/PagedView;

    invoke-interface {v0}, Lcom/google/android/youtube/core/ui/PagedView;->e()V

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->a:Lcom/google/android/youtube/core/ui/PagedView;

    invoke-interface {v0}, Lcom/google/android/youtube/core/ui/PagedView;->e()V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/ui/PagedView;III)V
    .locals 4

    add-int v2, p2, p3

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->d:Lcom/google/android/youtube/core/a/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/j;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    const/4 v1, 0x1

    instance-of v3, v0, Lcom/google/android/youtube/app/ui/bx;

    if-eqz v3, :cond_1

    check-cast v0, Lcom/google/android/youtube/app/ui/bx;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/bx;->b()I

    move-result v0

    :goto_0
    mul-int/2addr v0, v2

    iput v0, p0, Lcom/google/android/youtube/app/ui/bu;->m:I

    iget v0, p0, Lcom/google/android/youtube/app/ui/bu;->m:I

    iget v1, p0, Lcom/google/android/youtube/app/ui/bu;->n:I

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->g:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2

    check-cast p1, Ljava/util/List;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Error for request "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->d:Lcom/google/android/youtube/core/a/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/j;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->isEmpty()Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bu;->c:Lcom/google/android/youtube/core/e;

    invoke-virtual {v1, p2}, Lcom/google/android/youtube/core/e;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->e:Lcom/google/android/youtube/core/ui/i;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/ui/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->d:Lcom/google/android/youtube/core/a/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/j;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->a:Lcom/google/android/youtube/core/ui/PagedView;

    invoke-interface {v0}, Lcom/google/android/youtube/core/ui/PagedView;->d()V

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->a:Lcom/google/android/youtube/core/ui/PagedView;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/ui/PagedView;->b(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 6

    check-cast p1, Ljava/util/List;

    check-cast p2, Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->j:Ljava/util/List;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget v0, p0, Lcom/google/android/youtube/app/ui/bu;->k:I

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/BatchEntry;

    iget v4, v0, Lcom/google/android/youtube/core/model/BatchEntry;->b:I

    const/16 v5, 0xc8

    if-ne v4, v5, :cond_0

    iget-object v0, v0, Lcom/google/android/youtube/core/model/BatchEntry;->a:Ljava/lang/Object;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    new-instance v4, Lcom/google/android/youtube/core/model/Video$Builder;

    invoke-direct {v4}, Lcom/google/android/youtube/core/model/Video$Builder;-><init>()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->h:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v0}, Lcom/google/android/youtube/core/model/Video$Builder;->id(Ljava/lang/String;)Lcom/google/android/youtube/core/model/Video$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/Video$Builder;->build()Lcom/google/android/youtube/core/model/Video;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    iget v0, p0, Lcom/google/android/youtube/app/ui/bu;->k:I

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/youtube/app/ui/bu;->n:I

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->d:Lcom/google/android/youtube/core/a/j;

    iget v1, p0, Lcom/google/android/youtube/app/ui/bu;->k:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/a/j;->a(ILjava/lang/Iterable;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->d:Lcom/google/android/youtube/core/a/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/j;->getCount()I

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->d:Lcom/google/android/youtube/core/a/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/j;->c()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->e:Lcom/google/android/youtube/core/ui/i;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/ui/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->e:Lcom/google/android/youtube/core/ui/i;

    invoke-interface {v0}, Lcom/google/android/youtube/core/ui/i;->c()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->a:Lcom/google/android/youtube/core/ui/PagedView;

    invoke-interface {v0}, Lcom/google/android/youtube/core/ui/PagedView;->d()V

    :goto_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/ui/bu;->o:Z

    iget v0, p0, Lcom/google/android/youtube/app/ui/bu;->m:I

    iget v1, p0, Lcom/google/android/youtube/app/ui/bu;->n:I

    if-le v0, v1, :cond_2

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bu;->c()V

    :cond_2
    return-void

    :cond_3
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->a:Lcom/google/android/youtube/core/ui/PagedView;

    invoke-interface {v0}, Lcom/google/android/youtube/core/ui/PagedView;->c()V

    goto :goto_2
.end method

.method public final a(Ljava/util/List;)V
    .locals 4

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/bu;->h:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->a:Lcom/google/android/youtube/core/ui/PagedView;

    invoke-interface {v0}, Lcom/google/android/youtube/core/ui/PagedView;->c()V

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/youtube/app/ui/bu;->i:I

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    int-to-double v0, v0

    const-wide/high16 v2, 0x3ff0000000000000L

    sub-double/2addr v0, v2

    const-wide/high16 v2, 0x402e000000000000L

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/youtube/app/ui/bu;->l:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/ui/bu;->o:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->d:Lcom/google/android/youtube/core/a/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/j;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/a/a;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/a;->c()I

    move-result v1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-le v1, v0, :cond_2

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    if-ge v0, v1, :cond_1

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/bu;->d:Lcom/google/android/youtube/core/a/j;

    invoke-virtual {v3, v0}, Lcom/google/android/youtube/core/a/j;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->d:Lcom/google/android/youtube/core/a/j;

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/a/j;->a(Ljava/util/Collection;)V

    :cond_2
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bu;->c()V

    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->d:Lcom/google/android/youtube/core/a/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/j;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->j:Ljava/util/List;

    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->j:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->b:Lcom/google/android/youtube/core/async/au;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bu;->j:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bu;->f:Lcom/google/android/youtube/core/async/h;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    :cond_0
    return-void
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->d:Lcom/google/android/youtube/core/a/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/j;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/a/a;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/a;->c()I

    move-result v0

    if-ge p3, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->d:Lcom/google/android/youtube/core/a/j;

    invoke-virtual {v0, p3}, Lcom/google/android/youtube/core/a/j;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Video;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bu;->p:Lcom/google/android/youtube/app/d;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->REMOTE_QUEUE:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/youtube/app/d;->a(Ljava/lang/String;ZLcom/google/android/youtube/core/client/VideoStats2Client$Feature;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bu;->q:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "RemoteQueueGoToWatch"

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    :cond_0
    return-void
.end method
