.class final Lcom/google/android/youtube/app/ui/ci;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/ui/bz;

.field private b:Z


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/ui/bz;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/ci;->a:Lcom/google/android/youtube/app/ui/bz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/app/ui/bz;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/ui/ci;-><init>(Lcom/google/android/youtube/app/ui/bz;)V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 5

    const/4 v4, 0x0

    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/ci;->b:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    const/4 v1, 0x3

    if-lt v0, v1, :cond_0

    new-array v1, v0, [C

    invoke-interface {p1, v4, v0, v1, v4}, Landroid/text/Editable;->getChars(II[CI)V

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>([C)V

    const-string v1, "\\D"

    const-string v3, ""

    invoke-virtual {v2, v1, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/ci;->a:Lcom/google/android/youtube/app/ui/bz;

    invoke-static {v3, v1}, Lcom/google/android/youtube/app/ui/bz;->b(Lcom/google/android/youtube/app/ui/bz;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {p1, v4, v0, v1}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    goto :goto_0
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    const/4 v0, 0x1

    if-ne p3, v0, :cond_0

    if-nez p4, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/youtube/app/ui/ci;->b:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
