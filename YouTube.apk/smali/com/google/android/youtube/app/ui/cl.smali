.class public final Lcom/google/android/youtube/app/ui/cl;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private final d:F

.field private final e:I

.field private final f:I

.field private final g:[I

.field private final h:I

.field private final i:I

.field private final j:I

.field private final k:I

.field private final l:I

.field private final m:I

.field private final n:I

.field private final o:I

.field private final p:Landroid/graphics/Bitmap;

.field private final q:Landroid/graphics/Bitmap;


# direct methods
.method private constructor <init>(IIII[IIIIIIIIIII)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-lez p1, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-string v2, "stackSize must be > 0"

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    if-lez p2, :cond_1

    const/4 v1, 0x1

    :goto_1
    const-string v2, "width must be > 0"

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    if-lez p3, :cond_2

    const/4 v1, 0x1

    :goto_2
    const-string v2, "height must be > 0"

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    if-lez p4, :cond_3

    const/4 v1, 0x1

    :goto_3
    const-string v2, "scale must be > 0"

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    array-length v1, p5

    if-lt v1, p1, :cond_4

    const/4 v1, 0x1

    :goto_4
    const-string v2, "rotationAngles count must be greater or equal than stackSize"

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    if-lez p9, :cond_5

    const/4 v1, 0x1

    :goto_5
    const-string v2, "outerBorder must be > 0"

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    if-lez p11, :cond_6

    const/4 v1, 0x1

    :goto_6
    const-string v2, "innerBorder must be > 0"

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    if-lez p13, :cond_7

    const/4 v1, 0x1

    :goto_7
    const-string v2, "defaultBitmapWidth must be > 0"

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    if-lez p14, :cond_8

    const/4 v1, 0x1

    :goto_8
    const-string v2, "defaultBitmapHeight must be > 0"

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    iput p1, p0, Lcom/google/android/youtube/app/ui/cl;->a:I

    iput p2, p0, Lcom/google/android/youtube/app/ui/cl;->b:I

    iput p3, p0, Lcom/google/android/youtube/app/ui/cl;->c:I

    int-to-float v1, p4

    const/high16 v2, 0x42c80000

    div-float/2addr v1, v2

    iput v1, p0, Lcom/google/android/youtube/app/ui/cl;->d:F

    iput-object p5, p0, Lcom/google/android/youtube/app/ui/cl;->g:[I

    iput p6, p0, Lcom/google/android/youtube/app/ui/cl;->h:I

    iput p7, p0, Lcom/google/android/youtube/app/ui/cl;->e:I

    iput p8, p0, Lcom/google/android/youtube/app/ui/cl;->f:I

    iput p9, p0, Lcom/google/android/youtube/app/ui/cl;->i:I

    iput p10, p0, Lcom/google/android/youtube/app/ui/cl;->j:I

    iput p11, p0, Lcom/google/android/youtube/app/ui/cl;->k:I

    move/from16 v0, p12

    iput v0, p0, Lcom/google/android/youtube/app/ui/cl;->l:I

    move/from16 v0, p13

    iput v0, p0, Lcom/google/android/youtube/app/ui/cl;->m:I

    move/from16 v0, p14

    iput v0, p0, Lcom/google/android/youtube/app/ui/cl;->n:I

    move/from16 v0, p15

    iput v0, p0, Lcom/google/android/youtube/app/ui/cl;->o:I

    iget v1, p0, Lcom/google/android/youtube/app/ui/cl;->m:I

    iget v2, p0, Lcom/google/android/youtube/app/ui/cl;->n:I

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iget v3, p0, Lcom/google/android/youtube/app/ui/cl;->o:I

    invoke-virtual {v2, v3}, Landroid/graphics/Canvas;->drawColor(I)V

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/cl;->p:Landroid/graphics/Bitmap;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/youtube/app/ui/cl;->a(Ljava/util/List;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/cl;->q:Landroid/graphics/Bitmap;

    return-void

    :cond_0
    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_1
    const/4 v1, 0x0

    goto/16 :goto_1

    :cond_2
    const/4 v1, 0x0

    goto/16 :goto_2

    :cond_3
    const/4 v1, 0x0

    goto/16 :goto_3

    :cond_4
    const/4 v1, 0x0

    goto :goto_4

    :cond_5
    const/4 v1, 0x0

    goto :goto_5

    :cond_6
    const/4 v1, 0x0

    goto :goto_6

    :cond_7
    const/4 v1, 0x0

    goto :goto_7

    :cond_8
    const/4 v1, 0x0

    goto :goto_8
.end method

.method private constructor <init>(Landroid/content/res/Resources;)V
    .locals 17

    const v1, 0x7f0a0021

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    const v1, 0x7f0800c8

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    const v1, 0x7f0800c9

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    const v1, 0x7f0a0022

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    const/high16 v1, 0x7f100000

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v6

    const v1, 0x7f0800c7

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    const v1, 0x7f0800ca

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    const v1, 0x7f0800cb

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    const v1, 0x7f0800cc

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    const v1, 0x7f090051

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v11

    const v1, 0x7f0800cd

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    const v1, 0x7f090052

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v13

    const v1, 0x7f0800ce

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v14

    const v1, 0x7f0800cf

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v15

    const v1, 0x7f090053

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v16

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v16}, Lcom/google/android/youtube/app/ui/cl;-><init>(IIII[IIIIIIIIIII)V

    return-void
.end method

.method public static a(Landroid/content/res/Resources;)Landroid/graphics/Bitmap;
    .locals 1

    new-instance v0, Lcom/google/android/youtube/app/ui/cl;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/ui/cl;-><init>(Landroid/content/res/Resources;)V

    iget-object v0, v0, Lcom/google/android/youtube/app/ui/cl;->q:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method private a(Ljava/util/List;)Landroid/graphics/Bitmap;
    .locals 7

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/cl;->q:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/cl;->q:Landroid/graphics/Bitmap;

    :goto_0
    return-object v0

    :cond_0
    iget v0, p0, Lcom/google/android/youtube/app/ui/cl;->b:I

    iget v1, p0, Lcom/google/android/youtube/app/ui/cl;->c:I

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v6

    new-instance v5, Landroid/graphics/Canvas;

    invoke-direct {v5, v6}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/cl;->p:Landroid/graphics/Bitmap;

    iget v2, p0, Lcom/google/android/youtube/app/ui/cl;->m:I

    iget v3, p0, Lcom/google/android/youtube/app/ui/cl;->n:I

    const/4 v4, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/ui/cl;->a(Landroid/graphics/Bitmap;IIILandroid/graphics/Canvas;)V

    :cond_1
    move-object v0, v6

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/google/android/youtube/app/ui/cl;->a:I

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    add-int/lit8 v4, v0, -0x1

    :goto_1
    if-ltz v4, :cond_1

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/ui/cl;->a(Landroid/graphics/Bitmap;IIILandroid/graphics/Canvas;)V

    add-int/lit8 v4, v4, -0x1

    goto :goto_1
.end method

.method private a(Landroid/graphics/Bitmap;IIILandroid/graphics/Canvas;)V
    .locals 9

    const/4 v8, 0x1

    const/high16 v7, 0x40000000

    const/4 v1, 0x0

    invoke-virtual {p5}, Landroid/graphics/Canvas;->save()I

    iget v0, p0, Lcom/google/android/youtube/app/ui/cl;->d:F

    iget v2, p0, Lcom/google/android/youtube/app/ui/cl;->h:I

    int-to-float v2, v2

    mul-float/2addr v0, v2

    iget v2, p0, Lcom/google/android/youtube/app/ui/cl;->d:F

    iget v3, p0, Lcom/google/android/youtube/app/ui/cl;->i:I

    iget v4, p0, Lcom/google/android/youtube/app/ui/cl;->k:I

    add-int/2addr v3, v4

    int-to-float v3, v3

    mul-float/2addr v2, v3

    float-to-int v2, v2

    new-instance v3, Landroid/graphics/Matrix;

    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    neg-int v4, p2

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    neg-int v5, p3

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iget v4, p0, Lcom/google/android/youtube/app/ui/cl;->m:I

    int-to-float v4, v4

    int-to-float v5, p2

    div-float/2addr v4, v5

    iget v5, p0, Lcom/google/android/youtube/app/ui/cl;->d:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/google/android/youtube/app/ui/cl;->n:I

    int-to-float v5, v5

    int-to-float v6, p3

    div-float/2addr v5, v6

    iget v6, p0, Lcom/google/android/youtube/app/ui/cl;->d:F

    mul-float/2addr v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Matrix;->postScale(FF)Z

    iget-object v4, p0, Lcom/google/android/youtube/app/ui/cl;->g:[I

    aget v4, v4, p4

    int-to-float v4, v4

    invoke-virtual {v3, v4}, Landroid/graphics/Matrix;->postRotate(F)Z

    iget v4, p0, Lcom/google/android/youtube/app/ui/cl;->b:I

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    mul-int/lit8 v5, p4, 0x2

    int-to-float v5, v5

    mul-float/2addr v5, v0

    add-float/2addr v4, v5

    iget v5, p0, Lcom/google/android/youtube/app/ui/cl;->c:I

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    mul-int/lit8 v6, p4, 0x2

    int-to-float v6, v6

    mul-float/2addr v0, v6

    sub-float v0, v5, v0

    invoke-virtual {v3, v4, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iget v0, p0, Lcom/google/android/youtube/app/ui/cl;->b:I

    neg-int v0, v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iget v4, p0, Lcom/google/android/youtube/app/ui/cl;->d:F

    iget v5, p0, Lcom/google/android/youtube/app/ui/cl;->m:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    div-float/2addr v4, v7

    add-float/2addr v0, v4

    div-int/lit8 v4, v2, 0x2

    int-to-float v4, v4

    add-float/2addr v0, v4

    iget v4, p0, Lcom/google/android/youtube/app/ui/cl;->e:I

    int-to-float v4, v4

    add-float/2addr v0, v4

    iget v4, p0, Lcom/google/android/youtube/app/ui/cl;->c:I

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    iget v5, p0, Lcom/google/android/youtube/app/ui/cl;->d:F

    iget v6, p0, Lcom/google/android/youtube/app/ui/cl;->n:I

    int-to-float v6, v6

    mul-float/2addr v5, v6

    div-float/2addr v5, v7

    sub-float/2addr v4, v5

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sub-float v2, v4, v2

    iget v4, p0, Lcom/google/android/youtube/app/ui/cl;->f:I

    int-to-float v4, v4

    sub-float/2addr v2, v4

    invoke-virtual {v3, v0, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    invoke-virtual {p5, v3}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v5, v8}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget v0, p0, Lcom/google/android/youtube/app/ui/cl;->i:I

    int-to-float v0, v0

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget v0, p0, Lcom/google/android/youtube/app/ui/cl;->j:I

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    int-to-float v3, p2

    int-to-float v4, p3

    move-object v0, p5

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    iget v0, p0, Lcom/google/android/youtube/app/ui/cl;->k:I

    int-to-float v0, v0

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget v0, p0, Lcom/google/android/youtube/app/ui/cl;->l:I

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    int-to-float v3, p2

    int-to-float v4, p3

    move-object v0, p5

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    invoke-virtual {v5, v8}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    invoke-virtual {p5, p1, v1, v1, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    invoke-virtual {p5}, Landroid/graphics/Canvas;->restore()V

    return-void
.end method
