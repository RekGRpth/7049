.class final Lcom/google/android/youtube/app/ui/ct;
.super Lcom/google/android/youtube/core/ui/e;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/ui/SubscribeHelper;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/ui/SubscribeHelper;Lcom/google/android/youtube/core/e;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/ct;->a:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-direct {p0, p2}, Lcom/google/android/youtube/core/ui/e;-><init>(Lcom/google/android/youtube/core/e;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ct;->a:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-static {v0, p1}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->a(Lcom/google/android/youtube/app/ui/SubscribeHelper;Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/model/UserAuth;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ct;->a:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->f(Lcom/google/android/youtube/app/ui/SubscribeHelper;)Lcom/google/android/youtube/core/Analytics;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ct;->a:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->g(Lcom/google/android/youtube/app/ui/SubscribeHelper;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/ct;->a:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-static {v2}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->h(Lcom/google/android/youtube/app/ui/SubscribeHelper;)Lcom/google/android/youtube/app/YouTubeApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->I()I

    move-result v2

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->a(Lcom/google/android/youtube/core/Analytics;Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ct;->a:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->m(Lcom/google/android/youtube/app/ui/SubscribeHelper;)Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ct;->a:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->c(Lcom/google/android/youtube/app/ui/SubscribeHelper;)Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ct;->a:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->a(Lcom/google/android/youtube/app/ui/SubscribeHelper;)Lcom/google/android/youtube/core/model/UserProfile;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/youtube/core/model/UserProfile;->username:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/ct;->a:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-static {v2}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->i(Lcom/google/android/youtube/app/ui/SubscribeHelper;)Landroid/app/Activity;

    move-result-object v2

    new-instance v3, Lcom/google/android/youtube/app/ui/cv;

    iget-object v4, p0, Lcom/google/android/youtube/app/ui/ct;->a:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-direct {v3, v4, v5}, Lcom/google/android/youtube/app/ui/cv;-><init>(Lcom/google/android/youtube/app/ui/SubscribeHelper;B)V

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v2

    invoke-interface {v0, p1, v1, v2}, Lcom/google/android/youtube/core/client/bc;->a(Lcom/google/android/youtube/core/model/UserAuth;Ljava/lang/String;Lcom/google/android/youtube/core/async/n;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ct;->a:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->c(Lcom/google/android/youtube/app/ui/SubscribeHelper;)Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ct;->a:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->m(Lcom/google/android/youtube/app/ui/SubscribeHelper;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/ct;->a:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-static {v2}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->i(Lcom/google/android/youtube/app/ui/SubscribeHelper;)Landroid/app/Activity;

    move-result-object v2

    new-instance v3, Lcom/google/android/youtube/app/ui/cu;

    iget-object v4, p0, Lcom/google/android/youtube/app/ui/ct;->a:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-direct {v3, v4, v5}, Lcom/google/android/youtube/app/ui/cu;-><init>(Lcom/google/android/youtube/app/ui/SubscribeHelper;B)V

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v2

    invoke-interface {v0, v1, p1, v2}, Lcom/google/android/youtube/core/client/bc;->g(Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/core/ui/e;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ct;->a:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->k(Lcom/google/android/youtube/app/ui/SubscribeHelper;)V

    return-void
.end method

.method public final g_()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/youtube/core/ui/e;->g_()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ct;->a:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->k(Lcom/google/android/youtube/app/ui/SubscribeHelper;)V

    return-void
.end method
