.class final Lcom/google/android/youtube/app/ui/cv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/ui/SubscribeHelper;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/ui/SubscribeHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/cv;->a:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/app/ui/SubscribeHelper;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/ui/cv;-><init>(Lcom/google/android/youtube/app/ui/SubscribeHelper;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/cv;->a:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->l(Lcom/google/android/youtube/app/ui/SubscribeHelper;)Lcom/google/android/youtube/core/e;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/core/e;->b(Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/cv;->a:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->k(Lcom/google/android/youtube/app/ui/SubscribeHelper;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 7

    check-cast p2, Lcom/google/android/youtube/core/async/Optional;

    invoke-interface {p2}, Lcom/google/android/youtube/core/async/Optional;->get()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Subscription;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/cv;->a:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->l(Lcom/google/android/youtube/app/ui/SubscribeHelper;)Lcom/google/android/youtube/core/e;

    move-result-object v0

    const v1, 0x7f0b0197

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/e;->a(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/cv;->a:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->a(Lcom/google/android/youtube/app/ui/SubscribeHelper;Landroid/net/Uri;)Landroid/net/Uri;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/cv;->a:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    sget-object v1, Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->NOT_SUBSCRIBED:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->a(Lcom/google/android/youtube/app/ui/SubscribeHelper;Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/cv;->a:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    iget-object v2, v0, Lcom/google/android/youtube/core/model/Subscription;->editUri:Landroid/net/Uri;

    invoke-static {v1, v2}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->a(Lcom/google/android/youtube/app/ui/SubscribeHelper;Landroid/net/Uri;)Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/cv;->a:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-static {v1, v0}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->a(Lcom/google/android/youtube/app/ui/SubscribeHelper;Lcom/google/android/youtube/core/model/Subscription;)Lcom/google/android/youtube/core/model/Subscription;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/cv;->a:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->c(Lcom/google/android/youtube/app/ui/SubscribeHelper;)Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/cv;->a:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->m(Lcom/google/android/youtube/app/ui/SubscribeHelper;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/cv;->a:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-static {v2}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->n(Lcom/google/android/youtube/app/ui/SubscribeHelper;)Lcom/google/android/youtube/core/model/UserAuth;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/cv;->a:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-static {v3}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->i(Lcom/google/android/youtube/app/ui/SubscribeHelper;)Landroid/app/Activity;

    move-result-object v3

    new-instance v4, Lcom/google/android/youtube/app/ui/cu;

    iget-object v5, p0, Lcom/google/android/youtube/app/ui/cv;->a:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    const/4 v6, 0x0

    invoke-direct {v4, v5, v6}, Lcom/google/android/youtube/app/ui/cu;-><init>(Lcom/google/android/youtube/app/ui/SubscribeHelper;B)V

    invoke-static {v3, v4}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/youtube/core/client/bc;->g(Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V

    goto :goto_0
.end method
