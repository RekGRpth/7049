.class public final Lcom/google/android/youtube/app/ui/cz;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/youtube/core/model/Video;

.field public final b:Lcom/google/android/youtube/core/transfer/Transfer;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/core/model/Video;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "video cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Video;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/cz;->a:Lcom/google/android/youtube/core/model/Video;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/cz;->b:Lcom/google/android/youtube/core/transfer/Transfer;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/youtube/core/transfer/Transfer;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "transfer cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/transfer/Transfer;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/cz;->b:Lcom/google/android/youtube/core/transfer/Transfer;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/cz;->a:Lcom/google/android/youtube/core/model/Video;

    return-void
.end method

.method public static a(Lcom/google/android/youtube/core/model/Video;)Lcom/google/android/youtube/app/ui/cz;
    .locals 1

    new-instance v0, Lcom/google/android/youtube/app/ui/cz;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/ui/cz;-><init>(Lcom/google/android/youtube/core/model/Video;)V

    return-object v0
.end method

.method public static a(Lcom/google/android/youtube/core/transfer/Transfer;)Lcom/google/android/youtube/app/ui/cz;
    .locals 1

    new-instance v0, Lcom/google/android/youtube/app/ui/cz;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/ui/cz;-><init>(Lcom/google/android/youtube/core/transfer/Transfer;)V

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/cz;->a:Lcom/google/android/youtube/core/model/Video;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    instance-of v1, p1, Lcom/google/android/youtube/app/ui/cz;

    if-eqz v1, :cond_0

    check-cast p1, Lcom/google/android/youtube/app/ui/cz;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/cz;->a:Lcom/google/android/youtube/core/model/Video;

    iget-object v2, p1, Lcom/google/android/youtube/app/ui/cz;->a:Lcom/google/android/youtube/core/model/Video;

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/cz;->b:Lcom/google/android/youtube/core/transfer/Transfer;

    iget-object v2, p1, Lcom/google/android/youtube/app/ui/cz;->b:Lcom/google/android/youtube/core/transfer/Transfer;

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/cz;->a:Lcom/google/android/youtube/core/model/Video;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/cz;->b:Lcom/google/android/youtube/core/transfer/Transfer;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/cz;->a:Lcom/google/android/youtube/core/model/Video;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/Video;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/cz;->b:Lcom/google/android/youtube/core/transfer/Transfer;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/transfer/Transfer;->hashCode()I

    move-result v1

    goto :goto_1
.end method
