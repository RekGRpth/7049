.class final Lcom/google/android/youtube/app/ui/dr;
.super Lcom/google/android/youtube/core/ui/e;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/ui/dh;

.field private final b:Lcom/google/android/youtube/app/adapter/be;

.field private final d:Lcom/google/android/youtube/core/ui/j;

.field private final e:Lcom/google/android/youtube/core/ui/PagedListView;

.field private f:Lcom/google/android/youtube/core/model/Video;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/ui/dh;)V
    .locals 6

    const/4 v4, 0x0

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/dr;->a:Lcom/google/android/youtube/app/ui/dh;

    invoke-static {p1}, Lcom/google/android/youtube/app/ui/dh;->b(Lcom/google/android/youtube/app/ui/dh;)Lcom/google/android/youtube/core/e;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/ui/e;-><init>(Lcom/google/android/youtube/core/e;)V

    new-instance v0, Lcom/google/android/youtube/app/adapter/aq;

    iget-object v1, p1, Lcom/google/android/youtube/app/ui/dh;->a:Landroid/app/Activity;

    const v2, 0x7f040068

    const/4 v3, 0x1

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/google/android/youtube/app/adapter/aq;-><init>(Landroid/content/Context;Lcom/google/android/youtube/app/ui/ToolbarHelper;IZ)V

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/dr;->b:Lcom/google/android/youtube/app/adapter/be;

    iget-object v0, p1, Lcom/google/android/youtube/app/ui/dh;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04000b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/ui/PagedListView;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/dr;->e:Lcom/google/android/youtube/core/ui/PagedListView;

    new-instance v0, Lcom/google/android/youtube/core/ui/j;

    iget-object v1, p1, Lcom/google/android/youtube/app/ui/dh;->a:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/dr;->e:Lcom/google/android/youtube/core/ui/PagedListView;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/dr;->b:Lcom/google/android/youtube/app/adapter/be;

    invoke-static {p1}, Lcom/google/android/youtube/app/ui/dh;->l(Lcom/google/android/youtube/app/ui/dh;)Lcom/google/android/youtube/core/async/au;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/youtube/app/ui/dr;->c:Lcom/google/android/youtube/core/e;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/core/ui/j;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/PagedView;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/e;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/dr;->d:Lcom/google/android/youtube/core/ui/j;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dr;->e:Lcom/google/android/youtube/core/ui/PagedListView;

    new-instance v1, Lcom/google/android/youtube/app/ui/ds;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/app/ui/ds;-><init>(Lcom/google/android/youtube/app/ui/dr;Lcom/google/android/youtube/app/ui/dh;)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/PagedListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/dr;)Lcom/google/android/youtube/app/adapter/be;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dr;->b:Lcom/google/android/youtube/app/adapter/be;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/ui/dr;)Lcom/google/android/youtube/core/model/Video;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dr;->f:Lcom/google/android/youtube/core/model/Video;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/ui/dr;)Lcom/google/android/youtube/core/e;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dr;->c:Lcom/google/android/youtube/core/e;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/ui/dr;)Lcom/google/android/youtube/core/e;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dr;->c:Lcom/google/android/youtube/core/e;

    return-object v0
.end method


# virtual methods
.method public final a()Landroid/app/Dialog;
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dr;->a:Lcom/google/android/youtube/app/ui/dh;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/dh;->g(Lcom/google/android/youtube/app/ui/dh;)Lcom/google/android/youtube/core/model/Video;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/dr;->f:Lcom/google/android/youtube/core/model/Video;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dr;->f:Lcom/google/android/youtube/core/model/Video;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/youtube/core/ui/w;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/dr;->a:Lcom/google/android/youtube/app/ui/dh;

    iget-object v1, v1, Lcom/google/android/youtube/app/ui/dh;->a:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/ui/w;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0b01e5

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/w;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/dr;->e:Lcom/google/android/youtube/core/ui/PagedListView;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dr;->d:Lcom/google/android/youtube/core/ui/j;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/dr;->a:Lcom/google/android/youtube/app/ui/dh;

    invoke-static {v3}, Lcom/google/android/youtube/app/ui/dh;->e(Lcom/google/android/youtube/app/ui/dh;)Lcom/google/android/youtube/core/client/bc;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/youtube/core/client/bc;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->h(Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/j;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dr;->a:Lcom/google/android/youtube/app/ui/dh;

    iget-object v0, v0, Lcom/google/android/youtube/app/ui/dh;->a:Landroid/app/Activity;

    const/16 v1, 0x3f4

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    return-void
.end method
