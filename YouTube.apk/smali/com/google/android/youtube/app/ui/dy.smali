.class final Lcom/google/android/youtube/app/ui/dy;
.super Lcom/google/android/youtube/core/ui/e;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/ui/dh;

.field private final b:Z

.field private final d:Lcom/google/android/youtube/core/model/Video;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/ui/dh;ZLcom/google/android/youtube/core/model/Video;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/dy;->a:Lcom/google/android/youtube/app/ui/dh;

    invoke-static {p1}, Lcom/google/android/youtube/app/ui/dh;->b(Lcom/google/android/youtube/app/ui/dh;)Lcom/google/android/youtube/core/e;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/ui/e;-><init>(Lcom/google/android/youtube/core/e;)V

    iput-boolean p2, p0, Lcom/google/android/youtube/app/ui/dy;->b:Z

    iput-object p3, p0, Lcom/google/android/youtube/app/ui/dy;->d:Lcom/google/android/youtube/core/model/Video;

    return-void
.end method

.method private a()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dy;->d:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/dy;->a:Lcom/google/android/youtube/app/ui/dh;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/dh;->g(Lcom/google/android/youtube/app/ui/dh;)Lcom/google/android/youtube/core/model/Video;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/Video;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 4

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/dy;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dy;->a:Lcom/google/android/youtube/app/ui/dh;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/dh;->c(Lcom/google/android/youtube/app/ui/dh;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dy;->a:Lcom/google/android/youtube/app/ui/dh;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/dy;->a:Lcom/google/android/youtube/app/ui/dh;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/dh;->c(Lcom/google/android/youtube/app/ui/dh;)Landroid/view/View;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/youtube/app/ui/dy;->b:Z

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/app/ui/dh;->a(Lcom/google/android/youtube/app/ui/dh;Landroid/view/View;Z)V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/dy;->a:Lcom/google/android/youtube/app/ui/dh;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dy;->a:Lcom/google/android/youtube/app/ui/dh;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/dh;->d(Lcom/google/android/youtube/app/ui/dh;)Landroid/view/View;

    move-result-object v2

    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/dy;->b:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v2, v0}, Lcom/google/android/youtube/app/ui/dh;->a(Lcom/google/android/youtube/app/ui/dh;Landroid/view/View;Z)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dy;->a:Lcom/google/android/youtube/app/ui/dh;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/dh;->e(Lcom/google/android/youtube/app/ui/dh;)Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/client/bc;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dy;->a:Lcom/google/android/youtube/app/ui/dh;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/dh;->e(Lcom/google/android/youtube/app/ui/dh;)Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/dy;->d:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/google/android/youtube/app/ui/dy;->b:Z

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/dy;->a:Lcom/google/android/youtube/app/ui/dh;

    iget-object v3, v3, Lcom/google/android/youtube/app/ui/dh;->a:Landroid/app/Activity;

    invoke-static {v3, p0}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v3

    invoke-interface {v0, v1, p1, v2, v3}, Lcom/google/android/youtube/core/client/bc;->a(Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth;ZLcom/google/android/youtube/core/async/n;)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "Error rating"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dy;->c:Lcom/google/android/youtube/core/e;

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/core/e;->b(Ljava/lang/Throwable;)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/dy;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dy;->a:Lcom/google/android/youtube/app/ui/dh;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/dh;->c(Lcom/google/android/youtube/app/ui/dh;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dy;->a:Lcom/google/android/youtube/app/ui/dh;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/dy;->a:Lcom/google/android/youtube/app/ui/dh;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/dh;->c(Lcom/google/android/youtube/app/ui/dh;)Landroid/view/View;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/app/ui/dh;->a(Lcom/google/android/youtube/app/ui/dh;Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dy;->a:Lcom/google/android/youtube/app/ui/dh;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/dy;->a:Lcom/google/android/youtube/app/ui/dh;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/dh;->d(Lcom/google/android/youtube/app/ui/dh;)Landroid/view/View;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/app/ui/dh;->a(Lcom/google/android/youtube/app/ui/dh;Landroid/view/View;Z)V

    :cond_0
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/dy;->a:Lcom/google/android/youtube/app/ui/dh;

    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/dy;->b:Z

    if-eqz v0, :cond_1

    const v0, 0x7f0b01d8

    :goto_0
    invoke-static {v1, v0}, Lcom/google/android/youtube/app/ui/dh;->a(Lcom/google/android/youtube/app/ui/dh;I)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/dy;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dy;->a:Lcom/google/android/youtube/app/ui/dh;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/dh;->f(Lcom/google/android/youtube/app/ui/dh;)Lcom/google/android/youtube/app/ui/dq;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/youtube/app/ui/dy;->b:Z

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/ui/dq;->j(Z)V

    :cond_0
    return-void

    :cond_1
    const v0, 0x7f0b01d9

    goto :goto_0
.end method
