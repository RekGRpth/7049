.class final Lcom/google/android/youtube/app/ui/eb;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/ui/ec;


# instance fields
.field private final a:Lcom/google/android/youtube/app/d;

.field private final b:Z

.field private final c:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field private final d:Lcom/google/android/youtube/core/Analytics;

.field private final e:Lcom/google/android/youtube/core/Analytics$VideoCategory;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/d;ZLcom/google/android/youtube/core/client/VideoStats2Client$Feature;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/Analytics$VideoCategory;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/eb;->a:Lcom/google/android/youtube/app/d;

    iput-boolean p2, p0, Lcom/google/android/youtube/app/ui/eb;->b:Z

    iput-object p3, p0, Lcom/google/android/youtube/app/ui/eb;->c:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    iput-object p4, p0, Lcom/google/android/youtube/app/ui/eb;->d:Lcom/google/android/youtube/core/Analytics;

    iput-object p5, p0, Lcom/google/android/youtube/app/ui/eb;->e:Lcom/google/android/youtube/core/Analytics$VideoCategory;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/Video;I)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eb;->d:Lcom/google/android/youtube/core/Analytics;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eb;->e:Lcom/google/android/youtube/core/Analytics$VideoCategory;

    invoke-interface {v0, v1, p2}, Lcom/google/android/youtube/core/Analytics;->a(Lcom/google/android/youtube/core/Analytics$VideoCategory;I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eb;->a:Lcom/google/android/youtube/app/d;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/google/android/youtube/app/ui/eb;->b:Z

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/eb;->c:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/youtube/app/d;->a(Ljava/lang/String;ZLcom/google/android/youtube/core/client/VideoStats2Client$Feature;)V

    return-void
.end method
