.class public final Lcom/google/android/youtube/app/ui/ee;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Lcom/google/android/youtube/core/a/g;

.field static final b:Lcom/google/android/youtube/core/a/g;

.field static final c:Lcom/google/android/youtube/core/a/g;

.field static final d:Lcom/google/android/youtube/core/a/g;

.field static final e:Lcom/google/android/youtube/core/a/g;

.field static final f:Lcom/google/android/youtube/core/a/g;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/youtube/core/a/g;

    const-string v1, "Heading"

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/a/g;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/youtube/app/ui/ee;->a:Lcom/google/android/youtube/core/a/g;

    new-instance v0, Lcom/google/android/youtube/core/a/g;

    const-string v1, "RelatedVideo"

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/a/g;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/youtube/app/ui/ee;->b:Lcom/google/android/youtube/core/a/g;

    new-instance v0, Lcom/google/android/youtube/core/a/g;

    const-string v1, "RelatedVideoList"

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/a/g;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/youtube/app/ui/ee;->c:Lcom/google/android/youtube/core/a/g;

    new-instance v0, Lcom/google/android/youtube/core/a/g;

    const-string v1, "Comment"

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/a/g;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/youtube/app/ui/ee;->d:Lcom/google/android/youtube/core/a/g;

    new-instance v0, Lcom/google/android/youtube/core/a/g;

    const-string v1, "ArtistTrackList"

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/a/g;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/youtube/app/ui/ee;->e:Lcom/google/android/youtube/core/a/g;

    new-instance v0, Lcom/google/android/youtube/core/a/g;

    const-string v1, "RelatedArtist"

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/a/g;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/youtube/app/ui/ee;->f:Lcom/google/android/youtube/core/a/g;

    return-void
.end method
