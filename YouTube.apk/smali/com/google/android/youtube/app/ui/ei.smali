.class final Lcom/google/android/youtube/app/ui/ei;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/ui/eg;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/ui/eg;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/ei;->a:Lcom/google/android/youtube/app/ui/eg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/app/ui/eg;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/ui/ei;-><init>(Lcom/google/android/youtube/app/ui/eg;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 6

    const/4 v5, 0x1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ei;->a:Lcom/google/android/youtube/app/ui/eg;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/eg;->e(Lcom/google/android/youtube/app/ui/eg;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0b00d3

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/youtube/app/ui/ei;->a:Lcom/google/android/youtube/app/ui/eg;

    invoke-static {v4}, Lcom/google/android/youtube/app/ui/eg;->d(Lcom/google/android/youtube/app/ui/eg;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ei;->a:Lcom/google/android/youtube/app/ui/eg;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/eg;->c(Lcom/google/android/youtube/app/ui/eg;)Lcom/google/android/youtube/app/ui/e;

    move-result-object v1

    invoke-virtual {v1, v0, v5}, Lcom/google/android/youtube/app/ui/e;->b(Ljava/lang/String;Z)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 6

    check-cast p2, Ljava/util/List;

    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/BatchEntry;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/BatchEntry;->a:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/youtube/core/model/Video;

    if-eqz v0, :cond_0

    iget-object v4, v0, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ei;->a:Lcom/google/android/youtube/app/ui/eg;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/eg;->a(Lcom/google/android/youtube/app/ui/eg;)Ljava/util/LinkedHashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/model/Video;

    if-eqz v1, :cond_2

    new-instance v5, Landroid/util/Pair;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-direct {v5, v0, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ei;->a:Lcom/google/android/youtube/app/ui/eg;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/eg;->b(Lcom/google/android/youtube/app/ui/eg;)Lcom/google/android/youtube/app/adapter/bm;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/adapter/bm;->b(Ljava/lang/Iterable;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ei;->a:Lcom/google/android/youtube/app/ui/eg;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/eg;->c(Lcom/google/android/youtube/app/ui/eg;)Lcom/google/android/youtube/app/ui/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/e;->d()V

    return-void
.end method
