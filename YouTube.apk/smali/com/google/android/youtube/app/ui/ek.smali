.class public final Lcom/google/android/youtube/app/ui/ek;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/youtube/app/ui/cq;


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/google/android/youtube/core/Analytics;

.field private final c:Lcom/google/android/youtube/app/d;

.field private final d:Lcom/google/android/youtube/core/client/bc;

.field private final e:Lcom/google/android/youtube/core/client/be;

.field private f:Lcom/google/android/youtube/app/ui/dh;

.field private final g:Lcom/google/android/youtube/app/ui/en;

.field private final h:Lcom/google/android/youtube/app/ui/em;

.field private final i:Landroid/widget/TextView;

.field private final j:Landroid/widget/TextView;

.field private final k:Landroid/widget/TextView;

.field private final l:Landroid/widget/ProgressBar;

.field private final m:Landroid/widget/ImageView;

.field private final n:Landroid/view/View;

.field private final o:Landroid/widget/TextView;

.field private final p:Landroid/widget/TextView;

.field private final q:Landroid/widget/TextView;

.field private final r:Landroid/widget/ImageButton;

.field private final s:Landroid/widget/ImageButton;

.field private final t:Landroid/widget/TextView;

.field private final u:Landroid/widget/FrameLayout;

.field private final v:Landroid/widget/ProgressBar;

.field private w:Lcom/google/android/youtube/app/ui/SubscribeHelper;

.field private x:I

.field private y:I

.field private z:Lcom/google/android/youtube/core/model/Video;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/FragmentActivity;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/be;Landroid/view/View;Landroid/view/View;)V
    .locals 9

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v1, "activity cannot be null"

    invoke-static {p1, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->a:Landroid/app/Activity;

    const-string v1, "analytics cannot be null"

    invoke-static {p2, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/Analytics;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->b:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "navigation cannot be null"

    invoke-static {p3, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/app/d;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->c:Lcom/google/android/youtube/app/d;

    const-string v1, "gdataClient cannot be null"

    invoke-static {p5, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/client/bc;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->d:Lcom/google/android/youtube/core/client/bc;

    const-string v1, "imageClient cannot be null"

    invoke-static {p6, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/client/be;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->e:Lcom/google/android/youtube/core/client/be;

    new-instance v1, Lcom/google/android/youtube/app/ui/en;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/youtube/app/ui/en;-><init>(Lcom/google/android/youtube/app/ui/ek;B)V

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->g:Lcom/google/android/youtube/app/ui/en;

    new-instance v1, Lcom/google/android/youtube/app/ui/em;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/youtube/app/ui/em;-><init>(Lcom/google/android/youtube/app/ui/ek;B)V

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->h:Lcom/google/android/youtube/app/ui/em;

    const v1, 0x7f07002c

    move-object/from16 v0, p7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->i:Landroid/widget/TextView;

    const v1, 0x7f070070

    move-object/from16 v0, p7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->m:Landroid/widget/ImageView;

    const v1, 0x7f0700bc

    move-object/from16 v0, p7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->n:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->n:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f070073

    move-object/from16 v0, p7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->o:Landroid/widget/TextView;

    const v1, 0x7f0700bd

    move-object/from16 v0, p7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->t:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->t:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/e;

    move-result-object v6

    new-instance v1, Lcom/google/android/youtube/app/ui/SubscribeHelper;

    const-string v8, "Watch"

    move-object v2, p1

    move-object v3, p2

    move-object v4, p4

    move-object v5, p5

    move-object v7, p0

    invoke-direct/range {v1 .. v8}, Lcom/google/android/youtube/app/ui/SubscribeHelper;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/app/ui/cq;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->w:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090033

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/app/ui/ek;->y:I

    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090001

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/app/ui/ek;->x:I

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->t:Landroid/widget/TextView;

    new-instance v2, Lcom/google/android/youtube/app/ui/dg;

    invoke-direct {v2, p1}, Lcom/google/android/youtube/app/ui/dg;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    :cond_0
    const v1, 0x7f07007c

    move-object/from16 v0, p7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->u:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->u:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->u:Landroid/widget/FrameLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->u:Landroid/widget/FrameLayout;

    invoke-virtual {v1, p0}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->u:Landroid/widget/FrameLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setClickable(Z)V

    :cond_1
    const v1, 0x7f070071

    move-object/from16 v0, p7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->v:Landroid/widget/ProgressBar;

    const v1, 0x7f070096

    move-object/from16 v0, p8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->q:Landroid/widget/TextView;

    const v1, 0x7f070175

    move-object/from16 v0, p7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->j:Landroid/widget/TextView;

    const v1, 0x7f070174

    move-object/from16 v0, p7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->p:Landroid/widget/TextView;

    const v1, 0x7f070172

    move-object/from16 v0, p8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->k:Landroid/widget/TextView;

    const v1, 0x7f070171

    move-object/from16 v0, p8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->l:Landroid/widget/ProgressBar;

    const-string v1, "like_button"

    move-object/from16 v0, p8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->r:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->r:Landroid/widget/ImageButton;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->r:Landroid/widget/ImageButton;

    invoke-virtual {v1, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_2
    const-string v1, "dislike_button"

    move-object/from16 v0, p8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->s:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->s:Landroid/widget/ImageButton;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->s:Landroid/widget/ImageButton;

    invoke-virtual {v1, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_3
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/ek;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->a:Landroid/app/Activity;

    return-object v0
.end method

.method private a(JJ)V
    .locals 9

    const-wide/16 v7, 0xa

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->k:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->k:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/ek;->a:Landroid/app/Activity;

    const v4, 0x7f0b01db

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v3, v4, v5}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->l:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_2

    add-long v3, p1, p3

    :goto_0
    const-wide/32 v5, 0x7fffffff

    cmp-long v0, v3, v5

    if-lez v0, :cond_1

    div-long/2addr p1, v7

    div-long/2addr p3, v7

    add-long v3, p1, p3

    goto :goto_0

    :cond_1
    iget-object v5, p0, Lcom/google/android/youtube/app/ui/ek;->l:Landroid/widget/ProgressBar;

    const-wide/16 v6, 0x0

    cmp-long v0, v3, v6

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    invoke-virtual {v5, v0}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->l:Landroid/widget/ProgressBar;

    long-to-int v3, v3

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setMax(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->l:Landroid/widget/ProgressBar;

    long-to-int v3, p1

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setProgress(I)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->p:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->p:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/ek;->a:Landroid/app/Activity;

    const v4, 0x7f0b01da

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/youtube/app/ui/ek;->z:Lcom/google/android/youtube/core/model/Video;

    iget-wide v5, v5, Lcom/google/android/youtube/core/model/Video;->viewCount:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v1, v2

    invoke-virtual {v3, v4, v1}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    return-void

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method static synthetic b(Lcom/google/android/youtube/app/ui/ek;)Lcom/google/android/youtube/app/ui/em;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->h:Lcom/google/android/youtube/app/ui/em;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/ui/ek;)Lcom/google/android/youtube/core/client/be;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->e:Lcom/google/android/youtube/core/client/be;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/ui/ek;)Lcom/google/android/youtube/app/ui/SubscribeHelper;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->w:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/app/ui/ek;)Landroid/widget/FrameLayout;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->u:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/youtube/app/ui/ek;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->m:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0201c2

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method static synthetic g(Lcom/google/android/youtube/app/ui/ek;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->m:Landroid/widget/ImageView;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;)V
    .locals 7

    const/16 v6, 0x8

    const/high16 v1, 0x3f800000

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->w:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->d()Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->w:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    sget-object v3, Lcom/google/android/youtube/app/ui/el;->a:[I

    invoke-virtual {v2}, Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    :goto_1
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0f0003

    invoke-virtual {v0, v3, v5, v5}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v0

    goto :goto_0

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->v:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v6}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->t:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/ek;->a:Landroid/app/Activity;

    const v3, 0x7f0b018d

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->t:Landroid/widget/TextView;

    iget v2, p0, Lcom/google/android/youtube/app/ui/ek;->y:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->t:Landroid/widget/TextView;

    const v2, 0x7f0200c9

    invoke-virtual {v1, v2, v4, v4, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->u:Landroid/widget/FrameLayout;

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/utils/j;->a(Landroid/view/View;F)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->t:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->u:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->u:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout;->setClickable(Z)V

    goto :goto_1

    :pswitch_1
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->v:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v6}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->t:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/ek;->a:Landroid/app/Activity;

    const v3, 0x7f0b018b

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->t:Landroid/widget/TextView;

    iget v2, p0, Lcom/google/android/youtube/app/ui/ek;->x:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->t:Landroid/widget/TextView;

    const v2, 0x7f0200c8

    invoke-virtual {v1, v2, v4, v4, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->u:Landroid/widget/FrameLayout;

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/utils/j;->a(Landroid/view/View;F)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->t:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->u:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->u:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout;->setClickable(Z)V

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->u:Landroid/widget/FrameLayout;

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/j;->a(Landroid/view/View;F)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->t:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->v:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->u:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->u:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setClickable(Z)V

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lcom/google/android/youtube/app/ui/dh;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/ek;->f:Lcom/google/android/youtube/app/ui/dh;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->r:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->s:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->r:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->s:Landroid/widget/ImageButton;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/youtube/app/ui/dh;->a(Landroid/view/View;Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Video;)V
    .locals 7

    const/16 v2, 0x8

    const/4 v1, 0x0

    const-string v0, "video can\'t be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Video;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->z:Lcom/google/android/youtube/core/model/Video;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->i:Landroid/widget/TextView;

    iget-object v3, p1, Lcom/google/android/youtube/core/model/Video;->title:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->uploadedDate:Ljava/util/Date;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->j:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/ek;->a:Landroid/app/Activity;

    invoke-static {v3}, Landroid/text/format/DateFormat;->getLongDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v3

    iget-object v4, p1, Lcom/google/android/youtube/core/model/Video;->uploadedDate:Ljava/util/Date;

    invoke-virtual {v3, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    iget-wide v3, p1, Lcom/google/android/youtube/core/model/Video;->viewCount:J

    iget-wide v3, p1, Lcom/google/android/youtube/core/model/Video;->likesCount:J

    iget-wide v5, p1, Lcom/google/android/youtube/core/model/Video;->dislikesCount:J

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/google/android/youtube/app/ui/ek;->a(JJ)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->t:Landroid/widget/TextView;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->o:Landroid/widget/TextView;

    iget-object v3, p1, Lcom/google/android/youtube/core/model/Video;->ownerDisplayName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    invoke-static {p1}, Lcom/google/android/youtube/app/ui/dh;->a(Lcom/google/android/youtube/core/model/Video;)Z

    move-result v0

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/ek;->r:Landroid/widget/ImageButton;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/ek;->s:Landroid/widget/ImageButton;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/ek;->r:Landroid/widget/ImageButton;

    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/ek;->s:Landroid/widget/ImageButton;

    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->q:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    const-string v0, ""

    iget-object v3, p1, Lcom/google/android/youtube/core/model/Video;->description:Ljava/lang/String;

    if-eqz v3, :cond_1

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->description:Ljava/lang/String;

    const-string v3, "[\\r\\n]+"

    const-string v4, "\r\n\r\n"

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_1
    iget-object v3, p0, Lcom/google/android/youtube/app/ui/ek;->q:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/ek;->q:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_6

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->m:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->d:Lcom/google/android/youtube/core/client/bc;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Video;->ownerUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/ek;->a:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/ek;->g:Lcom/google/android/youtube/app/ui/en;

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/bc;->c(Landroid/net/Uri;Lcom/google/android/youtube/core/async/n;)V

    :cond_3
    return-void

    :cond_4
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->o:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, " "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p1, Lcom/google/android/youtube/core/model/Video;->ownerDisplayName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_6
    move v0, v2

    goto :goto_2
.end method

.method public final a(Z)V
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->z:Lcom/google/android/youtube/core/model/Video;

    iget-wide v3, v0, Lcom/google/android/youtube/core/model/Video;->viewCount:J

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->z:Lcom/google/android/youtube/core/model/Video;

    iget-wide v3, v0, Lcom/google/android/youtube/core/model/Video;->likesCount:J

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    int-to-long v5, v0

    add-long/2addr v3, v5

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->z:Lcom/google/android/youtube/core/model/Video;

    iget-wide v5, v0, Lcom/google/android/youtube/core/model/Video;->dislikesCount:J

    if-eqz p1, :cond_1

    :goto_1
    int-to-long v0, v2

    add-long/2addr v0, v5

    invoke-direct {p0, v3, v4, v0, v1}, Lcom/google/android/youtube/app/ui/ek;->a(JJ)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_1
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->n:Landroid/view/View;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->z:Lcom/google/android/youtube/core/model/Video;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->b:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "WatchChannel"

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->c:Lcom/google/android/youtube/app/d;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->z:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->owner:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/d;->a(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->r:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->f:Lcom/google/android/youtube/app/ui/dh;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->f:Lcom/google/android/youtube/app/ui/dh;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/dh;->a()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->s:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->f:Lcom/google/android/youtube/app/ui/dh;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->f:Lcom/google/android/youtube/app/ui/dh;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/dh;->b()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->u:Landroid/widget/FrameLayout;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->w:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->b()V

    goto :goto_0
.end method
