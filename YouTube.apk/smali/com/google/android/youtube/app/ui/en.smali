.class final Lcom/google/android/youtube/app/ui/en;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/ui/ek;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/ui/ek;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/en;->a:Lcom/google/android/youtube/app/ui/ek;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/app/ui/ek;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/ui/en;-><init>(Lcom/google/android/youtube/app/ui/ek;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2

    const-string v0, "Error retrieving user profile"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/en;->a:Lcom/google/android/youtube/app/ui/ek;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/ek;->e(Lcom/google/android/youtube/app/ui/ek;)Landroid/widget/FrameLayout;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/en;->a:Lcom/google/android/youtube/app/ui/ek;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/ek;->e(Lcom/google/android/youtube/app/ui/ek;)Landroid/widget/FrameLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/en;->a:Lcom/google/android/youtube/app/ui/ek;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/ek;->f(Lcom/google/android/youtube/app/ui/ek;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    check-cast p2, Lcom/google/android/youtube/core/model/UserProfile;

    iget-object v0, p2, Lcom/google/android/youtube/core/model/UserProfile;->thumbnailUri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/en;->a:Lcom/google/android/youtube/app/ui/ek;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/ek;->c(Lcom/google/android/youtube/app/ui/ek;)Lcom/google/android/youtube/core/client/be;

    move-result-object v0

    iget-object v1, p2, Lcom/google/android/youtube/core/model/UserProfile;->thumbnailUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/en;->a:Lcom/google/android/youtube/app/ui/ek;

    invoke-static {v2}, Lcom/google/android/youtube/app/ui/ek;->a(Lcom/google/android/youtube/app/ui/ek;)Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/en;->a:Lcom/google/android/youtube/app/ui/ek;

    invoke-static {v3}, Lcom/google/android/youtube/app/ui/ek;->b(Lcom/google/android/youtube/app/ui/ek;)Lcom/google/android/youtube/app/ui/em;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/be;->a(Landroid/net/Uri;Lcom/google/android/youtube/core/async/n;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/en;->a:Lcom/google/android/youtube/app/ui/ek;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/ek;->d(Lcom/google/android/youtube/app/ui/ek;)Lcom/google/android/youtube/app/ui/SubscribeHelper;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/en;->a:Lcom/google/android/youtube/app/ui/ek;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/ek;->d(Lcom/google/android/youtube/app/ui/ek;)Lcom/google/android/youtube/app/ui/SubscribeHelper;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->a(Lcom/google/android/youtube/core/model/UserProfile;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/en;->a:Lcom/google/android/youtube/app/ui/ek;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/ek;->e(Lcom/google/android/youtube/app/ui/ek;)Landroid/widget/FrameLayout;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/en;->a:Lcom/google/android/youtube/app/ui/ek;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/ek;->e(Lcom/google/android/youtube/app/ui/ek;)Landroid/widget/FrameLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    :cond_1
    return-void
.end method
