.class public final Lcom/google/android/youtube/app/ui/es;
.super Lcom/google/android/youtube/core/a/l;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/adapter/ay;
.implements Lcom/google/android/youtube/app/ui/f;
.implements Lcom/google/android/youtube/core/async/bk;


# instance fields
.field private final a:Landroid/app/Activity;

.field private final c:Lcom/google/android/youtube/app/ui/e;

.field private final d:Lcom/google/android/youtube/app/adapter/aw;

.field private final e:Lcom/google/android/youtube/core/a/l;

.field private final f:Lcom/google/android/youtube/app/adapter/cg;

.field private final g:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private final h:Lcom/google/android/youtube/core/async/GDataRequestFactory;

.field private i:Lcom/google/android/youtube/core/model/Video;

.field private j:Lcom/google/android/youtube/core/async/GDataRequest;

.field private k:Lcom/google/android/youtube/core/async/GDataRequest;

.field private final l:I

.field private final m:Ljava/lang/String;


# direct methods
.method private constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/async/GDataRequestFactory;ILcom/google/android/youtube/app/adapter/cg;Lcom/google/android/youtube/core/a/l;Lcom/google/android/youtube/app/adapter/ac;Lcom/google/android/youtube/app/ui/e;Lcom/google/android/youtube/app/adapter/aw;I)V
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/youtube/core/a/e;

    const/4 v1, 0x0

    aput-object p7, v0, v1

    const/4 v1, 0x1

    aput-object p6, v0, v1

    const/4 v1, 0x2

    invoke-virtual {p1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-static {v2, p10}, Lcom/google/android/youtube/app/adapter/cn;->a(Landroid/view/LayoutInflater;I)Lcom/google/android/youtube/app/adapter/cn;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/a/l;-><init>([Lcom/google/android/youtube/core/a/e;)V

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/es;->a:Landroid/app/Activity;

    const-string v0, "buttonStatusOutline cannot be null"

    invoke-static {p8, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/e;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/es;->c:Lcom/google/android/youtube/app/ui/e;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/es;->c:Lcom/google/android/youtube/app/ui/e;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/ui/e;->a(Lcom/google/android/youtube/app/ui/f;)V

    const-string v0, "pagedOutline cannot be null"

    invoke-static {p9, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/aw;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/es;->d:Lcom/google/android/youtube/app/adapter/aw;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/es;->d:Lcom/google/android/youtube/app/adapter/aw;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/adapter/aw;->a(Lcom/google/android/youtube/app/adapter/ay;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/es;->d:Lcom/google/android/youtube/app/adapter/aw;

    new-instance v1, Lcom/google/android/youtube/app/ui/az;

    invoke-direct {v1}, Lcom/google/android/youtube/app/ui/az;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/aw;->a(Lcom/google/android/youtube/core/utils/t;)V

    const-string v0, "videoListOutline cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/cg;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/es;->f:Lcom/google/android/youtube/app/adapter/cg;

    const-string v0, "bodyOutline cannot be null"

    invoke-static {p6, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/a/l;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/es;->e:Lcom/google/android/youtube/core/a/l;

    const-string v0, "userAuth cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/UserAuthorizer;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/es;->g:Lcom/google/android/youtube/core/async/UserAuthorizer;

    const-string v0, "gdataRequestFactory cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/GDataRequestFactory;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/es;->h:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    iput p4, p0, Lcom/google/android/youtube/app/ui/es;->l:I

    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b010e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/es;->m:Ljava/lang/String;

    return-void
.end method

.method public static a(Landroid/app/Activity;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/bg;Lcom/google/android/youtube/app/prefetch/d;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/async/GDataRequestFactory;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/Analytics;IIILcom/google/android/youtube/core/a/g;Lcom/google/android/youtube/core/a/g;Lcom/google/android/youtube/core/a/g;)Lcom/google/android/youtube/app/ui/es;
    .locals 21

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08008b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v20

    const v16, 0x7f040027

    const v17, 0x7f040078

    const v18, 0x7f04001a

    const v19, 0x7f0400b4

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move/from16 v10, p10

    move/from16 v11, p11

    move/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    move-object/from16 v15, p15

    invoke-static/range {v0 .. v20}, Lcom/google/android/youtube/app/ui/es;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/bg;Lcom/google/android/youtube/app/prefetch/d;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/async/GDataRequestFactory;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/Analytics;IIILcom/google/android/youtube/core/a/g;Lcom/google/android/youtube/core/a/g;Lcom/google/android/youtube/core/a/g;IIIII)Lcom/google/android/youtube/app/ui/es;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/app/Activity;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/bg;Lcom/google/android/youtube/app/prefetch/d;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/async/GDataRequestFactory;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/Analytics;IIILcom/google/android/youtube/core/a/g;Lcom/google/android/youtube/core/a/g;Lcom/google/android/youtube/core/a/g;IIIII)Lcom/google/android/youtube/app/ui/es;
    .locals 21

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0015

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v12

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00cb

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b01e9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    new-instance v2, Lcom/google/android/youtube/app/adapter/cj;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/google/android/youtube/app/adapter/cj;-><init>(Landroid/content/Context;)V

    sget-object v3, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;->SMALL:Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v3, v4}, Lcom/google/android/youtube/app/adapter/cm;->a(Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;Z)Lcom/google/android/youtube/app/adapter/cm;

    move-result-object v3

    new-instance v4, Lcom/google/android/youtube/app/adapter/h;

    invoke-static/range {p0 .. p0}, Lcom/google/android/youtube/core/utils/j;->d(Landroid/content/Context;)Z

    move-result v5

    const/4 v6, 0x2

    const/4 v7, 0x0

    move-object/from16 v0, p4

    invoke-direct {v4, v0, v5, v6, v7}, Lcom/google/android/youtube/app/adapter/h;-><init>(Lcom/google/android/youtube/app/prefetch/d;ZIZ)V

    new-instance v5, Lcom/google/android/youtube/app/adapter/af;

    invoke-direct {v5}, Lcom/google/android/youtube/app/adapter/af;-><init>()V

    invoke-virtual {v5, v2}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/google/android/youtube/app/adapter/af;->a(Lcom/google/android/youtube/app/adapter/bv;)Lcom/google/android/youtube/app/adapter/af;

    move-result-object v5

    new-instance v2, Lcom/google/android/youtube/app/adapter/aw;

    invoke-interface/range {p2 .. p2}, Lcom/google/android/youtube/core/client/bc;->z()Lcom/google/android/youtube/core/async/au;

    move-result-object v6

    move-object/from16 v3, p0

    move/from16 v4, p17

    move-object/from16 v7, p5

    move/from16 v8, p10

    move/from16 v9, p11

    move/from16 v10, p12

    move-object/from16 v11, p13

    invoke-direct/range {v2 .. v11}, Lcom/google/android/youtube/app/adapter/aw;-><init>(Landroid/app/Activity;ILcom/google/android/youtube/app/adapter/bv;Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/e;IIILcom/google/android/youtube/core/a/g;)V

    new-instance v3, Lcom/google/android/youtube/app/adapter/cg;

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v5

    new-instance v9, Lcom/google/android/youtube/app/ui/et;

    move-object/from16 v0, p9

    move-object/from16 v1, p8

    invoke-direct {v9, v0, v1}, Lcom/google/android/youtube/app/ui/et;-><init>(Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/app/d;)V

    const/4 v10, 0x0

    move-object v4, v2

    move-object/from16 v6, p15

    move v7, v12

    move/from16 v8, p20

    invoke-direct/range {v3 .. v10}, Lcom/google/android/youtube/app/adapter/cg;-><init>(Lcom/google/android/youtube/core/a/e;Landroid/view/LayoutInflater;Lcom/google/android/youtube/core/a/g;IILcom/google/android/youtube/app/adapter/ch;Lcom/google/android/youtube/app/adapter/ci;)V

    new-instance v18, Lcom/google/android/youtube/app/ui/e;

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v0, v18

    move/from16 v1, p18

    invoke-direct {v0, v4, v1, v5, v14}, Lcom/google/android/youtube/app/ui/e;-><init>(Landroid/view/LayoutInflater;ILandroid/view/ViewGroup;Ljava/lang/String;)V

    new-instance v9, Lcom/google/android/youtube/core/a/l;

    const/4 v4, 0x2

    new-array v4, v4, [Lcom/google/android/youtube/core/a/e;

    const/4 v5, 0x0

    aput-object v3, v4, v5

    const/4 v5, 0x1

    aput-object v18, v4, v5

    invoke-direct {v9, v4}, Lcom/google/android/youtube/core/a/l;-><init>([Lcom/google/android/youtube/core/a/e;)V

    new-instance v4, Lcom/google/android/youtube/app/adapter/ac;

    move-object/from16 v5, p0

    move-object/from16 v6, p14

    move-object v7, v13

    move/from16 v8, p16

    invoke-direct/range {v4 .. v9}, Lcom/google/android/youtube/app/adapter/ac;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/a/g;Ljava/lang/String;ILcom/google/android/youtube/core/a/e;)V

    new-instance v10, Lcom/google/android/youtube/app/ui/es;

    move-object/from16 v11, p0

    move-object/from16 v12, p6

    move-object/from16 v13, p7

    move/from16 v14, p10

    move-object v15, v3

    move-object/from16 v16, v9

    move-object/from16 v17, v4

    move-object/from16 v19, v2

    move/from16 v20, p19

    invoke-direct/range {v10 .. v20}, Lcom/google/android/youtube/app/ui/es;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/async/GDataRequestFactory;ILcom/google/android/youtube/app/adapter/cg;Lcom/google/android/youtube/core/a/l;Lcom/google/android/youtube/app/adapter/ac;Lcom/google/android/youtube/app/ui/e;Lcom/google/android/youtube/app/adapter/aw;I)V

    return-object v10
.end method

.method public static b(Landroid/app/Activity;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/bg;Lcom/google/android/youtube/app/prefetch/d;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/async/GDataRequestFactory;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/Analytics;IIILcom/google/android/youtube/core/a/g;Lcom/google/android/youtube/core/a/g;Lcom/google/android/youtube/core/a/g;)Lcom/google/android/youtube/app/ui/es;
    .locals 21

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08008b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v20

    const v16, 0x7f040026

    const v17, 0x7f0400b2

    const v18, 0x7f040019

    const v19, 0x7f0400b3

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move/from16 v10, p10

    move/from16 v11, p11

    move/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    move-object/from16 v15, p15

    invoke-static/range {v0 .. v20}, Lcom/google/android/youtube/app/ui/es;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/bg;Lcom/google/android/youtube/app/prefetch/d;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/async/GDataRequestFactory;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/Analytics;IIILcom/google/android/youtube/core/a/g;Lcom/google/android/youtube/core/a/g;Lcom/google/android/youtube/core/a/g;IIIII)Lcom/google/android/youtube/app/ui/es;

    move-result-object v0

    return-object v0
.end method

.method private o()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/es;->j:Lcom/google/android/youtube/core/async/GDataRequest;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/es;->k:Lcom/google/android/youtube/core/async/GDataRequest;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/es;->d:Lcom/google/android/youtube/app/adapter/aw;

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/es;->k:Lcom/google/android/youtube/core/async/GDataRequest;

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/es;->j:Lcom/google/android/youtube/core/async/GDataRequest;

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/aw;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/es;->j:Lcom/google/android/youtube/core/async/GDataRequest;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/es;->d:Lcom/google/android/youtube/app/adapter/aw;

    new-array v1, v4, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/es;->j:Lcom/google/android/youtube/core/async/GDataRequest;

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/aw;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/es;->k:Lcom/google/android/youtube/core/async/GDataRequest;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/es;->d:Lcom/google/android/youtube/app/adapter/aw;

    new-array v1, v4, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/es;->k:Lcom/google/android/youtube/core/async/GDataRequest;

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/aw;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/es;->d:Lcom/google/android/youtube/app/adapter/aw;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/aw;->f_()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/es;->c:Lcom/google/android/youtube/app/ui/e;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/es;->m:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/youtube/app/ui/e;->a(Ljava/lang/String;Z)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/Branding;)V
    .locals 3

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Branding;->featuredPlaylistId:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/es;->h:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Branding;->featuredPlaylistId:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/youtube/app/ui/es;->l:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->a(Ljava/lang/String;I)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/es;->k:Lcom/google/android/youtube/core/async/GDataRequest;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/es;->i:Lcom/google/android/youtube/core/model/Video;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/es;->i:Lcom/google/android/youtube/core/model/Video;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Video;->relatedUri:Landroid/net/Uri;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/es;->g:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bk;)V

    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/es;->o()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/es;->i:Lcom/google/android/youtube/core/model/Video;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/es;->h:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/es;->i:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->relatedUri:Landroid/net/Uri;

    iget v2, p0, Lcom/google/android/youtube/app/ui/es;->l:I

    invoke-virtual {v0, v1, p1, v2}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->a(Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;I)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/es;->j:Lcom/google/android/youtube/core/async/GDataRequest;

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/es;->o()V

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Video;)V
    .locals 1

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/es;->i:Lcom/google/android/youtube/core/model/Video;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/es;->k:Lcom/google/android/youtube/core/async/GDataRequest;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/es;->j:Lcom/google/android/youtube/core/async/GDataRequest;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/es;->d:Lcom/google/android/youtube/app/adapter/aw;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/aw;->b()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/es;->d:Lcom/google/android/youtube/app/adapter/aw;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/aw;->f_()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/es;->i:Lcom/google/android/youtube/core/model/Video;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/es;->h:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/es;->i:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->relatedUri:Landroid/net/Uri;

    iget v2, p0, Lcom/google/android/youtube/app/ui/es;->l:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->a(Landroid/net/Uri;I)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/es;->j:Lcom/google/android/youtube/core/async/GDataRequest;

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/es;->o()V

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/es;->c:Lcom/google/android/youtube/app/ui/e;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/app/ui/e;->b(Ljava/lang/String;Z)V

    return-void
.end method

.method public final a(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/es;->e:Lcom/google/android/youtube/core/a/l;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/a/l;->c(Z)V

    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/es;->c:Lcom/google/android/youtube/app/ui/e;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/e;->e()V

    return-void
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/es;->c:Lcom/google/android/youtube/app/ui/e;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/e;->d()V

    return-void
.end method

.method public final d(I)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final e()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/es;->c:Lcom/google/android/youtube/app/ui/e;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/es;->m:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/ui/e;->a(Ljava/lang/String;Z)V

    return-void
.end method

.method public final f()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/es;->c:Lcom/google/android/youtube/app/ui/e;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/e;->p_()V

    return-void
.end method

.method public final g()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/es;->d:Lcom/google/android/youtube/app/adapter/aw;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/aw;->e()V

    return-void
.end method

.method public final g_()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/es;->i:Lcom/google/android/youtube/core/model/Video;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/es;->h:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/es;->i:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->relatedUri:Landroid/net/Uri;

    iget v2, p0, Lcom/google/android/youtube/app/ui/es;->l:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->a(Landroid/net/Uri;I)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/es;->j:Lcom/google/android/youtube/core/async/GDataRequest;

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/es;->o()V

    :cond_0
    return-void
.end method

.method public final h()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/es;->d:Lcom/google/android/youtube/app/adapter/aw;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/aw;->d()V

    return-void
.end method

.method public final i()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/ui/es;->a(Lcom/google/android/youtube/core/model/Video;)V

    return-void
.end method

.method public final j()V
    .locals 6

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/es;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/es;->f:Lcom/google/android/youtube/app/adapter/cg;

    const v2, 0x7f080095

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    const v3, 0x7f080097

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    const v4, 0x7f080096

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    const v5, 0x7f080098

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/google/android/youtube/app/adapter/cg;->a(IIII)V

    return-void
.end method

.method public final u_()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/es;->c:Lcom/google/android/youtube/app/ui/e;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/e;->d()V

    return-void
.end method
