.class public final Lcom/google/android/youtube/app/ui/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/animation/Animation$AnimationListener;
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field private final A:Landroid/view/animation/Animation;

.field private final B:J

.field private C:Z

.field private D:Z

.field private final E:I

.field private F:Lcom/google/android/youtube/core/model/UserProfile;

.field private G:Lcom/google/android/youtube/app/ui/j;

.field private final a:Landroid/app/Activity;

.field private final b:Lcom/google/android/youtube/core/client/be;

.field private final c:Lcom/google/android/youtube/app/ui/k;

.field private final d:Lcom/google/android/youtube/app/ui/i;

.field private final e:Lcom/google/android/youtube/core/e;

.field private final f:Lcom/google/android/youtube/app/d;

.field private final g:Landroid/content/res/Resources;

.field private final h:Landroid/view/View;

.field private final i:Landroid/view/View;

.field private final j:Landroid/widget/ImageView;

.field private final k:Landroid/widget/TextView;

.field private final l:Landroid/widget/TextView;

.field private final m:Landroid/widget/TextView;

.field private final n:Landroid/widget/TextView;

.field private final o:Landroid/view/View;

.field private final p:Landroid/view/View;

.field private final q:Landroid/widget/TextView;

.field private final r:Landroid/widget/ProgressBar;

.field private final s:I

.field private final t:F

.field private u:I

.field private v:I

.field private final w:Lcom/google/android/youtube/app/ui/FixedAspectRatioFrameLayout;

.field private final x:Landroid/widget/ImageView;

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/View;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/app/d;)V
    .locals 7

    const v6, 0x7f0b00bd

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/app/ui/g;-><init>(Landroid/app/Activity;Landroid/view/View;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/app/d;I)V

    return-void
.end method

.method private constructor <init>(Landroid/app/Activity;Landroid/view/View;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/app/d;I)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "activity may not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/g;->a:Landroid/app/Activity;

    const-string v0, "imageClient may not be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/client/be;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/g;->b:Lcom/google/android/youtube/core/client/be;

    const-string v0, "errorHelper may not be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/e;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/g;->e:Lcom/google/android/youtube/core/e;

    const-string v0, "navigation may not be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/d;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/g;->f:Lcom/google/android/youtube/app/d;

    new-instance v0, Lcom/google/android/youtube/app/ui/k;

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/ui/k;-><init>(Lcom/google/android/youtube/app/ui/g;B)V

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/g;->c:Lcom/google/android/youtube/app/ui/k;

    new-instance v0, Lcom/google/android/youtube/app/ui/i;

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/ui/i;-><init>(Lcom/google/android/youtube/app/ui/g;B)V

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/g;->d:Lcom/google/android/youtube/app/ui/i;

    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/g;->g:Landroid/content/res/Resources;

    const v0, 0x7f0700b8

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/g;->h:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->h:Landroid/view/View;

    const v2, 0x7f0700bb

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/g;->i:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->i:Landroid/view/View;

    const v2, 0x7f070070

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/g;->j:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->i:Landroid/view/View;

    const v2, 0x7f070073

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/g;->k:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->i:Landroid/view/View;

    const v2, 0x7f070074

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/g;->m:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->i:Landroid/view/View;

    const v2, 0x7f070076

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/g;->l:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->i:Landroid/view/View;

    const v2, 0x7f070075

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/g;->n:Landroid/widget/TextView;

    const v0, 0x7f0b00bd

    iput v0, p0, Lcom/google/android/youtube/app/ui/g;->E:I

    iput-boolean v4, p0, Lcom/google/android/youtube/app/ui/g;->y:Z

    iput-boolean v4, p0, Lcom/google/android/youtube/app/ui/g;->z:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->h:Landroid/view/View;

    const v2, 0x7f0700b9

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/FixedAspectRatioFrameLayout;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/g;->w:Lcom/google/android/youtube/app/ui/FixedAspectRatioFrameLayout;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->w:Lcom/google/android/youtube/app/ui/FixedAspectRatioFrameLayout;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->w:Lcom/google/android/youtube/app/ui/FixedAspectRatioFrameLayout;

    const v2, 0x7f0700ba

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/ui/FixedAspectRatioFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const-string v2, "banner may not be null if bannerContainer is set"

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/g;->x:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/g;->w:Lcom/google/android/youtube/app/ui/FixedAspectRatioFrameLayout;

    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/g;->y:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Lcom/google/android/youtube/app/ui/FixedAspectRatioFrameLayout;->setVisibility(I)V

    :goto_1
    const/high16 v0, 0x10a0000

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/g;->A:Landroid/view/animation/Animation;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->A:Landroid/view/animation/Animation;

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->g:Landroid/content/res/Resources;

    const/high16 v2, 0x10e0000

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-long v2, v0

    iput-wide v2, p0, Lcom/google/android/youtube/app/ui/g;->B:J

    iput-boolean v1, p0, Lcom/google/android/youtube/app/ui/g;->C:Z

    iput-boolean v1, p0, Lcom/google/android/youtube/app/ui/g;->D:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->i:Landroid/view/View;

    const v1, 0x7f0700bc

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/g;->o:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->i:Landroid/view/View;

    const v1, 0x7f07007c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/g;->p:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->p:Landroid/view/View;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->i:Landroid/view/View;

    const v1, 0x7f0700bd

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/g;->q:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->q:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->q:Landroid/widget/TextView;

    new-instance v1, Lcom/google/android/youtube/app/ui/dg;

    invoke-direct {v1, p1}, Lcom/google/android/youtube/app/ui/dg;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->i:Landroid/view/View;

    const v1, 0x7f070071

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/g;->r:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->p:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_2
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->g:Landroid/content/res/Resources;

    const v1, 0x7f090033

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/ui/g;->s:I

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->g:Landroid/content/res/Resources;

    const v1, 0x7f0f0003

    invoke-virtual {v0, v1, v4, v4}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/ui/g;->t:F

    const v0, 0x7f0200c8

    iput v0, p0, Lcom/google/android/youtube/app/ui/g;->u:I

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->g:Landroid/content/res/Resources;

    const v1, 0x7f090001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/ui/g;->v:I

    return-void

    :cond_1
    const/16 v0, 0x8

    goto/16 :goto_0

    :cond_2
    iput-object v5, p0, Lcom/google/android/youtube/app/ui/g;->x:Landroid/widget/ImageView;

    goto/16 :goto_1

    :cond_3
    iput-object v5, p0, Lcom/google/android/youtube/app/ui/g;->q:Landroid/widget/TextView;

    iput-object v5, p0, Lcom/google/android/youtube/app/ui/g;->r:Landroid/widget/ProgressBar;

    goto :goto_2
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/app/d;)V
    .locals 7

    const v0, 0x1020002

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const v6, 0x7f0b00bd

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/app/ui/g;-><init>(Landroid/app/Activity;Landroid/view/View;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/app/d;I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/g;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->j:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/g;Z)Z
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/ui/g;->D:Z

    return v0
.end method

.method private b()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->j:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->j:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/g;->g:Landroid/content/res/Resources;

    const v2, 0x7f0201c2

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/google/android/youtube/app/ui/g;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/g;->b()V

    return-void
.end method

.method private b(Lcom/google/android/youtube/core/model/UserProfile;)V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/g;->F:Lcom/google/android/youtube/core/model/UserProfile;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->a:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->g(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/g;->k:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/g;->k:Landroid/widget/TextView;

    iget-object v2, p1, Lcom/google/android/youtube/core/model/UserProfile;->displayUsername:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/g;->a:Landroid/app/Activity;

    iget v2, p0, Lcom/google/android/youtube/app/ui/g;->E:I

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/g;->m:Landroid/widget/TextView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/g;->m:Landroid/widget/TextView;

    const-string v2, "%1$,d"

    new-array v3, v7, [Ljava/lang/Object;

    iget-wide v4, p1, Lcom/google/android/youtube/core/model/UserProfile;->uploadViewsCount:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/g;->l:Landroid/widget/TextView;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/g;->l:Landroid/widget/TextView;

    const-string v2, "%1$,d"

    new-array v3, v7, [Ljava/lang/Object;

    iget v4, p1, Lcom/google/android/youtube/core/model/UserProfile;->uploadedCount:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/g;->n:Landroid/widget/TextView;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/g;->n:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->n:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/g;->g:Landroid/content/res/Resources;

    const v2, 0x7f0c000b

    iget v3, p1, Lcom/google/android/youtube/core/model/UserProfile;->subscribersCount:I

    new-array v4, v7, [Ljava/lang/Object;

    iget v5, p1, Lcom/google/android/youtube/core/model/UserProfile;->subscribersCount:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->j:Landroid/widget/ImageView;

    if-eqz v0, :cond_4

    iget-object v0, p1, Lcom/google/android/youtube/core/model/UserProfile;->thumbnailUri:Landroid/net/Uri;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->b:Lcom/google/android/youtube/core/client/be;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/UserProfile;->thumbnailUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/g;->a:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/g;->c:Lcom/google/android/youtube/app/ui/k;

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/be;->a(Landroid/net/Uri;Lcom/google/android/youtube/core/async/n;)V

    :goto_0
    return-void

    :cond_4
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/g;->b()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/ui/g;Z)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/ui/g;->C:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/ui/g;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->x:Landroid/widget/ImageView;

    return-object v0
.end method

.method private c()V
    .locals 5

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/g;->i:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/g;->i:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getPaddingLeft()I

    move-result v3

    iget-boolean v1, p0, Lcom/google/android/youtube/app/ui/g;->y:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/youtube/app/ui/g;->z:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_1

    :goto_1
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/g;->i:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getPaddingRight()I

    move-result v1

    iget-object v4, p0, Lcom/google/android/youtube/app/ui/g;->i:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getPaddingBottom()I

    move-result v4

    invoke-virtual {v2, v3, v0, v1, v4}, Landroid/view/View;->setPadding(IIII)V

    return-void

    :cond_0
    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->g:Landroid/content/res/Resources;

    const v1, 0x7f08007a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    goto :goto_1
.end method

.method static synthetic d(Lcom/google/android/youtube/app/ui/g;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/g;->y:Z

    return v0
.end method

.method static synthetic e(Lcom/google/android/youtube/app/ui/g;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/g;->C:Z

    return v0
.end method

.method static synthetic f(Lcom/google/android/youtube/app/ui/g;)Landroid/view/animation/Animation;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->A:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/youtube/app/ui/g;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/youtube/app/ui/g;->B:J

    return-wide v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    const v3, 0x7f090037

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->i:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/g;->g:Landroid/content/res/Resources;

    const v2, 0x7f090041

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->g:Landroid/content/res/Resources;

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/g;->k:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/g;->k:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/g;->n:Landroid/widget/TextView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/g;->n:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_1
    const v0, 0x7f02013a

    iput v0, p0, Lcom/google/android/youtube/app/ui/g;->u:I

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->g:Landroid/content/res/Resources;

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/ui/g;->v:I

    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;Z)V
    .locals 7

    const/16 v6, 0x8

    const/4 v5, 0x1

    const/high16 v1, 0x3f800000

    const/4 v4, 0x0

    if-eqz p2, :cond_1

    move v0, v1

    :goto_0
    sget-object v2, Lcom/google/android/youtube/app/ui/h;->a:[I

    invoke-virtual {p1}, Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget v0, p0, Lcom/google/android/youtube/app/ui/g;->t:F

    goto :goto_0

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/g;->p:Landroid/view/View;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/g;->p:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setClickable(Z)V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/g;->p:Landroid/view/View;

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/utils/j;->a(Landroid/view/View;F)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->q:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->q:Landroid/widget/TextView;

    iget v1, p0, Lcom/google/android/youtube/app/ui/g;->s:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->q:Landroid/widget/TextView;

    const v1, 0x7f0b0195

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->q:Landroid/widget/TextView;

    const v1, 0x7f0200c9

    invoke-virtual {v0, v1, v4, v4, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->r:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->r:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v6}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_1

    :pswitch_1
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/g;->p:Landroid/view/View;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/g;->p:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setClickable(Z)V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/g;->p:Landroid/view/View;

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/utils/j;->a(Landroid/view/View;F)V

    :cond_4
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->q:Landroid/widget/TextView;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->q:Landroid/widget/TextView;

    iget v1, p0, Lcom/google/android/youtube/app/ui/g;->v:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->q:Landroid/widget/TextView;

    const v1, 0x7f0b018b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->q:Landroid/widget/TextView;

    iget v1, p0, Lcom/google/android/youtube/app/ui/g;->u:I

    invoke-virtual {v0, v1, v4, v4, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_5
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->r:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->r:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v6}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->p:Landroid/view/View;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->p:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setClickable(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->p:Landroid/view/View;

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/j;->a(Landroid/view/View;F)V

    :cond_6
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->q:Landroid/widget/TextView;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->q:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_7
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->r:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->r:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lcom/google/android/youtube/app/ui/j;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/g;->G:Lcom/google/android/youtube/app/ui/j;

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Branding;)V
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/g;->w:Lcom/google/android/youtube/app/ui/FixedAspectRatioFrameLayout;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/g;->x:Landroid/widget/ImageView;

    if-eqz v2, :cond_1

    if-eqz p1, :cond_3

    new-instance v2, Landroid/util/DisplayMetrics;

    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/g;->a:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/g;->g:Landroid/content/res/Resources;

    const v4, 0x7f0e0004

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    if-eqz v3, :cond_2

    iget v2, v2, Landroid/util/DisplayMetrics;->densityDpi:I

    sparse-switch v2, :sswitch_data_0

    iget-object v2, p1, Lcom/google/android/youtube/core/model/Branding;->channelBannerTabletMediumUri:Landroid/net/Uri;

    :goto_0
    if-eqz v2, :cond_5

    iput-boolean v0, p0, Lcom/google/android/youtube/app/ui/g;->z:Z

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/g;->b:Lcom/google/android/youtube/core/client/be;

    iget-object v4, p0, Lcom/google/android/youtube/app/ui/g;->a:Landroid/app/Activity;

    iget-object v5, p0, Lcom/google/android/youtube/app/ui/g;->d:Lcom/google/android/youtube/app/ui/i;

    invoke-static {v4, v5}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v4

    invoke-interface {v3, v2, v4}, Lcom/google/android/youtube/core/client/be;->a(Landroid/net/Uri;Lcom/google/android/youtube/core/async/n;)V

    iget-boolean v2, p0, Lcom/google/android/youtube/app/ui/g;->y:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/youtube/app/ui/g;->D:Z

    if-nez v2, :cond_4

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/youtube/app/ui/g;->C:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->w:Lcom/google/android/youtube/app/ui/FixedAspectRatioFrameLayout;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/FixedAspectRatioFrameLayout;->setVisibility(I)V

    :cond_0
    :goto_2
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/g;->c()V

    :cond_1
    return-void

    :sswitch_0
    iget-object v2, p1, Lcom/google/android/youtube/core/model/Branding;->channelBannerTabletLowUri:Landroid/net/Uri;

    goto :goto_0

    :sswitch_1
    iget-object v2, p1, Lcom/google/android/youtube/core/model/Branding;->channelBannerTabletHdUri:Landroid/net/Uri;

    goto :goto_0

    :sswitch_2
    iget-object v2, p1, Lcom/google/android/youtube/core/model/Branding;->channelBannerTabletExtraHdUri:Landroid/net/Uri;

    goto :goto_0

    :cond_2
    iget v2, v2, Landroid/util/DisplayMetrics;->densityDpi:I

    sparse-switch v2, :sswitch_data_1

    iget-object v2, p1, Lcom/google/android/youtube/core/model/Branding;->channelBannerMobileMediumUri:Landroid/net/Uri;

    goto :goto_0

    :sswitch_3
    iget-object v2, p1, Lcom/google/android/youtube/core/model/Branding;->channelBannerMobileLowUri:Landroid/net/Uri;

    goto :goto_0

    :sswitch_4
    iget-object v2, p1, Lcom/google/android/youtube/core/model/Branding;->channelBannerMobileMediumHdUri:Landroid/net/Uri;

    goto :goto_0

    :sswitch_5
    iget-object v2, p1, Lcom/google/android/youtube/core/model/Branding;->channelBannerMobileHdUri:Landroid/net/Uri;

    goto :goto_0

    :sswitch_6
    iget-object v2, p1, Lcom/google/android/youtube/core/model/Branding;->channelBannerMobileExtraHdUri:Landroid/net/Uri;

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    :cond_5
    iput-boolean v1, p0, Lcom/google/android/youtube/app/ui/g;->z:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->w:Lcom/google/android/youtube/app/ui/FixedAspectRatioFrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/FixedAspectRatioFrameLayout;->setVisibility(I)V

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x78 -> :sswitch_0
        0xf0 -> :sswitch_1
        0x140 -> :sswitch_2
        0x1e0 -> :sswitch_2
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x78 -> :sswitch_3
        0xf0 -> :sswitch_4
        0x140 -> :sswitch_5
        0x1e0 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserProfile;)V
    .locals 0

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/ui/g;->b(Lcom/google/android/youtube/core/model/UserProfile;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2

    const-string v0, "Error retrieving user profile"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    instance-of v0, p2, Lorg/apache/http/client/HttpResponseException;

    if-eqz v0, :cond_0

    move-object v0, p2

    check-cast v0, Lorg/apache/http/client/HttpResponseException;

    invoke-virtual {v0}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v0

    const/16 v1, 0x193

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->e:Lcom/google/android/youtube/core/e;

    const v1, 0x7f0b01b6

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/e;->a(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->e:Lcom/google/android/youtube/core/e;

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/core/e;->b(Ljava/lang/Throwable;)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/g;->b()V

    goto :goto_0
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Lcom/google/android/youtube/core/model/UserProfile;

    invoke-direct {p0, p2}, Lcom/google/android/youtube/app/ui/g;->b(Lcom/google/android/youtube/core/model/UserProfile;)V

    return-void
.end method

.method public final a(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->o:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->o:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final a(ZF)V
    .locals 2

    iput-boolean p1, p0, Lcom/google/android/youtube/app/ui/g;->y:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->w:Lcom/google/android/youtube/app/ui/FixedAspectRatioFrameLayout;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/g;->w:Lcom/google/android/youtube/app/ui/FixedAspectRatioFrameLayout;

    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/g;->y:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/g;->z:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/ui/FixedAspectRatioFrameLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->w:Lcom/google/android/youtube/app/ui/FixedAspectRatioFrameLayout;

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/app/ui/FixedAspectRatioFrameLayout;->setAspectRatio(F)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/g;->c()V

    :cond_0
    return-void

    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->x:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->x:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    :cond_0
    return-void
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->G:Lcom/google/android/youtube/app/ui/j;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->o:Landroid/view/View;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->f:Lcom/google/android/youtube/app/d;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/g;->F:Lcom/google/android/youtube/core/model/UserProfile;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/UserProfile;->uri:Landroid/net/Uri;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/d;->a(Landroid/net/Uri;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->p:Landroid/view/View;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/g;->G:Lcom/google/android/youtube/app/ui/j;

    invoke-interface {v0}, Lcom/google/android/youtube/app/ui/j;->n_()V

    goto :goto_0
.end method
