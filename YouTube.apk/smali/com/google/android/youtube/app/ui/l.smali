.class public final Lcom/google/android/youtube/app/ui/l;
.super Lcom/google/android/youtube/core/a/l;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

.field private final c:Landroid/app/Activity;

.field private final d:Lcom/google/android/youtube/app/d;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/adapter/cn;Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;Landroid/app/Activity;Lcom/google/android/youtube/app/d;Landroid/content/Context;)V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/youtube/core/a/e;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/a/l;-><init>([Lcom/google/android/youtube/core/a/e;)V

    const-string v0, "category cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/l;->a:Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

    const-string v0, "navigation cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/d;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/l;->d:Lcom/google/android/youtube/app/d;

    iput-object p3, p0, Lcom/google/android/youtube/app/ui/l;->c:Landroid/app/Activity;

    const-string v0, "categoryOutline cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/adapter/cn;->b()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/adapter/cn;->b()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f070079

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    new-instance v2, Lcom/google/android/youtube/app/ui/dg;

    invoke-direct {v2, p5}, Lcom/google/android/youtube/app/ui/dg;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    invoke-virtual {p3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p2, v2}, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;->toString(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f070078

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method public static a(Landroid/app/Activity;Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;Lcom/google/android/youtube/app/d;)Lcom/google/android/youtube/app/ui/l;
    .locals 6

    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040021

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/app/adapter/cn;

    invoke-direct {v1, v0}, Lcom/google/android/youtube/app/adapter/cn;-><init>(Landroid/view/View;)V

    new-instance v0, Lcom/google/android/youtube/app/ui/l;

    move-object v2, p1

    move-object v3, p0

    move-object v4, p2

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/ui/l;-><init>(Lcom/google/android/youtube/app/adapter/cn;Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;Landroid/app/Activity;Lcom/google/android/youtube/app/d;Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public final d(I)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/l;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    sget-object v2, Lcom/google/android/youtube/app/ui/m;->a:[I

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/l;->a:Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

    invoke-virtual {v3}, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    move-object v0, v1

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/l;->d:Lcom/google/android/youtube/app/d;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/l;->a:Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

    invoke-interface {v1, v0, v2}, Lcom/google/android/youtube/app/d;->a(Landroid/net/Uri;Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;)V

    return-void

    :pswitch_0
    sget-object v0, Lcom/google/android/youtube/core/async/GDataRequestFactory$ChannelFeed;->MOST_SUBSCRIBED:Lcom/google/android/youtube/core/async/GDataRequestFactory$ChannelFeed;

    invoke-static {v0}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->a(Lcom/google/android/youtube/core/async/GDataRequestFactory$ChannelFeed;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/google/android/youtube/core/async/GDataRequestFactory$ChannelFeed;->MOST_VIEWED:Lcom/google/android/youtube/core/async/GDataRequestFactory$ChannelFeed;

    invoke-static {v0}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->a(Lcom/google/android/youtube/core/async/GDataRequestFactory$ChannelFeed;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->F()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->j(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    sget-object v0, Lcom/google/android/youtube/core/async/GDataRequestFactory$ChannelFeed;->NOTEWORTHY:Lcom/google/android/youtube/core/async/GDataRequestFactory$ChannelFeed;

    invoke-static {v0}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->a(Lcom/google/android/youtube/core/async/GDataRequestFactory$ChannelFeed;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    sget-object v0, Lcom/google/android/youtube/core/async/GDataRequestFactory;->s:Landroid/net/Uri;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
