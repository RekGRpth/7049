.class public final Lcom/google/android/youtube/app/ui/n;
.super Lcom/google/android/youtube/core/a/l;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/bk;
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field private final a:Lcom/google/android/youtube/core/client/bc;

.field private final c:Lcom/google/android/youtube/core/async/h;

.field private final d:Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

.field private final e:Lcom/google/android/youtube/core/a/c;

.field private final f:Lcom/google/android/youtube/app/adapter/cn;

.field private final g:I

.field private final h:Lcom/google/android/youtube/app/YouTubeApplication;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/a/c;Lcom/google/android/youtube/core/a/l;Lcom/google/android/youtube/app/adapter/cn;Landroid/content/res/Resources;Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/youtube/core/a/e;

    const/4 v1, 0x0

    aput-object p4, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/a/l;-><init>([Lcom/google/android/youtube/core/a/e;)V

    invoke-virtual {p1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/n;->h:Lcom/google/android/youtube/app/YouTubeApplication;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/n;->h:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->a()Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/n;->a:Lcom/google/android/youtube/core/client/bc;

    invoke-static {p1, p0}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/n;->c:Lcom/google/android/youtube/core/async/h;

    const-string v0, "listAdapterOutline cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/a/c;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/n;->e:Lcom/google/android/youtube/core/a/c;

    const-string v0, "loadingOutline cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/cn;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/n;->f:Lcom/google/android/youtube/app/adapter/cn;

    const-string v0, "category cannot be null"

    invoke-static {p7, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/n;->d:Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

    const v0, 0x7f0a0009

    invoke-virtual {p6, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    const v1, 0x7f0a0008

    invoke-virtual {p6, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    mul-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/youtube/app/ui/n;->g:I

    const-string v0, "bodyOutline cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p2, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bk;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/n;->e:Lcom/google/android/youtube/core/a/c;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/c;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/ab;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/adapter/ab;->a(Landroid/net/Uri;)V

    return-void
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/n;->e:Lcom/google/android/youtube/core/a/c;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/c;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/ab;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/app/adapter/ab;->a(Landroid/net/Uri;Ljava/lang/String;)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Subscription;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/n;->e:Lcom/google/android/youtube/core/a/c;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/c;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/ab;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/adapter/ab;->a(Lcom/google/android/youtube/core/model/Subscription;)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 4

    sget-object v0, Lcom/google/android/youtube/app/ui/p;->a:[I

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/n;->d:Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/n;->a:Lcom/google/android/youtube/core/client/bc;

    iget v1, p0, Lcom/google/android/youtube/app/ui/n;->g:I

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/n;->c:Lcom/google/android/youtube/core/async/h;

    invoke-interface {v0, p1, v1, v2}, Lcom/google/android/youtube/core/client/bc;->f(Lcom/google/android/youtube/core/model/UserAuth;ILcom/google/android/youtube/core/async/n;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/n;->h:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->F()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/n;->a:Lcom/google/android/youtube/core/client/bc;

    iget v2, p0, Lcom/google/android/youtube/app/ui/n;->g:I

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/n;->c:Lcom/google/android/youtube/core/async/h;

    invoke-interface {v1, v2, v0, v3}, Lcom/google/android/youtube/core/client/bc;->a(ILjava/lang/String;Lcom/google/android/youtube/core/async/n;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/n;->a:Lcom/google/android/youtube/core/client/bc;

    sget-object v1, Lcom/google/android/youtube/core/async/GDataRequestFactory$ChannelFeed;->MOST_VIEWED:Lcom/google/android/youtube/core/async/GDataRequestFactory$ChannelFeed;

    iget v2, p0, Lcom/google/android/youtube/app/ui/n;->g:I

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/n;->c:Lcom/google/android/youtube/core/async/h;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/youtube/core/client/bc;->a(Lcom/google/android/youtube/core/async/GDataRequestFactory$ChannelFeed;ILcom/google/android/youtube/core/async/n;)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/n;->a:Lcom/google/android/youtube/core/client/bc;

    sget-object v1, Lcom/google/android/youtube/core/async/GDataRequestFactory$ChannelFeed;->MOST_SUBSCRIBED:Lcom/google/android/youtube/core/async/GDataRequestFactory$ChannelFeed;

    iget v2, p0, Lcom/google/android/youtube/app/ui/n;->g:I

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/n;->c:Lcom/google/android/youtube/core/async/h;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/youtube/core/client/bc;->a(Lcom/google/android/youtube/core/async/GDataRequestFactory$ChannelFeed;ILcom/google/android/youtube/core/async/n;)V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/n;->a:Lcom/google/android/youtube/core/client/bc;

    sget-object v1, Lcom/google/android/youtube/core/async/GDataRequestFactory$ChannelFeed;->NOTEWORTHY:Lcom/google/android/youtube/core/async/GDataRequestFactory$ChannelFeed;

    iget v2, p0, Lcom/google/android/youtube/app/ui/n;->g:I

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/n;->c:Lcom/google/android/youtube/core/async/h;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/youtube/core/client/bc;->a(Lcom/google/android/youtube/core/async/GDataRequestFactory$ChannelFeed;ILcom/google/android/youtube/core/async/n;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    check-cast p2, Lcom/google/android/youtube/core/model/Page;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/n;->f:Lcom/google/android/youtube/app/adapter/cn;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/cn;->c(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/n;->e:Lcom/google/android/youtube/core/a/c;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/c;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/bm;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/Page;->entries:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/bm;->b(Ljava/lang/Iterable;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0

    return-void
.end method

.method public final b(Landroid/net/Uri;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/n;->e:Lcom/google/android/youtube/core/a/c;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/c;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/ab;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/adapter/ab;->b(Landroid/net/Uri;)V

    return-void
.end method

.method public final g_()V
    .locals 0

    return-void
.end method

.method public final q_()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/n;->e:Lcom/google/android/youtube/core/a/c;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/c;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/ab;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/ab;->notifyDataSetChanged()V

    return-void
.end method
