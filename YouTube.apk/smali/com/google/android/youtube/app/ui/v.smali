.class public final Lcom/google/android/youtube/app/ui/v;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:I

.field private final c:Landroid/content/res/Resources;

.field private final d:Landroid/view/LayoutInflater;

.field private final e:Landroid/view/View;

.field private final f:Landroid/app/Dialog;

.field private final g:Lcom/google/android/youtube/app/ui/y;

.field private h:Landroid/view/View$OnClickListener;

.field private final i:Landroid/widget/AdapterView$OnItemClickListener;

.field private final j:I

.field private final k:I

.field private l:I

.field private m:Landroid/view/View;

.field private n:Lcom/google/android/youtube/app/ui/ab;

.field private o:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;I)V
    .locals 4

    const/4 v3, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v3, p0, Lcom/google/android/youtube/app/ui/v;->o:Z

    const-string v0, "activity can\'t be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/v;->a:Landroid/app/Activity;

    iput p2, p0, Lcom/google/android/youtube/app/ui/v;->b:I

    invoke-virtual {p1}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/app/ui/v;->j:I

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    const/high16 v1, -0x80000000

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/ui/v;->k:I

    invoke-virtual {p1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/v;->d:Landroid/view/LayoutInflater;

    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/v;->c:Landroid/content/res/Resources;

    new-instance v0, Lcom/google/android/youtube/app/ui/w;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/ui/w;-><init>(Lcom/google/android/youtube/app/ui/v;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/v;->h:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/google/android/youtube/app/ui/x;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/ui/x;-><init>(Lcom/google/android/youtube/app/ui/v;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/v;->i:Landroid/widget/AdapterView$OnItemClickListener;

    new-instance v0, Lcom/google/android/youtube/app/ui/y;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/ui/y;-><init>(Lcom/google/android/youtube/app/ui/v;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/v;->g:Lcom/google/android/youtube/app/ui/y;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/v;->d:Landroid/view/LayoutInflater;

    const v1, 0x7f04002e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/v;->e:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/v;->e:Landroid/view/View;

    const v1, 0x7f070087

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/v;->g:Lcom/google/android/youtube/app/ui/y;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/v;->i:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    new-instance v0, Landroid/app/Dialog;

    const v1, 0x7f0d002e

    invoke-direct {v0, p1, v1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/v;->f:Landroid/app/Dialog;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/v;->f:Landroid/app/Dialog;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/v;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/v;->f:Landroid/app/Dialog;

    invoke-virtual {v0, v3}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/v;->f:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    const/16 v1, 0x33

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    const/4 v1, -0x2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08006f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    const v1, 0x1030002

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    const/16 v1, 0x300

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/v;->f:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/v;Landroid/view/View;)V
    .locals 5

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/v;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {p1, v1}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/v;->f:Landroid/app/Dialog;

    invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    iget v3, v1, Landroid/graphics/Rect;->left:I

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v4

    add-int/2addr v3, v4

    iget v4, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    sub-int/2addr v3, v4

    add-int/lit8 v3, v3, -0x4

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    iget v3, v1, Landroid/graphics/Rect;->bottom:I

    iget v4, p0, Lcom/google/android/youtube/app/ui/v;->l:I

    add-int/2addr v3, v4

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    if-le v3, v0, :cond_0

    iget v0, v1, Landroid/graphics/Rect;->top:I

    iget v1, p0, Lcom/google/android/youtube/app/ui/v;->l:I

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x14

    iput v0, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/v;->e:Landroid/view/View;

    const v1, 0x7f02004d

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/v;->f:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/v;->a:Landroid/app/Activity;

    iget v1, p0, Lcom/google/android/youtube/app/ui/v;->b:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/v;->m:Landroid/view/View;

    return-void

    :cond_0
    iget v0, v1, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v0, v0, 0x4

    iput v0, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/v;->e:Landroid/view/View;

    const v1, 0x7f02004e

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/v;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/v;->o:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/ui/v;)Lcom/google/android/youtube/app/ui/ab;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/v;->n:Lcom/google/android/youtube/app/ui/ab;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/ui/v;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/v;->m:Landroid/view/View;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/ui/v;)Landroid/view/LayoutInflater;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/v;->d:Landroid/view/LayoutInflater;

    return-object v0
.end method


# virtual methods
.method public final a(II)I
    .locals 4

    const/4 v1, 0x0

    if-lez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/v;->c:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/v;->g:Lcom/google/android/youtube/app/ui/y;

    invoke-virtual {v2, v0, v1, p2}, Lcom/google/android/youtube/app/ui/y;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/v;->e:Landroid/view/View;

    iget v2, p0, Lcom/google/android/youtube/app/ui/v;->j:I

    iget v3, p0, Lcom/google/android/youtube/app/ui/v;->k:I

    invoke-virtual {v1, v2, v3}, Landroid/view/View;->measure(II)V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/v;->e:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/app/ui/v;->l:I

    return v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public final a()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/v;->m:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/v;->f:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/v;->h:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/ui/ab;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/v;->n:Lcom/google/android/youtube/app/ui/ab;

    return-void
.end method

.method public final a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/app/ui/v;->o:Z

    return-void
.end method

.method public final b()Landroid/app/Dialog;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/v;->f:Landroid/app/Dialog;

    return-object v0
.end method
