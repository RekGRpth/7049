.class public final Lcom/google/android/youtube/athome/app/common/h;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Landroid/support/place/connector/Broker;

.field private b:Landroid/support/place/rpc/EndpointInfo;

.field private c:Lcom/google/android/youtube/athome/app/common/m;


# direct methods
.method public constructor <init>(Landroid/support/place/connector/Broker;Landroid/support/place/rpc/EndpointInfo;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/athome/app/common/h;->a:Landroid/support/place/connector/Broker;

    iput-object p2, p0, Lcom/google/android/youtube/athome/app/common/h;->b:Landroid/support/place/rpc/EndpointInfo;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/athome/app/common/h;->c:Lcom/google/android/youtube/athome/app/common/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/athome/app/common/h;->c:Lcom/google/android/youtube/athome/app/common/m;

    invoke-virtual {v0}, Lcom/google/android/youtube/athome/app/common/m;->stopListening()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/athome/app/common/h;->c:Lcom/google/android/youtube/athome/app/common/m;

    :cond_0
    return-void
.end method

.method public final a(ILandroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v0, "time"

    invoke-virtual {v3, v0, p1}, Landroid/support/place/rpc/RpcData;->putInteger(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/google/android/youtube/athome/app/common/h;->a:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Lcom/google/android/youtube/athome/app/common/h;->b:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "seekTo"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    const/4 v4, 0x0

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public final a(Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    iget-object v0, p0, Lcom/google/android/youtube/athome/app/common/h;->a:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Lcom/google/android/youtube/athome/app/common/h;->b:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "play"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    const/4 v4, 0x0

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/athome/app/common/AtHomeSubtitleTrack;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v0, "subtitleTrack"

    invoke-virtual {v3, v0, p1}, Landroid/support/place/rpc/RpcData;->putFlattenable(Ljava/lang/String;Landroid/support/place/rpc/Flattenable;)V

    iget-object v0, p0, Lcom/google/android/youtube/athome/app/common/h;->a:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Lcom/google/android/youtube/athome/app/common/h;->b:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "setSubtitleTrack"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    const/4 v4, 0x0

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/athome/app/common/PlayRequest;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v0, "playRequest"

    invoke-virtual {v3, v0, p1}, Landroid/support/place/rpc/RpcData;->putFlattenable(Ljava/lang/String;Landroid/support/place/rpc/Flattenable;)V

    iget-object v0, p0, Lcom/google/android/youtube/athome/app/common/h;->a:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Lcom/google/android/youtube/athome/app/common/h;->b:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "requestPlay"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    const/4 v4, 0x0

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/athome/app/common/i;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/youtube/athome/app/common/h;->a()V

    new-instance v0, Lcom/google/android/youtube/athome/app/common/m;

    iget-object v1, p0, Lcom/google/android/youtube/athome/app/common/h;->a:Landroid/support/place/connector/Broker;

    invoke-direct {v0, p0, v1, p1}, Lcom/google/android/youtube/athome/app/common/m;-><init>(Lcom/google/android/youtube/athome/app/common/h;Landroid/support/place/connector/Broker;Lcom/google/android/youtube/athome/app/common/i;)V

    iput-object v0, p0, Lcom/google/android/youtube/athome/app/common/h;->c:Lcom/google/android/youtube/athome/app/common/m;

    iget-object v0, p0, Lcom/google/android/youtube/athome/app/common/h;->c:Lcom/google/android/youtube/athome/app/common/m;

    iget-object v1, p0, Lcom/google/android/youtube/athome/app/common/h;->b:Landroid/support/place/rpc/EndpointInfo;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/athome/app/common/m;->startListening(Landroid/support/place/rpc/EndpointInfo;)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/athome/app/common/j;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    iget-object v0, p0, Lcom/google/android/youtube/athome/app/common/h;->a:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Lcom/google/android/youtube/athome/app/common/h;->b:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "getPlayerState"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    new-instance v4, Lcom/google/android/youtube/athome/app/common/n;

    const/4 v5, 0x7

    invoke-direct {v4, p0, v5, p1}, Lcom/google/android/youtube/athome/app/common/n;-><init>(Lcom/google/android/youtube/athome/app/common/h;ILjava/lang/Object;)V

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/athome/app/common/k;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    iget-object v0, p0, Lcom/google/android/youtube/athome/app/common/h;->a:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Lcom/google/android/youtube/athome/app/common/h;->b:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "getScreenName"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    new-instance v4, Lcom/google/android/youtube/athome/app/common/n;

    const/16 v5, 0xd

    invoke-direct {v4, p0, v5, p1}, Lcom/google/android/youtube/athome/app/common/n;-><init>(Lcom/google/android/youtube/athome/app/common/h;ILjava/lang/Object;)V

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/athome/app/common/l;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    iget-object v0, p0, Lcom/google/android/youtube/athome/app/common/h;->a:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Lcom/google/android/youtube/athome/app/common/h;->b:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "getVolume"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    new-instance v4, Lcom/google/android/youtube/athome/app/common/n;

    const/16 v5, 0xa

    invoke-direct {v4, p0, v5, p1}, Lcom/google/android/youtube/athome/app/common/n;-><init>(Lcom/google/android/youtube/athome/app/common/h;ILjava/lang/Object;)V

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v0, "videoId"

    invoke-virtual {v3, v0, p1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/athome/app/common/h;->a:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Lcom/google/android/youtube/athome/app/common/h;->b:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "playYouTubeVideo"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    const/4 v4, 0x0

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public final b(ILandroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v0, "volume"

    invoke-virtual {v3, v0, p1}, Landroid/support/place/rpc/RpcData;->putInteger(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/google/android/youtube/athome/app/common/h;->a:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Lcom/google/android/youtube/athome/app/common/h;->b:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "setVolume"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    const/4 v4, 0x0

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public final b(Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    iget-object v0, p0, Lcom/google/android/youtube/athome/app/common/h;->a:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Lcom/google/android/youtube/athome/app/common/h;->b:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "pause"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    const/4 v4, 0x0

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public final c(Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    iget-object v0, p0, Lcom/google/android/youtube/athome/app/common/h;->a:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Lcom/google/android/youtube/athome/app/common/h;->b:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "stop"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    const/4 v4, 0x0

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method
