.class public abstract Lcom/google/android/youtube/athome/server/BinderActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/youtube/athome/server/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onPause()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/athome/server/BinderActivity;->a:Lcom/google/android/youtube/athome/server/a;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "should called setHandler in onCreate()"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/athome/server/BinderActivity;->a:Lcom/google/android/youtube/athome/server/a;

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    iget-object v0, p0, Lcom/google/android/youtube/athome/server/BinderActivity;->a:Lcom/google/android/youtube/athome/server/a;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "should called setHandler in onCreate()"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/athome/server/BinderActivity;->a:Lcom/google/android/youtube/athome/server/a;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
