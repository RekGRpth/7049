.class public abstract Lcom/google/android/youtube/core/BaseApplication;
.super Landroid/app/Application;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/a;
.implements Lcom/google/android/youtube/core/async/bn;
.implements Lcom/google/android/youtube/core/async/f;
.implements Lcom/google/android/youtube/core/f;
.implements Lcom/google/android/youtube/core/player/am;
.implements Lcom/google/android/youtube/core/utils/q;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:I

.field private d:Lcom/google/android/youtube/core/utils/ag;

.field private e:Landroid/os/Handler;

.field private f:Ljava/util/concurrent/Executor;

.field private g:Ljava/util/concurrent/Executor;

.field private h:Ljava/util/concurrent/Executor;

.field private i:Lorg/apache/http/client/HttpClient;

.field private j:Lorg/apache/http/client/HttpClient;

.field private k:Lcom/google/android/youtube/core/converter/m;

.field private l:Landroid/content/SharedPreferences;

.field private m:Lcom/google/android/youtube/core/player/ak;

.field private n:Lcom/google/android/youtube/core/player/c;

.field private o:Lcom/google/android/youtube/core/utils/p;

.field private p:Lcom/google/android/youtube/core/e;

.field private q:Lcom/google/android/youtube/core/async/a;

.field private r:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private s:Lcom/google/android/youtube/core/Analytics;

.field private t:Lcom/google/android/youtube/core/utils/SafeSearch;

.field private u:Z

.field private v:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/core/BaseApplication;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->e:Landroid/os/Handler;

    return-object v0
.end method

.method private static a(III)Ljava/util/concurrent/Executor;
    .locals 8

    new-instance v0, Ljava/util/concurrent/ThreadPoolExecutor;

    const-wide/16 v3, 0x3c

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v6, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v6}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    new-instance v7, Lcom/google/android/youtube/core/utils/x;

    const/16 v1, 0xa

    invoke-direct {v7, v1}, Lcom/google/android/youtube/core/utils/x;-><init>(I)V

    move v1, p0

    move v2, p0

    invoke-direct/range {v0 .. v7}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    return-object v0
.end method


# virtual methods
.method public final L()Lcom/google/android/youtube/core/utils/ag;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->d:Lcom/google/android/youtube/core/utils/ag;

    return-object v0
.end method

.method public final M()Ljava/util/concurrent/Executor;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->f:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method public final N()Ljava/util/concurrent/Executor;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->g:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method public final O()Ljava/util/concurrent/Executor;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->h:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method public final P()Lorg/apache/http/client/HttpClient;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->i:Lorg/apache/http/client/HttpClient;

    return-object v0
.end method

.method public final Q()Lorg/apache/http/client/HttpClient;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->j:Lorg/apache/http/client/HttpClient;

    return-object v0
.end method

.method public final R()Lcom/google/android/youtube/core/converter/m;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->k:Lcom/google/android/youtube/core/converter/m;

    return-object v0
.end method

.method public final S()Landroid/content/SharedPreferences;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->l:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method public final T()Lcom/google/android/youtube/core/async/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->q:Lcom/google/android/youtube/core/async/a;

    return-object v0
.end method

.method public final U()Lcom/google/android/youtube/core/async/UserAuthorizer;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->r:Lcom/google/android/youtube/core/async/UserAuthorizer;

    return-object v0
.end method

.method public final V()Lcom/google/android/youtube/core/utils/SafeSearch;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->t:Lcom/google/android/youtube/core/utils/SafeSearch;

    return-object v0
.end method

.method public final W()Lcom/google/android/youtube/core/player/c;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->n:Lcom/google/android/youtube/core/player/c;

    return-object v0
.end method

.method public final X()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/youtube/core/BaseApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/BaseApplication;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->a:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->a:Ljava/lang/String;

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "could not retrieve application version name"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    const-string v0, "Unknown"

    iput-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public final Y()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/youtube/core/BaseApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/BaseApplication;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->b:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->b:Ljava/lang/String;

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "could not retrieve application version code"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    const-string v0, "Unknown"

    iput-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public final Z()I
    .locals 4

    const/4 v0, 0x0

    iget v1, p0, Lcom/google/android/youtube/core/BaseApplication;->c:I

    if-nez v1, :cond_0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/youtube/core/BaseApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/BaseApplication;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    iput v0, p0, Lcom/google/android/youtube/core/BaseApplication;->c:I

    :cond_0
    iget v0, p0, Lcom/google/android/youtube/core/BaseApplication;->c:I

    :goto_0
    return v0

    :catch_0
    move-exception v1

    const-string v2, "could not retrieve application version number"

    invoke-static {v2, v1}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    iput v0, p0, Lcom/google/android/youtube/core/BaseApplication;->c:I

    goto :goto_0
.end method

.method public final aa()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/BaseApplication;->u:Z

    return v0
.end method

.method protected b()V
    .locals 10

    const/16 v5, 0x3c

    const/16 v4, 0xa

    const/4 v9, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/BaseApplication;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;)V

    const-string v0, "youtube"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/BaseApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->l:Landroid/content/SharedPreferences;

    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->l:Landroid/content/SharedPreferences;

    const-string v2, "version"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/BaseApplication;->X()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v9

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/youtube/core/BaseApplication;->u:Z

    iget-boolean v0, p0, Lcom/google/android/youtube/core/BaseApplication;->u:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->l:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v3, "version"

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "device_id"

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "device_key"

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/SharedPreferences$Editor;)V

    :cond_0
    new-instance v0, Lcom/google/android/youtube/core/utils/ag;

    invoke-direct {v0}, Lcom/google/android/youtube/core/utils/ag;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->d:Lcom/google/android/youtube/core/utils/ag;

    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/BaseApplication;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->e:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/youtube/core/b;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/core/b;-><init>(Lcom/google/android/youtube/core/BaseApplication;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->f:Ljava/util/concurrent/Executor;

    const/16 v0, 0x10

    invoke-static {v0, v5, v4}, Lcom/google/android/youtube/core/BaseApplication;->a(III)Ljava/util/concurrent/Executor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->g:Ljava/util/concurrent/Executor;

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v9, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v0, v5, v4}, Lcom/google/android/youtube/core/BaseApplication;->a(III)Ljava/util/concurrent/Executor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->h:Ljava/util/concurrent/Executor;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/BaseApplication;->X()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/BaseApplication;->c()Lcom/google/android/youtube/core/c;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/youtube/core/c;->z()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v0, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/ao;->a(Ljava/lang/String;)Lorg/apache/http/client/HttpClient;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/core/BaseApplication;->i:Lorg/apache/http/client/HttpClient;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/ao;->b(Ljava/lang/String;)Lorg/apache/http/client/HttpClient;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->j:Lorg/apache/http/client/HttpClient;

    invoke-static {}, Lcom/google/android/youtube/core/converter/m;->a()Lcom/google/android/youtube/core/converter/m;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->k:Lcom/google/android/youtube/core/converter/m;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/BaseApplication;->d()Lcom/google/android/youtube/core/async/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->q:Lcom/google/android/youtube/core/async/a;

    new-instance v2, Lcom/google/android/youtube/core/async/UserAuthorizer;

    iget-object v3, p0, Lcom/google/android/youtube/core/BaseApplication;->q:Lcom/google/android/youtube/core/async/a;

    iget-object v4, p0, Lcom/google/android/youtube/core/BaseApplication;->l:Landroid/content/SharedPreferences;

    sget v0, Lcom/google/android/youtube/core/utils/Util;->a:I

    const/16 v5, 0xe

    if-lt v0, v5, :cond_1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/BaseApplication;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    const/16 v5, 0xb

    if-ge v0, v5, :cond_3

    :cond_1
    new-instance v0, Lcom/google/android/youtube/core/async/ak;

    invoke-direct {v0}, Lcom/google/android/youtube/core/async/ak;-><init>()V

    :goto_1
    invoke-direct {v2, v3, v4, v0, v1}, Lcom/google/android/youtube/core/async/UserAuthorizer;-><init>(Lcom/google/android/youtube/core/async/a;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/async/bo;Z)V

    iput-object v2, p0, Lcom/google/android/youtube/core/BaseApplication;->r:Lcom/google/android/youtube/core/async/UserAuthorizer;

    new-instance v0, Lcom/google/android/youtube/core/utils/SafeSearch;

    iget-object v1, p0, Lcom/google/android/youtube/core/BaseApplication;->l:Landroid/content/SharedPreferences;

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/core/utils/SafeSearch;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->t:Lcom/google/android/youtube/core/utils/SafeSearch;

    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->g:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lcom/google/android/youtube/core/BaseApplication;->t:Lcom/google/android/youtube/core/utils/SafeSearch;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/BaseApplication;->c()Lcom/google/android/youtube/core/c;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/youtube/core/c;->u()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Lcom/google/android/youtube/core/c;->h()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Lcom/google/android/youtube/core/c;->z()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/BaseApplication;->X()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v0, ""

    :goto_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v0, Lcom/google/android/youtube/core/client/s;

    iget-object v2, p0, Lcom/google/android/youtube/core/BaseApplication;->g:Ljava/util/concurrent/Executor;

    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1}, Lcom/google/android/youtube/core/c;->h()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1}, Lcom/google/android/youtube/core/c;->v()I

    move-result v6

    invoke-interface {v1}, Lcom/google/android/youtube/core/c;->w()I

    move-result v7

    invoke-interface {v1}, Lcom/google/android/youtube/core/c;->x()Ljava/lang/String;

    move-result-object v8

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/youtube/core/client/s;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    move-object v1, v0

    :goto_3
    new-instance v2, Lcom/google/android/youtube/core/utils/h;

    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/BaseApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/BaseApplication;->c()Lcom/google/android/youtube/core/c;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Lcom/google/android/youtube/core/utils/h;-><init>(Landroid/net/ConnectivityManager;Lcom/google/android/youtube/core/c;)V

    iput-object v2, p0, Lcom/google/android/youtube/core/BaseApplication;->o:Lcom/google/android/youtube/core/utils/p;

    new-instance v0, Lcom/google/android/youtube/core/d;

    iget-object v2, p0, Lcom/google/android/youtube/core/BaseApplication;->o:Lcom/google/android/youtube/core/utils/p;

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/d;-><init>(Lcom/google/android/youtube/core/client/e;Lcom/google/android/youtube/core/utils/p;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->s:Lcom/google/android/youtube/core/Analytics;

    new-instance v0, Lcom/google/android/youtube/core/e;

    iget-object v1, p0, Lcom/google/android/youtube/core/BaseApplication;->o:Lcom/google/android/youtube/core/utils/p;

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/core/e;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/utils/p;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->p:Lcom/google/android/youtube/core/e;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/BaseApplication;->k()Lcom/google/android/youtube/core/player/ak;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->m:Lcom/google/android/youtube/core/player/ak;

    new-instance v0, Lcom/google/android/youtube/core/player/c;

    iget-object v1, p0, Lcom/google/android/youtube/core/BaseApplication;->d:Lcom/google/android/youtube/core/utils/ag;

    invoke-direct {v0, v1, v9}, Lcom/google/android/youtube/core/player/c;-><init>(Lcom/google/android/youtube/core/utils/e;Z)V

    iput-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->n:Lcom/google/android/youtube/core/player/c;

    return-void

    :cond_2
    move v0, v1

    goto/16 :goto_0

    :cond_3
    new-instance v0, Lcom/google/android/youtube/core/v14/a;

    invoke-direct {v0}, Lcom/google/android/youtube/core/v14/a;-><init>()V

    goto/16 :goto_1

    :cond_4
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, " ["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "]"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_5
    new-instance v0, Lcom/google/android/youtube/core/client/az;

    invoke-direct {v0}, Lcom/google/android/youtube/core/client/az;-><init>()V

    move-object v1, v0

    goto :goto_3
.end method

.method public abstract c()Lcom/google/android/youtube/core/c;
.end method

.method protected d()Lcom/google/android/youtube/core/async/a;
    .locals 2

    new-instance v0, Lcom/google/android/youtube/core/async/t;

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/async/t;-><init>(Landroid/accounts/AccountManager;)V

    return-object v0
.end method

.method protected d_()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final h()Lcom/google/android/youtube/core/player/ak;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->m:Lcom/google/android/youtube/core/player/ak;

    return-object v0
.end method

.method public final i()Lcom/google/android/youtube/core/Analytics;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->s:Lcom/google/android/youtube/core/Analytics;

    return-object v0
.end method

.method public final j()Lcom/google/android/youtube/core/e;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->p:Lcom/google/android/youtube/core/e;

    return-object v0
.end method

.method protected k()Lcom/google/android/youtube/core/player/ak;
    .locals 9

    const/4 v4, 0x1

    const/4 v5, 0x0

    new-instance v0, Lcom/google/android/youtube/core/player/k;

    iget-object v1, p0, Lcom/google/android/youtube/core/BaseApplication;->o:Lcom/google/android/youtube/core/utils/p;

    invoke-static {p0}, Lcom/google/android/youtube/core/utils/j;->c(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/BaseApplication;->c()Lcom/google/android/youtube/core/c;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/youtube/core/c;->C()Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v4

    :goto_0
    invoke-static {p0}, Lcom/google/android/youtube/core/utils/n;->a(Landroid/content/Context;)Z

    move-result v3

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "youtube:device_lowend"

    const/4 v8, 0x2

    invoke-static {v6, v7, v8}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    packed-switch v6, :pswitch_data_0

    invoke-static {p0}, Lcom/google/android/youtube/core/utils/Util;->e(Landroid/content/Context;)Z

    move-result v4

    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/youtube/core/BaseApplication;->c()Lcom/google/android/youtube/core/c;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/youtube/core/c;->A()Z

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/core/player/k;-><init>(Lcom/google/android/youtube/core/utils/p;ZZZZ)V

    return-object v0

    :cond_0
    move v2, v5

    goto :goto_0

    :pswitch_1
    move v4, v5

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final l()Lcom/google/android/youtube/core/utils/p;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/BaseApplication;->o:Lcom/google/android/youtube/core/utils/p;

    return-object v0
.end method

.method public m()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public o()Ljava/lang/String;
    .locals 1

    const-string v0, "?"

    return-object v0
.end method

.method public final onCreate()V
    .locals 1

    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/BaseApplication;->d_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/core/BaseApplication;->v:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/BaseApplication;->v:Z

    invoke-virtual {p0}, Lcom/google/android/youtube/core/BaseApplication;->b()V

    :cond_0
    return-void
.end method
