.class public Lcom/google/android/youtube/core/L;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final synthetic a:Z

.field private static b:Ljava/lang/String;

.field private static final c:I

.field private static final d:Ljava/lang/String;

.field private static e:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/youtube/core/L;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/youtube/core/L;->a:Z

    const-string v0, "L"

    sput-object v0, Lcom/google/android/youtube/core/L;->b:Ljava/lang/String;

    const/16 v0, 0x13

    sput v0, Lcom/google/android/youtube/core/L;->c:I

    const-class v0, Lcom/google/android/youtube/core/L;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/core/L;->d:Ljava/lang/String;

    const-string v0, ""

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/core/L;->e:Ljava/util/regex/Pattern;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()V
    .locals 0

    return-void
.end method

.method private static a(Lcom/google/android/youtube/core/L$Type;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    sget-object v0, Lcom/google/android/youtube/core/h;->a:[I

    invoke-virtual {p0}, Lcom/google/android/youtube/core/L$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    sget-boolean v0, Lcom/google/android/youtube/core/L;->a:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Unknown type."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :pswitch_0
    sget-object v0, Lcom/google/android/youtube/core/L;->b:Ljava/lang/String;

    invoke-static {v0, p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    :goto_0
    :pswitch_1
    return-void

    :pswitch_2
    sget-object v0, Lcom/google/android/youtube/core/L;->b:Ljava/lang/String;

    invoke-static {v0, p1, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :pswitch_3
    sget-object v0, Lcom/google/android/youtube/core/L;->b:Ljava/lang/String;

    invoke-static {v0, p1, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :pswitch_4
    sget-object v0, Lcom/google/android/youtube/core/L;->b:Ljava/lang/String;

    invoke-static {v0, p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Ljava/lang/String;)V
    .locals 0

    sput-object p0, Lcom/google/android/youtube/core/L;->b:Ljava/lang/String;

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    sget-object v0, Lcom/google/android/youtube/core/L$Type;->ERROR:Lcom/google/android/youtube/core/L$Type;

    invoke-static {v0, p0, p1}, Lcom/google/android/youtube/core/L;->a(Lcom/google/android/youtube/core/L$Type;Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public static b()V
    .locals 0

    return-void
.end method

.method public static b(Ljava/lang/String;)V
    .locals 2

    sget-object v0, Lcom/google/android/youtube/core/L$Type;->ERROR:Lcom/google/android/youtube/core/L$Type;

    const/4 v1, 0x0

    invoke-static {v0, p0, v1}, Lcom/google/android/youtube/core/L;->a(Lcom/google/android/youtube/core/L$Type;Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public static b(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    sget-object v0, Lcom/google/android/youtube/core/L$Type;->WARNING:Lcom/google/android/youtube/core/L$Type;

    invoke-static {v0, p0, p1}, Lcom/google/android/youtube/core/L;->a(Lcom/google/android/youtube/core/L$Type;Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public static c()V
    .locals 0

    return-void
.end method

.method public static c(Ljava/lang/String;)V
    .locals 2

    sget-object v0, Lcom/google/android/youtube/core/L$Type;->WARNING:Lcom/google/android/youtube/core/L$Type;

    const/4 v1, 0x0

    invoke-static {v0, p0, v1}, Lcom/google/android/youtube/core/L;->a(Lcom/google/android/youtube/core/L$Type;Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public static d()V
    .locals 0

    return-void
.end method

.method public static d(Ljava/lang/String;)V
    .locals 1

    invoke-static {p0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/core/L;->e:Ljava/util/regex/Pattern;

    return-void
.end method

.method public static e()V
    .locals 0

    return-void
.end method
