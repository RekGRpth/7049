.class public abstract Lcom/google/android/youtube/core/a/b;
.super Lcom/google/android/youtube/core/a/h;
.source "SourceFile"


# instance fields
.field private final c:Lcom/google/android/youtube/core/a/g;

.field private final d:I


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/a/e;Lcom/google/android/youtube/core/a/g;I)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/a/h;-><init>(Lcom/google/android/youtube/core/a/e;)V

    const-string v0, "viewType cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/a/g;

    iput-object v0, p0, Lcom/google/android/youtube/core/a/b;->c:Lcom/google/android/youtube/core/a/g;

    if-lez p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "groupSize must be positive"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    iput p3, p0, Lcom/google/android/youtube/core/a/b;->d:I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/a/b;->a:Lcom/google/android/youtube/core/a/e;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/e;->n()I

    move-result v0

    iget v1, p0, Lcom/google/android/youtube/core/a/b;->d:I

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lcom/google/android/youtube/core/a/b;->d:I

    div-int/2addr v0, v1

    return v0
.end method

.method public final a(I)Lcom/google/android/youtube/core/a/g;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/a/b;->c:Lcom/google/android/youtube/core/a/g;

    return-object v0
.end method

.method protected final a(Ljava/util/Set;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/a/b;->c:Lcom/google/android/youtube/core/a/g;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final b()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/core/a/b;->d:I

    return v0
.end method

.method public final c(I)J
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/a/b;->a:Lcom/google/android/youtube/core/a/e;

    iget v1, p0, Lcom/google/android/youtube/core/a/b;->d:I

    mul-int/2addr v1, p1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/a/e;->c(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public final d(I)Z
    .locals 3

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/a/b;->f(I)I

    move-result v1

    iget v0, p0, Lcom/google/android/youtube/core/a/b;->d:I

    mul-int/2addr v0, p1

    :goto_0
    if-ge v0, v1, :cond_1

    iget-object v2, p0, Lcom/google/android/youtube/core/a/b;->a:Lcom/google/android/youtube/core/a/e;

    invoke-virtual {v2, v0}, Lcom/google/android/youtube/core/a/e;->d(I)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected final e(I)I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/core/a/b;->d:I

    mul-int/2addr v0, p1

    return v0
.end method

.method protected final f(I)I
    .locals 2

    add-int/lit8 v0, p1, 0x1

    iget v1, p0, Lcom/google/android/youtube/core/a/b;->d:I

    mul-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/youtube/core/a/b;->a:Lcom/google/android/youtube/core/a/e;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/a/e;->n()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method public final s_()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/a/b;->a:Lcom/google/android/youtube/core/a/e;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/e;->s_()Z

    move-result v0

    return v0
.end method
