.class public final Lcom/google/android/youtube/core/a/c;
.super Lcom/google/android/youtube/core/a/e;
.source "SourceFile"


# instance fields
.field private final a:Landroid/widget/ListAdapter;

.field private final c:[Lcom/google/android/youtube/core/a/g;

.field private final d:Landroid/database/DataSetObserver;


# direct methods
.method public varargs constructor <init>(Landroid/widget/ListAdapter;Z[Lcom/google/android/youtube/core/a/g;)V
    .locals 4

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/youtube/core/a/e;-><init>()V

    const-string v0, "adapter cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListAdapter;

    iput-object v0, p0, Lcom/google/android/youtube/core/a/c;->a:Landroid/widget/ListAdapter;

    if-eqz p3, :cond_0

    array-length v0, p3

    if-nez v0, :cond_1

    :cond_0
    invoke-interface {p1}, Landroid/widget/ListAdapter;->getViewTypeCount()I

    move-result v0

    new-array p3, v0, [Lcom/google/android/youtube/core/a/g;

    move v0, v1

    :goto_0
    array-length v1, p3

    if-ge v0, v1, :cond_2

    new-instance v1, Lcom/google/android/youtube/core/a/g;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Auto "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/youtube/core/a/g;-><init>(Ljava/lang/String;)V

    aput-object v1, p3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    array-length v0, p3

    invoke-interface {p1}, Landroid/widget/ListAdapter;->getViewTypeCount()I

    move-result v3

    if-ne v0, v3, :cond_3

    move v0, v2

    :goto_1
    const-string v3, "viewTypes array size must match adapter\'s view type count"

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    invoke-static {p3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    :goto_2
    const-string v0, "viewTypes must not contain null"

    invoke-static {v2, v0}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    :cond_2
    iput-object p3, p0, Lcom/google/android/youtube/core/a/c;->c:[Lcom/google/android/youtube/core/a/g;

    new-instance v0, Lcom/google/android/youtube/core/a/d;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/core/a/d;-><init>(Lcom/google/android/youtube/core/a/c;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/a/c;->d:Landroid/database/DataSetObserver;

    iget-object v0, p0, Lcom/google/android/youtube/core/a/c;->a:Landroid/widget/ListAdapter;

    iget-object v1, p0, Lcom/google/android/youtube/core/a/c;->d:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    return-void

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    move v2, v1

    goto :goto_2
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/a/c;->a:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    return v0
.end method

.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/a/c;->a:Landroid/widget/ListAdapter;

    invoke-interface {v0, p1, p2, p3}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Lcom/google/android/youtube/core/a/g;
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/a/c;->a:Landroid/widget/ListAdapter;

    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v0

    if-gez v0, :cond_0

    sget-object v0, Lcom/google/android/youtube/core/a/c;->b:Lcom/google/android/youtube/core/a/g;

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/core/a/c;->c:[Lcom/google/android/youtube/core/a/g;

    aget-object v0, v1, v0

    goto :goto_0
.end method

.method protected final a(Ljava/util/Set;)V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/core/a/c;->c:[Lcom/google/android/youtube/core/a/g;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/a/c;->c:[Lcom/google/android/youtube/core/a/g;

    aget-object v1, v1, v0

    invoke-interface {p1, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final b()Landroid/widget/ListAdapter;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/a/c;->a:Landroid/widget/ListAdapter;

    return-object v0
.end method

.method public final b(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/a/c;->a:Landroid/widget/ListAdapter;

    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final c(I)J
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/a/c;->a:Landroid/widget/ListAdapter;

    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v0

    return-wide v0
.end method

.method protected final d()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/a/c;->k()V

    return-void
.end method

.method public final d(I)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/a/c;->a:Landroid/widget/ListAdapter;

    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v0

    return v0
.end method

.method protected final e()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/a/c;->k()V

    return-void
.end method

.method public final s_()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/a/c;->a:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v0

    return v0
.end method
