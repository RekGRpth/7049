.class public Lcom/google/android/youtube/core/async/FroyoSignInActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# instance fields
.field private a:Landroid/app/AlertDialog;

.field private b:Lcom/google/android/youtube/core/async/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/accounts/Account;[Landroid/accounts/Account;)Landroid/content/Intent;
    .locals 5

    array-length v0, p2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v1, -0x1

    const/4 v0, 0x0

    :goto_0
    array-length v3, p2

    if-ge v0, v3, :cond_1

    aget-object v3, p2, v0

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v3, v2, v0

    if-eqz p1, :cond_0

    aget-object v3, v2, v0

    iget-object v4, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v1, v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/google/android/youtube/core/async/FroyoSignInActivity;

    invoke-direct {v0, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "accounts"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "selected_account_index"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x10000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private a()V
    .locals 1

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/async/FroyoSignInActivity;->setResult(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/async/FroyoSignInActivity;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/core/async/FroyoSignInActivity;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/async/FroyoSignInActivity;->setResult(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/async/FroyoSignInActivity;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/core/async/FroyoSignInActivity;Ljava/lang/Exception;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "exception"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/youtube/core/async/FroyoSignInActivity;->setResult(ILandroid/content/Intent;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/async/FroyoSignInActivity;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/core/async/FroyoSignInActivity;Ljava/lang/String;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "authAccount"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/youtube/core/async/FroyoSignInActivity;->setResult(ILandroid/content/Intent;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/async/FroyoSignInActivity;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    return-void
.end method

.method static synthetic b(Lcom/google/android/youtube/core/async/FroyoSignInActivity;Ljava/lang/String;)Landroid/accounts/Account;
    .locals 5

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/async/FroyoSignInActivity;->b:Lcom/google/android/youtube/core/async/a;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/a;->b()[Landroid/accounts/Account;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static synthetic b(Lcom/google/android/youtube/core/async/FroyoSignInActivity;)V
    .locals 2

    invoke-static {}, Landroid/app/ActivityManager;->isUserAMonkey()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/core/async/FroyoSignInActivity;->a()V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/core/async/FroyoSignInActivity;->b:Lcom/google/android/youtube/core/async/a;

    new-instance v1, Lcom/google/android/youtube/core/async/ai;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/core/async/ai;-><init>(Lcom/google/android/youtube/core/async/FroyoSignInActivity;)V

    invoke-virtual {v0, p0, v1}, Lcom/google/android/youtube/core/async/a;->a(Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/youtube/core/async/FroyoSignInActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/core/async/FroyoSignInActivity;->a()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/async/FroyoSignInActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "accounts"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const-string v2, "selected_account_index"

    const/4 v3, -0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/youtube/core/async/FroyoSignInActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/f;

    invoke-interface {v0}, Lcom/google/android/youtube/core/async/f;->T()Lcom/google/android/youtube/core/async/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/async/FroyoSignInActivity;->b:Lcom/google/android/youtube/core/async/a;

    new-instance v0, Lcom/google/android/youtube/core/async/aj;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/youtube/core/async/aj;-><init>(Landroid/content/Context;[Ljava/lang/String;I)V

    new-instance v1, Lcom/google/android/youtube/core/ui/w;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/core/ui/w;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0b003a

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/core/ui/w;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/core/async/af;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/core/async/af;-><init>(Lcom/google/android/youtube/core/async/FroyoSignInActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/core/async/ae;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/core/async/ae;-><init>(Lcom/google/android/youtube/core/async/FroyoSignInActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/async/FroyoSignInActivity;->a:Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/google/android/youtube/core/async/FroyoSignInActivity;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/core/async/ag;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/core/async/ag;-><init>(Lcom/google/android/youtube/core/async/FroyoSignInActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/async/FroyoSignInActivity;->a:Landroid/app/AlertDialog;

    new-instance v1, Lcom/google/android/youtube/core/async/ah;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/core/async/ah;-><init>(Lcom/google/android/youtube/core/async/FroyoSignInActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/async/FroyoSignInActivity;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method
