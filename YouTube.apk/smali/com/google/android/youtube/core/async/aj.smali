.class final Lcom/google/android/youtube/core/async/aj;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field private final a:Landroid/view/LayoutInflater;

.field private final b:I

.field private final c:[Ljava/lang/String;

.field private final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;[Ljava/lang/String;I)V
    .locals 1

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/async/aj;->a:Landroid/view/LayoutInflater;

    const v0, 0x7f0b003b

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/async/aj;->d:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/youtube/core/async/aj;->c:[Ljava/lang/String;

    iput p3, p0, Lcom/google/android/youtube/core/async/aj;->b:I

    return-void
.end method


# virtual methods
.method public final getCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/async/aj;->c:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/async/aj;->c:[Ljava/lang/String;

    array-length v0, v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/async/aj;->c:[Ljava/lang/String;

    aget-object v0, v0, p1

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 2

    iget v0, p0, Lcom/google/android/youtube/core/async/aj;->b:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/async/aj;->c:[Ljava/lang/String;

    array-length v0, v0

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    const/4 v1, 0x0

    iget v0, p0, Lcom/google/android/youtube/core/async/aj;->b:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/async/aj;->c:[Ljava/lang/String;

    array-length v0, v0

    if-ne p1, v0, :cond_2

    :cond_0
    if-eqz p2, :cond_1

    move-object v0, p2

    :goto_0
    check-cast v0, Landroid/widget/TextView;

    :goto_1
    iget-object v1, p0, Lcom/google/android/youtube/core/async/aj;->c:[Ljava/lang/String;

    array-length v1, v1

    if-ge p1, v1, :cond_5

    iget-object v1, p0, Lcom/google/android/youtube/core/async/aj;->c:[Ljava/lang/String;

    aget-object v1, v1, p1

    :goto_2
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/async/aj;->a:Landroid/view/LayoutInflater;

    const v2, 0x1090011

    invoke-virtual {v0, v2, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_2
    if-eqz p2, :cond_4

    move-object v0, p2

    :goto_3
    check-cast v0, Landroid/widget/CheckedTextView;

    iget v2, p0, Lcom/google/android/youtube/core/async/aj;->b:I

    if-ne p1, v2, :cond_3

    const/4 v1, 0x1

    :cond_3
    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/google/android/youtube/core/async/aj;->a:Landroid/view/LayoutInflater;

    const v2, 0x1090012

    invoke-virtual {v0, v2, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_3

    :cond_5
    iget-object v1, p0, Lcom/google/android/youtube/core/async/aj;->d:Ljava/lang/String;

    goto :goto_2
.end method

.method public final getViewTypeCount()I
    .locals 2

    iget v0, p0, Lcom/google/android/youtube/core/async/aj;->b:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public final hasStableIds()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
