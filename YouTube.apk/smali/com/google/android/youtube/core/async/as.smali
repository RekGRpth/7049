.class public final Lcom/google/android/youtube/core/async/as;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/au;


# instance fields
.field private final a:Lcom/google/android/youtube/core/async/au;

.field private final b:Ljava/util/concurrent/atomic/AtomicLong;

.field private final c:Lcom/google/android/youtube/core/utils/e;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/async/au;Ljava/util/concurrent/atomic/AtomicLong;Lcom/google/android/youtube/core/utils/e;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "target may not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/au;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/as;->a:Lcom/google/android/youtube/core/async/au;

    const-string v0, "lastEventTime may not be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicLong;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/as;->b:Ljava/util/concurrent/atomic/AtomicLong;

    const-string v0, "clock may not be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/utils/e;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/as;->c:Lcom/google/android/youtube/core/utils/e;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/core/async/as;)Lcom/google/android/youtube/core/utils/e;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/async/as;->c:Lcom/google/android/youtube/core/utils/e;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/core/async/as;)Ljava/util/concurrent/atomic/AtomicLong;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/async/as;->b:Ljava/util/concurrent/atomic/AtomicLong;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/async/as;->a:Lcom/google/android/youtube/core/async/au;

    new-instance v1, Lcom/google/android/youtube/core/async/at;

    invoke-direct {v1, p0, p2}, Lcom/google/android/youtube/core/async/at;-><init>(Lcom/google/android/youtube/core/async/as;Lcom/google/android/youtube/core/async/n;)V

    invoke-interface {v0, p1, v1}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method
