.class final Lcom/google/android/youtube/core/async/aw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/au;


# instance fields
.field private final a:Lcom/google/android/youtube/core/async/au;

.field private final b:I


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/async/au;I)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "target requester cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/au;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/aw;->a:Lcom/google/android/youtube/core/async/au;

    if-lez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "pageSize must be greater than zero"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    iput p2, p0, Lcom/google/android/youtube/core/async/aw;->b:I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/async/aw;)Lcom/google/android/youtube/core/async/au;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/async/aw;->a:Lcom/google/android/youtube/core/async/au;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V
    .locals 3

    check-cast p1, Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v0, p1, Lcom/google/android/youtube/core/async/GDataRequest;->c:Landroid/net/Uri;

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/youtube/core/async/aw;->b:I

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/core/async/av;->a(Landroid/net/Uri;II)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/async/aw;->a:Lcom/google/android/youtube/core/async/au;

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/core/async/GDataRequest;->b(Landroid/net/Uri;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v0

    new-instance v2, Lcom/google/android/youtube/core/async/ax;

    invoke-direct {v2, p0, p1, p2}, Lcom/google/android/youtube/core/async/ax;-><init>(Lcom/google/android/youtube/core/async/aw;Lcom/google/android/youtube/core/async/GDataRequest;Lcom/google/android/youtube/core/async/n;)V

    invoke-interface {v1, v0, v2}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method
