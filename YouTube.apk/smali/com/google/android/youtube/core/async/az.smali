.class public final Lcom/google/android/youtube/core/async/az;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/au;


# instance fields
.field private final a:Lcom/google/android/youtube/core/async/au;

.field private final b:Lcom/google/android/youtube/core/async/au;

.field private volatile c:Z


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/async/au;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "headRequester can\'t be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/au;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/az;->a:Lcom/google/android/youtube/core/async/au;

    const-string v0, "getRequester can\'t be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/au;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/az;->b:Lcom/google/android/youtube/core/async/au;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/core/async/az;)Lcom/google/android/youtube/core/async/au;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/async/az;->b:Lcom/google/android/youtube/core/async/au;

    return-object v0
.end method

.method public static a(Lorg/apache/http/client/HttpClient;Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/utils/e;)Lcom/google/android/youtube/core/async/au;
    .locals 4

    const-string v0, "httpClient can\'t be null"

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "executor can\'t be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/youtube/core/converter/http/ap;

    invoke-direct {v0}, Lcom/google/android/youtube/core/converter/http/ap;-><init>()V

    new-instance v1, Lcom/google/android/youtube/core/async/ba;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/youtube/core/async/ba;-><init>(B)V

    new-instance v2, Lcom/google/android/youtube/core/async/az;

    new-instance v3, Lcom/google/android/youtube/core/async/an;

    invoke-direct {v3, p0, v0, v0}, Lcom/google/android/youtube/core/async/an;-><init>(Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/b;Lcom/google/android/youtube/core/converter/http/bn;)V

    new-instance v0, Lcom/google/android/youtube/core/async/an;

    invoke-direct {v0, p0, v1, v1}, Lcom/google/android/youtube/core/async/an;-><init>(Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/b;Lcom/google/android/youtube/core/converter/http/bn;)V

    invoke-direct {v2, v3, v0}, Lcom/google/android/youtube/core/async/az;-><init>(Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/async/au;)V

    invoke-static {p1, v2}, Lcom/google/android/youtube/core/async/i;->a(Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/async/au;)Lcom/google/android/youtube/core/async/i;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/core/cache/b;

    const/16 v2, 0x64

    invoke-direct {v1, v2}, Lcom/google/android/youtube/core/cache/b;-><init>(I)V

    const-wide/32 v2, 0x1b7740

    invoke-static {v1, v0, p2, v2, v3}, Lcom/google/android/youtube/core/async/bg;->a(Lcom/google/android/youtube/core/cache/a;Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/utils/e;J)Lcom/google/android/youtube/core/async/bg;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/async/az;Z)Z
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/async/az;->c:Z

    return v0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V
    .locals 3

    check-cast p1, Landroid/net/Uri;

    iget-boolean v0, p0, Lcom/google/android/youtube/core/async/az;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/async/az;->a:Lcom/google/android/youtube/core/async/au;

    new-instance v1, Lcom/google/android/youtube/core/async/bb;

    const/4 v2, 0x1

    invoke-direct {v1, p0, p2, v2}, Lcom/google/android/youtube/core/async/bb;-><init>(Lcom/google/android/youtube/core/async/az;Lcom/google/android/youtube/core/async/n;Z)V

    invoke-interface {v0, p1, v1}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/async/az;->b:Lcom/google/android/youtube/core/async/au;

    new-instance v1, Lcom/google/android/youtube/core/async/bb;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p2, v2}, Lcom/google/android/youtube/core/async/bb;-><init>(Lcom/google/android/youtube/core/async/az;Lcom/google/android/youtube/core/async/n;Z)V

    invoke-interface {v0, p1, v1}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    goto :goto_0
.end method
