.class public abstract Lcom/google/android/youtube/core/async/be;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/n;


# static fields
.field private static final a:Ljava/util/concurrent/LinkedBlockingQueue;


# instance fields
.field private final b:Lcom/google/android/youtube/core/async/n;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    sput-object v0, Lcom/google/android/youtube/core/async/be;->a:Ljava/util/concurrent/LinkedBlockingQueue;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/youtube/core/async/n;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/n;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/be;->b:Lcom/google/android/youtube/core/async/n;

    return-void
.end method

.method private static a()Lcom/google/android/youtube/core/async/bf;
    .locals 2

    sget-object v0, Lcom/google/android/youtube/core/async/be;->a:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/bf;

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/youtube/core/async/bf;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/async/bf;-><init>(B)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/async/bf;)V
    .locals 2

    :try_start_0
    sget-object v0, Lcom/google/android/youtube/core/async/be;->a:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/LinkedBlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Interrupted when releasing runnable to the queue"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2

    invoke-static {}, Lcom/google/android/youtube/core/async/be;->a()Lcom/google/android/youtube/core/async/bf;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/async/be;->b:Lcom/google/android/youtube/core/async/n;

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/android/youtube/core/async/bf;->a(Lcom/google/android/youtube/core/async/n;Ljava/lang/Object;Ljava/lang/Exception;)V

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/async/be;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    invoke-static {}, Lcom/google/android/youtube/core/async/be;->a()Lcom/google/android/youtube/core/async/bf;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/async/be;->b:Lcom/google/android/youtube/core/async/n;

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/android/youtube/core/async/bf;->a(Lcom/google/android/youtube/core/async/n;Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/async/be;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected abstract a(Ljava/lang/Runnable;)V
.end method
