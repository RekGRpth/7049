.class final Lcom/google/android/youtube/core/async/bh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/async/bg;

.field private final b:Lcom/google/android/youtube/core/async/n;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/async/bg;Lcom/google/android/youtube/core/async/n;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/async/bh;->a:Lcom/google/android/youtube/core/async/bg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/youtube/core/async/bh;->b:Lcom/google/android/youtube/core/async/n;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/async/bh;->b:Lcom/google/android/youtube/core/async/n;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/async/n;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 5

    iget-object v0, p0, Lcom/google/android/youtube/core/async/bh;->a:Lcom/google/android/youtube/core/async/bg;

    invoke-static {v0}, Lcom/google/android/youtube/core/async/bg;->b(Lcom/google/android/youtube/core/async/bg;)Lcom/google/android/youtube/core/cache/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/async/bh;->a:Lcom/google/android/youtube/core/async/bg;

    invoke-virtual {v1, p1}, Lcom/google/android/youtube/core/async/bg;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/core/async/Timestamped;

    iget-object v3, p0, Lcom/google/android/youtube/core/async/bh;->a:Lcom/google/android/youtube/core/async/bg;

    invoke-static {v3}, Lcom/google/android/youtube/core/async/bg;->a(Lcom/google/android/youtube/core/async/bg;)Lcom/google/android/youtube/core/utils/e;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/youtube/core/utils/e;->a()J

    move-result-wide v3

    invoke-direct {v2, p2, v3, v4}, Lcom/google/android/youtube/core/async/Timestamped;-><init>(Ljava/lang/Object;J)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/cache/a;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/async/bh;->b:Lcom/google/android/youtube/core/async/n;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/async/n;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method
