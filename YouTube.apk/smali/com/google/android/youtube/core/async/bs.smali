.class final Lcom/google/android/youtube/core/async/bs;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Ljava/util/List;

.field final synthetic b:Lcom/google/android/youtube/core/model/UserAuth;

.field final synthetic c:Landroid/app/Activity;

.field final synthetic d:Lcom/google/android/youtube/core/async/UserDelegator;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/core/async/UserDelegator;Ljava/util/List;Lcom/google/android/youtube/core/model/UserAuth;Landroid/app/Activity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/async/bs;->d:Lcom/google/android/youtube/core/async/UserDelegator;

    iput-object p2, p0, Lcom/google/android/youtube/core/async/bs;->a:Ljava/util/List;

    iput-object p3, p0, Lcom/google/android/youtube/core/async/bs;->b:Lcom/google/android/youtube/core/model/UserAuth;

    iput-object p4, p0, Lcom/google/android/youtube/core/async/bs;->c:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    check-cast p1, Landroid/app/AlertDialog;

    invoke-virtual {p1}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/async/bs;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/UserProfile;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/UserProfile;->plusUserId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/core/async/bs;->d:Lcom/google/android/youtube/core/async/UserDelegator;

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/async/UserDelegator;->a(Lcom/google/android/youtube/core/async/UserDelegator;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/core/async/bs;->d:Lcom/google/android/youtube/core/async/UserDelegator;

    iget-object v2, p0, Lcom/google/android/youtube/core/async/bs;->b:Lcom/google/android/youtube/core/model/UserAuth;

    invoke-virtual {v2, v0}, Lcom/google/android/youtube/core/model/UserAuth;->cloneWithDelegate(Ljava/lang/String;)Lcom/google/android/youtube/core/model/UserAuth;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/async/UserDelegator;->a(Lcom/google/android/youtube/core/async/UserDelegator;Lcom/google/android/youtube/core/model/UserAuth;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/core/async/bs;->c:Landroid/app/Activity;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/app/Activity;->removeDialog(I)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/async/bs;->d:Lcom/google/android/youtube/core/async/UserDelegator;

    const-string v1, "No +Page Delegate"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/async/UserDelegator;->a(Lcom/google/android/youtube/core/async/UserDelegator;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/async/bs;->d:Lcom/google/android/youtube/core/async/UserDelegator;

    iget-object v1, p0, Lcom/google/android/youtube/core/async/bs;->b:Lcom/google/android/youtube/core/model/UserAuth;

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/async/UserDelegator;->a(Lcom/google/android/youtube/core/async/UserDelegator;Lcom/google/android/youtube/core/model/UserAuth;)V

    goto :goto_0
.end method
