.class final Lcom/google/android/youtube/core/async/bu;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field final synthetic a:Landroid/widget/CheckedTextView;

.field final synthetic b:Lcom/google/android/youtube/core/model/UserProfile;

.field final synthetic c:Lcom/google/android/youtube/core/async/bt;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/core/async/bt;Landroid/widget/CheckedTextView;Lcom/google/android/youtube/core/model/UserProfile;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/async/bu;->c:Lcom/google/android/youtube/core/async/bt;

    iput-object p2, p0, Lcom/google/android/youtube/core/async/bu;->a:Landroid/widget/CheckedTextView;

    iput-object p3, p0, Lcom/google/android/youtube/core/async/bu;->b:Lcom/google/android/youtube/core/model/UserProfile;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    const/4 v3, 0x0

    check-cast p2, Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/youtube/core/async/bu;->a:Landroid/widget/CheckedTextView;

    invoke-virtual {v0}, Landroid/widget/CheckedTextView;->getTag()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/async/bu;->b:Lcom/google/android/youtube/core/model/UserProfile;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/async/bu;->c:Lcom/google/android/youtube/core/async/bt;

    invoke-static {v0}, Lcom/google/android/youtube/core/async/bt;->a(Lcom/google/android/youtube/core/async/bt;)Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080005

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    const/4 v1, 0x1

    invoke-static {p2, v0, v0, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v2, p0, Lcom/google/android/youtube/core/async/bu;->c:Lcom/google/android/youtube/core/async/bt;

    invoke-static {v2}, Lcom/google/android/youtube/core/async/bt;->a(Lcom/google/android/youtube/core/async/bt;)Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/async/bu;->a:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v1, v3, v3, v3}, Landroid/widget/CheckedTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/async/bu;->c:Lcom/google/android/youtube/core/async/bt;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/bt;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method
