.class final Lcom/google/android/youtube/core/async/cd;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/bk;
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/async/by;

.field private final b:Landroid/app/Activity;

.field private final c:Lcom/google/android/youtube/core/async/bk;

.field private final d:I

.field private e:Lcom/google/android/youtube/core/model/UserAuth;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/async/by;Landroid/app/Activity;Lcom/google/android/youtube/core/async/bk;I)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/async/cd;->a:Lcom/google/android/youtube/core/async/by;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/youtube/core/async/cd;->b:Landroid/app/Activity;

    iput-object p3, p0, Lcom/google/android/youtube/core/async/cd;->c:Lcom/google/android/youtube/core/async/bk;

    iput p4, p0, Lcom/google/android/youtube/core/async/cd;->d:I

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/core/async/cd;)Lcom/google/android/youtube/core/model/UserAuth;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/async/cd;->e:Lcom/google/android/youtube/core/model/UserAuth;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/core/async/cd;)Lcom/google/android/youtube/core/async/bk;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/async/cd;->c:Lcom/google/android/youtube/core/async/bk;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/youtube/core/async/cd;->e:Lcom/google/android/youtube/core/model/UserAuth;

    iget-object v0, p0, Lcom/google/android/youtube/core/async/cd;->a:Lcom/google/android/youtube/core/async/by;

    invoke-static {v0}, Lcom/google/android/youtube/core/async/by;->c(Lcom/google/android/youtube/core/async/by;)Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    invoke-interface {v0, p1, p0}, Lcom/google/android/youtube/core/client/bc;->a(Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/async/cd;->b:Landroid/app/Activity;

    new-instance v1, Lcom/google/android/youtube/core/async/ce;

    invoke-direct {v1, p0, p2}, Lcom/google/android/youtube/core/async/ce;-><init>(Lcom/google/android/youtube/core/async/cd;Ljava/lang/Exception;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 5

    check-cast p2, Lcom/google/android/youtube/core/model/UserProfile;

    iget-boolean v0, p2, Lcom/google/android/youtube/core/model/UserProfile;->isEligibleForChannel:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/async/cd;->a:Lcom/google/android/youtube/core/async/by;

    iget-object v1, p0, Lcom/google/android/youtube/core/async/cd;->c:Lcom/google/android/youtube/core/async/bk;

    iget-object v2, p0, Lcom/google/android/youtube/core/async/cd;->e:Lcom/google/android/youtube/core/model/UserAuth;

    iget-object v3, p0, Lcom/google/android/youtube/core/async/cd;->b:Landroid/app/Activity;

    iget v4, p0, Lcom/google/android/youtube/core/async/cd;->d:I

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/youtube/core/async/by;->a(Lcom/google/android/youtube/core/async/by;Lcom/google/android/youtube/core/async/bk;Lcom/google/android/youtube/core/model/UserAuth;Landroid/app/Activity;I)V

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p2, Lcom/google/android/youtube/core/model/UserProfile;->isLightweight:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/async/cd;->a:Lcom/google/android/youtube/core/async/by;

    iget-object v1, p0, Lcom/google/android/youtube/core/async/cd;->c:Lcom/google/android/youtube/core/async/bk;

    iget-object v2, p0, Lcom/google/android/youtube/core/async/cd;->e:Lcom/google/android/youtube/core/model/UserAuth;

    iget-object v3, p0, Lcom/google/android/youtube/core/async/cd;->b:Landroid/app/Activity;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/youtube/core/async/by;->a(Lcom/google/android/youtube/core/async/by;Lcom/google/android/youtube/core/async/bk;Lcom/google/android/youtube/core/model/UserAuth;Landroid/app/Activity;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/async/cd;->c:Lcom/google/android/youtube/core/async/bk;

    iget-object v1, p0, Lcom/google/android/youtube/core/async/cd;->e:Lcom/google/android/youtube/core/model/UserAuth;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/async/bk;->a(Lcom/google/android/youtube/core/model/UserAuth;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/async/cd;->c:Lcom/google/android/youtube/core/async/bk;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/async/bk;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public final g_()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/async/cd;->c:Lcom/google/android/youtube/core/async/bk;

    invoke-interface {v0}, Lcom/google/android/youtube/core/async/bk;->g_()V

    return-void
.end method
