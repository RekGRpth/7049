.class public final Lcom/google/android/youtube/core/async/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/au;


# instance fields
.field private final a:Ljava/util/concurrent/Executor;

.field private final b:Lcom/google/android/youtube/core/async/au;


# direct methods
.method private constructor <init>(Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/async/au;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/core/async/i;->a:Ljava/util/concurrent/Executor;

    iput-object p2, p0, Lcom/google/android/youtube/core/async/i;->b:Lcom/google/android/youtube/core/async/au;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/core/async/i;)Lcom/google/android/youtube/core/async/au;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/async/i;->b:Lcom/google/android/youtube/core/async/au;

    return-object v0
.end method

.method public static a(Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/async/au;)Lcom/google/android/youtube/core/async/i;
    .locals 1

    const-string v0, "executor may not be null"

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "target may not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/youtube/core/async/i;

    invoke-direct {v0, p0, p1}, Lcom/google/android/youtube/core/async/i;-><init>(Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/async/au;)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V
    .locals 2

    const-string v0, "request may not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "callback may not be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/core/async/i;->a:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/youtube/core/async/j;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/youtube/core/async/j;-><init>(Lcom/google/android/youtube/core/async/i;Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-interface {p2, p1, v0}, Lcom/google/android/youtube/core/async/n;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0
.end method
