.class public Lcom/google/android/youtube/core/async/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/au;


# instance fields
.field private final a:Lcom/google/android/youtube/core/async/au;

.field private final b:Lcom/google/android/youtube/core/converter/b;

.field private final c:Lcom/google/android/youtube/core/converter/c;

.field private final d:Ljava/util/concurrent/Executor;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/converter/b;Lcom/google/android/youtube/core/converter/c;Ljava/util/concurrent/Executor;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "requester may not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/au;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/q;->a:Lcom/google/android/youtube/core/async/au;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/async/q;->b:Lcom/google/android/youtube/core/converter/b;

    iput-object p3, p0, Lcom/google/android/youtube/core/async/q;->c:Lcom/google/android/youtube/core/converter/c;

    iput-object p4, p0, Lcom/google/android/youtube/core/async/q;->d:Ljava/util/concurrent/Executor;

    return-void
.end method

.method protected constructor <init>(Lcom/google/android/youtube/core/converter/b;Lcom/google/android/youtube/core/converter/c;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/async/q;->a:Lcom/google/android/youtube/core/async/au;

    iput-object p1, p0, Lcom/google/android/youtube/core/async/q;->b:Lcom/google/android/youtube/core/converter/b;

    iput-object p2, p0, Lcom/google/android/youtube/core/async/q;->c:Lcom/google/android/youtube/core/converter/c;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/q;->d:Ljava/util/concurrent/Executor;

    return-void
.end method

.method public static a(Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/converter/c;)Lcom/google/android/youtube/core/async/au;
    .locals 2

    const/4 v1, 0x0

    const-string v0, "responseConverter may not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/youtube/core/async/q;

    invoke-direct {v0, p0, v1, p1, v1}, Lcom/google/android/youtube/core/async/q;-><init>(Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/converter/b;Lcom/google/android/youtube/core/converter/c;Ljava/util/concurrent/Executor;)V

    return-object v0
.end method

.method public static a(Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/converter/c;Ljava/util/concurrent/Executor;)Lcom/google/android/youtube/core/async/au;
    .locals 2

    const-string v0, "responseConverter may not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "executor may not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/youtube/core/async/q;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/google/android/youtube/core/async/q;-><init>(Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/converter/b;Lcom/google/android/youtube/core/converter/c;Ljava/util/concurrent/Executor;)V

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/async/q;)Lcom/google/android/youtube/core/converter/c;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/async/q;->c:Lcom/google/android/youtube/core/converter/c;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/core/async/q;)Ljava/util/concurrent/Executor;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/async/q;->d:Ljava/util/concurrent/Executor;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/core/async/q;->b:Lcom/google/android/youtube/core/converter/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/async/q;->b:Lcom/google/android/youtube/core/converter/b;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/converter/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    new-instance v1, Lcom/google/android/youtube/core/async/r;

    invoke-direct {v1, p0, p1, v0, p2}, Lcom/google/android/youtube/core/async/r;-><init>(Lcom/google/android/youtube/core/async/q;Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/async/q;->b(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V
    :try_end_0
    .catch Lcom/google/android/youtube/core/converter/ConverterException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-void

    :cond_0
    move-object v0, p1

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, p2, v0}, Lcom/google/android/youtube/core/async/q;->a(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method protected a(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;Ljava/lang/Exception;)V
    .locals 0

    invoke-interface {p3, p1, p4}, Lcom/google/android/youtube/core/async/n;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    return-void
.end method

.method protected b(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/async/q;->a:Lcom/google/android/youtube/core/async/au;

    const-string v1, "subclasses should override doRequest"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/core/async/q;->a:Lcom/google/android/youtube/core/async/au;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method
