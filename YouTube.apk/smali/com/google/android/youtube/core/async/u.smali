.class final Lcom/google/android/youtube/core/async/u;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/async/t;

.field private final b:Ljava/lang/String;

.field private c:Lcom/google/android/youtube/core/async/bk;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/async/t;Ljava/lang/String;Lcom/google/android/youtube/core/async/bk;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/async/u;->a:Lcom/google/android/youtube/core/async/t;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/youtube/core/async/u;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/youtube/core/async/u;->c:Lcom/google/android/youtube/core/async/bk;

    return-void
.end method


# virtual methods
.method public final run(Landroid/accounts/AccountManagerFuture;)V
    .locals 6

    const/4 v5, 0x0

    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    const-string v1, "intent"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/async/u;->c:Lcom/google/android/youtube/core/async/bk;

    invoke-interface {v0}, Lcom/google/android/youtube/core/async/bk;->g_()V
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    iput-object v5, p0, Lcom/google/android/youtube/core/async/u;->c:Lcom/google/android/youtube/core/async/bk;

    :goto_1
    return-void

    :cond_0
    :try_start_1
    const-string v1, "authAccount"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "authtoken"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/google/android/youtube/core/async/u;->c:Lcom/google/android/youtube/core/async/bk;

    new-instance v3, Lcom/google/android/youtube/core/model/UserAuth;

    iget-object v4, p0, Lcom/google/android/youtube/core/async/u;->a:Lcom/google/android/youtube/core/async/t;

    iget-object v4, v4, Lcom/google/android/youtube/core/async/t;->c:Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;

    invoke-direct {v3, v1, v4, v0}, Lcom/google/android/youtube/core/model/UserAuth;-><init>(Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Lcom/google/android/youtube/core/async/bk;->a(Lcom/google/android/youtube/core/model/UserAuth;)V
    :try_end_1
    .catch Landroid/accounts/OperationCanceledException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    iget-object v0, p0, Lcom/google/android/youtube/core/async/u;->c:Lcom/google/android/youtube/core/async/bk;

    invoke-interface {v0}, Lcom/google/android/youtube/core/async/bk;->g_()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iput-object v5, p0, Lcom/google/android/youtube/core/async/u;->c:Lcom/google/android/youtube/core/async/bk;

    goto :goto_1

    :cond_1
    :try_start_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "got null authToken for "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/async/u;->c:Lcom/google/android/youtube/core/async/bk;

    iget-object v1, p0, Lcom/google/android/youtube/core/async/u;->b:Ljava/lang/String;

    new-instance v2, Landroid/accounts/AuthenticatorException;

    invoke-direct {v2}, Landroid/accounts/AuthenticatorException;-><init>()V

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/async/bk;->a(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_3
    .catch Landroid/accounts/OperationCanceledException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_4
    const-string v1, "login IOException"

    invoke-static {v1}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/core/async/u;->c:Lcom/google/android/youtube/core/async/bk;

    iget-object v2, p0, Lcom/google/android/youtube/core/async/u;->b:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Lcom/google/android/youtube/core/async/bk;->a(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    iput-object v5, p0, Lcom/google/android/youtube/core/async/u;->c:Lcom/google/android/youtube/core/async/bk;

    goto :goto_1

    :catch_2
    move-exception v0

    :try_start_5
    const-string v1, "login AuthenticatorException"

    invoke-static {v1}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/core/async/u;->c:Lcom/google/android/youtube/core/async/bk;

    iget-object v2, p0, Lcom/google/android/youtube/core/async/u;->b:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Lcom/google/android/youtube/core/async/bk;->a(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    iput-object v5, p0, Lcom/google/android/youtube/core/async/u;->c:Lcom/google/android/youtube/core/async/bk;

    goto :goto_1

    :catchall_0
    move-exception v0

    iput-object v5, p0, Lcom/google/android/youtube/core/async/u;->c:Lcom/google/android/youtube/core/async/bk;

    throw v0
.end method
