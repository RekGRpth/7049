.class public final Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/n;
.implements Lcom/google/android/youtube/core/client/QoeStatsClient;


# static fields
.field private static final a:Landroid/net/Uri;


# instance fields
.field private final b:Lcom/google/android/youtube/core/async/au;

.field private final c:J

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private f:Z

.field private g:Z

.field private h:I

.field private final i:Lcom/google/android/youtube/core/utils/e;

.field private j:Ljava/util/Map;

.field private k:Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "https://s.youtube.com/api/stats/qoe"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;->a:Landroid/net/Uri;

    return-void
.end method

.method constructor <init>(Lcom/google/android/youtube/core/utils/e;Lcom/google/android/youtube/core/async/au;Ljava/lang/String;Ljava/lang/String;ZIJZ)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;->b:Lcom/google/android/youtube/core/async/au;

    iput-object p3, p0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;->d:Ljava/lang/String;

    const-string v0, "videoId cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;->e:Ljava/lang/String;

    iput-boolean p5, p0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;->g:Z

    iput p6, p0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;->h:I

    iput-object p1, p0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;->i:Lcom/google/android/youtube/core/utils/e;

    const-wide/16 v0, 0x0

    cmp-long v0, p7, v0

    if-gez v0, :cond_0

    invoke-interface {p1}, Lcom/google/android/youtube/core/utils/e;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;->c:J

    :goto_0
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;->j:Ljava/util/Map;

    iget-object v0, p0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;->j:Ljava/util/Map;

    const-string v1, "vps"

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;->j:Ljava/util/Map;

    const-string v1, "vps"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "0.000:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;->NOT_PLAYING:Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;->NOT_PLAYING:Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

    iput-object v0, p0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;->k:Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

    iput-boolean p9, p0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;->f:Z

    return-void

    :cond_0
    iput-wide p7, p0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;->c:J

    goto :goto_0
.end method

.method private a(Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;)V
    .locals 8

    iget-object v0, p0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;->k:Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "%.3f"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;->i:Lcom/google/android/youtube/core/utils/e;

    invoke-interface {v4}, Lcom/google/android/youtube/core/utils/e;->a()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;->c:J

    sub-long/2addr v4, v6

    long-to-double v4, v4

    const-wide v6, 0x408f400000000000L

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;->j:Ljava/util/Map;

    const-string v2, "vps"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object p1, p0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;->k:Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

    goto :goto_0
.end method

.method private j()V
    .locals 6

    iget-object v0, p0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v0, "event"

    const-string v1, "streamingstats"

    invoke-virtual {v3, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "cpn"

    iget-object v2, p0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "ns"

    const-string v2, "yt"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "docid"

    iget-object v2, p0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "fmt"

    iget v2, p0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;->h:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "live"

    iget-boolean v0, p0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;->g:Z

    if-eqz v0, :cond_3

    const-string v0, "live"

    :goto_2
    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    iget-object v0, p0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    const-string v5, ","

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v5, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    goto :goto_3

    :cond_3
    const-string v0, "dvr"

    goto :goto_2

    :cond_4
    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;->b:Lcom/google/android/youtube/core/async/au;

    invoke-interface {v1, v0, p0}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    goto/16 :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 1

    sget-object v0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;->BUFFERING:Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;->a(Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    return-void
.end method

.method public final b()V
    .locals 1

    sget-object v0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;->ENDED:Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;->a(Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;)V

    invoke-direct {p0}, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;->j()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;->f:Z

    return-void
.end method

.method public final c()V
    .locals 1

    sget-object v0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;->PAUSED:Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;->a(Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;)V

    return-void
.end method

.method public final d()V
    .locals 1

    sget-object v0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;->ERROR:Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;->a(Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;)V

    invoke-direct {p0}, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;->j()V

    return-void
.end method

.method public final e()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;->f:Z

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;->PAUSED:Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;->a(Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;)V

    invoke-direct {p0}, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;->j()V

    :cond_0
    return-void
.end method

.method public final f()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;->f:Z

    sget-object v0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;->PLAYING:Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;->a(Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;)V

    return-void
.end method

.method public final g()V
    .locals 1

    sget-object v0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;->SEEKING:Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;->a(Lcom/google/android/youtube/core/client/DefaultQoeStatsClient$PlayerState;)V

    return-void
.end method

.method public final h()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;->f:Z

    return-void
.end method

.method public final i()Lcom/google/android/youtube/core/client/QoeStatsClient$QoeStatsClientState;
    .locals 8

    new-instance v0, Lcom/google/android/youtube/core/client/QoeStatsClient$QoeStatsClientState;

    iget-object v1, p0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;->e:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;->g:Z

    iget v4, p0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;->h:I

    iget-wide v5, p0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;->c:J

    iget-boolean v7, p0, Lcom/google/android/youtube/core/client/DefaultQoeStatsClient;->f:Z

    invoke-direct/range {v0 .. v7}, Lcom/google/android/youtube/core/client/QoeStatsClient$QoeStatsClientState;-><init>(Ljava/lang/String;Ljava/lang/String;ZIJZ)V

    return-object v0
.end method
