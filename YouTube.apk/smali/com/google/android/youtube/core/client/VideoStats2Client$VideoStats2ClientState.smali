.class public final Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final adformat:Ljava/lang/String;

.field public final autoplay:Z

.field public final currentPlaybackPosition:J

.field public final cvssPingCounter:I

.field public final cvssPlaybackId:Ljava/lang/String;

.field public final delay:I

.field public final delayedPingSent:Z

.field public final feature:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field public final finalPingSent:Z

.field public final initialPingSent:Z

.field public final length:Ljava/lang/String;

.field public final scriptedPlayback:Z

.field public final sessionStartTimestamp:J

.field public final throttled:Z

.field public final videoId:Ljava/lang/String;

.field public final vssPlaybackId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/youtube/core/client/bp;

    invoke-direct {v0}, Lcom/google/android/youtube/core/client/bp;-><init>()V

    sput-object v0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZZZZZLjava/lang/String;ILcom/google/android/youtube/core/client/VideoStats2Client$Feature;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->sessionStartTimestamp:J

    iput-wide p3, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->currentPlaybackPosition:J

    iput-object p5, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->adformat:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->length:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->videoId:Ljava/lang/String;

    iput-object p8, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->vssPlaybackId:Ljava/lang/String;

    iput p9, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->delay:I

    iput-boolean p10, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->autoplay:Z

    iput-boolean p11, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->scriptedPlayback:Z

    iput-boolean p12, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->delayedPingSent:Z

    iput-boolean p13, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->finalPingSent:Z

    iput-boolean p14, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->initialPingSent:Z

    move/from16 v0, p15

    iput-boolean v0, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->throttled:Z

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->feature:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->cvssPlaybackId:Ljava/lang/String;

    move/from16 v0, p17

    iput v0, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->cvssPingCounter:I

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->sessionStartTimestamp:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->currentPlaybackPosition:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->adformat:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->length:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->videoId:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->vssPlaybackId:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->cvssPlaybackId:Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->values()[Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    aget-object v0, v0, v3

    iput-object v0, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->feature:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->cvssPingCounter:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->delay:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->autoplay:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->scriptedPlayback:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->delayedPingSent:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->finalPingSent:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->initialPingSent:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_5

    :goto_5
    iput-boolean v1, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->throttled:Z

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v0, v2

    goto :goto_4

    :cond_5
    move v1, v2

    goto :goto_5
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "VideoStats2Client.VideoStats2ClientState{"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " sessionStartTimestamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->sessionStartTimestamp:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " currentPlaybackPosition="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->currentPlaybackPosition:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " adformat="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->adformat:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " length="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->length:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " videoId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->videoId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " vssPlaybackId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->vssPlaybackId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " delay="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->delay:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " autoplay="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->autoplay:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " scriptedPlayback="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->scriptedPlayback:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " delayedPingSent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->delayedPingSent:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " finalPingSent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->finalPingSent:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " initialPingSent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->initialPingSent:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " throttled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->throttled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " feature="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->feature:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " cvssPlaybackId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->cvssPlaybackId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " cvssPingCounter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->cvssPingCounter:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-wide v3, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->sessionStartTimestamp:J

    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    iget-wide v3, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->currentPlaybackPosition:J

    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    iget-object v0, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->adformat:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->length:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->videoId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->vssPlaybackId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->cvssPlaybackId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->feature:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->cvssPingCounter:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->delay:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->autoplay:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->scriptedPlayback:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->delayedPingSent:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->finalPingSent:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->initialPingSent:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->throttled:Z

    if-eqz v0, :cond_5

    :goto_5
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v0, v2

    goto :goto_4

    :cond_5
    move v1, v2

    goto :goto_5
.end method
