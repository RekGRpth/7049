.class public final Lcom/google/android/youtube/core/client/ai;
.super Lcom/google/android/youtube/core/client/h;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/client/be;


# instance fields
.field private final a:Ljava/util/concurrent/Executor;

.field private final h:I

.field private final i:I

.field private final j:Lcom/google/android/youtube/core/async/au;

.field private final k:Lcom/google/android/youtube/core/async/au;

.field private final l:Lcom/google/android/youtube/core/async/au;

.field private final m:Lcom/google/android/youtube/core/async/au;

.field private final n:Lcom/google/android/youtube/core/async/au;

.field private final o:Lcom/google/android/youtube/core/async/au;

.field private final p:Lcom/google/android/youtube/core/async/au;

.field private final q:Lcom/google/android/youtube/core/async/au;

.field private final r:Lcom/google/android/youtube/core/async/au;

.field private final s:Lcom/google/android/youtube/core/async/au;


# direct methods
.method private constructor <init>(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Ljava/lang/String;Lcom/google/android/youtube/core/utils/e;Lcom/google/android/youtube/core/client/aj;II)V
    .locals 5

    invoke-direct {p0, p1, p3, p4, p5}, Lcom/google/android/youtube/core/client/h;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Ljava/lang/String;Lcom/google/android/youtube/core/utils/e;)V

    const-string v0, "cpu executor can\'t be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/youtube/core/client/ai;->a:Ljava/util/concurrent/Executor;

    iput p7, p0, Lcom/google/android/youtube/core/client/ai;->h:I

    iput p8, p0, Lcom/google/android/youtube/core/client/ai;->i:I

    new-instance v0, Lcom/google/android/youtube/core/converter/http/bp;

    invoke-direct {v0}, Lcom/google/android/youtube/core/converter/http/bp;-><init>()V

    iget v1, p0, Lcom/google/android/youtube/core/client/ai;->h:I

    invoke-static {v1}, Lcom/google/android/youtube/core/client/ai;->a(I)Lcom/google/android/youtube/core/cache/b;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/core/client/ai;->f:Lcom/google/android/youtube/core/converter/http/ec;

    invoke-virtual {p0, v2, v0}, Lcom/google/android/youtube/core/client/ai;->a(Lcom/google/android/youtube/core/converter/b;Lcom/google/android/youtube/core/converter/http/bn;)Lcom/google/android/youtube/core/async/an;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/youtube/core/client/ai;->e:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/client/ai;->d()Lcom/google/android/youtube/core/cache/c;

    move-result-object v2

    const-wide/32 v3, 0x240c8400

    invoke-virtual {p0, v2, v0, v3, v4}, Lcom/google/android/youtube/core/client/ai;->a(Lcom/google/android/youtube/core/cache/a;Lcom/google/android/youtube/core/async/au;J)Lcom/google/android/youtube/core/async/bg;

    move-result-object v0

    :cond_0
    const-wide/32 v2, 0x6ddd00

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/google/android/youtube/core/client/ai;->a(Lcom/google/android/youtube/core/cache/a;Lcom/google/android/youtube/core/async/au;J)Lcom/google/android/youtube/core/async/bg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/client/ai;->j:Lcom/google/android/youtube/core/async/au;

    iget-object v0, p0, Lcom/google/android/youtube/core/client/ai;->j:Lcom/google/android/youtube/core/async/au;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/client/ai;->a(Lcom/google/android/youtube/core/async/au;)Lcom/google/android/youtube/core/async/i;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/client/ai;->k:Lcom/google/android/youtube/core/async/au;

    new-instance v0, Lcom/google/android/youtube/core/converter/http/bp;

    invoke-direct {v0}, Lcom/google/android/youtube/core/converter/http/bp;-><init>()V

    iget v1, p0, Lcom/google/android/youtube/core/client/ai;->h:I

    invoke-static {v1}, Lcom/google/android/youtube/core/client/ai;->b(I)Lcom/google/android/youtube/core/cache/j;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/core/client/ai;->f:Lcom/google/android/youtube/core/converter/http/ec;

    invoke-virtual {p0, v2, v0}, Lcom/google/android/youtube/core/client/ai;->a(Lcom/google/android/youtube/core/converter/b;Lcom/google/android/youtube/core/converter/http/bn;)Lcom/google/android/youtube/core/async/an;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/youtube/core/client/ai;->e:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/client/ai;->d()Lcom/google/android/youtube/core/cache/c;

    move-result-object v2

    const-wide/32 v3, 0x240c8400

    invoke-virtual {p0, v2, v0, v3, v4}, Lcom/google/android/youtube/core/client/ai;->a(Lcom/google/android/youtube/core/cache/a;Lcom/google/android/youtube/core/async/au;J)Lcom/google/android/youtube/core/async/bg;

    move-result-object v0

    :cond_1
    const-wide/32 v2, 0x6ddd00

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/google/android/youtube/core/client/ai;->a(Lcom/google/android/youtube/core/cache/a;Lcom/google/android/youtube/core/async/au;J)Lcom/google/android/youtube/core/async/bg;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/core/converter/http/h;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Lcom/google/android/youtube/core/converter/http/h;-><init>(Z)V

    iget v2, p0, Lcom/google/android/youtube/core/client/ai;->i:I

    invoke-static {v2}, Lcom/google/android/youtube/core/client/ai;->b(I)Lcom/google/android/youtube/core/cache/j;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/core/client/ai;->a:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v3}, Lcom/google/android/youtube/core/async/q;->a(Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/converter/c;Ljava/util/concurrent/Executor;)Lcom/google/android/youtube/core/async/au;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/client/ai;->a(Lcom/google/android/youtube/core/async/au;)Lcom/google/android/youtube/core/async/i;

    move-result-object v0

    const-wide/32 v3, 0x6ddd00

    invoke-virtual {p0, v2, v0, v3, v4}, Lcom/google/android/youtube/core/client/ai;->a(Lcom/google/android/youtube/core/cache/a;Lcom/google/android/youtube/core/async/au;J)Lcom/google/android/youtube/core/async/bg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/client/ai;->l:Lcom/google/android/youtube/core/async/au;

    iget v0, p6, Lcom/google/android/youtube/core/client/aj;->a:I

    iget-boolean v1, p6, Lcom/google/android/youtube/core/client/aj;->d:Z

    const/4 v2, 0x1

    iget-object v3, p6, Lcom/google/android/youtube/core/client/aj;->f:Landroid/graphics/Bitmap$Config;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/youtube/core/client/ai;->a(IZZLandroid/graphics/Bitmap$Config;)Lcom/google/android/youtube/core/async/au;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/client/ai;->m:Lcom/google/android/youtube/core/async/au;

    iget v0, p6, Lcom/google/android/youtube/core/client/aj;->b:I

    iget-boolean v1, p6, Lcom/google/android/youtube/core/client/aj;->e:Z

    const/4 v2, 0x1

    iget-object v3, p6, Lcom/google/android/youtube/core/client/aj;->g:Landroid/graphics/Bitmap$Config;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/youtube/core/client/ai;->a(IZZLandroid/graphics/Bitmap$Config;)Lcom/google/android/youtube/core/async/au;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/client/ai;->o:Lcom/google/android/youtube/core/async/au;

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/youtube/core/client/ai;->b(I)Lcom/google/android/youtube/core/cache/j;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/core/converter/a;

    const/high16 v2, 0x3fd00000

    invoke-direct {v1, v2}, Lcom/google/android/youtube/core/converter/a;-><init>(F)V

    iget-object v2, p0, Lcom/google/android/youtube/core/client/ai;->l:Lcom/google/android/youtube/core/async/au;

    iget-object v3, p0, Lcom/google/android/youtube/core/client/ai;->a:Ljava/util/concurrent/Executor;

    invoke-static {v2, v1, v3}, Lcom/google/android/youtube/core/async/q;->a(Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/converter/c;Ljava/util/concurrent/Executor;)Lcom/google/android/youtube/core/async/au;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/client/ai;->a(Lcom/google/android/youtube/core/async/au;)Lcom/google/android/youtube/core/async/i;

    move-result-object v1

    const-wide/32 v2, 0x6ddd00

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/youtube/core/client/ai;->a(Lcom/google/android/youtube/core/cache/a;Lcom/google/android/youtube/core/async/au;J)Lcom/google/android/youtube/core/async/bg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/client/ai;->n:Lcom/google/android/youtube/core/async/au;

    iget v0, p6, Lcom/google/android/youtube/core/client/aj;->a:I

    iget-boolean v1, p6, Lcom/google/android/youtube/core/client/aj;->d:Z

    const/4 v2, 0x0

    iget-object v3, p6, Lcom/google/android/youtube/core/client/aj;->f:Landroid/graphics/Bitmap$Config;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/youtube/core/client/ai;->a(IZZLandroid/graphics/Bitmap$Config;)Lcom/google/android/youtube/core/async/au;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/client/ai;->p:Lcom/google/android/youtube/core/async/au;

    iget v0, p6, Lcom/google/android/youtube/core/client/aj;->b:I

    iget-boolean v1, p6, Lcom/google/android/youtube/core/client/aj;->e:Z

    const/4 v2, 0x0

    iget-object v3, p6, Lcom/google/android/youtube/core/client/aj;->g:Landroid/graphics/Bitmap$Config;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/youtube/core/client/ai;->a(IZZLandroid/graphics/Bitmap$Config;)Lcom/google/android/youtube/core/async/au;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/client/ai;->q:Lcom/google/android/youtube/core/async/au;

    const/16 v0, 0x1e0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/youtube/core/client/ai;->a(IZZLandroid/graphics/Bitmap$Config;)Lcom/google/android/youtube/core/async/au;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/client/ai;->r:Lcom/google/android/youtube/core/async/au;

    iget v0, p6, Lcom/google/android/youtube/core/client/aj;->c:I

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/youtube/core/client/ai;->a(IZZLandroid/graphics/Bitmap$Config;)Lcom/google/android/youtube/core/async/au;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/client/ai;->s:Lcom/google/android/youtube/core/async/au;

    return-void
.end method

.method private a(IZZLandroid/graphics/Bitmap$Config;)Lcom/google/android/youtube/core/async/au;
    .locals 4

    new-instance v0, Lcom/google/android/youtube/core/converter/http/h;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/google/android/youtube/core/converter/http/h;-><init>(IZZLandroid/graphics/Bitmap$Config;)V

    iget-object v1, p0, Lcom/google/android/youtube/core/client/ai;->j:Lcom/google/android/youtube/core/async/au;

    iget-object v2, p0, Lcom/google/android/youtube/core/client/ai;->a:Ljava/util/concurrent/Executor;

    invoke-static {v1, v0, v2}, Lcom/google/android/youtube/core/async/q;->a(Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/converter/c;Ljava/util/concurrent/Executor;)Lcom/google/android/youtube/core/async/au;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/client/ai;->a(Lcom/google/android/youtube/core/async/au;)Lcom/google/android/youtube/core/async/i;

    move-result-object v0

    if-eqz p3, :cond_0

    iget v1, p0, Lcom/google/android/youtube/core/client/ai;->i:I

    invoke-static {v1}, Lcom/google/android/youtube/core/client/ai;->a(I)Lcom/google/android/youtube/core/cache/b;

    move-result-object v1

    const-wide/32 v2, 0x6ddd00

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/google/android/youtube/core/client/ai;->a(Lcom/google/android/youtube/core/cache/a;Lcom/google/android/youtube/core/async/au;J)Lcom/google/android/youtube/core/async/bg;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public static a(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/utils/e;Lcom/google/android/youtube/core/client/aj;II)Lcom/google/android/youtube/core/client/ai;
    .locals 9

    new-instance v0, Lcom/google/android/youtube/core/client/ai;

    const/4 v4, 0x0

    const/16 v7, 0x46

    const/16 v8, 0x1e

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v8}, Lcom/google/android/youtube/core/client/ai;-><init>(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Ljava/lang/String;Lcom/google/android/youtube/core/utils/e;Lcom/google/android/youtube/core/client/aj;II)V

    return-object v0
.end method

.method public static a(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Ljava/lang/String;Lcom/google/android/youtube/core/utils/e;Lcom/google/android/youtube/core/client/aj;Z)Lcom/google/android/youtube/core/client/ai;
    .locals 9

    const-string v0, "cachePath cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/youtube/core/client/ai;

    if-eqz p6, :cond_0

    const/16 v7, 0x12c

    :goto_0
    if-eqz p6, :cond_1

    const/16 v8, 0x64

    :goto_1
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v8}, Lcom/google/android/youtube/core/client/ai;-><init>(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Ljava/lang/String;Lcom/google/android/youtube/core/utils/e;Lcom/google/android/youtube/core/client/aj;II)V

    return-object v0

    :cond_0
    const/16 v7, 0x32

    goto :goto_0

    :cond_1
    const/16 v8, 0xf

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Lcom/google/android/youtube/core/async/n;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/client/ai;->l:Lcom/google/android/youtube/core/async/au;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method

.method public final b(Landroid/net/Uri;Lcom/google/android/youtube/core/async/n;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/client/ai;->n:Lcom/google/android/youtube/core/async/au;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method

.method public final c(Landroid/net/Uri;Lcom/google/android/youtube/core/async/n;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/client/ai;->o:Lcom/google/android/youtube/core/async/au;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method

.method public final d(Landroid/net/Uri;Lcom/google/android/youtube/core/async/n;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/client/ai;->s:Lcom/google/android/youtube/core/async/au;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method
