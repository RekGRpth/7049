.class public final Lcom/google/android/youtube/core/client/am;
.super Lcom/google/android/youtube/core/client/h;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/client/bh;


# static fields
.field private static final a:Landroid/net/Uri;


# instance fields
.field private final h:Lcom/google/android/youtube/core/async/au;

.field private final i:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "https://www.googleapis.com/plus/v1whitelisted/people"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/core/client/am;->a:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;Ljava/lang/String;Lcom/google/android/youtube/core/utils/e;Lorg/apache/http/client/HttpClient;Ljava/lang/String;)V
    .locals 5

    invoke-direct {p0, p1, p4, p2, p3}, Lcom/google/android/youtube/core/client/h;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Ljava/lang/String;Lcom/google/android/youtube/core/utils/e;)V

    iput-object p5, p0, Lcom/google/android/youtube/core/client/am;->i:Ljava/lang/String;

    const/16 v0, 0x1f4

    invoke-static {v0}, Lcom/google/android/youtube/core/client/am;->a(I)Lcom/google/android/youtube/core/cache/b;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/core/converter/http/cg;

    invoke-direct {v1}, Lcom/google/android/youtube/core/converter/http/cg;-><init>()V

    new-instance v2, Lcom/google/android/youtube/core/converter/http/cd;

    sget-object v3, Lcom/google/android/youtube/core/converter/http/HttpMethod;->GET:Lcom/google/android/youtube/core/converter/http/HttpMethod;

    invoke-direct {v2, v3}, Lcom/google/android/youtube/core/converter/http/cd;-><init>(Lcom/google/android/youtube/core/converter/http/HttpMethod;)V

    invoke-virtual {p0, v2, v1}, Lcom/google/android/youtube/core/client/am;->a(Lcom/google/android/youtube/core/converter/b;Lcom/google/android/youtube/core/converter/http/bn;)Lcom/google/android/youtube/core/async/an;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/client/am;->a(Lcom/google/android/youtube/core/async/au;)Lcom/google/android/youtube/core/async/i;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/core/async/s;

    invoke-direct {v2, v1}, Lcom/google/android/youtube/core/async/s;-><init>(Lcom/google/android/youtube/core/async/au;)V

    const-wide/32 v3, 0x6ddd00

    invoke-virtual {p0, v0, v2, v3, v4}, Lcom/google/android/youtube/core/client/am;->a(Lcom/google/android/youtube/core/cache/a;Lcom/google/android/youtube/core/async/au;J)Lcom/google/android/youtube/core/async/bg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/client/am;->h:Lcom/google/android/youtube/core/async/au;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V
    .locals 3

    sget-object v0, Lcom/google/android/youtube/core/client/am;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "me"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "fields"

    const-string v2, "displayName,id,image,url"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/youtube/core/async/ap;->b(Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/ap;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/client/am;->h:Lcom/google/android/youtube/core/async/au;

    invoke-interface {v1, v0, p2}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method
