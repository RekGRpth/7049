.class public final Lcom/google/android/youtube/core/client/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/n;
.implements Lcom/google/android/youtube/core/client/AdStatsClient;


# instance fields
.field private final a:Lcom/google/android/youtube/core/async/au;

.field private final b:Lcom/google/android/youtube/core/model/VastAd;

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:I


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/model/VastAd;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/core/client/j;->a:Lcom/google/android/youtube/core/async/au;

    iput-object p2, p0, Lcom/google/android/youtube/core/client/j;->b:Lcom/google/android/youtube/core/model/VastAd;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/model/VastAd;B)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/client/j;-><init>(Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/model/VastAd;)V

    return-void
.end method

.method constructor <init>(Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/model/VastAd;IZZZ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/client/j;-><init>(Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/model/VastAd;)V

    iput p3, p0, Lcom/google/android/youtube/core/client/j;->f:I

    iput-boolean p4, p0, Lcom/google/android/youtube/core/client/j;->c:Z

    iput-boolean p5, p0, Lcom/google/android/youtube/core/client/j;->d:Z

    iput-boolean p6, p0, Lcom/google/android/youtube/core/client/j;->e:Z

    return-void
.end method

.method private a(Ljava/util/List;)Z
    .locals 4

    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Pinging "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    iget-object v2, p0, Lcom/google/android/youtube/core/client/j;->a:Lcom/google/android/youtube/core/async/au;

    invoke-interface {v2, v0, p0}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/client/j;->b:Lcom/google/android/youtube/core/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/VastAd;->isDummy()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/core/client/j;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/client/j;->b:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/VastAd;->impressionUris:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/client/j;->a(Ljava/util/List;)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/client/j;->d:Z

    :cond_0
    return-void
.end method

.method public final a(I)V
    .locals 5

    const/4 v4, 0x5

    iget v0, p0, Lcom/google/android/youtube/core/client/j;->f:I

    if-eq v0, v4, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/client/j;->b:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/VastAd;->closePingUris:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/client/j;->a(Ljava/util/List;)Z

    iget-object v0, p0, Lcom/google/android/youtube/core/client/j;->b:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/VastAd;->errorPingUris:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "\\[ERRORCODE\\]"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error pinging "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    :try_start_0
    iget-object v2, p0, Lcom/google/android/youtube/core/client/j;->a:Lcom/google/android/youtube/core/async/au;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->d(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-interface {v2, v0, p0}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "Badly formed error uri - ignoring"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    iput v4, p0, Lcom/google/android/youtube/core/client/j;->f:I

    :cond_1
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2

    check-cast p1, Landroid/net/Uri;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Ping failed "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->c()V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    return-void
.end method

.method public final b()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/client/j;->e:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/client/j;->b:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/VastAd;->skipShownPingUris:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/client/j;->a(Ljava/util/List;)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/client/j;->e:Z

    :cond_0
    return-void
.end method

.method public final b(I)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/core/client/j;->b:Lcom/google/android/youtube/core/model/VastAd;

    iget v0, v0, Lcom/google/android/youtube/core/model/VastAd;->duration:I

    mul-int/lit16 v0, v0, 0x3e8

    if-lez v0, :cond_0

    mul-int/lit8 v1, p1, 0x4

    div-int v0, v1, v0

    :goto_0
    iget v1, p0, Lcom/google/android/youtube/core/client/j;->f:I

    if-lt v0, v1, :cond_2

    move v2, v0

    :goto_1
    iget v1, p0, Lcom/google/android/youtube/core/client/j;->f:I

    if-lt v2, v1, :cond_1

    packed-switch v2, :pswitch_data_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    :goto_2
    invoke-direct {p0, v1}, Lcom/google/android/youtube/core/client/j;->a(Ljava/util/List;)Z

    move-result v1

    if-nez v1, :cond_1

    add-int/lit8 v1, v2, -0x1

    move v2, v1

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/youtube/core/client/j;->b:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/VastAd;->firstQuartilePingUris:Ljava/util/List;

    goto :goto_2

    :pswitch_1
    iget-object v1, p0, Lcom/google/android/youtube/core/client/j;->b:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/VastAd;->midpointPingUris:Ljava/util/List;

    goto :goto_2

    :pswitch_2
    iget-object v1, p0, Lcom/google/android/youtube/core/client/j;->b:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/VastAd;->thirdQuartilePingUris:Ljava/util/List;

    goto :goto_2

    :cond_1
    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/youtube/core/client/j;->f:I

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/youtube/core/client/j;->c:Z

    if-nez v0, :cond_3

    const/16 v0, 0x7530

    if-lt p1, v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/core/client/j;->b:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/VastAd;->engagedViewPingUris:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/client/j;->a(Ljava/util/List;)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/client/j;->c:Z

    :cond_3
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/client/j;->b:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/VastAd;->skipPingUris:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/client/j;->a(Ljava/util/List;)Z

    return-void
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/client/j;->b:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/VastAd;->clickthroughPingUris:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/client/j;->a(Ljava/util/List;)Z

    return-void
.end method

.method public final e()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/client/j;->b:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/VastAd;->videoTitleClickedPingUris:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/client/j;->a(Ljava/util/List;)Z

    return-void
.end method

.method public final f()V
    .locals 2

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/client/j;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/client/j;->b:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/VastAd;->impressionUris:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/client/j;->a(Ljava/util/List;)Z

    iput-boolean v1, p0, Lcom/google/android/youtube/core/client/j;->d:Z

    :cond_0
    iget v0, p0, Lcom/google/android/youtube/core/client/j;->f:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/client/j;->b:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/VastAd;->startPingUris:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/client/j;->a(Ljava/util/List;)Z

    iput v1, p0, Lcom/google/android/youtube/core/client/j;->f:I

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/client/j;->b:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/VastAd;->resumePingUris:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/client/j;->a(Ljava/util/List;)Z

    goto :goto_0
.end method

.method public final g()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/client/j;->b:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/VastAd;->closePingUris:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/client/j;->a(Ljava/util/List;)Z

    return-void
.end method

.method public final h()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/client/j;->b:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/VastAd;->pausePingUris:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/client/j;->a(Ljava/util/List;)Z

    return-void
.end method

.method public final i()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/client/j;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/client/j;->b:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/VastAd;->engagedViewPingUris:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/client/j;->a(Ljava/util/List;)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/client/j;->c:Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/client/j;->b:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/VastAd;->completePingUris:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/client/j;->a(Ljava/util/List;)Z

    const/4 v0, 0x5

    iput v0, p0, Lcom/google/android/youtube/core/client/j;->f:I

    return-void
.end method

.method public final j()Lcom/google/android/youtube/core/client/AdStatsClient$AdStatsClientState;
    .locals 5

    new-instance v0, Lcom/google/android/youtube/core/client/AdStatsClient$AdStatsClientState;

    iget v1, p0, Lcom/google/android/youtube/core/client/j;->f:I

    iget-boolean v2, p0, Lcom/google/android/youtube/core/client/j;->c:Z

    iget-boolean v3, p0, Lcom/google/android/youtube/core/client/j;->d:Z

    iget-boolean v4, p0, Lcom/google/android/youtube/core/client/j;->e:Z

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/youtube/core/client/AdStatsClient$AdStatsClientState;-><init>(IZZZ)V

    return-object v0
.end method
