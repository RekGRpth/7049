.class public final Lcom/google/android/youtube/core/client/l;
.super Lcom/google/android/youtube/core/client/h;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/client/d;


# static fields
.field private static final a:Ljava/util/Set;

.field private static final h:Ljava/util/Set;

.field private static final i:Ljava/util/Set;


# instance fields
.field private final j:Landroid/content/SharedPreferences;

.field private final k:Lcom/google/android/youtube/core/client/bc;

.field private final l:Lcom/google/android/youtube/core/utils/a;

.field private final m:Lcom/google/android/youtube/core/async/au;

.field private final n:Lcom/google/android/youtube/core/async/au;

.field private final o:Lcom/google/android/youtube/core/async/au;

.field private p:Lcom/google/android/youtube/core/async/n;

.field private q:J

.field private r:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/google/android/youtube/core/client/l;->a:Ljava/util/Set;

    const-string v1, "YT:ADSENSE"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/youtube/core/client/l;->a:Ljava/util/Set;

    const-string v1, "ADSENSE"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/youtube/core/client/l;->a:Ljava/util/Set;

    const-string v1, "ADSENSE/ADX"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/google/android/youtube/core/client/l;->h:Ljava/util/Set;

    const-string v1, "YT:DOUBLECLICK"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/youtube/core/client/l;->h:Ljava/util/Set;

    const-string v1, "GDFP"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/youtube/core/client/l;->h:Ljava/util/Set;

    const-string v1, "DART"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/youtube/core/client/l;->h:Ljava/util/Set;

    const-string v1, "DART_DFA"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/youtube/core/client/l;->h:Ljava/util/Set;

    const-string v1, "DART_DFP"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/google/android/youtube/core/client/l;->i:Ljava/util/Set;

    const-string v1, "YT:FREEWHEEL"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/youtube/core/client/l;->i:Ljava/util/Set;

    const-string v1, "FREEWHEEL"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/utils/e;Landroid/content/SharedPreferences;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/converter/m;Lcom/google/android/youtube/core/player/ak;Ljava/util/concurrent/atomic/AtomicReference;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 18

    const/4 v14, 0x0

    const/16 v17, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v15, p15

    move-object/from16 v16, p16

    invoke-direct/range {v0 .. v17}, Lcom/google/android/youtube/core/client/l;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/utils/e;Landroid/content/SharedPreferences;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/converter/m;Lcom/google/android/youtube/core/player/ak;Ljava/util/concurrent/atomic/AtomicReference;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/utils/e;Landroid/content/SharedPreferences;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/converter/m;Lcom/google/android/youtube/core/player/ak;Ljava/util/concurrent/atomic/AtomicReference;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 10

    move-object/from16 v0, p7

    invoke-direct {p0, p2, p5, v0, p3}, Lcom/google/android/youtube/core/client/h;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/m;Lcom/google/android/youtube/core/utils/e;)V

    const-string v1, "context cannot be null"

    invoke-static {p1, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "preferences cannot be null"

    invoke-static {p4, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/SharedPreferences;

    iput-object v1, p0, Lcom/google/android/youtube/core/client/l;->j:Landroid/content/SharedPreferences;

    const-string v1, "gdataClient cannot be null"

    move-object/from16 v0, p6

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/client/bc;

    iput-object v1, p0, Lcom/google/android/youtube/core/client/l;->k:Lcom/google/android/youtube/core/client/bc;

    invoke-interface {p3}, Lcom/google/android/youtube/core/utils/e;->a()J

    move-result-wide v1

    const-string v3, "last_ad_time"

    const-wide/16 v4, 0x0

    invoke-interface {p4, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/youtube/core/client/l;->q:J

    move/from16 v0, p17

    iput-boolean v0, p0, Lcom/google/android/youtube/core/client/l;->r:Z

    new-instance v1, Lcom/google/android/youtube/core/utils/a;

    move-object v2, p1

    move-object/from16 v3, p8

    move-object/from16 v4, p11

    move-object/from16 v5, p15

    move-object/from16 v6, p16

    invoke-direct/range {v1 .. v6}, Lcom/google/android/youtube/core/utils/a;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/player/ak;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/youtube/core/client/l;->l:Lcom/google/android/youtube/core/utils/a;

    iget-object v1, p0, Lcom/google/android/youtube/core/client/l;->f:Lcom/google/android/youtube/core/converter/http/ec;

    new-instance v2, Lcom/google/android/youtube/core/converter/http/ex;

    move-object/from16 v0, p7

    invoke-direct {v2, v0, p3}, Lcom/google/android/youtube/core/converter/http/ex;-><init>(Lcom/google/android/youtube/core/converter/m;Lcom/google/android/youtube/core/utils/e;)V

    invoke-virtual {p0, v1, v2}, Lcom/google/android/youtube/core/client/l;->a(Lcom/google/android/youtube/core/converter/b;Lcom/google/android/youtube/core/converter/http/bn;)Lcom/google/android/youtube/core/async/an;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/client/l;->a(Lcom/google/android/youtube/core/async/au;)Lcom/google/android/youtube/core/async/i;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/client/l;->o:Lcom/google/android/youtube/core/async/au;

    new-instance v1, Lcom/google/android/youtube/core/converter/http/a;

    iget-object v4, p0, Lcom/google/android/youtube/core/client/l;->l:Lcom/google/android/youtube/core/utils/a;

    move-object v2, p3

    move-object/from16 v3, p9

    move-object/from16 v5, p10

    move-object/from16 v6, p12

    move-object/from16 v7, p14

    move-object/from16 v8, p13

    move/from16 v9, p17

    invoke-direct/range {v1 .. v9}, Lcom/google/android/youtube/core/converter/http/a;-><init>(Lcom/google/android/youtube/core/utils/e;Ljava/util/concurrent/atomic/AtomicReference;Lcom/google/android/youtube/core/utils/a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    if-eqz p17, :cond_0

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/youtube/core/client/l;->m:Lcom/google/android/youtube/core/async/au;

    new-instance v2, Lcom/google/android/youtube/core/converter/http/hc;

    move-object/from16 v0, p7

    invoke-direct {v2, v0, p3}, Lcom/google/android/youtube/core/converter/http/hc;-><init>(Lcom/google/android/youtube/core/converter/m;Lcom/google/android/youtube/core/utils/e;)V

    invoke-virtual {p0, v1, v2}, Lcom/google/android/youtube/core/client/l;->a(Lcom/google/android/youtube/core/converter/b;Lcom/google/android/youtube/core/converter/http/bn;)Lcom/google/android/youtube/core/async/an;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/client/l;->a(Lcom/google/android/youtube/core/async/au;)Lcom/google/android/youtube/core/async/i;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/client/l;->n:Lcom/google/android/youtube/core/async/au;

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, v1, v1}, Lcom/google/android/youtube/core/client/l;->a(Lcom/google/android/youtube/core/converter/b;Lcom/google/android/youtube/core/converter/http/bn;)Lcom/google/android/youtube/core/async/an;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/client/l;->a(Lcom/google/android/youtube/core/async/au;)Lcom/google/android/youtube/core/async/i;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/client/l;->m:Lcom/google/android/youtube/core/async/au;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/youtube/core/client/l;->n:Lcom/google/android/youtube/core/async/au;

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/client/l;)Lcom/google/android/youtube/core/async/au;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/client/l;->o:Lcom/google/android/youtube/core/async/au;

    return-object v0
.end method

.method private static a(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3

    const/4 v1, 0x0

    :try_start_0
    invoke-static {p0}, Lcom/google/android/youtube/core/utils/ak;->a(Landroid/net/Uri;)Lcom/google/android/youtube/core/utils/ak;

    move-result-object v0

    iget-object v2, v0, Lcom/google/android/youtube/core/utils/ak;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-gtz v2, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Unable to find video id in watch uri from VastAd "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, Lcom/google/android/youtube/core/utils/ak;->a:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Unable to parse watch uri from VastAd "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/client/l;Lcom/google/android/youtube/core/model/VastAd;Ljava/lang/String;Landroid/net/Uri;Ljava/util/List;)V
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/VastAd;->buildUpon()Lcom/google/android/youtube/core/model/ae;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/google/android/youtube/core/model/ae;->b(Ljava/lang/String;)Lcom/google/android/youtube/core/model/ae;

    move-result-object v2

    iget-boolean v3, p1, Lcom/google/android/youtube/core/model/VastAd;->shouldPingVssOnEngaged:Z

    if-eqz v3, :cond_0

    iget-object v3, p1, Lcom/google/android/youtube/core/model/VastAd;->adSystem:Ljava/lang/String;

    invoke-direct {p0, p3, v3}, Lcom/google/android/youtube/core/client/l;->a(Landroid/net/Uri;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p1, Lcom/google/android/youtube/core/model/VastAd;->adSystem:Ljava/lang/String;

    invoke-direct {p0, p3, v3}, Lcom/google/android/youtube/core/client/l;->b(Landroid/net/Uri;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2, v1}, Lcom/google/android/youtube/core/model/ae;->a(Z)Lcom/google/android/youtube/core/model/ae;

    :cond_0
    if-eqz p4, :cond_1

    invoke-virtual {v2, p4}, Lcom/google/android/youtube/core/model/ae;->a(Ljava/util/List;)Lcom/google/android/youtube/core/model/ae;

    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p1, Lcom/google/android/youtube/core/model/VastAd;->adSystem:Ljava/lang/String;

    invoke-direct {p0, p3, v4}, Lcom/google/android/youtube/core/client/l;->a(Landroid/net/Uri;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    const-string v0, "1"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    :goto_0
    const-string v0, "_2"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/VastAd;->isSkippable()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "_1"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/youtube/core/model/ae;->e(Ljava/lang/String;)Lcom/google/android/youtube/core/model/ae;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/model/ae;->b()Lcom/google/android/youtube/core/model/VastAd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/VastAd;->firstStreamUri()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/utils/Util;->b(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/VastAd;->firstStreamUri()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/client/l;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_9

    iget-object v1, p0, Lcom/google/android/youtube/core/client/l;->p:Lcom/google/android/youtube/core/async/n;

    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unable to parse video Id from watch Uri "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/VastAd;->firstStreamUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, p2, v2}, Lcom/google/android/youtube/core/async/n;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    :goto_1
    return-void

    :cond_4
    iget-object v4, p1, Lcom/google/android/youtube/core/model/VastAd;->adSystem:Ljava/lang/String;

    invoke-direct {p0, p3, v4}, Lcom/google/android/youtube/core/client/l;->b(Landroid/net/Uri;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    const-string v0, "2"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_5
    iget-object v4, p1, Lcom/google/android/youtube/core/model/VastAd;->adSystem:Ljava/lang/String;

    if-eqz v4, :cond_7

    sget-object v5, Lcom/google/android/youtube/core/client/l;->i:Ljava/util/Set;

    invoke-interface {v5, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    :cond_6
    :goto_2
    if-eqz v0, :cond_2

    const-string v0, "4"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_7
    if-eqz p3, :cond_8

    invoke-virtual {p3}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v4

    const-string v5, ".fwmrm.net"

    invoke-virtual {v4, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_6

    :cond_8
    move v0, v1

    goto :goto_2

    :cond_9
    iget-object v2, p0, Lcom/google/android/youtube/core/client/l;->k:Lcom/google/android/youtube/core/client/bc;

    new-instance v3, Lcom/google/android/youtube/core/client/q;

    invoke-direct {v3, p0, p2, v0}, Lcom/google/android/youtube/core/client/q;-><init>(Lcom/google/android/youtube/core/client/l;Ljava/lang/String;Lcom/google/android/youtube/core/model/VastAd;)V

    invoke-interface {v2, v1, v3}, Lcom/google/android/youtube/core/client/bc;->a(Ljava/lang/String;Lcom/google/android/youtube/core/async/n;)V

    goto :goto_1

    :cond_a
    iget-object v1, p0, Lcom/google/android/youtube/core/client/l;->p:Lcom/google/android/youtube/core/async/n;

    invoke-interface {v1, p2, v0}, Lcom/google/android/youtube/core/async/n;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1
.end method

.method private a(Landroid/net/Uri;Ljava/lang/String;)Z
    .locals 3

    const/4 v0, 0x1

    if-eqz p2, :cond_1

    sget-object v1, Lcom/google/android/youtube/core/client/l;->h:Ljava/util/Set;

    invoke-interface {v1, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    const-string v2, ".doubleclick.net"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/client/l;->b(Landroid/net/Uri;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/client/l;Landroid/net/Uri;Ljava/lang/String;)Z
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/client/l;->a(Landroid/net/Uri;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/google/android/youtube/core/client/l;)Lcom/google/android/youtube/core/async/n;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/client/l;->p:Lcom/google/android/youtube/core/async/n;

    return-object v0
.end method

.method private b(Landroid/net/Uri;Ljava/lang/String;)Z
    .locals 2

    const/4 v0, 0x1

    if-eqz p2, :cond_1

    sget-object v1, Lcom/google/android/youtube/core/client/l;->a:Ljava/util/Set;

    invoke-interface {v1, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p1, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/core/client/l;->l:Lcom/google/android/youtube/core/utils/a;

    invoke-virtual {v1, p1}, Lcom/google/android/youtube/core/utils/a;->c(Landroid/net/Uri;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/core/client/l;->d:Lcom/google/android/youtube/core/utils/e;

    invoke-interface {v0}, Lcom/google/android/youtube/core/utils/e;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/youtube/core/client/l;->q:J

    iget-object v0, p0, Lcom/google/android/youtube/core/client/l;->j:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "last_ad_time"

    iget-wide v2, p0, Lcom/google/android/youtube/core/client/l;->q:J

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/SharedPreferences$Editor;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/youtube/core/async/n;)V
    .locals 5

    const/4 v4, 0x0

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {p2}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/n;

    iput-object v0, p0, Lcom/google/android/youtube/core/client/l;->p:Lcom/google/android/youtube/core/async/n;

    iget-boolean v0, p0, Lcom/google/android/youtube/core/client/l;->r:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/client/l;->n:Lcom/google/android/youtube/core/async/au;

    new-instance v1, Lcom/google/android/youtube/core/converter/http/b;

    iget-wide v2, p0, Lcom/google/android/youtube/core/client/l;->q:J

    invoke-direct {v1, p1, v2, v3}, Lcom/google/android/youtube/core/converter/http/b;-><init>(Ljava/lang/String;J)V

    new-instance v2, Lcom/google/android/youtube/core/client/r;

    invoke-direct {v2, p0, v4}, Lcom/google/android/youtube/core/client/r;-><init>(Lcom/google/android/youtube/core/client/l;B)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/client/l;->m:Lcom/google/android/youtube/core/async/au;

    new-instance v1, Lcom/google/android/youtube/core/converter/http/b;

    iget-wide v2, p0, Lcom/google/android/youtube/core/client/l;->q:J

    invoke-direct {v1, p1, v2, v3}, Lcom/google/android/youtube/core/converter/http/b;-><init>(Ljava/lang/String;J)V

    new-instance v2, Lcom/google/android/youtube/core/client/n;

    invoke-direct {v2, p0, v4}, Lcom/google/android/youtube/core/client/n;-><init>(Lcom/google/android/youtube/core/client/l;B)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/client/l;->l:Lcom/google/android/youtube/core/utils/a;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/utils/a;->a(Z)V

    return-void
.end method
