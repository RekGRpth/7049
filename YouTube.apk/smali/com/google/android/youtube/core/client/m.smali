.class abstract Lcom/google/android/youtube/core/client/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/client/l;

.field private final b:Ljava/lang/String;

.field private c:J

.field private d:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/client/l;Ljava/lang/String;J)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/client/m;->a:Lcom/google/android/youtube/core/client/l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/youtube/core/client/m;->b:Ljava/lang/String;

    iput-wide p3, p0, Lcom/google/android/youtube/core/client/m;->c:J

    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/youtube/core/model/VastAd;Landroid/net/Uri;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/core/client/m;->a:Lcom/google/android/youtube/core/client/l;

    iget-object v1, p0, Lcom/google/android/youtube/core/client/m;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/core/client/m;->d:Ljava/util/List;

    invoke-static {v0, p1, v1, p2, v2}, Lcom/google/android/youtube/core/client/l;->a(Lcom/google/android/youtube/core/client/l;Lcom/google/android/youtube/core/model/VastAd;Ljava/lang/String;Landroid/net/Uri;Ljava/util/List;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 4

    check-cast p1, Landroid/net/Uri;

    const-string v0, "Error retrieving VAST ad"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/client/m;->a:Lcom/google/android/youtube/core/client/l;

    iget-object v0, v0, Lcom/google/android/youtube/core/client/l;->d:Lcom/google/android/youtube/core/utils/e;

    invoke-interface {v0}, Lcom/google/android/youtube/core/utils/e;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/youtube/core/client/m;->c:J

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/client/m;->a(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/client/m;->a:Lcom/google/android/youtube/core/client/l;

    invoke-static {v0}, Lcom/google/android/youtube/core/client/l;->b(Lcom/google/android/youtube/core/client/l;)Lcom/google/android/youtube/core/async/n;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/client/m;->b:Ljava/lang/String;

    invoke-interface {v0, v1, p2}, Lcom/google/android/youtube/core/async/n;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    :cond_1
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 6

    const/4 v1, 0x0

    const/4 v2, 0x0

    check-cast p1, Landroid/net/Uri;

    check-cast p2, Ljava/util/List;

    if-eqz p2, :cond_8

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/VastAd;

    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/VastAd;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/VastAd;->isDummy()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {p0, v0, p1}, Lcom/google/android/youtube/core/client/m;->a(Lcom/google/android/youtube/core/model/VastAd;Landroid/net/Uri;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/VastAd;->isDummy()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/youtube/core/client/m;->d:Ljava/util/List;

    if-nez v3, :cond_2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/google/android/youtube/core/client/m;->d:Ljava/util/List;

    :cond_2
    iget-object v3, p0, Lcom/google/android/youtube/core/client/m;->d:Ljava/util/List;

    iget-object v4, v0, Lcom/google/android/youtube/core/model/VastAd;->impressionUris:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_3
    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/android/youtube/core/client/m;->a:Lcom/google/android/youtube/core/client/l;

    iget-object v4, v0, Lcom/google/android/youtube/core/model/VastAd;->adSystem:Ljava/lang/String;

    invoke-static {v3, p1, v4}, Lcom/google/android/youtube/core/client/l;->a(Lcom/google/android/youtube/core/client/l;Landroid/net/Uri;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-boolean v0, v0, Lcom/google/android/youtube/core/model/VastAd;->fallbackHint:Z

    if-eqz v0, :cond_6

    :cond_4
    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/youtube/core/client/m;->a:Lcom/google/android/youtube/core/client/l;

    iget-object v0, v0, Lcom/google/android/youtube/core/client/l;->d:Lcom/google/android/youtube/core/utils/e;

    invoke-interface {v0}, Lcom/google/android/youtube/core/utils/e;->a()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/youtube/core/client/m;->c:J

    cmp-long v0, v2, v4

    if-gtz v0, :cond_5

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/client/m;->a(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/youtube/core/client/m;->d:Ljava/util/List;

    if-eqz v0, :cond_7

    new-instance v0, Lcom/google/android/youtube/core/model/ae;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/ae;-><init>()V

    iget-object v1, p0, Lcom/google/android/youtube/core/client/m;->d:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/ae;->a(Ljava/util/List;)Lcom/google/android/youtube/core/model/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/ae;->b()Lcom/google/android/youtube/core/model/VastAd;

    move-result-object v0

    :goto_3
    iget-object v1, p0, Lcom/google/android/youtube/core/client/m;->a:Lcom/google/android/youtube/core/client/l;

    invoke-static {v1}, Lcom/google/android/youtube/core/client/l;->b(Lcom/google/android/youtube/core/client/l;)Lcom/google/android/youtube/core/async/n;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/core/client/m;->b:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Lcom/google/android/youtube/core/async/n;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    :cond_6
    move v0, v2

    goto :goto_2

    :cond_7
    move-object v0, v1

    goto :goto_3

    :cond_8
    move-object v0, v1

    goto :goto_0
.end method

.method public abstract a(Landroid/net/Uri;)Z
.end method
