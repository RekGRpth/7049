.class final Lcom/google/android/youtube/core/client/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/client/l;

.field private final b:Ljava/lang/String;

.field private final c:Lcom/google/android/youtube/core/model/VastAd;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/client/l;Ljava/lang/String;Lcom/google/android/youtube/core/model/VastAd;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/client/q;->a:Lcom/google/android/youtube/core/client/l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/youtube/core/client/q;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/youtube/core/client/q;->c:Lcom/google/android/youtube/core/model/VastAd;

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2

    const-string v0, "Error retrieving video for the ad"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/client/q;->a:Lcom/google/android/youtube/core/client/l;

    invoke-static {v0}, Lcom/google/android/youtube/core/client/l;->b(Lcom/google/android/youtube/core/client/l;)Lcom/google/android/youtube/core/async/n;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/client/q;->b:Ljava/lang/String;

    invoke-interface {v0, v1, p2}, Lcom/google/android/youtube/core/async/n;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 6

    check-cast p2, Lcom/google/android/youtube/core/model/Video;

    if-eqz p2, :cond_2

    iget-object v0, p2, Lcom/google/android/youtube/core/model/Video;->state:Lcom/google/android/youtube/core/model/Video$State;

    sget-object v1, Lcom/google/android/youtube/core/model/Video$State;->PLAYABLE:Lcom/google/android/youtube/core/model/Video$State;

    if-ne v0, v1, :cond_2

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p2, Lcom/google/android/youtube/core/model/Video;->streams:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Stream;

    iget-object v3, v0, Lcom/google/android/youtube/core/model/Stream;->uri:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "splay"

    const-string v5, "1"

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "dnc"

    const-string v5, "1"

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/Stream;->buildUpon()Lcom/google/android/youtube/core/model/Stream$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/core/model/Stream$Builder;->uri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Stream$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/Stream$Builder;->build()Lcom/google/android/youtube/core/model/Stream;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/client/q;->c:Lcom/google/android/youtube/core/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/VastAd;->buildUpon()Lcom/google/android/youtube/core/model/ae;

    move-result-object v0

    iget-object v2, p2, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/model/ae;->a(Ljava/lang/String;)Lcom/google/android/youtube/core/model/ae;

    move-result-object v0

    iget-object v2, p2, Lcom/google/android/youtube/core/model/Video;->title:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/model/ae;->c(Ljava/lang/String;)Lcom/google/android/youtube/core/model/ae;

    move-result-object v0

    iget v2, p2, Lcom/google/android/youtube/core/model/Video;->duration:I

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/model/ae;->a(I)Lcom/google/android/youtube/core/model/ae;

    move-result-object v2

    iget-object v0, p2, Lcom/google/android/youtube/core/model/Video;->privacy:Lcom/google/android/youtube/core/model/Video$Privacy;

    sget-object v3, Lcom/google/android/youtube/core/model/Video$Privacy;->PUBLIC:Lcom/google/android/youtube/core/model/Video$Privacy;

    if-ne v0, v3, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v2, v0}, Lcom/google/android/youtube/core/model/ae;->c(Z)Lcom/google/android/youtube/core/model/ae;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/ae;->a(Ljava/util/Collection;)Lcom/google/android/youtube/core/model/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/ae;->b()Lcom/google/android/youtube/core/model/VastAd;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/client/q;->a:Lcom/google/android/youtube/core/client/l;

    invoke-static {v1}, Lcom/google/android/youtube/core/client/l;->b(Lcom/google/android/youtube/core/client/l;)Lcom/google/android/youtube/core/async/n;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/core/client/q;->b:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Lcom/google/android/youtube/core/async/n;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_2
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/core/client/q;->a:Lcom/google/android/youtube/core/client/l;

    invoke-static {v0}, Lcom/google/android/youtube/core/client/l;->b(Lcom/google/android/youtube/core/client/l;)Lcom/google/android/youtube/core/async/n;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/client/q;->b:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/async/n;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_2
.end method
