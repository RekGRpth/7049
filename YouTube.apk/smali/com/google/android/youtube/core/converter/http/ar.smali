.class public final Lcom/google/android/youtube/core/converter/http/ar;
.super Lcom/google/android/youtube/core/converter/http/bc;
.source "SourceFile"


# instance fields
.field private final c:Lcom/google/android/youtube/core/converter/d;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/converter/m;Z)V
    .locals 4

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/converter/http/bc;-><init>(Lcom/google/android/youtube/core/converter/m;)V

    const-string v0, "/feed"

    invoke-static {v0}, Lcom/google/android/youtube/core/converter/f;->a(Ljava/lang/String;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    const-string v1, "/feed/entry"

    new-instance v2, Lcom/google/android/youtube/core/converter/http/az;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/core/converter/http/az;-><init>(Lcom/google/android/youtube/core/converter/http/ar;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    const-string v1, "/feed/entry/author/name"

    new-instance v2, Lcom/google/android/youtube/core/converter/http/ay;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/core/converter/http/ay;-><init>(Lcom/google/android/youtube/core/converter/http/ar;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    const-string v1, "/feed/entry/author/uri"

    new-instance v2, Lcom/google/android/youtube/core/converter/http/ax;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/core/converter/http/ax;-><init>(Lcom/google/android/youtube/core/converter/http/ar;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    const-string v1, "/feed/entry/category"

    new-instance v2, Lcom/google/android/youtube/core/converter/http/aw;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/core/converter/http/aw;-><init>(Lcom/google/android/youtube/core/converter/http/ar;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    const-string v1, "/feed/entry/updated"

    new-instance v2, Lcom/google/android/youtube/core/converter/http/av;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/core/converter/http/av;-><init>(Lcom/google/android/youtube/core/converter/http/ar;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    const-string v1, "/feed/entry/yt:groupId"

    new-instance v2, Lcom/google/android/youtube/core/converter/http/au;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/core/converter/http/au;-><init>(Lcom/google/android/youtube/core/converter/http/ar;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    const-string v1, "/feed/entry/yt:videoid"

    new-instance v2, Lcom/google/android/youtube/core/converter/http/at;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/core/converter/http/at;-><init>(Lcom/google/android/youtube/core/converter/http/ar;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    const-string v1, "/feed/entry/yt:username"

    new-instance v2, Lcom/google/android/youtube/core/converter/http/as;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/core/converter/http/as;-><init>(Lcom/google/android/youtube/core/converter/http/ar;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    const-string v1, "/feed/entry/link"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/entry"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, p2}, Lcom/google/android/youtube/core/converter/http/fr;->d(Lcom/google/android/youtube/core/converter/e;Ljava/lang/String;Z)V

    new-instance v2, Lcom/google/android/youtube/core/converter/http/ba;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/core/converter/http/ba;-><init>(Lcom/google/android/youtube/core/converter/http/ar;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/converter/e;->a()Lcom/google/android/youtube/core/converter/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/converter/http/ar;->c:Lcom/google/android/youtube/core/converter/d;

    return-void
.end method


# virtual methods
.method protected final a()Lcom/google/android/youtube/core/converter/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/converter/http/ar;->c:Lcom/google/android/youtube/core/converter/d;

    return-object v0
.end method
