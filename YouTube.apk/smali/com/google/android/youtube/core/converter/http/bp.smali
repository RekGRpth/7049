.class public final Lcom/google/android/youtube/core/converter/http/bp;
.super Lcom/google/android/youtube/core/converter/http/bn;
.source "SourceFile"


# instance fields
.field private final a:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/core/converter/http/bn;-><init>()V

    const/16 v0, 0x4000

    iput v0, p0, Lcom/google/android/youtube/core/converter/http/bp;->a:I

    return-void
.end method


# virtual methods
.method protected final synthetic a(Lorg/apache/http/HttpEntity;)Ljava/lang/Object;
    .locals 2

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p1}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v0

    long-to-int v0, v0

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    if-lez v0, :cond_1

    :goto_1
    invoke-direct {v1, v0}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    invoke-interface {p1, v1}, Lorg/apache/http/HttpEntity;->writeTo(Ljava/io/OutputStream;)V

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V

    goto :goto_0

    :cond_1
    const/16 v0, 0x4000

    goto :goto_1
.end method
