.class public final Lcom/google/android/youtube/core/converter/http/cj;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Lcom/google/android/youtube/core/converter/e;)V
    .locals 2

    const-string v0, "/feed/entry"

    new-instance v1, Lcom/google/android/youtube/core/converter/http/ck;

    invoke-direct {v1}, Lcom/google/android/youtube/core/converter/http/ck;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    const-string v0, "/feed/entry"

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/converter/http/cj;->a(Lcom/google/android/youtube/core/converter/e;Ljava/lang/String;)V

    return-void
.end method

.method private static a(Lcom/google/android/youtube/core/converter/e;Ljava/lang/String;)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/summary"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/core/converter/http/cv;

    invoke-direct {v1}, Lcom/google/android/youtube/core/converter/http/cv;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/content"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/core/converter/http/cu;

    invoke-direct {v2}, Lcom/google/android/youtube/core/converter/http/cu;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/link"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/core/converter/http/ct;

    invoke-direct {v2}, Lcom/google/android/youtube/core/converter/http/ct;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/yt:countHint"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/core/converter/http/cs;

    invoke-direct {v2}, Lcom/google/android/youtube/core/converter/http/cs;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    invoke-static {p0, p1}, Lcom/google/android/youtube/core/converter/http/cj;->b(Lcom/google/android/youtube/core/converter/e;Ljava/lang/String;)V

    return-void
.end method

.method public static b(Lcom/google/android/youtube/core/converter/e;)V
    .locals 2

    const-string v0, "/entry"

    new-instance v1, Lcom/google/android/youtube/core/converter/http/cr;

    invoke-direct {v1}, Lcom/google/android/youtube/core/converter/http/cr;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    const-string v0, "/entry"

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/converter/http/cj;->a(Lcom/google/android/youtube/core/converter/e;Ljava/lang/String;)V

    return-void
.end method

.method private static b(Lcom/google/android/youtube/core/converter/e;Ljava/lang/String;)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/yt:playlistId"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/core/converter/http/cq;

    invoke-direct {v1}, Lcom/google/android/youtube/core/converter/http/cq;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/author/name"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/core/converter/http/cp;

    invoke-direct {v2}, Lcom/google/android/youtube/core/converter/http/cp;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/title"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/core/converter/http/co;

    invoke-direct {v2}, Lcom/google/android/youtube/core/converter/http/co;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/updated"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/core/converter/http/cn;

    invoke-direct {v2}, Lcom/google/android/youtube/core/converter/http/cn;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/media:group/media:thumbnail"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/core/converter/http/cm;

    invoke-direct {v2}, Lcom/google/android/youtube/core/converter/http/cm;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    return-void
.end method

.method public static c(Lcom/google/android/youtube/core/converter/e;)V
    .locals 3

    const-string v0, "/feed"

    new-instance v1, Lcom/google/android/youtube/core/converter/http/cl;

    invoke-direct {v1}, Lcom/google/android/youtube/core/converter/http/cl;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    const-string v1, "/feed/subtitle"

    new-instance v2, Lcom/google/android/youtube/core/converter/http/cy;

    invoke-direct {v2}, Lcom/google/android/youtube/core/converter/http/cy;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    const-string v1, "/feed/link"

    new-instance v2, Lcom/google/android/youtube/core/converter/http/cx;

    invoke-direct {v2}, Lcom/google/android/youtube/core/converter/http/cx;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    const-string v1, "/feed/openSearch:totalResults"

    new-instance v2, Lcom/google/android/youtube/core/converter/http/cw;

    invoke-direct {v2}, Lcom/google/android/youtube/core/converter/http/cw;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    const-string v0, "/feed"

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/converter/http/cj;->b(Lcom/google/android/youtube/core/converter/e;Ljava/lang/String;)V

    return-void
.end method
