.class final Lcom/google/android/youtube/core/converter/http/db;
.super Lcom/google/android/youtube/core/converter/n;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/converter/http/cz;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/core/converter/http/cz;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/converter/http/db;->a:Lcom/google/android/youtube/core/converter/http/cz;

    invoke-direct {p0}, Lcom/google/android/youtube/core/converter/n;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/utils/ae;Lorg/xml/sax/Attributes;)V
    .locals 2

    const-class v0, Lcom/google/android/youtube/core/model/SocialSettings$Builder;

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/core/utils/ae;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/SocialSettings$Builder;

    const-string v1, "autoShare"

    invoke-interface {p2, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/SocialSettings$Builder;->autoSharing(Z)Lcom/google/android/youtube/core/model/SocialSettings$Builder;

    return-void
.end method
