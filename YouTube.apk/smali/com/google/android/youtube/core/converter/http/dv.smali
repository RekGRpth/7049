.class public final Lcom/google/android/youtube/core/converter/http/dv;
.super Lcom/google/android/youtube/core/converter/http/hh;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/converter/b;


# static fields
.field private static final c:I


# instance fields
.field private final d:Lcom/google/android/youtube/core/converter/d;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/high16 v0, 0x40a00000

    const/high16 v1, 0x447a0000

    mul-float/2addr v0, v1

    float-to-int v0, v0

    sput v0, Lcom/google/android/youtube/core/converter/http/dv;->c:I

    return-void
.end method

.method public constructor <init>(Lcom/google/android/youtube/core/converter/m;)V
    .locals 4

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/converter/http/hh;-><init>(Lcom/google/android/youtube/core/converter/m;)V

    new-instance v0, Lcom/google/android/youtube/core/converter/e;

    invoke-direct {v0}, Lcom/google/android/youtube/core/converter/e;-><init>()V

    const-string v1, "/transcript"

    new-instance v2, Lcom/google/android/youtube/core/converter/http/dx;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/core/converter/http/dx;-><init>(Lcom/google/android/youtube/core/converter/http/dv;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v1

    const-string v2, "/transcript/text"

    new-instance v3, Lcom/google/android/youtube/core/converter/http/dw;

    invoke-direct {v3, p0}, Lcom/google/android/youtube/core/converter/http/dw;-><init>(Lcom/google/android/youtube/core/converter/http/dv;)V

    invoke-virtual {v1, v2, v3}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    const-string v1, "/timedtext"

    new-instance v2, Lcom/google/android/youtube/core/converter/http/ea;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/core/converter/http/ea;-><init>(Lcom/google/android/youtube/core/converter/http/dv;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v1

    const-string v2, "/timedtext/window"

    new-instance v3, Lcom/google/android/youtube/core/converter/http/dz;

    invoke-direct {v3, p0}, Lcom/google/android/youtube/core/converter/http/dz;-><init>(Lcom/google/android/youtube/core/converter/http/dv;)V

    invoke-virtual {v1, v2, v3}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v1

    const-string v2, "/timedtext/text"

    new-instance v3, Lcom/google/android/youtube/core/converter/http/dy;

    invoke-direct {v3, p0}, Lcom/google/android/youtube/core/converter/http/dy;-><init>(Lcom/google/android/youtube/core/converter/http/dv;)V

    invoke-virtual {v1, v2, v3}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/converter/e;->a()Lcom/google/android/youtube/core/converter/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/converter/http/dv;->d:Lcom/google/android/youtube/core/converter/d;

    return-void
.end method

.method static synthetic a(F)I
    .locals 1

    const/high16 v0, 0x447a0000

    mul-float/2addr v0, p0

    float-to-int v0, v0

    return v0
.end method

.method static synthetic a(I)I
    .locals 1

    const/16 v0, 0x22

    packed-switch p0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    const/16 v0, 0x9

    goto :goto_0

    :pswitch_2
    const/16 v0, 0xa

    goto :goto_0

    :pswitch_3
    const/16 v0, 0xc

    goto :goto_0

    :pswitch_4
    const/16 v0, 0x11

    goto :goto_0

    :pswitch_5
    const/16 v0, 0x12

    goto :goto_0

    :pswitch_6
    const/16 v0, 0x14

    goto :goto_0

    :pswitch_7
    const/16 v0, 0x21

    goto :goto_0

    :pswitch_8
    const/16 v0, 0x24

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_8
    .end packed-switch
.end method

.method static synthetic a(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_1

    aget-object v1, p1, v0

    invoke-interface {p0, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    move-object v0, v1

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/StringBuilder;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://video.google.com/timedtext?hl="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&v="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&type=track&lang="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p2}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&format="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b()I
    .locals 1

    sget v0, Lcom/google/android/youtube/core/converter/http/dv;->c:I

    return v0
.end method


# virtual methods
.method protected final a()Lcom/google/android/youtube/core/converter/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/converter/http/dv;->d:Lcom/google/android/youtube/core/converter/d;

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    check-cast p1, Lcom/google/android/youtube/core/model/SubtitleTrack;

    iget-object v0, p1, Lcom/google/android/youtube/core/model/SubtitleTrack;->videoId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/SubtitleTrack;->isAutoTranslated()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/google/android/youtube/core/model/SubtitleTrack;->sourceLanguageCode:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/youtube/core/model/SubtitleTrack;->trackName:Ljava/lang/String;

    iget v3, p1, Lcom/google/android/youtube/core/model/SubtitleTrack;->format:I

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/youtube/core/converter/http/dv;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&tlang="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/youtube/core/model/SubtitleTrack;->languageCode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    return-object v1

    :cond_0
    iget-object v1, p1, Lcom/google/android/youtube/core/model/SubtitleTrack;->languageCode:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/youtube/core/model/SubtitleTrack;->trackName:Ljava/lang/String;

    iget v3, p1, Lcom/google/android/youtube/core/model/SubtitleTrack;->format:I

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/youtube/core/converter/http/dv;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/StringBuilder;

    move-result-object v0

    goto :goto_0
.end method
