.class final Lcom/google/android/youtube/core/converter/http/dz;
.super Lcom/google/android/youtube/core/converter/n;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/converter/http/dv;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/core/converter/http/dv;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/converter/http/dz;->a:Lcom/google/android/youtube/core/converter/http/dv;

    invoke-direct {p0}, Lcom/google/android/youtube/core/converter/n;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/utils/ae;Lorg/xml/sax/Attributes;Ljava/lang/String;)V
    .locals 11

    const/16 v10, 0x64

    const/4 v3, 0x2

    const/4 v4, 0x1

    const/4 v9, 0x0

    const-class v0, Lcom/google/android/youtube/core/model/ab;

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/core/utils/ae;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/ab;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "w"

    aput-object v2, v1, v9

    const-string v2, "win"

    aput-object v2, v1, v4

    const-string v2, "id"

    aput-object v2, v1, v3

    invoke-static {p2, v1}, Lcom/google/android/youtube/core/converter/http/dv;->a(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v9}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;I)I

    move-result v5

    new-array v1, v3, [Ljava/lang/String;

    const-string v2, "t"

    aput-object v2, v1, v9

    const-string v2, "start"

    aput-object v2, v1, v4

    invoke-static {p2, v1}, Lcom/google/android/youtube/core/converter/http/dv;->a(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v6

    const-string v1, "op"

    invoke-interface {p2, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    const-string v2, "define"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const/16 v1, 0x22

    const/16 v2, 0x32

    const/16 v3, 0x5f

    new-array v7, v4, [Ljava/lang/String;

    const-string v8, "ap"

    aput-object v8, v7, v9

    invoke-static {p2, v7}, Lcom/google/android/youtube/core/converter/http/dv;->a(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/youtube/core/converter/http/dv;->a(I)I

    move-result v1

    :cond_0
    new-array v7, v4, [Ljava/lang/String;

    const-string v8, "ah"

    aput-object v8, v7, v9

    invoke-static {p2, v7}, Lcom/google/android/youtube/core/converter/http/dv;->a(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2, v10}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v9, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    :cond_1
    new-array v7, v4, [Ljava/lang/String;

    const-string v8, "av"

    aput-object v8, v7, v9

    invoke-static {p2, v7}, Lcom/google/android/youtube/core/converter/http/dv;->a(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_2

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v3, v10}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-static {v9, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    :cond_2
    new-array v7, v4, [Ljava/lang/String;

    const-string v8, "vs"

    aput-object v8, v7, v9

    invoke-static {p2, v7}, Lcom/google/android/youtube/core/converter/http/dv;->a(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_3

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    :cond_3
    new-instance v7, Lcom/google/android/youtube/core/model/SubtitleWindowSettings;

    invoke-direct {v7, v1, v2, v3, v4}, Lcom/google/android/youtube/core/model/SubtitleWindowSettings;-><init>(IIIZ)V

    invoke-virtual {v0, v5, v6, v7}, Lcom/google/android/youtube/core/model/ab;->a(IILcom/google/android/youtube/core/model/SubtitleWindowSettings;)Lcom/google/android/youtube/core/model/ab;

    :cond_4
    return-void
.end method
