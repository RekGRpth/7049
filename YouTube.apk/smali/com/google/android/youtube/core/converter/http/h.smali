.class public final Lcom/google/android/youtube/core/converter/http/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/converter/c;


# instance fields
.field private final a:I

.field private final b:Z

.field private final c:Z

.field private final d:Landroid/graphics/Bitmap$Config;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/converter/http/h;-><init>(Z)V

    return-void
.end method

.method public constructor <init>(IZZLandroid/graphics/Bitmap$Config;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-lez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "desiredWidth must be > 0"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    iput p1, p0, Lcom/google/android/youtube/core/converter/http/h;->a:I

    iput-boolean p2, p0, Lcom/google/android/youtube/core/converter/http/h;->b:Z

    iput-boolean p3, p0, Lcom/google/android/youtube/core/converter/http/h;->c:Z

    iput-object p4, p0, Lcom/google/android/youtube/core/converter/http/h;->d:Landroid/graphics/Bitmap$Config;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/google/android/youtube/core/converter/http/h;->a:I

    iput-boolean v0, p0, Lcom/google/android/youtube/core/converter/http/h;->b:Z

    iput-boolean p1, p0, Lcom/google/android/youtube/core/converter/http/h;->c:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/converter/http/h;->d:Landroid/graphics/Bitmap$Config;

    return-void
.end method

.method private static a(II)I
    .locals 1

    const/4 v0, 0x1

    :goto_0
    shr-int/lit8 p1, p1, 0x1

    if-lt p1, p0, :cond_0

    shl-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return v0
.end method


# virtual methods
.method public final synthetic a_(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v5, 0x0

    check-cast p1, [B

    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-boolean v1, v3, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    array-length v0, p1

    invoke-static {p1, v5, v0, v3}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    iget v0, v3, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-gez v0, :cond_1

    move-object v0, v2

    :cond_0
    :goto_0
    if-nez v0, :cond_4

    new-instance v0, Lcom/google/android/youtube/core/converter/ConverterException;

    const-string v1, "failed to decode bitmap"

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/converter/ConverterException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget v0, p0, Lcom/google/android/youtube/core/converter/http/h;->a:I

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    iget-boolean v4, p0, Lcom/google/android/youtube/core/converter/http/h;->b:Z

    if-nez v4, :cond_3

    iget-boolean v4, p0, Lcom/google/android/youtube/core/converter/http/h;->c:Z

    if-nez v4, :cond_3

    if-ne v0, v1, :cond_3

    iget-object v1, p0, Lcom/google/android/youtube/core/converter/http/h;->d:Landroid/graphics/Bitmap$Config;

    if-nez v1, :cond_3

    array-length v0, p1

    invoke-static {p1, v5, v0, v2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/google/android/youtube/core/converter/http/h;->a:I

    iget v4, v3, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    invoke-static {v0, v4}, Lcom/google/android/youtube/core/converter/http/h;->a(II)I

    move-result v0

    goto :goto_1

    :cond_3
    iput v0, v3, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    iput-boolean v5, v3, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    iget-object v0, p0, Lcom/google/android/youtube/core/converter/http/h;->d:Landroid/graphics/Bitmap$Config;

    iput-object v0, v3, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    iget-boolean v0, p0, Lcom/google/android/youtube/core/converter/http/h;->c:Z

    iput-boolean v0, v3, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    iput-boolean v5, v3, Landroid/graphics/BitmapFactory$Options;->inInputShareable:Z

    array-length v0, p1

    invoke-static {p1, v5, v0, v3}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/youtube/core/converter/http/h;->b:Z

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    iget v1, v3, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    int-to-float v1, v1

    const/high16 v2, 0x3f100000

    mul-float/2addr v1, v2

    iget v2, v3, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    int-to-float v2, v2

    sub-float v1, v2, v1

    const/high16 v2, 0x40000000

    div-float/2addr v1, v2

    invoke-static {v1}, Landroid/util/FloatMath;->ceil(F)F

    move-result v1

    float-to-int v1, v1

    if-lez v1, :cond_0

    iget v2, v3, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    mul-int/lit8 v4, v1, 0x2

    sub-int/2addr v2, v4

    if-lez v2, :cond_0

    iget v3, v3, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    invoke-static {v0, v5, v1, v3, v2}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    move-object v0, v1

    goto :goto_0

    :cond_4
    return-object v0
.end method
