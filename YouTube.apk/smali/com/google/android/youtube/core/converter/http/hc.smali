.class public final Lcom/google/android/youtube/core/converter/http/hc;
.super Lcom/google/android/youtube/core/converter/http/hh;
.source "SourceFile"


# instance fields
.field private final c:Lcom/google/android/youtube/core/converter/d;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/converter/m;Lcom/google/android/youtube/core/utils/e;)V
    .locals 3

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/converter/http/hh;-><init>(Lcom/google/android/youtube/core/converter/m;)V

    const-string v0, "clock cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/youtube/core/converter/e;

    invoke-direct {v0}, Lcom/google/android/youtube/core/converter/e;-><init>()V

    const-string v1, "/VMAP"

    invoke-static {v1}, Lcom/google/android/youtube/core/converter/http/hc;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/core/converter/http/hd;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/core/converter/http/hd;-><init>(Lcom/google/android/youtube/core/converter/http/hc;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    const-string v1, "/VMAP/AdBreak"

    invoke-static {v1}, Lcom/google/android/youtube/core/converter/http/hc;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/core/converter/http/he;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/core/converter/http/he;-><init>(Lcom/google/android/youtube/core/converter/http/hc;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    const-string v1, "/VMAP/AdBreak/TrackingEvents/Tracking"

    invoke-static {v1}, Lcom/google/android/youtube/core/converter/http/hc;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/core/converter/http/hf;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/core/converter/http/hf;-><init>(Lcom/google/android/youtube/core/converter/http/hc;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    const-string v1, "/VMAP/AdBreak/AdSource/VASTData"

    invoke-static {v1}, Lcom/google/android/youtube/core/converter/http/hc;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/core/converter/http/hg;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/core/converter/http/hg;-><init>(Lcom/google/android/youtube/core/converter/http/hc;)V

    invoke-static {v1, p2, v0, v2}, Lcom/google/android/youtube/core/converter/http/ex;->a(Ljava/lang/String;Lcom/google/android/youtube/core/utils/e;Lcom/google/android/youtube/core/converter/e;Lcom/google/android/youtube/core/converter/http/fn;)V

    invoke-virtual {v0}, Lcom/google/android/youtube/core/converter/e;->a()Lcom/google/android/youtube/core/converter/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/converter/http/hc;->c:Lcom/google/android/youtube/core/converter/d;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/core/converter/http/hc;Ljava/lang/String;)Lcom/google/android/youtube/core/model/VmapAdBreak$TrackingEventType;
    .locals 1

    const-string v0, "breakEnd"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/youtube/core/model/VmapAdBreak$TrackingEventType;->END:Lcom/google/android/youtube/core/model/VmapAdBreak$TrackingEventType;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "error"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/youtube/core/model/VmapAdBreak$TrackingEventType;->ERROR:Lcom/google/android/youtube/core/model/VmapAdBreak$TrackingEventType;

    goto :goto_0

    :cond_1
    const-string v0, "breakStart"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    sget-object v0, Lcom/google/android/youtube/core/model/VmapAdBreak$TrackingEventType;->START:Lcom/google/android/youtube/core/model/VmapAdBreak$TrackingEventType;

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const-string v0, "\\/"

    const-string v1, "/vmap:"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/converter/http/hc;Ljava/lang/String;Lcom/google/android/youtube/core/model/am;)V
    .locals 5

    const/16 v4, 0x64

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-nez p1, :cond_1

    const-string v0, "in Vmap AdBreak: timeOffset is null"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;->UNKNOWN:Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;

    invoke-virtual {p2, v0}, Lcom/google/android/youtube/core/model/am;->a(Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;)Lcom/google/android/youtube/core/model/am;

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "in Vmap AdBreak: timeOffset is empty string"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;->UNKNOWN:Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;

    invoke-virtual {p2, v0}, Lcom/google/android/youtube/core/model/am;->a(Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;)Lcom/google/android/youtube/core/model/am;

    goto :goto_0

    :cond_2
    const-string v0, "start"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;->PRE_ROLL:Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;

    invoke-virtual {p2, v0}, Lcom/google/android/youtube/core/model/am;->a(Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;)Lcom/google/android/youtube/core/model/am;

    goto :goto_0

    :cond_3
    const-string v0, "end"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;->POST_ROLL:Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;

    invoke-virtual {p2, v0}, Lcom/google/android/youtube/core/model/am;->a(Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;)Lcom/google/android/youtube/core/model/am;

    goto :goto_0

    :cond_4
    const/16 v0, 0x23

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v1

    if-ne v0, v1, :cond_5

    sget-object v0, Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;->POSITIONAL:Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;

    invoke-virtual {p2, v0}, Lcom/google/android/youtube/core/model/am;->a(Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;)Lcom/google/android/youtube/core/model/am;

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/google/android/youtube/core/model/am;->a(I)Lcom/google/android/youtube/core/model/am;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    invoke-virtual {p2}, Lcom/google/android/youtube/core/model/am;->a()I

    move-result v0

    if-gtz v0, :cond_0

    const-string v0, "in Vmap AdBreak(positional): timeOffset must be >= 1"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    invoke-virtual {p2, v2}, Lcom/google/android/youtube/core/model/am;->a(I)Lcom/google/android/youtube/core/model/am;

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "in Vmap AdBreak(positional): integer parse error"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {p2, v2}, Lcom/google/android/youtube/core/model/am;->a(I)Lcom/google/android/youtube/core/model/am;

    goto :goto_1

    :cond_5
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x25

    add-int/lit8 v2, v0, -0x1

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    if-ne v1, v2, :cond_7

    sget-object v1, Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;->PERCENTAGE:Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;

    invoke-virtual {p2, v1}, Lcom/google/android/youtube/core/model/am;->a(Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;)Lcom/google/android/youtube/core/model/am;

    const/4 v1, 0x0

    add-int/lit8 v0, v0, -0x1

    :try_start_1
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/google/android/youtube/core/model/am;->a(I)Lcom/google/android/youtube/core/model/am;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    invoke-virtual {p2}, Lcom/google/android/youtube/core/model/am;->a()I

    move-result v0

    if-gez v0, :cond_6

    const-string v0, "in Vmap AdBreak(percentage): value must not be <0"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    invoke-virtual {p2, v3}, Lcom/google/android/youtube/core/model/am;->a(I)Lcom/google/android/youtube/core/model/am;

    :cond_6
    invoke-virtual {p2}, Lcom/google/android/youtube/core/model/am;->a()I

    move-result v0

    if-le v0, v4, :cond_0

    const-string v0, "in Vmap AdBreak(percentage): value must not be >100"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    invoke-virtual {p2, v4}, Lcom/google/android/youtube/core/model/am;->a(I)Lcom/google/android/youtube/core/model/am;

    goto/16 :goto_0

    :catch_1
    move-exception v0

    const-string v1, "in Vmap AdBreak(percentage): integer parse error"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {p2, v3}, Lcom/google/android/youtube/core/model/am;->a(I)Lcom/google/android/youtube/core/model/am;

    goto :goto_2

    :cond_7
    sget-object v0, Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;->TIME:Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;

    invoke-virtual {p2, v0}, Lcom/google/android/youtube/core/model/am;->a(Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;)Lcom/google/android/youtube/core/model/am;

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/ah;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/google/android/youtube/core/model/am;->a(I)Lcom/google/android/youtube/core/model/am;

    invoke-virtual {p2}, Lcom/google/android/youtube/core/model/am;->a()I

    move-result v0

    if-gez v0, :cond_0

    const-string v0, "in Vmap AdBreak(time): value must not be <00:00:00.000"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    invoke-virtual {p2, v3}, Lcom/google/android/youtube/core/model/am;->a(I)Lcom/google/android/youtube/core/model/am;

    goto/16 :goto_0
.end method

.method private static a(Ljava/lang/String;Lcom/google/android/youtube/core/model/am;)V
    .locals 2

    const/4 v1, 0x1

    const-string v0, "linear"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1, v1}, Lcom/google/android/youtube/core/model/am;->a(Z)Lcom/google/android/youtube/core/model/am;

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "nonlinear"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1, v1}, Lcom/google/android/youtube/core/model/am;->b(Z)Lcom/google/android/youtube/core/model/am;

    goto :goto_0

    :cond_2
    const-string v0, "display"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v1}, Lcom/google/android/youtube/core/model/am;->c(Z)Lcom/google/android/youtube/core/model/am;

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/youtube/core/converter/http/hc;Ljava/lang/String;Lcom/google/android/youtube/core/model/am;)V
    .locals 4

    if-nez p1, :cond_1

    const-string v0, "in Vmap AdBreak: timeOffset is null"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, ","

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v0, v1

    const/4 v2, 0x1

    if-le v0, v2, :cond_2

    array-length v2, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-static {v3, p2}, Lcom/google/android/youtube/core/converter/http/hc;->a(Ljava/lang/String;Lcom/google/android/youtube/core/model/am;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    invoke-static {p1, p2}, Lcom/google/android/youtube/core/converter/http/hc;->a(Ljava/lang/String;Lcom/google/android/youtube/core/model/am;)V

    goto :goto_0
.end method


# virtual methods
.method protected final a()Lcom/google/android/youtube/core/converter/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/converter/http/hc;->c:Lcom/google/android/youtube/core/converter/d;

    return-object v0
.end method
