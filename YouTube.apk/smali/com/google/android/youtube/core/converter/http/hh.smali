.class public abstract Lcom/google/android/youtube/core/converter/http/hh;
.super Lcom/google/android/youtube/core/converter/http/bn;
.source "SourceFile"


# instance fields
.field protected final a:Lcom/google/android/youtube/core/converter/m;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/converter/m;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/core/converter/http/bn;-><init>()V

    const-string v0, "the parser can\'t be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/converter/m;

    iput-object v0, p0, Lcom/google/android/youtube/core/converter/http/hh;->a:Lcom/google/android/youtube/core/converter/m;

    return-void
.end method


# virtual methods
.method protected abstract a()Lcom/google/android/youtube/core/converter/d;
.end method

.method protected final a(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/converter/http/hh;->a:Lcom/google/android/youtube/core/converter/m;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/converter/http/hh;->a()Lcom/google/android/youtube/core/converter/d;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/youtube/core/converter/m;->a(Ljava/io/InputStream;Lcom/google/android/youtube/core/converter/d;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/i;

    :try_start_0
    invoke-interface {v0}, Lcom/google/android/youtube/core/model/i;->build()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/youtube/core/converter/ConverterException;

    invoke-direct {v1, v0}, Lcom/google/android/youtube/core/converter/ConverterException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
