.class public final Lcom/google/android/youtube/core/converter/http/i;
.super Lcom/google/android/youtube/core/converter/http/bn;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/youtube/core/converter/m;

.field private final c:Lcom/google/android/youtube/core/converter/d;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/converter/m;)V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/youtube/core/converter/http/bn;-><init>()V

    const-string v0, "the parser can\'t be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/converter/m;

    iput-object v0, p0, Lcom/google/android/youtube/core/converter/http/i;->a:Lcom/google/android/youtube/core/converter/m;

    new-instance v0, Lcom/google/android/youtube/core/converter/e;

    invoke-direct {v0}, Lcom/google/android/youtube/core/converter/e;-><init>()V

    const-string v1, "/app:categories"

    new-instance v2, Lcom/google/android/youtube/core/converter/http/m;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/core/converter/http/m;-><init>(Lcom/google/android/youtube/core/converter/http/i;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    const-string v1, "/app:categories/atom:category"

    new-instance v2, Lcom/google/android/youtube/core/converter/http/l;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/core/converter/http/l;-><init>(Lcom/google/android/youtube/core/converter/http/i;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    const-string v1, "/app:categories/atom:category/yt:browsable"

    new-instance v2, Lcom/google/android/youtube/core/converter/http/k;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/core/converter/http/k;-><init>(Lcom/google/android/youtube/core/converter/http/i;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    const-string v1, "/app:categories/atom:category/yt:deprecated"

    new-instance v2, Lcom/google/android/youtube/core/converter/http/j;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/core/converter/http/j;-><init>(Lcom/google/android/youtube/core/converter/http/i;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/converter/e;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/q;)Lcom/google/android/youtube/core/converter/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/converter/e;->a()Lcom/google/android/youtube/core/converter/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/converter/http/i;->c:Lcom/google/android/youtube/core/converter/d;

    return-void
.end method

.method public static a(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;
    .locals 6

    sget-object v0, Lcom/google/android/youtube/core/async/GDataRequestFactory;->t:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Category;

    iget-object v4, v0, Lcom/google/android/youtube/core/model/Category;->regions:Ljava/lang/String;

    iget-boolean v5, v0, Lcom/google/android/youtube/core/model/Category;->deprecated:Z

    if-nez v5, :cond_0

    if-eqz v4, :cond_0

    if-eqz v1, :cond_1

    invoke-virtual {v4, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    :cond_1
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-object v2
.end method


# virtual methods
.method protected final synthetic a(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/converter/http/i;->a:Lcom/google/android/youtube/core/converter/m;

    iget-object v1, p0, Lcom/google/android/youtube/core/converter/http/i;->c:Lcom/google/android/youtube/core/converter/d;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/youtube/core/converter/m;->a(Ljava/io/InputStream;Lcom/google/android/youtube/core/converter/d;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    new-instance v1, Lcom/google/android/youtube/core/converter/http/n;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/core/converter/http/n;-><init>(Lcom/google/android/youtube/core/converter/http/i;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-object v0
.end method
