.class public final Lcom/google/android/youtube/core/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/Analytics;


# instance fields
.field private final a:Lcom/google/android/youtube/core/client/e;

.field private final b:Lcom/google/android/youtube/core/utils/p;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/client/e;Lcom/google/android/youtube/core/utils/p;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "analyticsClient cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/client/e;

    iput-object v0, p0, Lcom/google/android/youtube/core/d;->a:Lcom/google/android/youtube/core/client/e;

    const-string v0, "networkStatus cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/utils/p;

    iput-object v0, p0, Lcom/google/android/youtube/core/d;->b:Lcom/google/android/youtube/core/utils/p;

    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/String;Ljava/lang/String;I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/d;->a:Lcom/google/android/youtube/core/client/e;

    const/4 v1, 0x2

    invoke-interface {v0, p1, p2, p3, v1}, Lcom/google/android/youtube/core/client/e;->a(ILjava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/Analytics$VideoCategory;I)V
    .locals 2

    const-string v0, "PlaySelected"

    invoke-virtual {p1}, Lcom/google/android/youtube/core/Analytics$VideoCategory;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, p2}, Lcom/google/android/youtube/core/d;->a(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    iget-object v1, p0, Lcom/google/android/youtube/core/d;->a:Lcom/google/android/youtube/core/client/e;

    const-string v2, "PageView"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " < "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/youtube/core/d;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "Entry"

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v3, -0x1

    invoke-interface {v1, v2, v0, v3}, Lcom/google/android/youtube/core/client/e;->a(Ljava/lang/String;Ljava/lang/String;I)V

    iput-object p1, p0, Lcom/google/android/youtube/core/d;->c:Ljava/lang/String;

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/d;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/4 v0, -0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/youtube/core/d;->a(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/d;->a:Lcom/google/android/youtube/core/client/e;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/youtube/core/client/e;->a(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    const/4 v0, -0x1

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/google/android/youtube/core/d;->a(Ljava/lang/String;Ljava/lang/String;ZI)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;ZI)V
    .locals 2

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p2, "?"

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/d;->b:Lcom/google/android/youtube/core/utils/p;

    invoke-interface {v1}, Lcom/google/android/youtube/core/utils/p;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz p3, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",offline"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_1
    invoke-virtual {p0, p1, v0, p4}, Lcom/google/android/youtube/core/d;->a(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/youtube/core/d;->a(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method
