.class public Lcom/google/android/youtube/core/model/Branding$Builder;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/model/i;
.implements Ljava/io/Serializable;


# instance fields
.field private bannerTargetUri:Landroid/net/Uri;

.field private bannerUri:Landroid/net/Uri;

.field private channelBannerMobileExtraHdUri:Landroid/net/Uri;

.field private channelBannerMobileHdUri:Landroid/net/Uri;

.field private channelBannerMobileLowUri:Landroid/net/Uri;

.field private channelBannerMobileMediumHdUri:Landroid/net/Uri;

.field private channelBannerMobileMediumUri:Landroid/net/Uri;

.field private channelBannerTabletExtraHdUri:Landroid/net/Uri;

.field private channelBannerTabletHdUri:Landroid/net/Uri;

.field private channelBannerTabletLowUri:Landroid/net/Uri;

.field private channelBannerTabletMediumUri:Landroid/net/Uri;

.field private channelBannerTvUri:Landroid/net/Uri;

.field private description:Ljava/lang/String;

.field private featuredPlaylistId:Ljava/lang/String;

.field private interstitialTargetUri:Landroid/net/Uri;

.field private interstitialUri:Landroid/net/Uri;

.field private keywords:Ljava/lang/String;

.field private largeBannerUri:Landroid/net/Uri;

.field private title:Ljava/lang/String;

.field private tvBannerUri:Landroid/net/Uri;

.field private watermarkTargetUri:Landroid/net/Uri;

.field private watermarkUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 1

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->title:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->description:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->keywords:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->bannerUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->largeBannerUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->bannerTargetUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->watermarkUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->watermarkTargetUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->interstitialUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->interstitialTargetUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->featuredPlaylistId:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->tvBannerUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerMobileLowUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerMobileMediumUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerMobileMediumHdUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerMobileHdUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerMobileExtraHdUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerTabletLowUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerTabletMediumUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerTabletHdUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerTabletExtraHdUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerTvUri:Landroid/net/Uri;

    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/Branding$Builder;->build()Lcom/google/android/youtube/core/model/Branding;

    move-result-object v0

    return-object v0
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->title:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->description:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->keywords:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->bannerUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->largeBannerUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->bannerTargetUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->watermarkUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->watermarkTargetUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->interstitialUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->interstitialTargetUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->featuredPlaylistId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->tvBannerUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerMobileLowUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerMobileMediumUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerMobileMediumHdUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerMobileHdUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerMobileExtraHdUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerTabletLowUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerTabletMediumUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerTabletHdUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerTabletExtraHdUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerTvUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public bannerTargetUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Branding$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->bannerTargetUri:Landroid/net/Uri;

    return-object p0
.end method

.method public bannerUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Branding$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->bannerUri:Landroid/net/Uri;

    return-object p0
.end method

.method public build()Lcom/google/android/youtube/core/model/Branding;
    .locals 24

    new-instance v1, Lcom/google/android/youtube/core/model/Branding;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/core/model/Branding$Builder;->title:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/core/model/Branding$Builder;->description:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/core/model/Branding$Builder;->keywords:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/core/model/Branding$Builder;->bannerUri:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/core/model/Branding$Builder;->largeBannerUri:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/youtube/core/model/Branding$Builder;->bannerTargetUri:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/youtube/core/model/Branding$Builder;->watermarkUri:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/youtube/core/model/Branding$Builder;->watermarkTargetUri:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/youtube/core/model/Branding$Builder;->interstitialUri:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/youtube/core/model/Branding$Builder;->interstitialTargetUri:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/youtube/core/model/Branding$Builder;->featuredPlaylistId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/youtube/core/model/Branding$Builder;->tvBannerUri:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerMobileLowUri:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerMobileMediumUri:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerMobileMediumHdUri:Landroid/net/Uri;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerMobileHdUri:Landroid/net/Uri;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerMobileExtraHdUri:Landroid/net/Uri;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerTabletLowUri:Landroid/net/Uri;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerTabletMediumUri:Landroid/net/Uri;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerTabletHdUri:Landroid/net/Uri;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerTabletExtraHdUri:Landroid/net/Uri;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerTvUri:Landroid/net/Uri;

    move-object/from16 v23, v0

    invoke-direct/range {v1 .. v23}, Lcom/google/android/youtube/core/model/Branding;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;)V

    return-object v1
.end method

.method public bridge synthetic build()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/Branding$Builder;->build()Lcom/google/android/youtube/core/model/Branding;

    move-result-object v0

    return-object v0
.end method

.method public channelBannerMobileExtraHdUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Branding$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerMobileExtraHdUri:Landroid/net/Uri;

    return-object p0
.end method

.method public channelBannerMobileHdUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Branding$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerMobileHdUri:Landroid/net/Uri;

    return-object p0
.end method

.method public channelBannerMobileLowUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Branding$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerMobileLowUri:Landroid/net/Uri;

    return-object p0
.end method

.method public channelBannerMobileMediumHdUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Branding$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerMobileMediumHdUri:Landroid/net/Uri;

    return-object p0
.end method

.method public channelBannerMobileMediumUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Branding$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerMobileMediumUri:Landroid/net/Uri;

    return-object p0
.end method

.method public channelBannerTabletExtraHdUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Branding$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerTabletExtraHdUri:Landroid/net/Uri;

    return-object p0
.end method

.method public channelBannerTabletHdUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Branding$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerTabletHdUri:Landroid/net/Uri;

    return-object p0
.end method

.method public channelBannerTabletLowUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Branding$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerTabletLowUri:Landroid/net/Uri;

    return-object p0
.end method

.method public channelBannerTabletMediumUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Branding$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerTabletMediumUri:Landroid/net/Uri;

    return-object p0
.end method

.method public channelBannerTvUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Branding$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->channelBannerTvUri:Landroid/net/Uri;

    return-object p0
.end method

.method public description(Ljava/lang/String;)Lcom/google/android/youtube/core/model/Branding$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->description:Ljava/lang/String;

    return-object p0
.end method

.method public featuredPlaylistId(Ljava/lang/String;)Lcom/google/android/youtube/core/model/Branding$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->featuredPlaylistId:Ljava/lang/String;

    return-object p0
.end method

.method public interstitialTargetUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Branding$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->interstitialTargetUri:Landroid/net/Uri;

    return-object p0
.end method

.method public interstitialUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Branding$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->interstitialUri:Landroid/net/Uri;

    return-object p0
.end method

.method public keywords(Ljava/lang/String;)Lcom/google/android/youtube/core/model/Branding$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->keywords:Ljava/lang/String;

    return-object p0
.end method

.method public largeBannerUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Branding$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->largeBannerUri:Landroid/net/Uri;

    return-object p0
.end method

.method public title(Ljava/lang/String;)Lcom/google/android/youtube/core/model/Branding$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->title:Ljava/lang/String;

    return-object p0
.end method

.method public tvBannerUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Branding$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->tvBannerUri:Landroid/net/Uri;

    return-object p0
.end method

.method public watermarkTargetUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Branding$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->watermarkTargetUri:Landroid/net/Uri;

    return-object p0
.end method

.method public watermarkUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Branding$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/Branding$Builder;->watermarkUri:Landroid/net/Uri;

    return-object p0
.end method
