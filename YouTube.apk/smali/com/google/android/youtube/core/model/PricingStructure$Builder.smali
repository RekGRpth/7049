.class public final Lcom/google/android/youtube/core/model/PricingStructure$Builder;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private duration:Lcom/google/android/youtube/core/model/Duration;

.field private formats:Ljava/util/List;

.field private infoUri:Landroid/net/Uri;

.field private offerId:Ljava/lang/String;

.field private price:Lcom/google/android/youtube/core/model/Money;

.field private type:Lcom/google/android/youtube/core/model/PricingStructure$Type;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 1

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/PricingStructure$Type;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->type:Lcom/google/android/youtube/core/model/PricingStructure$Type;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Duration;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->duration:Lcom/google/android/youtube/core/model/Duration;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->infoUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Money;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->price:Lcom/google/android/youtube/core/model/Money;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->formats:Ljava/util/List;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->offerId:Ljava/lang/String;

    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->build()Lcom/google/android/youtube/core/model/PricingStructure;

    move-result-object v0

    return-object v0
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->type:Lcom/google/android/youtube/core/model/PricingStructure$Type;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->duration:Lcom/google/android/youtube/core/model/Duration;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->infoUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->price:Lcom/google/android/youtube/core/model/Money;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->formats:Ljava/util/List;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->offerId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final build()Lcom/google/android/youtube/core/model/PricingStructure;
    .locals 5

    iget-object v0, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->formats:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->formats:Ljava/util/List;

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->type:Lcom/google/android/youtube/core/model/PricingStructure$Type;

    sget-object v2, Lcom/google/android/youtube/core/model/PricingStructure$Type;->RENT:Lcom/google/android/youtube/core/model/PricingStructure$Type;

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->duration:Lcom/google/android/youtube/core/model/Duration;

    iget-object v2, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->price:Lcom/google/android/youtube/core/model/Money;

    iget-object v3, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->infoUri:Landroid/net/Uri;

    iget-object v4, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->offerId:Ljava/lang/String;

    invoke-static {v1, v2, v0, v3, v4}, Lcom/google/android/youtube/core/model/PricingStructure;->createRental(Lcom/google/android/youtube/core/model/Duration;Lcom/google/android/youtube/core/model/Money;Ljava/util/List;Landroid/net/Uri;Ljava/lang/String;)Lcom/google/android/youtube/core/model/PricingStructure;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->price:Lcom/google/android/youtube/core/model/Money;

    iget-object v2, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->infoUri:Landroid/net/Uri;

    iget-object v3, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->offerId:Ljava/lang/String;

    invoke-static {v1, v0, v2, v3}, Lcom/google/android/youtube/core/model/PricingStructure;->createPurchase(Lcom/google/android/youtube/core/model/Money;Ljava/util/List;Landroid/net/Uri;Ljava/lang/String;)Lcom/google/android/youtube/core/model/PricingStructure;

    move-result-object v0

    goto :goto_1
.end method

.method public final duration(Lcom/google/android/youtube/core/model/Duration;)Lcom/google/android/youtube/core/model/PricingStructure$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->duration:Lcom/google/android/youtube/core/model/Duration;

    return-object p0
.end method

.method public final formats(Ljava/util/List;)Lcom/google/android/youtube/core/model/PricingStructure$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->formats:Ljava/util/List;

    return-object p0
.end method

.method public final infoUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/PricingStructure$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->infoUri:Landroid/net/Uri;

    return-object p0
.end method

.method public final offerId(Ljava/lang/String;)Lcom/google/android/youtube/core/model/PricingStructure$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->offerId:Ljava/lang/String;

    return-object p0
.end method

.method public final price(Lcom/google/android/youtube/core/model/Money;)Lcom/google/android/youtube/core/model/PricingStructure$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->price:Lcom/google/android/youtube/core/model/Money;

    return-object p0
.end method

.method public final type(Lcom/google/android/youtube/core/model/PricingStructure$Type;)Lcom/google/android/youtube/core/model/PricingStructure$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->type:Lcom/google/android/youtube/core/model/PricingStructure$Type;

    return-object p0
.end method
