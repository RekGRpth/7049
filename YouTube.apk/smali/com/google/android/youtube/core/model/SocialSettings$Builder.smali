.class public final Lcom/google/android/youtube/core/model/SocialSettings$Builder;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/model/i;
.implements Ljava/io/Serializable;


# static fields
.field private static final ACTION_TO_ACTION_TYPE:Ljava/util/Map;

.field private static final STRING_TO_NETWORK_ID:Ljava/util/Map;


# instance fields
.field private actions:Ljava/util/Set;

.field private autoSharing:Z

.field private facebook:Lcom/google/android/youtube/core/model/k;

.field private id:Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;

.field private orkut:Lcom/google/android/youtube/core/model/k;

.field private twitter:Lcom/google/android/youtube/core/model/k;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v0, 0x0

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Lcom/google/android/youtube/core/model/SocialSettings$Builder;->ACTION_TO_ACTION_TYPE:Ljava/util/Map;

    invoke-static {}, Lcom/google/android/youtube/core/model/SocialSettings$Action$ActionType;->values()[Lcom/google/android/youtube/core/model/SocialSettings$Action$ActionType;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    sget-object v5, Lcom/google/android/youtube/core/model/SocialSettings$Builder;->ACTION_TO_ACTION_TYPE:Ljava/util/Map;

    iget-object v6, v4, Lcom/google/android/youtube/core/model/SocialSettings$Action$ActionType;->action:Ljava/lang/String;

    invoke-interface {v5, v6, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Lcom/google/android/youtube/core/model/SocialSettings$Builder;->STRING_TO_NETWORK_ID:Ljava/util/Map;

    invoke-static {}, Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;->values()[Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;

    move-result-object v1

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    sget-object v4, Lcom/google/android/youtube/core/model/SocialSettings$Builder;->STRING_TO_NETWORK_ID:Ljava/util/Map;

    iget-object v5, v3, Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;->id:Ljava/lang/String;

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final addAction(Ljava/lang/String;Z)Lcom/google/android/youtube/core/model/SocialSettings$Builder;
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/core/model/SocialSettings$Builder;->actions:Ljava/util/Set;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/SocialSettings$Builder;->actions:Ljava/util/Set;

    :cond_0
    sget-object v0, Lcom/google/android/youtube/core/model/SocialSettings$Builder;->ACTION_TO_ACTION_TYPE:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/SocialSettings$Action$ActionType;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/core/model/SocialSettings$Builder;->actions:Ljava/util/Set;

    new-instance v2, Lcom/google/android/youtube/core/model/SocialSettings$Action;

    invoke-direct {v2, v0, p2}, Lcom/google/android/youtube/core/model/SocialSettings$Action;-><init>(Lcom/google/android/youtube/core/model/SocialSettings$Action$ActionType;Z)V

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_1
    return-object p0
.end method

.method public final addNetwork()V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/core/model/SocialSettings$Builder;->id:Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/youtube/core/model/j;->a:[I

    iget-object v1, p0, Lcom/google/android/youtube/core/model/SocialSettings$Builder;->id:Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    new-instance v0, Lcom/google/android/youtube/core/model/k;

    iget-object v1, p0, Lcom/google/android/youtube/core/model/SocialSettings$Builder;->id:Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;

    iget-boolean v2, p0, Lcom/google/android/youtube/core/model/SocialSettings$Builder;->autoSharing:Z

    iget-object v3, p0, Lcom/google/android/youtube/core/model/SocialSettings$Builder;->actions:Ljava/util/Set;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/youtube/core/model/k;-><init>(Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;ZLjava/util/Set;B)V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/SocialSettings$Builder;->facebook:Lcom/google/android/youtube/core/model/k;

    goto :goto_0

    :pswitch_1
    new-instance v0, Lcom/google/android/youtube/core/model/k;

    iget-object v1, p0, Lcom/google/android/youtube/core/model/SocialSettings$Builder;->id:Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;

    iget-boolean v2, p0, Lcom/google/android/youtube/core/model/SocialSettings$Builder;->autoSharing:Z

    iget-object v3, p0, Lcom/google/android/youtube/core/model/SocialSettings$Builder;->actions:Ljava/util/Set;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/youtube/core/model/k;-><init>(Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;ZLjava/util/Set;B)V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/SocialSettings$Builder;->twitter:Lcom/google/android/youtube/core/model/k;

    goto :goto_0

    :pswitch_2
    new-instance v0, Lcom/google/android/youtube/core/model/k;

    iget-object v1, p0, Lcom/google/android/youtube/core/model/SocialSettings$Builder;->id:Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;

    iget-boolean v2, p0, Lcom/google/android/youtube/core/model/SocialSettings$Builder;->autoSharing:Z

    iget-object v3, p0, Lcom/google/android/youtube/core/model/SocialSettings$Builder;->actions:Ljava/util/Set;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/youtube/core/model/k;-><init>(Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;ZLjava/util/Set;B)V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/SocialSettings$Builder;->orkut:Lcom/google/android/youtube/core/model/k;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final autoSharing(Z)Lcom/google/android/youtube/core/model/SocialSettings$Builder;
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/core/model/SocialSettings$Builder;->autoSharing:Z

    return-object p0
.end method

.method public final build()Lcom/google/android/youtube/core/model/SocialSettings;
    .locals 4

    new-instance v0, Lcom/google/android/youtube/core/model/SocialSettings;

    iget-object v1, p0, Lcom/google/android/youtube/core/model/SocialSettings$Builder;->facebook:Lcom/google/android/youtube/core/model/k;

    iget-object v2, p0, Lcom/google/android/youtube/core/model/SocialSettings$Builder;->twitter:Lcom/google/android/youtube/core/model/k;

    iget-object v3, p0, Lcom/google/android/youtube/core/model/SocialSettings$Builder;->orkut:Lcom/google/android/youtube/core/model/k;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/youtube/core/model/SocialSettings;-><init>(Lcom/google/android/youtube/core/model/k;Lcom/google/android/youtube/core/model/k;Lcom/google/android/youtube/core/model/k;)V

    return-object v0
.end method

.method public final bridge synthetic build()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/SocialSettings$Builder;->build()Lcom/google/android/youtube/core/model/SocialSettings;

    move-result-object v0

    return-object v0
.end method

.method public final id(Ljava/lang/String;)Lcom/google/android/youtube/core/model/SocialSettings$Builder;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/core/model/SocialSettings$Builder;->STRING_TO_NETWORK_ID:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/SocialSettings$Builder;->id:Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;

    return-object p0
.end method
