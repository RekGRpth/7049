.class public final Lcom/google/android/youtube/core/model/UserAuth;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final account:Ljava/lang/String;

.field public final authMethod:Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;

.field public final authToken:Ljava/lang/String;

.field public final channelId:Ljava/lang/String;

.field public final delegateId:Ljava/lang/String;

.field private volatile hashCode:I

.field public final username:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/youtube/core/model/ac;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/ac;-><init>()V

    sput-object v0, Lcom/google/android/youtube/core/model/UserAuth;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/UserAuth;->account:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/UserAuth;->username:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/UserAuth;->channelId:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/UserAuth;->authMethod:Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/UserAuth;->authToken:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/UserAuth;->delegateId:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/youtube/core/model/ac;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/model/UserAuth;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/youtube/core/model/UserAuth;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p1, Lcom/google/android/youtube/core/model/UserAuth;->account:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/UserAuth;->account:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/android/youtube/core/model/UserAuth;->username:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/UserAuth;->username:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/android/youtube/core/model/UserAuth;->channelId:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/UserAuth;->channelId:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/android/youtube/core/model/UserAuth;->authMethod:Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/UserAuth;->authMethod:Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;

    iget-object v0, p1, Lcom/google/android/youtube/core/model/UserAuth;->authToken:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/UserAuth;->authToken:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/youtube/core/model/UserAuth;->delegateId:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;Ljava/lang/String;)V
    .locals 7

    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    move-object v4, p2

    move-object v5, p3

    move-object v6, v2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/core/model/UserAuth;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/UserAuth;->account:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/youtube/core/model/UserAuth;->username:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/youtube/core/model/UserAuth;->channelId:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/youtube/core/model/UserAuth;->authMethod:Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;

    invoke-static {p5}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/UserAuth;->authToken:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/youtube/core/model/UserAuth;->delegateId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final cloneWithAccountDetails(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/youtube/core/model/UserAuth;
    .locals 7

    new-instance v0, Lcom/google/android/youtube/core/model/UserAuth;

    iget-object v1, p0, Lcom/google/android/youtube/core/model/UserAuth;->account:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/youtube/core/model/UserAuth;->authMethod:Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;

    iget-object v5, p0, Lcom/google/android/youtube/core/model/UserAuth;->authToken:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/youtube/core/model/UserAuth;->delegateId:Ljava/lang/String;

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/core/model/UserAuth;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final cloneWithDelegate(Ljava/lang/String;)Lcom/google/android/youtube/core/model/UserAuth;
    .locals 1

    new-instance v0, Lcom/google/android/youtube/core/model/UserAuth;

    invoke-direct {v0, p0, p1}, Lcom/google/android/youtube/core/model/UserAuth;-><init>(Lcom/google/android/youtube/core/model/UserAuth;Ljava/lang/String;)V

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/google/android/youtube/core/model/UserAuth;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/google/android/youtube/core/model/UserAuth;

    iget-object v2, p0, Lcom/google/android/youtube/core/model/UserAuth;->account:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/youtube/core/model/UserAuth;->account:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/youtube/core/model/UserAuth;->delegateId:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/youtube/core/model/UserAuth;->delegateId:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    iget v0, p0, Lcom/google/android/youtube/core/model/UserAuth;->hashCode:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/model/UserAuth;->account:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/youtube/core/model/UserAuth;->delegateId:Ljava/lang/String;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/youtube/core/model/UserAuth;->hashCode:I

    :cond_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/model/UserAuth;->delegateId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "accountName=["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/core/model/UserAuth;->account:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/model/UserAuth;->account:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/UserAuth;->username:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/UserAuth;->channelId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/UserAuth;->authMethod:Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/UserAuth;->authToken:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/UserAuth;->delegateId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
