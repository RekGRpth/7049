.class public final Lcom/google/android/youtube/core/model/VmapAdList;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final DEFAULT_FREQ_CAP_SECS:I = 0x1a4


# instance fields
.field public final adBreaks:Ljava/util/List;

.field public final clientFreqCapSecs:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/youtube/core/model/ao;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/ao;-><init>()V

    sput-object v0, Lcom/google/android/youtube/core/model/VmapAdList;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(ILjava/util/List;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-ltz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "clientFreqCapSecs must be >= 0"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    iput p1, p0, Lcom/google/android/youtube/core/model/VmapAdList;->clientFreqCapSecs:I

    invoke-static {p2}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/VmapAdList;->adBreaks:Ljava/util/List;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method synthetic constructor <init>(ILjava/util/List;Lcom/google/android/youtube/core/model/ao;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/model/VmapAdList;-><init>(ILjava/util/List;)V

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {p1}, Lcom/google/android/youtube/core/model/VmapAdList;->readAdBreakList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/core/model/VmapAdList;-><init>(ILjava/util/List;)V

    return-void
.end method

.method private static readAdBreakList(Landroid/os/Parcel;)Ljava/util/List;
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sget-object v1, Lcom/google/android/youtube/core/model/VmapAdBreak;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p0, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    check-cast p1, Lcom/google/android/youtube/core/model/VmapAdList;

    iget v1, p0, Lcom/google/android/youtube/core/model/VmapAdList;->clientFreqCapSecs:I

    iget v2, p1, Lcom/google/android/youtube/core/model/VmapAdList;->clientFreqCapSecs:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/core/model/VmapAdList;->adBreaks:Ljava/util/List;

    iget-object v2, p1, Lcom/google/android/youtube/core/model/VmapAdList;->adBreaks:Ljava/util/List;

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final firstPrerollAd()Lcom/google/android/youtube/core/model/VastAd;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/VmapAdList;->firstPrerollAdList()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/VastAd;

    goto :goto_0
.end method

.method public final firstPrerollAdList()Ljava/util/List;
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/core/model/VmapAdList;->adBreaks:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/VmapAdBreak;

    iget-boolean v2, v0, Lcom/google/android/youtube/core/model/VmapAdBreak;->isPreroll:Z

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/google/android/youtube/core/model/VmapAdBreak;->ads:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/google/android/youtube/core/model/VmapAdBreak;->ads:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v0, v0, Lcom/google/android/youtube/core/model/VmapAdBreak;->ads:Ljava/util/List;

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/core/model/VmapAdList;->clientFreqCapSecs:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/VmapAdList;->adBreaks:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    return-void
.end method
