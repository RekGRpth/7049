.class public Lcom/google/android/youtube/core/model/ae;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/model/i;


# instance fields
.field private A:Z

.field private B:Z

.field private C:Z

.field private D:Z

.field private E:Landroid/net/Uri;

.field private a:Ljava/util/List;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:I

.field private h:Ljava/util/List;

.field private i:Landroid/net/Uri;

.field private j:Ljava/util/List;

.field private k:Ljava/util/List;

.field private l:Ljava/util/List;

.field private m:Ljava/util/List;

.field private n:Ljava/util/List;

.field private o:Ljava/util/List;

.field private p:Ljava/util/List;

.field private q:Ljava/util/List;

.field private r:Ljava/util/List;

.field private s:Ljava/util/List;

.field private t:Ljava/util/List;

.field private u:Ljava/util/List;

.field private v:Ljava/util/List;

.field private w:Ljava/util/List;

.field private x:Ljava/util/List;

.field private y:Ljava/util/List;

.field private z:J


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/model/ae;->B:Z

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/youtube/core/model/ae;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/model/ae;->C:Z

    return-object p0
.end method

.method public final a(I)Lcom/google/android/youtube/core/model/ae;
    .locals 0

    iput p1, p0, Lcom/google/android/youtube/core/model/ae;->g:I

    return-object p0
.end method

.method public final a(J)Lcom/google/android/youtube/core/model/ae;
    .locals 0

    iput-wide p1, p0, Lcom/google/android/youtube/core/model/ae;->z:J

    return-object p0
.end method

.method public final a(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/ae;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/model/ae;->a:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/ae;->a:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/model/ae;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final a(Lcom/google/android/youtube/core/model/Stream;)Lcom/google/android/youtube/core/model/ae;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/model/ae;->h:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/ae;->h:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/model/ae;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/youtube/core/model/ae;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/ae;->c:Ljava/lang/String;

    return-object p0
.end method

.method public final a(Ljava/util/Collection;)Lcom/google/android/youtube/core/model/ae;
    .locals 1

    const-string v0, "streams cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/ae;->h:Ljava/util/List;

    return-object p0
.end method

.method public final a(Ljava/util/List;)Lcom/google/android/youtube/core/model/ae;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/model/ae;->a:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/ae;->a:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/model/ae;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-object p0
.end method

.method public final a(Z)Lcom/google/android/youtube/core/model/ae;
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/core/model/ae;->A:Z

    return-object p0
.end method

.method public final b()Lcom/google/android/youtube/core/model/VastAd;
    .locals 34

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/core/model/ae;->a:Ljava/util/List;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/core/model/ae;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    sget-object v2, Lcom/google/android/youtube/core/model/VastAd;->EMPTY_AD:Lcom/google/android/youtube/core/model/VastAd;

    :goto_0
    return-object v2

    :cond_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/youtube/core/model/ae;->C:Z

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/core/model/ae;->j:Ljava/util/List;

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/core/model/ae;->j:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/core/model/ae;->j:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    invoke-static {v2}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "eid\\d=\\d+"

    const-string v4, "eid1=5"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/model/ae;->f(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/ae;

    :cond_2
    new-instance v2, Lcom/google/android/youtube/core/model/VastAd;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/core/model/ae;->a:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/core/model/ae;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/core/model/ae;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/core/model/ae;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/youtube/core/model/ae;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/youtube/core/model/ae;->f:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v9, v0, Lcom/google/android/youtube/core/model/ae;->g:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/youtube/core/model/ae;->h:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/youtube/core/model/ae;->i:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/youtube/core/model/ae;->j:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/youtube/core/model/ae;->k:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/youtube/core/model/ae;->l:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/youtube/core/model/ae;->m:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/core/model/ae;->n:Ljava/util/List;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/core/model/ae;->o:Ljava/util/List;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/core/model/ae;->p:Ljava/util/List;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/core/model/ae;->q:Ljava/util/List;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/core/model/ae;->r:Ljava/util/List;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/core/model/ae;->s:Ljava/util/List;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/core/model/ae;->t:Ljava/util/List;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/core/model/ae;->u:Ljava/util/List;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/core/model/ae;->v:Ljava/util/List;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/core/model/ae;->w:Ljava/util/List;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/core/model/ae;->x:Ljava/util/List;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/core/model/ae;->y:Ljava/util/List;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/youtube/core/model/ae;->A:Z

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/youtube/core/model/ae;->B:Z

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/youtube/core/model/ae;->z:J

    move-wide/from16 v30, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/youtube/core/model/ae;->D:Z

    move/from16 v32, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/core/model/ae;->E:Landroid/net/Uri;

    move-object/from16 v33, v0

    invoke-direct/range {v2 .. v33}, Lcom/google/android/youtube/core/model/VastAd;-><init>(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/util/List;Landroid/net/Uri;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;ZZJZLandroid/net/Uri;)V

    goto/16 :goto_0
.end method

.method public final b(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/ae;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/model/ae;->j:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/ae;->j:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/model/ae;->j:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/youtube/core/model/ae;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/ae;->b:Ljava/lang/String;

    return-object p0
.end method

.method public final b(Ljava/util/List;)Lcom/google/android/youtube/core/model/ae;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/ae;->a:Ljava/util/List;

    return-object p0
.end method

.method public final b(Z)Lcom/google/android/youtube/core/model/ae;
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/core/model/ae;->B:Z

    return-object p0
.end method

.method public synthetic build()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/ae;->b()Lcom/google/android/youtube/core/model/VastAd;

    move-result-object v0

    return-object v0
.end method

.method public final c(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/ae;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/model/ae;->k:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/ae;->k:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/model/ae;->k:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final c(Ljava/lang/String;)Lcom/google/android/youtube/core/model/ae;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/ae;->d:Ljava/lang/String;

    return-object p0
.end method

.method public final c(Ljava/util/List;)Lcom/google/android/youtube/core/model/ae;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/ae;->j:Ljava/util/List;

    return-object p0
.end method

.method public final c(Z)Lcom/google/android/youtube/core/model/ae;
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/core/model/ae;->D:Z

    return-object p0
.end method

.method public final d(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/ae;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/model/ae;->l:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/ae;->l:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/model/ae;->l:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final d(Ljava/lang/String;)Lcom/google/android/youtube/core/model/ae;
    .locals 1

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/Util;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/ae;->e:Ljava/lang/String;

    return-object p0
.end method

.method public final d(Ljava/util/List;)Lcom/google/android/youtube/core/model/ae;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/ae;->k:Ljava/util/List;

    return-object p0
.end method

.method public final e(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/ae;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/model/ae;->m:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/ae;->m:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/model/ae;->m:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final e(Ljava/lang/String;)Lcom/google/android/youtube/core/model/ae;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/ae;->f:Ljava/lang/String;

    return-object p0
.end method

.method public final e(Ljava/util/List;)Lcom/google/android/youtube/core/model/ae;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/ae;->l:Ljava/util/List;

    return-object p0
.end method

.method public final f(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/ae;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/model/ae;->n:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/ae;->n:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/model/ae;->n:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final f(Ljava/util/List;)Lcom/google/android/youtube/core/model/ae;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/ae;->m:Ljava/util/List;

    return-object p0
.end method

.method public final g(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/ae;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/model/ae;->o:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/ae;->o:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/model/ae;->o:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final g(Ljava/util/List;)Lcom/google/android/youtube/core/model/ae;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/ae;->n:Ljava/util/List;

    return-object p0
.end method

.method public final h(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/ae;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/model/ae;->p:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/ae;->p:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/model/ae;->p:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final h(Ljava/util/List;)Lcom/google/android/youtube/core/model/ae;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/ae;->o:Ljava/util/List;

    return-object p0
.end method

.method public final i(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/ae;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/model/ae;->q:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/ae;->q:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/model/ae;->q:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final i(Ljava/util/List;)Lcom/google/android/youtube/core/model/ae;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/ae;->p:Ljava/util/List;

    return-object p0
.end method

.method public final j(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/ae;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/model/ae;->r:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/ae;->r:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/model/ae;->r:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final j(Ljava/util/List;)Lcom/google/android/youtube/core/model/ae;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/ae;->q:Ljava/util/List;

    return-object p0
.end method

.method public final k(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/ae;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/model/ae;->s:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/ae;->s:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/model/ae;->s:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final k(Ljava/util/List;)Lcom/google/android/youtube/core/model/ae;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/ae;->r:Ljava/util/List;

    return-object p0
.end method

.method public final l(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/ae;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/model/ae;->t:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/ae;->t:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/model/ae;->t:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final l(Ljava/util/List;)Lcom/google/android/youtube/core/model/ae;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/ae;->s:Ljava/util/List;

    return-object p0
.end method

.method public final m(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/ae;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/model/ae;->u:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/ae;->u:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/model/ae;->u:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final m(Ljava/util/List;)Lcom/google/android/youtube/core/model/ae;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/ae;->t:Ljava/util/List;

    return-object p0
.end method

.method public final n(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/ae;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/model/ae;->v:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/ae;->v:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/model/ae;->v:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final n(Ljava/util/List;)Lcom/google/android/youtube/core/model/ae;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/ae;->u:Ljava/util/List;

    return-object p0
.end method

.method public final o(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/ae;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/model/ae;->w:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/ae;->w:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/model/ae;->w:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final o(Ljava/util/List;)Lcom/google/android/youtube/core/model/ae;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/ae;->v:Ljava/util/List;

    return-object p0
.end method

.method public final p(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/ae;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/model/ae;->x:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/ae;->x:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/model/ae;->x:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final p(Ljava/util/List;)Lcom/google/android/youtube/core/model/ae;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/ae;->w:Ljava/util/List;

    return-object p0
.end method

.method public final q(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/ae;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/model/ae;->y:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/ae;->y:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/model/ae;->y:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final q(Ljava/util/List;)Lcom/google/android/youtube/core/model/ae;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/ae;->x:Ljava/util/List;

    return-object p0
.end method

.method public final r(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/ae;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/ae;->i:Landroid/net/Uri;

    return-object p0
.end method

.method public final r(Ljava/util/List;)Lcom/google/android/youtube/core/model/ae;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/ae;->y:Ljava/util/List;

    return-object p0
.end method

.method public final s(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/ae;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/ae;->E:Landroid/net/Uri;

    return-object p0
.end method
