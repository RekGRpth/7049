.class public Lcom/google/android/youtube/core/model/am;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/model/i;


# instance fields
.field private a:Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;

.field private b:I

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Ljava/util/List;

.field private g:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;->PRE_ROLL:Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/am;->a:Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;

    iput v1, p0, Lcom/google/android/youtube/core/model/am;->b:I

    iput-boolean v1, p0, Lcom/google/android/youtube/core/model/am;->c:Z

    iput-boolean v1, p0, Lcom/google/android/youtube/core/model/am;->d:Z

    iput-boolean v1, p0, Lcom/google/android/youtube/core/model/am;->e:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/am;->f:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/am;->g:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/core/model/am;->b:I

    return v0
.end method

.method public final a(I)Lcom/google/android/youtube/core/model/am;
    .locals 0

    iput p1, p0, Lcom/google/android/youtube/core/model/am;->b:I

    return-object p0
.end method

.method public final a(Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;)Lcom/google/android/youtube/core/model/am;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/am;->a:Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;

    return-object p0
.end method

.method public final a(Lcom/google/android/youtube/core/model/VmapAdBreak$TrackingEventType;Landroid/net/Uri;)Lcom/google/android/youtube/core/model/am;
    .locals 3

    new-instance v2, Lcom/google/android/youtube/core/model/VmapAdBreak$TrackingEvent;

    const-string v0, "eventType cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/VmapAdBreak$TrackingEventType;

    const-string v1, "uri cannot be null"

    invoke-static {p2, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    invoke-direct {v2, v0, v1}, Lcom/google/android/youtube/core/model/VmapAdBreak$TrackingEvent;-><init>(Lcom/google/android/youtube/core/model/VmapAdBreak$TrackingEventType;Landroid/net/Uri;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/model/am;->g:Ljava/util/List;

    sget-object v1, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/am;->g:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/model/am;->g:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final a(Ljava/util/List;)Lcom/google/android/youtube/core/model/am;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/model/am;->f:Ljava/util/List;

    return-object p0
.end method

.method public final a(Z)Lcom/google/android/youtube/core/model/am;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/model/am;->c:Z

    return-object p0
.end method

.method public final b()Lcom/google/android/youtube/core/model/VmapAdBreak;
    .locals 9

    new-instance v0, Lcom/google/android/youtube/core/model/VmapAdBreak;

    iget-object v1, p0, Lcom/google/android/youtube/core/model/am;->a:Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;

    iget v2, p0, Lcom/google/android/youtube/core/model/am;->b:I

    iget-boolean v3, p0, Lcom/google/android/youtube/core/model/am;->c:Z

    iget-boolean v4, p0, Lcom/google/android/youtube/core/model/am;->d:Z

    iget-boolean v5, p0, Lcom/google/android/youtube/core/model/am;->e:Z

    iget-object v6, p0, Lcom/google/android/youtube/core/model/am;->f:Ljava/util/List;

    iget-object v7, p0, Lcom/google/android/youtube/core/model/am;->g:Ljava/util/List;

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/youtube/core/model/VmapAdBreak;-><init>(Lcom/google/android/youtube/core/model/VmapAdBreak$OffsetType;IZZZLjava/util/List;Ljava/util/List;Lcom/google/android/youtube/core/model/al;)V

    return-object v0
.end method

.method public final b(Z)Lcom/google/android/youtube/core/model/am;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/model/am;->d:Z

    return-object p0
.end method

.method public synthetic build()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/am;->b()Lcom/google/android/youtube/core/model/VmapAdBreak;

    move-result-object v0

    return-object v0
.end method

.method public final c(Z)Lcom/google/android/youtube/core/model/am;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/model/am;->e:Z

    return-object p0
.end method
