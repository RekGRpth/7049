.class public final Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Lcom/google/android/youtube/core/async/bk;
.implements Lcom/google/android/youtube/core/async/n;
.implements Lcom/google/android/youtube/core/player/a;


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Landroid/content/SharedPreferences;

.field private final c:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private final d:Lcom/google/android/youtube/core/client/bc;

.field private final e:Landroid/app/Dialog;

.field private final f:Landroid/app/Dialog;

.field private g:Landroid/widget/CheckBox;

.field private h:Lcom/google/android/youtube/core/player/b;

.field private i:Lcom/google/android/youtube/core/async/o;

.field private j:Lcom/google/android/youtube/core/async/p;

.field private k:Lcom/google/android/youtube/core/model/UserAuth;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/client/bc;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->a:Landroid/app/Activity;

    invoke-static {p2}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->b:Landroid/content/SharedPreferences;

    invoke-static {p3}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/UserAuthorizer;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->c:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-static {p4}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/client/bc;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->d:Lcom/google/android/youtube/core/client/bc;

    new-instance v0, Lcom/google/android/youtube/core/ui/w;

    invoke-direct {v0, p1}, Lcom/google/android/youtube/core/ui/w;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0b006c

    invoke-virtual {p1, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/w;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0b0016

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->e:Landroid/app/Dialog;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->a:Landroid/app/Activity;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04000d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f07003f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->g:Landroid/widget/CheckBox;

    new-instance v0, Lcom/google/android/youtube/core/ui/w;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->a:Landroid/app/Activity;

    invoke-direct {v0, v2}, Lcom/google/android/youtube/core/ui/w;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/w;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0b0018

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0b0017

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->f:Landroid/app/Dialog;

    return-void
.end method

.method private c()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->g:Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->k:Lcom/google/android/youtube/core/model/UserAuth;

    if-nez v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setVisibility(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->i:Lcom/google/android/youtube/core/async/o;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->i:Lcom/google/android/youtube/core/async/o;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/o;->a()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->j:Lcom/google/android/youtube/core/async/p;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->j:Lcom/google/android/youtube/core/async/p;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/p;->a()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->e:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->f:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->h:Lcom/google/android/youtube/core/player/b;

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 3

    iput-object p1, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->k:Lcom/google/android/youtube/core/model/UserAuth;

    invoke-static {p0}, Lcom/google/android/youtube/core/async/p;->a(Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/p;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->j:Lcom/google/android/youtube/core/async/p;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->d:Lcom/google/android/youtube/core/client/bc;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->a:Landroid/app/Activity;

    invoke-static {v2, v0}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v0

    invoke-interface {v1, p1, v0}, Lcom/google/android/youtube/core/client/bc;->a(Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/player/b;)V
    .locals 3

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/b;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->h:Lcom/google/android/youtube/core/player/b;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->k:Lcom/google/android/youtube/core/model/UserAuth;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->c:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/youtube/core/async/o;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/core/async/o;-><init>(Lcom/google/android/youtube/core/async/bk;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->i:Lcom/google/android/youtube/core/async/o;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->c:Lcom/google/android/youtube/core/async/UserAuthorizer;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->a:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->i:Lcom/google/android/youtube/core/async/o;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bk;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->c()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->f:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->h:Lcom/google/android/youtube/core/player/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->h:Lcom/google/android/youtube/core/player/b;

    invoke-interface {v0, p2}, Lcom/google/android/youtube/core/player/b;->a(Ljava/lang/Exception;)V

    :cond_0
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    check-cast p2, Lcom/google/android/youtube/core/model/UserProfile;

    invoke-virtual {p2}, Lcom/google/android/youtube/core/model/UserProfile;->hasAge()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Lcom/google/android/youtube/core/model/UserProfile;->hasLegalAge()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->e:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->k:Lcom/google/android/youtube/core/model/UserAuth;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/UserAuth;->account:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->b:Landroid/content/SharedPreferences;

    const-string v2, "adult_content_confirmations"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->c()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->f:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->h:Lcom/google/android/youtube/core/player/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->h:Lcom/google/android/youtube/core/player/b;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/b;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->h:Lcom/google/android/youtube/core/player/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->h:Lcom/google/android/youtube/core/player/b;

    invoke-interface {v0, p2}, Lcom/google/android/youtube/core/player/b;->a(Ljava/lang/Exception;)V

    :cond_0
    return-void
.end method

.method public final g_()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->h:Lcom/google/android/youtube/core/player/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->h:Lcom/google/android/youtube/core/player/b;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/b;->f()V

    :cond_0
    return-void
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->h:Lcom/google/android/youtube/core/player/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->h:Lcom/google/android/youtube/core/player/b;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/b;->f()V

    :cond_0
    return-void
.end method

.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    packed-switch p2, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->f:Landroid/app/Dialog;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->f:Landroid/app/Dialog;

    const v1, 0x7f07003f

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->k:Lcom/google/android/youtube/core/model/UserAuth;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/UserAuth;->account:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->b:Landroid/content/SharedPreferences;

    const-string v2, "adult_content_confirmations"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->b:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "adult_content_confirmations"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ","

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/SharedPreferences$Editor;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->h:Lcom/google/android/youtube/core/player/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->h:Lcom/google/android/youtube/core/player/b;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/b;->a()V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->h:Lcom/google/android/youtube/core/player/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;->h:Lcom/google/android/youtube/core/player/b;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/b;->f()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
