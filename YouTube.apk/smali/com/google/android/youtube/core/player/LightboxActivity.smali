.class public Lcom/google/android/youtube/core/player/LightboxActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/youtube/core/player/ac;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 26

    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/core/player/LightboxActivity;->getApplication()Landroid/app/Application;

    move-result-object v2

    move-object v4, v2

    check-cast v4, Lcom/google/android/youtube/core/player/z;

    new-instance v2, Lcom/google/android/youtube/core/player/PlayerView;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/google/android/youtube/core/player/PlayerView;-><init>(Landroid/content/Context;)V

    new-instance v3, Lcom/google/android/youtube/core/player/as;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/player/PlayerView;->a()Lcom/google/android/youtube/core/player/af;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v5}, Lcom/google/android/youtube/core/player/as;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/player/af;)V

    invoke-interface {v4}, Lcom/google/android/youtube/core/player/z;->a()Lcom/google/android/youtube/core/client/bc;

    move-result-object v6

    invoke-interface {v4}, Lcom/google/android/youtube/core/player/z;->e_()Lcom/google/android/youtube/core/client/be;

    move-result-object v7

    invoke-interface {v4}, Lcom/google/android/youtube/core/player/z;->d()Lcom/google/android/youtube/core/client/bo;

    move-result-object v9

    invoke-interface {v4}, Lcom/google/android/youtube/core/player/z;->e()Lcom/google/android/youtube/core/client/b;

    move-result-object v24

    invoke-interface {v4}, Lcom/google/android/youtube/core/player/z;->f()Lcom/google/android/youtube/core/client/bj;

    move-result-object v25

    invoke-interface {v4}, Lcom/google/android/youtube/core/player/z;->g()Lcom/google/android/youtube/core/client/bl;

    move-result-object v10

    invoke-interface {v4}, Lcom/google/android/youtube/core/player/z;->i()Lcom/google/android/youtube/core/Analytics;

    move-result-object v12

    invoke-interface {v4}, Lcom/google/android/youtube/core/player/z;->h()Lcom/google/android/youtube/core/player/ak;

    move-result-object v22

    invoke-interface {v4}, Lcom/google/android/youtube/core/player/z;->l()Lcom/google/android/youtube/core/utils/p;

    move-result-object v21

    invoke-interface {v4}, Lcom/google/android/youtube/core/player/z;->j()Lcom/google/android/youtube/core/e;

    move-result-object v20

    invoke-interface {v4}, Lcom/google/android/youtube/core/player/z;->m()Lcom/google/android/youtube/core/player/c;

    move-result-object v23

    invoke-interface {v4}, Lcom/google/android/youtube/core/player/z;->k()Landroid/content/SharedPreferences;

    move-result-object v5

    invoke-interface {v4}, Lcom/google/android/youtube/core/player/z;->n()Lcom/google/android/youtube/core/player/a;

    move-result-object v11

    new-instance v14, Lcom/google/android/youtube/core/player/ab;

    const/4 v8, 0x0

    move-object/from16 v0, p0

    invoke-direct {v14, v0, v8}, Lcom/google/android/youtube/core/player/ab;-><init>(Lcom/google/android/youtube/core/player/LightboxActivity;B)V

    new-instance v15, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;-><init>(Landroid/content/Context;)V

    new-instance v17, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;

    invoke-virtual {v15}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->j()I

    move-result v8

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v12, v8}, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/Analytics;I)V

    const/4 v8, 0x0

    invoke-virtual {v15, v8}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setShowFullscreen(Z)V

    new-instance v16, Lcom/google/android/youtube/core/player/overlay/DefaultBrandingOverlay;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultBrandingOverlay;-><init>(Landroid/content/Context;)V

    new-instance v18, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;-><init>(Landroid/content/Context;)V

    new-instance v19, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;-><init>(Landroid/content/Context;)V

    const/4 v8, 0x5

    new-array v8, v8, [Lcom/google/android/youtube/core/player/overlay/s;

    const/4 v13, 0x0

    aput-object v16, v8, v13

    const/4 v13, 0x1

    aput-object v19, v8, v13

    const/4 v13, 0x2

    aput-object v15, v8, v13

    const/4 v13, 0x3

    aput-object v17, v8, v13

    const/4 v13, 0x4

    aput-object v18, v8, v13

    invoke-virtual {v2, v8}, Lcom/google/android/youtube/core/player/PlayerView;->a([Lcom/google/android/youtube/core/player/overlay/s;)V

    invoke-interface {v4}, Lcom/google/android/youtube/core/player/z;->b()Lcom/google/android/youtube/core/client/d;

    move-result-object v8

    const/4 v13, 0x0

    move-object/from16 v4, p0

    invoke-static/range {v2 .. v25}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/player/ai;Lcom/google/android/youtube/core/player/as;Landroid/content/Context;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/d;Lcom/google/android/youtube/core/client/bo;Lcom/google/android/youtube/core/client/bl;Lcom/google/android/youtube/core/player/a;Lcom/google/android/youtube/core/Analytics;Ljava/lang/String;Lcom/google/android/youtube/core/player/v;Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;Lcom/google/android/youtube/core/player/overlay/c;Lcom/google/android/youtube/core/player/overlay/a;Lcom/google/android/youtube/core/player/overlay/i;Lcom/google/android/youtube/core/player/overlay/aa;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/core/utils/p;Lcom/google/android/youtube/core/player/ak;Lcom/google/android/youtube/core/player/c;Lcom/google/android/youtube/core/client/b;Lcom/google/android/youtube/core/client/bj;)Lcom/google/android/youtube/core/player/Director;

    move-result-object v11

    new-instance v7, Lcom/google/android/youtube/core/player/ac;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/core/player/LightboxActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v9

    move-object/from16 v8, p0

    move-object v10, v2

    move-object v12, v6

    invoke-direct/range {v7 .. v12}, Lcom/google/android/youtube/core/player/ac;-><init>(Landroid/app/Activity;Landroid/os/Bundle;Landroid/view/View;Lcom/google/android/youtube/core/player/Director;Lcom/google/android/youtube/core/client/bc;)V

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/android/youtube/core/player/LightboxActivity;->a:Lcom/google/android/youtube/core/player/ac;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/core/player/LightboxActivity;->a:Lcom/google/android/youtube/core/player/ac;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/player/ac;->show()V

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/LightboxActivity;->a:Lcom/google/android/youtube/core/player/ac;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/ac;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/LightboxActivity;->a:Lcom/google/android/youtube/core/player/ac;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/ac;->dismiss()V

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method protected onPause()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/LightboxActivity;->a:Lcom/google/android/youtube/core/player/ac;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/ac;->a()V

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method
