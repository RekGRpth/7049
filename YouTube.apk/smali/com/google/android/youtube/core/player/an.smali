.class public final Lcom/google/android/youtube/core/player/an;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field private final a:Landroid/os/Handler;

.field private final b:Landroid/content/SharedPreferences;

.field private final c:Lcom/google/android/youtube/core/player/ao;

.field private final d:Lcom/google/android/youtube/core/client/bl;

.field private final e:Ljava/lang/String;

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Ljava/lang/String;

.field private l:Ljava/util/List;

.field private m:Ljava/lang/String;

.field private n:Lcom/google/android/youtube/core/async/p;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/player/ao;Lcom/google/android/youtube/core/client/bl;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "uiHandler cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/an;->a:Landroid/os/Handler;

    const-string v0, "preferences cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/an;->b:Landroid/content/SharedPreferences;

    const-string v0, "subtitlesClient cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/client/bl;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/an;->d:Lcom/google/android/youtube/core/client/bl;

    invoke-static {p3}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/ao;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/an;->c:Lcom/google/android/youtube/core/player/ao;

    iput-object p5, p0, Lcom/google/android/youtube/core/player/an;->e:Ljava/lang/String;

    return-void
.end method

.method private c()V
    .locals 4

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/an;->h:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/an;->l:Ljava/util/List;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/an;->f()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/an;->h:Z

    invoke-static {p0}, Lcom/google/android/youtube/core/async/p;->a(Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/p;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/an;->n:Lcom/google/android/youtube/core/async/p;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/an;->d:Lcom/google/android/youtube/core/client/bl;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/an;->m:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/an;->a:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/youtube/core/player/an;->n:Lcom/google/android/youtube/core/async/p;

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/async/am;->a(Landroid/os/Handler;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/am;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/bl;->a(Ljava/lang/String;Lcom/google/android/youtube/core/async/n;)V

    goto :goto_0
.end method

.method private d()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/an;->i:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/an;->i:Z

    iget-object v0, p0, Lcom/google/android/youtube/core/player/an;->c:Lcom/google/android/youtube/core/player/ao;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ao;->a()V

    :cond_0
    return-void
.end method

.method private e()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/an;->i:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/an;->i:Z

    iget-object v0, p0, Lcom/google/android/youtube/core/player/an;->c:Lcom/google/android/youtube/core/player/ao;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ao;->b()V

    :cond_0
    return-void
.end method

.method private f()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/youtube/core/player/an;->l:Ljava/util/List;

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/an;->j:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/an;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/youtube/core/player/an;->e:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/youtube/core/model/SubtitleTrack;->createDisableSubtitleOption(Ljava/lang/String;)Lcom/google/android/youtube/core/model/SubtitleTrack;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/core/player/an;->c:Lcom/google/android/youtube/core/player/ao;

    invoke-interface {v1, v0}, Lcom/google/android/youtube/core/player/ao;->a(Ljava/util/List;)V

    return-void

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method private g()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/an;->h:Z

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/an;->e()V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/an;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/an;->c:Lcom/google/android/youtube/core/player/ao;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ao;->c()V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/an;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    const-string v2, "call init() first"

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/utils/s;->b(ZLjava/lang/Object;)V

    iput-boolean v1, p0, Lcom/google/android/youtube/core/player/an;->g:Z

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/an;->c()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/youtube/core/model/SubtitleTrack;)V
    .locals 3

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/SubtitleTrack;->isDisableOption()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/an;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "subtitles_language_code"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/SharedPreferences$Editor;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/player/an;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "subtitles_language_code"

    iget-object v2, p1, Lcom/google/android/youtube/core/model/SubtitleTrack;->languageCode:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/SharedPreferences$Editor;)V

    goto :goto_0
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    const-string v0, "error retrieving subtitle tracks"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/an;->g()V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 7

    const/4 v1, 0x0

    const/4 v6, 0x0

    check-cast p2, Ljava/util/List;

    iput-boolean v6, p0, Lcom/google/android/youtube/core/player/an;->h:Z

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "SubtitleTrack response was empty"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/an;->g()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/an;->j:Z

    if-eqz v0, :cond_2

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x1

    if-le v0, v2, :cond_3

    :cond_2
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/an;->d()V

    :cond_3
    iput-object p2, p0, Lcom/google/android/youtube/core/player/an;->l:Ljava/util/List;

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/an;->f:Z

    if-eqz v0, :cond_7

    iput-boolean v6, p0, Lcom/google/android/youtube/core/player/an;->f:Z

    iget-object v0, p0, Lcom/google/android/youtube/core/player/an;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v2, v1

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/SubtitleTrack;

    iget-object v4, v0, Lcom/google/android/youtube/core/model/SubtitleTrack;->languageCode:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/youtube/core/player/an;->k:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    move-object v2, v0

    goto :goto_1

    :cond_4
    if-nez v1, :cond_8

    const-string v4, "en"

    iget-object v5, v0, Lcom/google/android/youtube/core/model/SubtitleTrack;->languageCode:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    :goto_2
    move-object v1, v0

    goto :goto_1

    :cond_5
    if-nez v2, :cond_6

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/an;->j:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/youtube/core/player/an;->l:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/SubtitleTrack;

    move-object v2, v0

    :cond_6
    if-eqz v2, :cond_7

    iget-object v0, p0, Lcom/google/android/youtube/core/player/an;->c:Lcom/google/android/youtube/core/player/ao;

    invoke-interface {v0, v2}, Lcom/google/android/youtube/core/player/ao;->a(Lcom/google/android/youtube/core/model/SubtitleTrack;)V

    :cond_7
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/an;->g:Z

    if-eqz v0, :cond_0

    iput-boolean v6, p0, Lcom/google/android/youtube/core/player/an;->g:Z

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/an;->f()V

    goto :goto_0

    :cond_8
    move-object v0, v1

    goto :goto_2
.end method

.method public final a(Ljava/lang/String;Landroid/net/Uri;ZZLjava/lang/String;)V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/an;->b()V

    const-string v0, "videoId cannot be empty"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/an;->m:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/google/android/youtube/core/player/an;->j:Z

    iget-object v0, p0, Lcom/google/android/youtube/core/player/an;->b:Landroid/content/SharedPreferences;

    const-string v1, "subtitles_language_code"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz p3, :cond_2

    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    :goto_0
    iput-object p5, p0, Lcom/google/android/youtube/core/player/an;->k:Ljava/lang/String;

    if-eqz p2, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/an;->d()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/an;->k:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/an;->f:Z

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/an;->c()V

    :cond_0
    return-void

    :cond_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object p5

    goto :goto_0

    :cond_2
    move-object p5, v0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    iput-object v1, p0, Lcom/google/android/youtube/core/player/an;->m:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/youtube/core/player/an;->l:Ljava/util/List;

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/an;->g:Z

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/an;->f:Z

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/an;->h:Z

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/an;->e()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/an;->n:Lcom/google/android/youtube/core/async/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/an;->n:Lcom/google/android/youtube/core/async/p;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/p;->a()V

    iput-object v1, p0, Lcom/google/android/youtube/core/player/an;->n:Lcom/google/android/youtube/core/async/p;

    :cond_0
    return-void
.end method
