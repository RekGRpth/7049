.class public final Lcom/google/android/youtube/core/player/as;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:I

.field public static final b:I

.field public static final c:I

.field public static final d:I

.field public static final e:I

.field public static final f:I

.field public static final g:I

.field public static final h:I

.field private static final i:Ljava/util/Set;


# instance fields
.field private volatile A:Z

.field private volatile B:I

.field private C:Lcom/google/android/youtube/core/model/Stream;

.field private D:Lcom/google/android/youtube/core/model/Stream;

.field private E:Z

.field private F:Z

.field private final G:Lcom/google/android/youtube/core/player/av;

.field private final H:Ljava/util/concurrent/CopyOnWriteArrayList;

.field private final I:Lcom/google/android/youtube/core/player/az;

.field private final j:Landroid/content/Context;

.field private final k:Ljava/util/concurrent/atomic/AtomicReference;

.field private final l:Ljava/util/concurrent/atomic/AtomicReference;

.field private final m:Lcom/google/android/youtube/core/player/ay;

.field private final n:Landroid/os/Handler;

.field private final o:Ljava/lang/Runnable;

.field private final p:Lcom/google/android/youtube/core/player/af;

.field private final q:Lcom/google/android/youtube/core/player/ax;

.field private r:Z

.field private s:Z

.field private t:Z

.field private volatile u:Z

.field private volatile v:Z

.field private volatile w:Z

.field private volatile x:Z

.field private volatile y:Z

.field private volatile z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const v3, 0x7fffffff

    const/16 v2, -0xbb8

    sget v0, Lcom/google/android/youtube/core/utils/Util;->a:I

    const/16 v1, 0xf

    if-le v0, v1, :cond_1

    const/16 v0, -0xbb6

    sput v0, Lcom/google/android/youtube/core/player/as;->a:I

    const/16 v0, -0xbb5

    sput v0, Lcom/google/android/youtube/core/player/as;->b:I

    const/16 v0, -0xbb4

    sput v0, Lcom/google/android/youtube/core/player/as;->c:I

    const/16 v0, -0xbb3

    sput v0, Lcom/google/android/youtube/core/player/as;->d:I

    const/16 v0, -0xbb2

    sput v0, Lcom/google/android/youtube/core/player/as;->e:I

    const/16 v0, -0xbb1

    sput v0, Lcom/google/android/youtube/core/player/as;->f:I

    const/16 v0, -0xbb0

    sput v0, Lcom/google/android/youtube/core/player/as;->g:I

    sput v2, Lcom/google/android/youtube/core/player/as;->h:I

    :goto_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    const/16 v1, -0x3e81

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const/16 v1, -0x3f2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const/16 v1, -0x7d2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const/16 v1, -0x7d1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget v1, Lcom/google/android/youtube/core/player/as;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget v1, Lcom/google/android/youtube/core/player/as;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget v1, Lcom/google/android/youtube/core/player/as;->c:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget v1, Lcom/google/android/youtube/core/player/as;->d:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget v1, Lcom/google/android/youtube/core/player/as;->e:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget v1, Lcom/google/android/youtube/core/player/as;->f:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget v1, Lcom/google/android/youtube/core/player/as;->g:I

    if-eq v1, v3, :cond_0

    sget v1, Lcom/google/android/youtube/core/player/as;->g:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_0
    sget v1, Lcom/google/android/youtube/core/player/as;->h:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const/16 v1, 0x1f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const/16 v1, 0x20

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const/16 v1, 0x21

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/core/player/as;->i:Ljava/util/Set;

    return-void

    :cond_1
    sput v2, Lcom/google/android/youtube/core/player/as;->a:I

    const/16 v0, -0xbb9

    sput v0, Lcom/google/android/youtube/core/player/as;->b:I

    const/16 v0, -0xbba

    sput v0, Lcom/google/android/youtube/core/player/as;->c:I

    const/16 v0, -0xbbb

    sput v0, Lcom/google/android/youtube/core/player/as;->d:I

    const/16 v0, -0xbbc

    sput v0, Lcom/google/android/youtube/core/player/as;->e:I

    const/16 v0, -0xbbd

    sput v0, Lcom/google/android/youtube/core/player/as;->f:I

    sput v3, Lcom/google/android/youtube/core/player/as;->g:I

    const/16 v0, -0xbbe

    sput v0, Lcom/google/android/youtube/core/player/as;->h:I

    goto/16 :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/core/player/af;)V
    .locals 1

    new-instance v0, Lcom/google/android/youtube/core/player/au;

    invoke-direct {v0}, Lcom/google/android/youtube/core/player/au;-><init>()V

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/youtube/core/player/as;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/player/af;Lcom/google/android/youtube/core/player/ax;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/core/player/af;Lcom/google/android/youtube/core/player/ax;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "context cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/as;->j:Landroid/content/Context;

    const-string v0, "playerSurface cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/af;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/as;->p:Lcom/google/android/youtube/core/player/af;

    const-string v0, "mediaPlayerFactory cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/ax;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/as;->q:Lcom/google/android/youtube/core/player/ax;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/as;->k:Ljava/util/concurrent/atomic/AtomicReference;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/as;->l:Ljava/util/concurrent/atomic/AtomicReference;

    new-instance v0, Lcom/google/android/youtube/core/player/av;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/core/player/av;-><init>(Lcom/google/android/youtube/core/player/as;B)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/as;->G:Lcom/google/android/youtube/core/player/av;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/as;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v0, Lcom/google/android/youtube/core/player/ay;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/core/player/ay;-><init>(Lcom/google/android/youtube/core/player/as;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/as;->m:Lcom/google/android/youtube/core/player/ay;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->m:Lcom/google/android/youtube/core/player/ay;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/ay;->start()V

    new-instance v0, Lcom/google/android/youtube/core/player/az;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/core/player/az;-><init>(Lcom/google/android/youtube/core/player/as;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/as;->I:Lcom/google/android/youtube/core/player/az;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->I:Lcom/google/android/youtube/core/player/az;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/az;->start()V

    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/as;->n:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/youtube/core/player/at;

    invoke-direct {v0, p0, p2}, Lcom/google/android/youtube/core/player/at;-><init>(Lcom/google/android/youtube/core/player/as;Lcom/google/android/youtube/core/player/af;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/as;->o:Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->G:Lcom/google/android/youtube/core/player/av;

    invoke-interface {p2, v0}, Lcom/google/android/youtube/core/player/af;->setListener(Lcom/google/android/youtube/core/player/ag;)V

    invoke-interface {p2}, Lcom/google/android/youtube/core/player/af;->f()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/as;->E:Z

    return-void
.end method

.method private a(Lcom/google/android/youtube/core/player/ad;)Landroid/media/audiofx/Virtualizer;
    .locals 4

    const/4 v1, 0x0

    :try_start_0
    new-instance v0, Landroid/media/audiofx/Virtualizer;

    const/4 v2, 0x0

    invoke-interface {p1}, Lcom/google/android/youtube/core/player/ad;->n()I

    move-result v3

    invoke-direct {v0, v2, v3}, Landroid/media/audiofx/Virtualizer;-><init>(II)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/youtube/core/player/as;->r:Z

    iget-object v1, p0, Lcom/google/android/youtube/core/player/as;->l:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    return-object v0

    :catch_0
    move-exception v0

    const-string v2, "Failed to initialize virtual surround sound"

    invoke-static {v2, v0}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/as;)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-static {}, Lcom/google/android/youtube/core/L;->e()V

    iput-boolean v2, p0, Lcom/google/android/youtube/core/player/as;->w:Z

    iput-boolean v2, p0, Lcom/google/android/youtube/core/player/as;->x:Z

    invoke-direct {p0, v2}, Lcom/google/android/youtube/core/player/as;->c(Z)V

    sget v0, Lcom/google/android/youtube/core/utils/Util;->a:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_0

    iput-boolean v2, p0, Lcom/google/android/youtube/core/player/as;->r:Z

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->l:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, v3}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/audiofx/Virtualizer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/media/audiofx/Virtualizer;->release()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->k:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, v3}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/ad;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/core/player/as;->I:Lcom/google/android/youtube/core/player/az;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/az;->b()V

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/as;->z:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/as;->A:Z

    if-nez v1, :cond_1

    const/4 v1, 0x4

    invoke-direct {p0, v1}, Lcom/google/android/youtube/core/player/as;->b(I)V

    :cond_1
    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ad;->a()V

    :cond_2
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/as;I)V
    .locals 5

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->k:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/ad;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/as;->l()Z

    move-result v1

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/youtube/core/player/as;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Handler;

    const/16 v3, 0xa

    const/4 v4, 0x0

    invoke-static {v1, v3, p1, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Error calling mediaPlayer"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    :try_start_1
    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/ad;->a(I)V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/as;II)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    const/16 v2, 0xe

    invoke-static {v0, v2, p1, p2}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/as;III)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    const/4 v2, 0x5

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0, v2, p1, p2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/as;Lcom/google/android/youtube/core/model/Stream;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/as;->b(Lcom/google/android/youtube/core/model/Stream;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/as;Lcom/google/android/youtube/core/player/ad;Landroid/net/Uri;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->k:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    if-eqz p1, :cond_3

    if-eqz p2, :cond_3

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/as;->z:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/as;->A:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/as;->b(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->p:Lcom/google/android/youtube/core/player/af;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/af;->a(Lcom/google/android/youtube/core/player/ad;)V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/as;->t:Z

    if-eqz v1, :cond_1

    invoke-interface {p1}, Lcom/google/android/youtube/core/player/ad;->j()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "x-disconnect-at-highwatermark"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    sget v1, Lcom/google/android/youtube/core/utils/Util;->a:I

    const/16 v2, 0xe

    if-lt v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/core/player/as;->j:Landroid/content/Context;

    invoke-interface {p1, v1, p2, v0}, Lcom/google/android/youtube/core/player/ad;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V

    :goto_0
    invoke-interface {p1}, Lcom/google/android/youtube/core/player/ad;->c()V

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Lcom/google/android/youtube/core/player/ad;->a(Z)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/as;->c(Z)V

    :goto_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->j:Landroid/content/Context;

    invoke-interface {p1, v0, p2}, Lcom/google/android/youtube/core/player/ad;->a(Landroid/content/Context;Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Media Player error preparing video"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/as;->a(Ljava/lang/Exception;)V

    goto :goto_1

    :catch_1
    move-exception v0

    const-string v1, "Media Player error preparing video"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/as;->a(Ljava/lang/Exception;)V

    goto :goto_1

    :catch_2
    move-exception v0

    const-string v1, "Error calling mediaPlayer"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Media Player null pointer preparing video "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/as;->a(Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method private a(Ljava/lang/Exception;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    const/16 v2, 0x9

    invoke-static {v0, v2, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/as;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/as;->E:Z

    return p1
.end method

.method static synthetic b(Lcom/google/android/youtube/core/player/as;Lcom/google/android/youtube/core/model/Stream;)Lcom/google/android/youtube/core/model/Stream;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/as;->C:Lcom/google/android/youtube/core/model/Stream;

    return-object v0
.end method

.method private b(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_0
    return-void
.end method

.method private b(Lcom/google/android/youtube/core/model/Stream;)V
    .locals 4

    const/4 v3, 0x1

    invoke-static {}, Lcom/google/android/youtube/core/L;->e()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->m:Lcom/google/android/youtube/core/player/ay;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/ay;->c()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->p:Lcom/google/android/youtube/core/player/af;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/af;->c()V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/as;->E:Z

    if-nez v0, :cond_0

    iput-object p1, p0, Lcom/google/android/youtube/core/player/as;->C:Lcom/google/android/youtube/core/model/Stream;

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->q:Lcom/google/android/youtube/core/player/ax;

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/as;->v:Z

    invoke-interface {v0, p1, v1}, Lcom/google/android/youtube/core/player/ax;->a(Lcom/google/android/youtube/core/model/Stream;Z)Lcom/google/android/youtube/core/player/ad;

    move-result-object v2

    const/4 v0, 0x3

    invoke-interface {v2, v0}, Lcom/google/android/youtube/core/player/ad;->d(I)V

    sget v0, Lcom/google/android/youtube/core/utils/Util;->a:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/as;->s:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/as;->s:Z

    if-eq v0, v3, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/as;->s:Z

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->k:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/ad;

    const/4 v1, 0x0

    iget-boolean v3, p0, Lcom/google/android/youtube/core/player/as;->r:Z

    if-eqz v3, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->l:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/audiofx/Virtualizer;

    :goto_1
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/media/audiofx/Virtualizer;->getDescriptor()Landroid/media/audiofx/AudioEffect$Descriptor;

    move-result-object v1

    iget-object v1, v1, Landroid/media/audiofx/AudioEffect$Descriptor;->uuid:Ljava/util/UUID;

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "3db6f600-0e41-11e2-af49-0002a5d5c51b"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/media/audiofx/Virtualizer;->setEnabled(Z)I

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->G:Lcom/google/android/youtube/core/player/av;

    invoke-interface {v2, v0}, Lcom/google/android/youtube/core/player/ad;->a(Lcom/google/android/youtube/core/player/ae;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->m:Lcom/google/android/youtube/core/player/ay;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Stream;->uri:Landroid/net/Uri;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/youtube/core/player/ay;->a(Lcom/google/android/youtube/core/player/ad;Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Factory failed to create a MediaPlayer for the stream"

    invoke-static {v1}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/as;->a(Ljava/lang/Exception;)V

    goto :goto_0

    :cond_2
    if-eqz v0, :cond_3

    :try_start_1
    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/as;->a(Lcom/google/android/youtube/core/player/ad;)Landroid/media/audiofx/Virtualizer;
    :try_end_1
    .catch Ljava/lang/InstantiationException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method static synthetic b(Lcom/google/android/youtube/core/player/as;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->k:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/ad;

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/google/android/youtube/core/L;->e()V

    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/as;->v:Z

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/as;->x:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/as;->w:Z

    if-eqz v1, :cond_0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ad;->e()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->n:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/as;->o:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/as;->x:Z

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/as;->A:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/as;->w:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/as;->u:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/as;->b(I)V

    :cond_1
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/as;->A:Z

    :cond_2
    :goto_1
    return-void

    :cond_3
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/as;->l()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ad;->e()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->n:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/as;->o:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/as;->x:Z

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/as;->A:Z

    if-nez v0, :cond_4

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/as;->b(I)V

    :cond_4
    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->I:Lcom/google/android/youtube/core/player/az;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/az;->a()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Error calling mediaPlayer"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method static synthetic b(Lcom/google/android/youtube/core/player/as;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/as;->b(I)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/youtube/core/player/as;II)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    const/16 v2, 0x9

    invoke-static {v0, v2, p1, p2}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/google/android/youtube/core/player/as;Z)Z
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/as;->w:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/youtube/core/player/as;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->k:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/ad;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/as;->l()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/google/android/youtube/core/L;->e()V

    :try_start_0
    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ad;->d()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/as;->x:Z

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/as;->b(I)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/as;->c(Z)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Error calling mediaPlayer"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/youtube/core/player/as;I)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    const/16 v2, 0xb

    const/4 v3, 0x0

    invoke-static {v0, v2, p1, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private c(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/as;->y:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/as;->y:Z

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/as;->y:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x6

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/as;->b(I)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x7

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/youtube/core/player/as;Z)Z
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/as;->u:Z

    return v0
.end method

.method static synthetic d(Lcom/google/android/youtube/core/player/as;I)I
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/youtube/core/player/as;->B:I

    return v0
.end method

.method static synthetic d(Lcom/google/android/youtube/core/player/as;)Lcom/google/android/youtube/core/model/Stream;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->C:Lcom/google/android/youtube/core/model/Stream;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/core/player/as;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/as;->z:Z

    return p1
.end method

.method static synthetic e(Lcom/google/android/youtube/core/player/as;)Lcom/google/android/youtube/core/player/af;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->p:Lcom/google/android/youtube/core/player/af;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/core/player/as;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/as;->c(Z)V

    return-void
.end method

.method static synthetic f(Lcom/google/android/youtube/core/player/as;)Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/as;->m()Z

    move-result v0

    return v0
.end method

.method static synthetic f(Lcom/google/android/youtube/core/player/as;Z)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/as;->x:Z

    return v0
.end method

.method static synthetic g(Lcom/google/android/youtube/core/player/as;)Lcom/google/android/youtube/core/player/az;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->I:Lcom/google/android/youtube/core/player/az;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/youtube/core/player/as;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/as;->A:Z

    return p1
.end method

.method static synthetic h(Lcom/google/android/youtube/core/player/as;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->j:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/youtube/core/player/as;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->n:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/youtube/core/player/as;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/as;->u:Z

    return v0
.end method

.method static synthetic k()Ljava/util/Set;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/core/player/as;->i:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/youtube/core/player/as;)Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/as;->l()Z

    move-result v0

    return v0
.end method

.method private l()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/as;->w:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/as;->F:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/as;->u:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic l(Lcom/google/android/youtube/core/player/as;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/as;->v:Z

    return v0
.end method

.method private m()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->j:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/pm/PackageManager;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->D:Lcom/google/android/youtube/core/model/Stream;

    if-eqz v0, :cond_0

    const-string v0, "video/wvm"

    iget-object v1, p0, Lcom/google/android/youtube/core/player/as;->D:Lcom/google/android/youtube/core/model/Stream;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Stream;->mimeType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic m(Lcom/google/android/youtube/core/player/as;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/as;->z:Z

    return v0
.end method

.method static synthetic n(Lcom/google/android/youtube/core/player/as;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/as;->A:Z

    return v0
.end method

.method static synthetic o(Lcom/google/android/youtube/core/player/as;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/as;->w:Z

    return v0
.end method

.method static synthetic p(Lcom/google/android/youtube/core/player/as;)I
    .locals 2

    iget v0, p0, Lcom/google/android/youtube/core/player/as;->B:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/youtube/core/player/as;->B:I

    return v0
.end method

.method static synthetic q(Lcom/google/android/youtube/core/player/as;)I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/core/player/as;->B:I

    return v0
.end method

.method static synthetic r(Lcom/google/android/youtube/core/player/as;)Lcom/google/android/youtube/core/model/Stream;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->D:Lcom/google/android/youtube/core/model/Stream;

    return-object v0
.end method

.method static synthetic s(Lcom/google/android/youtube/core/player/as;)Ljava/util/concurrent/atomic/AtomicReference;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->k:Ljava/util/concurrent/atomic/AtomicReference;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->k:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/ad;

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/youtube/core/player/aa;->a:Ljava/util/Set;

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ad;->i()Ljava/util/Set;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(FF)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->k:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/ad;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/player/ad;->a(FF)V

    :cond_0
    return-void
.end method

.method public final a(I)V
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/as;->v:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->I:Lcom/google/android/youtube/core/player/az;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/az;->a(Lcom/google/android/youtube/core/player/az;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->I:Lcom/google/android/youtube/core/player/az;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/az;->a(Lcom/google/android/youtube/core/player/az;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->m:Lcom/google/android/youtube/core/player/ay;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/youtube/core/player/as;->I:Lcom/google/android/youtube/core/player/az;

    invoke-static {v2}, Lcom/google/android/youtube/core/player/az;->b(Lcom/google/android/youtube/core/player/az;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    invoke-static {p1, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/ay;->a(I)V

    :cond_0
    return-void
.end method

.method public final a(Landroid/os/Handler;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Stream;)V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/as;->v:Z

    iput-boolean v1, p0, Lcom/google/android/youtube/core/player/as;->u:Z

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/as;->F:Z

    iput-object p1, p0, Lcom/google/android/youtube/core/player/as;->D:Lcom/google/android/youtube/core/model/Stream;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->I:Lcom/google/android/youtube/core/player/az;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/az;->a(Lcom/google/android/youtube/core/player/az;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/as;->b(Lcom/google/android/youtube/core/model/Stream;)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Stream;I)V
    .locals 2

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/as;->u:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->D:Lcom/google/android/youtube/core/model/Stream;

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/core/model/Stream;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/as;->u:Z

    iput-boolean v1, p0, Lcom/google/android/youtube/core/player/as;->v:Z

    iput-object p1, p0, Lcom/google/android/youtube/core/player/as;->D:Lcom/google/android/youtube/core/model/Stream;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->I:Lcom/google/android/youtube/core/player/az;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/az;->a(Lcom/google/android/youtube/core/player/az;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    const-string v0, "application/x-mpegURL"

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Stream;->mimeType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/as;->F:Z

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/as;->F:Z

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/as;->m()Z

    move-result v1

    or-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/as;->F:Z

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/as;->b(Lcom/google/android/youtube/core/model/Stream;)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/as;->t:Z

    return-void
.end method

.method public final b(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->I:Lcom/google/android/youtube/core/player/az;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/az;->quit()Z

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->m:Lcom/google/android/youtube/core/player/ay;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/ay;->quit()Z

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->p:Lcom/google/android/youtube/core/player/af;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/af;->e()V

    return-void
.end method

.method public final b()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->k:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/as;->x:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/os/Handler;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->H:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final c()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->I:Lcom/google/android/youtube/core/player/az;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/az;->a(Lcom/google/android/youtube/core/player/az;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    return v0
.end method

.method public final d()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->I:Lcom/google/android/youtube/core/player/az;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/az;->b(Lcom/google/android/youtube/core/player/az;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    return v0
.end method

.method public final e()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->m:Lcom/google/android/youtube/core/player/ay;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/ay;->a()V

    return-void
.end method

.method public final f()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->m:Lcom/google/android/youtube/core/player/ay;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/ay;->b()V

    return-void
.end method

.method public final g()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->m:Lcom/google/android/youtube/core/player/ay;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/ay;->c()V

    return-void
.end method

.method public final h()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->m:Lcom/google/android/youtube/core/player/ay;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/ay;->d()V

    return-void
.end method

.method public final i()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/as;->p:Lcom/google/android/youtube/core/player/af;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/af;->c()V

    return-void
.end method

.method public final j()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/as;->y:Z

    return v0
.end method
