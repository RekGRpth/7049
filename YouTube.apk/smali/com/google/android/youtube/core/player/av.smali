.class final Lcom/google/android/youtube/core/player/av;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/player/ae;
.implements Lcom/google/android/youtube/core/player/ag;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/player/as;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/core/player/as;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/core/player/as;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/av;-><init>(Lcom/google/android/youtube/core/player/as;)V

    return-void
.end method

.method private c(Lcom/google/android/youtube/core/player/ad;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/as;->k(Lcom/google/android/youtube/core/player/as;)Z

    move-result v0

    if-eqz v0, :cond_5

    sget v0, Lcom/google/android/youtube/core/utils/Util;->a:I

    const/16 v1, 0x10

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/as;->e(Lcom/google/android/youtube/core/player/as;)Lcom/google/android/youtube/core/player/af;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-static {v1}, Lcom/google/android/youtube/core/player/as;->l(Lcom/google/android/youtube/core/player/as;)Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/af;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    const/4 v0, 0x2

    invoke-interface {p1, v0}, Lcom/google/android/youtube/core/player/ad;->c(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/as;->g(Lcom/google/android/youtube/core/player/as;)Lcom/google/android/youtube/core/player/az;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/player/az;->a(Lcom/google/android/youtube/core/player/az;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-static {v1}, Lcom/google/android/youtube/core/player/as;->l(Lcom/google/android/youtube/core/player/as;)Z

    move-result v1

    if-nez v1, :cond_1

    if-eqz v0, :cond_1

    invoke-interface {p1, v0}, Lcom/google/android/youtube/core/player/ad;->a(I)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/as;->m(Lcom/google/android/youtube/core/player/as;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/as;->n(Lcom/google/android/youtube/core/player/as;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/as;->b(Lcom/google/android/youtube/core/player/as;I)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/player/as;->d(Lcom/google/android/youtube/core/player/as;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/as;->l(Lcom/google/android/youtube/core/player/as;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/as;->j(Lcom/google/android/youtube/core/player/as;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/player/as;->e(Lcom/google/android/youtube/core/player/as;Z)V

    :cond_4
    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/as;->e()V

    :cond_5
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    invoke-static {}, Lcom/google/android/youtube/core/L;->e()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/as;->a(Lcom/google/android/youtube/core/player/as;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/as;->d(Lcom/google/android/youtube/core/player/as;)Lcom/google/android/youtube/core/model/Stream;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-static {v1}, Lcom/google/android/youtube/core/player/as;->d(Lcom/google/android/youtube/core/player/as;)Lcom/google/android/youtube/core/model/Stream;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/as;->a(Lcom/google/android/youtube/core/player/as;Lcom/google/android/youtube/core/model/Stream;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/as;->b(Lcom/google/android/youtube/core/player/as;Lcom/google/android/youtube/core/model/Stream;)Lcom/google/android/youtube/core/model/Stream;

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/player/ad;)V
    .locals 2

    invoke-static {}, Lcom/google/android/youtube/core/L;->e()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/as;->b(Lcom/google/android/youtube/core/player/as;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/as;->f(Lcom/google/android/youtube/core/player/as;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/as;->g(Lcom/google/android/youtube/core/player/as;)Lcom/google/android/youtube/core/player/az;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/player/az;->b(Lcom/google/android/youtube/core/player/az;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/youtube/core/player/ad;->l()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/av;->c(Lcom/google/android/youtube/core/player/ad;)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/player/ad;II)Z
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "media player info "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->a()V

    sparse-switch p2, :sswitch_data_0

    :goto_0
    return v2

    :sswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Buffering data from "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-static {v1}, Lcom/google/android/youtube/core/player/as;->r(Lcom/google/android/youtube/core/player/as;)Lcom/google/android/youtube/core/model/Stream;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/as;->e(Lcom/google/android/youtube/core/player/as;Z)V

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/player/as;->e(Lcom/google/android/youtube/core/player/as;Z)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    const/16 v1, 0xc

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/as;->b(Lcom/google/android/youtube/core/player/as;I)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x2bd -> :sswitch_0
        0x2be -> :sswitch_1
        0x385 -> :sswitch_2
    .end sparse-switch
.end method

.method public final b()V
    .locals 0

    invoke-static {}, Lcom/google/android/youtube/core/L;->e()V

    return-void
.end method

.method public final b(I)V
    .locals 3

    const/16 v0, 0x64

    iget-object v1, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-static {v1}, Lcom/google/android/youtube/core/player/as;->g(Lcom/google/android/youtube/core/player/as;)Lcom/google/android/youtube/core/player/az;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/player/az;->c(Lcom/google/android/youtube/core/player/az;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    const/16 v2, 0x5a

    if-le p1, v2, :cond_1

    if-eq v1, p1, :cond_0

    if-ne v1, v0, :cond_1

    :cond_0
    move p1, v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/as;->g(Lcom/google/android/youtube/core/player/as;)Lcom/google/android/youtube/core/player/az;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/player/az;->c(Lcom/google/android/youtube/core/player/az;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    return-void
.end method

.method public final b(Lcom/google/android/youtube/core/player/ad;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-static {v1}, Lcom/google/android/youtube/core/player/as;->g(Lcom/google/android/youtube/core/player/as;)Lcom/google/android/youtube/core/player/az;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/player/az;->a(Lcom/google/android/youtube/core/player/az;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/as;->c(Lcom/google/android/youtube/core/player/as;I)V

    return-void
.end method

.method public final b(Lcom/google/android/youtube/core/player/ad;II)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/as;->o(Lcom/google/android/youtube/core/player/as;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/as;->d(Lcom/google/android/youtube/core/player/as;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/player/as;->g(Lcom/google/android/youtube/core/player/as;Z)Z

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "MediaPlayer error during prepare [what="

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", extra="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "]"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    :goto_0
    if-ne p2, v1, :cond_2

    invoke-static {}, Lcom/google/android/youtube/core/player/as;->k()Ljava/util/Set;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/as;->p(Lcom/google/android/youtube/core/player/as;)I

    move-result v0

    const/4 v3, 0x3

    if-ge v0, v3, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Retrying MediaPlayer error [retry="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-static {v2}, Lcom/google/android/youtube/core/player/as;->q(Lcom/google/android/youtube/core/player/as;)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", max=3"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    const/16 v0, 0x64

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/as;->e(Lcom/google/android/youtube/core/player/as;)Lcom/google/android/youtube/core/player/af;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/af;->d()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-static {v0, p2, p3}, Lcom/google/android/youtube/core/player/as;->a(Lcom/google/android/youtube/core/player/as;II)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/as;->l(Lcom/google/android/youtube/core/player/as;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-static {v2}, Lcom/google/android/youtube/core/player/as;->r(Lcom/google/android/youtube/core/player/as;)Lcom/google/android/youtube/core/model/Stream;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/player/as;->a(Lcom/google/android/youtube/core/model/Stream;)V

    :goto_2
    return v1

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/player/as;->d(Lcom/google/android/youtube/core/player/as;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/as;->g(Lcom/google/android/youtube/core/player/as;Z)Z

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "MediaPlayer error during playback [what="

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", extra="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "]"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_2
    move v0, v2

    goto/16 :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-static {v2}, Lcom/google/android/youtube/core/player/as;->r(Lcom/google/android/youtube/core/player/as;)Lcom/google/android/youtube/core/model/Stream;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-static {v3}, Lcom/google/android/youtube/core/player/as;->g(Lcom/google/android/youtube/core/player/as;)Lcom/google/android/youtube/core/player/az;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/youtube/core/player/az;->a(Lcom/google/android/youtube/core/player/az;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/youtube/core/player/as;->a(Lcom/google/android/youtube/core/model/Stream;I)V

    goto :goto_2

    :cond_4
    const-string v0, "Reporting MediaPlayer error"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/player/as;->d(Lcom/google/android/youtube/core/player/as;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/player/as;->g(Lcom/google/android/youtube/core/player/as;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/player/as;->d(Lcom/google/android/youtube/core/player/as;I)I

    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/as;->g()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-static {v0, p2, p3}, Lcom/google/android/youtube/core/player/as;->b(Lcom/google/android/youtube/core/player/as;II)V

    goto :goto_2
.end method

.method public final c()V
    .locals 2

    invoke-static {}, Lcom/google/android/youtube/core/L;->e()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/as;->h()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/as;->e(Lcom/google/android/youtube/core/player/as;)Lcom/google/android/youtube/core/player/af;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/af;->c()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/as;->a(Lcom/google/android/youtube/core/player/as;Z)Z

    return-void
.end method

.method public final c(Lcom/google/android/youtube/core/player/ad;II)V
    .locals 2

    invoke-static {}, Lcom/google/android/youtube/core/L;->e()V

    if-lez p2, :cond_0

    if-gtz p3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/as;->h(Lcom/google/android/youtube/core/player/as;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/as;->e(Lcom/google/android/youtube/core/player/as;)Lcom/google/android/youtube/core/player/af;

    move-result-object v0

    invoke-interface {v0, p2, p3}, Lcom/google/android/youtube/core/player/af;->setVideoSize(II)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/as;->j(Lcom/google/android/youtube/core/player/as;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/as;->c(Lcom/google/android/youtube/core/player/as;Z)Z

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/av;->c(Lcom/google/android/youtube/core/player/ad;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/as;->i(Lcom/google/android/youtube/core/player/as;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/core/player/aw;

    invoke-direct {v1, p0, p2, p3}, Lcom/google/android/youtube/core/player/aw;-><init>(Lcom/google/android/youtube/core/player/av;II)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1
.end method

.method public final m()V
    .locals 4

    const/4 v3, 0x0

    invoke-static {}, Lcom/google/android/youtube/core/L;->e()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/as;->g(Lcom/google/android/youtube/core/player/as;)Lcom/google/android/youtube/core/player/az;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/az;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/as;->g(Lcom/google/android/youtube/core/player/as;)Lcom/google/android/youtube/core/player/az;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/player/az;->a(Lcom/google/android/youtube/core/player/az;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/as;->g(Lcom/google/android/youtube/core/player/as;)Lcom/google/android/youtube/core/player/az;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/player/az;->b(Lcom/google/android/youtube/core/player/az;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    const/16 v2, 0x64

    invoke-static {v1, v0, v2, v0}, Lcom/google/android/youtube/core/player/as;->a(Lcom/google/android/youtube/core/player/as;III)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/player/as;->f(Lcom/google/android/youtube/core/player/as;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/core/player/av;->a:Lcom/google/android/youtube/core/player/as;

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/as;->b(Lcom/google/android/youtube/core/player/as;I)V

    return-void
.end method
