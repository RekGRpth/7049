.class final Lcom/google/android/youtube/core/player/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/player/d;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/core/player/d;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/player/e;->a:Lcom/google/android/youtube/core/player/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/core/player/d;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/e;-><init>(Lcom/google/android/youtube/core/player/d;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2

    check-cast p1, Lcom/google/android/youtube/core/async/GDataRequest;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Couldn\'t retrieve branding info from [uri="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p1, Lcom/google/android/youtube/core/async/GDataRequest;->c:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/e;->a:Lcom/google/android/youtube/core/player/d;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/d;->e(Lcom/google/android/youtube/core/player/d;)Lcom/google/android/youtube/core/player/f;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/f;->a(Lcom/google/android/youtube/core/model/Branding;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    check-cast p2, Lcom/google/android/youtube/core/model/Branding;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/e;->a:Lcom/google/android/youtube/core/player/d;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/Branding;->watermarkTargetUri:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/d;->a(Lcom/google/android/youtube/core/player/d;Landroid/net/Uri;)Landroid/net/Uri;

    iget-object v0, p2, Lcom/google/android/youtube/core/model/Branding;->watermarkUri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/e;->a:Lcom/google/android/youtube/core/player/d;

    new-instance v1, Lcom/google/android/youtube/core/player/g;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/e;->a:Lcom/google/android/youtube/core/player/d;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/google/android/youtube/core/player/g;-><init>(Lcom/google/android/youtube/core/player/d;Z)V

    invoke-static {v1}, Lcom/google/android/youtube/core/async/p;->a(Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/p;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/d;->a(Lcom/google/android/youtube/core/player/d;Lcom/google/android/youtube/core/async/p;)Lcom/google/android/youtube/core/async/p;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/e;->a:Lcom/google/android/youtube/core/player/d;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/d;->c(Lcom/google/android/youtube/core/player/d;)Lcom/google/android/youtube/core/client/be;

    move-result-object v0

    iget-object v1, p2, Lcom/google/android/youtube/core/model/Branding;->watermarkUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/e;->a:Lcom/google/android/youtube/core/player/d;

    invoke-static {v2}, Lcom/google/android/youtube/core/player/d;->a(Lcom/google/android/youtube/core/player/d;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/core/player/e;->a:Lcom/google/android/youtube/core/player/d;

    invoke-static {v3}, Lcom/google/android/youtube/core/player/d;->b(Lcom/google/android/youtube/core/player/d;)Lcom/google/android/youtube/core/async/p;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/async/am;->a(Landroid/os/Handler;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/am;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/be;->a(Landroid/net/Uri;Lcom/google/android/youtube/core/async/n;)V

    :cond_0
    iget-object v0, p2, Lcom/google/android/youtube/core/model/Branding;->tvBannerUri:Landroid/net/Uri;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/e;->a:Lcom/google/android/youtube/core/player/d;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/d;->d(Lcom/google/android/youtube/core/player/d;)Lcom/google/android/youtube/core/player/overlay/c;

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/player/e;->a:Lcom/google/android/youtube/core/player/d;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/d;->e(Lcom/google/android/youtube/core/player/d;)Lcom/google/android/youtube/core/player/f;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/google/android/youtube/core/player/f;->a(Lcom/google/android/youtube/core/model/Branding;)V

    return-void
.end method
