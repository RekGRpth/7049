.class public abstract Lcom/google/android/youtube/core/player/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/player/ad;
.implements Lcom/google/android/youtube/core/player/ae;


# instance fields
.field private final a:Lcom/google/android/youtube/core/player/ad;

.field private b:Lcom/google/android/youtube/core/player/ae;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/player/ad;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/core/player/l;->a:Lcom/google/android/youtube/core/player/ad;

    invoke-interface {p1, p0}, Lcom/google/android/youtube/core/player/ad;->a(Lcom/google/android/youtube/core/player/ae;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/l;->a:Lcom/google/android/youtube/core/player/ad;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ad;->a()V

    return-void
.end method

.method public final a(FF)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/l;->a:Lcom/google/android/youtube/core/player/ad;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/player/ad;->a(FF)V

    return-void
.end method

.method public a(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/l;->a:Lcom/google/android/youtube/core/player/ad;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/ad;->a(I)V

    return-void
.end method

.method public a(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/l;->a:Lcom/google/android/youtube/core/player/ad;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/player/ad;->a(Landroid/content/Context;Landroid/net/Uri;)V

    return-void
.end method

.method public a(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/l;->a:Lcom/google/android/youtube/core/player/ad;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/youtube/core/player/ad;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V

    return-void
.end method

.method public final a(Landroid/view/Surface;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/l;->a:Lcom/google/android/youtube/core/player/ad;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/ad;->a(Landroid/view/Surface;)V

    return-void
.end method

.method public final a(Landroid/view/SurfaceHolder;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/l;->a:Lcom/google/android/youtube/core/player/ad;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/ad;->a(Landroid/view/SurfaceHolder;)V

    return-void
.end method

.method public a(Lcom/google/android/youtube/core/player/ad;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/l;->o()V

    return-void
.end method

.method public a(Lcom/google/android/youtube/core/player/ae;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/player/l;->b:Lcom/google/android/youtube/core/player/ae;

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/l;->a:Lcom/google/android/youtube/core/player/ad;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/ad;->a(Ljava/lang/String;)V

    return-void
.end method

.method public final a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/l;->a:Lcom/google/android/youtube/core/player/ad;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/ad;->a(Z)V

    return-void
.end method

.method protected final a(II)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/l;->b:Lcom/google/android/youtube/core/player/ae;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/l;->b:Lcom/google/android/youtube/core/player/ae;

    invoke-interface {v0, p0, p1, p2}, Lcom/google/android/youtube/core/player/ae;->a(Lcom/google/android/youtube/core/player/ad;II)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/google/android/youtube/core/player/ad;II)Z
    .locals 1

    invoke-virtual {p0, p2, p3}, Lcom/google/android/youtube/core/player/l;->a(II)Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/l;->a:Lcom/google/android/youtube/core/player/ad;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ad;->b()V

    return-void
.end method

.method public b(I)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/player/l;->e(I)V

    return-void
.end method

.method public b(Lcom/google/android/youtube/core/player/ad;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/l;->b:Lcom/google/android/youtube/core/player/ae;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/l;->b:Lcom/google/android/youtube/core/player/ae;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/core/player/ae;->b(Lcom/google/android/youtube/core/player/ad;)V

    :cond_0
    return-void
.end method

.method protected final b(II)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/l;->b:Lcom/google/android/youtube/core/player/ae;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/l;->b:Lcom/google/android/youtube/core/player/ae;

    invoke-interface {v0, p0, p1, p2}, Lcom/google/android/youtube/core/player/ae;->b(Lcom/google/android/youtube/core/player/ad;II)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lcom/google/android/youtube/core/player/ad;II)Z
    .locals 1

    invoke-virtual {p0, p2, p3}, Lcom/google/android/youtube/core/player/l;->b(II)Z

    move-result v0

    return v0
.end method

.method public c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/l;->a:Lcom/google/android/youtube/core/player/ad;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ad;->c()V

    return-void
.end method

.method public final c(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/l;->a:Lcom/google/android/youtube/core/player/ad;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/ad;->c(I)V

    return-void
.end method

.method public final c(Lcom/google/android/youtube/core/player/ad;II)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/l;->b:Lcom/google/android/youtube/core/player/ae;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/l;->b:Lcom/google/android/youtube/core/player/ae;

    invoke-interface {v0, p0, p2, p3}, Lcom/google/android/youtube/core/player/ae;->c(Lcom/google/android/youtube/core/player/ad;II)V

    :cond_0
    return-void
.end method

.method public d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/l;->a:Lcom/google/android/youtube/core/player/ad;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ad;->d()V

    return-void
.end method

.method public final d(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/l;->a:Lcom/google/android/youtube/core/player/ad;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/ad;->d(I)V

    return-void
.end method

.method public e()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/l;->a:Lcom/google/android/youtube/core/player/ad;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ad;->e()V

    return-void
.end method

.method protected final e(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/l;->b:Lcom/google/android/youtube/core/player/ae;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/l;->b:Lcom/google/android/youtube/core/player/ae;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/ae;->b(I)V

    :cond_0
    return-void
.end method

.method public f()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/l;->a:Lcom/google/android/youtube/core/player/ad;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ad;->f()Z

    move-result v0

    return v0
.end method

.method public g()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/l;->a:Lcom/google/android/youtube/core/player/ad;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ad;->g()I

    move-result v0

    return v0
.end method

.method public i()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/l;->a:Lcom/google/android/youtube/core/player/ad;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ad;->i()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public j()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/l;->a:Lcom/google/android/youtube/core/player/ad;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ad;->j()Z

    move-result v0

    return v0
.end method

.method public final l()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/l;->a:Lcom/google/android/youtube/core/player/ad;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ad;->l()I

    move-result v0

    return v0
.end method

.method public final m()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/l;->b:Lcom/google/android/youtube/core/player/ae;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/l;->b:Lcom/google/android/youtube/core/player/ae;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ae;->m()V

    :cond_0
    return-void
.end method

.method public final n()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/l;->a:Lcom/google/android/youtube/core/player/ad;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ad;->n()I

    move-result v0

    return v0
.end method

.method protected final o()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/l;->b:Lcom/google/android/youtube/core/player/ae;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/l;->b:Lcom/google/android/youtube/core/player/ae;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/core/player/ae;->a(Lcom/google/android/youtube/core/player/ad;)V

    :cond_0
    return-void
.end method
