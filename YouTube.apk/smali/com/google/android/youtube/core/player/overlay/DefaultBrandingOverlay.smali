.class public Lcom/google/android/youtube/core/player/overlay/DefaultBrandingOverlay;
.super Landroid/widget/ImageView;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/youtube/core/player/overlay/c;


# instance fields
.field private a:Lcom/google/android/youtube/core/player/overlay/d;

.field private b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-string v0, "context cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {p0, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, p0}, Lcom/google/android/youtube/core/player/overlay/DefaultBrandingOverlay;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultBrandingOverlay;->e()V

    return-void
.end method


# virtual methods
.method public final b()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public final c()Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultBrandingOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x41700000

    mul-float/2addr v1, v0

    float-to-int v1, v1

    const/high16 v2, 0x428c0000

    mul-float/2addr v2, v0

    float-to-int v2, v2

    const/high16 v3, 0x41600000

    mul-float/2addr v0, v3

    float-to-int v0, v0

    new-instance v3, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;

    invoke-direct {v3, v2, v0}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v4, v4, v1, v1}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;->setMargins(IIII)V

    const/16 v0, 0xb

    invoke-virtual {v3, v0}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;->addRule(I)V

    const/16 v0, 0xc

    invoke-virtual {v3, v0}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;->addRule(I)V

    return-object v3
.end method

.method public final d()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultBrandingOverlay;->b:Z

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultBrandingOverlay;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultBrandingOverlay;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public final e()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultBrandingOverlay;->b:Z

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultBrandingOverlay;->setVisibility(I)V

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/core/player/overlay/DefaultBrandingOverlay;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultBrandingOverlay;->setFocusable(Z)V

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/core/player/overlay/DefaultBrandingOverlay;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultBrandingOverlay;->a:Lcom/google/android/youtube/core/player/overlay/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultBrandingOverlay;->a:Lcom/google/android/youtube/core/player/overlay/d;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/d;->b()V

    :cond_0
    return-void
.end method

.method public setListener(Lcom/google/android/youtube/core/player/overlay/d;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/player/overlay/DefaultBrandingOverlay;->a:Lcom/google/android/youtube/core/player/overlay/d;

    return-void
.end method

.method public setTvBanner(Landroid/graphics/Bitmap;)V
    .locals 0

    return-void
.end method

.method public setWatermark(Landroid/graphics/Bitmap;Z)V
    .locals 2

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/player/overlay/DefaultBrandingOverlay;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultBrandingOverlay;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultBrandingOverlay;->setVisibility(I)V

    invoke-virtual {p0, p2}, Lcom/google/android/youtube/core/player/overlay/DefaultBrandingOverlay;->setClickable(Z)V

    invoke-virtual {p0, p2}, Lcom/google/android/youtube/core/player/overlay/DefaultBrandingOverlay;->setFocusable(Z)V

    if-eqz p2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultBrandingOverlay;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0096

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultBrandingOverlay;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    const/4 v0, 0x4

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
