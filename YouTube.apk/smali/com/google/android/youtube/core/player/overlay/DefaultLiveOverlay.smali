.class public Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/youtube/core/player/overlay/i;


# instance fields
.field private final a:Lcom/google/android/youtube/core/utils/e;

.field private final b:Landroid/content/res/Resources;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/ImageView;

.field private final e:Landroid/os/Handler;

.field private f:Lcom/google/android/youtube/core/player/overlay/j;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const v0, 0x7f0201a5

    const v1, 0x7f0201cb

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;-><init>(Landroid/content/Context;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;II)V
    .locals 5

    const/16 v4, 0x11

    const/4 v3, -0x2

    const/4 v2, -0x1

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/google/android/youtube/core/utils/ag;

    invoke-direct {v0}, Lcom/google/android/youtube/core/utils/ag;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;->a:Lcom/google/android/youtube/core/utils/e;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;->b:Landroid/content/res/Resources;

    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;->e:Landroid/os/Handler;

    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;->c:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;->c:Landroid/widget/TextView;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;->c:Landroid/widget/TextView;

    invoke-virtual {p0, v0, v2, v2}, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;->addView(Landroid/view/View;II)V

    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;->d:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, p3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;->d:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;->d:Landroid/widget/ImageView;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v3, v3, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;->d:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;->addView(Landroid/view/View;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;->setClickable(Z)V

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;->setVisibility(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;->d:Landroid/widget/ImageView;

    return-object v0
.end method

.method private a(Landroid/view/View;)V
    .locals 4

    const/4 v2, 0x4

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;->c:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;->c:Landroid/widget/TextView;

    if-ne p1, v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;->d:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;->d:Landroid/widget/ImageView;

    if-ne p1, v3, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;->a(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public final a(JZZ)V
    .locals 8

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;->a:Lcom/google/android/youtube/core/utils/e;

    invoke-interface {v0}, Lcom/google/android/youtube/core/utils/e;->a()J

    move-result-wide v0

    cmp-long v2, v0, p1

    if-gez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;->c:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;->b:Landroid/content/res/Resources;

    const v4, 0x7f0b0084

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, p1, p2, p3, p4}, Lcom/google/android/youtube/core/utils/ah;->a(Landroid/content/Context;JZZ)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;->c:Landroid/widget/TextView;

    invoke-direct {p0, v2}, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;->a(Landroid/view/View;)V

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;->e:Landroid/os/Handler;

    new-instance v3, Lcom/google/android/youtube/core/player/overlay/h;

    invoke-direct {v3, p0}, Lcom/google/android/youtube/core/player/overlay/h;-><init>(Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;)V

    sub-long v0, p1, v0

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_0
    invoke-virtual {p0, v7}, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;->setVisibility(I)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;->d:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;->a(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final b()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public final c()Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;
    .locals 3

    const/4 v2, -0x1

    new-instance v0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;

    const/4 v1, 0x1

    invoke-direct {v0, v2, v2, v1}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;-><init>(IIZ)V

    return-object v0
.end method

.method public final d()V
    .locals 1

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;->setVisibility(I)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;->f:Lcom/google/android/youtube/core/player/overlay/j;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;->d:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;->f:Lcom/google/android/youtube/core/player/overlay/j;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/overlay/j;->v_()V

    :goto_0
    return-void

    :cond_0
    const-string v0, "Play button clicked in LiveOverlay, but no listener was registered"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setListener(Lcom/google/android/youtube/core/player/overlay/j;)V
    .locals 1

    const-string v0, "listener cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/overlay/j;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;->f:Lcom/google/android/youtube/core/player/overlay/j;

    return-void
.end method
