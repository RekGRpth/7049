.class public final Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/Map;


# instance fields
.field private final b:Lcom/google/android/youtube/core/player/overlay/o;

.field private final c:Ljava/util/Map;

.field private final d:I

.field private final e:Landroid/widget/ImageView;

.field private final f:Landroid/view/animation/Animation;

.field private final g:Landroid/view/animation/Animation;

.field private final h:Landroid/os/Handler;

.field private i:D

.field private j:Z

.field private k:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->FAST_FORWARD:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    const v2, 0x7f02020c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->REWIND:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    const v2, 0x7f020210

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->PAUSE:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    const v2, 0x7f02020e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->PLAY:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    const v2, 0x7f02020f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->NEXT:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    const v2, 0x7f02020d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->PREVIOUS:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    const v2, 0x7f02020a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->a:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/core/player/overlay/o;Landroid/widget/ImageView;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object p2, p0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->b:Lcom/google/android/youtube/core/player/overlay/o;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->a:Ljava/util/Map;

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->a(Landroid/content/res/Resources;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->c:Ljava/util/Map;

    iput-object p3, p0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->e:Landroid/widget/ImageView;

    new-instance v1, Landroid/os/Handler;

    new-instance v2, Lcom/google/android/youtube/core/player/overlay/m;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/core/player/overlay/m;-><init>(Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;)V

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v1, p0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->h:Landroid/os/Handler;

    const v1, 0x7f0a0003

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->d:I

    const v0, 0x7f050003

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->f:Landroid/view/animation/Animation;

    const v0, 0x7f050004

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->g:Landroid/view/animation/Animation;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;)D
    .locals 2

    iget-wide v0, p0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->i:D

    return-wide v0
.end method

.method private static a(Landroid/content/res/Resources;Ljava/util/Map;)Ljava/util/Map;
    .locals 8

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    const v0, 0x7f02020b

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    instance-of v0, v1, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_0

    move-object v0, v1

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    :cond_0
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    instance-of v2, v3, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v2, :cond_1

    move-object v2, v3

    check-cast v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    :cond_1
    new-instance v2, Landroid/graphics/drawable/LayerDrawable;

    const/4 v6, 0x2

    new-array v6, v6, [Landroid/graphics/drawable/Drawable;

    const/4 v7, 0x0

    aput-object v1, v6, v7

    const/4 v7, 0x1

    aput-object v3, v6, v7

    invoke-direct {v2, v6}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v4, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    invoke-static {v4}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method private a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->g:Landroid/view/animation/Animation;

    new-instance v1, Lcom/google/android/youtube/core/player/overlay/l;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/core/player/overlay/l;-><init>(Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->g:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->reset()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->e:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->g:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method private a(Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;Z)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->f:Landroid/view/animation/Animation;

    new-instance v1, Lcom/google/android/youtube/core/player/overlay/k;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/android/youtube/core/player/overlay/k;-><init>(Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;ZLcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->f:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->reset()V

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->e:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->e:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->f:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->a()V

    return-void
.end method

.method static synthetic b(Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;)Lcom/google/android/youtube/core/player/overlay/o;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->b:Lcom/google/android/youtube/core/player/overlay/o;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;)V
    .locals 1

    invoke-virtual {p1}, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->isRepeatable()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/s;->a(Z)V

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->c(Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;)V

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->d(Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->e:Landroid/widget/ImageView;

    return-object v0
.end method

.method private c(Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;)V
    .locals 4

    invoke-virtual {p1}, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->isRepeatable()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/s;->a(Z)V

    invoke-virtual {p1, p0}, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->execute(Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;)V

    iget-wide v0, p0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->i:D

    const-wide v2, 0x3ff199999999999aL

    mul-double/2addr v0, v2

    const-wide/high16 v2, 0x4024000000000000L

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->i:D

    return-void
.end method

.method private d(Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;)V
    .locals 4

    invoke-virtual {p1}, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->isRepeatable()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/s;->a(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->h:Landroid/os/Handler;

    const/16 v1, 0xea

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->h:Landroid/os/Handler;

    iget v2, p0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->d:I

    int-to-long v2, v2

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->b:Lcom/google/android/youtube/core/player/overlay/o;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/overlay/o;->a(I)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->k:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->isRepeatable()Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object p1, p0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->k:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->a(Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;Z)V

    invoke-virtual {p1}, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->isRepeatable()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/s;->a(Z)V

    iput-object p1, p0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->k:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    const-wide/high16 v0, 0x3ff0000000000000L

    iput-wide v0, p0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->i:D

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->c(Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;)V

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->d(Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->a(Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;Z)V

    invoke-virtual {p1, p0}, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->execute(Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 2

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->j:Z

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->k:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->REWIND:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->k:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->FAST_FORWARD:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    if-ne v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->k:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->b(Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;)V

    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;)Z
    .locals 2

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->b(Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;)Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$ActionState;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$ActionState;->ACTIVE:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$ActionState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;)Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$ActionState;
    .locals 2

    sget-object v0, Lcom/google/android/youtube/core/player/overlay/n;->a:[I

    invoke-virtual {p1}, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    sget-object v0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$ActionState;->ACTIVE:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$ActionState;

    :goto_0
    return-object v0

    :pswitch_0
    iget-boolean v0, p2, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->supportsNextPrevious:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$ActionState;->ACTIVE:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$ActionState;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$ActionState;->GONE:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$ActionState;

    goto :goto_0

    :pswitch_1
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->j:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p2, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->supportsScrubber:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$ActionState;->ACTIVE:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$ActionState;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$ActionState;->GONE:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$ActionState;

    goto :goto_0

    :pswitch_2
    iget-boolean v0, p2, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->supportsScrubber:Z

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$ActionState;->ACTIVE:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$ActionState;

    goto :goto_0

    :cond_2
    iget-boolean v0, p2, Lcom/google/android/youtube/core/player/overlay/ControllerOverlay$Style;->supportsTimeBar:Z

    if-eqz v0, :cond_3

    sget-object v0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$ActionState;->INACTIVE:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$ActionState;

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$ActionState;->GONE:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$ActionState;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b(Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->k:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    if-ne v0, p1, :cond_1

    invoke-virtual {p1}, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->isRepeatable()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->a()V

    invoke-virtual {p1}, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;->isRepeatable()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/s;->a(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->h:Landroid/os/Handler;

    const/16 v1, 0xea

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/MediaActionHelper;->k:Lcom/google/android/youtube/core/player/overlay/MediaActionHelper$Action;

    :cond_1
    return-void
.end method
