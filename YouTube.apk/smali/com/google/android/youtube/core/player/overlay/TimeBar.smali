.class public Lcom/google/android/youtube/core/player/overlay/TimeBar;
.super Landroid/view/View;
.source "SourceFile"


# static fields
.field private static final a:[I

.field private static final b:[I

.field private static final c:[I


# instance fields
.field private final d:Lcom/google/android/youtube/core/player/overlay/ac;

.field private final e:Landroid/graphics/Rect;

.field private final f:Landroid/graphics/Rect;

.field private final g:Landroid/graphics/Rect;

.field private final h:Landroid/graphics/Paint;

.field private final i:Landroid/graphics/Paint;

.field private final j:Landroid/graphics/Paint;

.field private final k:Landroid/graphics/Paint;

.field private final l:Landroid/graphics/drawable/StateListDrawable;

.field private final m:I

.field private n:I

.field private o:I

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:I

.field private v:I

.field private w:I

.field private x:I

.field private y:Ljava/lang/String;

.field private final z:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v3, [I

    const v1, 0x101009e

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->a:[I

    new-array v0, v3, [I

    const v1, -0x101009e

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->b:[I

    new-array v0, v3, [I

    const v1, 0x10100a7

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->c:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/core/player/overlay/ac;)V
    .locals 6

    const/4 v4, -0x1

    const/4 v3, 0x1

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    invoke-static {p2}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/overlay/ac;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->d:Lcom/google/android/youtube/core/player/overlay/ac;

    iput-boolean v3, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->r:Z

    iput-boolean v3, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->s:Z

    iput-boolean v3, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->t:Z

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->e:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->f:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->g:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->h:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->h:Landroid/graphics/Paint;

    const v1, -0x7f7f80

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->i:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->i:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->j:Landroid/graphics/Paint;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v0, v1, Landroid/util/DisplayMetrics;->density:F

    const/high16 v2, 0x41700000

    mul-float/2addr v0, v2

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->k:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->k:Landroid/graphics/Paint;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->k:Landroid/graphics/Paint;

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->k:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->z:Landroid/graphics/Rect;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->k:Landroid/graphics/Paint;

    const-string v2, "0:00:00"

    const/4 v3, 0x0

    const/4 v4, 0x7

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->z:Landroid/graphics/Rect;

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    const-wide/16 v2, 0x0

    invoke-direct {p0, v2, v3}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->a(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->y:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0201df

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/StateListDrawable;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->l:Landroid/graphics/drawable/StateListDrawable;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->l:Landroid/graphics/drawable/StateListDrawable;

    sget-object v2, Lcom/google/android/youtube/core/player/overlay/TimeBar;->b:[I

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/StateListDrawable;->setState([I)Z

    iget v0, v1, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x41000000

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->m:I

    return-void
.end method

.method private a(J)Ljava/lang/String;
    .locals 8

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    long-to-int v0, p1

    div-int/lit16 v0, v0, 0x3e8

    rem-int/lit8 v1, v0, 0x3c

    div-int/lit8 v2, v0, 0x3c

    rem-int/lit8 v2, v2, 0x3c

    div-int/lit16 v0, v0, 0xe10

    iget v3, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->u:I

    const v4, 0x36ee80

    if-lt v3, v4, :cond_0

    const-string v3, "%d:%02d:%02d"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "%02d:%02d"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v6

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(F)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->l:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->e:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    sub-int/2addr v1, v0

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->e:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v0

    float-to-int v3, p1

    sub-int v0, v3, v0

    iput v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->n:I

    iget v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->n:I

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->n:I

    return-void
.end method

.method private d()V
    .locals 7

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->f:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->e:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->g:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->e:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->q:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->w:I

    :goto_0
    iget v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->u:I

    if-lez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->f:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->f:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->e:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    iget v4, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->x:I

    mul-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x64

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->g:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->g:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->e:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-long v3, v3

    iget v5, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->v:I

    int-to-long v5, v5

    mul-long/2addr v3, v5

    iget v5, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->u:I

    int-to-long v5, v5

    div-long/2addr v3, v5

    long-to-int v3, v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->g:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->l:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->e:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-long v2, v2

    int-to-long v4, v0

    mul-long/2addr v2, v4

    iget v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->u:I

    int-to-long v4, v0

    div-long/2addr v2, v4

    long-to-int v0, v2

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->n:I

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->invalidate()V

    return-void

    :cond_0
    iget v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->v:I

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->f:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->e:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iput v1, v0, Landroid/graphics/Rect;->right:I

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->g:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->e:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iput v1, v0, Landroid/graphics/Rect;->right:I

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->e:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->l:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->n:I

    goto :goto_1
.end method

.method private e()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->u:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->p:Z

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->q:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->p:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->g()V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->d()V

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->l:Landroid/graphics/drawable/StateListDrawable;

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->q:Z

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->c:[I

    :goto_2
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/StateListDrawable;->setState([I)Z

    goto :goto_1

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->p:Z

    if-eqz v0, :cond_3

    sget-object v0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->a:[I

    goto :goto_2

    :cond_3
    sget-object v0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->b:[I

    goto :goto_2
.end method

.method private f()I
    .locals 4

    iget v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->n:I

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->l:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->e:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    int-to-long v0, v0

    iget v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->u:I

    int-to-long v2, v2

    mul-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->e:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-long v2, v2

    div-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method private g()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->q:Z

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->e()V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->invalidate()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v0, v0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->setTime(III)V

    return-void
.end method

.method public final b()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->z:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->l:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicHeight()I

    move-result v1

    add-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->m:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final c()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->l:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicHeight()I

    move-result v0

    iget v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->m:I

    add-int/2addr v0, v1

    return v0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 6

    invoke-static {}, Lcom/google/android/youtube/core/L;->e()V

    invoke-super {p0, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->e:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->h:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->t:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->f:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->i:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->g:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->j:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->s:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->l:Landroid/graphics/drawable/StateListDrawable;

    iget v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->n:I

    iget v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->o:I

    iget v3, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->n:I

    iget-object v4, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->l:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicWidth()I

    move-result v4

    add-int/2addr v3, v4

    iget v4, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->o:I

    iget-object v5, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->l:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicHeight()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/StateListDrawable;->setBounds(IIII)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->l:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/StateListDrawable;->draw(Landroid/graphics/Canvas;)V

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->r:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->q:Z

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->w:I

    int-to-long v0, v0

    :goto_0
    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->a(J)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->z:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->getPaddingLeft()I

    move-result v2

    add-int/2addr v1, v2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->z:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->k:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->y:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->z:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->z:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->k:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :cond_2
    return-void

    :cond_3
    iget v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->v:I

    int-to-long v0, v0

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 6

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->r:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->s:Z

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->b()I

    move-result v0

    :goto_0
    invoke-static {v1, p1}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->getDefaultSize(II)I

    move-result v2

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->resolveSize(II)I

    move-result v0

    invoke-virtual {p0, v2, v0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->setMeasuredDimension(II)V

    iget-boolean v3, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->r:Z

    if-nez v3, :cond_2

    iget-boolean v3, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->s:Z

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->e:Landroid/graphics/Rect;

    invoke-virtual {v3, v1, v1, v2, v0}, Landroid/graphics/Rect;->set(IIII)V

    :goto_1
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->d()V

    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->l:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x3

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->z:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    iget v3, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->m:I

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v1, v3

    iput v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->o:I

    iget v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->o:I

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->l:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v1, v3

    add-int/lit8 v1, v1, -0x1

    iget-object v3, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->e:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->getPaddingLeft()I

    move-result v4

    add-int/2addr v4, v0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->getPaddingRight()I

    move-result v5

    sub-int/2addr v2, v5

    sub-int v0, v2, v0

    add-int/lit8 v2, v1, 0x4

    invoke-virtual {v3, v4, v1, v0, v2}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9

    const/4 v1, 0x0

    const/4 v0, 0x1

    iget-boolean v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->p:Z

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v3, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    :cond_0
    move v0, v1

    :goto_0
    return v0

    :pswitch_0
    int-to-float v4, v3

    int-to-float v2, v2

    iget v5, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->o:I

    iget-object v6, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->l:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicHeight()I

    move-result v6

    add-int/2addr v5, v6

    iget-object v6, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->e:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    iget-object v7, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->l:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v7}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicWidth()I

    move-result v7

    sub-int/2addr v6, v7

    iget-object v7, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->e:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->right:I

    iget-object v8, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->l:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v8}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicWidth()I

    move-result v8

    add-int/2addr v7, v8

    int-to-float v6, v6

    cmpg-float v6, v6, v4

    if-gez v6, :cond_1

    int-to-float v6, v7

    cmpg-float v4, v4, v6

    if-gez v4, :cond_1

    iget v4, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->o:I

    iget v6, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->m:I

    sub-int/2addr v4, v6

    int-to-float v4, v4

    cmpg-float v4, v4, v2

    if-gez v4, :cond_1

    iget v4, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->m:I

    add-int/2addr v4, v5

    int-to-float v4, v4

    cmpg-float v2, v2, v4

    if-gez v2, :cond_1

    move v2, v0

    :goto_1
    if-eqz v2, :cond_0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->q:Z

    int-to-float v1, v3

    invoke-direct {p0, v1}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->a(F)V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->f()I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->w:I

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->d:Lcom/google/android/youtube/core/player/overlay/ac;

    invoke-interface {v1}, Lcom/google/android/youtube/core/player/overlay/ac;->a()V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->e()V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->d()V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->invalidate()V

    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_1

    :pswitch_1
    iget-boolean v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->q:Z

    if-eqz v2, :cond_0

    int-to-float v1, v3

    invoke-direct {p0, v1}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->a(F)V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->f()I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->w:I

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->d()V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->invalidate()V

    goto :goto_0

    :pswitch_2
    iget-boolean v2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->q:Z

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->g()V

    iget v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->w:I

    iput v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->v:I

    iget-object v1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->d:Lcom/google/android/youtube/core/player/overlay/ac;

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->f()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/google/android/youtube/core/player/overlay/ac;->a(I)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setBufferedPercent(I)V
    .locals 0

    iput p1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->x:I

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->d()V

    return-void
.end method

.method public setEnabled(Z)V
    .locals 0

    invoke-super {p0, p1}, Landroid/view/View;->setEnabled(Z)V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->e()V

    return-void
.end method

.method public setProgressColor(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->j:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->d()V

    return-void
.end method

.method public setScrubberTime(I)V
    .locals 0

    iput p1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->w:I

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->d()V

    return-void
.end method

.method public setScrubbing(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->q:Z

    return-void
.end method

.method public setShowBuffered(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->t:Z

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->d()V

    return-void
.end method

.method public setShowScrubber(Z)V
    .locals 2

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->s:Z

    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->q:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->d:Lcom/google/android/youtube/core/player/overlay/ac;

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->f()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/overlay/ac;->a(I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->q:Z

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->requestLayout()V

    return-void
.end method

.method public setShowTimes(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->r:Z

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->requestLayout()V

    return-void
.end method

.method public setTime(III)V
    .locals 2

    iget v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->v:I

    if-ne v0, p1, :cond_0

    iget v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->u:I

    if-ne v0, p2, :cond_0

    iget v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->x:I

    if-eq v0, p3, :cond_2

    :cond_0
    iget v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->u:I

    if-eq v0, p2, :cond_1

    iput p2, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->u:I

    int-to-long v0, p2

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->a(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->y:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->e()V

    :cond_1
    iput p1, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->v:I

    iput p3, p0, Lcom/google/android/youtube/core/player/overlay/TimeBar;->x:I

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/overlay/TimeBar;->d()V

    :cond_2
    return-void
.end method
