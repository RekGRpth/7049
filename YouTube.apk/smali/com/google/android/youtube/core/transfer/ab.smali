.class public final Lcom/google/android/youtube/core/transfer/ab;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/transfer/m;


# static fields
.field private static final a:Ljava/util/regex/Pattern;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Landroid/app/NotificationManager;

.field private final d:Lcom/google/android/youtube/core/Analytics;

.field private final e:Lorg/apache/http/client/HttpClient;

.field private final f:Lcom/google/android/youtube/core/client/bc;

.field private final g:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private final h:Lcom/google/android/youtube/core/transfer/Transfer;

.field private final i:Ljava/lang/String;

.field private final j:Lcom/google/android/youtube/core/converter/http/fq;

.field private final k:Lcom/google/android/youtube/core/transfer/n;

.field private final l:Ljava/util/concurrent/Executor;

.field private final m:Ljava/lang/Object;

.field private volatile n:Z

.field private volatile o:Lorg/apache/http/client/methods/HttpUriRequest;

.field private volatile p:Z

.field private q:J

.field private r:J

.field private final s:Landroid/os/ConditionVariable;

.field private final t:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "bytes=(\\d+)-(\\d+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/core/transfer/ab;->a:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/converter/http/fq;Lcom/google/android/youtube/core/transfer/Transfer;Lcom/google/android/youtube/core/transfer/n;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/transfer/ab;->t:Ljava/util/List;

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/youtube/core/transfer/ab;->b:Landroid/content/Context;

    const-string v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/google/android/youtube/core/transfer/ab;->c:Landroid/app/NotificationManager;

    invoke-static {p3}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/client/HttpClient;

    iput-object v0, p0, Lcom/google/android/youtube/core/transfer/ab;->e:Lorg/apache/http/client/HttpClient;

    invoke-static {p2}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/youtube/core/transfer/ab;->l:Ljava/util/concurrent/Executor;

    invoke-static {p4}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/client/bc;

    iput-object v0, p0, Lcom/google/android/youtube/core/transfer/ab;->f:Lcom/google/android/youtube/core/client/bc;

    invoke-static {p5}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/UserAuthorizer;

    iput-object v0, p0, Lcom/google/android/youtube/core/transfer/ab;->g:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-static {p6}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/Analytics;

    iput-object v0, p0, Lcom/google/android/youtube/core/transfer/ab;->d:Lcom/google/android/youtube/core/Analytics;

    invoke-static {p7}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/converter/http/fq;

    iput-object v0, p0, Lcom/google/android/youtube/core/transfer/ab;->j:Lcom/google/android/youtube/core/converter/http/fq;

    invoke-static {p8}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/transfer/Transfer;

    iput-object v0, p0, Lcom/google/android/youtube/core/transfer/ab;->h:Lcom/google/android/youtube/core/transfer/Transfer;

    iget-object v0, p8, Lcom/google/android/youtube/core/transfer/Transfer;->g:Lcom/google/android/youtube/core/transfer/d;

    const-string v1, "authAccount"

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/transfer/d;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/transfer/ab;->i:Ljava/lang/String;

    invoke-static {p9}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/transfer/n;

    iput-object v0, p0, Lcom/google/android/youtube/core/transfer/ab;->k:Lcom/google/android/youtube/core/transfer/n;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/transfer/ab;->m:Ljava/lang/Object;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/transfer/ab;->n:Z

    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/transfer/ab;->s:Landroid/os/ConditionVariable;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/core/transfer/ab;J)J
    .locals 0

    iput-wide p1, p0, Lcom/google/android/youtube/core/transfer/ab;->r:J

    return-wide p1
.end method

.method private a(Ljava/io/File;JZ)Lorg/apache/http/HttpResponse;
    .locals 10

    iget-boolean v0, p0, Lcom/google/android/youtube/core/transfer/ab;->p:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-wide/16 v0, 0x1

    add-long v3, p2, v0

    iget-wide v0, p0, Lcom/google/android/youtube/core/transfer/ab;->q:J

    const-wide/16 v5, 0x1

    sub-long v1, v0, v5

    if-eqz p4, :cond_1

    const/4 v0, 0x0

    :goto_1
    int-to-long v5, v0

    sub-long v0, v1, v5

    if-nez p4, :cond_2

    cmp-long v2, v3, v0

    if-lez v2, :cond_2

    new-instance v0, Lorg/apache/http/message/BasicHttpResponse;

    new-instance v1, Lorg/apache/http/message/BasicStatusLine;

    sget-object v2, Lorg/apache/http/HttpVersion;->HTTP_1_1:Lorg/apache/http/HttpVersion;

    const/16 v3, 0x134

    const-string v4, "Already uploaded all possible content for a gated upload."

    invoke-direct {v1, v2, v3, v4}, Lorg/apache/http/message/BasicStatusLine;-><init>(Lorg/apache/http/ProtocolVersion;ILjava/lang/String;)V

    invoke-direct {v0, v1}, Lorg/apache/http/message/BasicHttpResponse;-><init>(Lorg/apache/http/StatusLine;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lcom/google/android/youtube/core/transfer/ab;->k:Lcom/google/android/youtube/core/transfer/n;

    iget-object v5, p0, Lcom/google/android/youtube/core/transfer/ab;->h:Lcom/google/android/youtube/core/transfer/Transfer;

    iget-object v5, v5, Lcom/google/android/youtube/core/transfer/Transfer;->a:Ljava/lang/String;

    invoke-interface {v2, v5, p2, p3}, Lcom/google/android/youtube/core/transfer/n;->b(Ljava/lang/String;J)V

    iget-object v2, p0, Lcom/google/android/youtube/core/transfer/ab;->k:Lcom/google/android/youtube/core/transfer/n;

    iget-object v5, p0, Lcom/google/android/youtube/core/transfer/ab;->h:Lcom/google/android/youtube/core/transfer/Transfer;

    iget-object v5, v5, Lcom/google/android/youtube/core/transfer/Transfer;->a:Ljava/lang/String;

    iget-wide v6, p0, Lcom/google/android/youtube/core/transfer/ab;->q:J

    invoke-interface {v2, v5, v6, v7}, Lcom/google/android/youtube/core/transfer/n;->a(Ljava/lang/String;J)V

    new-instance v7, Lorg/apache/http/client/methods/HttpPut;

    iget-object v2, p0, Lcom/google/android/youtube/core/transfer/ab;->h:Lcom/google/android/youtube/core/transfer/Transfer;

    iget-object v2, v2, Lcom/google/android/youtube/core/transfer/Transfer;->b:Ljava/lang/String;

    invoke-direct {v7, v2}, Lorg/apache/http/client/methods/HttpPut;-><init>(Ljava/lang/String;)V

    const-string v2, "Content-Type"

    const-string v5, "application/octet-stream"

    invoke-virtual {v7, v2, v5}, Lorg/apache/http/client/methods/HttpPut;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "Content-Range"

    const-string v5, "bytes %d-%d/%d"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v6, v8

    const/4 v8, 0x1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v6, v8

    const/4 v0, 0x2

    iget-wide v8, p0, Lcom/google/android/youtube/core/transfer/ab;->q:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v6, v0

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v2, v0}, Lorg/apache/http/client/methods/HttpPut;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v2, v3, v4}, Ljava/io/FileInputStream;->skip(J)J

    move-result-wide v0

    cmp-long v0, v0, v3

    if-eqz v0, :cond_3

    new-instance v0, Lcom/google/android/youtube/core/transfer/TransferException;

    const-string v1, "unable to skip to upload position"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/transfer/TransferException;-><init>(Ljava/lang/String;Z)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/youtube/core/transfer/TransferException;

    const/4 v2, 0x1

    invoke-direct {v1, v0, v2}, Lcom/google/android/youtube/core/transfer/TransferException;-><init>(Ljava/lang/Throwable;Z)V

    throw v1

    :cond_3
    :try_start_1
    new-instance v0, Lcom/google/android/youtube/core/transfer/ad;

    iget-wide v5, p0, Lcom/google/android/youtube/core/transfer/ab;->q:J

    sub-long v3, v5, v3

    if-eqz p4, :cond_4

    const/4 v1, 0x0

    :goto_2
    int-to-long v5, v1

    sub-long/2addr v3, v5

    move-object v1, p0

    move-wide v5, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/core/transfer/ad;-><init>(Lcom/google/android/youtube/core/transfer/ab;Ljava/io/InputStream;JJ)V

    invoke-virtual {v7, v0}, Lorg/apache/http/client/methods/HttpPut;->setEntity(Lorg/apache/http/HttpEntity;)V

    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/ab;->m:Ljava/lang/Object;

    monitor-enter v1
    :try_end_1
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    :try_start_2
    iput-object v7, p0, Lcom/google/android/youtube/core/transfer/ab;->o:Lorg/apache/http/client/methods/HttpUriRequest;

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/ab;->e:Lorg/apache/http/client/HttpClient;

    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/ab;->o:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v0, v1}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    goto/16 :goto_0

    :cond_4
    const/4 v1, 0x1

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
    :try_end_3
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_1
    move-exception v0

    invoke-direct {p0}, Lcom/google/android/youtube/core/transfer/ab;->c()V

    new-instance v1, Lcom/google/android/youtube/core/transfer/TransferException;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/google/android/youtube/core/transfer/TransferException;-><init>(Ljava/lang/Throwable;Z)V

    throw v1

    :catch_2
    move-exception v0

    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/ab;->m:Ljava/lang/Object;

    monitor-enter v1

    :try_start_4
    iget-object v2, p0, Lcom/google/android/youtube/core/transfer/ab;->o:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v2}, Lorg/apache/http/client/methods/HttpUriRequest;->isAborted()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    const/4 v0, 0x0

    monitor-exit v1

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    :cond_5
    :try_start_5
    iget-object v2, p0, Lcom/google/android/youtube/core/transfer/ab;->o:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v2}, Lorg/apache/http/client/methods/HttpUriRequest;->abort()V

    new-instance v2, Lcom/google/android/youtube/core/transfer/TransferException;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3}, Lcom/google/android/youtube/core/transfer/TransferException;-><init>(Ljava/lang/Throwable;Z)V

    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1
.end method

.method private a(Lcom/google/android/youtube/core/model/Video;)V
    .locals 11

    const v6, 0x7f0b0035

    const/4 v10, 0x1

    const/4 v1, 0x0

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->state:Lcom/google/android/youtube/core/model/Video$State;

    sget-object v2, Lcom/google/android/youtube/core/model/Video$State;->PLAYABLE:Lcom/google/android/youtube/core/model/Video$State;

    if-eq v0, v2, :cond_0

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->state:Lcom/google/android/youtube/core/model/Video$State;

    sget-object v2, Lcom/google/android/youtube/core/model/Video$State;->PROCESSING:Lcom/google/android/youtube/core/model/Video$State;

    if-ne v0, v2, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    const/16 v2, 0xa

    if-ge v0, v2, :cond_6

    add-int/lit8 v2, v0, 0x1

    const-wide/16 v3, 0x7530

    :try_start_0
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_1
    :try_start_1
    invoke-direct {p0}, Lcom/google/android/youtube/core/transfer/ab;->d()Lcom/google/android/youtube/core/model/UserAuth;
    :try_end_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    if-nez v0, :cond_4

    :cond_1
    :goto_2
    invoke-direct {p0}, Lcom/google/android/youtube/core/transfer/ab;->d()Lcom/google/android/youtube/core/model/UserAuth;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/ab;->b:Landroid/content/Context;

    invoke-virtual {v0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/youtube/core/transfer/ab;->b:Landroid/content/Context;

    invoke-virtual {v2, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    new-instance v5, Landroid/content/Intent;

    const-string v6, "android.intent.action.VIEW"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "http://www.youtube.com/watch?v="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    iget-object v6, p0, Lcom/google/android/youtube/core/transfer/ab;->b:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v5}, Landroid/content/Intent;->getFlags()I

    move-result v6

    const/high16 v7, 0x10000000

    or-int/2addr v6, v7

    invoke-virtual {v5, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v6, "authenticate"

    invoke-virtual {v5, v6, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v6, "uploader_notification"

    invoke-virtual {v5, v6, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v6, "feature"

    sget-object v7, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->UPLOAD_NOTIFICATION:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    iget-object v6, p0, Lcom/google/android/youtube/core/transfer/ab;->b:Landroid/content/Context;

    const/high16 v7, 0x40000000

    invoke-static {v6, v1, v5, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    new-instance v6, Landroid/app/Notification;

    const v7, 0x7f020196

    invoke-direct {v6, v7, v0, v3, v4}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    iget-object v3, p0, Lcom/google/android/youtube/core/transfer/ab;->b:Landroid/content/Context;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->title:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/ab;->h:Lcom/google/android/youtube/core/transfer/Transfer;

    iget-object v0, v0, Lcom/google/android/youtube/core/transfer/Transfer;->g:Lcom/google/android/youtube/core/transfer/d;

    const-string v7, "upload_social_post_networks"

    invoke-virtual {v0, v7}, Lcom/google/android/youtube/core/transfer/d;->c(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/ab;->t:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    const-string v0, " "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/ab;->t:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v10, :cond_7

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/ab;->b:Landroid/content/Context;

    const v7, 0x7f0b0037

    new-array v8, v10, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/google/android/youtube/core/transfer/ab;->t:Ljava/util/List;

    invoke-interface {v9, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    aput-object v9, v8, v1

    invoke-virtual {v0, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    :goto_4
    invoke-virtual {v6, v3, v2, v4, v5}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    const/16 v0, 0x10

    iput v0, v6, Landroid/app/Notification;->flags:I

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/ab;->c:Landroid/app/NotificationManager;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-virtual {v0, v1, v10, v6}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    :cond_3
    return-void

    :cond_4
    :try_start_2
    new-instance v3, Lcom/google/android/youtube/core/async/bd;

    invoke-direct {v3}, Lcom/google/android/youtube/core/async/bd;-><init>()V

    iget-object v4, p0, Lcom/google/android/youtube/core/transfer/ab;->f:Lcom/google/android/youtube/core/client/bc;

    iget-object v5, p1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-interface {v4, v5, v0, v3}, Lcom/google/android/youtube/core/client/bc;->b(Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V

    invoke-virtual {v3}, Lcom/google/android/youtube/core/async/bd;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Video;

    iget-object v3, v0, Lcom/google/android/youtube/core/model/Video;->state:Lcom/google/android/youtube/core/model/Video$State;

    sget-object v4, Lcom/google/android/youtube/core/model/Video$State;->PROCESSING:Lcom/google/android/youtube/core/model/Video$State;

    if-eq v3, v4, :cond_5

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Video;->streams:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V
    :try_end_2
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_2

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    move v0, v2

    goto/16 :goto_0

    :cond_5
    move v0, v2

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/ab;->d:Lcom/google/android/youtube/core/Analytics;

    const-string v2, "UploadTranscodingWaitAbort"

    invoke-interface {v0, v2}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    goto/16 :goto_2

    :cond_7
    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/ab;->b:Landroid/content/Context;

    const v7, 0x7f0b0036

    new-array v8, v10, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/google/android/youtube/core/transfer/ab;->t:Ljava/util/List;

    invoke-interface {v9, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    aput-object v9, v8, v1

    invoke-virtual {v0, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    :cond_8
    if-eqz v7, :cond_2

    array-length v0, v7

    if-lez v0, :cond_2

    sget-object v0, Lcom/google/android/youtube/core/model/SocialSettings;->ID_TO_NETWORK:Ljava/util/Map;

    aget-object v8, v7, v1

    invoke-interface {v0, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;->name:Ljava/lang/String;

    const-string v8, " "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    array-length v7, v7

    if-le v7, v10, :cond_9

    iget-object v7, p0, Lcom/google/android/youtube/core/transfer/ab;->b:Landroid/content/Context;

    const v8, 0x7f0b0039

    new-array v9, v10, [Ljava/lang/Object;

    aput-object v0, v9, v1

    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_5
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    :cond_9
    iget-object v7, p0, Lcom/google/android/youtube/core/transfer/ab;->b:Landroid/content/Context;

    const v8, 0x7f0b0038

    new-array v9, v10, [Ljava/lang/Object;

    aput-object v0, v9, v1

    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    :catch_1
    move-exception v0

    goto/16 :goto_1
.end method

.method static synthetic a(Lcom/google/android/youtube/core/transfer/ab;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/core/transfer/ab;->c()V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 12

    const/4 v8, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/ab;->h:Lcom/google/android/youtube/core/transfer/Transfer;

    iget-object v0, v0, Lcom/google/android/youtube/core/transfer/Transfer;->g:Lcom/google/android/youtube/core/transfer/d;

    const-string v1, "upload_social_post_networks"

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/transfer/d;->c(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_4

    array-length v0, v9

    if-lez v0, :cond_4

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    array-length v11, v9

    move v7, v8

    :goto_0
    if-ge v7, v11, :cond_3

    aget-object v3, v9, v7

    new-instance v6, Lcom/google/android/youtube/core/async/bd;

    invoke-direct {v6}, Lcom/google/android/youtube/core/async/bd;-><init>()V

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/ab;->f:Lcom/google/android/youtube/core/client/bc;

    sget-object v1, Lcom/google/android/youtube/core/model/SocialSettings$Action$ActionType;->UPLOAD:Lcom/google/android/youtube/core/model/SocialSettings$Action$ActionType;

    iget-object v2, v1, Lcom/google/android/youtube/core/model/SocialSettings$Action$ActionType;->action:Ljava/lang/String;

    const-string v4, ""

    invoke-direct {p0}, Lcom/google/android/youtube/core/transfer/ab;->d()Lcom/google/android/youtube/core/model/UserAuth;

    move-result-object v5

    move-object v1, p1

    invoke-interface/range {v0 .. v6}, Lcom/google/android/youtube/core/client/bc;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V

    :try_start_0
    invoke-virtual {v6}, Lcom/google/android/youtube/core/async/bd;->a()Ljava/lang/Object;

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ","

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v1, v0

    sget-object v0, Lcom/google/android/youtube/core/model/SocialSettings;->ID_TO_NETWORK:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/google/android/youtube/core/transfer/ab;->t:Ljava/util/List;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;->name:Ljava/lang/String;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-eqz v1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "failed to execute social post for upload for network id: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "failed to execute social post for upload for network id: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    if-eqz v1, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "failed to execute social post to unknown network with id: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "failed to execute social post to unknown network with id: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/ab;->d:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "UploadWithSocialSharing"

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v10, v8, v2}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    return-void
.end method

.method private static a(Lorg/apache/http/HttpResponse;)V
    .locals 1

    invoke-interface {p0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->consumeContent()V

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/google/android/youtube/core/transfer/ab;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/ab;->m:Ljava/lang/Object;

    return-object v0
.end method

.method private b()Lorg/apache/http/HttpResponse;
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Lorg/apache/http/client/methods/HttpPut;

    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/ab;->h:Lcom/google/android/youtube/core/transfer/Transfer;

    iget-object v1, v1, Lcom/google/android/youtube/core/transfer/Transfer;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpPut;-><init>(Ljava/lang/String;)V

    const-string v1, "Content-Range"

    const-string v2, "bytes */*"

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpPut;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/ab;->m:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput-object v0, p0, Lcom/google/android/youtube/core/transfer/ab;->o:Lorg/apache/http/client/methods/HttpUriRequest;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/ab;->e:Lorg/apache/http/client/HttpClient;

    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/ab;->o:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v0, v1}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    :try_end_1
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    :goto_0
    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_0
    move-exception v0

    invoke-direct {p0}, Lcom/google/android/youtube/core/transfer/ab;->c()V

    new-instance v1, Lcom/google/android/youtube/core/transfer/TransferException;

    invoke-direct {v1, v0, v3}, Lcom/google/android/youtube/core/transfer/TransferException;-><init>(Ljava/lang/Throwable;Z)V

    throw v1

    :catch_1
    move-exception v0

    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/ab;->m:Ljava/lang/Object;

    monitor-enter v1

    :try_start_2
    iget-object v2, p0, Lcom/google/android/youtube/core/transfer/ab;->o:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v2}, Lorg/apache/http/client/methods/HttpUriRequest;->isAborted()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :cond_0
    :try_start_3
    iget-object v2, p0, Lcom/google/android/youtube/core/transfer/ab;->o:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v2}, Lorg/apache/http/client/methods/HttpUriRequest;->abort()V

    new-instance v2, Lcom/google/android/youtube/core/transfer/TransferException;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3}, Lcom/google/android/youtube/core/transfer/TransferException;-><init>(Ljava/lang/Throwable;Z)V

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1
.end method

.method private b(Lorg/apache/http/HttpResponse;)V
    .locals 15

    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/ab;->k:Lcom/google/android/youtube/core/transfer/n;

    iget-object v2, p0, Lcom/google/android/youtube/core/transfer/ab;->h:Lcom/google/android/youtube/core/transfer/Transfer;

    iget-object v2, v2, Lcom/google/android/youtube/core/transfer/Transfer;->a:Ljava/lang/String;

    iget-wide v3, p0, Lcom/google/android/youtube/core/transfer/ab;->q:J

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/youtube/core/transfer/n;->b(Ljava/lang/String;J)V

    :try_start_0
    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/ab;->j:Lcom/google/android/youtube/core/converter/http/fq;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/core/converter/http/fq;->b(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lcom/google/android/youtube/core/model/Video;

    move-object v11, v0

    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/ab;->h:Lcom/google/android/youtube/core/transfer/Transfer;

    iget-object v1, v1, Lcom/google/android/youtube/core/transfer/Transfer;->g:Lcom/google/android/youtube/core/transfer/d;

    const-string v2, "metadata_updated"

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/core/transfer/d;->a(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/youtube/core/converter/ConverterException; {:try_start_0 .. :try_end_0} :catch_2

    move-result v1

    if-eqz v1, :cond_1

    :try_start_1
    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    new-instance v13, Lcom/google/android/youtube/core/async/bd;

    invoke-direct {v13}, Lcom/google/android/youtube/core/async/bd;-><init>()V

    invoke-direct {p0}, Lcom/google/android/youtube/core/transfer/ab;->d()Lcom/google/android/youtube/core/model/UserAuth;

    move-result-object v12

    if-nez v12, :cond_0

    const-string v1, "Error updating metadata, auth is null"

    invoke-static {v1}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    :goto_0
    invoke-direct {p0, v11}, Lcom/google/android/youtube/core/transfer/ab;->a(Lcom/google/android/youtube/core/model/Video;)V
    :try_end_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/google/android/youtube/core/converter/ConverterException; {:try_start_1 .. :try_end_1} :catch_2

    :goto_1
    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/ab;->f:Lcom/google/android/youtube/core/client/bc;

    invoke-interface {v1}, Lcom/google/android/youtube/core/client/bc;->i()V

    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/ab;->h:Lcom/google/android/youtube/core/transfer/Transfer;

    iget-object v1, v1, Lcom/google/android/youtube/core/transfer/Transfer;->g:Lcom/google/android/youtube/core/transfer/d;

    const-string v2, "upload_start_time_millis"

    const-wide/16 v3, -0x1

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/youtube/core/transfer/d;->b(Ljava/lang/String;J)J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-lez v3, :cond_2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    sub-long v1, v3, v1

    :goto_2
    iget-object v3, p0, Lcom/google/android/youtube/core/transfer/ab;->d:Lcom/google/android/youtube/core/Analytics;

    const-string v4, "UploadCompleted"

    const/4 v5, 0x0

    long-to-int v1, v1

    invoke-interface {v3, v4, v5, v1}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/ab;->k:Lcom/google/android/youtube/core/transfer/n;

    iget-object v2, p0, Lcom/google/android/youtube/core/transfer/ab;->h:Lcom/google/android/youtube/core/transfer/Transfer;

    iget-object v2, v2, Lcom/google/android/youtube/core/transfer/Transfer;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/core/transfer/ab;->h:Lcom/google/android/youtube/core/transfer/Transfer;

    iget-object v3, v3, Lcom/google/android/youtube/core/transfer/Transfer;->h:Lcom/google/android/youtube/core/transfer/d;

    invoke-interface {v1, v2, v3}, Lcom/google/android/youtube/core/transfer/n;->a(Ljava/lang/String;Lcom/google/android/youtube/core/transfer/d;)V

    return-void

    :cond_0
    :try_start_2
    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/ab;->f:Lcom/google/android/youtube/core/client/bc;

    iget-object v2, p0, Lcom/google/android/youtube/core/transfer/ab;->h:Lcom/google/android/youtube/core/transfer/Transfer;

    iget-object v2, v2, Lcom/google/android/youtube/core/transfer/Transfer;->g:Lcom/google/android/youtube/core/transfer/d;

    const-string v3, "upload_title"

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/transfer/d;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/core/transfer/ab;->h:Lcom/google/android/youtube/core/transfer/Transfer;

    iget-object v3, v3, Lcom/google/android/youtube/core/transfer/Transfer;->g:Lcom/google/android/youtube/core/transfer/d;

    const-string v4, "upload_description"

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/transfer/d;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, v11, Lcom/google/android/youtube/core/model/Video;->categoryTerm:Ljava/lang/String;

    iget-object v5, v11, Lcom/google/android/youtube/core/model/Video;->categoryLabel:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/youtube/core/transfer/ab;->h:Lcom/google/android/youtube/core/transfer/Transfer;

    iget-object v6, v6, Lcom/google/android/youtube/core/transfer/Transfer;->g:Lcom/google/android/youtube/core/transfer/d;

    const-string v7, "upload_keywords"

    invoke-virtual {v6, v7}, Lcom/google/android/youtube/core/transfer/d;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/youtube/core/transfer/ab;->h:Lcom/google/android/youtube/core/transfer/Transfer;

    iget-object v7, v7, Lcom/google/android/youtube/core/transfer/Transfer;->g:Lcom/google/android/youtube/core/transfer/d;

    const-string v8, "upload_privacy"

    invoke-virtual {v7, v8}, Lcom/google/android/youtube/core/transfer/d;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/youtube/core/model/Video$Privacy;->valueOf(Ljava/lang/String;)Lcom/google/android/youtube/core/model/Video$Privacy;

    move-result-object v7

    iget-object v8, v11, Lcom/google/android/youtube/core/model/Video;->accessControl:Ljava/util/Map;

    iget-object v9, v11, Lcom/google/android/youtube/core/model/Video;->location:Ljava/lang/String;

    iget-object v10, p0, Lcom/google/android/youtube/core/transfer/ab;->h:Lcom/google/android/youtube/core/transfer/Transfer;

    iget-object v10, v10, Lcom/google/android/youtube/core/transfer/Transfer;->g:Lcom/google/android/youtube/core/transfer/d;

    const-string v14, "upload_location"

    invoke-virtual {v10, v14}, Lcom/google/android/youtube/core/transfer/d;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iget-object v11, v11, Lcom/google/android/youtube/core/model/Video;->editUri:Landroid/net/Uri;

    invoke-interface/range {v1 .. v13}, Lcom/google/android/youtube/core/client/bc;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/core/model/Video$Privacy;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V

    invoke-virtual {v13}, Lcom/google/android/youtube/core/async/bd;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/model/Video;

    iget-object v2, p0, Lcom/google/android/youtube/core/transfer/ab;->h:Lcom/google/android/youtube/core/transfer/Transfer;

    iget-object v2, v2, Lcom/google/android/youtube/core/transfer/Transfer;->h:Lcom/google/android/youtube/core/transfer/d;

    const-string v3, "video_id"

    iget-object v4, v1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/youtube/core/transfer/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, v1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/google/android/youtube/core/transfer/ab;->a(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/google/android/youtube/core/converter/ConverterException; {:try_start_2 .. :try_end_2} :catch_2

    move-object v11, v1

    goto/16 :goto_0

    :catch_0
    move-exception v1

    :try_start_3
    new-instance v1, Lcom/google/android/youtube/core/transfer/TransferException;

    const-string v2, "Error updating video metadata after upload"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/google/android/youtube/core/transfer/TransferException;-><init>(Ljava/lang/String;Z)V

    throw v1
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/google/android/youtube/core/converter/ConverterException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_1
    move-exception v1

    const-string v2, "error parsing uploaded video"

    invoke-static {v2, v1}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    :cond_1
    :try_start_4
    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/ab;->h:Lcom/google/android/youtube/core/transfer/Transfer;

    iget-object v1, v1, Lcom/google/android/youtube/core/transfer/Transfer;->h:Lcom/google/android/youtube/core/transfer/d;

    const-string v2, "video_id"

    iget-object v3, v11, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/youtube/core/transfer/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v11}, Lcom/google/android/youtube/core/transfer/ab;->a(Lcom/google/android/youtube/core/model/Video;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Lcom/google/android/youtube/core/converter/ConverterException; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_1

    :catch_2
    move-exception v1

    const-string v2, "error parsing uploaded video"

    invoke-static {v2, v1}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    :cond_2
    const-wide/16 v1, -0x1

    goto/16 :goto_2
.end method

.method static synthetic c(Lcom/google/android/youtube/core/transfer/ab;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/youtube/core/transfer/ab;->q:J

    return-wide v0
.end method

.method private c()V
    .locals 6

    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/ab;->m:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-wide v2, p0, Lcom/google/android/youtube/core/transfer/ab;->q:J

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/ab;->h:Lcom/google/android/youtube/core/transfer/Transfer;

    iget-object v0, v0, Lcom/google/android/youtube/core/transfer/Transfer;->g:Lcom/google/android/youtube/core/transfer/d;

    const-string v4, "metadata_updated"

    const/4 v5, 0x1

    invoke-virtual {v0, v4, v5}, Lcom/google/android/youtube/core/transfer/d;->b(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    int-to-long v4, v0

    sub-long/2addr v2, v4

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/ab;->o:Lorg/apache/http/client/methods/HttpUriRequest;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/ab;->o:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v0}, Lorg/apache/http/client/methods/HttpUriRequest;->isAborted()Z

    move-result v0

    if-nez v0, :cond_0

    iget-wide v4, p0, Lcom/google/android/youtube/core/transfer/ab;->r:J

    cmp-long v0, v4, v2

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/ab;->o:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v0}, Lorg/apache/http/client/methods/HttpUriRequest;->abort()V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :cond_1
    const/4 v0, 0x2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private d()Lcom/google/android/youtube/core/model/UserAuth;
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/ab;->i:Ljava/lang/String;

    if-nez v1, :cond_0

    const-string v1, "null user auth due to null accountName"

    invoke-static {v1}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lcom/google/android/youtube/core/async/bc;

    invoke-direct {v1}, Lcom/google/android/youtube/core/async/bc;-><init>()V

    iget-object v2, p0, Lcom/google/android/youtube/core/transfer/ab;->g:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v2, v1}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bk;)V

    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/youtube/core/async/bc;->a()Lcom/google/android/youtube/core/model/UserAuth;

    move-result-object v1

    if-nez v1, :cond_1

    const-string v1, "authentication produced a null user auth"

    invoke-static {v1}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "exception during authentication"

    invoke-static {v2, v1}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/google/android/youtube/core/transfer/ab;->i:Ljava/lang/String;

    iget-object v3, v1, Lcom/google/android/youtube/core/model/UserAuth;->account:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v1, "authentication produced user auth for a different account"

    invoke-static {v1}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method static synthetic d(Lcom/google/android/youtube/core/transfer/ab;)Lcom/google/android/youtube/core/transfer/Transfer;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/ab;->h:Lcom/google/android/youtube/core/transfer/Transfer;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/core/transfer/ab;)Lcom/google/android/youtube/core/transfer/n;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/ab;->k:Lcom/google/android/youtube/core/transfer/n;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/transfer/ab;->p:Z

    iget-boolean v0, p0, Lcom/google/android/youtube/core/transfer/ab;->n:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/ab;->l:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/youtube/core/transfer/ac;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/core/transfer/ac;-><init>(Lcom/google/android/youtube/core/transfer/ab;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/ab;->s:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    return-void
.end method

.method public final run()V
    .locals 9

    const/16 v8, 0x134

    const/16 v7, 0xc9

    const/16 v6, 0xc8

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Upload starting ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/ab;->h:Lcom/google/android/youtube/core/transfer/Transfer;

    iget-object v1, v1, Lcom/google/android/youtube/core/transfer/Transfer;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/transfer/ab;->p:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Upload cancelled before the task started ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/ab;->h:Lcom/google/android/youtube/core/transfer/Transfer;

    iget-object v1, v1, Lcom/google/android/youtube/core/transfer/Transfer;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V
    :try_end_0
    .catch Lorg/apache/http/conn/HttpHostConnectException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/google/android/youtube/core/transfer/TransferException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/ab;->s:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    :goto_1
    return-void

    :cond_1
    :try_start_1
    new-instance v2, Ljava/io/File;

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/ab;->h:Lcom/google/android/youtube/core/transfer/Transfer;

    iget-object v0, v0, Lcom/google/android/youtube/core/transfer/Transfer;->a:Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/youtube/core/transfer/TransferException;

    const-string v1, "file not found"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/transfer/TransferException;-><init>(Ljava/lang/String;Z)V

    throw v0
    :try_end_1
    .catch Lorg/apache/http/conn/HttpHostConnectException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/google/android/youtube/core/transfer/TransferException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catch_0
    move-exception v0

    :try_start_2
    const-string v1, "failure uploading"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/ab;->k:Lcom/google/android/youtube/core/transfer/n;

    iget-object v2, p0, Lcom/google/android/youtube/core/transfer/ab;->h:Lcom/google/android/youtube/core/transfer/Transfer;

    iget-object v2, v2, Lcom/google/android/youtube/core/transfer/Transfer;->a:Ljava/lang/String;

    new-instance v3, Lcom/google/android/youtube/core/transfer/TransferException;

    const/4 v4, 0x0

    invoke-direct {v3, v0, v4}, Lcom/google/android/youtube/core/transfer/TransferException;-><init>(Ljava/lang/Throwable;Z)V

    invoke-interface {v1, v2, v3}, Lcom/google/android/youtube/core/transfer/n;->a(Ljava/lang/String;Lcom/google/android/youtube/core/transfer/TransferException;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/ab;->s:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    goto :goto_1

    :cond_2
    :try_start_3
    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Lcom/google/android/youtube/core/transfer/TransferException;

    const-string v1, "file is a directory"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/transfer/TransferException;-><init>(Ljava/lang/String;Z)V

    throw v0
    :try_end_3
    .catch Lorg/apache/http/conn/HttpHostConnectException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Lcom/google/android/youtube/core/transfer/TransferException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catch_1
    move-exception v0

    :try_start_4
    const-string v1, "FATAL failure uploading"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/ab;->d:Lcom/google/android/youtube/core/Analytics;

    const-string v2, "UploadFatalError"

    invoke-virtual {v0}, Lorg/apache/http/client/ClientProtocolException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/ab;->k:Lcom/google/android/youtube/core/transfer/n;

    iget-object v2, p0, Lcom/google/android/youtube/core/transfer/ab;->h:Lcom/google/android/youtube/core/transfer/Transfer;

    iget-object v2, v2, Lcom/google/android/youtube/core/transfer/Transfer;->a:Ljava/lang/String;

    new-instance v3, Lcom/google/android/youtube/core/transfer/TransferException;

    const/4 v4, 0x1

    invoke-direct {v3, v0, v4}, Lcom/google/android/youtube/core/transfer/TransferException;-><init>(Ljava/lang/Throwable;Z)V

    invoke-interface {v1, v2, v3}, Lcom/google/android/youtube/core/transfer/n;->a(Ljava/lang/String;Lcom/google/android/youtube/core/transfer/TransferException;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/ab;->s:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    goto :goto_1

    :cond_3
    :try_start_5
    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/youtube/core/transfer/ab;->q:J

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/ab;->k:Lcom/google/android/youtube/core/transfer/n;

    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/ab;->h:Lcom/google/android/youtube/core/transfer/Transfer;

    iget-object v1, v1, Lcom/google/android/youtube/core/transfer/Transfer;->a:Ljava/lang/String;

    iget-wide v3, p0, Lcom/google/android/youtube/core/transfer/ab;->q:J

    invoke-interface {v0, v1, v3, v4}, Lcom/google/android/youtube/core/transfer/n;->a(Ljava/lang/String;J)V

    iget-wide v0, p0, Lcom/google/android/youtube/core/transfer/ab;->q:J

    const-wide/16 v3, 0x0

    cmp-long v0, v0, v3

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/android/youtube/core/transfer/TransferException;

    const-string v1, "file is empty"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/transfer/TransferException;-><init>(Ljava/lang/String;Z)V

    throw v0
    :try_end_5
    .catch Lorg/apache/http/conn/HttpHostConnectException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Lcom/google/android/youtube/core/transfer/TransferException; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catch_2
    move-exception v0

    :try_start_6
    const-string v1, "failure uploading"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/ab;->d:Lcom/google/android/youtube/core/Analytics;

    const-string v2, "UploadError"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/ab;->k:Lcom/google/android/youtube/core/transfer/n;

    iget-object v2, p0, Lcom/google/android/youtube/core/transfer/ab;->h:Lcom/google/android/youtube/core/transfer/Transfer;

    iget-object v2, v2, Lcom/google/android/youtube/core/transfer/Transfer;->a:Ljava/lang/String;

    new-instance v3, Lcom/google/android/youtube/core/transfer/TransferException;

    const/4 v4, 0x0

    invoke-direct {v3, v0, v4}, Lcom/google/android/youtube/core/transfer/TransferException;-><init>(Ljava/lang/Throwable;Z)V

    invoke-interface {v1, v2, v3}, Lcom/google/android/youtube/core/transfer/n;->a(Ljava/lang/String;Lcom/google/android/youtube/core/transfer/TransferException;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/ab;->s:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    goto/16 :goto_1

    :cond_4
    :try_start_7
    iget-boolean v0, p0, Lcom/google/android/youtube/core/transfer/ab;->p:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/transfer/ab;->n:Z

    invoke-direct {p0}, Lcom/google/android/youtube/core/transfer/ab;->b()Lorg/apache/http/HttpResponse;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/youtube/core/transfer/ab;->p:Z

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v1

    if-ne v1, v8, :cond_a

    invoke-static {v0}, Lcom/google/android/youtube/core/transfer/ab;->a(Lorg/apache/http/HttpResponse;)V

    const-string v1, "range"

    invoke-interface {v0, v1}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    if-nez v0, :cond_6

    const-wide/16 v0, -0x1

    :goto_2
    iget-object v3, p0, Lcom/google/android/youtube/core/transfer/ab;->m:Ljava/lang/Object;

    monitor-enter v3
    :try_end_7
    .catch Lorg/apache/http/conn/HttpHostConnectException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Lcom/google/android/youtube/core/transfer/TransferException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :try_start_8
    iput-wide v0, p0, Lcom/google/android/youtube/core/transfer/ab;->r:J

    monitor-exit v3
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :try_start_9
    iget-object v3, p0, Lcom/google/android/youtube/core/transfer/ab;->h:Lcom/google/android/youtube/core/transfer/Transfer;

    iget-object v3, v3, Lcom/google/android/youtube/core/transfer/Transfer;->g:Lcom/google/android/youtube/core/transfer/d;

    const-string v4, "metadata_updated"

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Lcom/google/android/youtube/core/transfer/d;->b(Ljava/lang/String;Z)Z

    move-result v3

    invoke-direct {p0, v2, v0, v1, v3}, Lcom/google/android/youtube/core/transfer/ab;->a(Ljava/io/File;JZ)Lorg/apache/http/HttpResponse;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/youtube/core/transfer/ab;->p:Z

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v1

    if-eq v1, v6, :cond_5

    if-ne v1, v7, :cond_8

    :cond_5
    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/transfer/ab;->b(Lorg/apache/http/HttpResponse;)V
    :try_end_9
    .catch Lorg/apache/http/conn/HttpHostConnectException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_9 .. :try_end_9} :catch_1
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2
    .catch Lcom/google/android/youtube/core/transfer/TransferException; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_0

    :catch_3
    move-exception v0

    :try_start_a
    const-string v1, "failure uploading"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/ab;->d:Lcom/google/android/youtube/core/Analytics;

    const-string v2, "UploadError"

    invoke-virtual {v0}, Lcom/google/android/youtube/core/transfer/TransferException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/ab;->k:Lcom/google/android/youtube/core/transfer/n;

    iget-object v2, p0, Lcom/google/android/youtube/core/transfer/ab;->h:Lcom/google/android/youtube/core/transfer/Transfer;

    iget-object v2, v2, Lcom/google/android/youtube/core/transfer/Transfer;->a:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Lcom/google/android/youtube/core/transfer/n;->a(Ljava/lang/String;Lcom/google/android/youtube/core/transfer/TransferException;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/ab;->s:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    goto/16 :goto_1

    :cond_6
    :try_start_b
    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/core/transfer/ab;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-nez v3, :cond_7

    new-instance v1, Ljava/io/IOException;

    const-string v2, "malformed range header=%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_b
    .catch Lorg/apache/http/conn/HttpHostConnectException; {:try_start_b .. :try_end_b} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_b .. :try_end_b} :catch_1
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_2
    .catch Lcom/google/android/youtube/core/transfer/TransferException; {:try_start_b .. :try_end_b} :catch_3
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/ab;->s:Landroid/os/ConditionVariable;

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->open()V

    throw v0

    :cond_7
    const/4 v0, 0x2

    :try_start_c
    invoke-virtual {v1, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_2

    :catchall_1
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_8
    if-ne v1, v8, :cond_9

    invoke-static {v0}, Lcom/google/android/youtube/core/transfer/ab;->a(Lorg/apache/http/HttpResponse;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/ab;->h:Lcom/google/android/youtube/core/transfer/Transfer;

    iget-object v0, v0, Lcom/google/android/youtube/core/transfer/Transfer;->g:Lcom/google/android/youtube/core/transfer/d;

    const-string v1, "metadata_updated"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/transfer/d;->b(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/youtube/core/transfer/ab;->r:J

    iget-wide v2, p0, Lcom/google/android/youtube/core/transfer/ab;->q:J

    const-wide/16 v4, 0x2

    sub-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    new-instance v0, Lcom/google/android/youtube/core/transfer/TransferException;

    const-string v1, "upload request got http status: 308"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/transfer/TransferException;-><init>(Ljava/lang/String;Z)V

    throw v0

    :cond_9
    invoke-static {v0}, Lcom/google/android/youtube/core/transfer/ab;->a(Lorg/apache/http/HttpResponse;)V

    new-instance v0, Lcom/google/android/youtube/core/transfer/TransferException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "upload request got http status: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/transfer/TransferException;-><init>(Ljava/lang/String;Z)V

    throw v0

    :cond_a
    if-eq v1, v6, :cond_b

    if-ne v1, v7, :cond_c

    :cond_b
    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/transfer/ab;->b(Lorg/apache/http/HttpResponse;)V

    goto/16 :goto_0

    :cond_c
    invoke-static {v0}, Lcom/google/android/youtube/core/transfer/ab;->a(Lorg/apache/http/HttpResponse;)V

    new-instance v0, Lcom/google/android/youtube/core/transfer/TransferException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "range request got http status: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/transfer/TransferException;-><init>(Ljava/lang/String;Z)V

    throw v0
    :try_end_c
    .catch Lorg/apache/http/conn/HttpHostConnectException; {:try_start_c .. :try_end_c} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_c .. :try_end_c} :catch_1
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_2
    .catch Lcom/google/android/youtube/core/transfer/TransferException; {:try_start_c .. :try_end_c} :catch_3
    .catchall {:try_start_c .. :try_end_c} :catchall_0
.end method
