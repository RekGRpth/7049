.class final Lcom/google/android/youtube/core/transfer/g;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:J

.field public final b:I

.field private final c:Ljava/lang/Object;

.field private d:I

.field private e:Z


# direct methods
.method public constructor <init>(Ljava/lang/Object;IJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/core/transfer/g;->c:Ljava/lang/Object;

    iput-wide p3, p0, Lcom/google/android/youtube/core/transfer/g;->a:J

    iput p2, p0, Lcom/google/android/youtube/core/transfer/g;->b:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/core/transfer/g;->d:I

    return v0
.end method

.method public final a(I)V
    .locals 2

    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/g;->c:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_0
    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/transfer/g;->d:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/transfer/g;->e:Z

    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/g;->c:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/transfer/g;->e:Z

    return v0
.end method
