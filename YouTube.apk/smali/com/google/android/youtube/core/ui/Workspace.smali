.class public Lcom/google/android/youtube/core/ui/Workspace;
.super Lcom/google/android/youtube/core/ui/AbstractWorkspace;
.source "SourceFile"


# instance fields
.field private b:Lcom/google/android/youtube/core/ui/TabRow;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/ui/AbstractWorkspace;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public static a(Landroid/app/Activity;III)Lcom/google/android/youtube/core/ui/Workspace;
    .locals 2

    const v0, 0x7f070069

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/ui/TabRow;

    const v1, 0x7f07006a

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/ui/Workspace;

    invoke-static {v1, v0, p3}, Lcom/google/android/youtube/core/ui/Workspace;->a(Lcom/google/android/youtube/core/ui/Workspace;Lcom/google/android/youtube/core/ui/TabRow;I)Lcom/google/android/youtube/core/ui/Workspace;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/Workspace;Lcom/google/android/youtube/core/ui/TabRow;)Lcom/google/android/youtube/core/ui/Workspace;
    .locals 1

    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, Lcom/google/android/youtube/core/ui/Workspace;->a(Lcom/google/android/youtube/core/ui/Workspace;Lcom/google/android/youtube/core/ui/TabRow;I)Lcom/google/android/youtube/core/ui/Workspace;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/google/android/youtube/core/ui/Workspace;Lcom/google/android/youtube/core/ui/TabRow;I)Lcom/google/android/youtube/core/ui/Workspace;
    .locals 4

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/google/android/youtube/core/ui/Workspace;->b:Lcom/google/android/youtube/core/ui/TabRow;

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/Workspace;->b:Lcom/google/android/youtube/core/ui/TabRow;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/ui/TabRow;->a()V

    move v0, v1

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/Workspace;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/core/ui/Workspace;->b:Lcom/google/android/youtube/core/ui/TabRow;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/ui/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/ui/TabRow;->a(Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/Workspace;->a()I

    move-result v0

    if-ltz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/Workspace;->getChildCount()I

    move-result v2

    if-le v0, v2, :cond_2

    :cond_1
    move v0, v1

    :cond_2
    iget-object v2, p0, Lcom/google/android/youtube/core/ui/Workspace;->b:Lcom/google/android/youtube/core/ui/TabRow;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/youtube/core/ui/Workspace;->b:Lcom/google/android/youtube/core/ui/TabRow;

    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, Lcom/google/android/youtube/core/ui/TabRow;->a(IZ)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/Workspace;->requestLayout()V

    new-instance v0, Lcom/google/android/youtube/core/ui/u;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/core/ui/u;-><init>(Lcom/google/android/youtube/core/ui/Workspace;)V

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/core/ui/TabRow;->setOnTabClickListener(Lcom/google/android/youtube/core/ui/s;)V

    invoke-virtual {p0, p2}, Lcom/google/android/youtube/core/ui/Workspace;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/ui/Workspace;->indexOfChild(Landroid/view/View;)I

    move-result v0

    invoke-virtual {p1, v0, v1}, Lcom/google/android/youtube/core/ui/TabRow;->a(IZ)V

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/ui/Workspace;->setCurrentScreen(I)V

    :cond_4
    return-object p0
.end method


# virtual methods
.method protected final a(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/Workspace;->b:Lcom/google/android/youtube/core/ui/TabRow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/Workspace;->b:Lcom/google/android/youtube/core/ui/TabRow;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/youtube/core/ui/TabRow;->a(IZ)V

    :cond_0
    return-void
.end method
