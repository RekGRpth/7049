.class final Lcom/google/android/youtube/core/ui/d;
.super Landroid/database/DataSetObserver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/ui/BasePagedView;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/core/ui/BasePagedView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/core/ui/d;->a:Lcom/google/android/youtube/core/ui/BasePagedView;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/core/ui/BasePagedView;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/ui/d;-><init>(Lcom/google/android/youtube/core/ui/BasePagedView;)V

    return-void
.end method

.method private a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/d;->a:Lcom/google/android/youtube/core/ui/BasePagedView;

    iget-object v0, v0, Lcom/google/android/youtube/core/ui/BasePagedView;->i:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/d;->a:Lcom/google/android/youtube/core/ui/BasePagedView;

    iget-object v0, v0, Lcom/google/android/youtube/core/ui/BasePagedView;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/d;->a:Lcom/google/android/youtube/core/ui/BasePagedView;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/ui/BasePagedView;->c()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/d;->a:Lcom/google/android/youtube/core/ui/BasePagedView;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/ui/BasePagedView;->d()V

    goto :goto_0
.end method


# virtual methods
.method public final onChanged()V
    .locals 0

    invoke-super {p0}, Landroid/database/DataSetObserver;->onChanged()V

    invoke-direct {p0}, Lcom/google/android/youtube/core/ui/d;->a()V

    return-void
.end method

.method public final onInvalidated()V
    .locals 0

    invoke-super {p0}, Landroid/database/DataSetObserver;->onInvalidated()V

    invoke-direct {p0}, Lcom/google/android/youtube/core/ui/d;->a()V

    return-void
.end method
