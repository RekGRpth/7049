.class public Lcom/google/android/youtube/core/ui/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/youtube/core/async/n;
.implements Lcom/google/android/youtube/core/ui/h;
.implements Lcom/google/android/youtube/core/utils/t;


# instance fields
.field private final a:Lcom/google/android/youtube/core/async/au;

.field protected final b:Landroid/app/Activity;

.field final c:Landroid/os/Handler;

.field protected final d:Lcom/google/android/youtube/core/ui/PagedView;

.field protected final e:Lcom/google/android/youtube/core/a/j;

.field protected f:I

.field private final g:Lcom/google/android/youtube/core/e;

.field private final h:Lcom/google/android/youtube/core/async/h;

.field private final i:Lcom/google/android/youtube/core/a/a;

.field private final j:Lcom/google/android/youtube/core/ui/i;

.field private k:Ljava/util/LinkedList;

.field private l:Lcom/google/android/youtube/core/async/GDataRequest;

.field private m:I

.field private n:Z

.field private o:Landroid/net/Uri;

.field private p:I

.field private q:I


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/PagedView;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/e;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "activity cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/j;->b:Landroid/app/Activity;

    const-string v0, "view cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/ui/PagedView;

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/j;->d:Lcom/google/android/youtube/core/ui/PagedView;

    const-string v0, "requester cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/au;

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/j;->a:Lcom/google/android/youtube/core/async/au;

    const-string v0, "errorHelper cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/e;

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/j;->g:Lcom/google/android/youtube/core/e;

    const-string v0, "targetAdapter cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/a/a;

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/j;->i:Lcom/google/android/youtube/core/a/a;

    invoke-interface {p2}, Lcom/google/android/youtube/core/ui/PagedView;->h()Lcom/google/android/youtube/core/ui/i;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/j;->j:Lcom/google/android/youtube/core/ui/i;

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->j:Lcom/google/android/youtube/core/ui/i;

    invoke-interface {v0}, Lcom/google/android/youtube/core/ui/i;->a()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p3, v0, v1}, Lcom/google/android/youtube/core/a/j;->a(Lcom/google/android/youtube/core/a/a;Landroid/view/View;Z)Lcom/google/android/youtube/core/a/j;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/j;->e:Lcom/google/android/youtube/core/a/j;

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->d:Lcom/google/android/youtube/core/ui/PagedView;

    iget-object v1, p0, Lcom/google/android/youtube/core/ui/j;->e:Lcom/google/android/youtube/core/a/j;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/ui/PagedView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->d:Lcom/google/android/youtube/core/ui/PagedView;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/core/ui/PagedView;->setOnScrollListener(Lcom/google/android/youtube/core/ui/h;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->d:Lcom/google/android/youtube/core/ui/PagedView;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/core/ui/PagedView;->setOnRetryClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->j:Lcom/google/android/youtube/core/ui/i;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/core/ui/i;->a(Landroid/view/View$OnClickListener;)V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/j;->k:Ljava/util/LinkedList;

    invoke-static {p1, p0}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/j;->h:Lcom/google/android/youtube/core/async/h;

    new-instance v0, Lcom/google/android/youtube/core/ui/k;

    invoke-virtual {p1}, Landroid/app/Activity;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/core/ui/k;-><init>(Lcom/google/android/youtube/core/ui/j;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/j;->c:Landroid/os/Handler;

    return-void
.end method

.method private a(Lcom/google/android/youtube/core/async/GDataRequest;)V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/ui/j;->n:Z

    iput-object p1, p0, Lcom/google/android/youtube/core/ui/j;->l:Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v1, p0, Lcom/google/android/youtube/core/ui/j;->e:Lcom/google/android/youtube/core/a/j;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/a/j;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v1}, Landroid/widget/ListAdapter;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->j:Lcom/google/android/youtube/core/ui/i;

    invoke-interface {v0}, Lcom/google/android/youtube/core/ui/i;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->e:Lcom/google/android/youtube/core/a/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/j;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->d:Lcom/google/android/youtube/core/ui/PagedView;

    invoke-interface {v0}, Lcom/google/android/youtube/core/ui/PagedView;->d()V

    :goto_1
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->a:Lcom/google/android/youtube/core/async/au;

    iget-object v1, p0, Lcom/google/android/youtube/core/ui/j;->h:Lcom/google/android/youtube/core/async/h;

    invoke-interface {v0, p1, v1}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->d:Lcom/google/android/youtube/core/ui/PagedView;

    invoke-interface {v0}, Lcom/google/android/youtube/core/ui/PagedView;->e()V

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/youtube/core/ui/j;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/core/ui/j;->h()V

    return-void
.end method

.method private a(Ljava/lang/String;ZZ)V
    .locals 1

    if-nez p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->j:Lcom/google/android/youtube/core/ui/i;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/ui/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->e:Lcom/google/android/youtube/core/a/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/j;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->d:Lcom/google/android/youtube/core/ui/PagedView;

    invoke-interface {v0}, Lcom/google/android/youtube/core/ui/PagedView;->d()V

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->i:Lcom/google/android/youtube/core/a/a;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/a;->a()V

    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->d:Lcom/google/android/youtube/core/ui/PagedView;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/ui/PagedView;->b(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->d:Lcom/google/android/youtube/core/ui/PagedView;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/ui/PagedView;->a(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private h()V
    .locals 3

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/youtube/core/ui/j;->n:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->o:Landroid/net/Uri;

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/youtube/core/ui/j;->i()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->l:Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v0, v0, Lcom/google/android/youtube/core/async/GDataRequest;->c:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/youtube/core/ui/j;->o:Landroid/net/Uri;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->e:Lcom/google/android/youtube/core/a/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/j;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/core/ui/j;->d:Lcom/google/android/youtube/core/ui/PagedView;

    invoke-interface {v1}, Lcom/google/android/youtube/core/ui/PagedView;->a()I

    move-result v1

    if-ge v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->o:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/youtube/core/ui/j;->l:Lcom/google/android/youtube/core/async/GDataRequest;

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;Lcom/google/android/youtube/core/async/GDataRequest;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v0

    iput-object v2, p0, Lcom/google/android/youtube/core/ui/j;->o:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/ui/j;->a(Lcom/google/android/youtube/core/async/GDataRequest;)V

    goto :goto_0

    :cond_3
    iput-object v2, p0, Lcom/google/android/youtube/core/ui/j;->o:Landroid/net/Uri;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/j;->b()V

    goto :goto_0
.end method

.method private i()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->k:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/j;->b()V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/j;->o:Landroid/net/Uri;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/youtube/core/ui/j;->f:I

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->k:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/GDataRequest;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/ui/j;->a(Lcom/google/android/youtube/core/async/GDataRequest;)V

    goto :goto_0
.end method


# virtual methods
.method protected a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->d:Lcom/google/android/youtube/core/ui/PagedView;

    invoke-interface {v0}, Lcom/google/android/youtube/core/ui/PagedView;->c()V

    return-void
.end method

.method public a(Lcom/google/android/youtube/core/async/GDataRequest;Lcom/google/android/youtube/core/model/Page;)V
    .locals 11

    const/4 v9, 0x0

    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/google/android/youtube/core/ui/j;->n:Z

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->l:Lcom/google/android/youtube/core/async/GDataRequest;

    if-eq p1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->e:Lcom/google/android/youtube/core/a/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/j;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->d:Lcom/google/android/youtube/core/ui/PagedView;

    invoke-interface {v0}, Lcom/google/android/youtube/core/ui/PagedView;->a()I

    move-result v2

    iget v0, p2, Lcom/google/android/youtube/core/model/Page;->totalResults:I

    iput v0, p0, Lcom/google/android/youtube/core/ui/j;->m:I

    iget-object v3, p2, Lcom/google/android/youtube/core/model/Page;->entries:Ljava/util/List;

    new-instance v4, Ljava/util/ArrayList;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    iget v0, p0, Lcom/google/android/youtube/core/ui/j;->f:I

    iget v5, p2, Lcom/google/android/youtube/core/model/Page;->startIndex:I

    sub-int/2addr v0, v5

    add-int/lit8 v0, v0, 0x1

    invoke-static {v8, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    move v10, v0

    move v0, v1

    move v1, v10

    :goto_1
    if-ge v1, v5, :cond_3

    if-ge v0, v2, :cond_3

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    iget v7, p0, Lcom/google/android/youtube/core/ui/j;->p:I

    if-lez v7, :cond_2

    iget v6, p0, Lcom/google/android/youtube/core/ui/j;->p:I

    add-int/lit8 v6, v6, -0x1

    iput v6, p0, Lcom/google/android/youtube/core/ui/j;->p:I

    :cond_1
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {p0, v6}, Lcom/google/android/youtube/core/ui/j;->a(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    iget v6, p0, Lcom/google/android/youtube/core/ui/j;->q:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/android/youtube/core/ui/j;->q:I

    goto :goto_2

    :cond_3
    iget v0, p0, Lcom/google/android/youtube/core/ui/j;->f:I

    iget v1, p2, Lcom/google/android/youtube/core/model/Page;->startIndex:I

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/ui/j;->f:I

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Received "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " entries; after filtering "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; realLastIndex = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/youtube/core/ui/j;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->d()V

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->e:Lcom/google/android/youtube/core/a/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/j;->getCount()I

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->d:Lcom/google/android/youtube/core/ui/PagedView;

    invoke-interface {v0}, Lcom/google/android/youtube/core/ui/PagedView;->j()V

    :cond_4
    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->e:Lcom/google/android/youtube/core/a/j;

    invoke-virtual {v0, v4}, Lcom/google/android/youtube/core/a/j;->a(Ljava/lang/Iterable;)V

    iget-object v0, p2, Lcom/google/android/youtube/core/model/Page;->nextUri:Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/j;->o:Landroid/net/Uri;

    invoke-virtual {p0, p1, v4}, Lcom/google/android/youtube/core/ui/j;->a(Lcom/google/android/youtube/core/async/GDataRequest;Ljava/util/List;)V

    :goto_3
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->e:Lcom/google/android/youtube/core/a/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/j;->getCount()I

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->k:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/j;->a()V

    :goto_4
    iget v0, p0, Lcom/google/android/youtube/core/ui/j;->q:I

    iget-object v1, p0, Lcom/google/android/youtube/core/ui/j;->d:Lcom/google/android/youtube/core/ui/PagedView;

    invoke-interface {v1}, Lcom/google/android/youtube/core/ui/PagedView;->b()I

    move-result v1

    if-ge v0, v1, :cond_7

    invoke-direct {p0}, Lcom/google/android/youtube/core/ui/j;->h()V

    goto/16 :goto_0

    :cond_5
    iput-object v9, p0, Lcom/google/android/youtube/core/ui/j;->o:Landroid/net/Uri;

    goto :goto_3

    :cond_6
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->e:Lcom/google/android/youtube/core/a/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/j;->c()V

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->j:Lcom/google/android/youtube/core/ui/i;

    invoke-interface {v0, v9}, Lcom/google/android/youtube/core/ui/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->j:Lcom/google/android/youtube/core/ui/i;

    invoke-interface {v0}, Lcom/google/android/youtube/core/ui/i;->c()V

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->d:Lcom/google/android/youtube/core/ui/PagedView;

    invoke-interface {v0}, Lcom/google/android/youtube/core/ui/PagedView;->d()V

    goto :goto_4

    :cond_7
    iput v8, p0, Lcom/google/android/youtube/core/ui/j;->q:I

    goto/16 :goto_0
.end method

.method public a(Lcom/google/android/youtube/core/async/GDataRequest;Ljava/lang/Exception;)V
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Error for request "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    iput-boolean v3, p0, Lcom/google/android/youtube/core/ui/j;->n:Z

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->e:Lcom/google/android/youtube/core/a/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/j;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->isEmpty()Z

    move-result v1

    instance-of v0, p2, Lorg/apache/http/client/HttpResponseException;

    if-eqz v0, :cond_0

    move-object v0, p2

    check-cast v0, Lorg/apache/http/client/HttpResponseException;

    invoke-virtual {v0}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v0

    const/16 v2, 0x193

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->b:Landroid/app/Activity;

    const v2, 0x7f0b0012

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v3, v1}, Lcom/google/android/youtube/core/ui/j;->a(Ljava/lang/String;ZZ)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->g:Lcom/google/android/youtube/core/e;

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/core/e;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v2, v1}, Lcom/google/android/youtube/core/ui/j;->a(Ljava/lang/String;ZZ)V

    goto :goto_0
.end method

.method protected a(Lcom/google/android/youtube/core/async/GDataRequest;Ljava/util/List;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/android/youtube/core/ui/PagedView;III)V
    .locals 2

    add-int v0, p2, p3

    if-ne v0, p4, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->c:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/ui/g;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->d:Lcom/google/android/youtube/core/ui/PagedView;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/ui/PagedView;->setOnPagedViewStateChangeListener(Lcom/google/android/youtube/core/ui/g;)V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0

    check-cast p1, Lcom/google/android/youtube/core/async/GDataRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/core/ui/j;->a(Lcom/google/android/youtube/core/async/GDataRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/google/android/youtube/core/async/GDataRequest;

    check-cast p2, Lcom/google/android/youtube/core/model/Page;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/core/ui/j;->a(Lcom/google/android/youtube/core/async/GDataRequest;Lcom/google/android/youtube/core/model/Page;)V

    return-void
.end method

.method public final varargs a([Lcom/google/android/youtube/core/async/GDataRequest;)V
    .locals 5

    const/4 v1, 0x0

    const-string v0, "requests cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    array-length v0, p1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "requests cannot be empty"

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/utils/s;->a(ZLjava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/j;->c()V

    :goto_1
    array-length v0, p1

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->k:Ljava/util/LinkedList;

    aget-object v2, p1, v1

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "request "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " cannot be null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/youtube/core/ui/j;->i()V

    return-void
.end method

.method public a(Ljava/lang/Object;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected b()V
    .locals 0

    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->e:Lcom/google/android/youtube/core/a/j;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Lcom/google/android/youtube/core/a/j;->a(ILjava/lang/Object;)V

    iget v0, p0, Lcom/google/android/youtube/core/ui/j;->p:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/youtube/core/ui/j;->p:I

    return-void
.end method

.method public c()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    iput-object v1, p0, Lcom/google/android/youtube/core/ui/j;->o:Landroid/net/Uri;

    iput-object v1, p0, Lcom/google/android/youtube/core/ui/j;->l:Lcom/google/android/youtube/core/async/GDataRequest;

    iput v0, p0, Lcom/google/android/youtube/core/ui/j;->f:I

    iput v0, p0, Lcom/google/android/youtube/core/ui/j;->m:I

    iput v0, p0, Lcom/google/android/youtube/core/ui/j;->p:I

    iput v0, p0, Lcom/google/android/youtube/core/ui/j;->q:I

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->k:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->e:Lcom/google/android/youtube/core/a/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/j;->a()V

    return-void
.end method

.method public final d()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/j;->c()V

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->d:Lcom/google/android/youtube/core/ui/PagedView;

    invoke-interface {v0}, Lcom/google/android/youtube/core/ui/PagedView;->e()V

    return-void
.end method

.method public final e()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/j;->c()V

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->d:Lcom/google/android/youtube/core/ui/PagedView;

    invoke-interface {v0}, Lcom/google/android/youtube/core/ui/PagedView;->c()V

    return-void
.end method

.method protected final f()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/core/ui/j;->m:I

    return v0
.end method

.method public final g()Lcom/google/android/youtube/core/ui/PagedView$State;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->d:Lcom/google/android/youtube/core/ui/PagedView;

    invoke-interface {v0}, Lcom/google/android/youtube/core/ui/PagedView;->f()Lcom/google/android/youtube/core/ui/PagedView$State;

    move-result-object v0

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->l:Lcom/google/android/youtube/core/async/GDataRequest;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/j;->l:Lcom/google/android/youtube/core/async/GDataRequest;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/ui/j;->a(Lcom/google/android/youtube/core/async/GDataRequest;)V

    :cond_0
    return-void
.end method
