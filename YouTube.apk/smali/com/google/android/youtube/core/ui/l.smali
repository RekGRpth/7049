.class public final Lcom/google/android/youtube/core/ui/l;
.super Landroid/app/AlertDialog;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/content/DialogInterface$OnDismissListener;


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/google/android/youtube/core/Analytics;

.field private final c:Lcom/google/android/youtube/core/client/bc;

.field private final d:Lcom/google/android/youtube/core/client/be;

.field private final e:Lcom/google/android/youtube/core/client/bh;

.field private final f:Lcom/google/android/youtube/core/model/UserAuth;

.field private final g:Lcom/google/android/youtube/core/async/cc;

.field private final h:Landroid/view/View;

.field private final i:Landroid/widget/TextView;

.field private final j:Landroid/widget/ImageView;

.field private k:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/bh;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/cc;)V
    .locals 6

    const/4 v2, 0x0

    new-instance v0, Landroid/view/ContextThemeWrapper;

    const v1, 0x7f0d000c

    invoke-direct {v0, p1, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {p0, v0}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;)V

    const-string v0, "activity cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/l;->a:Landroid/app/Activity;

    const-string v0, "gdataClient cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/client/bc;

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/l;->c:Lcom/google/android/youtube/core/client/bc;

    const-string v0, "imageClient cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/client/be;

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/l;->d:Lcom/google/android/youtube/core/client/be;

    const-string v0, "plusClient cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/client/bh;

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/l;->e:Lcom/google/android/youtube/core/client/bh;

    const-string v0, "analytics cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/Analytics;

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/l;->b:Lcom/google/android/youtube/core/Analytics;

    const-string v0, "userAuth cannot be null"

    invoke-static {p6, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/UserAuth;

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/l;->f:Lcom/google/android/youtube/core/model/UserAuth;

    const-string v0, "callback cannot be null"

    invoke-static {p7, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/cc;

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/l;->g:Lcom/google/android/youtube/core/async/cc;

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f04002f

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0b0043

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/ui/l;->setTitle(I)V

    move-object v0, p0

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/youtube/core/ui/l;->setView(Landroid/view/View;IIII)V

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/core/ui/l;->setIcon(I)V

    const v0, 0x7f070089

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/l;->h:Landroid/view/View;

    const v0, 0x7f070054

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/l;->i:Landroid/widget/TextView;

    const v0, 0x7f070035

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/l;->j:Landroid/widget/ImageView;

    const/4 v0, -0x1

    const v1, 0x104000a

    invoke-virtual {p1, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, p0}, Lcom/google/android/youtube/core/ui/l;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    const/4 v0, -0x2

    const/high16 v1, 0x1040000

    invoke-virtual {p1, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, p0}, Lcom/google/android/youtube/core/ui/l;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    invoke-virtual {p0, p0}, Lcom/google/android/youtube/core/ui/l;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    invoke-virtual {p0, p0}, Lcom/google/android/youtube/core/ui/l;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/core/ui/l;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/l;->h:Landroid/view/View;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/ui/l;Z)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/ui/l;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 1

    iput-boolean p1, p0, Lcom/google/android/youtube/core/ui/l;->k:Z

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/ui/l;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/google/android/youtube/core/ui/l;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/l;->i:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/core/ui/l;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/l;->a:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/core/ui/l;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/l;->j:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/core/ui/l;)Lcom/google/android/youtube/core/client/be;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/l;->d:Lcom/google/android/youtube/core/client/be;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/youtube/core/ui/l;)Lcom/google/android/youtube/core/Analytics;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/l;->b:Lcom/google/android/youtube/core/Analytics;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/youtube/core/ui/l;)Lcom/google/android/youtube/core/model/UserAuth;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/l;->f:Lcom/google/android/youtube/core/model/UserAuth;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/youtube/core/ui/l;)Lcom/google/android/youtube/core/async/cc;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/l;->g:Lcom/google/android/youtube/core/async/cc;

    return-object v0
.end method


# virtual methods
.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/l;->g:Lcom/google/android/youtube/core/async/cc;

    invoke-interface {v0}, Lcom/google/android/youtube/core/async/cc;->a()V

    return-void
.end method

.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    packed-switch p2, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/ui/l;->a(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/l;->c:Lcom/google/android/youtube/core/client/bc;

    iget-object v1, p0, Lcom/google/android/youtube/core/ui/l;->f:Lcom/google/android/youtube/core/model/UserAuth;

    iget-object v2, p0, Lcom/google/android/youtube/core/ui/l;->a:Landroid/app/Activity;

    new-instance v3, Lcom/google/android/youtube/core/ui/o;

    invoke-direct {v3, p0}, Lcom/google/android/youtube/core/ui/o;-><init>(Lcom/google/android/youtube/core/ui/l;)V

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/bc;->f(Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/l;->g:Lcom/google/android/youtube/core/async/cc;

    invoke-interface {v0}, Lcom/google/android/youtube/core/async/cc;->a()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/l;->a:Landroid/app/Activity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->removeDialog(I)V

    return-void
.end method

.method protected final onStart()V
    .locals 4

    const/4 v1, 0x0

    invoke-super {p0}, Landroid/app/AlertDialog;->onStart()V

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/l;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0, v1}, Lcom/google/android/youtube/core/ui/l;->a(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/l;->e:Lcom/google/android/youtube/core/client/bh;

    iget-object v1, p0, Lcom/google/android/youtube/core/ui/l;->f:Lcom/google/android/youtube/core/model/UserAuth;

    iget-object v2, p0, Lcom/google/android/youtube/core/ui/l;->a:Landroid/app/Activity;

    new-instance v3, Lcom/google/android/youtube/core/ui/m;

    invoke-direct {v3, p0}, Lcom/google/android/youtube/core/ui/m;-><init>(Lcom/google/android/youtube/core/ui/l;)V

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/bh;->a(Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/ui/l;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/youtube/core/ui/l;->k:Z

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method
