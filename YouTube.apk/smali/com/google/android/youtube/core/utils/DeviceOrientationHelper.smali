.class public final Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;
.super Landroid/view/OrientationEventListener;
.source "SourceFile"

# interfaces
.implements Landroid/os/Handler$Callback;


# instance fields
.field private final a:Lcom/google/android/youtube/core/utils/i;

.field private final b:Landroid/os/Handler;

.field private final c:Z

.field private d:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper$DeviceOrientation;

.field private e:I

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/utils/i;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-virtual {p1}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;-><init>(Landroid/content/Context;Landroid/view/WindowManager;Lcom/google/android/youtube/core/utils/i;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/WindowManager;Lcom/google/android/youtube/core/utils/i;)V
    .locals 5

    const/4 v4, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x3

    invoke-direct {p0, p1, v0}, Landroid/view/OrientationEventListener;-><init>(Landroid/content/Context;I)V

    const-string v0, "listener cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/utils/i;

    iput-object v0, p0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->a:Lcom/google/android/youtube/core/utils/i;

    invoke-interface {p2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    if-eqz v0, :cond_0

    if-ne v0, v4, :cond_2

    :cond_0
    if-ne v3, v4, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->c:Z

    :goto_1
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->b:Landroid/os/Handler;

    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    if-ne v3, v1, :cond_3

    :goto_2
    iput-boolean v1, p0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->c:Z

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_2
.end method


# virtual methods
.method public final a()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->f:Z

    return v0
.end method

.method public final disable()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->f:Z

    iget-object v0, p0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->b:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    invoke-super {p0}, Landroid/view/OrientationEventListener;->disable()V

    return-void
.end method

.method public final enable()V
    .locals 1

    sget-object v0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper$DeviceOrientation;->UNKNOWN:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper$DeviceOrientation;

    iput-object v0, p0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->d:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper$DeviceOrientation;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->e:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->f:Z

    invoke-super {p0}, Landroid/view/OrientationEventListener;->enable()V

    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)Z
    .locals 4

    const/4 v1, 0x1

    iget v0, p1, Landroid/os/Message;->what:I

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iget v2, p0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->e:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->a:Lcom/google/android/youtube/core/utils/i;

    invoke-interface {v2, v0}, Lcom/google/android/youtube/core/utils/i;->b(Z)V

    :goto_1
    iget v0, p1, Landroid/os/Message;->what:I

    iput v0, p0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->e:I

    return v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->a:Lcom/google/android/youtube/core/utils/i;

    invoke-interface {v2, v0}, Lcom/google/android/youtube/core/utils/i;->c(Z)V

    goto :goto_1
.end method

.method public final onOrientationChanged(I)V
    .locals 6

    const/4 v3, 0x1

    const/4 v2, 0x0

    if-ltz p1, :cond_0

    const/16 v0, 0x1e

    if-le p1, v0, :cond_1

    :cond_0
    const/16 v0, 0x14a

    if-lt p1, v0, :cond_4

    const/16 v0, 0x168

    if-ge p1, v0, :cond_4

    :cond_1
    sget-object v0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper$DeviceOrientation;->UPRIGHT:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper$DeviceOrientation;

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->d:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper$DeviceOrientation;

    if-eq v0, v1, :cond_3

    iget-object v1, p0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->b:Landroid/os/Handler;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    sget-object v1, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper$DeviceOrientation;->UNKNOWN:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper$DeviceOrientation;

    if-eq v0, v1, :cond_3

    sget-object v1, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper$DeviceOrientation;->UPRIGHT:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper$DeviceOrientation;

    if-eq v0, v1, :cond_2

    sget-object v1, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper$DeviceOrientation;->BOTTOMUP:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper$DeviceOrientation;

    if-ne v0, v1, :cond_8

    :cond_2
    move v1, v3

    :goto_1
    iget-boolean v4, p0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->c:Z

    if-eqz v4, :cond_9

    :goto_2
    if-eqz v1, :cond_b

    :goto_3
    iget v1, p0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->e:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_c

    iget-object v1, p0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->b:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_3
    :goto_4
    iput-object v0, p0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->d:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper$DeviceOrientation;

    return-void

    :cond_4
    const/16 v0, 0x3c

    if-lt p1, v0, :cond_5

    const/16 v0, 0x78

    if-gt p1, v0, :cond_5

    sget-object v0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper$DeviceOrientation;->LEFTONTOP:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper$DeviceOrientation;

    goto :goto_0

    :cond_5
    const/16 v0, 0x96

    if-lt p1, v0, :cond_6

    const/16 v0, 0xd2

    if-gt p1, v0, :cond_6

    sget-object v0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper$DeviceOrientation;->BOTTOMUP:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper$DeviceOrientation;

    goto :goto_0

    :cond_6
    const/16 v0, 0xf0

    if-lt p1, v0, :cond_7

    const/16 v0, 0x12c

    if-gt p1, v0, :cond_7

    sget-object v0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper$DeviceOrientation;->RIGHTONTOP:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper$DeviceOrientation;

    goto :goto_0

    :cond_7
    sget-object v0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper$DeviceOrientation;->UNKNOWN:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper$DeviceOrientation;

    goto :goto_0

    :cond_8
    move v1, v2

    goto :goto_1

    :cond_9
    if-nez v1, :cond_a

    move v1, v3

    goto :goto_2

    :cond_a
    move v1, v2

    goto :goto_2

    :cond_b
    move v3, v2

    goto :goto_3

    :cond_c
    iget v1, p0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->e:I

    if-eq v1, v3, :cond_3

    iget-object v1, p0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->b:Landroid/os/Handler;

    const-wide/16 v4, 0xc8

    invoke-virtual {v1, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_4
.end method
