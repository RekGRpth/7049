.class public abstract Lcom/google/android/youtube/core/v11/Controller;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/youtube/core/Analytics;

.field private b:Lcom/google/android/youtube/core/v11/Controller$LifecycleState;

.field private c:Landroid/view/View;


# direct methods
.method private c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/Controller;->a:Lcom/google/android/youtube/core/Analytics;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected abstract a()I
.end method

.method final a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)V
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/v11/Controller;->a()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1, v1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    :cond_0
    iput-object v0, p0, Lcom/google/android/youtube/core/v11/Controller;->c:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/Controller;->c:Landroid/view/View;

    return-void
.end method

.method final a(Lcom/google/android/youtube/core/v11/Controller$LifecycleState;)V
    .locals 2

    const-string v0, "newState cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/Controller;->b:Lcom/google/android/youtube/core/v11/Controller$LifecycleState;

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/v11/Controller;->b:Lcom/google/android/youtube/core/v11/Controller$LifecycleState;

    iput-object p1, p0, Lcom/google/android/youtube/core/v11/Controller;->b:Lcom/google/android/youtube/core/v11/Controller$LifecycleState;

    sget-object v1, Lcom/google/android/youtube/core/v11/a;->a:[I

    invoke-virtual {v0}, Lcom/google/android/youtube/core/v11/Controller$LifecycleState;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    sget-object v0, Lcom/google/android/youtube/core/v11/a;->a:[I

    invoke-virtual {p1}, Lcom/google/android/youtube/core/v11/Controller$LifecycleState;->ordinal()I

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/google/android/youtube/core/v11/a;->a:[I

    invoke-virtual {p1}, Lcom/google/android/youtube/core/v11/Controller$LifecycleState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/youtube/core/v11/Controller;->c()V

    goto :goto_0

    :pswitch_3
    invoke-direct {p0}, Lcom/google/android/youtube/core/v11/Controller;->c()V

    goto :goto_0

    :pswitch_4
    sget-object v0, Lcom/google/android/youtube/core/v11/a;->a:[I

    invoke-virtual {p1}, Lcom/google/android/youtube/core/v11/Controller$LifecycleState;->ordinal()I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_4
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method final b()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/Controller;->c:Landroid/view/View;

    return-object v0
.end method
