.class public abstract Lcom/google/android/youtube/core/v11/SingleControllerActivity;
.super Lcom/google/android/youtube/core/v11/ControllerActivity;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/youtube/core/v11/Controller;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/core/v11/ControllerActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract b()Lcom/google/android/youtube/core/v11/Controller;
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    const/16 v2, 0x18

    invoke-super {p0, p1}, Lcom/google/android/youtube/core/v11/ControllerActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/v11/SingleControllerActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setNavigationMode(I)V

    invoke-virtual {v0, v2, v2}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/core/v11/SingleControllerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/v11/SingleControllerActivity;->b()Lcom/google/android/youtube/core/v11/Controller;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/v11/SingleControllerActivity;->a:Lcom/google/android/youtube/core/v11/Controller;

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/SingleControllerActivity;->a:Lcom/google/android/youtube/core/v11/Controller;

    invoke-virtual {p0, v0, p1}, Lcom/google/android/youtube/core/v11/SingleControllerActivity;->a(Lcom/google/android/youtube/core/v11/Controller;Landroid/os/Bundle;)V

    return-void
.end method
