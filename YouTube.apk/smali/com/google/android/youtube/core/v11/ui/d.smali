.class public final Lcom/google/android/youtube/core/v11/ui/d;
.super Landroid/os/Handler;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnSystemUiVisibilityChangeListener;
.implements Lcom/google/android/youtube/core/player/overlay/t;
.implements Lcom/google/android/youtube/core/v11/ui/b;


# static fields
.field private static final a:I

.field private static final b:I

.field private static final c:I


# instance fields
.field private final d:Landroid/view/Window;

.field private final e:Landroid/app/ActionBar;

.field private final f:Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;

.field private final g:Lcom/google/android/youtube/core/v11/ui/c;

.field private h:Landroid/graphics/Rect;

.field private final i:I

.field private j:I

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v1, 0x0

    const/4 v0, 0x1

    sget v2, Lcom/google/android/youtube/core/utils/Util;->a:I

    const/16 v3, 0xe

    if-lt v2, v3, :cond_0

    const/4 v0, 0x3

    :cond_0
    sget v2, Lcom/google/android/youtube/core/utils/Util;->a:I

    const/16 v3, 0x10

    if-lt v2, v3, :cond_1

    or-int/lit8 v0, v0, 0x4

    const/16 v1, 0x700

    :cond_1
    sput v0, Lcom/google/android/youtube/core/v11/ui/d;->a:I

    sput v1, Lcom/google/android/youtube/core/v11/ui/d;->b:I

    or-int/2addr v0, v1

    sput v0, Lcom/google/android/youtube/core/v11/ui/d;->c:I

    return-void
.end method

.method public constructor <init>(Landroid/view/Window;Landroid/app/ActionBar;Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;Lcom/google/android/youtube/core/v11/ui/c;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    const-string v0, "window cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/Window;

    iput-object v0, p0, Lcom/google/android/youtube/core/v11/ui/d;->d:Landroid/view/Window;

    const-string v0, "playerOverlaysLayout cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;

    iput-object v0, p0, Lcom/google/android/youtube/core/v11/ui/d;->f:Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;

    invoke-virtual {p3, p0}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->setSystemWindowInsetsListener(Lcom/google/android/youtube/core/player/overlay/t;)V

    iput-object p2, p0, Lcom/google/android/youtube/core/v11/ui/d;->e:Landroid/app/ActionBar;

    iput-object p4, p0, Lcom/google/android/youtube/core/v11/ui/d;->g:Lcom/google/android/youtube/core/v11/ui/c;

    invoke-virtual {p3, p0}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->setOnSystemUiVisibilityChangeListener(Landroid/view/View$OnSystemUiVisibilityChangeListener;)V

    invoke-virtual {p3}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->getSystemUiVisibility()I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/v11/ui/d;->j:I

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/app/ActionBar;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/youtube/core/v11/ui/d;->m:Z

    if-eqz p2, :cond_1

    invoke-virtual {p1}, Landroid/view/Window;->getContext()Landroid/content/Context;

    move-result-object v0

    new-array v1, v1, [I

    const v3, 0x10102eb

    aput v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/youtube/core/v11/ui/d;->i:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    :goto_1
    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    iput v2, p0, Lcom/google/android/youtube/core/v11/ui/d;->i:I

    goto :goto_1
.end method

.method private b()V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/core/v11/ui/d;->removeMessages(I)V

    iget v0, p0, Lcom/google/android/youtube/core/v11/ui/d;->j:I

    iget-boolean v3, p0, Lcom/google/android/youtube/core/v11/ui/d;->k:Z

    iget-boolean v4, p0, Lcom/google/android/youtube/core/v11/ui/d;->l:Z

    and-int/2addr v3, v4

    sget v4, Lcom/google/android/youtube/core/v11/ui/d;->a:I

    and-int/2addr v0, v4

    sget v4, Lcom/google/android/youtube/core/v11/ui/d;->a:I

    if-ne v0, v4, :cond_1

    move v0, v1

    :goto_0
    iget-boolean v4, p0, Lcom/google/android/youtube/core/v11/ui/d;->o:Z

    if-nez v4, :cond_2

    if-eq v3, v0, :cond_2

    :goto_1
    if-eqz v1, :cond_0

    const-wide/16 v0, 0x9c4

    invoke-virtual {p0, v2, v0, v1}, Lcom/google/android/youtube/core/v11/ui/d;->sendEmptyMessageDelayed(IJ)Z

    :cond_0
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method private c()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/youtube/core/v11/ui/d;->b()V

    iget-boolean v0, p0, Lcom/google/android/youtube/core/v11/ui/d;->k:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/v11/ui/d;->l:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/youtube/core/v11/ui/d;->c:I

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/core/v11/ui/d;->f:Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->setSystemUiVisibility(I)V

    return-void

    :cond_0
    sget v0, Lcom/google/android/youtube/core/v11/ui/d;->b:I

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()V
    .locals 9

    const/16 v8, 0x10

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ui/d;->f:Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->getChildCount()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_5

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ui/d;->f:Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;

    iget-boolean v0, v0, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;->a:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/youtube/core/v11/ui/d;->k:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/youtube/core/utils/Util;->a:I

    if-lt v0, v8, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ui/d;->h:Landroid/graphics/Rect;

    if-nez v0, :cond_2

    :cond_0
    invoke-virtual {v4, v2, v2, v2, v2}, Landroid/view/View;->setPadding(IIII)V

    :cond_1
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    sget v0, Lcom/google/android/youtube/core/utils/Util;->a:I

    if-lt v0, v8, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ui/d;->h:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v5, p0, Lcom/google/android/youtube/core/v11/ui/d;->h:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    iget-object v6, p0, Lcom/google/android/youtube/core/v11/ui/d;->h:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->right:I

    iget-object v7, p0, Lcom/google/android/youtube/core/v11/ui/d;->h:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v4, v0, v5, v6, v7}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_1

    :cond_3
    iget-boolean v0, p0, Lcom/google/android/youtube/core/v11/ui/d;->m:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ui/d;->d:Landroid/view/Window;

    const/16 v5, 0x9

    invoke-virtual {v0, v5}, Landroid/view/Window;->hasFeature(I)Z

    move-result v0

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/google/android/youtube/core/v11/ui/d;->i:I

    invoke-virtual {v4, v2, v0, v2, v2}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_1

    :cond_4
    invoke-virtual {v4, v2, v2, v2, v2}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_1

    :cond_5
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/v11/ui/d;->removeMessages(I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/v11/ui/d;->o:Z

    return-void
.end method

.method public final a(Landroid/graphics/Rect;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ui/d;->h:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iput-object p1, p0, Lcom/google/android/youtube/core/v11/ui/d;->h:Landroid/graphics/Rect;

    invoke-direct {p0}, Lcom/google/android/youtube/core/v11/ui/d;->d()V

    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 4

    const/16 v2, 0x400

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/youtube/core/v11/ui/d;->k:Z

    if-eq v0, p1, :cond_4

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ui/d;->d:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/youtube/core/v11/ui/d;->n:Z

    :cond_0
    iput-boolean p1, p0, Lcom/google/android/youtube/core/v11/ui/d;->k:Z

    invoke-direct {p0}, Lcom/google/android/youtube/core/v11/ui/d;->c()V

    invoke-direct {p0}, Lcom/google/android/youtube/core/v11/ui/d;->d()V

    sget v0, Lcom/google/android/youtube/core/utils/Util;->a:I

    const/16 v3, 0x10

    if-ge v0, v3, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ui/d;->d:Landroid/view/Window;

    if-nez p1, :cond_1

    iget-boolean v3, p0, Lcom/google/android/youtube/core/v11/ui/d;->n:Z

    if-eqz v3, :cond_2

    :cond_1
    move v1, v2

    :cond_2
    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setFlags(II)V

    :cond_3
    iget-boolean v0, p0, Lcom/google/android/youtube/core/v11/ui/d;->m:Z

    if-eqz v0, :cond_4

    if-eqz p1, :cond_6

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ui/d;->d:Landroid/view/Window;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/view/Window;->hasFeature(I)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ui/d;->e:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    :cond_4
    :goto_1
    return-void

    :cond_5
    move v0, v1

    goto :goto_0

    :cond_6
    if-nez p1, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ui/d;->e:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    goto :goto_1
.end method

.method public final b(Z)V
    .locals 2

    iput-boolean p1, p0, Lcom/google/android/youtube/core/v11/ui/d;->l:Z

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/v11/ui/d;->removeMessages(I)V

    invoke-direct {p0}, Lcom/google/android/youtube/core/v11/ui/d;->c()V

    sget v0, Lcom/google/android/youtube/core/utils/Util;->a:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/core/v11/ui/d;->k:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/core/v11/ui/d;->m:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ui/d;->d:Landroid/view/Window;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/view/Window;->hasFeature(I)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ui/d;->e:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ui/d;->e:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    goto :goto_0
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .locals 1

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/youtube/core/v11/ui/d;->c()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final onSystemUiVisibilityChange(I)V
    .locals 2

    sget v0, Lcom/google/android/youtube/core/utils/Util;->a:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ui/d;->f:Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->getSystemUiVisibility()I

    move-result v0

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ui/d;->f:Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;->setSystemUiVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ui/d;->g:Lcom/google/android/youtube/core/v11/ui/c;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/youtube/core/v11/ui/d;->j:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    and-int/lit8 v0, p1, 0x2

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/core/v11/ui/d;->g:Lcom/google/android/youtube/core/v11/ui/c;

    invoke-interface {v0}, Lcom/google/android/youtube/core/v11/ui/c;->k_()V

    :cond_1
    iput p1, p0, Lcom/google/android/youtube/core/v11/ui/d;->j:I

    invoke-direct {p0}, Lcom/google/android/youtube/core/v11/ui/d;->b()V

    return-void
.end method
