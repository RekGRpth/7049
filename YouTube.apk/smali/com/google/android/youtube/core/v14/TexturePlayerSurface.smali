.class public Lcom/google/android/youtube/core/v14/TexturePlayerSurface;
.super Landroid/view/ViewGroup;
.source "SourceFile"

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;
.implements Lcom/google/android/youtube/core/player/af;


# instance fields
.field private final a:Lcom/google/android/youtube/core/v14/b;

.field private final b:Landroid/view/ViewGroup;

.field private c:Lcom/google/android/youtube/core/player/ag;

.field private d:Landroid/view/Surface;

.field private e:I

.field private f:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    const/4 v3, -0x2

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/google/android/youtube/core/v14/c;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/v14/TexturePlayerSurface;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/core/v14/c;-><init>(Lcom/google/android/youtube/core/v14/TexturePlayerSurface;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/v14/TexturePlayerSurface;->b:Landroid/view/ViewGroup;

    new-instance v0, Lcom/google/android/youtube/core/v14/b;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/v14/TexturePlayerSurface;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/core/v14/b;-><init>(Lcom/google/android/youtube/core/v14/TexturePlayerSurface;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/v14/TexturePlayerSurface;->a:Lcom/google/android/youtube/core/v14/b;

    iget-object v0, p0, Lcom/google/android/youtube/core/v14/TexturePlayerSurface;->a:Lcom/google/android/youtube/core/v14/b;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/v14/b;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/v14/TexturePlayerSurface;->a:Lcom/google/android/youtube/core/v14/b;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/v14/b;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/v14/TexturePlayerSurface;->a:Lcom/google/android/youtube/core/v14/b;

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/v14/b;->setPivotX(F)V

    iget-object v0, p0, Lcom/google/android/youtube/core/v14/TexturePlayerSurface;->a:Lcom/google/android/youtube/core/v14/b;

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/v14/b;->setPivotY(F)V

    iget-object v0, p0, Lcom/google/android/youtube/core/v14/TexturePlayerSurface;->b:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/youtube/core/v14/TexturePlayerSurface;->a:Lcom/google/android/youtube/core/v14/b;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/v14/TexturePlayerSurface;->b:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/v14/TexturePlayerSurface;->addView(Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/core/v14/TexturePlayerSurface;)I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/core/v14/TexturePlayerSurface;->e:I

    return v0
.end method

.method static synthetic b(Lcom/google/android/youtube/core/v14/TexturePlayerSurface;)I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/core/v14/TexturePlayerSurface;->f:I

    return v0
.end method

.method static synthetic c(Lcom/google/android/youtube/core/v14/TexturePlayerSurface;)Lcom/google/android/youtube/core/v14/b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/v14/TexturePlayerSurface;->a:Lcom/google/android/youtube/core/v14/b;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/player/ad;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/v14/TexturePlayerSurface;->d:Landroid/view/Surface;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "MediaPlayer should only be attached after Surface has been created"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/core/v14/TexturePlayerSurface;->d:Landroid/view/Surface;

    invoke-interface {p1, v0}, Lcom/google/android/youtube/core/player/ad;->a(Landroid/view/Surface;)V

    return-void
.end method

.method public final a(Z)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final b()V
    .locals 0

    return-void
.end method

.method public final c()V
    .locals 0

    return-void
.end method

.method public final d()V
    .locals 0

    return-void
.end method

.method public final e()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/v14/TexturePlayerSurface;->d:Landroid/view/Surface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/v14/TexturePlayerSurface;->d:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    :cond_0
    return-void
.end method

.method public final f()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/v14/TexturePlayerSurface;->d:Landroid/view/Surface;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/core/v14/TexturePlayerSurface;->b:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/youtube/core/v14/TexturePlayerSurface;->b:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/youtube/core/v14/TexturePlayerSurface;->b:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/view/ViewGroup;->layout(IIII)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/core/v14/TexturePlayerSurface;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1, p2}, Landroid/view/ViewGroup;->measure(II)V

    iget-object v0, p0, Lcom/google/android/youtube/core/v14/TexturePlayerSurface;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/core/v14/TexturePlayerSurface;->b:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/v14/TexturePlayerSurface;->setMeasuredDimension(II)V

    return-void
.end method

.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 1

    new-instance v0, Landroid/view/Surface;

    invoke-direct {v0, p1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/v14/TexturePlayerSurface;->d:Landroid/view/Surface;

    iget-object v0, p0, Lcom/google/android/youtube/core/v14/TexturePlayerSurface;->c:Lcom/google/android/youtube/core/player/ag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/v14/TexturePlayerSurface;->c:Lcom/google/android/youtube/core/player/ag;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ag;->a()V

    :cond_0
    return-void
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/v14/TexturePlayerSurface;->d:Landroid/view/Surface;

    iget-object v0, p0, Lcom/google/android/youtube/core/v14/TexturePlayerSurface;->c:Lcom/google/android/youtube/core/player/ag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/v14/TexturePlayerSurface;->c:Lcom/google/android/youtube/core/player/ag;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ag;->c()V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/v14/TexturePlayerSurface;->c:Lcom/google/android/youtube/core/player/ag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/v14/TexturePlayerSurface;->c:Lcom/google/android/youtube/core/player/ag;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ag;->b()V

    :cond_0
    return-void
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/core/v14/TexturePlayerSurface;->c:Lcom/google/android/youtube/core/player/ag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/core/v14/TexturePlayerSurface;->c:Lcom/google/android/youtube/core/player/ag;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ag;->b()V

    :cond_0
    return-void
.end method

.method public setListener(Lcom/google/android/youtube/core/player/ag;)V
    .locals 1

    const-string v0, "listener cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/ag;

    iput-object v0, p0, Lcom/google/android/youtube/core/v14/TexturePlayerSurface;->c:Lcom/google/android/youtube/core/player/ag;

    return-void
.end method

.method public setOnLetterboxChangedListener(Lcom/google/android/youtube/core/player/ah;)V
    .locals 0

    return-void
.end method

.method public setVideoSize(II)V
    .locals 1

    iput p1, p0, Lcom/google/android/youtube/core/v14/TexturePlayerSurface;->e:I

    iput p2, p0, Lcom/google/android/youtube/core/v14/TexturePlayerSurface;->f:I

    iget-object v0, p0, Lcom/google/android/youtube/core/v14/TexturePlayerSurface;->a:Lcom/google/android/youtube/core/v14/b;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/v14/b;->requestLayout()V

    return-void
.end method

.method public setZoom(I)V
    .locals 0

    return-void
.end method
