.class public final Lcom/google/android/youtube/core/v14/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/bo;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/youtube/core/async/a;Landroid/accounts/Account;Landroid/os/Bundle;)Landroid/content/Intent;
    .locals 8

    const/4 v4, 0x0

    const/4 v3, 0x1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p2}, Lcom/google/android/youtube/core/async/a;->b()[Landroid/accounts/Account;

    move-result-object v0

    invoke-static {v1, v0}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    new-array v2, v3, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-virtual {p2}, Lcom/google/android/youtube/core/async/a;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v0

    iget-object v0, p2, Lcom/google/android/youtube/core/async/a;->c:Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;

    iget-object v5, v0, Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;->scope:Ljava/lang/String;

    move-object v0, p3

    move-object v6, v4

    move-object v7, p4

    invoke-static/range {v0 .. v7}, Landroid/accounts/AccountManager;->newChooseAccountIntent(Landroid/accounts/Account;Ljava/util/ArrayList;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method
