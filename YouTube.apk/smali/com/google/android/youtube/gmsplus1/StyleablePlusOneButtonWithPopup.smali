.class public Lcom/google/android/youtube/gmsplus1/StyleablePlusOneButtonWithPopup;
.super Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/gmsplus1/StyleablePlusOneButtonWithPopup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/gmsplus1/StyleablePlusOneButtonWithPopup;->f:Lcom/google/android/gms/plus/internal/ResizingTextView;

    invoke-virtual {v0, v3, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v3, v2}, Lcom/google/android/gms/plus/internal/ResizingTextView;->setTextSize(IF)V

    :cond_0
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/gmsplus1/StyleablePlusOneButtonWithPopup;->f:Lcom/google/android/gms/plus/internal/ResizingTextView;

    invoke-virtual {v0, v4, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/plus/internal/ResizingTextView;->setTextColor(I)V

    :cond_1
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/StyleablePlusOneButtonWithPopup;->f:Lcom/google/android/gms/plus/internal/ResizingTextView;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/plus/internal/ResizingTextView;->setClickable(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/StyleablePlusOneButtonWithPopup;->f:Lcom/google/android/gms/plus/internal/ResizingTextView;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/plus/internal/ResizingTextView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/StyleablePlusOneButtonWithPopup;->e:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v3}, Landroid/widget/CompoundButton;->setClickable(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/StyleablePlusOneButtonWithPopup;->e:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v3}, Landroid/widget/CompoundButton;->setFocusable(Z)V

    invoke-virtual {p0, v4}, Lcom/google/android/youtube/gmsplus1/StyleablePlusOneButtonWithPopup;->setClickable(Z)V

    return-void

    nop

    :array_0
    .array-data 4
        0x1010095
        0x1010098
    .end array-data
.end method


# virtual methods
.method public performClick()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/StyleablePlusOneButtonWithPopup;->e:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->performClick()Z

    move-result v0

    return v0
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Setting OnClickListener on StyleablePlusOneButtonWithPopup is not allowed."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
