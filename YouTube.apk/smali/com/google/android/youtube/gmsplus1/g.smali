.class final Lcom/google/android/youtube/gmsplus1/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/c;
.implements Lcom/google/android/gms/common/d;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/gmsplus1/f;

.field private final b:Lcom/google/android/gms/plus/a;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/gmsplus1/f;Landroid/content/Context;Ljava/lang/String;)V
    .locals 6

    iput-object p1, p0, Lcom/google/android/youtube/gmsplus1/g;->a:Lcom/google/android/youtube/gmsplus1/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/gms/plus/a;

    const/4 v1, 0x1

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "https://www.googleapis.com/auth/pos"

    aput-object v2, v5, v1

    move-object v1, p2

    move-object v2, p3

    move-object v3, p0

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/plus/a;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/c;Lcom/google/android/gms/common/d;[Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/youtube/gmsplus1/g;->b:Lcom/google/android/gms/plus/a;

    iput-object p3, p0, Lcom/google/android/youtube/gmsplus1/g;->c:Ljava/lang/String;

    return-void
.end method

.method private e()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/g;->a:Lcom/google/android/youtube/gmsplus1/f;

    invoke-static {v0}, Lcom/google/android/youtube/gmsplus1/f;->a(Lcom/google/android/youtube/gmsplus1/f;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/gmsplus1/g;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/g;->b:Lcom/google/android/gms/plus/a;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/a;->c(Lcom/google/android/gms/common/c;)V

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/g;->b:Lcom/google/android/gms/plus/a;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/a;->c(Lcom/google/android/gms/common/d;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/g;->a:Lcom/google/android/youtube/gmsplus1/f;

    invoke-static {v0}, Lcom/google/android/youtube/gmsplus1/f;->b(Lcom/google/android/youtube/gmsplus1/f;)Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/gmsplus1/g;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lcom/google/android/youtube/gmsplus1/g;->e()V

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/g;->b:Lcom/google/android/gms/plus/a;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/a;->d()V

    return-void
.end method

.method public final b()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/gmsplus1/g;->e()V

    return-void
.end method

.method public final c()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/gmsplus1/g;->e()V

    return-void
.end method

.method public final d()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/g;->a:Lcom/google/android/youtube/gmsplus1/f;

    invoke-static {v0}, Lcom/google/android/youtube/gmsplus1/f;->a(Lcom/google/android/youtube/gmsplus1/f;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/gmsplus1/g;->c:Ljava/lang/String;

    invoke-interface {v0, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/gmsplus1/g;->b:Lcom/google/android/gms/plus/a;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/a;->b()V

    return-void
.end method
