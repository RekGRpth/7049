.class final Lcom/google/android/ytremote/b/e;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/net/Uri;

.field final synthetic b:Lcom/google/android/ytremote/model/PairingCode;

.field final synthetic c:Lcom/google/android/ytremote/logic/e;

.field final synthetic d:Lcom/google/android/ytremote/b/d;


# direct methods
.method constructor <init>(Lcom/google/android/ytremote/b/d;Ljava/lang/String;Landroid/net/Uri;Lcom/google/android/ytremote/model/PairingCode;Lcom/google/android/ytremote/logic/e;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/ytremote/b/e;->d:Lcom/google/android/ytremote/b/d;

    iput-object p3, p0, Lcom/google/android/ytremote/b/e;->a:Landroid/net/Uri;

    iput-object p4, p0, Lcom/google/android/ytremote/b/e;->b:Lcom/google/android/ytremote/model/PairingCode;

    iput-object p5, p0, Lcom/google/android/ytremote/b/e;->c:Lcom/google/android/ytremote/logic/e;

    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    const/4 v4, 0x1

    new-instance v0, Lorg/apache/http/client/methods/HttpPost;

    iget-object v1, p0, Lcom/google/android/ytremote/b/e;->a:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "Content-Type"

    const-string v3, "text/plain; charset=\"utf-8\""

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Lorg/apache/http/Header;)V

    :try_start_0
    new-instance v1, Lorg/apache/http/entity/StringEntity;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "pairingCode="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/ytremote/b/e;->b:Lcom/google/android/ytremote/model/PairingCode;

    invoke-virtual {v3}, Lcom/google/android/ytremote/model/PairingCode;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "UTF-8"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    iget-object v1, p0, Lcom/google/android/ytremote/b/e;->d:Lcom/google/android/ytremote/b/d;

    invoke-static {v1}, Lcom/google/android/ytremote/b/d;->a(Lcom/google/android/ytremote/b/d;)Lorg/apache/http/client/HttpClient;

    move-result-object v1

    invoke-interface {v1, v0}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/ytremote/b/e;->c:Lcom/google/android/ytremote/logic/e;

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Lcom/google/android/ytremote/logic/e;->a(I)V

    goto :goto_0

    :sswitch_0
    invoke-static {}, Lcom/google/android/ytremote/b/d;->a()Ljava/lang/String;

    move-result-object v0

    const-string v2, "Starting polling for online device"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/ytremote/b/e;->d:Lcom/google/android/ytremote/b/d;

    invoke-static {v0}, Lcom/google/android/ytremote/b/d;->b(Lcom/google/android/ytremote/b/d;)V

    iget-object v0, p0, Lcom/google/android/ytremote/b/e;->d:Lcom/google/android/ytremote/b/d;

    invoke-static {v0}, Lcom/google/android/ytremote/b/d;->c(Lcom/google/android/ytremote/b/d;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    iget-object v0, p0, Lcom/google/android/ytremote/b/e;->d:Lcom/google/android/ytremote/b/d;

    iget-object v2, p0, Lcom/google/android/ytremote/b/e;->d:Lcom/google/android/ytremote/b/d;

    invoke-static {v2}, Lcom/google/android/ytremote/b/d;->d(Lcom/google/android/ytremote/b/d;)J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/google/android/ytremote/b/d;->a(Lcom/google/android/ytremote/b/d;J)J

    iget-object v0, p0, Lcom/google/android/ytremote/b/e;->d:Lcom/google/android/ytremote/b/d;

    invoke-static {v0}, Lcom/google/android/ytremote/b/d;->f(Lcom/google/android/ytremote/b/d;)Ljava/util/Timer;

    move-result-object v0

    new-instance v2, Lcom/google/android/ytremote/b/i;

    iget-object v3, p0, Lcom/google/android/ytremote/b/e;->d:Lcom/google/android/ytremote/b/d;

    iget-object v4, p0, Lcom/google/android/ytremote/b/e;->b:Lcom/google/android/ytremote/model/PairingCode;

    iget-object v5, p0, Lcom/google/android/ytremote/b/e;->c:Lcom/google/android/ytremote/logic/e;

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/ytremote/b/i;-><init>(Lcom/google/android/ytremote/b/d;Lcom/google/android/ytremote/model/PairingCode;Lcom/google/android/ytremote/logic/e;)V

    iget-object v3, p0, Lcom/google/android/ytremote/b/e;->d:Lcom/google/android/ytremote/b/d;

    invoke-static {v3}, Lcom/google/android/ytremote/b/d;->e(Lcom/google/android/ytremote/b/d;)J

    move-result-wide v3

    invoke-virtual {v0, v2, v3, v4}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    const/4 v0, 0x0

    const-string v2, "LOCATION"

    invoke-interface {v1, v2}, Lorg/apache/http/HttpResponse;->containsHeader(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v0, "LOCATION"

    invoke-interface {v1, v0}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/ytremote/b/e;->c:Lcom/google/android/ytremote/logic/e;

    invoke-interface {v1, v0}, Lcom/google/android/ytremote/logic/e;->a(Landroid/net/Uri;)V

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/ytremote/b/e;->c:Lcom/google/android/ytremote/logic/e;

    invoke-interface {v0, v4}, Lcom/google/android/ytremote/logic/e;->a(I)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/ytremote/b/e;->c:Lcom/google/android/ytremote/logic/e;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lcom/google/android/ytremote/logic/e;->a(I)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/android/ytremote/b/e;->c:Lcom/google/android/ytremote/logic/e;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/ytremote/logic/e;->a(I)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0xc9 -> :sswitch_0
        0x12f -> :sswitch_1
        0x194 -> :sswitch_2
        0x1f7 -> :sswitch_3
    .end sparse-switch
.end method
