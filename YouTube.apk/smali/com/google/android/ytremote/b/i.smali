.class final Lcom/google/android/ytremote/b/i;
.super Ljava/util/TimerTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/ytremote/b/d;

.field private final b:Lcom/google/android/ytremote/logic/e;

.field private final c:Lcom/google/android/ytremote/model/PairingCode;


# direct methods
.method public constructor <init>(Lcom/google/android/ytremote/b/d;Lcom/google/android/ytremote/model/PairingCode;Lcom/google/android/ytremote/logic/e;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/ytremote/b/i;->a:Lcom/google/android/ytremote/b/d;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    iput-object p2, p0, Lcom/google/android/ytremote/b/i;->c:Lcom/google/android/ytremote/model/PairingCode;

    iput-object p3, p0, Lcom/google/android/ytremote/b/i;->b:Lcom/google/android/ytremote/logic/e;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/ytremote/b/i;->a:Lcom/google/android/ytremote/b/d;

    invoke-static {v0}, Lcom/google/android/ytremote/b/d;->c(Lcom/google/android/ytremote/b/d;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/ytremote/b/i;->a:Lcom/google/android/ytremote/b/d;

    invoke-static {v0}, Lcom/google/android/ytremote/b/d;->g(Lcom/google/android/ytremote/b/d;)Lcom/google/android/ytremote/backend/logic/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/ytremote/b/i;->c:Lcom/google/android/ytremote/model/PairingCode;

    invoke-interface {v0, v1}, Lcom/google/android/ytremote/backend/logic/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/ytremote/model/CloudScreen;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/ytremote/b/d;->a()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Found screen with id: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/ytremote/model/CloudScreen;->getScreenId()Lcom/google/android/ytremote/model/ScreenId;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/ytremote/model/ScreenId;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/google/android/ytremote/b/i;->a:Lcom/google/android/ytremote/b/d;

    invoke-static {v1}, Lcom/google/android/ytremote/b/d;->c(Lcom/google/android/ytremote/b/d;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    iget-object v1, p0, Lcom/google/android/ytremote/b/i;->a:Lcom/google/android/ytremote/b/d;

    iget-object v2, p0, Lcom/google/android/ytremote/b/i;->b:Lcom/google/android/ytremote/logic/e;

    invoke-static {v1, v2, v0}, Lcom/google/android/ytremote/b/d;->a(Lcom/google/android/ytremote/b/d;Lcom/google/android/ytremote/logic/e;Lcom/google/android/ytremote/model/CloudScreen;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/ytremote/b/i;->a:Lcom/google/android/ytremote/b/d;

    iget-object v1, p0, Lcom/google/android/ytremote/b/i;->a:Lcom/google/android/ytremote/b/d;

    invoke-static {v1}, Lcom/google/android/ytremote/b/d;->d(Lcom/google/android/ytremote/b/d;)J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/google/android/ytremote/b/d;->a(Lcom/google/android/ytremote/b/d;J)J

    iget-object v0, p0, Lcom/google/android/ytremote/b/i;->a:Lcom/google/android/ytremote/b/d;

    invoke-static {v0}, Lcom/google/android/ytremote/b/d;->e(Lcom/google/android/ytremote/b/d;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/ytremote/b/i;->a:Lcom/google/android/ytremote/b/d;

    invoke-static {v0}, Lcom/google/android/ytremote/b/d;->c(Lcom/google/android/ytremote/b/d;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    iget-object v0, p0, Lcom/google/android/ytremote/b/i;->a:Lcom/google/android/ytremote/b/d;

    iget-object v1, p0, Lcom/google/android/ytremote/b/i;->b:Lcom/google/android/ytremote/logic/e;

    invoke-static {v0, v1, v5}, Lcom/google/android/ytremote/b/d;->a(Lcom/google/android/ytremote/b/d;Lcom/google/android/ytremote/logic/e;Lcom/google/android/ytremote/model/CloudScreen;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/ytremote/b/i;->a:Lcom/google/android/ytremote/b/d;

    invoke-static {v0}, Lcom/google/android/ytremote/b/d;->f(Lcom/google/android/ytremote/b/d;)Ljava/util/Timer;

    move-result-object v0

    new-instance v1, Lcom/google/android/ytremote/b/i;

    iget-object v2, p0, Lcom/google/android/ytremote/b/i;->a:Lcom/google/android/ytremote/b/d;

    iget-object v3, p0, Lcom/google/android/ytremote/b/i;->c:Lcom/google/android/ytremote/model/PairingCode;

    iget-object v4, p0, Lcom/google/android/ytremote/b/i;->b:Lcom/google/android/ytremote/logic/e;

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/ytremote/b/i;-><init>(Lcom/google/android/ytremote/b/d;Lcom/google/android/ytremote/model/PairingCode;Lcom/google/android/ytremote/logic/e;)V

    iget-object v2, p0, Lcom/google/android/ytremote/b/i;->a:Lcom/google/android/ytremote/b/d;

    invoke-static {v2}, Lcom/google/android/ytremote/b/d;->e(Lcom/google/android/ytremote/b/d;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/ytremote/b/i;->a:Lcom/google/android/ytremote/b/d;

    iget-object v1, p0, Lcom/google/android/ytremote/b/i;->b:Lcom/google/android/ytremote/logic/e;

    invoke-static {v0, v1, v5}, Lcom/google/android/ytremote/b/d;->a(Lcom/google/android/ytremote/b/d;Lcom/google/android/ytremote/logic/e;Lcom/google/android/ytremote/model/CloudScreen;)V

    goto :goto_0
.end method
