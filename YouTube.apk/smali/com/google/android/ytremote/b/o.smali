.class final Lcom/google/android/ytremote/b/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/ytremote/b/k;


# direct methods
.method constructor <init>(Lcom/google/android/ytremote/b/k;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/ytremote/b/o;->a:Lcom/google/android/ytremote/b/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 11

    const-wide/16 v0, 0x24b8

    iget-object v2, p0, Lcom/google/android/ytremote/b/o;->a:Lcom/google/android/ytremote/b/k;

    invoke-static {v2}, Lcom/google/android/ytremote/b/k;->a(Lcom/google/android/ytremote/b/k;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Future;

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3}, Ljava/util/concurrent/Future;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    const-wide/16 v7, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    sub-long v5, v9, v5

    sub-long v5, v1, v5

    invoke-static {v7, v8, v5, v6}, Ljava/lang/Math;->max(JJ)J
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-wide v0

    move-wide v1, v0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/google/android/ytremote/b/k;->a()Ljava/lang/String;

    move-result-object v3

    const-string v5, "Error waiting for reading device response task to complete"

    invoke-static {v3, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {}, Lcom/google/android/ytremote/b/k;->a()Ljava/lang/String;

    move-result-object v3

    const-string v5, "Error waiting for reading device response task to complete"

    invoke-static {v3, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_2
    move-exception v3

    invoke-static {}, Lcom/google/android/ytremote/b/k;->a()Ljava/lang/String;

    move-result-object v5

    const-string v6, "Timed out while waiting for reading device response task to complete"

    invoke-static {v5, v6, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v3, 0x1

    invoke-interface {v0, v3}, Ljava/util/concurrent/Future;->cancel(Z)Z

    goto :goto_0

    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/android/ytremote/b/o;->a:Lcom/google/android/ytremote/b/k;

    invoke-static {v0}, Lcom/google/android/ytremote/b/k;->b(Lcom/google/android/ytremote/b/k;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/ytremote/b/o;->a:Lcom/google/android/ytremote/b/k;

    invoke-static {v1}, Lcom/google/android/ytremote/b/k;->b(Lcom/google/android/ytremote/b/k;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/ytremote/model/d;

    iget-object v4, p0, Lcom/google/android/ytremote/b/o;->a:Lcom/google/android/ytremote/b/k;

    invoke-static {v4}, Lcom/google/android/ytremote/b/k;->c(Lcom/google/android/ytremote/b/k;)Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/ytremote/b/o;->a:Lcom/google/android/ytremote/b/k;

    invoke-static {v2}, Lcom/google/android/ytremote/b/k;->b(Lcom/google/android/ytremote/b/k;)Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/android/ytremote/b/o;->a:Lcom/google/android/ytremote/b/k;

    invoke-static {v0}, Lcom/google/android/ytremote/b/k;->d(Lcom/google/android/ytremote/b/k;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-object v0, p0, Lcom/google/android/ytremote/b/o;->a:Lcom/google/android/ytremote/b/k;

    invoke-static {v0}, Lcom/google/android/ytremote/b/k;->c(Lcom/google/android/ytremote/b/k;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lcom/google/android/ytremote/b/o;->a:Lcom/google/android/ytremote/b/k;

    invoke-static {v0}, Lcom/google/android/ytremote/b/k;->e(Lcom/google/android/ytremote/b/k;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-object v0, p0, Lcom/google/android/ytremote/b/o;->a:Lcom/google/android/ytremote/b/k;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/ytremote/b/k;->a(Lcom/google/android/ytremote/b/k;Z)Z

    return-void
.end method
