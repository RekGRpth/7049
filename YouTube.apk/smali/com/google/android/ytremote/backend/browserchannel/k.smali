.class public Lcom/google/android/ytremote/backend/browserchannel/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/ytremote/backend/logic/CloudService;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/String;

.field private static final c:Ljava/util/List;


# instance fields
.field private d:Lcom/google/android/ytremote/backend/browserchannel/r;

.field private final e:Lcom/google/android/ytremote/backend/browserchannel/c;

.field private f:Lcom/google/android/ytremote/backend/browserchannel/a;

.field private g:Lcom/google/android/ytremote/backend/model/a;

.field private final h:Landroid/content/Context;

.field private final i:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private j:Z

.field private k:Ljava/lang/Thread;

.field private l:Z

.field private m:Z

.field private n:Ljava/util/concurrent/CountDownLatch;

.field private final o:Z

.field private final p:Ljava/util/concurrent/ExecutorService;

.field private final q:Ljava/util/Queue;

.field private r:I

.field private s:I

.field private t:Ljava/util/concurrent/CountDownLatch;

.field private final u:Ljava/util/Timer;

.field private v:Ljava/util/TimerTask;

.field private w:J

.field private final x:Ljava/util/concurrent/ScheduledExecutorService;

.field private y:Ljava/util/concurrent/ScheduledFuture;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/ytremote/backend/browserchannel/k;->a:Ljava/lang/String;

    const-class v0, Lcom/google/android/ytremote/backend/browserchannel/k;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/ytremote/backend/browserchannel/k;->b:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/google/android/ytremote/backend/browserchannel/k;->c:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/ytremote/backend/browserchannel/c;)V
    .locals 3

    const/4 v0, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->j:Z

    iput-boolean v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->l:Z

    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->q:Ljava/util/Queue;

    const/16 v0, 0x1388

    iput v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->r:I

    iput-object p1, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->h:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->e:Lcom/google/android/ytremote/backend/browserchannel/c;

    invoke-direct {p0}, Lcom/google/android/ytremote/backend/browserchannel/k;->f()V

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v0, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->t:Ljava/util/concurrent/CountDownLatch;

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v0, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->n:Ljava/util/concurrent/CountDownLatch;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->p:Ljava/util/concurrent/ExecutorService;

    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadScheduledExecutor()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->x:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v0, Ljava/util/Timer;

    const-string v1, "Timer - Reconnect to RC server"

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->u:Ljava/util/Timer;

    invoke-static {p1}, Lcom/google/android/ytremote/a/b/a;->a(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->o:Z

    return-void
.end method

.method static synthetic a(Lcom/google/android/ytremote/backend/browserchannel/k;Lcom/google/android/ytremote/backend/browserchannel/a;)Lcom/google/android/ytremote/backend/browserchannel/a;
    .locals 0

    iput-object p1, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->f:Lcom/google/android/ytremote/backend/browserchannel/a;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/ytremote/backend/browserchannel/k;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/ytremote/backend/browserchannel/k;Lcom/google/android/ytremote/backend/model/Method;Lcom/google/android/ytremote/backend/model/Params;Ljava/util/List;)V
    .locals 6

    const-wide/16 v2, 0x0

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->n:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->getCount()J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->t:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->getCount()J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->t:Ljava/util/concurrent/CountDownLatch;

    sget v1, Lcom/google/android/ytremote/backend/browserchannel/r;->a:I

    int-to-long v1, v1

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->n:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v1, 0x5

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    iget-boolean v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->m:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->q:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    sget-object v0, Lcom/google/android/ytremote/backend/logic/CloudService$OnSendMessageResult$SendMessageResult;->NOT_CONNECTED:Lcom/google/android/ytremote/backend/logic/CloudService$OnSendMessageResult$SendMessageResult;

    invoke-static {p3, v0}, Lcom/google/android/ytremote/backend/browserchannel/k;->a(Ljava/util/List;Lcom/google/android/ytremote/backend/logic/CloudService$OnSendMessageResult$SendMessageResult;)V

    sget-object v1, Lcom/google/android/ytremote/backend/browserchannel/k;->b:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Dropping call for method:"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "["

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "], because"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/ytremote/backend/browserchannel/k;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, " still connecting, but not done"

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->h:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/ytremote/a/b/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Had to drop call for method: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " because not connected"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/google/android/ytremote/backend/browserchannel/k;->b:Ljava/lang/String;

    const-string v2, "Interrupted while waiting to connect."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_2
    const-string v0, " not connected"

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->f:Lcom/google/android/ytremote/backend/browserchannel/a;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/ytremote/backend/browserchannel/a;->a(Lcom/google/android/ytremote/backend/model/Method;Lcom/google/android/ytremote/backend/model/Params;)Lcom/google/android/ytremote/backend/browserchannel/j;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    :goto_2
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/google/android/ytremote/backend/browserchannel/j;->b()I

    move-result v0

    const/16 v1, 0xc8

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->q:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->s:I

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/ytremote/backend/logic/CloudService$OnSendMessageResult;

    :try_start_2
    sget-object v0, Lcom/google/android/ytremote/backend/logic/CloudService$OnSendMessageResult$SendMessageResult;->OK:Lcom/google/android/ytremote/backend/logic/CloudService$OnSendMessageResult$SendMessageResult;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_3

    :catch_1
    move-exception v0

    sget-object v2, Lcom/google/android/ytremote/backend/browserchannel/k;->b:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exception while sending message: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    goto :goto_2

    :catch_2
    move-exception v2

    sget-object v3, Lcom/google/android/ytremote/backend/browserchannel/k;->b:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "OnSendMessageResult "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " threw exception"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_4
    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->h:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/ytremote/a/b/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "sendJson returned a non-200 response. This shouldn\'t happen."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    iget v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->s:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->s:I

    const/4 v1, 0x2

    if-ge v0, v1, :cond_7

    sget-object v0, Lcom/google/android/ytremote/backend/browserchannel/k;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Increasing recent errors and retrying: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->s:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    :goto_4
    return-void

    :cond_7
    sget-object v0, Lcom/google/android/ytremote/backend/browserchannel/k;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Too many errors on sending "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "). Reconnecting..."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/ytremote/backend/browserchannel/k;->e()V

    goto :goto_4
.end method

.method static synthetic a(Lcom/google/android/ytremote/backend/browserchannel/k;Ljava/util/List;Lcom/google/android/ytremote/backend/logic/CloudService$OnSendMessageResult$SendMessageResult;)V
    .locals 0

    invoke-static {p1, p2}, Lcom/google/android/ytremote/backend/browserchannel/k;->a(Ljava/util/List;Lcom/google/android/ytremote/backend/logic/CloudService$OnSendMessageResult$SendMessageResult;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/ytremote/backend/browserchannel/k;Ljava/util/concurrent/CountDownLatch;)V
    .locals 0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    :cond_0
    return-void
.end method

.method private declared-synchronized a(Lcom/google/android/ytremote/backend/model/Method;Lcom/google/android/ytremote/backend/model/Params;Ljava/util/List;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->q:Ljava/util/Queue;

    new-instance v1, Lcom/google/android/ytremote/backend/browserchannel/r;

    invoke-direct {v1, p1, p2, p3}, Lcom/google/android/ytremote/backend/browserchannel/r;-><init>(Lcom/google/android/ytremote/backend/model/Method;Lcom/google/android/ytremote/backend/model/Params;Ljava/util/List;)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->d:Lcom/google/android/ytremote/backend/browserchannel/r;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/ytremote/backend/browserchannel/k;->g()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static a(Ljava/util/List;Lcom/google/android/ytremote/backend/logic/CloudService$OnSendMessageResult$SendMessageResult;)V
    .locals 2

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/ytremote/backend/browserchannel/k;Z)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->m:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/ytremote/backend/browserchannel/k;Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/ytremote/backend/browserchannel/k;->b(Z)V

    return-void
.end method

.method private b(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->k:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->k:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->f:Lcom/google/android/ytremote/backend/browserchannel/a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Lcom/google/android/ytremote/backend/browserchannel/a;->a(ZZ)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/ytremote/backend/browserchannel/k;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->m:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/ytremote/backend/browserchannel/k;)Lcom/google/android/ytremote/backend/browserchannel/c;
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->e:Lcom/google/android/ytremote/backend/browserchannel/c;

    return-object v0
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/ytremote/backend/browserchannel/k;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/ytremote/backend/browserchannel/k;Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/ytremote/backend/browserchannel/k;->d(Z)V

    return-void
.end method

.method private c(Z)V
    .locals 2

    iput-boolean p1, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->m:Z

    iget-object v1, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->h:Landroid/content/Context;

    if-eqz p1, :cond_0

    sget-object v0, Lcom/google/android/ytremote/intent/Intents$IntentAction;->CONNECTION_STATUS_CONNECTED:Lcom/google/android/ytremote/intent/Intents$IntentAction;

    invoke-virtual {v0}, Lcom/google/android/ytremote/intent/Intents$IntentAction;->asIntent()Landroid/content/Intent;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void

    :cond_0
    sget-object v0, Lcom/google/android/ytremote/intent/Intents$IntentAction;->CONNECTION_STATUS_DISCONNECTED:Lcom/google/android/ytremote/intent/Intents$IntentAction;

    invoke-virtual {v0}, Lcom/google/android/ytremote/intent/Intents$IntentAction;->asIntent()Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method private static d()Ljava/lang/String;
    .locals 4

    :try_start_0
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/NetworkInterface;

    invoke-virtual {v0}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/InetAddress;

    invoke-virtual {v0}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/google/android/ytremote/backend/browserchannel/k;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/net/SocketException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic d(Lcom/google/android/ytremote/backend/browserchannel/k;)V
    .locals 4

    :try_start_0
    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->f:Lcom/google/android/ytremote/backend/browserchannel/a;

    invoke-virtual {v0}, Lcom/google/android/ytremote/backend/browserchannel/a;->a()V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/ytremote/backend/browserchannel/k;->c(Z)V

    invoke-direct {p0}, Lcom/google/android/ytremote/backend/browserchannel/k;->f()V
    :try_end_0
    .catch Lcom/google/android/ytremote/backend/browserchannel/UnexpectedReponseCodeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    new-instance v0, Lcom/google/android/ytremote/backend/browserchannel/n;

    const-string v1, "HangingGetThread"

    invoke-direct {v0, p0, v1}, Lcom/google/android/ytremote/backend/browserchannel/n;-><init>(Lcom/google/android/ytremote/backend/browserchannel/k;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->k:Ljava/lang/Thread;

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->k:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/google/android/ytremote/backend/browserchannel/k;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected response when binding channel: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/ytremote/backend/browserchannel/UnexpectedReponseCodeException;->getCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-virtual {v0}, Lcom/google/android/ytremote/backend/browserchannel/UnexpectedReponseCodeException;->getCode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_1
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/ytremote/backend/browserchannel/k;->e()V

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/ytremote/backend/browserchannel/k;->a(Z)V

    goto :goto_1

    :catch_1
    move-exception v0

    sget-object v1, Lcom/google/android/ytremote/backend/browserchannel/k;->b:Ljava/lang/String;

    const-string v2, "Error connecting to Remote Control server:"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-direct {p0}, Lcom/google/android/ytremote/backend/browserchannel/k;->e()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x191
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private d(Z)V
    .locals 2

    if-eqz p1, :cond_0

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->n:Ljava/util/concurrent/CountDownLatch;

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->h:Landroid/content/Context;

    sget-object v1, Lcom/google/android/ytremote/intent/Intents$IntentAction;->CONNECTION_STATUS_STARTED_CONNECTING:Lcom/google/android/ytremote/intent/Intents$IntentAction;

    invoke-virtual {v1}, Lcom/google/android/ytremote/intent/Intents$IntentAction;->asIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->n:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->h:Landroid/content/Context;

    sget-object v1, Lcom/google/android/ytremote/intent/Intents$IntentAction;->CONNECTION_STATUS_STOPPED_CONNECTING:Lcom/google/android/ytremote/intent/Intents$IntentAction;

    invoke-virtual {v1}, Lcom/google/android/ytremote/intent/Intents$IntentAction;->asIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method static synthetic d(Lcom/google/android/ytremote/backend/browserchannel/k;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->l:Z

    return p1
.end method

.method private e()V
    .locals 6

    const-wide/16 v4, 0x2

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/ytremote/backend/browserchannel/k;->c(Z)V

    invoke-direct {p0, v1}, Lcom/google/android/ytremote/backend/browserchannel/k;->d(Z)V

    invoke-direct {p0, v1}, Lcom/google/android/ytremote/backend/browserchannel/k;->b(Z)V

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->j:Z

    if-eqz v0, :cond_1

    iput-boolean v1, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->j:Z

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->g:Lcom/google/android/ytremote/backend/model/a;

    invoke-virtual {p0, v0}, Lcom/google/android/ytremote/backend/browserchannel/k;->a(Lcom/google/android/ytremote/backend/model/a;)Ljava/util/concurrent/CountDownLatch;

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->h:Landroid/content/Context;

    sget-object v1, Lcom/google/android/ytremote/intent/Intents$IntentAction;->LOUNGE_SERVER_CONNECTION_ERROR:Lcom/google/android/ytremote/intent/Intents$IntentAction;

    invoke-virtual {v1}, Lcom/google/android/ytremote/intent/Intents$IntentAction;->asIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->h:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/ytremote/a/d/b;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/android/ytremote/intent/Intents$IntentAction;->CLOUD_SERVICE_NO_NETWORK:Lcom/google/android/ytremote/intent/Intents$IntentAction;

    invoke-virtual {v0}, Lcom/google/android/ytremote/intent/Intents$IntentAction;->asIntent()Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->h:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->t:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->getCount()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->w:J

    mul-long/2addr v0, v4

    const-wide/32 v2, 0x3a980

    cmp-long v0, v0, v2

    if-gez v0, :cond_3

    iget-wide v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->w:J

    mul-long/2addr v0, v4

    iput-wide v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->w:J

    :cond_3
    sget-object v0, Lcom/google/android/ytremote/backend/browserchannel/k;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Reconnecting in "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->w:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ms."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->t:Ljava/util/concurrent/CountDownLatch;

    new-instance v0, Lcom/google/android/ytremote/backend/browserchannel/q;

    invoke-direct {v0, p0}, Lcom/google/android/ytremote/backend/browserchannel/q;-><init>(Lcom/google/android/ytremote/backend/browserchannel/k;)V

    iput-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->v:Ljava/util/TimerTask;

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->u:Ljava/util/Timer;

    iget-object v1, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->v:Ljava/util/TimerTask;

    iget-wide v2, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->w:J

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    goto :goto_0
.end method

.method static synthetic e(Lcom/google/android/ytremote/backend/browserchannel/k;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/ytremote/backend/browserchannel/k;->g()V

    return-void
.end method

.method static synthetic f(Lcom/google/android/ytremote/backend/browserchannel/k;)Ljava/util/concurrent/CountDownLatch;
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->n:Ljava/util/concurrent/CountDownLatch;

    return-object v0
.end method

.method private f()V
    .locals 4

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->j:Z

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    const-wide v2, 0x408f400000000000L

    mul-double/2addr v0, v2

    double-to-int v0, v0

    add-int/lit16 v0, v0, 0x7d0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->w:J

    return-void
.end method

.method static synthetic g(Lcom/google/android/ytremote/backend/browserchannel/k;)Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/google/android/ytremote/backend/browserchannel/k;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized g()V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->q:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/ytremote/backend/browserchannel/r;

    iput-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->d:Lcom/google/android/ytremote/backend/browserchannel/r;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->p:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/google/android/ytremote/backend/browserchannel/o;

    invoke-direct {v1, p0}, Lcom/google/android/ytremote/backend/browserchannel/o;-><init>(Lcom/google/android/ytremote/backend/browserchannel/k;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->x:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v2, Lcom/google/android/ytremote/backend/browserchannel/p;

    invoke-direct {v2, p0, v0}, Lcom/google/android/ytremote/backend/browserchannel/p;-><init>(Lcom/google/android/ytremote/backend/browserchannel/k;Ljava/util/concurrent/Future;)V

    iget v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->r:I

    int-to-long v3, v0

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v2, v3, v4, v0}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/util/concurrent/Callable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->y:Ljava/util/concurrent/ScheduledFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic h(Lcom/google/android/ytremote/backend/browserchannel/k;)Lcom/google/android/ytremote/backend/browserchannel/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->f:Lcom/google/android/ytremote/backend/browserchannel/a;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/ytremote/backend/browserchannel/k;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->h:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/ytremote/backend/browserchannel/k;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->l:Z

    return v0
.end method

.method static synthetic k(Lcom/google/android/ytremote/backend/browserchannel/k;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/ytremote/backend/browserchannel/k;->e()V

    return-void
.end method

.method static synthetic l(Lcom/google/android/ytremote/backend/browserchannel/k;)Lcom/google/android/ytremote/backend/browserchannel/r;
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->d:Lcom/google/android/ytremote/backend/browserchannel/r;

    return-object v0
.end method

.method static synthetic m(Lcom/google/android/ytremote/backend/browserchannel/k;)Ljava/util/Queue;
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->q:Ljava/util/Queue;

    return-object v0
.end method

.method static synthetic n(Lcom/google/android/ytremote/backend/browserchannel/k;)Ljava/util/concurrent/ScheduledFuture;
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->y:Ljava/util/concurrent/ScheduledFuture;

    return-object v0
.end method

.method static synthetic o(Lcom/google/android/ytremote/backend/browserchannel/k;)I
    .locals 1

    iget v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->r:I

    return v0
.end method

.method static synthetic p(Lcom/google/android/ytremote/backend/browserchannel/k;)Lcom/google/android/ytremote/backend/model/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->g:Lcom/google/android/ytremote/backend/model/a;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/ytremote/backend/model/a;)Ljava/util/concurrent/CountDownLatch;
    .locals 4

    invoke-static {p1}, Lcom/google/android/ytremote/util/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->n:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->getCount()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/ytremote/backend/browserchannel/k;->b:Ljava/lang/String;

    const-string v1, "Already in the process of connecting. Ignoring connect request"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->n:Ljava/util/concurrent/CountDownLatch;

    :goto_0
    return-object v0

    :cond_0
    iput-object p1, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->g:Lcom/google/android/ytremote/backend/model/a;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->s:I

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/ytremote/backend/browserchannel/k;->d(Z)V

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->t:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    new-instance v0, Lcom/google/android/ytremote/backend/browserchannel/l;

    const-string v1, "asyncConnect"

    invoke-direct {v0, p0, v1, p1}, Lcom/google/android/ytremote/backend/browserchannel/l;-><init>(Lcom/google/android/ytremote/backend/browserchannel/k;Ljava/lang/String;Lcom/google/android/ytremote/backend/model/a;)V

    invoke-virtual {v0}, Lcom/google/android/ytremote/backend/browserchannel/l;->start()V

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->n:Ljava/util/concurrent/CountDownLatch;

    goto :goto_0
.end method

.method public final a(Lcom/google/android/ytremote/backend/model/Method;Lcom/google/android/ytremote/backend/model/Params;)V
    .locals 1

    sget-object v0, Lcom/google/android/ytremote/backend/browserchannel/k;->c:Ljava/util/List;

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/ytremote/backend/browserchannel/k;->a(Lcom/google/android/ytremote/backend/model/Method;Lcom/google/android/ytremote/backend/model/Params;Ljava/util/List;)V

    return-void
.end method

.method final a(Ljava/util/concurrent/CountDownLatch;)V
    .locals 3
    .annotation build Lcom/google/android/ytremote/util/VisibleForTesting;
    .end annotation

    new-instance v0, Lcom/google/android/ytremote/backend/browserchannel/m;

    const-string v1, "Testing for buffered proxy"

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/ytremote/backend/browserchannel/m;-><init>(Lcom/google/android/ytremote/backend/browserchannel/k;Ljava/lang/String;Ljava/util/concurrent/CountDownLatch;)V

    invoke-virtual {v0}, Lcom/google/android/ytremote/backend/browserchannel/m;->start()V

    return-void
.end method

.method public final a(Z)V
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->q:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/ytremote/backend/browserchannel/r;

    sget-object v2, Lcom/google/android/ytremote/backend/browserchannel/k;->b:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Dropping message: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, v0, Lcom/google/android/ytremote/backend/browserchannel/r;->b:Ljava/util/List;

    sget-object v2, Lcom/google/android/ytremote/backend/logic/CloudService$OnSendMessageResult$SendMessageResult;->CANCELED:Lcom/google/android/ytremote/backend/logic/CloudService$OnSendMessageResult$SendMessageResult;

    invoke-static {v0, v2}, Lcom/google/android/ytremote/backend/browserchannel/k;->a(Ljava/util/List;Lcom/google/android/ytremote/backend/logic/CloudService$OnSendMessageResult$SendMessageResult;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->q:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->v:Ljava/util/TimerTask;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->v:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->v:Ljava/util/TimerTask;

    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->n:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v1, 0x3

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->n:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->getCount()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    sget-object v0, Lcom/google/android/ytremote/backend/browserchannel/k;->b:Ljava/lang/String;

    const-string v1, "Timed out while waiting for BC to connect. Will attempt stopping the connection anyway."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->m:Z

    if-eqz v0, :cond_3

    invoke-direct {p0, p1}, Lcom/google/android/ytremote/backend/browserchannel/k;->b(Z)V

    :cond_3
    invoke-direct {p0, v5}, Lcom/google/android/ytremote/backend/browserchannel/k;->c(Z)V

    invoke-direct {p0, v5}, Lcom/google/android/ytremote/backend/browserchannel/k;->d(Z)V

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->h:Landroid/content/Context;

    sget-object v1, Lcom/google/android/ytremote/intent/Intents$IntentAction;->BIG_SCREEN_DISCONNECTED:Lcom/google/android/ytremote/intent/Intents$IntentAction;

    invoke-virtual {v1}, Lcom/google/android/ytremote/intent/Intents$IntentAction;->asIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/google/android/ytremote/backend/browserchannel/k;->b:Ljava/lang/String;

    const-string v2, "Interrupted while waiting for BC to connect"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public final a()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->m:Z

    return v0
.end method

.method public final b()Z
    .locals 4

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/k;->n:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->getCount()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
