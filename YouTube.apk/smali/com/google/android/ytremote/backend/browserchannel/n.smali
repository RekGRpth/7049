.class final Lcom/google/android/ytremote/backend/browserchannel/n;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/ytremote/backend/browserchannel/k;


# direct methods
.method constructor <init>(Lcom/google/android/ytremote/backend/browserchannel/k;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/ytremote/backend/browserchannel/n;->a:Lcom/google/android/ytremote/backend/browserchannel/k;

    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/n;->a:Lcom/google/android/ytremote/backend/browserchannel/k;

    invoke-static {v0}, Lcom/google/android/ytremote/backend/browserchannel/k;->h(Lcom/google/android/ytremote/backend/browserchannel/k;)Lcom/google/android/ytremote/backend/browserchannel/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/ytremote/backend/browserchannel/n;->a:Lcom/google/android/ytremote/backend/browserchannel/k;

    invoke-static {v1}, Lcom/google/android/ytremote/backend/browserchannel/k;->j(Lcom/google/android/ytremote/backend/browserchannel/k;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/ytremote/backend/browserchannel/a;->a(Z)V

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/n;->a:Lcom/google/android/ytremote/backend/browserchannel/k;

    invoke-static {v0}, Lcom/google/android/ytremote/backend/browserchannel/k;->b(Lcom/google/android/ytremote/backend/browserchannel/k;)Z
    :try_end_0
    .catch Lcom/google/android/ytremote/logic/exception/NotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/google/android/ytremote/logic/exception/HttpConnectionException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/google/android/ytremote/backend/browserchannel/UnexpectedReponseCodeException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/google/android/ytremote/backend/browserchannel/NetworkNotAvailableException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_6

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/n;->a:Lcom/google/android/ytremote/backend/browserchannel/k;

    invoke-static {v0}, Lcom/google/android/ytremote/backend/browserchannel/k;->k(Lcom/google/android/ytremote/backend/browserchannel/k;)V

    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/google/android/ytremote/backend/browserchannel/k;->c()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Error on hanging get: server not found."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {}, Lcom/google/android/ytremote/backend/browserchannel/k;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Hanging GET thread interrupted."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :catch_2
    move-exception v0

    invoke-static {}, Lcom/google/android/ytremote/backend/browserchannel/k;->c()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Error on hanging get"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_3
    move-exception v0

    invoke-static {}, Lcom/google/android/ytremote/backend/browserchannel/k;->c()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Error on hanging get"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_4
    move-exception v0

    invoke-static {}, Lcom/google/android/ytremote/backend/browserchannel/k;->c()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected response on hanging get "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/ytremote/backend/browserchannel/UnexpectedReponseCodeException;->getCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Lcom/google/android/ytremote/backend/browserchannel/UnexpectedReponseCodeException;->getCode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/n;->a:Lcom/google/android/ytremote/backend/browserchannel/k;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/ytremote/backend/browserchannel/k;->a(Z)V

    goto :goto_1

    :catch_5
    move-exception v0

    invoke-static {}, Lcom/google/android/ytremote/backend/browserchannel/k;->c()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Error on hanging get. No network connection: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_6
    move-exception v0

    invoke-static {}, Lcom/google/android/ytremote/backend/browserchannel/k;->c()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Unexpected exception on hanging get"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x191
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
