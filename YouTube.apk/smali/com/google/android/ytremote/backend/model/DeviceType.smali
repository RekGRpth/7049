.class public final enum Lcom/google/android/ytremote/backend/model/DeviceType;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/ytremote/backend/model/DeviceType;

.field public static final enum LOUNGE_SCREEN:Lcom/google/android/ytremote/backend/model/DeviceType;

.field public static final enum REMOTE_CONTROL:Lcom/google/android/ytremote/backend/model/DeviceType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/ytremote/backend/model/DeviceType;

    const-string v1, "REMOTE_CONTROL"

    invoke-direct {v0, v1, v2}, Lcom/google/android/ytremote/backend/model/DeviceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/ytremote/backend/model/DeviceType;->REMOTE_CONTROL:Lcom/google/android/ytremote/backend/model/DeviceType;

    new-instance v0, Lcom/google/android/ytremote/backend/model/DeviceType;

    const-string v1, "LOUNGE_SCREEN"

    invoke-direct {v0, v1, v3}, Lcom/google/android/ytremote/backend/model/DeviceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/ytremote/backend/model/DeviceType;->LOUNGE_SCREEN:Lcom/google/android/ytremote/backend/model/DeviceType;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/ytremote/backend/model/DeviceType;

    sget-object v1, Lcom/google/android/ytremote/backend/model/DeviceType;->REMOTE_CONTROL:Lcom/google/android/ytremote/backend/model/DeviceType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/ytremote/backend/model/DeviceType;->LOUNGE_SCREEN:Lcom/google/android/ytremote/backend/model/DeviceType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/ytremote/backend/model/DeviceType;->$VALUES:[Lcom/google/android/ytremote/backend/model/DeviceType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/ytremote/backend/model/DeviceType;
    .locals 1

    const-class v0, Lcom/google/android/ytremote/backend/model/DeviceType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/ytremote/backend/model/DeviceType;

    return-object v0
.end method

.method public static values()[Lcom/google/android/ytremote/backend/model/DeviceType;
    .locals 1

    sget-object v0, Lcom/google/android/ytremote/backend/model/DeviceType;->$VALUES:[Lcom/google/android/ytremote/backend/model/DeviceType;

    invoke-virtual {v0}, [Lcom/google/android/ytremote/backend/model/DeviceType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/ytremote/backend/model/DeviceType;

    return-object v0
.end method
