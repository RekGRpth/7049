.class public final Lcom/google/android/ytremote/backend/model/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Lcom/google/android/ytremote/backend/model/DeviceType;

.field b:Ljava/lang/String;

.field c:Ljava/lang/String;

.field d:Ljava/lang/String;

.field e:Ljava/lang/String;

.field f:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/ytremote/backend/model/c;
    .locals 8

    new-instance v0, Lcom/google/android/ytremote/backend/model/c;

    iget-object v1, p0, Lcom/google/android/ytremote/backend/model/d;->a:Lcom/google/android/ytremote/backend/model/DeviceType;

    iget-object v2, p0, Lcom/google/android/ytremote/backend/model/d;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/ytremote/backend/model/d;->c:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/ytremote/backend/model/d;->d:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/ytremote/backend/model/d;->e:Ljava/lang/String;

    iget-boolean v6, p0, Lcom/google/android/ytremote/backend/model/d;->f:Z

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/ytremote/backend/model/c;-><init>(Lcom/google/android/ytremote/backend/model/DeviceType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZB)V

    return-object v0
.end method

.method public final a(Lcom/google/android/ytremote/backend/model/DeviceType;)Lcom/google/android/ytremote/backend/model/d;
    .locals 0

    iput-object p1, p0, Lcom/google/android/ytremote/backend/model/d;->a:Lcom/google/android/ytremote/backend/model/DeviceType;

    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/ytremote/backend/model/d;
    .locals 0

    iput-object p1, p0, Lcom/google/android/ytremote/backend/model/d;->b:Ljava/lang/String;

    return-object p0
.end method

.method public final a(Z)Lcom/google/android/ytremote/backend/model/d;
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/ytremote/backend/model/d;->f:Z

    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/ytremote/backend/model/d;
    .locals 0

    iput-object p1, p0, Lcom/google/android/ytremote/backend/model/d;->d:Ljava/lang/String;

    return-object p0
.end method

.method public final c(Ljava/lang/String;)Lcom/google/android/ytremote/backend/model/d;
    .locals 0

    iput-object p1, p0, Lcom/google/android/ytremote/backend/model/d;->e:Ljava/lang/String;

    return-object p0
.end method
