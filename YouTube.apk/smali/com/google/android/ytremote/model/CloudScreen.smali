.class public Lcom/google/android/ytremote/model/CloudScreen;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private final accessType:Lcom/google/android/ytremote/model/CloudScreen$AccessType;

.field private final loungeToken:Lcom/google/android/ytremote/model/LoungeToken;

.field private final name:Ljava/lang/String;

.field private final screenId:Lcom/google/android/ytremote/model/ScreenId;

.field private final temporaryAccessToken:Lcom/google/android/ytremote/model/TemporaryAccessToken;


# direct methods
.method public constructor <init>(Lcom/google/android/ytremote/model/ScreenId;Ljava/lang/String;Lcom/google/android/ytremote/model/LoungeToken;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/google/android/ytremote/model/CloudScreen$AccessType;->PERMANENT:Lcom/google/android/ytremote/model/CloudScreen$AccessType;

    iput-object v0, p0, Lcom/google/android/ytremote/model/CloudScreen;->accessType:Lcom/google/android/ytremote/model/CloudScreen$AccessType;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/ytremote/model/CloudScreen;->temporaryAccessToken:Lcom/google/android/ytremote/model/TemporaryAccessToken;

    iput-object p1, p0, Lcom/google/android/ytremote/model/CloudScreen;->screenId:Lcom/google/android/ytremote/model/ScreenId;

    iput-object p2, p0, Lcom/google/android/ytremote/model/CloudScreen;->name:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/ytremote/model/CloudScreen;->loungeToken:Lcom/google/android/ytremote/model/LoungeToken;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/ytremote/model/TemporaryAccessToken;Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/google/android/ytremote/model/CloudScreen$AccessType;->TEMPORARY:Lcom/google/android/ytremote/model/CloudScreen$AccessType;

    iput-object v0, p0, Lcom/google/android/ytremote/model/CloudScreen;->accessType:Lcom/google/android/ytremote/model/CloudScreen$AccessType;

    iput-object p1, p0, Lcom/google/android/ytremote/model/CloudScreen;->temporaryAccessToken:Lcom/google/android/ytremote/model/TemporaryAccessToken;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/ytremote/model/CloudScreen;->screenId:Lcom/google/android/ytremote/model/ScreenId;

    iput-object p2, p0, Lcom/google/android/ytremote/model/CloudScreen;->name:Ljava/lang/String;

    new-instance v0, Lcom/google/android/ytremote/model/LoungeToken;

    invoke-virtual {p1}, Lcom/google/android/ytremote/model/TemporaryAccessToken;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/ytremote/model/LoungeToken;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/ytremote/model/CloudScreen;->loungeToken:Lcom/google/android/ytremote/model/LoungeToken;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lcom/google/android/ytremote/model/CloudScreen;

    iget-object v2, p0, Lcom/google/android/ytremote/model/CloudScreen;->accessType:Lcom/google/android/ytremote/model/CloudScreen$AccessType;

    iget-object v3, p1, Lcom/google/android/ytremote/model/CloudScreen;->accessType:Lcom/google/android/ytremote/model/CloudScreen$AccessType;

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/google/android/ytremote/model/CloudScreen;->name:Ljava/lang/String;

    if-nez v2, :cond_5

    iget-object v2, p1, Lcom/google/android/ytremote/model/CloudScreen;->name:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lcom/google/android/ytremote/model/CloudScreen;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/ytremote/model/CloudScreen;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p0, Lcom/google/android/ytremote/model/CloudScreen;->screenId:Lcom/google/android/ytremote/model/ScreenId;

    if-nez v2, :cond_7

    iget-object v2, p1, Lcom/google/android/ytremote/model/CloudScreen;->screenId:Lcom/google/android/ytremote/model/ScreenId;

    if-eqz v2, :cond_8

    move v0, v1

    goto :goto_0

    :cond_7
    iget-object v2, p0, Lcom/google/android/ytremote/model/CloudScreen;->screenId:Lcom/google/android/ytremote/model/ScreenId;

    iget-object v3, p1, Lcom/google/android/ytremote/model/CloudScreen;->screenId:Lcom/google/android/ytremote/model/ScreenId;

    invoke-virtual {v2, v3}, Lcom/google/android/ytremote/model/ScreenId;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    iget-object v2, p0, Lcom/google/android/ytremote/model/CloudScreen;->temporaryAccessToken:Lcom/google/android/ytremote/model/TemporaryAccessToken;

    if-nez v2, :cond_9

    iget-object v2, p1, Lcom/google/android/ytremote/model/CloudScreen;->temporaryAccessToken:Lcom/google/android/ytremote/model/TemporaryAccessToken;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_9
    iget-object v2, p0, Lcom/google/android/ytremote/model/CloudScreen;->temporaryAccessToken:Lcom/google/android/ytremote/model/TemporaryAccessToken;

    iget-object v3, p1, Lcom/google/android/ytremote/model/CloudScreen;->temporaryAccessToken:Lcom/google/android/ytremote/model/TemporaryAccessToken;

    invoke-virtual {v2, v3}, Lcom/google/android/ytremote/model/TemporaryAccessToken;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public getAccessType()Lcom/google/android/ytremote/model/CloudScreen$AccessType;
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/model/CloudScreen;->accessType:Lcom/google/android/ytremote/model/CloudScreen$AccessType;

    return-object v0
.end method

.method public getLoungeToken()Lcom/google/android/ytremote/model/LoungeToken;
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/model/CloudScreen;->loungeToken:Lcom/google/android/ytremote/model/LoungeToken;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/model/CloudScreen;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getScreenId()Lcom/google/android/ytremote/model/ScreenId;
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/model/CloudScreen;->screenId:Lcom/google/android/ytremote/model/ScreenId;

    return-object v0
.end method

.method public getTemporaryAccessToken()Lcom/google/android/ytremote/model/TemporaryAccessToken;
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/model/CloudScreen;->temporaryAccessToken:Lcom/google/android/ytremote/model/TemporaryAccessToken;

    return-object v0
.end method

.method public hasLoungeToken()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/model/CloudScreen;->loungeToken:Lcom/google/android/ytremote/model/LoungeToken;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/ytremote/model/CloudScreen;->accessType:Lcom/google/android/ytremote/model/CloudScreen$AccessType;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/ytremote/model/CloudScreen;->name:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/ytremote/model/CloudScreen;->screenId:Lcom/google/android/ytremote/model/ScreenId;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/ytremote/model/CloudScreen;->temporaryAccessToken:Lcom/google/android/ytremote/model/TemporaryAccessToken;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/ytremote/model/CloudScreen;->accessType:Lcom/google/android/ytremote/model/CloudScreen$AccessType;

    invoke-virtual {v0}, Lcom/google/android/ytremote/model/CloudScreen$AccessType;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/ytremote/model/CloudScreen;->name:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/ytremote/model/CloudScreen;->screenId:Lcom/google/android/ytremote/model/ScreenId;

    invoke-virtual {v0}, Lcom/google/android/ytremote/model/ScreenId;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_3
    iget-object v1, p0, Lcom/google/android/ytremote/model/CloudScreen;->temporaryAccessToken:Lcom/google/android/ytremote/model/TemporaryAccessToken;

    invoke-virtual {v1}, Lcom/google/android/ytremote/model/TemporaryAccessToken;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CloudScreen [accessType="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/ytremote/model/CloudScreen;->accessType:Lcom/google/android/ytremote/model/CloudScreen$AccessType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", screenId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/ytremote/model/CloudScreen;->screenId:Lcom/google/android/ytremote/model/ScreenId;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/ytremote/model/CloudScreen;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public withLoungeToken(Lcom/google/android/ytremote/model/LoungeToken;)Lcom/google/android/ytremote/model/CloudScreen;
    .locals 3

    invoke-static {p1}, Lcom/google/android/ytremote/util/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/ytremote/model/CloudScreen;->accessType:Lcom/google/android/ytremote/model/CloudScreen$AccessType;

    sget-object v1, Lcom/google/android/ytremote/model/CloudScreen$AccessType;->PERMANENT:Lcom/google/android/ytremote/model/CloudScreen$AccessType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/ytremote/model/CloudScreen;

    iget-object v1, p0, Lcom/google/android/ytremote/model/CloudScreen;->screenId:Lcom/google/android/ytremote/model/ScreenId;

    iget-object v2, p0, Lcom/google/android/ytremote/model/CloudScreen;->name:Ljava/lang/String;

    invoke-direct {v0, v1, v2, p1}, Lcom/google/android/ytremote/model/CloudScreen;-><init>(Lcom/google/android/ytremote/model/ScreenId;Ljava/lang/String;Lcom/google/android/ytremote/model/LoungeToken;)V

    return-object v0
.end method

.method public withName(Ljava/lang/String;)Lcom/google/android/ytremote/model/CloudScreen;
    .locals 3

    sget-object v0, Lcom/google/android/ytremote/model/b;->a:[I

    iget-object v1, p0, Lcom/google/android/ytremote/model/CloudScreen;->accessType:Lcom/google/android/ytremote/model/CloudScreen$AccessType;

    invoke-virtual {v1}, Lcom/google/android/ytremote/model/CloudScreen$AccessType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-object p0

    :pswitch_0
    new-instance v0, Lcom/google/android/ytremote/model/CloudScreen;

    iget-object v1, p0, Lcom/google/android/ytremote/model/CloudScreen;->screenId:Lcom/google/android/ytremote/model/ScreenId;

    iget-object v2, p0, Lcom/google/android/ytremote/model/CloudScreen;->loungeToken:Lcom/google/android/ytremote/model/LoungeToken;

    invoke-direct {v0, v1, p1, v2}, Lcom/google/android/ytremote/model/CloudScreen;-><init>(Lcom/google/android/ytremote/model/ScreenId;Ljava/lang/String;Lcom/google/android/ytremote/model/LoungeToken;)V

    move-object p0, v0

    goto :goto_0

    :pswitch_1
    new-instance v0, Lcom/google/android/ytremote/model/CloudScreen;

    iget-object v1, p0, Lcom/google/android/ytremote/model/CloudScreen;->temporaryAccessToken:Lcom/google/android/ytremote/model/TemporaryAccessToken;

    invoke-direct {v0, v1, p1}, Lcom/google/android/ytremote/model/CloudScreen;-><init>(Lcom/google/android/ytremote/model/TemporaryAccessToken;Ljava/lang/String;)V

    move-object p0, v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
