.class public final Lcom/google/android/ytremote/model/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/net/Uri;

.field private final b:Ljava/lang/String;

.field private final c:Lcom/google/android/ytremote/model/ScreenId;

.field private final d:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, v0, v0}, Lcom/google/android/ytremote/model/a;-><init>(ILandroid/net/Uri;Ljava/lang/String;Lcom/google/android/ytremote/model/ScreenId;)V

    return-void
.end method

.method public constructor <init>(ILandroid/net/Uri;Ljava/lang/String;Lcom/google/android/ytremote/model/ScreenId;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/ytremote/model/a;->d:I

    iput-object p2, p0, Lcom/google/android/ytremote/model/a;->a:Landroid/net/Uri;

    iput-object p3, p0, Lcom/google/android/ytremote/model/a;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/ytremote/model/a;->c:Lcom/google/android/ytremote/model/ScreenId;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lcom/google/android/ytremote/model/a;->d:I

    return v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lcom/google/android/ytremote/model/a;

    iget-object v2, p0, Lcom/google/android/ytremote/model/a;->c:Lcom/google/android/ytremote/model/ScreenId;

    if-nez v2, :cond_4

    iget-object v2, p1, Lcom/google/android/ytremote/model/a;->c:Lcom/google/android/ytremote/model/ScreenId;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/google/android/ytremote/model/a;->c:Lcom/google/android/ytremote/model/ScreenId;

    iget-object v3, p1, Lcom/google/android/ytremote/model/a;->c:Lcom/google/android/ytremote/model/ScreenId;

    invoke-virtual {v2, v3}, Lcom/google/android/ytremote/model/ScreenId;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final b()Lcom/google/android/ytremote/model/ScreenId;
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/model/a;->c:Lcom/google/android/ytremote/model/ScreenId;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/model/a;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lcom/google/android/ytremote/model/a;

    iget-object v2, p0, Lcom/google/android/ytremote/model/a;->a:Landroid/net/Uri;

    if-nez v2, :cond_4

    iget-object v2, p1, Lcom/google/android/ytremote/model/a;->a:Landroid/net/Uri;

    if-eqz v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/google/android/ytremote/model/a;->a:Landroid/net/Uri;

    iget-object v3, p1, Lcom/google/android/ytremote/model/a;->a:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lcom/google/android/ytremote/model/a;->b:Ljava/lang/String;

    if-nez v2, :cond_6

    iget-object v2, p1, Lcom/google/android/ytremote/model/a;->b:Ljava/lang/String;

    if-eqz v2, :cond_7

    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p0, Lcom/google/android/ytremote/model/a;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/ytremote/model/a;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    iget-object v2, p0, Lcom/google/android/ytremote/model/a;->c:Lcom/google/android/ytremote/model/ScreenId;

    if-nez v2, :cond_8

    iget-object v2, p1, Lcom/google/android/ytremote/model/a;->c:Lcom/google/android/ytremote/model/ScreenId;

    if-eqz v2, :cond_9

    move v0, v1

    goto :goto_0

    :cond_8
    iget-object v2, p0, Lcom/google/android/ytremote/model/a;->c:Lcom/google/android/ytremote/model/ScreenId;

    iget-object v3, p1, Lcom/google/android/ytremote/model/a;->c:Lcom/google/android/ytremote/model/ScreenId;

    invoke-virtual {v2, v3}, Lcom/google/android/ytremote/model/ScreenId;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    goto :goto_0

    :cond_9
    iget v2, p0, Lcom/google/android/ytremote/model/a;->d:I

    iget v3, p1, Lcom/google/android/ytremote/model/a;->d:I

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/ytremote/model/a;->a:Landroid/net/Uri;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/ytremote/model/a;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/ytremote/model/a;->c:Lcom/google/android/ytremote/model/ScreenId;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/ytremote/model/a;->d:I

    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/ytremote/model/a;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/ytremote/model/a;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/google/android/ytremote/model/a;->c:Lcom/google/android/ytremote/model/ScreenId;

    invoke-virtual {v1}, Lcom/google/android/ytremote/model/ScreenId;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AppStatus [runningPathSegment="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/ytremote/model/a;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", screenId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/ytremote/model/a;->c:Lcom/google/android/ytremote/model/ScreenId;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/ytremote/model/a;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
