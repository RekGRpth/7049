.class public final Lcom/google/android/ytremote/model/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/ytremote/model/a;

.field private b:Landroid/net/Uri;

.field private c:Ljava/lang/String;

.field private d:Z

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Lcom/google/android/ytremote/model/SsdpId;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/ytremote/model/d;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/ytremote/model/d;->a(Lcom/google/android/ytremote/model/d;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ytremote/model/e;->c:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/ytremote/model/d;->b(Lcom/google/android/ytremote/model/d;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ytremote/model/e;->e:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/ytremote/model/d;->c(Lcom/google/android/ytremote/model/d;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ytremote/model/e;->f:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/ytremote/model/d;->d(Lcom/google/android/ytremote/model/d;)Lcom/google/android/ytremote/model/SsdpId;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ytremote/model/e;->g:Lcom/google/android/ytremote/model/SsdpId;

    invoke-static {p1}, Lcom/google/android/ytremote/model/d;->e(Lcom/google/android/ytremote/model/d;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ytremote/model/e;->b:Landroid/net/Uri;

    invoke-static {p1}, Lcom/google/android/ytremote/model/d;->f(Lcom/google/android/ytremote/model/d;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/ytremote/model/e;->d:Z

    invoke-static {p1}, Lcom/google/android/ytremote/model/d;->g(Lcom/google/android/ytremote/model/d;)Lcom/google/android/ytremote/model/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ytremote/model/e;->a:Lcom/google/android/ytremote/model/a;

    return-void
.end method

.method static synthetic a(Lcom/google/android/ytremote/model/e;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/model/e;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/ytremote/model/e;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/model/e;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/ytremote/model/e;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/model/e;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/ytremote/model/e;)Lcom/google/android/ytremote/model/SsdpId;
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/model/e;->g:Lcom/google/android/ytremote/model/SsdpId;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/ytremote/model/e;)Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/model/e;->b:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/ytremote/model/e;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/ytremote/model/e;->d:Z

    return v0
.end method

.method static synthetic g(Lcom/google/android/ytremote/model/e;)Lcom/google/android/ytremote/model/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/model/e;->a:Lcom/google/android/ytremote/model/a;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/ytremote/model/d;
    .locals 1

    new-instance v0, Lcom/google/android/ytremote/model/d;

    invoke-direct {v0, p0}, Lcom/google/android/ytremote/model/d;-><init>(Lcom/google/android/ytremote/model/e;)V

    return-object v0
.end method

.method public final a(Landroid/net/Uri;)Lcom/google/android/ytremote/model/e;
    .locals 0

    iput-object p1, p0, Lcom/google/android/ytremote/model/e;->b:Landroid/net/Uri;

    return-object p0
.end method

.method public final a(Lcom/google/android/ytremote/model/SsdpId;)Lcom/google/android/ytremote/model/e;
    .locals 0

    iput-object p1, p0, Lcom/google/android/ytremote/model/e;->g:Lcom/google/android/ytremote/model/SsdpId;

    return-object p0
.end method

.method public final a(Lcom/google/android/ytremote/model/a;)Lcom/google/android/ytremote/model/e;
    .locals 0

    iput-object p1, p0, Lcom/google/android/ytremote/model/e;->a:Lcom/google/android/ytremote/model/a;

    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/ytremote/model/e;
    .locals 0

    iput-object p1, p0, Lcom/google/android/ytremote/model/e;->c:Ljava/lang/String;

    return-object p0
.end method

.method public final a(Z)Lcom/google/android/ytremote/model/e;
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/ytremote/model/e;->d:Z

    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/ytremote/model/e;
    .locals 0

    iput-object p1, p0, Lcom/google/android/ytremote/model/e;->e:Ljava/lang/String;

    return-object p0
.end method

.method public final c(Ljava/lang/String;)Lcom/google/android/ytremote/model/e;
    .locals 0

    iput-object p1, p0, Lcom/google/android/ytremote/model/e;->f:Ljava/lang/String;

    return-object p0
.end method
