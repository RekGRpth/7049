.class final Lcom/google/common/collect/bz;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Lcom/google/common/collect/bw;Lcom/google/common/collect/by;Lcom/google/common/collect/bl;)Lcom/google/common/collect/bt;
    .locals 3

    invoke-static {p0}, Lcom/google/common/base/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/common/base/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/common/base/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/common/collect/BstSide;->LEFT:Lcom/google/common/collect/BstSide;

    invoke-virtual {p0, v0}, Lcom/google/common/collect/bw;->hasChild(Lcom/google/common/collect/BstSide;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/common/collect/BstSide;->LEFT:Lcom/google/common/collect/BstSide;

    invoke-virtual {p0, v0}, Lcom/google/common/collect/bw;->getChild(Lcom/google/common/collect/BstSide;)Lcom/google/common/collect/bw;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/google/common/collect/bz;->a(Lcom/google/common/collect/bw;Lcom/google/common/collect/by;Lcom/google/common/collect/bl;)Lcom/google/common/collect/bt;

    move-result-object v0

    sget-object v1, Lcom/google/common/collect/BstSide;->LEFT:Lcom/google/common/collect/BstSide;

    invoke-virtual {v0, p0, v1, p1, p2}, Lcom/google/common/collect/bt;->a(Lcom/google/common/collect/bw;Lcom/google/common/collect/BstSide;Lcom/google/common/collect/by;Lcom/google/common/collect/bl;)Lcom/google/common/collect/bt;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/common/collect/bw;->getKey()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/google/common/collect/BstSide;->RIGHT:Lcom/google/common/collect/BstSide;

    invoke-virtual {p0, v1}, Lcom/google/common/collect/bw;->childOrNull(Lcom/google/common/collect/BstSide;)Lcom/google/common/collect/bw;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p0, v2}, Lcom/google/common/collect/BstModificationResult;->b(Lcom/google/common/collect/bw;Lcom/google/common/collect/bw;)Lcom/google/common/collect/BstModificationResult;

    move-result-object v2

    invoke-static {v0, p0, v1, v2}, Lcom/google/common/collect/bt;->a(Ljava/lang/Object;Lcom/google/common/collect/bw;Lcom/google/common/collect/bw;Lcom/google/common/collect/BstModificationResult;)Lcom/google/common/collect/bt;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/util/Comparator;Lcom/google/common/collect/bv;Lcom/google/common/collect/bw;Ljava/lang/Object;)Lcom/google/common/collect/bt;
    .locals 8
    .param p2    # Lcom/google/common/collect/bw;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    const/4 v1, 0x0

    invoke-static {p0}, Lcom/google/common/base/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/common/base/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcom/google/common/collect/bw;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p0, p3, v0}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-eqz v0, :cond_1

    if-gez v0, :cond_0

    sget-object v0, Lcom/google/common/collect/BstSide;->LEFT:Lcom/google/common/collect/BstSide;

    :goto_0
    invoke-virtual {p2, v0}, Lcom/google/common/collect/bw;->childOrNull(Lcom/google/common/collect/BstSide;)Lcom/google/common/collect/bw;

    move-result-object v1

    invoke-static {p0, p1, v1, p3}, Lcom/google/common/collect/bz;->a(Ljava/util/Comparator;Lcom/google/common/collect/bv;Lcom/google/common/collect/bw;Ljava/lang/Object;)Lcom/google/common/collect/bt;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/common/collect/bv;->c()Lcom/google/common/collect/by;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/common/collect/bv;->b()Lcom/google/common/collect/bl;

    move-result-object v3

    invoke-virtual {v1, p2, v0, v2, v3}, Lcom/google/common/collect/bt;->a(Lcom/google/common/collect/bw;Lcom/google/common/collect/BstSide;Lcom/google/common/collect/by;Lcom/google/common/collect/bl;)Lcom/google/common/collect/bt;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/common/collect/BstSide;->RIGHT:Lcom/google/common/collect/BstSide;

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/google/common/collect/bv;->b()Lcom/google/common/collect/bl;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/common/collect/bv;->c()Lcom/google/common/collect/by;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/common/collect/bv;->a()Lcom/google/common/collect/bs;

    move-result-object v2

    if-nez p2, :cond_2

    move-object v0, v1

    :goto_2
    invoke-interface {v2, p3, v0}, Lcom/google/common/collect/bs;->a(Ljava/lang/Object;Lcom/google/common/collect/bw;)Lcom/google/common/collect/BstModificationResult;

    move-result-object v5

    if-eqz p2, :cond_6

    sget-object v0, Lcom/google/common/collect/BstSide;->LEFT:Lcom/google/common/collect/BstSide;

    invoke-virtual {p2, v0}, Lcom/google/common/collect/bw;->childOrNull(Lcom/google/common/collect/BstSide;)Lcom/google/common/collect/bw;

    move-result-object v2

    sget-object v0, Lcom/google/common/collect/BstSide;->RIGHT:Lcom/google/common/collect/BstSide;

    invoke-virtual {p2, v0}, Lcom/google/common/collect/bw;->childOrNull(Lcom/google/common/collect/BstSide;)Lcom/google/common/collect/bw;

    move-result-object v0

    :goto_3
    sget-object v6, Lcom/google/common/collect/ca;->a:[I

    invoke-virtual {v5}, Lcom/google/common/collect/BstModificationResult;->c()Lcom/google/common/collect/BstModificationResult$ModificationType;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/common/collect/BstModificationResult$ModificationType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_2
    invoke-virtual {v4, p2, v1, v1}, Lcom/google/common/collect/by;->a(Lcom/google/common/collect/bw;Lcom/google/common/collect/bw;Lcom/google/common/collect/bw;)Lcom/google/common/collect/bw;

    move-result-object v0

    goto :goto_2

    :pswitch_0
    move-object v1, p2

    :cond_3
    :goto_4
    invoke-static {p3, p2, v1, v5}, Lcom/google/common/collect/bt;->a(Ljava/lang/Object;Lcom/google/common/collect/bw;Lcom/google/common/collect/bw;Lcom/google/common/collect/BstModificationResult;)Lcom/google/common/collect/bt;

    move-result-object v0

    goto :goto_1

    :pswitch_1
    invoke-virtual {v5}, Lcom/google/common/collect/BstModificationResult;->b()Lcom/google/common/collect/bw;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {v5}, Lcom/google/common/collect/BstModificationResult;->b()Lcom/google/common/collect/bw;

    move-result-object v1

    invoke-virtual {v4, v1, v2, v0}, Lcom/google/common/collect/by;->a(Lcom/google/common/collect/bw;Lcom/google/common/collect/bw;Lcom/google/common/collect/bw;)Lcom/google/common/collect/bw;

    move-result-object v1

    goto :goto_4

    :cond_4
    if-eqz p2, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Modification result is a REBUILDING_CHANGE, but rebalancing required"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :pswitch_2
    invoke-virtual {v5}, Lcom/google/common/collect/BstModificationResult;->b()Lcom/google/common/collect/bw;

    move-result-object v6

    if-eqz v6, :cond_5

    invoke-virtual {v5}, Lcom/google/common/collect/BstModificationResult;->b()Lcom/google/common/collect/bw;

    move-result-object v1

    invoke-interface {v3, v4, v1, v2, v0}, Lcom/google/common/collect/bl;->a(Lcom/google/common/collect/by;Lcom/google/common/collect/bw;Lcom/google/common/collect/bw;Lcom/google/common/collect/bw;)Lcom/google/common/collect/bw;

    move-result-object v1

    goto :goto_4

    :cond_5
    if-eqz p2, :cond_3

    invoke-interface {v3, v4, v2, v0}, Lcom/google/common/collect/bl;->a(Lcom/google/common/collect/by;Lcom/google/common/collect/bw;Lcom/google/common/collect/bw;)Lcom/google/common/collect/bw;

    move-result-object v1

    goto :goto_4

    :cond_6
    move-object v0, v1

    move-object v2, v1

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Lcom/google/common/collect/bw;Lcom/google/common/collect/bw;Lcom/google/common/collect/by;Lcom/google/common/collect/bl;)Lcom/google/common/collect/bw;
    .locals 2
    .param p0    # Lcom/google/common/collect/bw;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x0

    invoke-static {p1}, Lcom/google/common/base/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/common/base/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p3}, Lcom/google/common/base/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    if-nez p0, :cond_0

    invoke-virtual {p2, p1, v0, v0}, Lcom/google/common/collect/by;->a(Lcom/google/common/collect/bw;Lcom/google/common/collect/bw;Lcom/google/common/collect/bw;)Lcom/google/common/collect/bw;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/common/collect/BstSide;->LEFT:Lcom/google/common/collect/BstSide;

    invoke-virtual {p0, v0}, Lcom/google/common/collect/bw;->childOrNull(Lcom/google/common/collect/BstSide;)Lcom/google/common/collect/bw;

    move-result-object v0

    invoke-static {v0, p1, p2, p3}, Lcom/google/common/collect/bz;->a(Lcom/google/common/collect/bw;Lcom/google/common/collect/bw;Lcom/google/common/collect/by;Lcom/google/common/collect/bl;)Lcom/google/common/collect/bw;

    move-result-object v0

    sget-object v1, Lcom/google/common/collect/BstSide;->RIGHT:Lcom/google/common/collect/BstSide;

    invoke-virtual {p0, v1}, Lcom/google/common/collect/bw;->childOrNull(Lcom/google/common/collect/BstSide;)Lcom/google/common/collect/bw;

    move-result-object v1

    invoke-interface {p3, p2, p0, v0, v1}, Lcom/google/common/collect/bl;->a(Lcom/google/common/collect/by;Lcom/google/common/collect/bw;Lcom/google/common/collect/bw;Lcom/google/common/collect/bw;)Lcom/google/common/collect/bw;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/util/Comparator;Lcom/google/common/collect/bw;Ljava/lang/Object;)Lcom/google/common/collect/bw;
    .locals 1
    .param p1    # Lcom/google/common/collect/bw;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    :goto_0
    invoke-static {p0}, Lcom/google/common/base/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    if-nez p1, :cond_1

    const/4 p1, 0x0

    :cond_0
    return-object p1

    :cond_1
    invoke-virtual {p1}, Lcom/google/common/collect/bw;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p0, p2, v0}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-eqz v0, :cond_0

    if-gez v0, :cond_2

    sget-object v0, Lcom/google/common/collect/BstSide;->LEFT:Lcom/google/common/collect/BstSide;

    :goto_1
    invoke-virtual {p1, v0}, Lcom/google/common/collect/bw;->childOrNull(Lcom/google/common/collect/BstSide;)Lcom/google/common/collect/bw;

    move-result-object p1

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/common/collect/BstSide;->RIGHT:Lcom/google/common/collect/BstSide;

    goto :goto_1
.end method

.method public static b(Lcom/google/common/collect/bw;Lcom/google/common/collect/by;Lcom/google/common/collect/bl;)Lcom/google/common/collect/bt;
    .locals 3

    invoke-static {p0}, Lcom/google/common/base/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/common/base/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/common/base/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/common/collect/BstSide;->RIGHT:Lcom/google/common/collect/BstSide;

    invoke-virtual {p0, v0}, Lcom/google/common/collect/bw;->hasChild(Lcom/google/common/collect/BstSide;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/common/collect/BstSide;->RIGHT:Lcom/google/common/collect/BstSide;

    invoke-virtual {p0, v0}, Lcom/google/common/collect/bw;->getChild(Lcom/google/common/collect/BstSide;)Lcom/google/common/collect/bw;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/google/common/collect/bz;->b(Lcom/google/common/collect/bw;Lcom/google/common/collect/by;Lcom/google/common/collect/bl;)Lcom/google/common/collect/bt;

    move-result-object v0

    sget-object v1, Lcom/google/common/collect/BstSide;->RIGHT:Lcom/google/common/collect/BstSide;

    invoke-virtual {v0, p0, v1, p1, p2}, Lcom/google/common/collect/bt;->a(Lcom/google/common/collect/bw;Lcom/google/common/collect/BstSide;Lcom/google/common/collect/by;Lcom/google/common/collect/bl;)Lcom/google/common/collect/bt;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/common/collect/bw;->getKey()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/google/common/collect/BstSide;->LEFT:Lcom/google/common/collect/BstSide;

    invoke-virtual {p0, v1}, Lcom/google/common/collect/bw;->childOrNull(Lcom/google/common/collect/BstSide;)Lcom/google/common/collect/bw;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p0, v2}, Lcom/google/common/collect/BstModificationResult;->b(Lcom/google/common/collect/bw;Lcom/google/common/collect/bw;)Lcom/google/common/collect/BstModificationResult;

    move-result-object v2

    invoke-static {v0, p0, v1, v2}, Lcom/google/common/collect/bt;->a(Ljava/lang/Object;Lcom/google/common/collect/bw;Lcom/google/common/collect/bw;Lcom/google/common/collect/BstModificationResult;)Lcom/google/common/collect/bt;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Lcom/google/common/collect/bw;Lcom/google/common/collect/bw;Lcom/google/common/collect/by;Lcom/google/common/collect/bl;)Lcom/google/common/collect/bw;
    .locals 2
    .param p0    # Lcom/google/common/collect/bw;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x0

    invoke-static {p1}, Lcom/google/common/base/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/common/base/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p3}, Lcom/google/common/base/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    if-nez p0, :cond_0

    invoke-virtual {p2, p1, v0, v0}, Lcom/google/common/collect/by;->a(Lcom/google/common/collect/bw;Lcom/google/common/collect/bw;Lcom/google/common/collect/bw;)Lcom/google/common/collect/bw;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/common/collect/BstSide;->LEFT:Lcom/google/common/collect/BstSide;

    invoke-virtual {p0, v0}, Lcom/google/common/collect/bw;->childOrNull(Lcom/google/common/collect/BstSide;)Lcom/google/common/collect/bw;

    move-result-object v0

    sget-object v1, Lcom/google/common/collect/BstSide;->RIGHT:Lcom/google/common/collect/BstSide;

    invoke-virtual {p0, v1}, Lcom/google/common/collect/bw;->childOrNull(Lcom/google/common/collect/BstSide;)Lcom/google/common/collect/bw;

    move-result-object v1

    invoke-static {v1, p1, p2, p3}, Lcom/google/common/collect/bz;->b(Lcom/google/common/collect/bw;Lcom/google/common/collect/bw;Lcom/google/common/collect/by;Lcom/google/common/collect/bl;)Lcom/google/common/collect/bw;

    move-result-object v1

    invoke-interface {p3, p2, p0, v0, v1}, Lcom/google/common/collect/bl;->a(Lcom/google/common/collect/by;Lcom/google/common/collect/bw;Lcom/google/common/collect/bw;Lcom/google/common/collect/bw;)Lcom/google/common/collect/bw;

    move-result-object v0

    goto :goto_0
.end method
