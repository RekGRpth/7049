.class final Lcom/google/common/collect/gk;
.super Lcom/google/common/collect/gh;
.source "SourceFile"

# interfaces
.implements Lcom/google/common/collect/gg;


# instance fields
.field volatile d:J

.field e:Lcom/google/common/collect/gg;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "Segment.this"
    .end annotation
.end field

.field f:Lcom/google/common/collect/gg;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "Segment.this"
    .end annotation
.end field

.field g:Lcom/google/common/collect/gg;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "Segment.this"
    .end annotation
.end field

.field h:Lcom/google/common/collect/gg;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "Segment.this"
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILcom/google/common/collect/gg;)V
    .locals 2
    .param p4    # Lcom/google/common/collect/gg;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/common/collect/gh;-><init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILcom/google/common/collect/gg;)V

    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/google/common/collect/gk;->d:J

    invoke-static {}, Lcom/google/common/collect/MapMakerInternalMap;->nullEntry()Lcom/google/common/collect/gg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/gk;->e:Lcom/google/common/collect/gg;

    invoke-static {}, Lcom/google/common/collect/MapMakerInternalMap;->nullEntry()Lcom/google/common/collect/gg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/gk;->f:Lcom/google/common/collect/gg;

    invoke-static {}, Lcom/google/common/collect/MapMakerInternalMap;->nullEntry()Lcom/google/common/collect/gg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/gk;->g:Lcom/google/common/collect/gg;

    invoke-static {}, Lcom/google/common/collect/MapMakerInternalMap;->nullEntry()Lcom/google/common/collect/gg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/gk;->h:Lcom/google/common/collect/gg;

    return-void
.end method


# virtual methods
.method public final getExpirationTime()J
    .locals 2

    iget-wide v0, p0, Lcom/google/common/collect/gk;->d:J

    return-wide v0
.end method

.method public final getNextEvictable()Lcom/google/common/collect/gg;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/gk;->g:Lcom/google/common/collect/gg;

    return-object v0
.end method

.method public final getNextExpirable()Lcom/google/common/collect/gg;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/gk;->e:Lcom/google/common/collect/gg;

    return-object v0
.end method

.method public final getPreviousEvictable()Lcom/google/common/collect/gg;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/gk;->h:Lcom/google/common/collect/gg;

    return-object v0
.end method

.method public final getPreviousExpirable()Lcom/google/common/collect/gg;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/gk;->f:Lcom/google/common/collect/gg;

    return-object v0
.end method

.method public final setExpirationTime(J)V
    .locals 0

    iput-wide p1, p0, Lcom/google/common/collect/gk;->d:J

    return-void
.end method

.method public final setNextEvictable(Lcom/google/common/collect/gg;)V
    .locals 0

    iput-object p1, p0, Lcom/google/common/collect/gk;->g:Lcom/google/common/collect/gg;

    return-void
.end method

.method public final setNextExpirable(Lcom/google/common/collect/gg;)V
    .locals 0

    iput-object p1, p0, Lcom/google/common/collect/gk;->e:Lcom/google/common/collect/gg;

    return-void
.end method

.method public final setPreviousEvictable(Lcom/google/common/collect/gg;)V
    .locals 0

    iput-object p1, p0, Lcom/google/common/collect/gk;->h:Lcom/google/common/collect/gg;

    return-void
.end method

.method public final setPreviousExpirable(Lcom/google/common/collect/gg;)V
    .locals 0

    iput-object p1, p0, Lcom/google/common/collect/gk;->f:Lcom/google/common/collect/gg;

    return-void
.end method
