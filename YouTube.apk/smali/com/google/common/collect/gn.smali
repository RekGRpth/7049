.class final Lcom/google/common/collect/gn;
.super Lcom/google/common/collect/gm;
.source "SourceFile"

# interfaces
.implements Lcom/google/common/collect/gg;


# instance fields
.field e:Lcom/google/common/collect/gg;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "Segment.this"
    .end annotation
.end field

.field f:Lcom/google/common/collect/gg;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "Segment.this"
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/Object;ILcom/google/common/collect/gg;)V
    .locals 1
    .param p3    # Lcom/google/common/collect/gg;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2, p3}, Lcom/google/common/collect/gm;-><init>(Ljava/lang/Object;ILcom/google/common/collect/gg;)V

    invoke-static {}, Lcom/google/common/collect/MapMakerInternalMap;->nullEntry()Lcom/google/common/collect/gg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/gn;->e:Lcom/google/common/collect/gg;

    invoke-static {}, Lcom/google/common/collect/MapMakerInternalMap;->nullEntry()Lcom/google/common/collect/gg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/gn;->f:Lcom/google/common/collect/gg;

    return-void
.end method


# virtual methods
.method public final getNextEvictable()Lcom/google/common/collect/gg;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/gn;->e:Lcom/google/common/collect/gg;

    return-object v0
.end method

.method public final getPreviousEvictable()Lcom/google/common/collect/gg;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/gn;->f:Lcom/google/common/collect/gg;

    return-object v0
.end method

.method public final setNextEvictable(Lcom/google/common/collect/gg;)V
    .locals 0

    iput-object p1, p0, Lcom/google/common/collect/gn;->e:Lcom/google/common/collect/gg;

    return-void
.end method

.method public final setPreviousEvictable(Lcom/google/common/collect/gg;)V
    .locals 0

    iput-object p1, p0, Lcom/google/common/collect/gn;->f:Lcom/google/common/collect/gg;

    return-void
.end method
