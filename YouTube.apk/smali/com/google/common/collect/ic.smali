.class final Lcom/google/common/collect/ic;
.super Lcom/google/common/collect/iy;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/common/collect/hy;


# direct methods
.method constructor <init>(Lcom/google/common/collect/hy;)V
    .locals 0

    iput-object p1, p0, Lcom/google/common/collect/ic;->a:Lcom/google/common/collect/hy;

    invoke-direct {p0}, Lcom/google/common/collect/iy;-><init>()V

    return-void
.end method


# virtual methods
.method final a()Lcom/google/common/collect/iq;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/ic;->a:Lcom/google/common/collect/hy;

    return-object v0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    const/4 v1, 0x0

    instance-of v0, p1, Lcom/google/common/collect/ir;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/google/common/collect/ir;

    iget-object v0, p0, Lcom/google/common/collect/ic;->a:Lcom/google/common/collect/hy;

    invoke-virtual {v0}, Lcom/google/common/collect/hy;->a()Lcom/google/common/collect/hu;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/common/collect/hu;->asMap()Ljava/util/Map;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/common/collect/ir;->getElement()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-interface {p1}, Lcom/google/common/collect/ir;->getCount()I

    move-result v2

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final isEmpty()Z
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/ic;->a:Lcom/google/common/collect/hy;

    invoke-virtual {v0}, Lcom/google/common/collect/hy;->a()Lcom/google/common/collect/hu;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/common/collect/hu;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/ic;->a:Lcom/google/common/collect/hy;

    invoke-virtual {v0}, Lcom/google/common/collect/hy;->entryIterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    instance-of v0, p1, Lcom/google/common/collect/ir;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/common/collect/ir;

    iget-object v0, p0, Lcom/google/common/collect/ic;->a:Lcom/google/common/collect/hy;

    invoke-virtual {v0}, Lcom/google/common/collect/hy;->a()Lcom/google/common/collect/hu;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/common/collect/hu;->asMap()Ljava/util/Map;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/common/collect/ir;->getElement()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-interface {p1}, Lcom/google/common/collect/ir;->getCount()I

    move-result v2

    if-ne v1, v2, :cond_0

    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final size()I
    .locals 1

    iget-object v0, p0, Lcom/google/common/collect/ic;->a:Lcom/google/common/collect/hy;

    invoke-virtual {v0}, Lcom/google/common/collect/hy;->distinctElements()I

    move-result v0

    return v0
.end method
