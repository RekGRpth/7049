.class final enum Lcom/google/common/hash/BloomFilterStrategies$1;
.super Lcom/google/common/hash/BloomFilterStrategies;
.source "SourceFile"


# direct methods
.method constructor <init>(Ljava/lang/String;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/common/hash/BloomFilterStrategies;-><init>(Ljava/lang/String;ILcom/google/common/hash/BloomFilterStrategies$1;)V

    return-void
.end method


# virtual methods
.method public final mightContain(Ljava/lang/Object;Lcom/google/common/hash/f;ILcom/google/common/hash/e;)Z
    .locals 10

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {}, Lcom/google/common/hash/m;->a()Lcom/google/common/hash/k;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/common/hash/k;->newHasher()Lcom/google/common/hash/l;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/google/common/hash/l;->a(Ljava/lang/Object;Lcom/google/common/hash/f;)Lcom/google/common/hash/l;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/common/hash/l;->a()Lcom/google/common/hash/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/hash/g;->b()J

    move-result-wide v3

    long-to-int v5, v3

    const/16 v0, 0x20

    ushr-long/2addr v3, v0

    long-to-int v4, v3

    move v3, v2

    :goto_0
    if-gt v3, p3, :cond_3

    mul-int v0, v3, v4

    add-int/2addr v0, v5

    if-gez v0, :cond_0

    xor-int/lit8 v0, v0, -0x1

    :cond_0
    invoke-virtual {p4}, Lcom/google/common/hash/e;->a()I

    move-result v6

    rem-int/2addr v0, v6

    iget-object v6, p4, Lcom/google/common/hash/e;->a:[J

    shr-int/lit8 v7, v0, 0x6

    aget-wide v6, v6, v7

    const-wide/16 v8, 0x1

    shl-long/2addr v8, v0

    and-long/2addr v6, v8

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    move v0, v2

    :goto_1
    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    return v0

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_2
.end method

.method public final put(Ljava/lang/Object;Lcom/google/common/hash/f;ILcom/google/common/hash/e;)V
    .locals 10

    invoke-static {}, Lcom/google/common/hash/m;->a()Lcom/google/common/hash/k;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/common/hash/k;->newHasher()Lcom/google/common/hash/l;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/google/common/hash/l;->a(Ljava/lang/Object;Lcom/google/common/hash/f;)Lcom/google/common/hash/l;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/common/hash/l;->a()Lcom/google/common/hash/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/hash/g;->b()J

    move-result-wide v0

    long-to-int v2, v0

    const/16 v3, 0x20

    ushr-long/2addr v0, v3

    long-to-int v3, v0

    const/4 v0, 0x1

    move v1, v0

    :goto_0
    if-gt v1, p3, :cond_1

    mul-int v0, v1, v3

    add-int/2addr v0, v2

    if-gez v0, :cond_0

    xor-int/lit8 v0, v0, -0x1

    :cond_0
    invoke-virtual {p4}, Lcom/google/common/hash/e;->a()I

    move-result v4

    rem-int/2addr v0, v4

    iget-object v4, p4, Lcom/google/common/hash/e;->a:[J

    shr-int/lit8 v5, v0, 0x6

    aget-wide v6, v4, v5

    const-wide/16 v8, 0x1

    shl-long/2addr v8, v0

    or-long/2addr v6, v8

    aput-wide v6, v4, v5

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method
