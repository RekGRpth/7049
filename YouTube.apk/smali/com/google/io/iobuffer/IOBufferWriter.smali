.class public final Lcom/google/io/iobuffer/IOBufferWriter;
.super Ljava/io/Writer;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/io/iobuffer/b;


# virtual methods
.method public final close()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/io/iobuffer/IOBufferWriter;->a:Lcom/google/io/iobuffer/b;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/io/iobuffer/IOBufferWriter;->a:Lcom/google/io/iobuffer/b;

    invoke-virtual {v0}, Lcom/google/io/iobuffer/b;->b()V

    iget-object v0, p0, Lcom/google/io/iobuffer/IOBufferWriter;->a:Lcom/google/io/iobuffer/b;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/io/iobuffer/b;->a(Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-object v2, p0, Lcom/google/io/iobuffer/IOBufferWriter;->a:Lcom/google/io/iobuffer/b;

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    iput-object v2, p0, Lcom/google/io/iobuffer/IOBufferWriter;->a:Lcom/google/io/iobuffer/b;

    throw v0
.end method

.method public final flush()V
    .locals 2

    iget-object v0, p0, Lcom/google/io/iobuffer/IOBufferWriter;->a:Lcom/google/io/iobuffer/b;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/io/iobuffer/IOBufferWriter$IOBufferWriterClosedException;

    invoke-direct {v0}, Lcom/google/io/iobuffer/IOBufferWriter$IOBufferWriterClosedException;-><init>()V

    throw v0

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/io/iobuffer/IOBufferWriter;->a:Lcom/google/io/iobuffer/b;

    invoke-virtual {v0}, Lcom/google/io/iobuffer/b;->b()V

    iget-object v0, p0, Lcom/google/io/iobuffer/IOBufferWriter;->a:Lcom/google/io/iobuffer/b;

    invoke-virtual {v0}, Lcom/google/io/iobuffer/b;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/io/iobuffer/IOBufferWriter;->a:Lcom/google/io/iobuffer/b;

    throw v0
.end method

.method public final write(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/io/iobuffer/IOBufferWriter;->a:Lcom/google/io/iobuffer/b;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/io/iobuffer/IOBufferWriter$IOBufferWriterClosedException;

    invoke-direct {v0}, Lcom/google/io/iobuffer/IOBufferWriter$IOBufferWriterClosedException;-><init>()V

    throw v0

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/io/iobuffer/IOBufferWriter;->a:Lcom/google/io/iobuffer/b;

    invoke-virtual {v0, p1}, Lcom/google/io/iobuffer/b;->a(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/io/iobuffer/IOBufferWriter;->a:Lcom/google/io/iobuffer/b;

    throw v0
.end method

.method public final write(Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/io/iobuffer/IOBufferWriter;->write(Ljava/lang/String;II)V

    return-void
.end method

.method public final write(Ljava/lang/String;II)V
    .locals 1

    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3}, Lcom/google/io/iobuffer/IOBufferWriter;->write([CII)V

    return-void
.end method

.method public final write([C)V
    .locals 2

    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/io/iobuffer/IOBufferWriter;->write([CII)V

    return-void
.end method

.method public final write([CII)V
    .locals 2

    iget-object v0, p0, Lcom/google/io/iobuffer/IOBufferWriter;->a:Lcom/google/io/iobuffer/b;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/io/iobuffer/IOBufferWriter$IOBufferWriterClosedException;

    invoke-direct {v0}, Lcom/google/io/iobuffer/IOBufferWriter$IOBufferWriterClosedException;-><init>()V

    throw v0

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/io/iobuffer/IOBufferWriter;->a:Lcom/google/io/iobuffer/b;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/io/iobuffer/b;->a([CII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/io/iobuffer/IOBufferWriter;->a:Lcom/google/io/iobuffer/b;

    throw v0
.end method
