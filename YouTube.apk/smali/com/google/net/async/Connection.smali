.class public final Lcom/google/net/async/Connection;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/net/async/af;
.implements Lcom/google/net/async/an;
.implements Lcom/google/net/async/e;


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lcom/google/net/async/aj;

.field private final c:Lcom/google/net/async/u;

.field private volatile d:Lcom/google/net/async/f;

.field private final e:Lcom/google/io/iobuffer/b;

.field private final f:Lcom/google/io/iobuffer/b;

.field private final g:Ljava/io/InputStream;

.field private final h:Ljava/io/OutputStream;

.field private i:Z

.field private final j:Ljava/nio/ByteBuffer;

.field private volatile k:I

.field private volatile l:I

.field private m:I

.field private volatile n:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/net/async/Connection;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/net/async/Connection;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/net/async/aj;Lcom/google/net/async/u;Lcom/google/net/async/f;Lcom/google/net/async/Connection$ConnectionMode;)V
    .locals 6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/net/async/Connection;-><init>(Lcom/google/net/async/aj;Lcom/google/net/async/u;Lcom/google/net/async/f;Lcom/google/net/async/Connection$ConnectionMode;[B)V

    return-void
.end method

.method private constructor <init>(Lcom/google/net/async/aj;Lcom/google/net/async/u;Lcom/google/net/async/f;Lcom/google/net/async/Connection$ConnectionMode;[B)V
    .locals 4

    const v3, 0x7fffffff

    const/16 v2, 0x400

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/io/iobuffer/b;

    invoke-direct {v0}, Lcom/google/io/iobuffer/b;-><init>()V

    iput-object v0, p0, Lcom/google/net/async/Connection;->e:Lcom/google/io/iobuffer/b;

    new-instance v0, Lcom/google/io/iobuffer/b;

    invoke-direct {v0}, Lcom/google/io/iobuffer/b;-><init>()V

    iput-object v0, p0, Lcom/google/net/async/Connection;->f:Lcom/google/io/iobuffer/b;

    iput-boolean v1, p0, Lcom/google/net/async/Connection;->i:Z

    iput v3, p0, Lcom/google/net/async/Connection;->k:I

    iput v3, p0, Lcom/google/net/async/Connection;->l:I

    iput v1, p0, Lcom/google/net/async/Connection;->m:I

    iput-boolean v1, p0, Lcom/google/net/async/Connection;->n:Z

    if-nez p4, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "mode cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "channel cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez p2, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "eventRegistry cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    if-nez p3, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "callback cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    invoke-virtual {p1}, Lcom/google/net/async/aj;->isBlocking()Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Ljava/nio/channels/IllegalBlockingModeException;

    invoke-direct {v0}, Ljava/nio/channels/IllegalBlockingModeException;-><init>()V

    throw v0

    :cond_4
    invoke-virtual {p1}, Lcom/google/net/async/aj;->c()Z

    move-result v0

    if-nez v0, :cond_5

    new-instance v0, Ljava/nio/channels/NotYetConnectedException;

    invoke-direct {v0}, Ljava/nio/channels/NotYetConnectedException;-><init>()V

    throw v0

    :cond_5
    iput-object p1, p0, Lcom/google/net/async/Connection;->b:Lcom/google/net/async/aj;

    iput-object p2, p0, Lcom/google/net/async/Connection;->c:Lcom/google/net/async/u;

    iput-object p3, p0, Lcom/google/net/async/Connection;->d:Lcom/google/net/async/f;

    new-instance v0, Lcom/google/io/iobuffer/c;

    iget-object v1, p0, Lcom/google/net/async/Connection;->e:Lcom/google/io/iobuffer/b;

    invoke-direct {v0, v1}, Lcom/google/io/iobuffer/c;-><init>(Lcom/google/io/iobuffer/b;)V

    iput-object v0, p0, Lcom/google/net/async/Connection;->g:Ljava/io/InputStream;

    new-instance v0, Lcom/google/io/iobuffer/IOBufferOutputStream;

    iget-object v1, p0, Lcom/google/net/async/Connection;->f:Lcom/google/io/iobuffer/b;

    invoke-direct {v0, v1}, Lcom/google/io/iobuffer/IOBufferOutputStream;-><init>(Lcom/google/io/iobuffer/b;)V

    iput-object v0, p0, Lcom/google/net/async/Connection;->h:Ljava/io/OutputStream;

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/net/async/Connection;->j:Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lcom/google/net/async/Connection;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    :try_start_0
    iget-object v0, p0, Lcom/google/net/async/Connection;->b:Lcom/google/net/async/aj;

    invoke-virtual {v0}, Lcom/google/net/async/aj;->b()Ljava/net/Socket;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/net/Socket;->setTcpNoDelay(Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/google/net/async/Connection;->d:Lcom/google/net/async/f;

    iget-object v2, p0, Lcom/google/net/async/Connection;->c:Lcom/google/net/async/u;

    invoke-static {v1, v0, v2}, Lcom/google/net/async/g;->a(Lcom/google/net/async/f;Ljava/lang/Exception;Lcom/google/net/async/u;)V

    goto :goto_0
.end method

.method private a(Ljava/nio/ByteBuffer;I)I
    .locals 2

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    if-gt v0, p2, :cond_0

    iget-object v0, p0, Lcom/google/net/async/Connection;->b:Lcom/google/net/async/aj;

    invoke-virtual {v0, p1}, Lcom/google/net/async/aj;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v1

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    add-int/2addr v0, p2

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    :try_start_0
    iget-object v0, p0, Lcom/google/net/async/Connection;->b:Lcom/google/net/async/aj;

    invoke-virtual {v0, p1}, Lcom/google/net/async/aj;->a(Ljava/nio/ByteBuffer;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    throw v0
.end method

.method private static a(Ljava/nio/channels/ReadableByteChannel;Ljava/nio/ByteBuffer;I)I
    .locals 2

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    if-gt v0, p2, :cond_0

    invoke-interface {p0, p1}, Ljava/nio/channels/ReadableByteChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v1

    :try_start_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    add-int/2addr v0, p2

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    invoke-interface {p0, p1}, Ljava/nio/channels/ReadableByteChannel;->read(Ljava/nio/ByteBuffer;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    throw v0
.end method

.method public static a(Ljava/lang/String;)V
    .locals 0

    return-void
.end method


# virtual methods
.method public final a()Ljava/io/InputStream;
    .locals 1

    iget-object v0, p0, Lcom/google/net/async/Connection;->g:Ljava/io/InputStream;

    return-object v0
.end method

.method public final a(I)V
    .locals 2

    if-gtz p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "maxNumByte must be positive"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput p1, p0, Lcom/google/net/async/Connection;->k:I

    return-void
.end method

.method public final b()Ljava/io/OutputStream;
    .locals 1

    iget-object v0, p0, Lcom/google/net/async/Connection;->h:Ljava/io/OutputStream;

    return-object v0
.end method

.method public final b(I)V
    .locals 3

    if-lez p1, :cond_0

    iput p1, p0, Lcom/google/net/async/Connection;->l:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/net/async/Connection;->n:Z

    :goto_0
    return-void

    :cond_0
    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/net/async/Connection;->l:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/net/async/Connection;->n:Z

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid maxNumByte "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/net/async/Connection;->c:Lcom/google/net/async/u;

    iget-object v1, p0, Lcom/google/net/async/Connection;->b:Lcom/google/net/async/aj;

    invoke-interface {v0, v1, p0}, Lcom/google/net/async/u;->a(Ljava/nio/channels/SelectableChannel;Lcom/google/net/async/af;)V

    return-void
.end method

.method public final d()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/net/async/Connection;->f:Lcom/google/io/iobuffer/b;

    invoke-virtual {v0}, Lcom/google/io/iobuffer/b;->b()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/net/async/Connection;->i:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/net/async/Connection;->c:Lcom/google/net/async/u;

    iget-object v1, p0, Lcom/google/net/async/Connection;->b:Lcom/google/net/async/aj;

    invoke-interface {v0, v1, p0}, Lcom/google/net/async/u;->a(Ljava/nio/channels/SelectableChannel;Lcom/google/net/async/an;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/net/async/Connection;->i:Z

    :cond_0
    monitor-exit p0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/google/net/async/Connection;->d:Lcom/google/net/async/f;

    iget-object v2, p0, Lcom/google/net/async/Connection;->c:Lcom/google/net/async/u;

    invoke-static {v1, v0, v2}, Lcom/google/net/async/g;->a(Lcom/google/net/async/f;Ljava/lang/Exception;Lcom/google/net/async/u;)V

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final e()V
    .locals 1

    iget-object v0, p0, Lcom/google/net/async/Connection;->b:Lcom/google/net/async/aj;

    invoke-virtual {v0}, Lcom/google/net/async/aj;->close()V

    return-void
.end method

.method public final f()V
    .locals 8

    const/4 v0, -0x1

    :try_start_0
    iget-object v4, p0, Lcom/google/net/async/Connection;->b:Lcom/google/net/async/aj;

    iget-object v5, p0, Lcom/google/net/async/Connection;->e:Lcom/google/io/iobuffer/b;

    iget v3, p0, Lcom/google/net/async/Connection;->k:I

    sget-boolean v1, Lcom/google/net/async/Connection;->a:Z

    if-nez v1, :cond_0

    if-gez v3, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/google/net/async/Connection;->d:Lcom/google/net/async/f;

    invoke-interface {v1, v0}, Lcom/google/net/async/f;->a(Ljava/lang/Exception;)V

    :goto_0
    return-void

    :cond_0
    :try_start_1
    invoke-interface {v4}, Ljava/nio/channels/ReadableByteChannel;->isOpen()Z

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    :goto_1
    if-gez v0, :cond_a

    iget-object v0, p0, Lcom/google/net/async/Connection;->c:Lcom/google/net/async/u;

    iget-object v1, p0, Lcom/google/net/async/Connection;->b:Lcom/google/net/async/aj;

    invoke-interface {v0, v1}, Lcom/google/net/async/u;->b(Ljava/nio/channels/SelectableChannel;)V

    iget-object v0, p0, Lcom/google/net/async/Connection;->d:Lcom/google/net/async/f;

    invoke-interface {v0}, Lcom/google/net/async/f;->a()V

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    move v2, v3

    :cond_3
    invoke-virtual {v5}, Lcom/google/io/iobuffer/b;->e()Ljava/nio/ByteBuffer;

    move-result-object v6

    invoke-static {v4, v6, v2}, Lcom/google/net/async/Connection;->a(Ljava/nio/channels/ReadableByteChannel;Ljava/nio/ByteBuffer;I)I

    move-result v6

    sget-boolean v7, Lcom/google/net/async/Connection;->a:Z

    if-nez v7, :cond_4

    if-le v6, v2, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_4
    if-lez v6, :cond_9

    sub-int/2addr v2, v6

    :cond_5
    :goto_2
    if-lez v2, :cond_6

    if-gtz v6, :cond_3

    :cond_6
    if-eqz v1, :cond_7

    if-eq v2, v3, :cond_1

    :cond_7
    if-ge v2, v3, :cond_8

    invoke-virtual {v5}, Lcom/google/io/iobuffer/b;->b()V

    :cond_8
    sub-int v0, v3, v2

    goto :goto_1

    :cond_9
    if-gez v6, :cond_5

    const/4 v1, 0x1

    goto :goto_2

    :cond_a
    iget-object v1, p0, Lcom/google/net/async/Connection;->d:Lcom/google/net/async/f;

    invoke-interface {v1, v0}, Lcom/google/net/async/f;->a(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public final g()V
    .locals 6

    const/4 v0, 0x0

    :try_start_0
    sget-boolean v1, Lcom/google/net/async/Connection;->a:Z

    if-nez v1, :cond_1

    iget v1, p0, Lcom/google/net/async/Connection;->l:I

    if-gez v1, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/google/net/async/Connection;->d:Lcom/google/net/async/f;

    invoke-interface {v1, v0}, Lcom/google/net/async/f;->a(Ljava/lang/Exception;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_1
    iget v3, p0, Lcom/google/net/async/Connection;->l:I

    iget-object v1, p0, Lcom/google/net/async/Connection;->b:Lcom/google/net/async/aj;

    invoke-virtual {v1}, Lcom/google/net/async/aj;->isOpen()Z

    move-result v1

    if-nez v1, :cond_2

    move v1, v0

    :goto_1
    if-gez v1, :cond_9

    new-instance v0, Lcom/google/net/async/IORuntimeException;

    const-string v1, "-1 returned when writing to channel"

    invoke-direct {v0, v1}, Lcom/google/net/async/IORuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move v2, v3

    :cond_3
    iget-object v1, p0, Lcom/google/net/async/Connection;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/net/async/Connection;->j:Ljava/nio/ByteBuffer;

    invoke-direct {p0, v1, v2}, Lcom/google/net/async/Connection;->a(Ljava/nio/ByteBuffer;I)I

    move-result v1

    :goto_2
    sub-int/2addr v2, v1

    if-lez v2, :cond_4

    if-gtz v1, :cond_3

    :cond_4
    sub-int v1, v3, v2

    goto :goto_1

    :cond_5
    iget-object v1, p0, Lcom/google/net/async/Connection;->f:Lcom/google/io/iobuffer/b;

    invoke-virtual {v1}, Lcom/google/io/iobuffer/b;->f()Ljava/nio/ByteBuffer;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v4

    if-nez v4, :cond_7

    :cond_6
    move v1, v0

    goto :goto_2

    :cond_7
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v4

    const/16 v5, 0x400

    if-le v4, v5, :cond_8

    invoke-direct {p0, v1, v2}, Lcom/google/net/async/Connection;->a(Ljava/nio/ByteBuffer;I)I

    move-result v1

    goto :goto_2

    :cond_8
    iget-object v1, p0, Lcom/google/net/async/Connection;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    iget-object v1, p0, Lcom/google/net/async/Connection;->f:Lcom/google/io/iobuffer/b;

    iget-object v4, p0, Lcom/google/net/async/Connection;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v4}, Lcom/google/io/iobuffer/b;->a(Ljava/nio/ByteBuffer;)I

    iget-object v1, p0, Lcom/google/net/async/Connection;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    iget-object v1, p0, Lcom/google/net/async/Connection;->j:Ljava/nio/ByteBuffer;

    invoke-direct {p0, v1, v2}, Lcom/google/net/async/Connection;->a(Ljava/nio/ByteBuffer;I)I

    move-result v1

    goto :goto_2

    :cond_9
    iget v2, p0, Lcom/google/net/async/Connection;->m:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/net/async/Connection;->m:I

    monitor-enter p0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :try_start_2
    iget-object v1, p0, Lcom/google/net/async/Connection;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v1

    if-nez v1, :cond_a

    iget-object v1, p0, Lcom/google/net/async/Connection;->f:Lcom/google/io/iobuffer/b;

    invoke-virtual {v1}, Lcom/google/io/iobuffer/b;->d()I

    move-result v1

    if-nez v1, :cond_a

    iget-object v0, p0, Lcom/google/net/async/Connection;->c:Lcom/google/net/async/u;

    iget-object v1, p0, Lcom/google/net/async/Connection;->b:Lcom/google/net/async/aj;

    invoke-interface {v0, v1}, Lcom/google/net/async/u;->c(Ljava/nio/channels/SelectableChannel;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/net/async/Connection;->i:Z

    const/4 v0, 0x1

    :cond_a
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    iget-boolean v1, p0, Lcom/google/net/async/Connection;->n:Z

    if-nez v1, :cond_b

    if-eqz v0, :cond_0

    :cond_b
    iget v1, p0, Lcom/google/net/async/Connection;->m:I

    const/4 v2, 0x0

    iput v2, p0, Lcom/google/net/async/Connection;->m:I

    iget-object v2, p0, Lcom/google/net/async/Connection;->d:Lcom/google/net/async/f;

    invoke-interface {v2, v1, v0}, Lcom/google/net/async/f;->a(IZ)V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "SocketChannel = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/google/net/async/Connection;->b:Lcom/google/net/async/aj;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "; Input buffer = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/google/net/async/Connection;->e:Lcom/google/io/iobuffer/b;

    invoke-virtual {v1}, Lcom/google/io/iobuffer/b;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "; Output buffer = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/google/net/async/Connection;->f:Lcom/google/io/iobuffer/b;

    invoke-virtual {v1}, Lcom/google/io/iobuffer/b;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "; Maximum size per read = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget v1, p0, Lcom/google/net/async/Connection;->k:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    const-string v1, "; Maximum size per flush = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget v1, p0, Lcom/google/net/async/Connection;->l:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
