.class final Lcom/google/net/async/z;
.super Ljava/lang/Thread;
.source "SourceFile"

# interfaces
.implements Lcom/google/net/async/ae;


# static fields
.field static final synthetic a:Z


# instance fields
.field private b:Lcom/google/net/async/p;

.field private c:Ljava/lang/RuntimeException;

.field private final d:Lcom/google/net/async/ab;

.field private e:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/net/async/x;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/net/async/z;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(ZLcom/google/net/async/ab;Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0, p3}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/net/async/z;->b:Lcom/google/net/async/p;

    iput-object v0, p0, Lcom/google/net/async/z;->c:Ljava/lang/RuntimeException;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/net/async/z;->e:J

    iput-object p2, p0, Lcom/google/net/async/z;->d:Lcom/google/net/async/ab;

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/net/async/z;->setDaemon(Z)V

    :cond_0
    new-instance v0, Lcom/google/net/async/aa;

    invoke-direct {v0, p0}, Lcom/google/net/async/aa;-><init>(Lcom/google/net/async/z;)V

    invoke-virtual {p0, v0}, Lcom/google/net/async/z;->setUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    return-void
.end method


# virtual methods
.method public final declared-synchronized a()Lcom/google/net/async/p;
    .locals 1

    monitor-enter p0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/net/async/z;->b:Lcom/google/net/async/p;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/net/async/z;->c:Ljava/lang/RuntimeException;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_0
    :try_start_2
    iget-object v0, p0, Lcom/google/net/async/z;->c:Ljava/lang/RuntimeException;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/net/async/z;->c:Ljava/lang/RuntimeException;

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_3
    sget-boolean v0, Lcom/google/net/async/z;->a:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/net/async/z;->b:Lcom/google/net/async/p;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_2
    iget-object v0, p0, Lcom/google/net/async/z;->b:Lcom/google/net/async/p;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    return-object v0
.end method

.method public final b()V
    .locals 2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/net/async/z;->e:J

    return-void
.end method

.method public final run()V
    .locals 4

    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :try_start_1
    new-instance v0, Lcom/google/net/async/p;

    invoke-direct {v0}, Lcom/google/net/async/p;-><init>()V

    iput-object v0, p0, Lcom/google/net/async/z;->b:Lcom/google/net/async/p;

    invoke-static {}, Lcom/google/net/async/x;->c()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    new-instance v0, Lcom/google/net/async/ad;

    iget-object v1, p0, Lcom/google/net/async/z;->b:Lcom/google/net/async/p;

    invoke-direct {v0, v1}, Lcom/google/net/async/ad;-><init>(Lcom/google/net/async/d;)V

    invoke-static {}, Lcom/google/net/async/x;->c()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2, p0}, Lcom/google/net/async/ad;->a(JLcom/google/net/async/ae;)V

    invoke-static {}, Lcom/google/net/async/x;->d()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :try_start_2
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    iget-object v0, p0, Lcom/google/net/async/z;->d:Lcom/google/net/async/ab;

    iget-object v1, p0, Lcom/google/net/async/z;->b:Lcom/google/net/async/p;

    invoke-interface {v0, v1}, Lcom/google/net/async/ab;->a(Lcom/google/net/async/p;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    invoke-static {}, Lcom/google/net/async/x;->d()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    :try_start_4
    iget-object v0, p0, Lcom/google/net/async/z;->b:Lcom/google/net/async/p;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/net/async/z;->b:Lcom/google/net/async/p;

    invoke-virtual {v0}, Lcom/google/net/async/p;->d()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_5
    iput-object v0, p0, Lcom/google/net/async/z;->c:Ljava/lang/RuntimeException;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    invoke-static {}, Lcom/google/net/async/x;->d()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    :try_start_7
    iget-object v0, p0, Lcom/google/net/async/z;->b:Lcom/google/net/async/p;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/net/async/z;->b:Lcom/google/net/async/p;

    invoke-virtual {v0}, Lcom/google/net/async/p;->d()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_8
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :catchall_1
    move-exception v0

    :try_start_9
    monitor-exit p0

    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    :catchall_2
    move-exception v0

    invoke-static {}, Lcom/google/net/async/x;->d()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1, p0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    :try_start_a
    iget-object v1, p0, Lcom/google/net/async/z;->b:Lcom/google/net/async/p;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/net/async/z;->b:Lcom/google/net/async/p;

    invoke-virtual {v1}, Lcom/google/net/async/p;->d()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_2

    :cond_2
    :goto_1
    throw v0

    :catch_2
    move-exception v1

    goto :goto_1

    :catch_3
    move-exception v0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Thread["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/net/async/z;->getId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/net/async/z;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
