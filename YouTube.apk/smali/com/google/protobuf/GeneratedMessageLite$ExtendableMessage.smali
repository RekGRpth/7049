.class public abstract Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SourceFile"

# interfaces
.implements Lcom/google/protobuf/q;


# instance fields
.field private final extensions:Lcom/google/protobuf/j;


# direct methods
.method protected constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    invoke-static {}, Lcom/google/protobuf/j;->a()Lcom/google/protobuf/j;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;->extensions:Lcom/google/protobuf/j;

    return-void
.end method

.method protected constructor <init>(Lcom/google/protobuf/o;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    invoke-static {p1}, Lcom/google/protobuf/o;->a(Lcom/google/protobuf/o;)Lcom/google/protobuf/j;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;->extensions:Lcom/google/protobuf/j;

    return-void
.end method

.method static synthetic access$200(Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;)Lcom/google/protobuf/j;
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;->extensions:Lcom/google/protobuf/j;

    return-object v0
.end method

.method private verifyExtensionContainingType(Lcom/google/protobuf/s;)V
    .locals 2

    iget-object v0, p1, Lcom/google/protobuf/s;->a:Lcom/google/protobuf/ad;

    invoke-virtual {p0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;->f()Lcom/google/protobuf/ad;

    move-result-object v1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "This extension is for a different message type.  Please make sure that you are not suppressing any generics type warnings."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method


# virtual methods
.method protected extensionsAreInitialized()Z
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;->extensions:Lcom/google/protobuf/j;

    invoke-virtual {v0}, Lcom/google/protobuf/j;->f()Z

    move-result v0

    return v0
.end method

.method protected extensionsSerializedSize()I
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;->extensions:Lcom/google/protobuf/j;

    invoke-virtual {v0}, Lcom/google/protobuf/j;->g()I

    move-result v0

    return v0
.end method

.method protected extensionsSerializedSizeAsMessageSet()I
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;->extensions:Lcom/google/protobuf/j;

    invoke-virtual {v0}, Lcom/google/protobuf/j;->h()I

    move-result v0

    return v0
.end method

.method public final getExtension(Lcom/google/protobuf/s;)Ljava/lang/Object;
    .locals 3

    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;->verifyExtensionContainingType(Lcom/google/protobuf/s;)V

    iget-object v0, p0, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;->extensions:Lcom/google/protobuf/j;

    iget-object v1, p1, Lcom/google/protobuf/s;->d:Lcom/google/protobuf/r;

    invoke-virtual {v0, v1}, Lcom/google/protobuf/j;->b(Lcom/google/protobuf/l;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p1, Lcom/google/protobuf/s;->b:Ljava/lang/Object;

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p1, Lcom/google/protobuf/s;->d:Lcom/google/protobuf/r;

    invoke-virtual {v1}, Lcom/google/protobuf/r;->d()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p1, Lcom/google/protobuf/s;->d:Lcom/google/protobuf/r;

    invoke-virtual {v1}, Lcom/google/protobuf/r;->c()Lcom/google/protobuf/WireFormat$JavaType;

    move-result-object v1

    sget-object v2, Lcom/google/protobuf/WireFormat$JavaType;->ENUM:Lcom/google/protobuf/WireFormat$JavaType;

    if-ne v1, v2, :cond_0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/google/protobuf/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0

    :cond_3
    invoke-virtual {p1, v0}, Lcom/google/protobuf/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final getExtension(Lcom/google/protobuf/s;I)Ljava/lang/Object;
    .locals 2

    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;->verifyExtensionContainingType(Lcom/google/protobuf/s;)V

    iget-object v0, p0, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;->extensions:Lcom/google/protobuf/j;

    iget-object v1, p1, Lcom/google/protobuf/s;->d:Lcom/google/protobuf/r;

    invoke-virtual {v0, v1, p2}, Lcom/google/protobuf/j;->a(Lcom/google/protobuf/l;I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getExtensionCount(Lcom/google/protobuf/s;)I
    .locals 2

    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;->verifyExtensionContainingType(Lcom/google/protobuf/s;)V

    iget-object v0, p0, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;->extensions:Lcom/google/protobuf/j;

    iget-object v1, p1, Lcom/google/protobuf/s;->d:Lcom/google/protobuf/r;

    invoke-virtual {v0, v1}, Lcom/google/protobuf/j;->c(Lcom/google/protobuf/l;)I

    move-result v0

    return v0
.end method

.method public final hasExtension(Lcom/google/protobuf/s;)Z
    .locals 2

    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;->verifyExtensionContainingType(Lcom/google/protobuf/s;)V

    iget-object v0, p0, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;->extensions:Lcom/google/protobuf/j;

    iget-object v1, p1, Lcom/google/protobuf/s;->d:Lcom/google/protobuf/r;

    invoke-virtual {v0, v1}, Lcom/google/protobuf/j;->a(Lcom/google/protobuf/l;)Z

    move-result v0

    return v0
.end method

.method protected makeExtensionsImmutable()V
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;->extensions:Lcom/google/protobuf/j;

    invoke-virtual {v0}, Lcom/google/protobuf/j;->c()V

    return-void
.end method

.method public mutableCopy()Lcom/google/protobuf/ag;
    .locals 2

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->mutableCopy()Lcom/google/protobuf/ag;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/u;

    iget-object v1, p0, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;->extensions:Lcom/google/protobuf/j;

    invoke-virtual {v1}, Lcom/google/protobuf/j;->d()Lcom/google/protobuf/j;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protobuf/u;->a(Lcom/google/protobuf/j;)V

    return-object v0
.end method

.method protected newExtensionWriter()Lcom/google/protobuf/p;
    .locals 2

    const/4 v1, 0x0

    new-instance v0, Lcom/google/protobuf/p;

    invoke-direct {v0, p0, v1, v1}, Lcom/google/protobuf/p;-><init>(Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;ZB)V

    return-object v0
.end method

.method protected newMessageSetExtensionWriter()Lcom/google/protobuf/p;
    .locals 3

    new-instance v0, Lcom/google/protobuf/p;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/google/protobuf/p;-><init>(Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;ZB)V

    return-object v0
.end method

.method protected parseUnknownField(Lcom/google/protobuf/g;Lcom/google/protobuf/h;I)Z
    .locals 2

    iget-object v0, p0, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;->extensions:Lcom/google/protobuf/j;

    invoke-virtual {p0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;->f()Lcom/google/protobuf/ad;

    move-result-object v1

    # invokes: Lcom/google/protobuf/GeneratedMessageLite;->parseUnknownField(Lcom/google/protobuf/j;Lcom/google/protobuf/ad;Lcom/google/protobuf/g;Lcom/google/protobuf/h;I)Z
    invoke-static {v0, v1, p1, p2, p3}, Lcom/google/protobuf/GeneratedMessageLite;->access$100(Lcom/google/protobuf/j;Lcom/google/protobuf/ad;Lcom/google/protobuf/g;Lcom/google/protobuf/h;I)Z

    move-result v0

    return v0
.end method
