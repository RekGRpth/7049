.class final Lcom/google/protobuf/ak;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/protobuf/e;


# instance fields
.field a:I

.field final synthetic b:Lcom/google/protobuf/ai;

.field private final c:Lcom/google/protobuf/aj;

.field private d:Lcom/google/protobuf/e;


# direct methods
.method private constructor <init>(Lcom/google/protobuf/ai;)V
    .locals 2

    iput-object p1, p0, Lcom/google/protobuf/ak;->b:Lcom/google/protobuf/ai;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/protobuf/aj;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/google/protobuf/aj;-><init>(Lcom/google/protobuf/d;B)V

    iput-object v0, p0, Lcom/google/protobuf/ak;->c:Lcom/google/protobuf/aj;

    iget-object v0, p0, Lcom/google/protobuf/ak;->c:Lcom/google/protobuf/aj;

    invoke-virtual {v0}, Lcom/google/protobuf/aj;->a()Lcom/google/protobuf/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/ab;->a()Lcom/google/protobuf/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/ak;->d:Lcom/google/protobuf/e;

    invoke-virtual {p1}, Lcom/google/protobuf/ai;->b()I

    move-result v0

    iput v0, p0, Lcom/google/protobuf/ak;->a:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/ai;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/protobuf/ak;-><init>(Lcom/google/protobuf/ai;)V

    return-void
.end method


# virtual methods
.method public final a()B
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/ak;->d:Lcom/google/protobuf/e;

    invoke-interface {v0}, Lcom/google/protobuf/e;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/protobuf/ak;->c:Lcom/google/protobuf/aj;

    invoke-virtual {v0}, Lcom/google/protobuf/aj;->a()Lcom/google/protobuf/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/ab;->a()Lcom/google/protobuf/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/ak;->d:Lcom/google/protobuf/e;

    :cond_0
    iget v0, p0, Lcom/google/protobuf/ak;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/protobuf/ak;->a:I

    iget-object v0, p0, Lcom/google/protobuf/ak;->d:Lcom/google/protobuf/e;

    invoke-interface {v0}, Lcom/google/protobuf/e;->a()B

    move-result v0

    return v0
.end method

.method public final hasNext()Z
    .locals 1

    iget v0, p0, Lcom/google/protobuf/ak;->a:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic next()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/ak;->a()B

    move-result v0

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    return-object v0
.end method

.method public final remove()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
