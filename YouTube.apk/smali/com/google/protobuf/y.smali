.class final Lcom/google/protobuf/y;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/protobuf/ad;

.field private final b:Lcom/google/protobuf/h;

.field private c:Lcom/google/protobuf/d;

.field private volatile d:Lcom/google/protobuf/ad;

.field private volatile e:Z


# direct methods
.method private c()V
    .locals 2

    iget-object v0, p0, Lcom/google/protobuf/y;->d:Lcom/google/protobuf/ad;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/protobuf/y;->d:Lcom/google/protobuf/ad;

    if-eqz v0, :cond_1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/protobuf/y;->c:Lcom/google/protobuf/d;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/protobuf/y;->a:Lcom/google/protobuf/ad;

    invoke-interface {v0}, Lcom/google/protobuf/ad;->getParserForType()Lcom/google/protobuf/ah;

    move-result-object v0

    iget-object v1, p0, Lcom/google/protobuf/y;->c:Lcom/google/protobuf/d;

    iget-object v1, p0, Lcom/google/protobuf/y;->b:Lcom/google/protobuf/h;

    invoke-interface {v0}, Lcom/google/protobuf/ah;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/ad;

    iput-object v0, p0, Lcom/google/protobuf/y;->d:Lcom/google/protobuf/ad;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_2
    :goto_1
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method public final a()Lcom/google/protobuf/ad;
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/y;->c()V

    iget-object v0, p0, Lcom/google/protobuf/y;->d:Lcom/google/protobuf/ad;

    return-object v0
.end method

.method public final a(Lcom/google/protobuf/ad;)Lcom/google/protobuf/ad;
    .locals 2

    iget-object v0, p0, Lcom/google/protobuf/y;->d:Lcom/google/protobuf/ad;

    iput-object p1, p0, Lcom/google/protobuf/y;->d:Lcom/google/protobuf/ad;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/protobuf/y;->c:Lcom/google/protobuf/d;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/protobuf/y;->e:Z

    return-object v0
.end method

.method public final b()I
    .locals 1

    iget-boolean v0, p0, Lcom/google/protobuf/y;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/protobuf/y;->d:Lcom/google/protobuf/ad;

    invoke-interface {v0}, Lcom/google/protobuf/ad;->a()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/protobuf/y;->c:Lcom/google/protobuf/d;

    invoke-virtual {v0}, Lcom/google/protobuf/d;->b()I

    move-result v0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/y;->c()V

    iget-object v0, p0, Lcom/google/protobuf/y;->d:Lcom/google/protobuf/ad;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final hashCode()I
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/y;->c()V

    iget-object v0, p0, Lcom/google/protobuf/y;->d:Lcom/google/protobuf/ad;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/y;->c()V

    iget-object v0, p0, Lcom/google/protobuf/y;->d:Lcom/google/protobuf/ad;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
