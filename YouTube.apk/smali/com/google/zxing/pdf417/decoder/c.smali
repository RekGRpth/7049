.class public final Lcom/google/zxing/pdf417/decoder/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/zxing/pdf417/decoder/a/a;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/zxing/pdf417/decoder/a/a;

    invoke-direct {v0}, Lcom/google/zxing/pdf417/decoder/a/a;-><init>()V

    iput-object v0, p0, Lcom/google/zxing/pdf417/decoder/c;->a:Lcom/google/zxing/pdf417/decoder/a/a;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/zxing/common/b;)Lcom/google/zxing/common/d;
    .locals 6

    const/4 v5, 0x0

    new-instance v0, Lcom/google/zxing/pdf417/decoder/a;

    invoke-direct {v0, p1}, Lcom/google/zxing/pdf417/decoder/a;-><init>(Lcom/google/zxing/common/b;)V

    invoke-virtual {v0}, Lcom/google/zxing/pdf417/decoder/a;->a()[I

    move-result-object v1

    array-length v2, v1

    if-nez v2, :cond_0

    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {v0}, Lcom/google/zxing/pdf417/decoder/a;->c()I

    move-result v2

    const/4 v3, 0x1

    add-int/lit8 v2, v2, 0x1

    shl-int v2, v3, v2

    invoke-virtual {v0}, Lcom/google/zxing/pdf417/decoder/a;->b()[I

    move-result-object v0

    array-length v3, v0

    div-int/lit8 v4, v2, 0x2

    add-int/lit8 v4, v4, 0x3

    if-gt v3, v4, :cond_1

    if-ltz v2, :cond_1

    const/16 v3, 0x200

    if-le v2, v3, :cond_2

    :cond_1
    invoke-static {}, Lcom/google/zxing/ChecksumException;->getChecksumInstance()Lcom/google/zxing/ChecksumException;

    move-result-object v0

    throw v0

    :cond_2
    iget-object v3, p0, Lcom/google/zxing/pdf417/decoder/c;->a:Lcom/google/zxing/pdf417/decoder/a/a;

    invoke-virtual {v3, v1, v2, v0}, Lcom/google/zxing/pdf417/decoder/a/a;->a([II[I)V

    array-length v0, v1

    const/4 v3, 0x4

    if-ge v0, v3, :cond_3

    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    :cond_3
    aget v0, v1, v5

    array-length v3, v1

    if-le v0, v3, :cond_4

    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    :cond_4
    if-nez v0, :cond_5

    array-length v0, v1

    if-ge v2, v0, :cond_6

    array-length v0, v1

    sub-int/2addr v0, v2

    aput v0, v1, v5

    :cond_5
    invoke-static {v1}, Lcom/google/zxing/pdf417/decoder/DecodedBitStreamParser;->a([I)Lcom/google/zxing/common/d;

    move-result-object v0

    return-object v0

    :cond_6
    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0
.end method
