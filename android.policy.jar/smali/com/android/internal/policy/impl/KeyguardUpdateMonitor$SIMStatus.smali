.class Lcom/android/internal/policy/impl/KeyguardUpdateMonitor$SIMStatus;
.super Ljava/lang/Object;
.source "KeyguardUpdateMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/KeyguardUpdateMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SIMStatus"
.end annotation


# instance fields
.field private mDialogType:I

.field private mTotal:I

.field final synthetic this$0:Lcom/android/internal/policy/impl/KeyguardUpdateMonitor;


# direct methods
.method public constructor <init>(Lcom/android/internal/policy/impl/KeyguardUpdateMonitor;II)V
    .locals 1
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/android/internal/policy/impl/KeyguardUpdateMonitor$SIMStatus;->this$0:Lcom/android/internal/policy/impl/KeyguardUpdateMonitor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/android/internal/policy/impl/KeyguardUpdateMonitor$SIMStatus;->mTotal:I

    iput v0, p0, Lcom/android/internal/policy/impl/KeyguardUpdateMonitor$SIMStatus;->mDialogType:I

    iput p2, p0, Lcom/android/internal/policy/impl/KeyguardUpdateMonitor$SIMStatus;->mDialogType:I

    iput p3, p0, Lcom/android/internal/policy/impl/KeyguardUpdateMonitor$SIMStatus;->mTotal:I

    return-void
.end method


# virtual methods
.method public getSIMCardCount()I
    .locals 1

    iget v0, p0, Lcom/android/internal/policy/impl/KeyguardUpdateMonitor$SIMStatus;->mTotal:I

    return v0
.end method

.method public getSimType()I
    .locals 1

    iget v0, p0, Lcom/android/internal/policy/impl/KeyguardUpdateMonitor$SIMStatus;->mDialogType:I

    return v0
.end method
