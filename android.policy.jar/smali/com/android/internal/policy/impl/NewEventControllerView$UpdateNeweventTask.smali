.class Lcom/android/internal/policy/impl/NewEventControllerView$UpdateNeweventTask;
.super Landroid/os/AsyncTask;
.source "NewEventControllerView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/NewEventControllerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UpdateNeweventTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Landroid/content/ContentValues;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field mContext:Landroid/content/Context;

.field mHandler:Landroid/os/Handler;

.field mMisscallCount:I

.field mMmsCount:I

.field final synthetic this$0:Lcom/android/internal/policy/impl/NewEventControllerView;


# direct methods
.method public constructor <init>(Lcom/android/internal/policy/impl/NewEventControllerView;Landroid/content/Context;Landroid/os/Handler;)V
    .locals 0
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/os/Handler;

    iput-object p1, p0, Lcom/android/internal/policy/impl/NewEventControllerView$UpdateNeweventTask;->this$0:Lcom/android/internal/policy/impl/NewEventControllerView;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p2, p0, Lcom/android/internal/policy/impl/NewEventControllerView$UpdateNeweventTask;->mContext:Landroid/content/Context;

    iput-object p3, p0, Lcom/android/internal/policy/impl/NewEventControllerView$UpdateNeweventTask;->mHandler:Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Landroid/content/ContentValues;

    invoke-virtual {p0, p1}, Lcom/android/internal/policy/impl/NewEventControllerView$UpdateNeweventTask;->doInBackground([Landroid/content/ContentValues;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Landroid/content/ContentValues;)Ljava/lang/Void;
    .locals 3
    .param p1    # [Landroid/content/ContentValues;

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/internal/policy/impl/NewEventControllerView$UpdateNeweventTask;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "com_android_mms_mtk_unread"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/internal/policy/impl/NewEventControllerView$UpdateNeweventTask;->mMmsCount:I

    iget-object v0, p0, Lcom/android/internal/policy/impl/NewEventControllerView$UpdateNeweventTask;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "com_android_contacts_mtk_unread"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/internal/policy/impl/NewEventControllerView$UpdateNeweventTask;->mMisscallCount:I

    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/internal/policy/impl/NewEventControllerView$UpdateNeweventTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 4
    .param p1    # Ljava/lang/Void;

    iget-object v2, p0, Lcom/android/internal/policy/impl/NewEventControllerView$UpdateNeweventTask;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x3e9

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    new-instance v1, Lcom/android/internal/policy/impl/NewEventControllerView$NewEventInfo;

    iget-object v2, p0, Lcom/android/internal/policy/impl/NewEventControllerView$UpdateNeweventTask;->this$0:Lcom/android/internal/policy/impl/NewEventControllerView;

    invoke-direct {v1, v2}, Lcom/android/internal/policy/impl/NewEventControllerView$NewEventInfo;-><init>(Lcom/android/internal/policy/impl/NewEventControllerView;)V

    iget v2, p0, Lcom/android/internal/policy/impl/NewEventControllerView$UpdateNeweventTask;->mMmsCount:I

    iput v2, v1, Lcom/android/internal/policy/impl/NewEventControllerView$NewEventInfo;->mmsCount:I

    iget v2, p0, Lcom/android/internal/policy/impl/NewEventControllerView$UpdateNeweventTask;->mMisscallCount:I

    iput v2, v1, Lcom/android/internal/policy/impl/NewEventControllerView$NewEventInfo;->misscallCount:I

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v2, p0, Lcom/android/internal/policy/impl/NewEventControllerView$UpdateNeweventTask;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    iget-object v0, p0, Lcom/android/internal/policy/impl/NewEventControllerView$UpdateNeweventTask;->this$0:Lcom/android/internal/policy/impl/NewEventControllerView;

    const/4 v1, 0x0

    # setter for: Lcom/android/internal/policy/impl/NewEventControllerView;->mUpdateScheduled:Z
    invoke-static {v0, v1}, Lcom/android/internal/policy/impl/NewEventControllerView;->access$002(Lcom/android/internal/policy/impl/NewEventControllerView;Z)Z

    return-void
.end method
