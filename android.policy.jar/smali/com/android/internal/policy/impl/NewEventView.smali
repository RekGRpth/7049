.class public Lcom/android/internal/policy/impl/NewEventView;
.super Landroid/widget/TextView;
.source "NewEventView.java"


# static fields
.field private static final DEBUG:Z = true

.field private static final DRAWABLE_PADDING:I = 0xa

.field private static final MAX_COUNT:I = 0x63

.field private static final MORE_STRING:Ljava/lang/String; = "+"

.field private static final TAG:Ljava/lang/String; = "NewEventView"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mEventChangeObserver:Landroid/database/ContentObserver;

.field private mNewEventController:Lcom/android/internal/policy/impl/NewEventControllerView;

.field private mNumber:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/internal/policy/impl/NewEventView;->mNumber:I

    new-instance v0, Lcom/android/internal/policy/impl/NewEventView$1;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/android/internal/policy/impl/NewEventView$1;-><init>(Lcom/android/internal/policy/impl/NewEventView;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/internal/policy/impl/NewEventView;->mEventChangeObserver:Landroid/database/ContentObserver;

    iput-object p1, p0, Lcom/android/internal/policy/impl/NewEventView;->mContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/policy/impl/NewEventView;)Lcom/android/internal/policy/impl/NewEventControllerView;
    .locals 1
    .param p0    # Lcom/android/internal/policy/impl/NewEventView;

    iget-object v0, p0, Lcom/android/internal/policy/impl/NewEventView;->mNewEventController:Lcom/android/internal/policy/impl/NewEventControllerView;

    return-object v0
.end method


# virtual methods
.method public Init(I)V
    .locals 4
    .param p1    # I

    const/4 v3, 0x0

    const-string v1, "NewEventView"

    const-string v2, "Init"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/internal/policy/impl/NewEventView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0, v3, v3, v3}, Lcom/android/internal/policy/impl/NewEventView;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    const/16 v1, 0xa

    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/NewEventView;->setCompoundDrawablePadding(I)V

    const/16 v1, 0x50

    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/NewEventView;->setGravity(I)V

    return-void
.end method

.method public registerUpdateListener(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/internal/policy/impl/NewEventView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {p1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/android/internal/policy/impl/NewEventView;->mEventChangeObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    return-void
.end method

.method public setController(Lcom/android/internal/policy/impl/NewEventControllerView;)V
    .locals 0
    .param p1    # Lcom/android/internal/policy/impl/NewEventControllerView;

    iput-object p1, p0, Lcom/android/internal/policy/impl/NewEventView;->mNewEventController:Lcom/android/internal/policy/impl/NewEventControllerView;

    return-void
.end method

.method public setNumber(I)V
    .locals 4
    .param p1    # I

    const/16 v3, 0x63

    const-string v0, "NewEventView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setNumber count="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-gtz p1, :cond_0

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/NewEventView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iput p1, p0, Lcom/android/internal/policy/impl/NewEventView;->mNumber:I

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/NewEventView;->setVisibility(I)V

    if-le p1, v3, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/NewEventView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/NewEventView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public unRegisterUpdateListener()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/internal/policy/impl/NewEventView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/internal/policy/impl/NewEventView;->mEventChangeObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    return-void
.end method
