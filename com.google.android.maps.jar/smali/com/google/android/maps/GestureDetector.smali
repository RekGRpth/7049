.class Lcom/google/android/maps/GestureDetector;
.super Ljava/lang/Object;
.source "GestureDetector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/maps/GestureDetector$GestureHandler;,
        Lcom/google/android/maps/GestureDetector$SimpleOnGestureListener;,
        Lcom/google/android/maps/GestureDetector$OnDoubleTapListener;,
        Lcom/google/android/maps/GestureDetector$OnGestureListener;
    }
.end annotation


# static fields
.field private static final DOUBLE_TAP_TIMEOUT:I

.field private static final LONGPRESS_TIMEOUT:I

.field private static final TAP_TIMEOUT:I


# instance fields
.field private mAlwaysInBiggerTapRegion:Z

.field private mAlwaysInTapRegion:Z

.field private mCurrentDownEvent:Landroid/view/MotionEvent;

.field private mDoubleTapListener:Lcom/google/android/maps/GestureDetector$OnDoubleTapListener;

.field private mDoubleTapSlopSquare:I

.field private mDoubleTapTouchSlopSquare:I

.field private mDownFocusX:F

.field private mDownFocusY:F

.field private final mHandler:Landroid/os/Handler;

.field private mInLongPress:Z

.field private mIsDoubleTapping:Z

.field private mIsLongpressEnabled:Z

.field private mLastFocusX:F

.field private mLastFocusY:F

.field private final mListener:Lcom/google/android/maps/GestureDetector$OnGestureListener;

.field private mMaximumFlingVelocity:I

.field private mMinimumFlingVelocity:I

.field private mPreviousUpEvent:Landroid/view/MotionEvent;

.field private mStillDown:Z

.field private mTouchSlopSquare:I

.field private mVelocityTracker:Landroid/view/VelocityTracker;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v0

    sput v0, Lcom/google/android/maps/GestureDetector;->LONGPRESS_TIMEOUT:I

    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v0

    sput v0, Lcom/google/android/maps/GestureDetector;->TAP_TIMEOUT:I

    invoke-static {}, Landroid/view/ViewConfiguration;->getDoubleTapTimeout()I

    move-result v0

    sput v0, Lcom/google/android/maps/GestureDetector;->DOUBLE_TAP_TIMEOUT:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/maps/GestureDetector$OnGestureListener;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/maps/GestureDetector$OnGestureListener;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/maps/GestureDetector;-><init>(Landroid/content/Context;Lcom/google/android/maps/GestureDetector$OnGestureListener;Landroid/os/Handler;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/maps/GestureDetector$OnGestureListener;Landroid/os/Handler;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/maps/GestureDetector$OnGestureListener;
    .param p3    # Landroid/os/Handler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p3, :cond_1

    new-instance v0, Lcom/google/android/maps/GestureDetector$GestureHandler;

    invoke-direct {v0, p0, p3}, Lcom/google/android/maps/GestureDetector$GestureHandler;-><init>(Lcom/google/android/maps/GestureDetector;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/maps/GestureDetector;->mHandler:Landroid/os/Handler;

    :goto_0
    iput-object p2, p0, Lcom/google/android/maps/GestureDetector;->mListener:Lcom/google/android/maps/GestureDetector$OnGestureListener;

    instance-of v0, p2, Lcom/google/android/maps/GestureDetector$OnDoubleTapListener;

    if-eqz v0, :cond_0

    check-cast p2, Lcom/google/android/maps/GestureDetector$OnDoubleTapListener;

    invoke-virtual {p0, p2}, Lcom/google/android/maps/GestureDetector;->setOnDoubleTapListener(Lcom/google/android/maps/GestureDetector$OnDoubleTapListener;)V

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/maps/GestureDetector;->init(Landroid/content/Context;)V

    return-void

    :cond_1
    new-instance v0, Lcom/google/android/maps/GestureDetector$GestureHandler;

    invoke-direct {v0, p0}, Lcom/google/android/maps/GestureDetector$GestureHandler;-><init>(Lcom/google/android/maps/GestureDetector;)V

    iput-object v0, p0, Lcom/google/android/maps/GestureDetector;->mHandler:Landroid/os/Handler;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/maps/GestureDetector;)Landroid/view/MotionEvent;
    .locals 1
    .param p0    # Lcom/google/android/maps/GestureDetector;

    iget-object v0, p0, Lcom/google/android/maps/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/maps/GestureDetector;)Lcom/google/android/maps/GestureDetector$OnGestureListener;
    .locals 1
    .param p0    # Lcom/google/android/maps/GestureDetector;

    iget-object v0, p0, Lcom/google/android/maps/GestureDetector;->mListener:Lcom/google/android/maps/GestureDetector$OnGestureListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/maps/GestureDetector;)V
    .locals 0
    .param p0    # Lcom/google/android/maps/GestureDetector;

    invoke-direct {p0}, Lcom/google/android/maps/GestureDetector;->dispatchLongPress()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/maps/GestureDetector;)Lcom/google/android/maps/GestureDetector$OnDoubleTapListener;
    .locals 1
    .param p0    # Lcom/google/android/maps/GestureDetector;

    iget-object v0, p0, Lcom/google/android/maps/GestureDetector;->mDoubleTapListener:Lcom/google/android/maps/GestureDetector$OnDoubleTapListener;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/maps/GestureDetector;)Z
    .locals 1
    .param p0    # Lcom/google/android/maps/GestureDetector;

    iget-boolean v0, p0, Lcom/google/android/maps/GestureDetector;->mStillDown:Z

    return v0
.end method

.method private cancel()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/maps/GestureDetector;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/maps/GestureDetector;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/maps/GestureDetector;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/maps/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    iput-boolean v2, p0, Lcom/google/android/maps/GestureDetector;->mIsDoubleTapping:Z

    iput-boolean v2, p0, Lcom/google/android/maps/GestureDetector;->mStillDown:Z

    iput-boolean v2, p0, Lcom/google/android/maps/GestureDetector;->mAlwaysInTapRegion:Z

    iput-boolean v2, p0, Lcom/google/android/maps/GestureDetector;->mAlwaysInBiggerTapRegion:Z

    iget-boolean v0, p0, Lcom/google/android/maps/GestureDetector;->mInLongPress:Z

    if-eqz v0, :cond_0

    iput-boolean v2, p0, Lcom/google/android/maps/GestureDetector;->mInLongPress:Z

    :cond_0
    return-void
.end method

.method private cancelTaps()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/maps/GestureDetector;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/maps/GestureDetector;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/maps/GestureDetector;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iput-boolean v2, p0, Lcom/google/android/maps/GestureDetector;->mIsDoubleTapping:Z

    iput-boolean v2, p0, Lcom/google/android/maps/GestureDetector;->mAlwaysInTapRegion:Z

    iput-boolean v2, p0, Lcom/google/android/maps/GestureDetector;->mAlwaysInBiggerTapRegion:Z

    iget-boolean v0, p0, Lcom/google/android/maps/GestureDetector;->mInLongPress:Z

    if-eqz v0, :cond_0

    iput-boolean v2, p0, Lcom/google/android/maps/GestureDetector;->mInLongPress:Z

    :cond_0
    return-void
.end method

.method private dispatchLongPress()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/maps/GestureDetector;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/GestureDetector;->mInLongPress:Z

    iget-object v0, p0, Lcom/google/android/maps/GestureDetector;->mListener:Lcom/google/android/maps/GestureDetector$OnGestureListener;

    iget-object v1, p0, Lcom/google/android/maps/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    invoke-interface {v0, v1}, Lcom/google/android/maps/GestureDetector$OnGestureListener;->onLongPress(Landroid/view/MotionEvent;)V

    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 6
    .param p1    # Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/maps/GestureDetector;->mListener:Lcom/google/android/maps/GestureDetector$OnGestureListener;

    if-nez v4, :cond_0

    new-instance v4, Ljava/lang/NullPointerException;

    const-string v5, "OnGestureListener must not be null"

    invoke-direct {v4, v5}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_0
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/google/android/maps/GestureDetector;->mIsLongpressEnabled:Z

    if-nez p1, :cond_1

    invoke-static {}, Landroid/view/ViewConfiguration;->getTouchSlop()I

    move-result v3

    move v2, v3

    invoke-static {}, Landroid/view/ViewConfiguration;->getDoubleTapSlop()I

    move-result v1

    invoke-static {}, Landroid/view/ViewConfiguration;->getMinimumFlingVelocity()I

    move-result v4

    iput v4, p0, Lcom/google/android/maps/GestureDetector;->mMinimumFlingVelocity:I

    invoke-static {}, Landroid/view/ViewConfiguration;->getMaximumFlingVelocity()I

    move-result v4

    iput v4, p0, Lcom/google/android/maps/GestureDetector;->mMaximumFlingVelocity:I

    :goto_0
    mul-int v4, v3, v3

    iput v4, p0, Lcom/google/android/maps/GestureDetector;->mTouchSlopSquare:I

    mul-int v4, v2, v2

    iput v4, p0, Lcom/google/android/maps/GestureDetector;->mDoubleTapTouchSlopSquare:I

    mul-int v4, v1, v1

    iput v4, p0, Lcom/google/android/maps/GestureDetector;->mDoubleTapSlopSquare:I

    return-void

    :cond_1
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v3

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledDoubleTapTouchSlop()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledDoubleTapSlop()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v4

    iput v4, p0, Lcom/google/android/maps/GestureDetector;->mMinimumFlingVelocity:I

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v4

    iput v4, p0, Lcom/google/android/maps/GestureDetector;->mMaximumFlingVelocity:I

    goto :goto_0
.end method

.method private isConsideredDoubleTap(Landroid/view/MotionEvent;Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1    # Landroid/view/MotionEvent;
    .param p2    # Landroid/view/MotionEvent;
    .param p3    # Landroid/view/MotionEvent;

    const/4 v2, 0x0

    iget-boolean v3, p0, Lcom/google/android/maps/GestureDetector;->mAlwaysInBiggerTapRegion:Z

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v5

    sub-long/2addr v3, v5

    sget v5, Lcom/google/android/maps/GestureDetector;->DOUBLE_TAP_TIMEOUT:I

    int-to-long v5, v5

    cmp-long v3, v3, v5

    if-gtz v3, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p3}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v4, v4

    sub-int v0, v3, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    sub-int v1, v3, v4

    mul-int v3, v0, v0

    mul-int v4, v1, v1

    add-int/2addr v3, v4

    iget v4, p0, Lcom/google/android/maps/GestureDetector;->mDoubleTapSlopSquare:I

    if-ge v3, v4, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 32
    .param p1    # Landroid/view/MotionEvent;

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v26, v0

    if-nez v26, :cond_0

    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/maps/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    and-int/lit16 v0, v5, 0xff

    move/from16 v26, v0

    const/16 v27, 0x6

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_1

    const/16 v17, 0x1

    :goto_0
    if-eqz v17, :cond_2

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v20

    :goto_1
    const/16 v21, 0x0

    const/16 v22, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v6

    const/16 v16, 0x0

    :goto_2
    move/from16 v0, v16

    if-ge v0, v6, :cond_4

    move/from16 v0, v20

    move/from16 v1, v16

    if-ne v0, v1, :cond_3

    :goto_3
    add-int/lit8 v16, v16, 0x1

    goto :goto_2

    :cond_1
    const/16 v17, 0x0

    goto :goto_0

    :cond_2
    const/16 v20, -0x1

    goto :goto_1

    :cond_3
    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v26

    add-float v21, v21, v26

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v26

    add-float v22, v22, v26

    goto :goto_3

    :cond_4
    if-eqz v17, :cond_6

    add-int/lit8 v11, v6, -0x1

    :goto_4
    int-to-float v0, v11

    move/from16 v26, v0

    div-float v12, v21, v26

    int-to-float v0, v11

    move/from16 v26, v0

    div-float v13, v22, v26

    const/4 v15, 0x0

    and-int/lit16 v0, v5, 0xff

    move/from16 v26, v0

    packed-switch v26, :pswitch_data_0

    :cond_5
    :goto_5
    :pswitch_0
    return v15

    :cond_6
    move v11, v6

    goto :goto_4

    :pswitch_1
    move-object/from16 v0, p0

    iput v12, v0, Lcom/google/android/maps/GestureDetector;->mLastFocusX:F

    move-object/from16 v0, p0

    iput v12, v0, Lcom/google/android/maps/GestureDetector;->mDownFocusX:F

    move-object/from16 v0, p0

    iput v13, v0, Lcom/google/android/maps/GestureDetector;->mLastFocusY:F

    move-object/from16 v0, p0

    iput v13, v0, Lcom/google/android/maps/GestureDetector;->mDownFocusY:F

    invoke-direct/range {p0 .. p0}, Lcom/google/android/maps/GestureDetector;->cancelTaps()V

    goto :goto_5

    :pswitch_2
    move-object/from16 v0, p0

    iput v12, v0, Lcom/google/android/maps/GestureDetector;->mLastFocusX:F

    move-object/from16 v0, p0

    iput v12, v0, Lcom/google/android/maps/GestureDetector;->mDownFocusX:F

    move-object/from16 v0, p0

    iput v13, v0, Lcom/google/android/maps/GestureDetector;->mLastFocusY:F

    move-object/from16 v0, p0

    iput v13, v0, Lcom/google/android/maps/GestureDetector;->mDownFocusY:F

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/view/VelocityTracker;->clear()V

    goto :goto_5

    :pswitch_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mDoubleTapListener:Lcom/google/android/maps/GestureDetector$OnDoubleTapListener;

    move-object/from16 v26, v0

    if-eqz v26, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v26, v0

    const/16 v27, 0x3

    invoke-virtual/range {v26 .. v27}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v14

    if-eqz v14, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v26, v0

    const/16 v27, 0x3

    invoke-virtual/range {v26 .. v27}, Landroid/os/Handler;->removeMessages(I)V

    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v26, v0

    if-eqz v26, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mPreviousUpEvent:Landroid/view/MotionEvent;

    move-object/from16 v26, v0

    if-eqz v26, :cond_b

    if-eqz v14, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mPreviousUpEvent:Landroid/view/MotionEvent;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    move-object/from16 v2, v27

    move-object/from16 v3, p1

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/maps/GestureDetector;->isConsideredDoubleTap(Landroid/view/MotionEvent;Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z

    move-result v26

    if-eqz v26, :cond_b

    const/16 v26, 0x1

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/maps/GestureDetector;->mIsDoubleTapping:Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mDoubleTapListener:Lcom/google/android/maps/GestureDetector$OnDoubleTapListener;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v27, v0

    invoke-interface/range {v26 .. v27}, Lcom/google/android/maps/GestureDetector$OnDoubleTapListener;->onDoubleTap(Landroid/view/MotionEvent;)Z

    move-result v26

    or-int v15, v15, v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mDoubleTapListener:Lcom/google/android/maps/GestureDetector$OnDoubleTapListener;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Lcom/google/android/maps/GestureDetector$OnDoubleTapListener;->onDoubleTapEvent(Landroid/view/MotionEvent;)Z

    move-result v26

    or-int v15, v15, v26

    :cond_8
    :goto_6
    move-object/from16 v0, p0

    iput v12, v0, Lcom/google/android/maps/GestureDetector;->mLastFocusX:F

    move-object/from16 v0, p0

    iput v12, v0, Lcom/google/android/maps/GestureDetector;->mDownFocusX:F

    move-object/from16 v0, p0

    iput v13, v0, Lcom/google/android/maps/GestureDetector;->mLastFocusY:F

    move-object/from16 v0, p0

    iput v13, v0, Lcom/google/android/maps/GestureDetector;->mDownFocusY:F

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v26, v0

    if-eqz v26, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/view/MotionEvent;->recycle()V

    :cond_9
    invoke-static/range {p1 .. p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/maps/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    const/16 v26, 0x1

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/maps/GestureDetector;->mAlwaysInTapRegion:Z

    const/16 v26, 0x1

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/maps/GestureDetector;->mAlwaysInBiggerTapRegion:Z

    const/16 v26, 0x1

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/maps/GestureDetector;->mStillDown:Z

    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/maps/GestureDetector;->mInLongPress:Z

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/maps/GestureDetector;->mIsLongpressEnabled:Z

    move/from16 v26, v0

    if-eqz v26, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v26, v0

    const/16 v27, 0x2

    invoke-virtual/range {v26 .. v27}, Landroid/os/Handler;->removeMessages(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v26, v0

    const/16 v27, 0x2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v28

    sget v30, Lcom/google/android/maps/GestureDetector;->TAP_TIMEOUT:I

    move/from16 v0, v30

    int-to-long v0, v0

    move-wide/from16 v30, v0

    add-long v28, v28, v30

    sget v30, Lcom/google/android/maps/GestureDetector;->LONGPRESS_TIMEOUT:I

    move/from16 v0, v30

    int-to-long v0, v0

    move-wide/from16 v30, v0

    add-long v28, v28, v30

    invoke-virtual/range {v26 .. v29}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v28

    sget v30, Lcom/google/android/maps/GestureDetector;->TAP_TIMEOUT:I

    move/from16 v0, v30

    int-to-long v0, v0

    move-wide/from16 v30, v0

    add-long v28, v28, v30

    invoke-virtual/range {v26 .. v29}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mListener:Lcom/google/android/maps/GestureDetector$OnGestureListener;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Lcom/google/android/maps/GestureDetector$OnGestureListener;->onDown(Landroid/view/MotionEvent;)Z

    move-result v26

    or-int v15, v15, v26

    goto/16 :goto_5

    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v26, v0

    const/16 v27, 0x3

    sget v28, Lcom/google/android/maps/GestureDetector;->DOUBLE_TAP_TIMEOUT:I

    move/from16 v0, v28

    int-to-long v0, v0

    move-wide/from16 v28, v0

    invoke-virtual/range {v26 .. v29}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_6

    :pswitch_4
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/maps/GestureDetector;->mInLongPress:Z

    move/from16 v26, v0

    if-nez v26, :cond_5

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/maps/GestureDetector;->mLastFocusX:F

    move/from16 v26, v0

    sub-float v18, v26, v12

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/maps/GestureDetector;->mLastFocusY:F

    move/from16 v26, v0

    sub-float v19, v26, v13

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/maps/GestureDetector;->mIsDoubleTapping:Z

    move/from16 v26, v0

    if-eqz v26, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mDoubleTapListener:Lcom/google/android/maps/GestureDetector$OnDoubleTapListener;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Lcom/google/android/maps/GestureDetector$OnDoubleTapListener;->onDoubleTapEvent(Landroid/view/MotionEvent;)Z

    move-result v26

    or-int v15, v15, v26

    goto/16 :goto_5

    :cond_c
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/maps/GestureDetector;->mAlwaysInTapRegion:Z

    move/from16 v26, v0

    if-eqz v26, :cond_e

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/maps/GestureDetector;->mDownFocusX:F

    move/from16 v26, v0

    sub-float v26, v12, v26

    move/from16 v0, v26

    float-to-int v8, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/maps/GestureDetector;->mDownFocusY:F

    move/from16 v26, v0

    sub-float v26, v13, v26

    move/from16 v0, v26

    float-to-int v9, v0

    mul-int v26, v8, v8

    mul-int v27, v9, v9

    add-int v10, v26, v27

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/maps/GestureDetector;->mTouchSlopSquare:I

    move/from16 v26, v0

    move/from16 v0, v26

    if-le v10, v0, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mListener:Lcom/google/android/maps/GestureDetector$OnGestureListener;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    move-object/from16 v2, p1

    move/from16 v3, v18

    move/from16 v4, v19

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/maps/GestureDetector$OnGestureListener;->onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v15

    move-object/from16 v0, p0

    iput v12, v0, Lcom/google/android/maps/GestureDetector;->mLastFocusX:F

    move-object/from16 v0, p0

    iput v13, v0, Lcom/google/android/maps/GestureDetector;->mLastFocusY:F

    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/maps/GestureDetector;->mAlwaysInTapRegion:Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v26, v0

    const/16 v27, 0x3

    invoke-virtual/range {v26 .. v27}, Landroid/os/Handler;->removeMessages(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-virtual/range {v26 .. v27}, Landroid/os/Handler;->removeMessages(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v26, v0

    const/16 v27, 0x2

    invoke-virtual/range {v26 .. v27}, Landroid/os/Handler;->removeMessages(I)V

    :cond_d
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/maps/GestureDetector;->mDoubleTapTouchSlopSquare:I

    move/from16 v26, v0

    move/from16 v0, v26

    if-le v10, v0, :cond_5

    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/maps/GestureDetector;->mAlwaysInBiggerTapRegion:Z

    goto/16 :goto_5

    :cond_e
    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v26

    const/high16 v27, 0x3f800000

    cmpl-float v26, v26, v27

    if-gez v26, :cond_f

    invoke-static/range {v19 .. v19}, Ljava/lang/Math;->abs(F)F

    move-result v26

    const/high16 v27, 0x3f800000

    cmpl-float v26, v26, v27

    if-ltz v26, :cond_5

    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mListener:Lcom/google/android/maps/GestureDetector$OnGestureListener;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    move-object/from16 v2, p1

    move/from16 v3, v18

    move/from16 v4, v19

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/maps/GestureDetector$OnGestureListener;->onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v15

    move-object/from16 v0, p0

    iput v12, v0, Lcom/google/android/maps/GestureDetector;->mLastFocusX:F

    move-object/from16 v0, p0

    iput v13, v0, Lcom/google/android/maps/GestureDetector;->mLastFocusY:F

    goto/16 :goto_5

    :pswitch_5
    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/maps/GestureDetector;->mStillDown:Z

    invoke-static/range {p1 .. p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v7

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/maps/GestureDetector;->mIsDoubleTapping:Z

    move/from16 v26, v0

    if-eqz v26, :cond_13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mDoubleTapListener:Lcom/google/android/maps/GestureDetector$OnDoubleTapListener;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Lcom/google/android/maps/GestureDetector$OnDoubleTapListener;->onDoubleTapEvent(Landroid/view/MotionEvent;)Z

    move-result v26

    or-int v15, v15, v26

    :cond_10
    :goto_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mPreviousUpEvent:Landroid/view/MotionEvent;

    move-object/from16 v26, v0

    if-eqz v26, :cond_11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mPreviousUpEvent:Landroid/view/MotionEvent;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/view/MotionEvent;->recycle()V

    :cond_11
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/android/maps/GestureDetector;->mPreviousUpEvent:Landroid/view/MotionEvent;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v26, v0

    if-eqz v26, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/view/VelocityTracker;->recycle()V

    const/16 v26, 0x0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/maps/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    :cond_12
    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/maps/GestureDetector;->mIsDoubleTapping:Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-virtual/range {v26 .. v27}, Landroid/os/Handler;->removeMessages(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v26, v0

    const/16 v27, 0x2

    invoke-virtual/range {v26 .. v27}, Landroid/os/Handler;->removeMessages(I)V

    goto/16 :goto_5

    :cond_13
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/maps/GestureDetector;->mInLongPress:Z

    move/from16 v26, v0

    if-eqz v26, :cond_14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v26, v0

    const/16 v27, 0x3

    invoke-virtual/range {v26 .. v27}, Landroid/os/Handler;->removeMessages(I)V

    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/maps/GestureDetector;->mInLongPress:Z

    goto :goto_7

    :cond_14
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/maps/GestureDetector;->mAlwaysInTapRegion:Z

    move/from16 v26, v0

    if-eqz v26, :cond_15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mListener:Lcom/google/android/maps/GestureDetector$OnGestureListener;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Lcom/google/android/maps/GestureDetector$OnGestureListener;->onSingleTapUp(Landroid/view/MotionEvent;)Z

    move-result v15

    goto :goto_7

    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v23, v0

    const/16 v26, 0x3e8

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/maps/GestureDetector;->mMaximumFlingVelocity:I

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v27, v0

    move-object/from16 v0, v23

    move/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    invoke-virtual/range {v23 .. v23}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v25

    invoke-virtual/range {v23 .. v23}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v24

    invoke-static/range {v25 .. v25}, Ljava/lang/Math;->abs(F)F

    move-result v26

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/maps/GestureDetector;->mMinimumFlingVelocity:I

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v27, v0

    cmpl-float v26, v26, v27

    if-gtz v26, :cond_16

    invoke-static/range {v24 .. v24}, Ljava/lang/Math;->abs(F)F

    move-result v26

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/maps/GestureDetector;->mMinimumFlingVelocity:I

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v27, v0

    cmpl-float v26, v26, v27

    if-lez v26, :cond_10

    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mListener:Lcom/google/android/maps/GestureDetector$OnGestureListener;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    move-object/from16 v2, p1

    move/from16 v3, v24

    move/from16 v4, v25

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/maps/GestureDetector$OnGestureListener;->onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v15

    goto/16 :goto_7

    :pswitch_6
    invoke-direct/range {p0 .. p0}, Lcom/google/android/maps/GestureDetector;->cancel()V

    goto/16 :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_5
        :pswitch_4
        :pswitch_6
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setIsLongpressEnabled(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/maps/GestureDetector;->mIsLongpressEnabled:Z

    return-void
.end method

.method public setOnDoubleTapListener(Lcom/google/android/maps/GestureDetector$OnDoubleTapListener;)V
    .locals 0
    .param p1    # Lcom/google/android/maps/GestureDetector$OnDoubleTapListener;

    iput-object p1, p0, Lcom/google/android/maps/GestureDetector;->mDoubleTapListener:Lcom/google/android/maps/GestureDetector$OnDoubleTapListener;

    return-void
.end method
