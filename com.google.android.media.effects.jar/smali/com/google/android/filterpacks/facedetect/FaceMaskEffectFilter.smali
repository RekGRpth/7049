.class public abstract Lcom/google/android/filterpacks/facedetect/FaceMaskEffectFilter;
.super Landroid/filterfw/core/Filter;
.source "FaceMaskEffectFilter.java"


# static fields
.field private static final PCA_DATA_SIZE:I = 0xf

.field protected static final RGB_TO_YUV_MATRIX:Ljava/lang/String; = "0.299, -0.168736,  0.5,      0.000, 0.587, -0.331264, -0.418688, 0.000, 0.114,  0.5,      -0.081312, 0.000, 0.000,  0.5,       0.5,      1.000 "

.field protected static final YUV_TO_RGB_MATRIX:Ljava/lang/String; = " 1.0,       1.0,       1.0,   0.0, -0.000001, -0.344135,  1.772, 0.0,  1.401999, -0.714136,  0.0,   0.0, -0.700999,  0.529135, -0.886, 1.000"

.field private static final mRgbToYuvShader:Ljava/lang/String; = "precision mediump float;\nuniform sampler2D tex_sampler_0;\nconst mat4 coeff_yuv = mat4(0.299, -0.168736,  0.5,      0.000, 0.587, -0.331264, -0.418688, 0.000, 0.114,  0.5,      -0.081312, 0.000, 0.000,  0.5,       0.5,      1.000 );\nvarying vec2 v_texcoord;\nvoid main() {\n  vec4 rgb = vec4(texture2D(tex_sampler_0, v_texcoord).rgb, 1.);\n  gl_FragColor = coeff_yuv * rgb;\n}\n"


# instance fields
.field protected mColorPcaProgram:Landroid/filterfw/core/NativeProgram;

.field protected mHeight:I

.field protected mIdentityProgram:Landroid/filterfw/core/ShaderProgram;

.field protected mRgbToYuvProgram:Landroid/filterfw/core/ShaderProgram;

.field protected mWidth:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;-><init>(Ljava/lang/String;)V

    iput v0, p0, Lcom/google/android/filterpacks/facedetect/FaceMaskEffectFilter;->mWidth:I

    iput v0, p0, Lcom/google/android/filterpacks/facedetect/FaceMaskEffectFilter;->mHeight:I

    return-void
.end method


# virtual methods
.method protected computeFaceColorPCA(Landroid/filterfw/core/FilterContext;Landroid/filterfw/core/Frame;Landroid/graphics/Rect;[F[F[F)V
    .locals 11
    .param p1    # Landroid/filterfw/core/FilterContext;
    .param p2    # Landroid/filterfw/core/Frame;
    .param p3    # Landroid/graphics/Rect;
    .param p4    # [F
    .param p5    # [F
    .param p6    # [F

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/filterpacks/facedetect/FaceMaskEffectFilter;->cropRectRegion(Landroid/filterfw/core/FilterContext;Landroid/filterfw/core/Frame;Landroid/graphics/Rect;)Landroid/filterfw/core/Frame;

    move-result-object v0

    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    move-result-object v8

    const/4 v9, 0x2

    invoke-virtual {v8, v0, v9}, Landroid/filterfw/core/FrameManager;->duplicateFrameToTarget(Landroid/filterfw/core/Frame;I)Landroid/filterfw/core/Frame;

    move-result-object v4

    const/16 v8, 0xf

    const/4 v9, 0x2

    invoke-static {v8, v9}, Landroid/filterfw/format/PrimitiveFormat;->createFloatFormat(II)Landroid/filterfw/core/MutableFrameFormat;

    move-result-object v6

    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    move-result-object v8

    invoke-virtual {v8, v6}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    move-result-object v5

    iget-object v8, p0, Lcom/google/android/filterpacks/facedetect/FaceMaskEffectFilter;->mColorPcaProgram:Landroid/filterfw/core/NativeProgram;

    const-string v9, "width"

    invoke-virtual {v0}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    move-result-object v10

    invoke-virtual {v10}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/filterfw/core/NativeProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v8, p0, Lcom/google/android/filterpacks/facedetect/FaceMaskEffectFilter;->mColorPcaProgram:Landroid/filterfw/core/NativeProgram;

    const-string v9, "height"

    invoke-virtual {v0}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    move-result-object v10

    invoke-virtual {v10}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/filterfw/core/NativeProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v8, p0, Lcom/google/android/filterpacks/facedetect/FaceMaskEffectFilter;->mColorPcaProgram:Landroid/filterfw/core/NativeProgram;

    invoke-virtual {v8, v4, v5}, Landroid/filterfw/core/NativeProgram;->process(Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    invoke-virtual {v5}, Landroid/filterfw/core/Frame;->getFloats()[F

    move-result-object v7

    const/4 v1, 0x0

    :goto_0
    const/4 v8, 0x3

    if-ge v1, v8, :cond_1

    aget v8, v7, v1

    aput v8, p4, v1

    add-int/lit8 v8, v1, 0x3

    aget v8, v7, v8

    aput v8, p5, v1

    const/4 v3, 0x0

    :goto_1
    const/4 v8, 0x3

    if-ge v3, v8, :cond_0

    mul-int/lit8 v8, v1, 0x3

    add-int v2, v8, v3

    add-int/lit8 v8, v2, 0x6

    aget v8, v7, v8

    aput v8, p6, v2

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v5}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    invoke-virtual {v0}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    return-void
.end method

.method protected createBoundedRect(Landroid/filterfw/core/Frame;Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 8
    .param p1    # Landroid/filterfw/core/Frame;
    .param p2    # Landroid/graphics/Rect;

    const/4 v7, 0x0

    invoke-virtual {p1}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    move-result-object v6

    invoke-virtual {v6}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    move-result-object v6

    invoke-virtual {v6}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    move-result v0

    iget v6, p2, Landroid/graphics/Rect;->left:I

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v3

    iget v6, p2, Landroid/graphics/Rect;->top:I

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v5

    iget v6, p2, Landroid/graphics/Rect;->right:I

    add-int/lit8 v7, v1, -0x1

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v2

    iget v6, p2, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v7, v0, -0x1

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v4

    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6, v3, v5, v2, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v6
.end method

.method protected createTanhTable(Landroid/filterfw/core/FilterContext;IFFF)Landroid/filterfw/core/Frame;
    .locals 10
    .param p1    # Landroid/filterfw/core/FilterContext;
    .param p2    # I
    .param p3    # F
    .param p4    # F
    .param p5    # F

    const v2, 0x477fff00

    int-to-float v7, p2

    const/high16 v8, 0x3f800000

    sub-float/2addr v7, v8

    div-float v3, p3, v7

    new-array v0, p2, [I

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p2, :cond_0

    neg-float v7, p5

    int-to-float v8, v1

    mul-float/2addr v8, v3

    sub-float/2addr v8, p4

    mul-float/2addr v7, v8

    float-to-double v7, v7

    invoke-static {v7, v8}, Ljava/lang/Math;->tanh(D)D

    move-result-wide v7

    double-to-float v7, v7

    const/high16 v8, 0x3f000000

    mul-float/2addr v7, v8

    const/high16 v8, 0x3f000000

    add-float v6, v7, v8

    const v7, 0x477fff00

    mul-float/2addr v7, v6

    float-to-int v7, v7

    aput v7, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v7, 0x1

    const/4 v8, 0x3

    const/4 v9, 0x3

    invoke-static {p2, v7, v8, v9}, Landroid/filterfw/format/ImageFormat;->create(IIII)Landroid/filterfw/core/MutableFrameFormat;

    move-result-object v4

    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    move-result-object v7

    invoke-virtual {v7, v4}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/filterfw/core/Frame;->setInts([I)V

    return-object v5
.end method

.method protected cropRectRegion(Landroid/filterfw/core/FilterContext;Landroid/filterfw/core/Frame;Landroid/graphics/Rect;)Landroid/filterfw/core/Frame;
    .locals 19
    .param p1    # Landroid/filterfw/core/FilterContext;
    .param p2    # Landroid/filterfw/core/Frame;
    .param p3    # Landroid/graphics/Rect;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/filterpacks/facedetect/FaceMaskEffectFilter;->createBoundedRect(Landroid/filterfw/core/Frame;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v13

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v4

    invoke-virtual/range {p2 .. p2}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    move-result-object v14

    invoke-virtual {v14}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    move-result v6

    invoke-virtual/range {p2 .. p2}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    move-result-object v14

    invoke-virtual {v14}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    move-result v5

    const/4 v14, 0x3

    const/4 v15, 0x3

    invoke-static {v13, v4, v14, v15}, Landroid/filterfw/format/ImageFormat;->create(IIII)Landroid/filterfw/core/MutableFrameFormat;

    move-result-object v8

    invoke-virtual/range {p1 .. p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    move-result-object v14

    invoke-virtual {v14, v8}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    move-result-object v7

    iget v14, v3, Landroid/graphics/Rect;->left:I

    int-to-float v14, v14

    int-to-float v15, v6

    div-float v11, v14, v15

    iget v14, v3, Landroid/graphics/Rect;->top:I

    int-to-float v14, v14

    int-to-float v15, v5

    div-float v12, v14, v15

    int-to-float v14, v13

    int-to-float v15, v6

    div-float v10, v14, v15

    int-to-float v14, v4

    int-to-float v15, v5

    div-float v9, v14, v15

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/filterpacks/facedetect/FaceMaskEffectFilter;->mIdentityProgram:Landroid/filterfw/core/ShaderProgram;

    invoke-virtual {v14, v11, v12, v10, v9}, Landroid/filterfw/core/ShaderProgram;->setSourceRect(FFFF)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/filterpacks/facedetect/FaceMaskEffectFilter;->mIdentityProgram:Landroid/filterfw/core/ShaderProgram;

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/high16 v17, 0x3f800000

    const/high16 v18, 0x3f800000

    invoke-virtual/range {v14 .. v18}, Landroid/filterfw/core/ShaderProgram;->setTargetRect(FFFF)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/filterpacks/facedetect/FaceMaskEffectFilter;->mIdentityProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v0, p2

    invoke-virtual {v14, v0, v7}, Landroid/filterfw/core/ShaderProgram;->process(Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    return-object v7
.end method

.method public getOutputFormat(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/FrameFormat;
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/filterfw/core/FrameFormat;

    return-object p2
.end method

.method protected abstract initPrograms(Landroid/filterfw/core/FilterContext;)V
.end method

.method protected prepare(Landroid/filterfw/core/FilterContext;)V
    .locals 3
    .param p1    # Landroid/filterfw/core/FilterContext;

    new-instance v0, Landroid/filterfw/core/ShaderProgram;

    const-string v1, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nconst mat4 coeff_yuv = mat4(0.299, -0.168736,  0.5,      0.000, 0.587, -0.331264, -0.418688, 0.000, 0.114,  0.5,      -0.081312, 0.000, 0.000,  0.5,       0.5,      1.000 );\nvarying vec2 v_texcoord;\nvoid main() {\n  vec4 rgb = vec4(texture2D(tex_sampler_0, v_texcoord).rgb, 1.);\n  gl_FragColor = coeff_yuv * rgb;\n}\n"

    invoke-direct {v0, p1, v1}, Landroid/filterfw/core/ShaderProgram;-><init>(Landroid/filterfw/core/FilterContext;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/filterpacks/facedetect/FaceMaskEffectFilter;->mRgbToYuvProgram:Landroid/filterfw/core/ShaderProgram;

    invoke-static {p1}, Landroid/filterfw/core/ShaderProgram;->createIdentity(Landroid/filterfw/core/FilterContext;)Landroid/filterfw/core/ShaderProgram;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/filterpacks/facedetect/FaceMaskEffectFilter;->mIdentityProgram:Landroid/filterfw/core/ShaderProgram;

    new-instance v0, Landroid/filterfw/core/NativeProgram;

    const-string v1, "filterpack_facedetect"

    const-string v2, "color_pca"

    invoke-direct {v0, v1, v2}, Landroid/filterfw/core/NativeProgram;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/filterpacks/facedetect/FaceMaskEffectFilter;->mColorPcaProgram:Landroid/filterfw/core/NativeProgram;

    invoke-virtual {p0, p1}, Lcom/google/android/filterpacks/facedetect/FaceMaskEffectFilter;->initPrograms(Landroid/filterfw/core/FilterContext;)V

    return-void
.end method

.method public setupPorts()V
    .locals 3

    const-class v1, Lcom/google/android/filterpacks/facedetect/FaceMeta;

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/filterfw/format/ObjectFormat;->fromClass(Ljava/lang/Class;I)Landroid/filterfw/core/MutableFrameFormat;

    move-result-object v0

    const-string v1, "faces"

    invoke-virtual {p0, v1, v0}, Lcom/google/android/filterpacks/facedetect/FaceMaskEffectFilter;->addMaskedInputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    const-string v1, "image"

    const/4 v2, 0x3

    invoke-static {v2}, Landroid/filterfw/format/ImageFormat;->create(I)Landroid/filterfw/core/MutableFrameFormat;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/filterpacks/facedetect/FaceMaskEffectFilter;->addMaskedInputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    const-string v1, "image"

    const-string v2, "image"

    invoke-virtual {p0, v1, v2}, Lcom/google/android/filterpacks/facedetect/FaceMaskEffectFilter;->addOutputBasedOnInput(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
