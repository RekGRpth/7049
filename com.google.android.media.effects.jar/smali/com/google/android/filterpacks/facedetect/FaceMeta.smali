.class public Lcom/google/android/filterpacks/facedetect/FaceMeta;
.super Landroid/filterfw/core/NativeBuffer;
.source "FaceMeta.java"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "filterpack_facedetect"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/filterfw/core/NativeBuffer;-><init>()V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0
    .param p1    # I

    invoke-direct {p0, p1}, Landroid/filterfw/core/NativeBuffer;-><init>(I)V

    return-void
.end method

.method private native nativeGetConfidence(I)F
.end method

.method private native nativeGetFaceX0(I)F
.end method

.method private native nativeGetFaceX1(I)F
.end method

.method private native nativeGetFaceY0(I)F
.end method

.method private native nativeGetFaceY1(I)F
.end method

.method private native nativeGetId(I)I
.end method

.method private native nativeGetLeftEyeX(I)F
.end method

.method private native nativeGetLeftEyeY(I)F
.end method

.method private native nativeGetLowerLipX(I)F
.end method

.method private native nativeGetLowerLipY(I)F
.end method

.method private native nativeGetMouthLeftX(I)F
.end method

.method private native nativeGetMouthLeftY(I)F
.end method

.method private native nativeGetMouthRightX(I)F
.end method

.method private native nativeGetMouthRightY(I)F
.end method

.method private native nativeGetMouthX(I)F
.end method

.method private native nativeGetMouthY(I)F
.end method

.method private native nativeGetRightEyeX(I)F
.end method

.method private native nativeGetRightEyeY(I)F
.end method

.method private native nativeGetUpperLipX(I)F
.end method

.method private native nativeGetUpperLipY(I)F
.end method

.method private native nativeSetConfidence(IF)Z
.end method

.method private native nativeSetFaceX0(IF)Z
.end method

.method private native nativeSetFaceX1(IF)Z
.end method

.method private native nativeSetFaceY0(IF)Z
.end method

.method private native nativeSetFaceY1(IF)Z
.end method

.method private native nativeSetId(II)Z
.end method

.method private native nativeSetLeftEyeX(IF)Z
.end method

.method private native nativeSetLeftEyeY(IF)Z
.end method

.method private native nativeSetLowerLipX(IF)Z
.end method

.method private native nativeSetLowerLipY(IF)Z
.end method

.method private native nativeSetMouthLeftX(IF)Z
.end method

.method private native nativeSetMouthLeftY(IF)Z
.end method

.method private native nativeSetMouthRightX(IF)Z
.end method

.method private native nativeSetMouthRightY(IF)Z
.end method

.method private native nativeSetMouthX(IF)Z
.end method

.method private native nativeSetMouthY(IF)Z
.end method

.method private native nativeSetRightEyeX(IF)Z
.end method

.method private native nativeSetRightEyeY(IF)Z
.end method

.method private native nativeSetUpperLipX(IF)Z
.end method

.method private native nativeSetUpperLipY(IF)Z
.end method


# virtual methods
.method public getConfidence(I)F
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->assertReadable()V

    invoke-direct {p0, p1}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->nativeGetConfidence(I)F

    move-result v0

    return v0
.end method

.method public native getElementSize()I
.end method

.method public getFaceX0(I)F
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->assertReadable()V

    invoke-direct {p0, p1}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->nativeGetFaceX0(I)F

    move-result v0

    return v0
.end method

.method public getFaceX1(I)F
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->assertReadable()V

    invoke-direct {p0, p1}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->nativeGetFaceX1(I)F

    move-result v0

    return v0
.end method

.method public getFaceY0(I)F
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->assertReadable()V

    invoke-direct {p0, p1}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->nativeGetFaceY0(I)F

    move-result v0

    return v0
.end method

.method public getFaceY1(I)F
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->assertReadable()V

    invoke-direct {p0, p1}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->nativeGetFaceY1(I)F

    move-result v0

    return v0
.end method

.method public getId(I)I
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->assertReadable()V

    invoke-direct {p0, p1}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->nativeGetId(I)I

    move-result v0

    return v0
.end method

.method public getLeftEyeX(I)F
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->assertReadable()V

    invoke-direct {p0, p1}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->nativeGetLeftEyeX(I)F

    move-result v0

    return v0
.end method

.method public getLeftEyeY(I)F
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->assertReadable()V

    invoke-direct {p0, p1}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->nativeGetLeftEyeY(I)F

    move-result v0

    return v0
.end method

.method public getLowerLipX(I)F
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->assertReadable()V

    invoke-direct {p0, p1}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->nativeGetLowerLipX(I)F

    move-result v0

    return v0
.end method

.method public getLowerLipY(I)F
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->assertReadable()V

    invoke-direct {p0, p1}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->nativeGetLowerLipY(I)F

    move-result v0

    return v0
.end method

.method public getMouthLeftX(I)F
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->assertReadable()V

    invoke-direct {p0, p1}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->nativeGetMouthLeftX(I)F

    move-result v0

    return v0
.end method

.method public getMouthLeftY(I)F
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->assertReadable()V

    invoke-direct {p0, p1}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->nativeGetMouthLeftY(I)F

    move-result v0

    return v0
.end method

.method public getMouthRightX(I)F
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->assertReadable()V

    invoke-direct {p0, p1}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->nativeGetMouthRightX(I)F

    move-result v0

    return v0
.end method

.method public getMouthRightY(I)F
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->assertReadable()V

    invoke-direct {p0, p1}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->nativeGetMouthRightY(I)F

    move-result v0

    return v0
.end method

.method public getMouthX(I)F
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->assertReadable()V

    invoke-direct {p0, p1}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->nativeGetMouthX(I)F

    move-result v0

    return v0
.end method

.method public getMouthY(I)F
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->assertReadable()V

    invoke-direct {p0, p1}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->nativeGetMouthY(I)F

    move-result v0

    return v0
.end method

.method public getRightEyeX(I)F
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->assertReadable()V

    invoke-direct {p0, p1}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->nativeGetRightEyeX(I)F

    move-result v0

    return v0
.end method

.method public getRightEyeY(I)F
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->assertReadable()V

    invoke-direct {p0, p1}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->nativeGetRightEyeY(I)F

    move-result v0

    return v0
.end method

.method public getUpperLipX(I)F
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->assertReadable()V

    invoke-direct {p0, p1}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->nativeGetUpperLipX(I)F

    move-result v0

    return v0
.end method

.method public getUpperLipY(I)F
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->assertReadable()V

    invoke-direct {p0, p1}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->nativeGetUpperLipY(I)F

    move-result v0

    return v0
.end method

.method public setConfidence(IF)V
    .locals 0
    .param p1    # I
    .param p2    # F

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->assertWritable()V

    invoke-direct {p0, p1, p2}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->nativeSetConfidence(IF)Z

    return-void
.end method

.method public setFaceX0(IF)V
    .locals 0
    .param p1    # I
    .param p2    # F

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->assertWritable()V

    invoke-direct {p0, p1, p2}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->nativeSetFaceX0(IF)Z

    return-void
.end method

.method public setFaceX1(IF)V
    .locals 0
    .param p1    # I
    .param p2    # F

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->assertWritable()V

    invoke-direct {p0, p1, p2}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->nativeSetFaceX1(IF)Z

    return-void
.end method

.method public setFaceY0(IF)V
    .locals 0
    .param p1    # I
    .param p2    # F

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->assertWritable()V

    invoke-direct {p0, p1, p2}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->nativeSetFaceY0(IF)Z

    return-void
.end method

.method public setFaceY1(IF)V
    .locals 0
    .param p1    # I
    .param p2    # F

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->assertWritable()V

    invoke-direct {p0, p1, p2}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->nativeSetFaceY1(IF)Z

    return-void
.end method

.method public setId(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->assertWritable()V

    invoke-direct {p0, p1, p2}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->nativeSetId(II)Z

    return-void
.end method

.method public setLeftEyeX(IF)V
    .locals 0
    .param p1    # I
    .param p2    # F

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->assertWritable()V

    invoke-direct {p0, p1, p2}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->nativeSetLeftEyeX(IF)Z

    return-void
.end method

.method public setLeftEyeY(IF)V
    .locals 0
    .param p1    # I
    .param p2    # F

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->assertWritable()V

    invoke-direct {p0, p1, p2}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->nativeSetLeftEyeY(IF)Z

    return-void
.end method

.method public setLowerLipX(IF)V
    .locals 0
    .param p1    # I
    .param p2    # F

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->assertWritable()V

    invoke-direct {p0, p1, p2}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->nativeSetLowerLipX(IF)Z

    return-void
.end method

.method public setLowerLipY(IF)V
    .locals 0
    .param p1    # I
    .param p2    # F

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->assertWritable()V

    invoke-direct {p0, p1, p2}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->nativeSetLowerLipY(IF)Z

    return-void
.end method

.method public setMouthLeftX(IF)V
    .locals 0
    .param p1    # I
    .param p2    # F

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->assertWritable()V

    invoke-direct {p0, p1, p2}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->nativeSetMouthLeftX(IF)Z

    return-void
.end method

.method public setMouthLeftY(IF)V
    .locals 0
    .param p1    # I
    .param p2    # F

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->assertWritable()V

    invoke-direct {p0, p1, p2}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->nativeSetMouthLeftY(IF)Z

    return-void
.end method

.method public setMouthRightX(IF)V
    .locals 0
    .param p1    # I
    .param p2    # F

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->assertWritable()V

    invoke-direct {p0, p1, p2}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->nativeSetMouthRightX(IF)Z

    return-void
.end method

.method public setMouthRightY(IF)V
    .locals 0
    .param p1    # I
    .param p2    # F

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->assertWritable()V

    invoke-direct {p0, p1, p2}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->nativeSetMouthRightY(IF)Z

    return-void
.end method

.method public setMouthX(IF)V
    .locals 0
    .param p1    # I
    .param p2    # F

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->assertWritable()V

    invoke-direct {p0, p1, p2}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->nativeSetMouthX(IF)Z

    return-void
.end method

.method public setMouthY(IF)V
    .locals 0
    .param p1    # I
    .param p2    # F

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->assertWritable()V

    invoke-direct {p0, p1, p2}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->nativeSetMouthY(IF)Z

    return-void
.end method

.method public setRightEyeX(IF)V
    .locals 0
    .param p1    # I
    .param p2    # F

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->assertWritable()V

    invoke-direct {p0, p1, p2}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->nativeSetRightEyeX(IF)Z

    return-void
.end method

.method public setRightEyeY(IF)V
    .locals 0
    .param p1    # I
    .param p2    # F

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->assertWritable()V

    invoke-direct {p0, p1, p2}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->nativeSetRightEyeY(IF)Z

    return-void
.end method

.method public setUpperLipX(IF)V
    .locals 0
    .param p1    # I
    .param p2    # F

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->assertWritable()V

    invoke-direct {p0, p1, p2}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->nativeSetUpperLipX(IF)Z

    return-void
.end method

.method public setUpperLipY(IF)V
    .locals 0
    .param p1    # I
    .param p2    # F

    invoke-virtual {p0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->assertWritable()V

    invoke-direct {p0, p1, p2}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->nativeSetUpperLipY(IF)Z

    return-void
.end method
