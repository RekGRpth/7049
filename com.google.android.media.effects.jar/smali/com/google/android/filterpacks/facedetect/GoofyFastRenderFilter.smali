.class public Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;
.super Landroid/filterfw/core/Filter;
.source "GoofyFastRenderFilter.java"


# static fields
.field private static final BIG_EYES:I = 0x1

.field private static final BIG_MOUTH:I = 0x2

.field private static final BIG_NOSE:I = 0x4

.field private static final NUM_EFFECTS:I = 0x6

.field private static final SMALL_EYES:I = 0x5

.field private static final SMALL_MOUTH:I = 0x3

.field private static final SQUEEZE:I = 0x0

.field private static final TAG:Ljava/lang/String; = "GoofyFastRenderFilter"


# instance fields
.field private mAnimateCurrent:F

.field private mAnimationStartTimeStamp:J

.field private mAspect:[F

.field private mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

.field private mCurrentEffect:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "currentEffect"
    .end annotation
.end field

.field private mCurrentTimeStamp:J

.field private mDistortionAmount:F
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "distortionAmount"
    .end annotation
.end field

.field private final mDistortionVertexShader:Ljava/lang/String;

.field private final mDistortionVertexShader2:Ljava/lang/String;

.field private mEnableAnimation:Z
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "enableAnimation"
    .end annotation
.end field

.field private mIdentityProgram:Landroid/filterfw/core/ShaderProgram;

.field private final mIdentityShader:Ljava/lang/String;

.field private mMeshDistortionFrame:Landroid/filterfw/core/VertexFrame;

.field private mPureIdentityProgram:Landroid/filterfw/core/ShaderProgram;

.field private mSmoothness:F
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "smoothness"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mCurrentEffect:I

    iput v1, p0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mDistortionAmount:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mEnableAnimation:Z

    const v0, 0x3e99999a

    iput v0, p0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mSmoothness:F

    iput v1, p0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mAnimateCurrent:F

    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mAspect:[F

    const-string v0, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nvarying vec2 v_texcoord;\nvoid main() {\n  gl_FragColor = texture2D(tex_sampler_0, v_texcoord);\n}\n"

    iput-object v0, p0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mIdentityShader:Ljava/lang/String;

    const-string v0, "uniform vec2 center;\nuniform vec2 weight;\nuniform mat2 rotate;\nuniform float amount;\nattribute vec4 positions;\nattribute vec2 texcoords;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec2 mesh_point = (rotate * positions.xy) * weight *2.0 +\n                    2.0 * (center - vec2(0.5, 0.5));\n  gl_Position = positions;\n  gl_Position.x = mesh_point.x;\n  gl_Position.y = mesh_point.y;\n  vec2 p = (1.0 + texcoords * amount) * positions.xy;\n  v_texcoord = (rotate * p) * weight  + center;\n}\n"

    iput-object v0, p0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mDistortionVertexShader:Ljava/lang/String;

    const-string v0, "uniform vec2 center;\nuniform mat2 rotate;\nuniform vec2 weight;\nuniform float amount;\nattribute vec4 positions;\nattribute vec2 texcoords;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec2 mesh_point = (rotate * (positions.xy * vec2(3.0, 2.0))) * weight +\n                    2.0 * (center - vec2(0.5, 0.5));\n  gl_Position = positions;\n  gl_Position.x = mesh_point.x;\n  gl_Position.y = mesh_point.y;\n  float x = (1.0 + amount * texcoords.x) * positions.x + amount * texcoords.y;\n  float y = positions.y * (1.0 + texcoords.x * amount);\n  vec2 p = vec2(x,y);\n  v_texcoord = (rotate * (p * vec2(3.0,2.0))) * weight * 0.5 + center;\n}\n"

    iput-object v0, p0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mDistortionVertexShader2:Ljava/lang/String;

    return-void

    :array_0
    .array-data 4
        0x3f800000
        0x3f800000
    .end array-data
.end method

.method private createMesh(Landroid/filterfw/core/FilterContext;)V
    .locals 32
    .param p1    # Landroid/filterfw/core/FilterContext;

    move-object/from16 v0, p0

    iget v12, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mDistortionAmount:F

    const/high16 v3, 0x42c80000

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mSmoothness:F

    mul-float/2addr v3, v4

    float-to-int v0, v3

    move/from16 v20, v0

    const/high16 v3, 0x42c80000

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mSmoothness:F

    mul-float/2addr v3, v4

    float-to-int v0, v3

    move/from16 v19, v0

    const/4 v11, 0x4

    const/16 v18, 0x6

    const/16 v17, 0x4

    mul-int v3, v20, v19

    mul-int/lit8 v3, v3, 0x6

    mul-int/lit8 v21, v3, 0x4

    move/from16 v0, v21

    new-array v0, v0, [F

    move-object/from16 v23, v0

    const/4 v14, 0x0

    :goto_0
    move/from16 v0, v20

    if-ge v14, v0, :cond_2

    const/4 v15, 0x0

    :goto_1
    move/from16 v0, v19

    if-ge v15, v0, :cond_1

    mul-int v3, v14, v19

    add-int/2addr v3, v15

    mul-int/lit8 v3, v3, 0x6

    mul-int/lit8 v22, v3, 0x4

    int-to-float v3, v15

    move/from16 v0, v19

    int-to-float v4, v0

    div-float v27, v3, v4

    int-to-float v3, v14

    move/from16 v0, v20

    int-to-float v4, v0

    div-float v30, v3, v4

    add-int/lit8 v3, v15, 0x1

    int-to-float v3, v3

    move/from16 v0, v19

    int-to-float v4, v0

    div-float v28, v3, v4

    add-int/lit8 v3, v14, 0x1

    int-to-float v3, v3

    move/from16 v0, v20

    int-to-float v4, v0

    div-float v31, v3, v4

    const/16 v16, 0x0

    :goto_2
    const/4 v3, 0x6

    move/from16 v0, v16

    if-ge v0, v3, :cond_0

    const/16 v26, 0x0

    const/16 v29, 0x0

    packed-switch v16, :pswitch_data_0

    :goto_3
    const/high16 v3, 0x3f000000

    sub-float v3, v26, v3

    const/high16 v4, 0x40000000

    mul-float v26, v3, v4

    const/high16 v3, 0x3f000000

    sub-float v3, v29, v3

    const/high16 v4, 0x40000000

    mul-float v29, v3, v4

    mul-int/lit8 v3, v16, 0x4

    add-int v3, v3, v22

    aput v26, v23, v3

    mul-int/lit8 v3, v16, 0x4

    add-int v3, v3, v22

    add-int/lit8 v3, v3, 0x1

    aput v29, v23, v3

    move-object/from16 v0, p0

    move/from16 v1, v26

    move/from16 v2, v29

    invoke-direct {v0, v1, v2, v12}, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->getTexturePosition(FFF)[F

    move-result-object v24

    mul-int/lit8 v3, v16, 0x4

    add-int v3, v3, v22

    add-int/lit8 v3, v3, 0x2

    const/4 v4, 0x0

    aget v4, v24, v4

    aput v4, v23, v3

    mul-int/lit8 v3, v16, 0x4

    add-int v3, v3, v22

    add-int/lit8 v3, v3, 0x3

    const/4 v4, 0x1

    aget v4, v24, v4

    aput v4, v23, v3

    add-int/lit8 v16, v16, 0x1

    goto :goto_2

    :pswitch_0
    move/from16 v26, v27

    move/from16 v29, v30

    goto :goto_3

    :pswitch_1
    move/from16 v26, v27

    move/from16 v29, v31

    goto :goto_3

    :pswitch_2
    move/from16 v26, v28

    move/from16 v29, v30

    goto :goto_3

    :pswitch_3
    move/from16 v26, v28

    move/from16 v29, v31

    goto :goto_3

    :cond_0
    add-int/lit8 v15, v15, 0x1

    goto/16 :goto_1

    :cond_1
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_0

    :cond_2
    const/4 v3, 0x4

    move/from16 v0, v21

    invoke-static {v0, v3}, Landroid/filterfw/format/PrimitiveFormat;->createFloatFormat(II)Landroid/filterfw/core/MutableFrameFormat;

    move-result-object v25

    invoke-virtual/range {p1 .. p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mMeshDistortionFrame:Landroid/filterfw/core/VertexFrame;

    if-eqz v3, :cond_3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mMeshDistortionFrame:Landroid/filterfw/core/VertexFrame;

    invoke-virtual {v3}, Landroid/filterfw/core/VertexFrame;->release()Landroid/filterfw/core/Frame;

    :cond_3
    move-object/from16 v0, v25

    invoke-virtual {v13, v0}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    move-result-object v3

    check-cast v3, Landroid/filterfw/core/VertexFrame;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mMeshDistortionFrame:Landroid/filterfw/core/VertexFrame;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mMeshDistortionFrame:Landroid/filterfw/core/VertexFrame;

    move-object/from16 v0, v23

    invoke-virtual {v3, v0}, Landroid/filterfw/core/VertexFrame;->setFloats([F)V

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mCurrentEffect:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    const-string v4, "positions"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mMeshDistortionFrame:Landroid/filterfw/core/VertexFrame;

    const/16 v6, 0x1406

    const/4 v7, 0x2

    const/16 v8, 0x10

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v3 .. v10}, Landroid/filterfw/core/ShaderProgram;->setAttributeValues(Ljava/lang/String;Landroid/filterfw/core/VertexFrame;IIIIZ)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    const-string v4, "texcoords"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mMeshDistortionFrame:Landroid/filterfw/core/VertexFrame;

    const/16 v6, 0x1406

    const/4 v7, 0x2

    const/16 v8, 0x10

    const/16 v9, 0x8

    const/4 v10, 0x0

    invoke-virtual/range {v3 .. v10}, Landroid/filterfw/core/ShaderProgram;->setAttributeValues(Ljava/lang/String;Landroid/filterfw/core/VertexFrame;IIIIZ)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    mul-int v4, v20, v19

    mul-int/lit8 v4, v4, 0x6

    invoke-virtual {v3, v4}, Landroid/filterfw/core/ShaderProgram;->setVertexCount(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/filterfw/core/ShaderProgram;->setDrawMode(I)V

    :goto_4
    return-void

    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mIdentityProgram:Landroid/filterfw/core/ShaderProgram;

    const-string v4, "positions"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mMeshDistortionFrame:Landroid/filterfw/core/VertexFrame;

    const/16 v6, 0x1406

    const/4 v7, 0x2

    const/16 v8, 0x10

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v3 .. v10}, Landroid/filterfw/core/ShaderProgram;->setAttributeValues(Ljava/lang/String;Landroid/filterfw/core/VertexFrame;IIIIZ)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mIdentityProgram:Landroid/filterfw/core/ShaderProgram;

    const-string v4, "texcoords"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mMeshDistortionFrame:Landroid/filterfw/core/VertexFrame;

    const/16 v6, 0x1406

    const/4 v7, 0x2

    const/16 v8, 0x10

    const/16 v9, 0x8

    const/4 v10, 0x0

    invoke-virtual/range {v3 .. v10}, Landroid/filterfw/core/ShaderProgram;->setAttributeValues(Ljava/lang/String;Landroid/filterfw/core/VertexFrame;IIIIZ)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mIdentityProgram:Landroid/filterfw/core/ShaderProgram;

    mul-int v4, v20, v19

    mul-int/lit8 v4, v4, 0x6

    invoke-virtual {v3, v4}, Landroid/filterfw/core/ShaderProgram;->setVertexCount(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mIdentityProgram:Landroid/filterfw/core/ShaderProgram;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/filterfw/core/ShaderProgram;->setDrawMode(I)V

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method private createProgram(Landroid/filterfw/core/FilterContext;)V
    .locals 3
    .param p1    # Landroid/filterfw/core/FilterContext;

    new-instance v0, Landroid/filterfw/core/ShaderProgram;

    const-string v1, "uniform vec2 center;\nuniform vec2 weight;\nuniform mat2 rotate;\nuniform float amount;\nattribute vec4 positions;\nattribute vec2 texcoords;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec2 mesh_point = (rotate * positions.xy) * weight *2.0 +\n                    2.0 * (center - vec2(0.5, 0.5));\n  gl_Position = positions;\n  gl_Position.x = mesh_point.x;\n  gl_Position.y = mesh_point.y;\n  vec2 p = (1.0 + texcoords * amount) * positions.xy;\n  v_texcoord = (rotate * p) * weight  + center;\n}\n"

    const-string v2, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nvarying vec2 v_texcoord;\nvoid main() {\n  gl_FragColor = texture2D(tex_sampler_0, v_texcoord);\n}\n"

    invoke-direct {v0, p1, v1, v2}, Landroid/filterfw/core/ShaderProgram;-><init>(Landroid/filterfw/core/FilterContext;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mIdentityProgram:Landroid/filterfw/core/ShaderProgram;

    new-instance v0, Landroid/filterfw/core/ShaderProgram;

    const-string v1, "uniform vec2 center;\nuniform mat2 rotate;\nuniform vec2 weight;\nuniform float amount;\nattribute vec4 positions;\nattribute vec2 texcoords;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec2 mesh_point = (rotate * (positions.xy * vec2(3.0, 2.0))) * weight +\n                    2.0 * (center - vec2(0.5, 0.5));\n  gl_Position = positions;\n  gl_Position.x = mesh_point.x;\n  gl_Position.y = mesh_point.y;\n  float x = (1.0 + amount * texcoords.x) * positions.x + amount * texcoords.y;\n  float y = positions.y * (1.0 + texcoords.x * amount);\n  vec2 p = vec2(x,y);\n  v_texcoord = (rotate * (p * vec2(3.0,2.0))) * weight * 0.5 + center;\n}\n"

    const-string v2, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nvarying vec2 v_texcoord;\nvoid main() {\n  gl_FragColor = texture2D(tex_sampler_0, v_texcoord);\n}\n"

    invoke-direct {v0, p1, v1, v2}, Landroid/filterfw/core/ShaderProgram;-><init>(Landroid/filterfw/core/FilterContext;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    return-void
.end method

.method private getDistortionScale(FF)F
    .locals 12
    .param p1    # F
    .param p2    # F

    const v5, 0x3e4ccccd

    const/high16 v11, 0x3f800000

    const/high16 v10, 0x40000000

    move v3, p1

    move v0, p1

    iget v4, p0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mCurrentEffect:I

    packed-switch v4, :pswitch_data_0

    :goto_0
    return v3

    :pswitch_0
    const v2, 0x3e99999a

    add-float v4, v0, v5

    neg-float v4, v4

    add-float/2addr v5, v0

    mul-float/2addr v4, v5

    const/high16 v5, 0x40400000

    mul-float/2addr v4, v5

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->exp(D)D

    move-result-wide v4

    double-to-float v4, v4

    const v5, 0x3f63126f

    div-float/2addr v4, v5

    add-float/2addr v4, v11

    div-float v4, v10, v4

    sub-float v4, v10, v4

    mul-float v3, p2, v4

    goto :goto_0

    :pswitch_1
    const v2, 0x3e99999a

    const v4, 0x3f4ccccd

    mul-float/2addr v4, p2

    neg-float v5, v0

    mul-float/2addr v5, v0

    mul-float v6, v2, v2

    div-float/2addr v5, v6

    float-to-double v5, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->exp(D)D

    move-result-wide v5

    double-to-float v5, v5

    sub-float v5, v10, v5

    const/high16 v6, 0x3e800000

    add-float/2addr v6, v0

    float-to-double v6, v6

    const-wide/high16 v8, 0x4010000000000000L

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    neg-double v6, v6

    const-wide/high16 v8, 0x4000000000000000L

    mul-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->exp(D)D

    move-result-wide v6

    double-to-float v6, v6

    add-float/2addr v6, v11

    div-float v6, v10, v6

    sub-float/2addr v5, v6

    mul-float v3, v4, v5

    goto :goto_0

    :pswitch_2
    const v2, 0x3f333333

    const-wide/high16 v4, 0x4090000000000000L

    invoke-static {v4, v5}, Ljava/lang/Math;->log(D)D

    move-result-wide v4

    float-to-double v6, v2

    mul-double/2addr v4, v6

    float-to-double v6, v2

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v1, v4

    cmpl-float v4, v1, v11

    if-lez v4, :cond_0

    mul-float/2addr v0, v1

    :cond_0
    const v4, -0x40d9999a

    mul-float/2addr v4, p2

    neg-float v5, v0

    mul-float/2addr v5, v0

    mul-float v6, v2, v2

    div-float/2addr v5, v6

    float-to-double v5, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->exp(D)D

    move-result-wide v5

    double-to-float v5, v5

    mul-float v3, v4, v5

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private getTexturePosition(FFF)[F
    .locals 19
    .param p1    # F
    .param p2    # F
    .param p3    # F

    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v15, v0, [F

    fill-array-data v15, :array_0

    const/4 v6, 0x0

    const/high16 v11, 0x3f800000

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mCurrentEffect:I

    move/from16 v16, v0

    const/16 v17, 0x1

    move/from16 v0, v16

    move/from16 v1, v17

    if-eq v0, v1, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->getEffectAspectRatio()[F

    move-result-object v7

    const/16 v16, 0x0

    aget v16, v7, v16

    const/16 v17, 0x1

    aget v17, v7, v17

    cmpg-float v16, v16, v17

    if-gez v16, :cond_1

    const/16 v16, 0x1

    aget v2, v7, v16

    :goto_0
    mul-float v16, p1, p1

    const/16 v17, 0x0

    aget v17, v7, v17

    div-float v16, v16, v17

    const/16 v17, 0x0

    aget v17, v7, v17

    div-float v16, v16, v17

    mul-float v17, p2, p2

    const/16 v18, 0x1

    aget v18, v7, v18

    div-float v17, v17, v18

    const/16 v18, 0x1

    aget v18, v7, v18

    div-float v17, v17, v18

    add-float v16, v16, v17

    move/from16 v0, v16

    float-to-double v0, v0

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-float v5, v0

    const/high16 v16, 0x3f800000

    cmpg-float v16, v5, v16

    if-gtz v16, :cond_0

    const/high16 v16, 0x3f800000

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v5, v1}, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->getDistortionScale(FF)F

    move-result v11

    const/16 v16, 0x0

    aput v11, v15, v16

    const/16 v16, 0x1

    aput v11, v15, v16

    :cond_0
    :goto_1
    return-object v15

    :cond_1
    const/16 v16, 0x0

    aget v2, v7, v16

    goto :goto_0

    :cond_2
    const/high16 v8, 0x3f800000

    const/high16 v16, 0x40000000

    mul-float v16, v16, v8

    const/high16 v17, 0x3f800000

    add-float v14, v16, v17

    div-float v9, v8, v14

    const/high16 v16, 0x3f800000

    sub-float v10, v16, v9

    const/high16 v16, 0x3f000000

    sub-float v16, v9, v16

    const/high16 v17, 0x40000000

    mul-float v9, v16, v17

    const/high16 v16, 0x3f000000

    sub-float v16, v10, v16

    const/high16 v17, 0x40000000

    mul-float v10, v16, v17

    sub-float v3, p1, v9

    const/high16 v16, 0x40000000

    div-float v16, v14, v16

    mul-float v17, v3, v3

    mul-float v18, p2, p2

    add-float v17, v17, v18

    move/from16 v0, v17

    float-to-double v0, v0

    move-wide/from16 v17, v0

    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v17

    move-wide/from16 v0, v17

    double-to-float v0, v0

    move/from16 v17, v0

    mul-float v5, v16, v17

    sub-float v4, p1, v10

    const/high16 v16, 0x40000000

    div-float v16, v14, v16

    mul-float v17, v4, v4

    mul-float v18, p2, p2

    add-float v17, v17, v18

    move/from16 v0, v17

    float-to-double v0, v0

    move-wide/from16 v17, v0

    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v17

    move-wide/from16 v0, v17

    double-to-float v0, v0

    move/from16 v17, v0

    mul-float v6, v16, v17

    const/high16 v16, 0x3f800000

    cmpg-float v16, v5, v16

    if-ltz v16, :cond_3

    const/high16 v16, 0x3f800000

    cmpg-float v16, v6, v16

    if-gez v16, :cond_0

    :cond_3
    const/high16 v16, 0x3f800000

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v5, v1}, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->getDistortionScale(FF)F

    move-result v12

    const/high16 v16, 0x3f800000

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v6, v1}, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->getDistortionScale(FF)F

    move-result v13

    const/16 v16, 0x0

    mul-float v17, v12, v12

    mul-float v18, v13, v13

    add-float v17, v17, v18

    add-float v18, v12, v13

    div-float v17, v17, v18

    aput v17, v15, v16

    const/16 v16, 0x1

    sub-float v17, v12, v13

    move/from16 v0, v17

    neg-float v0, v0

    move/from16 v17, v0

    mul-float v17, v17, v9

    aput v17, v15, v16

    goto/16 :goto_1

    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method


# virtual methods
.method public fieldPortValueUpdated(Ljava/lang/String;Landroid/filterfw/core/FilterContext;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/filterfw/core/FilterContext;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mAnimateCurrent:F

    iget-object v0, p0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mIdentityProgram:Landroid/filterfw/core/ShaderProgram;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    if-eqz v0, :cond_0

    invoke-direct {p0, p2}, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->createMesh(Landroid/filterfw/core/FilterContext;)V

    :cond_0
    return-void
.end method

.method getEffectAspectRatio()[F
    .locals 2

    const/4 v1, 0x2

    iget v0, p0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mCurrentEffect:I

    packed-switch v0, :pswitch_data_0

    new-array v0, v1, [F

    fill-array-data v0, :array_0

    :goto_0
    return-object v0

    :pswitch_0
    new-array v0, v1, [F

    fill-array-data v0, :array_1

    goto :goto_0

    :pswitch_1
    new-array v0, v1, [F

    fill-array-data v0, :array_2

    goto :goto_0

    :pswitch_2
    new-array v0, v1, [F

    fill-array-data v0, :array_3

    goto :goto_0

    :pswitch_3
    new-array v0, v1, [F

    fill-array-data v0, :array_4

    goto :goto_0

    :pswitch_4
    new-array v0, v1, [F

    fill-array-data v0, :array_5

    goto :goto_0

    :pswitch_5
    new-array v0, v1, [F

    fill-array-data v0, :array_6

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_5
        :pswitch_2
        :pswitch_4
    .end packed-switch

    :array_0
    .array-data 4
        0x3f800000
        0x3f800000
    .end array-data

    :array_1
    .array-data 4
        0x3f800000
        0x3f800000
    .end array-data

    :array_2
    .array-data 4
        0x3f800000
        0x3f19999a
    .end array-data

    :array_3
    .array-data 4
        0x3f800000
        0x3f4ccccd
    .end array-data

    :array_4
    .array-data 4
        0x3f800000
        0x3f333333
    .end array-data

    :array_5
    .array-data 4
        0x3f800000
        0x3ecccccd
    .end array-data

    :array_6
    .array-data 4
        0x3f800000
        0x3f19999a
    .end array-data
.end method

.method public getOutputFormat(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/FrameFormat;
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/filterfw/core/FrameFormat;

    return-object p2
.end method

.method public process(Landroid/filterfw/core/FilterContext;)V
    .locals 49
    .param p1    # Landroid/filterfw/core/FilterContext;

    const-string v44, "image"

    move-object/from16 v0, p0

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/filterfw/core/Frame;->getTimestamp()J

    move-result-wide v44

    move-wide/from16 v0, v44

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mCurrentTimeStamp:J

    invoke-virtual/range {v22 .. v22}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    move-result v43

    invoke-virtual/range {v23 .. v23}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    move-result v20

    move/from16 v0, v43

    move/from16 v1, v20

    if-le v0, v1, :cond_4

    const/16 v44, 0x2

    move/from16 v0, v44

    new-array v9, v0, [F

    const/16 v44, 0x0

    const/high16 v45, 0x3f800000

    aput v45, v9, v44

    const/16 v44, 0x1

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v45, v0

    move/from16 v0, v43

    int-to-float v0, v0

    move/from16 v46, v0

    div-float v45, v45, v46

    aput v45, v9, v44

    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v44, v0

    if-nez v44, :cond_5

    invoke-direct/range {p0 .. p1}, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->createProgram(Landroid/filterfw/core/FilterContext;)V

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mAspect:[F

    invoke-direct/range {p0 .. p1}, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->createMesh(Landroid/filterfw/core/FilterContext;)V

    :cond_0
    :goto_1
    const-string v44, "faces"

    move-object/from16 v0, p0

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/filterfw/core/Frame;->getObjectValue()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/google/android/filterpacks/facedetect/FaceMeta;

    const/16 v44, 0x2

    move/from16 v0, v44

    new-array v13, v0, [F

    const/16 v44, 0x2

    move/from16 v0, v44

    new-array v0, v0, [F

    move-object/from16 v42, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->count()I

    move-result v17

    if-lez v17, :cond_d

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mDistortionAmount:F

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mEnableAnimation:Z

    move/from16 v44, v0

    if-eqz v44, :cond_1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mAnimateCurrent:F

    move/from16 v44, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mDistortionAmount:F

    move/from16 v45, v0

    cmpg-float v44, v44, v45

    if-gez v44, :cond_1

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mCurrentTimeStamp:J

    move-wide/from16 v44, v0

    const-wide/16 v46, 0x0

    cmp-long v44, v44, v46

    if-lez v44, :cond_8

    const/high16 v24, 0x44fa0000

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mAnimateCurrent:F

    move/from16 v44, v0

    const/16 v45, 0x0

    cmpl-float v44, v44, v45

    if-nez v44, :cond_7

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mCurrentTimeStamp:J

    move-wide/from16 v44, v0

    move-wide/from16 v0, v44

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mAnimationStartTimeStamp:J

    const v44, 0x3a83126f

    move/from16 v0, v44

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mAnimateCurrent:F

    :goto_2
    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mAnimateCurrent:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mDistortionAmount:F

    move/from16 v44, v0

    cmpl-float v44, v5, v44

    if-lez v44, :cond_1

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mDistortionAmount:F

    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    move-result-object v44

    invoke-virtual/range {v22 .. v22}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    move-result-object v11

    move-object/from16 v0, v22

    invoke-virtual {v11, v0}, Landroid/filterfw/core/Frame;->setDataFromFrame(Landroid/filterfw/core/Frame;)V

    const/4 v12, 0x0

    const/16 v44, 0x1

    move/from16 v0, v17

    move/from16 v1, v44

    if-le v0, v1, :cond_2

    invoke-virtual/range {p1 .. p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    move-result-object v44

    invoke-virtual/range {v22 .. v22}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    move-result-object v12

    :cond_2
    move-object/from16 v36, v22

    move-object/from16 v29, v11

    const/16 v21, 0x0

    :goto_3
    move/from16 v0, v21

    move/from16 v1, v17

    if-ge v0, v1, :cond_b

    const/16 v44, 0x1

    move/from16 v0, v17

    move/from16 v1, v44

    if-le v0, v1, :cond_3

    if-lez v21, :cond_3

    rem-int/lit8 v44, v21, 0x2

    const/16 v45, 0x1

    move/from16 v0, v44

    move/from16 v1, v45

    if-ne v0, v1, :cond_9

    move-object/from16 v36, v11

    move-object/from16 v29, v12

    move-object/from16 v0, v36

    invoke-virtual {v12, v0}, Landroid/filterfw/core/Frame;->setDataFromFrame(Landroid/filterfw/core/Frame;)V

    :cond_3
    :goto_4
    const/16 v44, 0x2

    move/from16 v0, v44

    new-array v0, v0, [F

    move-object/from16 v26, v0

    const/16 v44, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getLeftEyeX(I)F

    move-result v45

    aput v45, v26, v44

    const/16 v44, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getLeftEyeY(I)F

    move-result v45

    aput v45, v26, v44

    const/16 v44, 0x2

    move/from16 v0, v44

    new-array v0, v0, [F

    move-object/from16 v33, v0

    const/16 v44, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getRightEyeX(I)F

    move-result v45

    aput v45, v33, v44

    const/16 v44, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getRightEyeY(I)F

    move-result v45

    aput v45, v33, v44

    const/16 v44, 0x2

    move/from16 v0, v44

    new-array v0, v0, [F

    move-object/from16 v28, v0

    const/16 v44, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getMouthX(I)F

    move-result v45

    aput v45, v28, v44

    const/16 v44, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getMouthY(I)F

    move-result v45

    aput v45, v28, v44

    const/16 v44, 0x1

    aget v44, v33, v44

    const/16 v45, 0x1

    aget v45, v26, v45

    sub-float v44, v44, v45

    const/16 v45, 0x1

    aget v45, v9, v45

    mul-float v44, v44, v45

    move/from16 v0, v44

    float-to-double v0, v0

    move-wide/from16 v44, v0

    const/16 v46, 0x0

    aget v46, v33, v46

    const/16 v47, 0x0

    aget v47, v26, v47

    sub-float v46, v46, v47

    const/16 v47, 0x0

    aget v47, v9, v47

    mul-float v46, v46, v47

    move/from16 v0, v46

    float-to-double v0, v0

    move-wide/from16 v46, v0

    invoke-static/range {v44 .. v47}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v44

    move-wide/from16 v0, v44

    double-to-float v7, v0

    const/16 v44, 0x0

    aget v44, v33, v44

    const/16 v45, 0x0

    aget v45, v26, v45

    add-float v44, v44, v45

    const/high16 v45, 0x40000000

    div-float v44, v44, v45

    const/16 v45, 0x0

    aget v45, v28, v45

    sub-float v40, v44, v45

    const/16 v44, 0x1

    aget v44, v33, v44

    const/16 v45, 0x1

    aget v45, v26, v45

    add-float v44, v44, v45

    const/high16 v45, 0x40000000

    div-float v44, v44, v45

    const/16 v45, 0x1

    aget v45, v28, v45

    sub-float v41, v44, v45

    move/from16 v0, v41

    float-to-double v0, v0

    move-wide/from16 v44, v0

    move/from16 v0, v40

    float-to-double v0, v0

    move-wide/from16 v46, v0

    invoke-static/range {v44 .. v47}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v44

    move-wide/from16 v0, v44

    double-to-float v8, v0

    float-to-double v0, v8

    move-wide/from16 v44, v0

    const-wide v46, 0x3ff921fb54442d18L

    sub-double v44, v44, v46

    move-wide/from16 v0, v44

    double-to-float v8, v0

    const/16 v44, 0x0

    aget v44, v26, v44

    const/16 v45, 0x0

    aget v45, v33, v45

    sub-float v44, v44, v45

    const/16 v45, 0x0

    aget v45, v9, v45

    mul-float v14, v44, v45

    const/16 v44, 0x1

    aget v44, v26, v44

    const/16 v45, 0x1

    aget v45, v33, v45

    sub-float v44, v44, v45

    const/16 v45, 0x1

    aget v45, v9, v45

    mul-float v15, v44, v45

    mul-float v44, v14, v14

    mul-float v45, v15, v15

    add-float v44, v44, v45

    move/from16 v0, v44

    float-to-double v0, v0

    move-wide/from16 v44, v0

    invoke-static/range {v44 .. v45}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v44

    move-wide/from16 v0, v44

    double-to-float v0, v0

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mCurrentEffect:I

    move/from16 v44, v0

    const/16 v45, 0x1

    move/from16 v0, v44

    move/from16 v1, v45

    if-ne v0, v1, :cond_a

    const/16 v44, 0x1

    aget v44, v26, v44

    sub-float v44, v44, v27

    const/16 v45, 0x1

    aget v45, v33, v45

    sub-float v45, v45, v27

    invoke-static/range {v44 .. v45}, Ljava/lang/Math;->min(FF)F

    move-result v39

    const/16 v44, 0x0

    aget v44, v26, v44

    sub-float v44, v44, v27

    const/16 v45, 0x0

    aget v45, v33, v45

    sub-float v45, v45, v27

    invoke-static/range {v44 .. v45}, Ljava/lang/Math;->min(FF)F

    move-result v25

    const/16 v44, 0x1

    aget v44, v26, v44

    add-float v44, v44, v27

    const/16 v45, 0x1

    aget v45, v33, v45

    add-float v45, v45, v27

    invoke-static/range {v44 .. v45}, Ljava/lang/Math;->max(FF)F

    move-result v10

    const/16 v44, 0x0

    aget v44, v26, v44

    add-float v44, v44, v27

    const/16 v45, 0x0

    aget v45, v33, v45

    add-float v45, v45, v27

    invoke-static/range {v44 .. v45}, Ljava/lang/Math;->max(FF)F

    move-result v32

    new-instance v30, Landroid/filterfw/geometry/Rectangle;

    sub-float v44, v32, v25

    sub-float v45, v10, v39

    move-object/from16 v0, v30

    move/from16 v1, v25

    move/from16 v2, v39

    move/from16 v3, v44

    move/from16 v4, v45

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/filterfw/geometry/Rectangle;-><init>(FFFF)V

    const/16 v44, 0x0

    const/16 v45, 0x0

    move-object/from16 v0, v30

    move/from16 v1, v44

    move/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Landroid/filterfw/geometry/Rectangle;->translated(FF)Landroid/filterfw/geometry/Quad;

    move-result-object v31

    const/16 v44, 0x0

    const/16 v45, 0x0

    aget v45, v26, v45

    const/16 v46, 0x0

    aget v46, v33, v46

    add-float v45, v45, v46

    const/high16 v46, 0x40000000

    div-float v45, v45, v46

    aput v45, v13, v44

    const/16 v44, 0x1

    const/16 v45, 0x1

    aget v45, v26, v45

    const/16 v46, 0x1

    aget v46, v33, v46

    add-float v45, v45, v46

    const/high16 v46, 0x40000000

    div-float v45, v45, v46

    aput v45, v13, v44

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v44, v0

    const-string v45, "center"

    move-object/from16 v0, v44

    move-object/from16 v1, v45

    invoke-virtual {v0, v1, v13}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    const/16 v44, 0x4

    move/from16 v0, v44

    new-array v0, v0, [F

    move-object/from16 v34, v0

    const/16 v44, 0x0

    float-to-double v0, v7

    move-wide/from16 v45, v0

    invoke-static/range {v45 .. v46}, Ljava/lang/Math;->cos(D)D

    move-result-wide v45

    move-wide/from16 v0, v45

    double-to-float v0, v0

    move/from16 v45, v0

    aput v45, v34, v44

    const/16 v44, 0x1

    float-to-double v0, v7

    move-wide/from16 v45, v0

    invoke-static/range {v45 .. v46}, Ljava/lang/Math;->sin(D)D

    move-result-wide v45

    move-wide/from16 v0, v45

    double-to-float v0, v0

    move/from16 v45, v0

    aput v45, v34, v44

    const/16 v44, 0x2

    float-to-double v0, v7

    move-wide/from16 v45, v0

    invoke-static/range {v45 .. v46}, Ljava/lang/Math;->sin(D)D

    move-result-wide v45

    move-wide/from16 v0, v45

    neg-double v0, v0

    move-wide/from16 v45, v0

    move-wide/from16 v0, v45

    double-to-float v0, v0

    move/from16 v45, v0

    aput v45, v34, v44

    const/16 v44, 0x3

    float-to-double v0, v7

    move-wide/from16 v45, v0

    invoke-static/range {v45 .. v46}, Ljava/lang/Math;->cos(D)D

    move-result-wide v45

    move-wide/from16 v0, v45

    double-to-float v0, v0

    move/from16 v45, v0

    aput v45, v34, v44

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v44, v0

    const-string v45, "rotate"

    move-object/from16 v0, v44

    move-object/from16 v1, v45

    move-object/from16 v2, v34

    invoke-virtual {v0, v1, v2}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    const/16 v44, 0x2

    move/from16 v0, v44

    new-array v0, v0, [F

    move-object/from16 v35, v0

    const/16 v44, 0x0

    const/16 v45, 0x0

    aget v45, v9, v45

    div-float v45, v27, v45

    aput v45, v35, v44

    const/16 v44, 0x1

    const/16 v45, 0x1

    aget v45, v9, v45

    div-float v45, v27, v45

    aput v45, v35, v44

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v44, v0

    const-string v45, "weight"

    move-object/from16 v0, v44

    move-object/from16 v1, v45

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v44, v0

    const-string v45, "amount"

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v46

    invoke-virtual/range {v44 .. v46}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v44, v0

    move-object/from16 v0, v44

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/filterfw/core/ShaderProgram;->setSourceRegion(Landroid/filterfw/geometry/Quad;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v44, v0

    move-object/from16 v0, v44

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/filterfw/core/ShaderProgram;->setTargetRegion(Landroid/filterfw/geometry/Quad;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v44, v0

    move-object/from16 v0, v44

    move-object/from16 v1, v36

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Landroid/filterfw/core/ShaderProgram;->process(Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    :goto_5
    add-int/lit8 v21, v21, 0x1

    goto/16 :goto_3

    :cond_4
    const/16 v44, 0x2

    move/from16 v0, v44

    new-array v9, v0, [F

    const/16 v44, 0x0

    move/from16 v0, v43

    int-to-float v0, v0

    move/from16 v45, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v46, v0

    div-float v45, v45, v46

    aput v45, v9, v44

    const/16 v44, 0x1

    const/high16 v45, 0x3f800000

    aput v45, v9, v44

    goto/16 :goto_0

    :cond_5
    const/16 v44, 0x0

    aget v44, v9, v44

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mAspect:[F

    move-object/from16 v45, v0

    const/16 v46, 0x0

    aget v45, v45, v46

    cmpl-float v44, v44, v45

    if-nez v44, :cond_6

    const/16 v44, 0x1

    aget v44, v9, v44

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mAspect:[F

    move-object/from16 v45, v0

    const/16 v46, 0x1

    aget v45, v45, v46

    cmpl-float v44, v44, v45

    if-eqz v44, :cond_0

    :cond_6
    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mAspect:[F

    invoke-direct/range {p0 .. p1}, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->createMesh(Landroid/filterfw/core/FilterContext;)V

    goto/16 :goto_1

    :cond_7
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mCurrentTimeStamp:J

    move-wide/from16 v44, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mAnimationStartTimeStamp:J

    move-wide/from16 v46, v0

    sub-long v37, v44, v46

    const-wide/32 v44, 0xf4240

    div-long v37, v37, v44

    move-wide/from16 v0, v37

    long-to-float v0, v0

    move/from16 v44, v0

    const/high16 v45, 0x44fa0000

    div-float v44, v44, v45

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mDistortionAmount:F

    move/from16 v45, v0

    mul-float v44, v44, v45

    move/from16 v0, v44

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mAnimateCurrent:F

    goto/16 :goto_2

    :cond_8
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mAnimateCurrent:F

    move/from16 v44, v0

    const v45, 0x3cf5c28f

    add-float v44, v44, v45

    move/from16 v0, v44

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mAnimateCurrent:F

    goto/16 :goto_2

    :cond_9
    move-object/from16 v36, v12

    move-object/from16 v29, v11

    move-object/from16 v0, v36

    invoke-virtual {v11, v0}, Landroid/filterfw/core/Frame;->setDataFromFrame(Landroid/filterfw/core/Frame;)V

    goto/16 :goto_4

    :cond_a
    move v6, v8

    const/high16 v16, 0x3f800000

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mCurrentEffect:I

    move/from16 v44, v0

    packed-switch v44, :pswitch_data_0

    :pswitch_0
    new-instance v44, Ljava/lang/RuntimeException;

    new-instance v45, Ljava/lang/StringBuilder;

    invoke-direct/range {v45 .. v45}, Ljava/lang/StringBuilder;-><init>()V

    const-string v46, "Undefined effect: "

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mCurrentEffect:I

    move/from16 v46, v0

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    invoke-direct/range {v44 .. v45}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v44

    :pswitch_1
    const/16 v44, 0x0

    const/high16 v45, 0x3f000000

    const/16 v46, 0x0

    aget v46, v26, v46

    mul-float v45, v45, v46

    const/high16 v46, 0x3f000000

    const/16 v47, 0x0

    aget v47, v33, v47

    mul-float v46, v46, v47

    add-float v45, v45, v46

    const v46, 0x38d1b717

    const/16 v47, 0x0

    aget v47, v28, v47

    mul-float v46, v46, v47

    add-float v45, v45, v46

    aput v45, v13, v44

    const/16 v44, 0x1

    const/high16 v45, 0x3f000000

    const/16 v46, 0x1

    aget v46, v26, v46

    mul-float v45, v45, v46

    const/high16 v46, 0x3f000000

    const/16 v47, 0x1

    aget v47, v33, v47

    mul-float v46, v46, v47

    add-float v45, v45, v46

    const v46, 0x38d1b717

    const/16 v47, 0x1

    aget v47, v28, v47

    mul-float v46, v46, v47

    add-float v45, v45, v46

    aput v45, v13, v44

    const/high16 v16, 0x3f800000

    move v6, v7

    :goto_6
    const/16 v44, 0x0

    mul-float v45, v16, v27

    const/16 v46, 0x0

    aget v46, v9, v46

    div-float v45, v45, v46

    aput v45, v42, v44

    const/16 v44, 0x1

    mul-float v45, v16, v27

    const/16 v46, 0x1

    aget v46, v9, v46

    div-float v45, v45, v46

    aput v45, v42, v44

    new-instance v30, Landroid/filterfw/geometry/Rectangle;

    const/high16 v44, -0x40800000

    const/16 v45, 0x0

    aget v45, v42, v45

    mul-float v44, v44, v45

    const/high16 v45, -0x40800000

    const/16 v46, 0x1

    aget v46, v42, v46

    mul-float v45, v45, v46

    const/high16 v46, 0x40000000

    const/16 v47, 0x0

    aget v47, v42, v47

    mul-float v46, v46, v47

    const/high16 v47, 0x40000000

    const/16 v48, 0x1

    aget v48, v42, v48

    mul-float v47, v47, v48

    move-object/from16 v0, v30

    move/from16 v1, v44

    move/from16 v2, v45

    move/from16 v3, v46

    move/from16 v4, v47

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/filterfw/geometry/Rectangle;-><init>(FFFF)V

    const/16 v44, 0x0

    aget v44, v13, v44

    const/16 v45, 0x1

    aget v45, v13, v45

    move-object/from16 v0, v30

    move/from16 v1, v44

    move/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Landroid/filterfw/geometry/Rectangle;->translated(FF)Landroid/filterfw/geometry/Quad;

    move-result-object v31

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mIdentityProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v44, v0

    const-string v45, "center"

    move-object/from16 v0, v44

    move-object/from16 v1, v45

    invoke-virtual {v0, v1, v13}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mIdentityProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v44, v0

    const-string v45, "weight"

    move-object/from16 v0, v44

    move-object/from16 v1, v45

    move-object/from16 v2, v42

    invoke-virtual {v0, v1, v2}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    const/16 v44, 0x4

    move/from16 v0, v44

    new-array v0, v0, [F

    move-object/from16 v34, v0

    const/16 v44, 0x0

    float-to-double v0, v6

    move-wide/from16 v45, v0

    invoke-static/range {v45 .. v46}, Ljava/lang/Math;->cos(D)D

    move-result-wide v45

    move-wide/from16 v0, v45

    double-to-float v0, v0

    move/from16 v45, v0

    aput v45, v34, v44

    const/16 v44, 0x1

    float-to-double v0, v6

    move-wide/from16 v45, v0

    invoke-static/range {v45 .. v46}, Ljava/lang/Math;->sin(D)D

    move-result-wide v45

    move-wide/from16 v0, v45

    double-to-float v0, v0

    move/from16 v45, v0

    aput v45, v34, v44

    const/16 v44, 0x2

    float-to-double v0, v6

    move-wide/from16 v45, v0

    invoke-static/range {v45 .. v46}, Ljava/lang/Math;->sin(D)D

    move-result-wide v45

    move-wide/from16 v0, v45

    neg-double v0, v0

    move-wide/from16 v45, v0

    move-wide/from16 v0, v45

    double-to-float v0, v0

    move/from16 v45, v0

    aput v45, v34, v44

    const/16 v44, 0x3

    float-to-double v0, v6

    move-wide/from16 v45, v0

    invoke-static/range {v45 .. v46}, Ljava/lang/Math;->cos(D)D

    move-result-wide v45

    move-wide/from16 v0, v45

    double-to-float v0, v0

    move/from16 v45, v0

    aput v45, v34, v44

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mIdentityProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v44, v0

    const-string v45, "rotate"

    move-object/from16 v0, v44

    move-object/from16 v1, v45

    move-object/from16 v2, v34

    invoke-virtual {v0, v1, v2}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mIdentityProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v44, v0

    const-string v45, "amount"

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v46

    invoke-virtual/range {v44 .. v46}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mIdentityProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v44, v0

    move-object/from16 v0, v44

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/filterfw/core/ShaderProgram;->setSourceRegion(Landroid/filterfw/geometry/Quad;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mIdentityProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v44, v0

    move-object/from16 v0, v44

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/filterfw/core/ShaderProgram;->setTargetRegion(Landroid/filterfw/geometry/Quad;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mIdentityProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v44, v0

    move-object/from16 v0, v44

    move-object/from16 v1, v36

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Landroid/filterfw/core/ShaderProgram;->process(Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    goto/16 :goto_5

    :pswitch_2
    const/16 v44, 0x0

    const/16 v45, 0x0

    aget v45, v28, v45

    const v46, 0x3d75c28f

    mul-float v46, v46, v40

    sub-float v45, v45, v46

    aput v45, v13, v44

    const/16 v44, 0x1

    const/16 v45, 0x1

    aget v45, v28, v45

    const v46, 0x3d75c28f

    mul-float v46, v46, v41

    sub-float v45, v45, v46

    aput v45, v13, v44

    const v16, 0x3f333333

    goto/16 :goto_6

    :pswitch_3
    const/16 v44, 0x0

    const/16 v45, 0x0

    aget v45, v28, v45

    const v46, 0x3d75c28f

    mul-float v46, v46, v40

    sub-float v45, v45, v46

    aput v45, v13, v44

    const/16 v44, 0x1

    const/16 v45, 0x1

    aget v45, v28, v45

    const v46, 0x3d75c28f

    mul-float v46, v46, v41

    sub-float v45, v45, v46

    aput v45, v13, v44

    const/high16 v16, 0x40200000

    goto/16 :goto_6

    :pswitch_4
    const/16 v44, 0x0

    const/high16 v45, 0x3e800000

    const/16 v46, 0x0

    aget v46, v26, v46

    mul-float v45, v45, v46

    const/high16 v46, 0x3e800000

    const/16 v47, 0x0

    aget v47, v33, v47

    mul-float v46, v46, v47

    add-float v45, v45, v46

    const/high16 v46, 0x3f000000

    const/16 v47, 0x0

    aget v47, v28, v47

    mul-float v46, v46, v47

    add-float v45, v45, v46

    aput v45, v13, v44

    const/16 v44, 0x1

    const/high16 v45, 0x3e800000

    const/16 v46, 0x1

    aget v46, v26, v46

    mul-float v45, v45, v46

    const/high16 v46, 0x3e800000

    const/16 v47, 0x1

    aget v47, v33, v47

    mul-float v46, v46, v47

    add-float v45, v45, v46

    const/high16 v46, 0x3f000000

    const/16 v47, 0x1

    aget v47, v28, v47

    mul-float v46, v46, v47

    add-float v45, v45, v46

    aput v45, v13, v44

    const v16, 0x3f8ccccd

    goto/16 :goto_6

    :pswitch_5
    const/16 v44, 0x0

    const/high16 v45, 0x3e800000

    const/16 v46, 0x0

    aget v46, v26, v46

    mul-float v45, v45, v46

    const/high16 v46, 0x3e800000

    const/16 v47, 0x0

    aget v47, v33, v47

    mul-float v46, v46, v47

    add-float v45, v45, v46

    const/high16 v46, 0x3f000000

    const/16 v47, 0x0

    aget v47, v28, v47

    mul-float v46, v46, v47

    add-float v45, v45, v46

    aput v45, v13, v44

    const/16 v44, 0x1

    const/high16 v45, 0x3e800000

    const/16 v46, 0x1

    aget v46, v26, v46

    mul-float v45, v45, v46

    const/high16 v46, 0x3e800000

    const/16 v47, 0x1

    aget v47, v33, v47

    mul-float v46, v46, v47

    add-float v45, v45, v46

    const/high16 v46, 0x3f000000

    const/16 v47, 0x1

    aget v47, v28, v47

    mul-float v46, v46, v47

    add-float v45, v45, v46

    aput v45, v13, v44

    const/high16 v16, 0x40000000

    goto/16 :goto_6

    :cond_b
    const-string v44, "outimage"

    move-object/from16 v0, p0

    move-object/from16 v1, v44

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    invoke-virtual {v11}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    if-eqz v12, :cond_c

    invoke-virtual {v12}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    :cond_c
    :goto_7
    return-void

    :cond_d
    const-string v44, "outimage"

    move-object/from16 v0, p0

    move-object/from16 v1, v44

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    goto :goto_7

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_1
    .end packed-switch
.end method

.method public setupPorts()V
    .locals 4

    const/4 v2, 0x3

    invoke-static {v2, v2}, Landroid/filterfw/format/ImageFormat;->create(II)Landroid/filterfw/core/MutableFrameFormat;

    move-result-object v1

    const-class v2, Lcom/google/android/filterpacks/facedetect/FaceMeta;

    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/filterfw/format/ObjectFormat;->fromClass(Ljava/lang/Class;I)Landroid/filterfw/core/MutableFrameFormat;

    move-result-object v0

    const-string v2, "image"

    invoke-virtual {p0, v2, v1}, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->addMaskedInputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    const-string v2, "faces"

    invoke-virtual {p0, v2, v0}, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->addMaskedInputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    const-string v2, "outimage"

    const-string v3, "image"

    invoke-virtual {p0, v2, v3}, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->addOutputBasedOnInput(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
