.class public Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;
.super Landroid/filterfw/core/Filter;
.source "GoofyRenderFilter.java"


# static fields
.field private static final BIG_EYES:I = 0x1

.field private static final BIG_MOUTH:I = 0x2

.field private static final BIG_NOSE:I = 0x4

.field private static final NUM_EFFECTS:I = 0x6

.field private static final SMALL_EYES:I = 0x5

.field private static final SMALL_MOUTH:I = 0x3

.field private static final SQUEEZE:I = 0x0

.field private static final TAG:Ljava/lang/String; = "GoofyRenderFilter"


# instance fields
.field private mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

.field private final mBigEyesShader:Ljava/lang/String;

.field private mCurrentEffect:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "currentEffect"
    .end annotation
.end field

.field private mDistortionAmount:F
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "distortionAmount"
    .end annotation
.end field

.field private mGoofyProgram:Landroid/filterfw/core/ShaderProgram;

.field private final mGoofyShader:Ljava/lang/String;

.field private mShrinkFunc:Z

.field private mTableFrame:Landroid/filterfw/core/Frame;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mCurrentEffect:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mDistortionAmount:F

    const-string v0, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform sampler2D tex_sampler_1;\nuniform vec2 center;\nuniform vec2 weight;\nuniform float dist_offset;\nuniform float dist_mult;\nuniform bool use_shrink;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec2 point = v_texcoord - center;\n  vec2 spoint;\n  spoint = weight * point;\n  float dist = length(spoint) * dist_mult + dist_offset;\n  vec4 scale_byte = texture2D(tex_sampler_1, vec2(dist, 0.5));\n  float scale = scale_byte.g + scale_byte.r * 0.00390625;\n  if (use_shrink) {\n    scale = 1.0 + scale;\n  } else {\n    scale = 1.0 - scale;\n  }\n  if (dist >= 1.0) { \n     scale = 1.0;\n  } \n  gl_FragColor = texture2D(tex_sampler_0, center + scale * point);\n}\n"

    iput-object v0, p0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mGoofyShader:Ljava/lang/String;

    const-string v0, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform sampler2D tex_sampler_1;\nuniform vec2 left_eye;\nuniform vec2 right_eye;\nuniform vec2 scale;\nuniform float dist_offset;\nuniform float dist_mult;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec2 left_eye_offset = (v_texcoord - left_eye); \n  float left_eye_dist = length(left_eye_offset * scale); \n  vec2 right_eye_offset = (v_texcoord - right_eye); \n  float right_eye_dist = length(right_eye_offset * scale); \n  float dist;\n  vec2 offset;\n  vec2 center;\n  if (left_eye_dist < 1.0 || right_eye_dist < 1.0){\n    float dist_left = left_eye_dist * dist_mult + dist_offset;\n    vec4 value_byte = texture2D(tex_sampler_1, vec2(dist_left, 0.5));\n    float value_left = (value_byte.g + value_byte.r * 0.00390625);\n    vec4 color_left = texture2D(tex_sampler_0,\n            left_eye + (1.0 - value_left) * left_eye_offset);\n    float dist_right = right_eye_dist * dist_mult + dist_offset;\n    value_byte = texture2D(tex_sampler_1, vec2(dist_right, 0.5));\n    float value_right = (value_byte.g + value_byte.r * 0.00390625);\n    vec4 color_right = texture2D(tex_sampler_0,\n            right_eye + (1.0 - value_right) * right_eye_offset);\n    float alpha = value_left / (value_left + value_right);\n    gl_FragColor = mix(color_right, color_left, alpha);\n  } else {\n    gl_FragColor = texture2D(tex_sampler_0, v_texcoord);\n  }\n}\n"

    iput-object v0, p0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mBigEyesShader:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mShrinkFunc:Z

    return-void
.end method

.method private createLookupTable(Landroid/filterfw/core/FilterContext;)V
    .locals 19
    .param p1    # Landroid/filterfw/core/FilterContext;

    const/16 v9, 0x7d0

    const v5, 0x477fff00

    const v4, 0x40490fda

    const/16 v11, 0x7d0

    new-array v1, v11, [I

    const/high16 v6, 0x3f800000

    const/4 v3, 0x0

    :goto_0
    const/16 v11, 0x7d0

    if-ge v3, v11, :cond_4

    int-to-float v11, v3

    const/high16 v12, 0x44fa0000

    div-float v2, v11, v12

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mCurrentEffect:I

    packed-switch v11, :pswitch_data_0

    :goto_1
    const v11, 0x477fff00

    mul-float/2addr v11, v10

    float-to-int v11, v11

    aput v11, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :pswitch_0
    const v7, 0x3e99999a

    if-nez v3, :cond_0

    const-wide/high16 v11, 0x4090000000000000L

    invoke-static {v11, v12}, Ljava/lang/Math;->log(D)D

    move-result-wide v11

    float-to-double v13, v7

    mul-double/2addr v11, v13

    float-to-double v13, v7

    mul-double/2addr v11, v13

    invoke-static {v11, v12}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v11

    double-to-float v6, v11

    :cond_0
    const/high16 v11, 0x3f800000

    cmpl-float v11, v6, v11

    if-lez v11, :cond_1

    mul-float/2addr v2, v6

    :cond_1
    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mDistortionAmount:F

    const v12, 0x3f4ccccd

    mul-float/2addr v11, v12

    const/high16 v12, 0x40000000

    neg-float v13, v2

    mul-float/2addr v13, v2

    mul-float v14, v7, v7

    div-float/2addr v13, v14

    float-to-double v13, v13

    invoke-static {v13, v14}, Ljava/lang/Math;->exp(D)D

    move-result-wide v13

    double-to-float v13, v13

    sub-float/2addr v12, v13

    const/high16 v13, 0x40000000

    const/high16 v14, 0x3f800000

    const/high16 v15, 0x3e800000

    add-float/2addr v15, v2

    float-to-double v15, v15

    const-wide/high16 v17, 0x4010000000000000L

    invoke-static/range {v15 .. v18}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v15

    neg-double v15, v15

    const-wide/high16 v17, 0x4000000000000000L

    mul-double v15, v15, v17

    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->exp(D)D

    move-result-wide v15

    double-to-float v15, v15

    add-float/2addr v14, v15

    div-float/2addr v13, v14

    sub-float/2addr v12, v13

    mul-float v10, v11, v12

    const/4 v11, 0x1

    move-object/from16 v0, p0

    iput-boolean v11, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mShrinkFunc:Z

    goto :goto_1

    :pswitch_1
    const v7, 0x3f333333

    if-nez v3, :cond_2

    const-wide/high16 v11, 0x4090000000000000L

    invoke-static {v11, v12}, Ljava/lang/Math;->log(D)D

    move-result-wide v11

    float-to-double v13, v7

    mul-double/2addr v11, v13

    float-to-double v13, v7

    mul-double/2addr v11, v13

    invoke-static {v11, v12}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v11

    double-to-float v6, v11

    :cond_2
    const/high16 v11, 0x3f800000

    cmpl-float v11, v6, v11

    if-lez v11, :cond_3

    mul-float/2addr v2, v6

    :cond_3
    const v11, 0x3f266666

    move-object/from16 v0, p0

    iget v12, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mDistortionAmount:F

    mul-float/2addr v11, v12

    neg-float v12, v2

    mul-float/2addr v12, v2

    mul-float v13, v7, v7

    div-float/2addr v12, v13

    float-to-double v12, v12

    invoke-static {v12, v13}, Ljava/lang/Math;->exp(D)D

    move-result-wide v12

    double-to-float v12, v12

    mul-float v10, v11, v12

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iput-boolean v11, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mShrinkFunc:Z

    goto/16 :goto_1

    :cond_4
    const/16 v11, 0x7d0

    const/4 v12, 0x1

    const/4 v13, 0x3

    const/4 v14, 0x3

    invoke-static {v11, v12, v13, v14}, Landroid/filterfw/format/ImageFormat;->create(IIII)Landroid/filterfw/core/MutableFrameFormat;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mTableFrame:Landroid/filterfw/core/Frame;

    if-eqz v11, :cond_5

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mTableFrame:Landroid/filterfw/core/Frame;

    invoke-virtual {v11}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    :cond_5
    invoke-virtual/range {p1 .. p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    move-result-object v11

    invoke-virtual {v11, v8}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    move-result-object v11

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mTableFrame:Landroid/filterfw/core/Frame;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mTableFrame:Landroid/filterfw/core/Frame;

    invoke-virtual {v11, v1}, Landroid/filterfw/core/Frame;->setInts([I)V

    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mCurrentEffect:I

    const/4 v12, 0x1

    if-ne v11, v12, :cond_6

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    const-string v12, "dist_offset"

    const v13, 0x3a03126f

    invoke-static {v13}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    const-string v12, "dist_mult"

    const v13, 0x3f7fdf3b

    invoke-static {v13}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    :goto_2
    return-void

    :cond_6
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mGoofyProgram:Landroid/filterfw/core/ShaderProgram;

    const-string v12, "dist_offset"

    const v13, 0x3a03126f

    invoke-static {v13}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mGoofyProgram:Landroid/filterfw/core/ShaderProgram;

    const-string v12, "dist_mult"

    const v13, 0x3f7fdf3b

    invoke-static {v13}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mGoofyProgram:Landroid/filterfw/core/ShaderProgram;

    const-string v12, "use_shrink"

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mShrinkFunc:Z

    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public fieldPortValueUpdated(Ljava/lang/String;Landroid/filterfw/core/FilterContext;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/filterfw/core/FilterContext;

    iget-object v0, p0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mGoofyProgram:Landroid/filterfw/core/ShaderProgram;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    if-eqz v0, :cond_0

    invoke-direct {p0, p2}, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->createLookupTable(Landroid/filterfw/core/FilterContext;)V

    :cond_0
    return-void
.end method

.method public getOutputFormat(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/FrameFormat;
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/filterfw/core/FrameFormat;

    return-object p2
.end method

.method protected prepare(Landroid/filterfw/core/FilterContext;)V
    .locals 2
    .param p1    # Landroid/filterfw/core/FilterContext;

    new-instance v0, Landroid/filterfw/core/ShaderProgram;

    const-string v1, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform sampler2D tex_sampler_1;\nuniform vec2 center;\nuniform vec2 weight;\nuniform float dist_offset;\nuniform float dist_mult;\nuniform bool use_shrink;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec2 point = v_texcoord - center;\n  vec2 spoint;\n  spoint = weight * point;\n  float dist = length(spoint) * dist_mult + dist_offset;\n  vec4 scale_byte = texture2D(tex_sampler_1, vec2(dist, 0.5));\n  float scale = scale_byte.g + scale_byte.r * 0.00390625;\n  if (use_shrink) {\n    scale = 1.0 + scale;\n  } else {\n    scale = 1.0 - scale;\n  }\n  if (dist >= 1.0) { \n     scale = 1.0;\n  } \n  gl_FragColor = texture2D(tex_sampler_0, center + scale * point);\n}\n"

    invoke-direct {v0, p1, v1}, Landroid/filterfw/core/ShaderProgram;-><init>(Landroid/filterfw/core/FilterContext;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mGoofyProgram:Landroid/filterfw/core/ShaderProgram;

    new-instance v0, Landroid/filterfw/core/ShaderProgram;

    const-string v1, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform sampler2D tex_sampler_1;\nuniform vec2 left_eye;\nuniform vec2 right_eye;\nuniform vec2 scale;\nuniform float dist_offset;\nuniform float dist_mult;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec2 left_eye_offset = (v_texcoord - left_eye); \n  float left_eye_dist = length(left_eye_offset * scale); \n  vec2 right_eye_offset = (v_texcoord - right_eye); \n  float right_eye_dist = length(right_eye_offset * scale); \n  float dist;\n  vec2 offset;\n  vec2 center;\n  if (left_eye_dist < 1.0 || right_eye_dist < 1.0){\n    float dist_left = left_eye_dist * dist_mult + dist_offset;\n    vec4 value_byte = texture2D(tex_sampler_1, vec2(dist_left, 0.5));\n    float value_left = (value_byte.g + value_byte.r * 0.00390625);\n    vec4 color_left = texture2D(tex_sampler_0,\n            left_eye + (1.0 - value_left) * left_eye_offset);\n    float dist_right = right_eye_dist * dist_mult + dist_offset;\n    value_byte = texture2D(tex_sampler_1, vec2(dist_right, 0.5));\n    float value_right = (value_byte.g + value_byte.r * 0.00390625);\n    vec4 color_right = texture2D(tex_sampler_0,\n            right_eye + (1.0 - value_right) * right_eye_offset);\n    float alpha = value_left / (value_left + value_right);\n    gl_FragColor = mix(color_right, color_left, alpha);\n  } else {\n    gl_FragColor = texture2D(tex_sampler_0, v_texcoord);\n  }\n}\n"

    invoke-direct {v0, p1, v1}, Landroid/filterfw/core/ShaderProgram;-><init>(Landroid/filterfw/core/FilterContext;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    invoke-direct {p0, p1}, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->createLookupTable(Landroid/filterfw/core/FilterContext;)V

    return-void
.end method

.method public process(Landroid/filterfw/core/FilterContext;)V
    .locals 41
    .param p1    # Landroid/filterfw/core/FilterContext;

    const-string v36, "image"

    move-object/from16 v0, p0

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    move-result v35

    invoke-virtual/range {v20 .. v20}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    move-result v17

    move/from16 v0, v35

    move/from16 v1, v17

    if-le v0, v1, :cond_2

    const/16 v36, 0x2

    move/from16 v0, v36

    new-array v5, v0, [F

    const/16 v36, 0x0

    const/high16 v37, 0x3f800000

    aput v37, v5, v36

    const/16 v36, 0x1

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v37, v0

    move/from16 v0, v35

    int-to-float v0, v0

    move/from16 v38, v0

    div-float v37, v37, v38

    aput v37, v5, v36

    :goto_0
    const-string v36, "faces"

    move-object/from16 v0, p0

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Landroid/filterfw/core/Frame;->getObjectValue()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/filterpacks/facedetect/FaceMeta;

    const/16 v36, 0x2

    move/from16 v0, v36

    new-array v10, v0, [F

    const/16 v36, 0x2

    move/from16 v0, v36

    new-array v0, v0, [F

    move-object/from16 v34, v0

    invoke-virtual {v15}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->count()I

    move-result v13

    if-lez v13, :cond_7

    invoke-virtual/range {p1 .. p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    move-result-object v36

    invoke-virtual/range {v19 .. v19}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    move-result-object v37

    invoke-virtual/range {v36 .. v37}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    move-result-object v8

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Landroid/filterfw/core/Frame;->setDataFromFrame(Landroid/filterfw/core/Frame;)V

    const/4 v9, 0x0

    const/16 v36, 0x1

    move/from16 v0, v36

    if-le v13, v0, :cond_0

    invoke-virtual/range {p1 .. p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    move-result-object v36

    invoke-virtual/range {v19 .. v19}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    move-result-object v37

    invoke-virtual/range {v36 .. v37}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    move-result-object v9

    :cond_0
    move-object/from16 v32, v19

    move-object/from16 v26, v8

    const/16 v18, 0x0

    :goto_1
    move/from16 v0, v18

    if-ge v0, v13, :cond_5

    const/16 v36, 0x1

    move/from16 v0, v36

    if-le v13, v0, :cond_1

    if-lez v18, :cond_1

    rem-int/lit8 v36, v18, 0x2

    const/16 v37, 0x1

    move/from16 v0, v36

    move/from16 v1, v37

    if-ne v0, v1, :cond_3

    move-object/from16 v32, v8

    move-object/from16 v26, v9

    move-object/from16 v0, v32

    invoke-virtual {v9, v0}, Landroid/filterfw/core/Frame;->setDataFromFrame(Landroid/filterfw/core/Frame;)V

    :cond_1
    :goto_2
    const/16 v36, 0x2

    move/from16 v0, v36

    new-array v0, v0, [F

    move-object/from16 v23, v0

    const/16 v36, 0x0

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getLeftEyeX(I)F

    move-result v37

    aput v37, v23, v36

    const/16 v36, 0x1

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getLeftEyeY(I)F

    move-result v37

    aput v37, v23, v36

    const/16 v36, 0x2

    move/from16 v0, v36

    new-array v0, v0, [F

    move-object/from16 v30, v0

    const/16 v36, 0x0

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getRightEyeX(I)F

    move-result v37

    aput v37, v30, v36

    const/16 v36, 0x1

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getRightEyeY(I)F

    move-result v37

    aput v37, v30, v36

    const/16 v36, 0x2

    move/from16 v0, v36

    new-array v0, v0, [F

    move-object/from16 v25, v0

    const/16 v36, 0x0

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getMouthX(I)F

    move-result v37

    aput v37, v25, v36

    const/16 v36, 0x1

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getMouthY(I)F

    move-result v37

    aput v37, v25, v36

    const/16 v36, 0x0

    aget v36, v23, v36

    const/16 v37, 0x0

    aget v37, v30, v37

    sub-float v36, v36, v37

    const/16 v37, 0x0

    aget v37, v5, v37

    mul-float v11, v36, v37

    const/16 v36, 0x1

    aget v36, v23, v36

    const/16 v37, 0x1

    aget v37, v30, v37

    sub-float v36, v36, v37

    const/16 v37, 0x1

    aget v37, v5, v37

    mul-float v12, v36, v37

    mul-float v36, v11, v11

    mul-float v37, v12, v12

    add-float v36, v36, v37

    move/from16 v0, v36

    float-to-double v0, v0

    move-wide/from16 v36, v0

    invoke-static/range {v36 .. v37}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v36

    move-wide/from16 v0, v36

    double-to-float v6, v0

    const/high16 v14, 0x3f800000

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mCurrentEffect:I

    move/from16 v36, v0

    const/16 v37, 0x1

    move/from16 v0, v36

    move/from16 v1, v37

    if-ne v0, v1, :cond_4

    move v14, v6

    move/from16 v24, v14

    const/16 v36, 0x1

    aget v36, v23, v36

    sub-float v36, v36, v24

    const/16 v37, 0x1

    aget v37, v30, v37

    sub-float v37, v37, v24

    invoke-static/range {v36 .. v37}, Ljava/lang/Math;->min(FF)F

    move-result v33

    const/16 v36, 0x0

    aget v36, v23, v36

    sub-float v36, v36, v24

    const/16 v37, 0x0

    aget v37, v30, v37

    sub-float v37, v37, v24

    invoke-static/range {v36 .. v37}, Ljava/lang/Math;->min(FF)F

    move-result v22

    const/16 v36, 0x1

    aget v36, v23, v36

    add-float v36, v36, v24

    const/16 v37, 0x1

    aget v37, v30, v37

    add-float v37, v37, v24

    invoke-static/range {v36 .. v37}, Ljava/lang/Math;->max(FF)F

    move-result v7

    const/16 v36, 0x0

    aget v36, v23, v36

    add-float v36, v36, v24

    const/16 v37, 0x0

    aget v37, v30, v37

    add-float v37, v37, v24

    invoke-static/range {v36 .. v37}, Ljava/lang/Math;->max(FF)F

    move-result v29

    new-instance v27, Landroid/filterfw/geometry/Rectangle;

    sub-float v36, v29, v22

    sub-float v37, v7, v33

    move-object/from16 v0, v27

    move/from16 v1, v22

    move/from16 v2, v33

    move/from16 v3, v36

    move/from16 v4, v37

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/filterfw/geometry/Rectangle;-><init>(FFFF)V

    const/16 v36, 0x0

    const/16 v37, 0x0

    move-object/from16 v0, v27

    move/from16 v1, v36

    move/from16 v2, v37

    invoke-virtual {v0, v1, v2}, Landroid/filterfw/geometry/Rectangle;->translated(FF)Landroid/filterfw/geometry/Quad;

    move-result-object v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v36, v0

    const-string v37, "left_eye"

    move-object/from16 v0, v36

    move-object/from16 v1, v37

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v36, v0

    const-string v37, "right_eye"

    move-object/from16 v0, v36

    move-object/from16 v1, v37

    move-object/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    const/16 v36, 0x2

    move/from16 v0, v36

    new-array v0, v0, [F

    move-object/from16 v31, v0

    const/16 v36, 0x0

    const/16 v37, 0x0

    aget v37, v5, v37

    div-float v37, v37, v14

    aput v37, v31, v36

    const/16 v36, 0x1

    const/16 v37, 0x1

    aget v37, v5, v37

    div-float v37, v37, v14

    aput v37, v31, v36

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v36, v0

    const-string v37, "scale"

    move-object/from16 v0, v36

    move-object/from16 v1, v37

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/filterfw/core/ShaderProgram;->setSourceRegion(Landroid/filterfw/geometry/Quad;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/filterfw/core/ShaderProgram;->setTargetRegion(Landroid/filterfw/geometry/Quad;)V

    const/16 v36, 0x2

    move/from16 v0, v36

    new-array v0, v0, [Landroid/filterfw/core/Frame;

    move-object/from16 v21, v0

    const/16 v36, 0x0

    aput-object v32, v21, v36

    const/16 v36, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mTableFrame:Landroid/filterfw/core/Frame;

    move-object/from16 v37, v0

    aput-object v37, v21, v36

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    move-object/from16 v1, v21

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/filterfw/core/ShaderProgram;->process([Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    :goto_3
    add-int/lit8 v18, v18, 0x1

    goto/16 :goto_1

    :cond_2
    const/16 v36, 0x2

    move/from16 v0, v36

    new-array v5, v0, [F

    const/16 v36, 0x0

    move/from16 v0, v35

    int-to-float v0, v0

    move/from16 v37, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v38, v0

    div-float v37, v37, v38

    aput v37, v5, v36

    const/16 v36, 0x1

    const/high16 v37, 0x3f800000

    aput v37, v5, v36

    goto/16 :goto_0

    :cond_3
    move-object/from16 v32, v9

    move-object/from16 v26, v8

    move-object/from16 v0, v32

    invoke-virtual {v8, v0}, Landroid/filterfw/core/Frame;->setDataFromFrame(Landroid/filterfw/core/Frame;)V

    goto/16 :goto_2

    :cond_4
    const/16 v36, 0x0

    const/high16 v37, 0x3f800000

    aput v37, v34, v36

    const/16 v36, 0x1

    const/high16 v37, 0x3f800000

    aput v37, v34, v36

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mCurrentEffect:I

    move/from16 v36, v0

    packed-switch v36, :pswitch_data_0

    :pswitch_0
    new-instance v36, Ljava/lang/RuntimeException;

    new-instance v37, Ljava/lang/StringBuilder;

    invoke-direct/range {v37 .. v37}, Ljava/lang/StringBuilder;-><init>()V

    const-string v38, "Undefined effect: "

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mCurrentEffect:I

    move/from16 v38, v0

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v37

    invoke-direct/range {v36 .. v37}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v36

    :pswitch_1
    const v36, 0x3f4ccccd

    mul-float v14, v36, v6

    const/16 v36, 0x0

    const/high16 v37, 0x3f000000

    const/16 v38, 0x0

    aget v38, v23, v38

    mul-float v37, v37, v38

    const/high16 v38, 0x3f000000

    const/16 v39, 0x0

    aget v39, v30, v39

    mul-float v38, v38, v39

    add-float v37, v37, v38

    const v38, 0x38d1b717

    const/16 v39, 0x0

    aget v39, v25, v39

    mul-float v38, v38, v39

    add-float v37, v37, v38

    aput v37, v10, v36

    const/16 v36, 0x1

    const/high16 v37, 0x3f000000

    const/16 v38, 0x1

    aget v38, v23, v38

    mul-float v37, v37, v38

    const/high16 v38, 0x3f000000

    const/16 v39, 0x1

    aget v39, v30, v39

    mul-float v38, v38, v39

    add-float v37, v37, v38

    const v38, 0x38d1b717

    const/16 v39, 0x1

    aget v39, v25, v39

    mul-float v38, v38, v39

    add-float v37, v37, v38

    aput v37, v10, v36

    const/16 v36, 0x0

    const-wide v37, 0x3ff3333333333333L

    invoke-static/range {v37 .. v38}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v37

    move-wide/from16 v0, v37

    double-to-float v0, v0

    move/from16 v37, v0

    aput v37, v34, v36

    const/16 v36, 0x1

    const-wide v37, 0x3ff3333333333333L

    invoke-static/range {v37 .. v38}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v37

    move-wide/from16 v0, v37

    double-to-float v0, v0

    move/from16 v37, v0

    aput v37, v34, v36

    :goto_4
    const/16 v36, 0x0

    aget v37, v34, v36

    div-float v37, v37, v14

    aput v37, v34, v36

    const/16 v36, 0x1

    aget v37, v34, v36

    div-float v37, v37, v14

    aput v37, v34, v36

    const/16 v36, 0x0

    aget v37, v34, v36

    const/16 v38, 0x0

    aget v38, v5, v38

    mul-float v37, v37, v38

    aput v37, v34, v36

    const/16 v36, 0x1

    aget v37, v34, v36

    const/16 v38, 0x1

    aget v38, v5, v38

    mul-float v37, v37, v38

    aput v37, v34, v36

    new-instance v27, Landroid/filterfw/geometry/Rectangle;

    const/high16 v36, -0x40800000

    const/16 v37, 0x0

    aget v37, v34, v37

    div-float v36, v36, v37

    const/high16 v37, -0x40800000

    const/16 v38, 0x1

    aget v38, v34, v38

    div-float v37, v37, v38

    const/high16 v38, 0x40000000

    const/16 v39, 0x0

    aget v39, v34, v39

    div-float v38, v38, v39

    const/high16 v39, 0x40000000

    const/16 v40, 0x1

    aget v40, v34, v40

    div-float v39, v39, v40

    move-object/from16 v0, v27

    move/from16 v1, v36

    move/from16 v2, v37

    move/from16 v3, v38

    move/from16 v4, v39

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/filterfw/geometry/Rectangle;-><init>(FFFF)V

    const/16 v36, 0x0

    aget v36, v10, v36

    const/16 v37, 0x1

    aget v37, v10, v37

    move-object/from16 v0, v27

    move/from16 v1, v36

    move/from16 v2, v37

    invoke-virtual {v0, v1, v2}, Landroid/filterfw/geometry/Rectangle;->translated(FF)Landroid/filterfw/geometry/Quad;

    move-result-object v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mGoofyProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v36, v0

    const-string v37, "center"

    move-object/from16 v0, v36

    move-object/from16 v1, v37

    invoke-virtual {v0, v1, v10}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mGoofyProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v36, v0

    const-string v37, "weight"

    move-object/from16 v0, v36

    move-object/from16 v1, v37

    move-object/from16 v2, v34

    invoke-virtual {v0, v1, v2}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mGoofyProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/filterfw/core/ShaderProgram;->setSourceRegion(Landroid/filterfw/geometry/Quad;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mGoofyProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/filterfw/core/ShaderProgram;->setTargetRegion(Landroid/filterfw/geometry/Quad;)V

    const/16 v36, 0x2

    move/from16 v0, v36

    new-array v0, v0, [Landroid/filterfw/core/Frame;

    move-object/from16 v21, v0

    const/16 v36, 0x0

    aput-object v32, v21, v36

    const/16 v36, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mTableFrame:Landroid/filterfw/core/Frame;

    move-object/from16 v37, v0

    aput-object v37, v21, v36

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mGoofyProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    move-object/from16 v1, v21

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/filterfw/core/ShaderProgram;->process([Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    goto/16 :goto_3

    :pswitch_2
    const/high16 v36, 0x3f000000

    mul-float v14, v36, v6

    const/16 v36, 0x0

    const/16 v37, 0x0

    aget v37, v25, v37

    aput v37, v10, v36

    const/16 v36, 0x1

    const/16 v37, 0x1

    aget v37, v25, v37

    aput v37, v10, v36

    goto/16 :goto_4

    :pswitch_3
    const/high16 v36, 0x40200000

    mul-float v14, v36, v6

    const/16 v36, 0x0

    const/16 v37, 0x0

    aget v37, v25, v37

    aput v37, v10, v36

    const/16 v36, 0x1

    const/16 v37, 0x1

    aget v37, v25, v37

    aput v37, v10, v36

    goto/16 :goto_4

    :pswitch_4
    const v36, 0x3f8ccccd

    mul-float v14, v36, v6

    const/16 v36, 0x0

    const/high16 v37, 0x3e800000

    const/16 v38, 0x0

    aget v38, v23, v38

    mul-float v37, v37, v38

    const/high16 v38, 0x3e800000

    const/16 v39, 0x0

    aget v39, v30, v39

    mul-float v38, v38, v39

    add-float v37, v37, v38

    const/high16 v38, 0x3f000000

    const/16 v39, 0x0

    aget v39, v25, v39

    mul-float v38, v38, v39

    add-float v37, v37, v38

    aput v37, v10, v36

    const/16 v36, 0x1

    const/high16 v37, 0x3e800000

    const/16 v38, 0x1

    aget v38, v23, v38

    mul-float v37, v37, v38

    const/high16 v38, 0x3e800000

    const/16 v39, 0x1

    aget v39, v30, v39

    mul-float v38, v38, v39

    add-float v37, v37, v38

    const/high16 v38, 0x3f000000

    const/16 v39, 0x1

    aget v39, v25, v39

    mul-float v38, v38, v39

    add-float v37, v37, v38

    aput v37, v10, v36

    goto/16 :goto_4

    :pswitch_5
    const v36, 0x3f4ccccd

    mul-float v14, v36, v6

    const/16 v36, 0x0

    const/high16 v37, 0x3e800000

    const/16 v38, 0x0

    aget v38, v23, v38

    mul-float v37, v37, v38

    const/high16 v38, 0x3e800000

    const/16 v39, 0x0

    aget v39, v30, v39

    mul-float v38, v38, v39

    add-float v37, v37, v38

    const/high16 v38, 0x3f000000

    const/16 v39, 0x0

    aget v39, v25, v39

    mul-float v38, v38, v39

    add-float v37, v37, v38

    aput v37, v10, v36

    const/16 v36, 0x1

    const/high16 v37, 0x3e800000

    const/16 v38, 0x1

    aget v38, v23, v38

    mul-float v37, v37, v38

    const/high16 v38, 0x3e800000

    const/16 v39, 0x1

    aget v39, v30, v39

    mul-float v38, v38, v39

    add-float v37, v37, v38

    const/high16 v38, 0x3f000000

    const/16 v39, 0x1

    aget v39, v25, v39

    mul-float v38, v38, v39

    add-float v37, v37, v38

    aput v37, v10, v36

    goto/16 :goto_4

    :cond_5
    const-string v36, "outimage"

    move-object/from16 v0, p0

    move-object/from16 v1, v36

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    invoke-virtual {v8}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    if-eqz v9, :cond_6

    invoke-virtual {v9}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    :cond_6
    :goto_5
    return-void

    :cond_7
    const-string v36, "outimage"

    move-object/from16 v0, p0

    move-object/from16 v1, v36

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    goto :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_1
    .end packed-switch
.end method

.method public setupPorts()V
    .locals 4

    const/4 v2, 0x3

    invoke-static {v2, v2}, Landroid/filterfw/format/ImageFormat;->create(II)Landroid/filterfw/core/MutableFrameFormat;

    move-result-object v1

    const-class v2, Lcom/google/android/filterpacks/facedetect/FaceMeta;

    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/filterfw/format/ObjectFormat;->fromClass(Ljava/lang/Class;I)Landroid/filterfw/core/MutableFrameFormat;

    move-result-object v0

    const-string v2, "image"

    invoke-virtual {p0, v2, v1}, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->addMaskedInputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    const-string v2, "faces"

    invoke-virtual {p0, v2, v0}, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->addMaskedInputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    const-string v2, "outimage"

    const-string v3, "image"

    invoke-virtual {p0, v2, v3}, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->addOutputBasedOnInput(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
