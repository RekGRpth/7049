.class public Lcom/google/android/filterpacks/facedetect/MultiFaceTrackerFilter;
.super Landroid/filterfw/core/Filter;
.source "MultiFaceTrackerFilter.java"


# instance fields
.field private mInputChannels:I

.field private mInputHeight:I

.field private mInputWidth:I

.field private mIsInitialized:Z

.field private mProgram:Landroid/filterfw/core/NativeProgram;
    .annotation runtime Landroid/filterfw/core/GenerateProgramPorts;
        value = {
            .subannotation Landroid/filterfw/core/GenerateProgramPort;
                hasDefault = true
                name = "modulePath"
                type = Ljava/lang/String;
            .end subannotation,
            .subannotation Landroid/filterfw/core/GenerateProgramPort;
                hasDefault = true
                name = "ffModule"
                type = Ljava/lang/String;
            .end subannotation,
            .subannotation Landroid/filterfw/core/GenerateProgramPort;
                hasDefault = true
                name = "lmModule"
                type = Ljava/lang/String;
            .end subannotation,
            .subannotation Landroid/filterfw/core/GenerateProgramPort;
                hasDefault = true
                name = "numSkipFrames"
                type = I
            .end subannotation,
            .subannotation Landroid/filterfw/core/GenerateProgramPort;
                hasDefault = true
                name = "trackingError"
                type = F
            .end subannotation,
            .subannotation Landroid/filterfw/core/GenerateProgramPort;
                hasDefault = true
                name = "minEyeDist"
                type = F
            .end subannotation,
            .subannotation Landroid/filterfw/core/GenerateProgramPort;
                hasDefault = true
                name = "rollRange"
                type = F
            .end subannotation,
            .subannotation Landroid/filterfw/core/GenerateProgramPort;
                hasDefault = true
                name = "quality"
                type = F
            .end subannotation,
            .subannotation Landroid/filterfw/core/GenerateProgramPort;
                hasDefault = true
                name = "smoothness"
                type = F
            .end subannotation,
            .subannotation Landroid/filterfw/core/GenerateProgramPort;
                hasDefault = true
                name = "mouthOnlySmoothing"
                type = I
            .end subannotation,
            .subannotation Landroid/filterfw/core/GenerateProgramPort;
                hasDefault = true
                name = "useAffineCorrection"
                type = I
            .end subannotation,
            .subannotation Landroid/filterfw/core/GenerateProgramPort;
                hasDefault = true
                name = "numChannelsDetector"
                type = I
            .end subannotation,
            .subannotation Landroid/filterfw/core/GenerateProgramPort;
                hasDefault = true
                name = "patchSize"
                type = I
            .end subannotation
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/filterpacks/facedetect/MultiFaceTrackerFilter;->mProgram:Landroid/filterfw/core/NativeProgram;

    iput-boolean v1, p0, Lcom/google/android/filterpacks/facedetect/MultiFaceTrackerFilter;->mIsInitialized:Z

    iput v1, p0, Lcom/google/android/filterpacks/facedetect/MultiFaceTrackerFilter;->mInputWidth:I

    iput v1, p0, Lcom/google/android/filterpacks/facedetect/MultiFaceTrackerFilter;->mInputHeight:I

    iput v1, p0, Lcom/google/android/filterpacks/facedetect/MultiFaceTrackerFilter;->mInputChannels:I

    return-void
.end method


# virtual methods
.method public close(Landroid/filterfw/core/FilterContext;)V
    .locals 1
    .param p1    # Landroid/filterfw/core/FilterContext;

    iget-object v0, p0, Lcom/google/android/filterpacks/facedetect/MultiFaceTrackerFilter;->mProgram:Landroid/filterfw/core/NativeProgram;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/filterpacks/facedetect/MultiFaceTrackerFilter;->mProgram:Landroid/filterfw/core/NativeProgram;

    invoke-virtual {v0}, Landroid/filterfw/core/NativeProgram;->reset()V

    :cond_0
    return-void
.end method

.method public prepare(Landroid/filterfw/core/FilterContext;)V
    .locals 3
    .param p1    # Landroid/filterfw/core/FilterContext;

    new-instance v0, Landroid/filterfw/core/NativeProgram;

    const-string v1, "filterpack_facedetect"

    const-string v2, "multiface_tracker"

    invoke-direct {v0, v1, v2}, Landroid/filterfw/core/NativeProgram;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/filterpacks/facedetect/MultiFaceTrackerFilter;->mProgram:Landroid/filterfw/core/NativeProgram;

    iget-object v0, p0, Lcom/google/android/filterpacks/facedetect/MultiFaceTrackerFilter;->mProgram:Landroid/filterfw/core/NativeProgram;

    invoke-virtual {p0, v0, p1}, Lcom/google/android/filterpacks/facedetect/MultiFaceTrackerFilter;->initProgramInputs(Landroid/filterfw/core/Program;Landroid/filterfw/core/FilterContext;)V

    return-void
.end method

.method public process(Landroid/filterfw/core/FilterContext;)V
    .locals 9
    .param p1    # Landroid/filterfw/core/FilterContext;

    const/4 v6, 0x0

    const-string v5, "image"

    invoke-virtual {p0, v5}, Lcom/google/android/filterpacks/facedetect/MultiFaceTrackerFilter;->pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    move-result-object v1

    invoke-virtual {v1}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    move-result-object v2

    iget-boolean v5, p0, Lcom/google/android/filterpacks/facedetect/MultiFaceTrackerFilter;->mIsInitialized:Z

    if-nez v5, :cond_1

    invoke-virtual {v2}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    move-result v5

    iput v5, p0, Lcom/google/android/filterpacks/facedetect/MultiFaceTrackerFilter;->mInputWidth:I

    invoke-virtual {v2}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    move-result v5

    iput v5, p0, Lcom/google/android/filterpacks/facedetect/MultiFaceTrackerFilter;->mInputHeight:I

    invoke-virtual {v2}, Landroid/filterfw/core/FrameFormat;->getBytesPerSample()I

    move-result v5

    iput v5, p0, Lcom/google/android/filterpacks/facedetect/MultiFaceTrackerFilter;->mInputChannels:I

    iget-object v5, p0, Lcom/google/android/filterpacks/facedetect/MultiFaceTrackerFilter;->mProgram:Landroid/filterfw/core/NativeProgram;

    const-string v7, "imgWidth"

    iget v8, p0, Lcom/google/android/filterpacks/facedetect/MultiFaceTrackerFilter;->mInputWidth:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/filterfw/core/NativeProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v5, p0, Lcom/google/android/filterpacks/facedetect/MultiFaceTrackerFilter;->mProgram:Landroid/filterfw/core/NativeProgram;

    const-string v7, "imgHeight"

    iget v8, p0, Lcom/google/android/filterpacks/facedetect/MultiFaceTrackerFilter;->mInputHeight:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/filterfw/core/NativeProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v5, p0, Lcom/google/android/filterpacks/facedetect/MultiFaceTrackerFilter;->mProgram:Landroid/filterfw/core/NativeProgram;

    const-string v7, "imgChannels"

    iget v8, p0, Lcom/google/android/filterpacks/facedetect/MultiFaceTrackerFilter;->mInputChannels:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/filterfw/core/NativeProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/google/android/filterpacks/facedetect/MultiFaceTrackerFilter;->mIsInitialized:Z

    :cond_0
    :goto_0
    iget-object v5, p0, Lcom/google/android/filterpacks/facedetect/MultiFaceTrackerFilter;->mProgram:Landroid/filterfw/core/NativeProgram;

    invoke-virtual {v5, v1, v6}, Landroid/filterfw/core/NativeProgram;->process(Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    iget-object v5, p0, Lcom/google/android/filterpacks/facedetect/MultiFaceTrackerFilter;->mProgram:Landroid/filterfw/core/NativeProgram;

    const-string v7, "num_faces"

    invoke-virtual {v5, v7}, Landroid/filterfw/core/NativeProgram;->getHostValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    const-class v5, Lcom/google/android/filterpacks/facedetect/FaceMeta;

    const/4 v7, 0x2

    invoke-static {v5, v0, v7}, Landroid/filterfw/format/ObjectFormat;->fromClass(Ljava/lang/Class;II)Landroid/filterfw/core/MutableFrameFormat;

    move-result-object v4

    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    move-result-object v3

    iget-object v7, p0, Lcom/google/android/filterpacks/facedetect/MultiFaceTrackerFilter;->mProgram:Landroid/filterfw/core/NativeProgram;

    move-object v5, v6

    check-cast v5, Landroid/filterfw/core/Frame;

    invoke-virtual {v7, v5, v3}, Landroid/filterfw/core/NativeProgram;->process(Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    const-string v5, "faces"

    invoke-virtual {p0, v5, v3}, Lcom/google/android/filterpacks/facedetect/MultiFaceTrackerFilter;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    invoke-virtual {v3}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    return-void

    :cond_1
    iget v5, p0, Lcom/google/android/filterpacks/facedetect/MultiFaceTrackerFilter;->mInputWidth:I

    invoke-virtual {v2}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    move-result v7

    if-ne v5, v7, :cond_2

    iget v5, p0, Lcom/google/android/filterpacks/facedetect/MultiFaceTrackerFilter;->mInputHeight:I

    invoke-virtual {v2}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    move-result v7

    if-ne v5, v7, :cond_2

    iget v5, p0, Lcom/google/android/filterpacks/facedetect/MultiFaceTrackerFilter;->mInputChannels:I

    invoke-virtual {v2}, Landroid/filterfw/core/FrameFormat;->getBytesPerSample()I

    move-result v7

    if-eq v5, v7, :cond_0

    :cond_2
    iget-object v5, p0, Lcom/google/android/filterpacks/facedetect/MultiFaceTrackerFilter;->mProgram:Landroid/filterfw/core/NativeProgram;

    invoke-virtual {v5}, Landroid/filterfw/core/NativeProgram;->reset()V

    invoke-virtual {v2}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    move-result v5

    iput v5, p0, Lcom/google/android/filterpacks/facedetect/MultiFaceTrackerFilter;->mInputWidth:I

    invoke-virtual {v2}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    move-result v5

    iput v5, p0, Lcom/google/android/filterpacks/facedetect/MultiFaceTrackerFilter;->mInputHeight:I

    invoke-virtual {v2}, Landroid/filterfw/core/FrameFormat;->getBytesPerSample()I

    move-result v5

    iput v5, p0, Lcom/google/android/filterpacks/facedetect/MultiFaceTrackerFilter;->mInputChannels:I

    iget-object v5, p0, Lcom/google/android/filterpacks/facedetect/MultiFaceTrackerFilter;->mProgram:Landroid/filterfw/core/NativeProgram;

    const-string v7, "imgWidth"

    iget v8, p0, Lcom/google/android/filterpacks/facedetect/MultiFaceTrackerFilter;->mInputWidth:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/filterfw/core/NativeProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v5, p0, Lcom/google/android/filterpacks/facedetect/MultiFaceTrackerFilter;->mProgram:Landroid/filterfw/core/NativeProgram;

    const-string v7, "imgHeight"

    iget v8, p0, Lcom/google/android/filterpacks/facedetect/MultiFaceTrackerFilter;->mInputHeight:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/filterfw/core/NativeProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v5, p0, Lcom/google/android/filterpacks/facedetect/MultiFaceTrackerFilter;->mProgram:Landroid/filterfw/core/NativeProgram;

    const-string v7, "imgChannels"

    iget v8, p0, Lcom/google/android/filterpacks/facedetect/MultiFaceTrackerFilter;->mInputChannels:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/filterfw/core/NativeProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method public setupPorts()V
    .locals 4

    const/4 v3, 0x2

    const/4 v2, 0x1

    invoke-static {v2, v3}, Landroid/filterfw/format/ImageFormat;->create(II)Landroid/filterfw/core/MutableFrameFormat;

    move-result-object v0

    const-class v2, Lcom/google/android/filterpacks/facedetect/FaceMeta;

    invoke-static {v2, v3}, Landroid/filterfw/format/ObjectFormat;->fromClass(Ljava/lang/Class;I)Landroid/filterfw/core/MutableFrameFormat;

    move-result-object v1

    const-string v2, "image"

    invoke-virtual {p0, v2, v0}, Lcom/google/android/filterpacks/facedetect/MultiFaceTrackerFilter;->addMaskedInputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    const-string v2, "faces"

    invoke-virtual {p0, v2, v1}, Lcom/google/android/filterpacks/facedetect/MultiFaceTrackerFilter;->addOutputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    return-void
.end method

.method public tearDown(Landroid/filterfw/core/FilterContext;)V
    .locals 1
    .param p1    # Landroid/filterfw/core/FilterContext;

    iget-object v0, p0, Lcom/google/android/filterpacks/facedetect/MultiFaceTrackerFilter;->mProgram:Landroid/filterfw/core/NativeProgram;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/filterpacks/facedetect/MultiFaceTrackerFilter;->mProgram:Landroid/filterfw/core/NativeProgram;

    invoke-virtual {v0}, Landroid/filterfw/core/NativeProgram;->tearDown()V

    :cond_0
    return-void
.end method
