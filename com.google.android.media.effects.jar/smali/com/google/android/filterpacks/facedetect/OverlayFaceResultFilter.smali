.class public Lcom/google/android/filterpacks/facedetect/OverlayFaceResultFilter;
.super Landroid/filterfw/core/Filter;
.source "OverlayFaceResultFilter.java"


# instance fields
.field private final mBlendShader:Ljava/lang/String;

.field private mOverlayProgram:Landroid/filterfw/core/ShaderProgram;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;-><init>(Ljava/lang/String;)V

    const-string v0, "precision mediump float;\nuniform int num_face;\nuniform float alpha;\nuniform vec4 blend_color;\nuniform vec4 face_rect;\nuniform vec2 left_eye;\nuniform vec2 right_eye;\nuniform vec2 mouth_pos;\nuniform vec2 upper_lip_pos;\nuniform vec2 lower_lip_pos;\nuniform sampler2D tex_sampler_0;\nfloat eye_size;\nvec2 face_size;\nvec2 face_center;\nfloat dist;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec4 color = texture2D(tex_sampler_0, v_texcoord);\n  if (num_face!=0) {\n    face_center[0] = (face_rect[0] + face_rect[2]) / 2.0;\n    face_center[1] = (face_rect[1] + face_rect[3]) / 2.0;\n    face_size[0] = (face_rect[2] - face_rect[0]) / 2.0;\n    face_size[1] = (face_rect[3] - face_rect[1]) / 2.0;\n    dist = length((v_texcoord - face_center) / face_size);\n    if (dist<1.0) {\n       eye_size = distance(left_eye, right_eye) / 6.0;\n       if ( distance(left_eye, v_texcoord) < eye_size ||\n            distance(right_eye, v_texcoord) < eye_size) { \n         gl_FragColor = mix(color, vec4(1.0,0,0,1.0), alpha);\n       } else if (distance(mouth_pos, v_texcoord) < 0.5*eye_size ) { \n         gl_FragColor = mix(color, vec4(0,0,1.0,1.0), alpha);\n       } else if (distance(upper_lip_pos, v_texcoord) < 0.5*eye_size ) { \n         gl_FragColor = mix(color, vec4(1.0,1.0,0,1.0), alpha);\n       } else if (distance(lower_lip_pos, v_texcoord) < 0.5*eye_size ) { \n         gl_FragColor = mix(color, vec4(1.0,1.0,0,1.0), alpha);\n       }\n       else gl_FragColor = mix(color, blend_color, alpha);\n    }\n    else {\n      gl_FragColor = color;\n    }\n  } else gl_FragColor = color;\n}\n"

    iput-object v0, p0, Lcom/google/android/filterpacks/facedetect/OverlayFaceResultFilter;->mBlendShader:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getOutputFormat(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/FrameFormat;
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/filterfw/core/FrameFormat;

    return-object p2
.end method

.method protected prepare(Landroid/filterfw/core/FilterContext;)V
    .locals 2
    .param p1    # Landroid/filterfw/core/FilterContext;

    new-instance v0, Landroid/filterfw/core/ShaderProgram;

    const-string v1, "precision mediump float;\nuniform int num_face;\nuniform float alpha;\nuniform vec4 blend_color;\nuniform vec4 face_rect;\nuniform vec2 left_eye;\nuniform vec2 right_eye;\nuniform vec2 mouth_pos;\nuniform vec2 upper_lip_pos;\nuniform vec2 lower_lip_pos;\nuniform sampler2D tex_sampler_0;\nfloat eye_size;\nvec2 face_size;\nvec2 face_center;\nfloat dist;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec4 color = texture2D(tex_sampler_0, v_texcoord);\n  if (num_face!=0) {\n    face_center[0] = (face_rect[0] + face_rect[2]) / 2.0;\n    face_center[1] = (face_rect[1] + face_rect[3]) / 2.0;\n    face_size[0] = (face_rect[2] - face_rect[0]) / 2.0;\n    face_size[1] = (face_rect[3] - face_rect[1]) / 2.0;\n    dist = length((v_texcoord - face_center) / face_size);\n    if (dist<1.0) {\n       eye_size = distance(left_eye, right_eye) / 6.0;\n       if ( distance(left_eye, v_texcoord) < eye_size ||\n            distance(right_eye, v_texcoord) < eye_size) { \n         gl_FragColor = mix(color, vec4(1.0,0,0,1.0), alpha);\n       } else if (distance(mouth_pos, v_texcoord) < 0.5*eye_size ) { \n         gl_FragColor = mix(color, vec4(0,0,1.0,1.0), alpha);\n       } else if (distance(upper_lip_pos, v_texcoord) < 0.5*eye_size ) { \n         gl_FragColor = mix(color, vec4(1.0,1.0,0,1.0), alpha);\n       } else if (distance(lower_lip_pos, v_texcoord) < 0.5*eye_size ) { \n         gl_FragColor = mix(color, vec4(1.0,1.0,0,1.0), alpha);\n       }\n       else gl_FragColor = mix(color, blend_color, alpha);\n    }\n    else {\n      gl_FragColor = color;\n    }\n  } else gl_FragColor = color;\n}\n"

    invoke-direct {v0, p1, v1}, Landroid/filterfw/core/ShaderProgram;-><init>(Landroid/filterfw/core/FilterContext;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/filterpacks/facedetect/OverlayFaceResultFilter;->mOverlayProgram:Landroid/filterfw/core/ShaderProgram;

    return-void
.end method

.method public process(Landroid/filterfw/core/FilterContext;)V
    .locals 21
    .param p1    # Landroid/filterfw/core/FilterContext;

    invoke-virtual/range {p1 .. p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    move-result-object v4

    const-string v15, "image"

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/google/android/filterpacks/facedetect/OverlayFaceResultFilter;->pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    move-result-object v7

    const-string v15, "faces"

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/google/android/filterpacks/facedetect/OverlayFaceResultFilter;->pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    move-result-object v2

    invoke-virtual {v2}, Landroid/filterfw/core/Frame;->getObjectValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/filterpacks/facedetect/FaceMeta;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/filterpacks/facedetect/OverlayFaceResultFilter;->mOverlayProgram:Landroid/filterfw/core/ShaderProgram;

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/high16 v18, 0x3f800000

    const/high16 v19, 0x3f800000

    invoke-virtual/range {v15 .. v19}, Landroid/filterfw/core/ShaderProgram;->setSourceRect(FFFF)V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/filterpacks/facedetect/OverlayFaceResultFilter;->mOverlayProgram:Landroid/filterfw/core/ShaderProgram;

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/high16 v18, 0x3f800000

    const/high16 v19, 0x3f800000

    invoke-virtual/range {v15 .. v19}, Landroid/filterfw/core/ShaderProgram;->setTargetRect(FFFF)V

    const/4 v15, 0x4

    new-array v5, v15, [F

    fill-array-data v5, :array_0

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/filterpacks/facedetect/OverlayFaceResultFilter;->mOverlayProgram:Landroid/filterfw/core/ShaderProgram;

    const-string v16, "blend_color"

    move-object/from16 v0, v16

    invoke-virtual {v15, v0, v5}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/filterpacks/facedetect/OverlayFaceResultFilter;->mOverlayProgram:Landroid/filterfw/core/ShaderProgram;

    const-string v16, "alpha"

    const/high16 v17, 0x3f000000

    invoke-static/range {v17 .. v17}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v17

    invoke-virtual/range {v15 .. v17}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v1}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->count()I

    move-result v11

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/filterpacks/facedetect/OverlayFaceResultFilter;->mOverlayProgram:Landroid/filterfw/core/ShaderProgram;

    const-string v16, "num_face"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    invoke-virtual/range {v15 .. v17}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v15, "Overlay Result"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "number of faces"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v11, :cond_2

    invoke-virtual {v7}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    move-result-object v15

    invoke-virtual {v4, v15}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    move-result-object v12

    const/4 v6, 0x0

    :goto_0
    if-ge v6, v11, :cond_1

    const/4 v15, 0x4

    new-array v3, v15, [F

    const/4 v15, 0x0

    invoke-virtual {v1, v6}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getFaceX0(I)F

    move-result v16

    aput v16, v3, v15

    const/4 v15, 0x1

    invoke-virtual {v1, v6}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getFaceY0(I)F

    move-result v16

    aput v16, v3, v15

    const/4 v15, 0x2

    invoke-virtual {v1, v6}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getFaceX1(I)F

    move-result v16

    aput v16, v3, v15

    const/4 v15, 0x3

    invoke-virtual {v1, v6}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getFaceY1(I)F

    move-result v16

    aput v16, v3, v15

    const/4 v15, 0x2

    new-array v8, v15, [F

    const/4 v15, 0x0

    invoke-virtual {v1, v6}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getLeftEyeX(I)F

    move-result v16

    aput v16, v8, v15

    const/4 v15, 0x1

    invoke-virtual {v1, v6}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getLeftEyeY(I)F

    move-result v16

    aput v16, v8, v15

    const/4 v15, 0x2

    new-array v13, v15, [F

    const/4 v15, 0x0

    invoke-virtual {v1, v6}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getRightEyeX(I)F

    move-result v16

    aput v16, v13, v15

    const/4 v15, 0x1

    invoke-virtual {v1, v6}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getRightEyeY(I)F

    move-result v16

    aput v16, v13, v15

    const/4 v15, 0x2

    new-array v10, v15, [F

    const/4 v15, 0x0

    invoke-virtual {v1, v6}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getMouthX(I)F

    move-result v16

    aput v16, v10, v15

    const/4 v15, 0x1

    invoke-virtual {v1, v6}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getMouthY(I)F

    move-result v16

    aput v16, v10, v15

    const/4 v15, 0x2

    new-array v14, v15, [F

    const/4 v15, 0x0

    invoke-virtual {v1, v6}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getUpperLipX(I)F

    move-result v16

    aput v16, v14, v15

    const/4 v15, 0x1

    invoke-virtual {v1, v6}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getUpperLipY(I)F

    move-result v16

    aput v16, v14, v15

    const/4 v15, 0x2

    new-array v9, v15, [F

    const/4 v15, 0x0

    invoke-virtual {v1, v6}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getLowerLipX(I)F

    move-result v16

    aput v16, v9, v15

    const/4 v15, 0x1

    invoke-virtual {v1, v6}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getLowerLipY(I)F

    move-result v16

    aput v16, v9, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/filterpacks/facedetect/OverlayFaceResultFilter;->mOverlayProgram:Landroid/filterfw/core/ShaderProgram;

    const-string v16, "face_rect"

    move-object/from16 v0, v16

    invoke-virtual {v15, v0, v3}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/filterpacks/facedetect/OverlayFaceResultFilter;->mOverlayProgram:Landroid/filterfw/core/ShaderProgram;

    const-string v16, "left_eye"

    move-object/from16 v0, v16

    invoke-virtual {v15, v0, v8}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/filterpacks/facedetect/OverlayFaceResultFilter;->mOverlayProgram:Landroid/filterfw/core/ShaderProgram;

    const-string v16, "right_eye"

    move-object/from16 v0, v16

    invoke-virtual {v15, v0, v13}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/filterpacks/facedetect/OverlayFaceResultFilter;->mOverlayProgram:Landroid/filterfw/core/ShaderProgram;

    const-string v16, "mouth_pos"

    move-object/from16 v0, v16

    invoke-virtual {v15, v0, v10}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/filterpacks/facedetect/OverlayFaceResultFilter;->mOverlayProgram:Landroid/filterfw/core/ShaderProgram;

    const-string v16, "upper_lip_pos"

    move-object/from16 v0, v16

    invoke-virtual {v15, v0, v14}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/filterpacks/facedetect/OverlayFaceResultFilter;->mOverlayProgram:Landroid/filterfw/core/ShaderProgram;

    const-string v16, "lower_lip_pos"

    move-object/from16 v0, v16

    invoke-virtual {v15, v0, v9}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    if-lez v6, :cond_0

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/filterpacks/facedetect/OverlayFaceResultFilter;->mOverlayProgram:Landroid/filterfw/core/ShaderProgram;

    const/16 v16, 0x0

    aget v16, v3, v16

    const/16 v17, 0x1

    aget v17, v3, v17

    const/16 v18, 0x2

    aget v18, v3, v18

    const/16 v19, 0x0

    aget v19, v3, v19

    sub-float v18, v18, v19

    const/16 v19, 0x3

    aget v19, v3, v19

    const/16 v20, 0x1

    aget v20, v3, v20

    sub-float v19, v19, v20

    invoke-virtual/range {v15 .. v19}, Landroid/filterfw/core/ShaderProgram;->setSourceRect(FFFF)V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/filterpacks/facedetect/OverlayFaceResultFilter;->mOverlayProgram:Landroid/filterfw/core/ShaderProgram;

    const/16 v16, 0x0

    aget v16, v3, v16

    const/16 v17, 0x1

    aget v17, v3, v17

    const/16 v18, 0x2

    aget v18, v3, v18

    const/16 v19, 0x0

    aget v19, v3, v19

    sub-float v18, v18, v19

    const/16 v19, 0x3

    aget v19, v3, v19

    const/16 v20, 0x1

    aget v20, v3, v20

    sub-float v19, v19, v20

    invoke-virtual/range {v15 .. v19}, Landroid/filterfw/core/ShaderProgram;->setTargetRect(FFFF)V

    :cond_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/filterpacks/facedetect/OverlayFaceResultFilter;->mOverlayProgram:Landroid/filterfw/core/ShaderProgram;

    invoke-virtual {v15, v7, v12}, Landroid/filterfw/core/ShaderProgram;->process(Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_0

    :cond_1
    const-string v15, "image"

    move-object/from16 v0, p0

    invoke-virtual {v0, v15, v12}, Lcom/google/android/filterpacks/facedetect/OverlayFaceResultFilter;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    invoke-virtual {v12}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    :goto_1
    return-void

    :cond_2
    const-string v15, "image"

    move-object/from16 v0, p0

    invoke-virtual {v0, v15, v7}, Lcom/google/android/filterpacks/facedetect/OverlayFaceResultFilter;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    goto :goto_1

    :array_0
    .array-data 4
        0x0
        0x3f800000
        0x0
        0x3f800000
    .end array-data
.end method

.method public setupPorts()V
    .locals 4

    const/4 v2, 0x3

    invoke-static {v2, v2}, Landroid/filterfw/format/ImageFormat;->create(II)Landroid/filterfw/core/MutableFrameFormat;

    move-result-object v1

    const-class v2, Lcom/google/android/filterpacks/facedetect/FaceMeta;

    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/filterfw/format/ObjectFormat;->fromClass(Ljava/lang/Class;I)Landroid/filterfw/core/MutableFrameFormat;

    move-result-object v0

    const-string v2, "image"

    invoke-virtual {p0, v2, v1}, Lcom/google/android/filterpacks/facedetect/OverlayFaceResultFilter;->addMaskedInputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    const-string v2, "faces"

    invoke-virtual {p0, v2, v0}, Lcom/google/android/filterpacks/facedetect/OverlayFaceResultFilter;->addMaskedInputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    const-string v2, "image"

    const-string v3, "image"

    invoke-virtual {p0, v2, v3}, Lcom/google/android/filterpacks/facedetect/OverlayFaceResultFilter;->addOutputBasedOnInput(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
