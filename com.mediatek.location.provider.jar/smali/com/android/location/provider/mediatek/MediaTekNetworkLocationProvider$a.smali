.class Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$a;
.super Ljava/lang/Thread;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;

.field private b:Z


# direct methods
.method constructor <init>(Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;)V
    .locals 1

    iput-object p1, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$a;->a:Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$a;->b:Z

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$a;->b()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_0
    :try_start_2
    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method public b()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$a;->b:Z

    return v0
.end method

.method public run()V
    .locals 3

    monitor-enter p0

    const/4 v0, -0x1

    :try_start_0
    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_NETWOKLOCATION_SERVICE:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaTekNetworkLocationProvider"

    const-string v1, "NetworkLocationProviderThread is started"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {}, Landroid/os/Looper;->prepare()V

    iget-object v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$a;->a:Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;

    new-instance v1, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;

    iget-object v2, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$a;->a:Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;

    invoke-direct {v1, v2}, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;-><init>(Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;)V

    invoke-static {v0, v1}, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->a(Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;)Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$a;->b:Z

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v0, 0xa

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    const-string v1, "NetworkLocationProviderThread"

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/Looper;->loop()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
