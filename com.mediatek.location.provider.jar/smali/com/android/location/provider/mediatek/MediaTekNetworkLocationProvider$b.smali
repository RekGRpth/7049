.class Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;
.super Landroid/os/Handler;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;


# direct methods
.method constructor <init>(Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;)V
    .locals 0

    iput-object p1, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;->a:Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_NETWOKLOCATION_SERVICE:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaTekNetworkLocationProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handle Message is called message id:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;->a:Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;

    invoke-static {v0}, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->e(Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;->a:Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;

    invoke-static {v0}, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->f(Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;->a:Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;

    invoke-static {v0}, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->g(Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;)V

    goto :goto_0

    :pswitch_4
    iget-object v1, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;->a:Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->a(Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_5
    iget-object v1, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;->a:Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->b(Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_6
    iget-object v1, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;->a:Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/location/Location;

    invoke-static {v1, v0}, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->a(Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;Landroid/location/Location;)V

    goto :goto_0

    :pswitch_7
    iget-object v1, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;->a:Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;

    iget v2, p1, Landroid/os/Message;->arg1:I

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/WorkSource;

    invoke-static {v1, v2, v0}, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->a(Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;ILandroid/os/WorkSource;)V

    goto :goto_0

    :pswitch_8
    iget-object v1, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;->a:Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;

    iget v2, p1, Landroid/os/Message;->arg1:I

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/WorkSource;

    invoke-static {v1, v2, v0}, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->b(Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;ILandroid/os/WorkSource;)V

    goto :goto_0

    :pswitch_9
    iget-object v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;->a:Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-static {v0, v1}, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->a(Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method
