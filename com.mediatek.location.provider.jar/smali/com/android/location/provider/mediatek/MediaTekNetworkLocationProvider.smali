.class public Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;
.super Lcom/android/location/provider/LocationProvider;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;,
        Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$a;
    }
.end annotation


# instance fields
.field private a:Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;

.field private b:Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$a;

.field private c:Landroid/content/Context;

.field private d:Lcom/android/location/provider/mediatek/connectivity/LocationTelephonyUtilityInterface;

.field private e:Lcom/android/location/provider/mediatek/connectivity/impl/LocationWifiUtility;

.field private f:Lcom/android/location/provider/mediatek/serviceaccessor/NetworkLocationContentAccessorInterface;

.field private g:Lcom/android/location/provider/mediatek/locationem/NetworkLocationBR;

.field private h:J

.field private i:I

.field private j:Z

.field private k:Landroid/location/Location;

.field private final l:Landroid/util/SparseIntArray;

.field private m:I

.field private n:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/android/location/provider/LocationProvider;-><init>()V

    iput-object v2, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->a:Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;

    iput-object v2, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->b:Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$a;

    iput-object v2, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->c:Landroid/content/Context;

    iput-object v2, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->d:Lcom/android/location/provider/mediatek/connectivity/LocationTelephonyUtilityInterface;

    iput-object v2, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->e:Lcom/android/location/provider/mediatek/connectivity/impl/LocationWifiUtility;

    iput-object v2, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->f:Lcom/android/location/provider/mediatek/serviceaccessor/NetworkLocationContentAccessorInterface;

    iput-object v2, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->g:Lcom/android/location/provider/mediatek/locationem/NetworkLocationBR;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->h:J

    const/4 v0, 0x1

    iput v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->i:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->j:Z

    iput-object v2, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->k:Landroid/location/Location;

    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->l:Landroid/util/SparseIntArray;

    const/16 v0, 0x2710

    iput v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->m:I

    new-instance v0, Lcom/android/location/provider/mediatek/a;

    invoke-direct {v0, p0}, Lcom/android/location/provider/mediatek/a;-><init>(Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;)V

    iput-object v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->n:Ljava/lang/Runnable;

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_NETWOKLOCATION_SERVICE:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaTekNetworkLocationProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MediaTekNetworkLocationProvider() is called context:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iput-object p1, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->c:Landroid/content/Context;

    invoke-direct {p0}, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->a()V

    return-void
.end method

.method static synthetic a(Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;)Landroid/location/Location;
    .locals 1

    iget-object v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->k:Landroid/location/Location;

    return-object v0
.end method

.method static synthetic a(Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;)Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;
    .locals 0

    iput-object p1, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->a:Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;

    return-object p1
.end method

.method private a()V
    .locals 4

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_NETWOKLOCATION_SERVICE:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaTekNetworkLocationProvider"

    const-string v1, "Initialize() is called"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$a;

    invoke-direct {v0, p0}, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$a;-><init>(Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;)V

    iput-object v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->b:Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$a;

    iget-object v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->b:Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$a;

    invoke-virtual {v0}, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$a;->start()V

    iget-object v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->b:Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$a;

    invoke-virtual {v0}, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$a;->a()V

    iget-object v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->a:Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_NETWOKLOCATION_SERVICE:Z

    if-eqz v0, :cond_1

    const-string v0, "MediaTekNetworkLocationProvider"

    const-string v1, "mHandler == null This is caused by the thread is not started"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    new-instance v0, Lcom/android/location/provider/mediatek/connectivity/impl/LocationWifiUtility;

    iget-object v1, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->a:Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;

    invoke-direct {v0, v1, v2}, Lcom/android/location/provider/mediatek/connectivity/impl/LocationWifiUtility;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->e:Lcom/android/location/provider/mediatek/connectivity/impl/LocationWifiUtility;

    invoke-direct {p0}, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->b()Ljava/lang/String;

    move-result-object v1

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_NETWOKLOCATION_SERVICE:Z

    if-eqz v0, :cond_2

    const-string v2, "MediaTekNetworkLocationProvider"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Initialize() is called contentAccessor: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz v1, :cond_4

    move-object v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const-string v0, "Google"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Lcom/android/location/provider/mediatek/serviceaccessor/google/GoogleNetworkLocationContentAccessor;

    iget-object v1, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->a:Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;

    invoke-direct {v0, v1}, Lcom/android/location/provider/mediatek/serviceaccessor/google/GoogleNetworkLocationContentAccessor;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->f:Lcom/android/location/provider/mediatek/serviceaccessor/NetworkLocationContentAccessorInterface;

    :cond_3
    new-instance v0, Lcom/android/location/provider/mediatek/locationem/NetworkLocationBR;

    iget-object v1, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->a:Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;

    invoke-direct {v0, v1, v2}, Lcom/android/location/provider/mediatek/locationem/NetworkLocationBR;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->g:Lcom/android/location/provider/mediatek/locationem/NetworkLocationBR;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.android.mediatek.locationem.locationfrequency"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.android.mediatek.locationem.autotest"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->g:Lcom/android/location/provider/mediatek/locationem/NetworkLocationBR;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void

    :cond_4
    const-string v0, "null"

    goto :goto_0
.end method

.method private a(I)V
    .locals 3

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_NETWOKLOCATION_SERVICE:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaTekNetworkLocationProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleLocationFrencyChanged is started newFrequency:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const v0, 0x1d4c0

    if-ge p1, v0, :cond_1

    if-lez p1, :cond_1

    mul-int/lit16 v0, p1, 0x3e8

    iput v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->m:I

    :cond_1
    return-void
.end method

.method private a(ILandroid/os/WorkSource;)V
    .locals 4

    iget-object v1, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->l:Landroid/util/SparseIntArray;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->l:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseIntArray;->indexOfKey(I)I

    move-result v0

    if-ltz v0, :cond_1

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_NETWOKLOCATION_SERVICE:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaTekNetworkLocationProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Duplicate add listener for uid "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    monitor-exit v1

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->l:Landroid/util/SparseIntArray;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v2}, Landroid/util/SparseIntArray;->put(II)V

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(Landroid/location/Location;)V
    .locals 3

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_NETWOKLOCATION_SERVICE:Z

    if-eqz v0, :cond_0

    const-string v1, "MediaTekNetworkLocationProvider"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleReportLocation is called location:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz p1, :cond_2

    move-object v0, p1

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    if-eqz p1, :cond_1

    iput-object p1, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->k:Landroid/location/Location;

    invoke-virtual {p0, p1}, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->reportLocation(Landroid/location/Location;)V

    :cond_1
    return-void

    :cond_2
    const-string v0, "null"

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;ILandroid/os/WorkSource;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->a(ILandroid/os/WorkSource;)V

    return-void
.end method

.method static synthetic a(Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;Landroid/location/Location;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->a(Landroid/location/Location;)V

    return-void
.end method

.method static synthetic a(Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->f:Lcom/android/location/provider/mediatek/serviceaccessor/NetworkLocationContentAccessorInterface;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->f:Lcom/android/location/provider/mediatek/serviceaccessor/NetworkLocationContentAccessorInterface;

    invoke-virtual {v0, p1}, Lcom/android/location/provider/mediatek/serviceaccessor/NetworkLocationContentAccessorInterface;->finishGetLocationByCellIdAndWifi(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    :cond_0
    sget-boolean v1, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_NETWOKLOCATION_SERVICE:Z

    if-eqz v1, :cond_1

    const-string v2, "MediaTekNetworkLocationProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleSendSucessfully() is called with location: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz v0, :cond_3

    move-object v1, v0

    :goto_0
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    if-eqz v0, :cond_2

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    const/16 v2, 0xa

    iput v2, v1, Landroid/os/Message;->what:I

    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->a:Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;

    invoke-virtual {v0, v1}, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;->sendMessage(Landroid/os/Message;)Z

    :cond_2
    return-void

    :cond_3
    const-string v1, "null"

    goto :goto_0
.end method

.method private a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/location/provider/mediatek/utility/CellInfo;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/location/provider/mediatek/utility/WifiInfo;",
            ">;)V"
        }
    .end annotation

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_NETWOKLOCATION_SERVICE:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaTekNetworkLocationProvider"

    const-string v1, "get frist location getWifiandCellInfo() is called"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    invoke-virtual {p2}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->d:Lcom/android/location/provider/mediatek/connectivity/LocationTelephonyUtilityInterface;

    invoke-virtual {v0}, Lcom/android/location/provider/mediatek/connectivity/LocationTelephonyUtilityInterface;->getCellInfo()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->e:Lcom/android/location/provider/mediatek/connectivity/impl/LocationWifiUtility;

    invoke-virtual {v0}, Lcom/android/location/provider/mediatek/connectivity/impl/LocationWifiUtility;->getWifiMacInfo()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_NETWOKLOCATION_SERVICE:Z

    if-eqz v0, :cond_1

    const-string v0, "MediaTekNetworkLocationProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getWifiandCellInfo() is called with CellInfoList_Size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "WifiMacInfoList_Size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "MediaTekNetworkLocationProvider"

    invoke-virtual {p1}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/location/provider/mediatek/utility/LocationLogUtility;->writeLargeContentLog(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "MediaTekNetworkLocationProvider"

    invoke-virtual {p2}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/location/provider/mediatek/utility/LocationLogUtility;->writeLargeContentLog(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void
.end method

.method static synthetic b(Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;)Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;
    .locals 1

    iget-object v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->a:Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;

    return-object v0
.end method

.method private b()Ljava/lang/String;
    .locals 8

    const/4 v1, 0x0

    const/4 v4, 0x1

    const-string v3, "Google"

    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v5

    iget-object v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    :try_start_0
    const-string v2, "settings.xml"

    invoke-virtual {v0, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v5, v2, v0}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_9
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v1

    const/4 v0, 0x0

    move v7, v0

    move-object v0, v3

    move v3, v1

    move v1, v7

    :goto_0
    if-eq v3, v4, :cond_3

    if-nez v1, :cond_3

    packed-switch v3, :pswitch_data_0

    :cond_0
    :pswitch_0
    move v7, v1

    move-object v1, v0

    move v0, v7

    :goto_1
    :try_start_2
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->next()I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_a
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v3

    move v7, v0

    move-object v0, v1

    move v1, v7

    goto :goto_0

    :pswitch_1
    move v7, v1

    move-object v1, v0

    move v0, v7

    goto :goto_1

    :pswitch_2
    :try_start_3
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v6, "NetworkLocationServiceAccessor"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v0

    move v7, v1

    move-object v1, v0

    move v0, v7

    goto :goto_1

    :pswitch_3
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v6, "NetworkLocationConfig"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_b
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_3 .. :try_end_3} :catch_8
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v3

    if-eqz v3, :cond_0

    move-object v1, v0

    move v0, v4

    goto :goto_1

    :catch_0
    move-exception v0

    move-object v2, v1

    move-object v1, v0

    move-object v0, v3

    :goto_2
    :try_start_4
    const-string v3, "MediaTekNetworkLocationProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "loadConfig IOException"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    if-eqz v2, :cond_1

    :try_start_5
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    :cond_1
    :goto_3
    return-object v0

    :catch_1
    move-exception v0

    move-object v2, v1

    move-object v1, v0

    move-object v0, v3

    :goto_4
    :try_start_6
    const-string v3, "MediaTekNetworkLocationProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "loadConfig XmlPullParserException"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lorg/xmlpull/v1/XmlPullParserException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    if-eqz v2, :cond_1

    :try_start_7
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    goto :goto_3

    :catch_2
    move-exception v1

    const-string v2, "MediaTekNetworkLocationProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "loadConfig IOException"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_5
    invoke-static {v2, v1}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_6
    if-eqz v2, :cond_2

    :try_start_8
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    :cond_2
    :goto_7
    throw v0

    :catch_3
    move-exception v1

    const-string v2, "MediaTekNetworkLocationProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "loadConfig IOException"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    :catch_4
    move-exception v1

    const-string v2, "MediaTekNetworkLocationProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "loadConfig IOException"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_5

    :cond_3
    if-eqz v2, :cond_1

    :try_start_9
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5

    goto/16 :goto_3

    :catch_5
    move-exception v1

    const-string v2, "MediaTekNetworkLocationProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "loadConfig IOException"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_5

    :catchall_1
    move-exception v0

    goto :goto_6

    :catch_6
    move-exception v0

    move-object v1, v0

    move-object v0, v3

    goto/16 :goto_4

    :catch_7
    move-exception v0

    move-object v7, v0

    move-object v0, v1

    move-object v1, v7

    goto/16 :goto_4

    :catch_8
    move-exception v1

    goto/16 :goto_4

    :catch_9
    move-exception v0

    move-object v1, v0

    move-object v0, v3

    goto/16 :goto_2

    :catch_a
    move-exception v0

    move-object v7, v0

    move-object v0, v1

    move-object v1, v7

    goto/16 :goto_2

    :catch_b
    move-exception v1

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private b(I)V
    .locals 1

    iget-object v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->a:Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;

    invoke-virtual {v0, p1}, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;->removeMessages(I)V

    return-void
.end method

.method private b(ILandroid/os/WorkSource;)V
    .locals 4

    iget-object v1, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->l:Landroid/util/SparseIntArray;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->l:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseIntArray;->indexOfKey(I)I

    move-result v0

    if-gez v0, :cond_1

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_NETWOKLOCATION_SERVICE:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaTekNetworkLocationProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unneeded remove listener for uid "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    monitor-exit v1

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->l:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseIntArray;->delete(I)V

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic b(Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;ILandroid/os/WorkSource;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->b(ILandroid/os/WorkSource;)V

    return-void
.end method

.method static synthetic b(Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->b(Ljava/lang/String;)V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 4

    const/4 v3, 0x3

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_NETWOKLOCATION_SERVICE:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaTekNetworkLocationProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error has occured when sending google server to get location errorString: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->i:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->a:Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;

    invoke-virtual {v0, v3}, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->a:Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;

    iget-object v1, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->a:Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;

    invoke-static {v1, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    const-wide/32 v2, 0xea60

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_1
    return-void
.end method

.method static synthetic c(Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->n:Ljava/lang/Runnable;

    return-object v0
.end method

.method private c()V
    .locals 2

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_NETWOKLOCATION_SERVICE:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaTekNetworkLocationProvider"

    const-string v1, "unInitializeEnableLocationTracking is called"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->d:Lcom/android/location/provider/mediatek/connectivity/LocationTelephonyUtilityInterface;

    invoke-virtual {v0}, Lcom/android/location/provider/mediatek/connectivity/LocationTelephonyUtilityInterface;->unRegisterListener()V

    iget-object v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->e:Lcom/android/location/provider/mediatek/connectivity/impl/LocationWifiUtility;

    invoke-virtual {v0}, Lcom/android/location/provider/mediatek/connectivity/impl/LocationWifiUtility;->unregisterWifiMacListerner()V

    return-void
.end method

.method static synthetic d(Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;)I
    .locals 1

    iget v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->m:I

    return v0
.end method

.method private d()V
    .locals 2

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_NETWOKLOCATION_SERVICE:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaTekNetworkLocationProvider"

    const-string v1, "initializeEnableLocationTracking is called"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->d:Lcom/android/location/provider/mediatek/connectivity/LocationTelephonyUtilityInterface;

    invoke-virtual {v0}, Lcom/android/location/provider/mediatek/connectivity/LocationTelephonyUtilityInterface;->registerListener()V

    iget-object v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->e:Lcom/android/location/provider/mediatek/connectivity/impl/LocationWifiUtility;

    invoke-virtual {v0}, Lcom/android/location/provider/mediatek/connectivity/impl/LocationWifiUtility;->registerWifiMacListerner()V

    iget-object v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->a:Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;

    iget-object v1, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->n:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private e()V
    .locals 3

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_NETWOKLOCATION_SERVICE:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaTekNetworkLocationProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleEnableLocationTracking is called m_isStarted: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->j:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-boolean v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->j:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->d()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->j:Z

    iget v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->i:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x3

    iput v1, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->a:Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;

    invoke-virtual {v1, v0}, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    return-void
.end method

.method static synthetic e(Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->e()V

    return-void
.end method

.method private f()V
    .locals 3

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_NETWOKLOCATION_SERVICE:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaTekNetworkLocationProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleDisableLocationTracking is started m_isStarted: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->j:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-boolean v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->j:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->c()V

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->b(I)V

    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->b(I)V

    iget-object v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->a:Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;

    iget-object v1, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->n:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;->removeCallbacks(Ljava/lang/Runnable;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->j:Z

    :cond_1
    return-void
.end method

.method static synthetic f(Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->f()V

    return-void
.end method

.method private g()V
    .locals 5

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_NETWOKLOCATION_SERVICE:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaTekNetworkLocationProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleGetLocationFromServer() is called: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->i:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->i:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {p0, v1, v0}, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    sget-boolean v2, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_NETWOKLOCATION_SERVICE:Z

    if-eqz v2, :cond_1

    const-string v2, "MediaTekNetworkLocationProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "handleGetLocationFromServer() is called CellInfoList: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "WifiMacInfoList: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v2, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->f:Lcom/android/location/provider/mediatek/serviceaccessor/NetworkLocationContentAccessorInterface;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->f:Lcom/android/location/provider/mediatek/serviceaccessor/NetworkLocationContentAccessorInterface;

    invoke-virtual {v2, v0, v1}, Lcom/android/location/provider/mediatek/serviceaccessor/NetworkLocationContentAccessorInterface;->startGetLocationByCellIdAndWifi(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    :cond_2
    return-void
.end method

.method static synthetic g(Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->g()V

    return-void
.end method


# virtual methods
.method public onAddListener(ILandroid/os/WorkSource;)V
    .locals 3

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_NETWOKLOCATION_SERVICE:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaTekNetworkLocationProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onAddListener is called with uid:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "WorkSource"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    const/16 v1, 0xc

    iput v1, v0, Landroid/os/Message;->what:I

    iput p1, v0, Landroid/os/Message;->arg1:I

    iput-object p2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v1, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->a:Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;

    invoke-virtual {v1, v0}, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public onDisable()V
    .locals 2

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_NETWOKLOCATION_SERVICE:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaTekNetworkLocationProvider"

    const-string v1, "onDisable is called"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public onEnable()V
    .locals 2

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_NETWOKLOCATION_SERVICE:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaTekNetworkLocationProvider"

    const-string v1, "onEnable is called"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public onEnableLocationTracking(Z)V
    .locals 3

    const-string v0, "MediaTekNetworkLocationProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onEnableLocationTracking is called enable:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    if-eqz p1, :cond_0

    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->what:I

    :goto_0
    iget-object v1, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->a:Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;

    invoke-virtual {v1, v0}, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;->sendMessage(Landroid/os/Message;)Z

    return-void

    :cond_0
    const/4 v1, 0x2

    iput v1, v0, Landroid/os/Message;->what:I

    goto :goto_0
.end method

.method public onGetAccuracy()I
    .locals 2

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_NETWOKLOCATION_SERVICE:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaTekNetworkLocationProvider"

    const-string v1, "onGetAccuracy is called"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x2

    return v0
.end method

.method public onGetInternalState()Ljava/lang/String;
    .locals 2

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_NETWOKLOCATION_SERVICE:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaTekNetworkLocationProvider"

    const-string v1, "onGetInternalState is called"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public onGetPowerRequirement()I
    .locals 2

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_NETWOKLOCATION_SERVICE:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaTekNetworkLocationProvider"

    const-string v1, "onGetPowerRequirement is called"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x2

    return v0
.end method

.method public onGetStatus(Landroid/os/Bundle;)I
    .locals 3

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_NETWOKLOCATION_SERVICE:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaTekNetworkLocationProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onGetStatus is called extras"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->i:I

    return v0
.end method

.method public onGetStatusUpdateTime()J
    .locals 2

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_NETWOKLOCATION_SERVICE:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaTekNetworkLocationProvider"

    const-string v1, "onGetStatusUpdateTime is called"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-wide v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->h:J

    return-wide v0
.end method

.method public onHasMonetaryCost()Z
    .locals 2

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_NETWOKLOCATION_SERVICE:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaTekNetworkLocationProvider"

    const-string v1, "onHasMonetaryCost is called"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onMeetsCriteria(Landroid/location/Criteria;)Z
    .locals 4

    const/4 v0, 0x1

    sget-boolean v1, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_NETWOKLOCATION_SERVICE:Z

    if-eqz v1, :cond_0

    const-string v1, "MediaTekNetworkLocationProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onMeetsCriteria is called criteria:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p1}, Landroid/location/Criteria;->getAccuracy()I

    move-result v1

    if-eq v1, v0, :cond_1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onRemoveListener(ILandroid/os/WorkSource;)V
    .locals 3

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_NETWOKLOCATION_SERVICE:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaTekNetworkLocationProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onRemoveListener is called with uid:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "WorkSource"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    const/16 v1, 0xd

    iput v1, v0, Landroid/os/Message;->what:I

    iput p1, v0, Landroid/os/Message;->arg1:I

    iput-object p2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v1, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->a:Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;

    invoke-virtual {v1, v0}, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public onRequiresCell()Z
    .locals 2

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_NETWOKLOCATION_SERVICE:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaTekNetworkLocationProvider"

    const-string v1, "onRequiresCell is called"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onRequiresNetwork()Z
    .locals 2

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_NETWOKLOCATION_SERVICE:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaTekNetworkLocationProvider"

    const-string v1, "onRequiresNetwork is called"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onRequiresSatellite()Z
    .locals 2

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_NETWOKLOCATION_SERVICE:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaTekNetworkLocationProvider"

    const-string v1, "onRequiresSatellite is called"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onSendExtraCommand(Ljava/lang/String;Landroid/os/Bundle;)Z
    .locals 3

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_NETWOKLOCATION_SERVICE:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaTekNetworkLocationProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSendExtraCommand is called command:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "extras:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onSetMinTime(JLandroid/os/WorkSource;)V
    .locals 3

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_NETWOKLOCATION_SERVICE:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaTekNetworkLocationProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSetMinTime is called command:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "WorkSource:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public onSupportsAltitude()Z
    .locals 2

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_NETWOKLOCATION_SERVICE:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaTekNetworkLocationProvider"

    const-string v1, "onSupportsAltitude is called "

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onSupportsBearing()Z
    .locals 2

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_NETWOKLOCATION_SERVICE:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaTekNetworkLocationProvider"

    const-string v1, "onSupportsBearing is called "

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onSupportsSpeed()Z
    .locals 2

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_NETWOKLOCATION_SERVICE:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaTekNetworkLocationProvider"

    const-string v1, "onSupportsSpeed is called "

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onUpdateLocation(Landroid/location/Location;)V
    .locals 3

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_NETWOKLOCATION_SERVICE:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaTekNetworkLocationProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onUpdateLocation is called location:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/location/Location;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public onUpdateNetworkState(ILandroid/net/NetworkInfo;)V
    .locals 3

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_NETWOKLOCATION_SERVICE:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaTekNetworkLocationProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onUpdateNetworkState is called state:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "NetworkInfo"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iput p1, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->i:I

    iget v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->i:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x3

    iput v1, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->a:Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;

    invoke-virtual {v1, v0}, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->h:J

    return-void
.end method

.method public setGEMINISUPPORT(Z)V
    .locals 2

    if-eqz p1, :cond_0

    new-instance v0, Lcom/android/location/provider/mediatek/connectivity/impl/LocationTelephonyGeminiUtility;

    iget-object v1, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/location/provider/mediatek/connectivity/impl/LocationTelephonyGeminiUtility;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->d:Lcom/android/location/provider/mediatek/connectivity/LocationTelephonyUtilityInterface;

    :goto_0
    iget-object v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->d:Lcom/android/location/provider/mediatek/connectivity/LocationTelephonyUtilityInterface;

    iget-object v1, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->a:Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider$b;

    invoke-virtual {v0, v1}, Lcom/android/location/provider/mediatek/connectivity/LocationTelephonyUtilityInterface;->addLocationPhoneStateListener(Landroid/os/Handler;)V

    return-void

    :cond_0
    new-instance v0, Lcom/android/location/provider/mediatek/connectivity/impl/LocationTelephonyBasicUtility;

    iget-object v1, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/location/provider/mediatek/connectivity/impl/LocationTelephonyBasicUtility;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/location/provider/mediatek/MediaTekNetworkLocationProvider;->d:Lcom/android/location/provider/mediatek/connectivity/LocationTelephonyUtilityInterface;

    goto :goto_0
.end method
