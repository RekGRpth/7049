.class public abstract Lcom/android/location/provider/mediatek/connectivity/LocationPhoneStateListenerInterface;
.super Landroid/telephony/PhoneStateListener;
.source "SourceFile"


# static fields
.field public static sIsGEMINISUPPORT:Z


# instance fields
.field public mContext:Landroid/content/Context;

.field public mHandler:Landroid/os/Handler;

.field public mNetworkType:Lcom/android/location/provider/mediatek/connectivity/Network_Type;

.field public mRadioType:Ljava/lang/String;

.field public mServiceState:Ljava/lang/Boolean;

.field public mSimID:I

.field public mSingalStrength:I

.field public mTelephonyManager:Landroid/telephony/TelephonyManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/location/provider/mediatek/connectivity/LocationPhoneStateListenerInterface;->sIsGEMINISUPPORT:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;I)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    iput-object v1, p0, Lcom/android/location/provider/mediatek/connectivity/LocationPhoneStateListenerInterface;->mContext:Landroid/content/Context;

    iput-object v1, p0, Lcom/android/location/provider/mediatek/connectivity/LocationPhoneStateListenerInterface;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    iput-object v1, p0, Lcom/android/location/provider/mediatek/connectivity/LocationPhoneStateListenerInterface;->mHandler:Landroid/os/Handler;

    sget-object v0, Lcom/android/location/provider/mediatek/connectivity/Network_Type;->Network_2G:Lcom/android/location/provider/mediatek/connectivity/Network_Type;

    iput-object v0, p0, Lcom/android/location/provider/mediatek/connectivity/LocationPhoneStateListenerInterface;->mNetworkType:Lcom/android/location/provider/mediatek/connectivity/Network_Type;

    iput-object v1, p0, Lcom/android/location/provider/mediatek/connectivity/LocationPhoneStateListenerInterface;->mRadioType:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/android/location/provider/mediatek/connectivity/LocationPhoneStateListenerInterface;->mServiceState:Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/android/location/provider/mediatek/connectivity/LocationPhoneStateListenerInterface;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/android/location/provider/mediatek/connectivity/LocationPhoneStateListenerInterface;->mContext:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/android/location/provider/mediatek/connectivity/LocationPhoneStateListenerInterface;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    iput-object p2, p0, Lcom/android/location/provider/mediatek/connectivity/LocationPhoneStateListenerInterface;->mHandler:Landroid/os/Handler;

    iput v2, p0, Lcom/android/location/provider/mediatek/connectivity/LocationPhoneStateListenerInterface;->mSingalStrength:I

    iput p3, p0, Lcom/android/location/provider/mediatek/connectivity/LocationPhoneStateListenerInterface;->mSimID:I

    invoke-direct {p0, p3}, Lcom/android/location/provider/mediatek/connectivity/LocationPhoneStateListenerInterface;->a(I)Lcom/android/location/provider/mediatek/connectivity/Network_Type;

    move-result-object v0

    iput-object v0, p0, Lcom/android/location/provider/mediatek/connectivity/LocationPhoneStateListenerInterface;->mNetworkType:Lcom/android/location/provider/mediatek/connectivity/Network_Type;

    return-void
.end method

.method private a(I)Lcom/android/location/provider/mediatek/connectivity/Network_Type;
    .locals 3

    const/4 v0, 0x0

    sget-object v1, Lcom/android/location/provider/mediatek/connectivity/Network_Type;->Network_2G:Lcom/android/location/provider/mediatek/connectivity/Network_Type;

    if-nez p1, :cond_3

    const-string v0, "gsm.network.type"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_0
    const-string v2, "UMTS"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "HSPA"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "HSDPA"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    sget-object v1, Lcom/android/location/provider/mediatek/connectivity/Network_Type;->Network_3G:Lcom/android/location/provider/mediatek/connectivity/Network_Type;

    :cond_2
    iput-object v0, p0, Lcom/android/location/provider/mediatek/connectivity/LocationPhoneStateListenerInterface;->mRadioType:Ljava/lang/String;

    return-object v1

    :cond_3
    const/4 v2, 0x1

    if-ne p1, v2, :cond_0

    const-string v0, "gsm.network.type.2"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public abstract getCellInfo()Lcom/android/location/provider/mediatek/utility/CellInfo;
.end method

.method public getNetworkType()Lcom/android/location/provider/mediatek/connectivity/Network_Type;
    .locals 1

    iget-object v0, p0, Lcom/android/location/provider/mediatek/connectivity/LocationPhoneStateListenerInterface;->mNetworkType:Lcom/android/location/provider/mediatek/connectivity/Network_Type;

    return-object v0
.end method

.method public getServiceState()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/android/location/provider/mediatek/connectivity/LocationPhoneStateListenerInterface;->mServiceState:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getSimID()I
    .locals 1

    iget v0, p0, Lcom/android/location/provider/mediatek/connectivity/LocationPhoneStateListenerInterface;->mSimID:I

    return v0
.end method

.method public onCellLocationChanged(Landroid/telephony/CellLocation;)V
    .locals 3

    const-string v1, "LocationPhoneStateListenerInterface"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "OnCellLocationChanged is called SimID: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v0, p0, Lcom/android/location/provider/mediatek/connectivity/LocationPhoneStateListenerInterface;->mSimID:I

    if-nez v0, :cond_0

    const-string v0, "SIM1"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "CellLocation: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "Thread_Name:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x3

    iput v1, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/android/location/provider/mediatek/connectivity/LocationPhoneStateListenerInterface;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :cond_0
    const-string v0, "SIM2"

    goto :goto_0
.end method

.method public onServiceStateChanged(Landroid/telephony/ServiceState;)V
    .locals 3

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_NETWOKLOCATION_SERVICE:Z

    if-eqz v0, :cond_0

    const-string v1, "LocationPhoneStateListenerInterface"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onServiceStateChanged is called serviceState: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "m_sim: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v0, p0, Lcom/android/location/provider/mediatek/connectivity/LocationPhoneStateListenerInterface;->mSimID:I

    if-nez v0, :cond_1

    const-string v0, "SIM1"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getState()I

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/android/location/provider/mediatek/connectivity/LocationPhoneStateListenerInterface;->mServiceState:Ljava/lang/Boolean;

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x3

    iput v1, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/android/location/provider/mediatek/connectivity/LocationPhoneStateListenerInterface;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :goto_1
    return-void

    :cond_1
    const-string v0, "SIM2"

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/android/location/provider/mediatek/connectivity/LocationPhoneStateListenerInterface;->mServiceState:Ljava/lang/Boolean;

    goto :goto_1
.end method

.method public onSignalStrengthChanged(Landroid/telephony/SignalStrength;)V
    .locals 3

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_NETWOKLOCATION_SERVICE:Z

    if-eqz v0, :cond_0

    const-string v1, "LocationPhoneStateListenerInterface"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "OnSignalStrengthChanged is called SimID: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v0, p0, Lcom/android/location/provider/mediatek/connectivity/LocationPhoneStateListenerInterface;->mSimID:I

    if-nez v0, :cond_2

    const-string v0, "SIM1"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->isGsm()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->getGsmSignalStrengthDbm()I

    move-result v0

    iput v0, p0, Lcom/android/location/provider/mediatek/connectivity/LocationPhoneStateListenerInterface;->mSingalStrength:I

    :cond_1
    return-void

    :cond_2
    const-string v0, "SIM2"

    goto :goto_0
.end method
