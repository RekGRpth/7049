.class public abstract Lcom/android/location/provider/mediatek/connectivity/LocationTelephonyUtilityInterface;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public mContext:Landroid/content/Context;

.field public mLocationPhoneStateListenerList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/location/provider/mediatek/connectivity/LocationPhoneStateListenerInterface;",
            ">;"
        }
    .end annotation
.end field

.field public mTelephonyMgr:Landroid/telephony/TelephonyManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/location/provider/mediatek/connectivity/LocationTelephonyUtilityInterface;->mContext:Landroid/content/Context;

    iput-object v0, p0, Lcom/android/location/provider/mediatek/connectivity/LocationTelephonyUtilityInterface;->mTelephonyMgr:Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/android/location/provider/mediatek/connectivity/LocationTelephonyUtilityInterface;->mLocationPhoneStateListenerList:Ljava/util/List;

    iput-object p1, p0, Lcom/android/location/provider/mediatek/connectivity/LocationTelephonyUtilityInterface;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/android/location/provider/mediatek/connectivity/LocationTelephonyUtilityInterface;->mContext:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/android/location/provider/mediatek/connectivity/LocationTelephonyUtilityInterface;->mTelephonyMgr:Landroid/telephony/TelephonyManager;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/location/provider/mediatek/connectivity/LocationTelephonyUtilityInterface;->mLocationPhoneStateListenerList:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public abstract addLocationPhoneStateListener(Landroid/os/Handler;)V
.end method

.method public getCellInfo()Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/android/location/provider/mediatek/utility/CellInfo;",
            ">;"
        }
    .end annotation

    const/4 v8, 0x0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-object v4, p0, Lcom/android/location/provider/mediatek/connectivity/LocationTelephonyUtilityInterface;->mLocationPhoneStateListenerList:Ljava/util/List;

    monitor-enter v4

    :try_start_0
    iget-object v0, p0, Lcom/android/location/provider/mediatek/connectivity/LocationTelephonyUtilityInterface;->mLocationPhoneStateListenerList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/location/provider/mediatek/connectivity/LocationPhoneStateListenerInterface;

    const/4 v2, 0x0

    invoke-virtual {v0}, Lcom/android/location/provider/mediatek/connectivity/LocationPhoneStateListenerInterface;->getServiceState()Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v0}, Lcom/android/location/provider/mediatek/connectivity/LocationPhoneStateListenerInterface;->getCellInfo()Lcom/android/location/provider/mediatek/utility/CellInfo;

    move-result-object v2

    :cond_1
    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/android/location/provider/mediatek/connectivity/LocationPhoneStateListenerInterface;->getNetworkType()Lcom/android/location/provider/mediatek/connectivity/Network_Type;

    move-result-object v6

    sget-object v7, Lcom/android/location/provider/mediatek/connectivity/Network_Type;->Network_2G:Lcom/android/location/provider/mediatek/connectivity/Network_Type;

    if-ne v6, v7, :cond_2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    invoke-virtual {v0}, Lcom/android/location/provider/mediatek/connectivity/LocationPhoneStateListenerInterface;->getNetworkType()Lcom/android/location/provider/mediatek/connectivity/Network_Type;

    move-result-object v0

    sget-object v6, Lcom/android/location/provider/mediatek/connectivity/Network_Type;->Network_3G:Lcom/android/location/provider/mediatek/connectivity/Network_Type;

    if-ne v0, v6, :cond_0

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    return-object v0

    :cond_4
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_5
    move-object v0, v1

    goto :goto_1
.end method

.method public abstract registerListener()V
.end method

.method public abstract unRegisterListener()V
.end method
