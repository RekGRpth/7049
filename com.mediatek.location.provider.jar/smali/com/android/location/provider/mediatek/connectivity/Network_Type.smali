.class public final enum Lcom/android/location/provider/mediatek/connectivity/Network_Type;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/location/provider/mediatek/connectivity/Network_Type;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum Network_2G:Lcom/android/location/provider/mediatek/connectivity/Network_Type;

.field public static final enum Network_3G:Lcom/android/location/provider/mediatek/connectivity/Network_Type;

.field public static final enum Network_4G:Lcom/android/location/provider/mediatek/connectivity/Network_Type;

.field private static final synthetic a:[Lcom/android/location/provider/mediatek/connectivity/Network_Type;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/android/location/provider/mediatek/connectivity/Network_Type;

    const-string v1, "Network_2G"

    invoke-direct {v0, v1, v2}, Lcom/android/location/provider/mediatek/connectivity/Network_Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/location/provider/mediatek/connectivity/Network_Type;->Network_2G:Lcom/android/location/provider/mediatek/connectivity/Network_Type;

    new-instance v0, Lcom/android/location/provider/mediatek/connectivity/Network_Type;

    const-string v1, "Network_3G"

    invoke-direct {v0, v1, v3}, Lcom/android/location/provider/mediatek/connectivity/Network_Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/location/provider/mediatek/connectivity/Network_Type;->Network_3G:Lcom/android/location/provider/mediatek/connectivity/Network_Type;

    new-instance v0, Lcom/android/location/provider/mediatek/connectivity/Network_Type;

    const-string v1, "Network_4G"

    invoke-direct {v0, v1, v4}, Lcom/android/location/provider/mediatek/connectivity/Network_Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/location/provider/mediatek/connectivity/Network_Type;->Network_4G:Lcom/android/location/provider/mediatek/connectivity/Network_Type;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/android/location/provider/mediatek/connectivity/Network_Type;

    sget-object v1, Lcom/android/location/provider/mediatek/connectivity/Network_Type;->Network_2G:Lcom/android/location/provider/mediatek/connectivity/Network_Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/location/provider/mediatek/connectivity/Network_Type;->Network_3G:Lcom/android/location/provider/mediatek/connectivity/Network_Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/location/provider/mediatek/connectivity/Network_Type;->Network_4G:Lcom/android/location/provider/mediatek/connectivity/Network_Type;

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/location/provider/mediatek/connectivity/Network_Type;->a:[Lcom/android/location/provider/mediatek/connectivity/Network_Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/location/provider/mediatek/connectivity/Network_Type;
    .locals 1

    const-class v0, Lcom/android/location/provider/mediatek/connectivity/Network_Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/location/provider/mediatek/connectivity/Network_Type;

    return-object v0
.end method

.method public static values()[Lcom/android/location/provider/mediatek/connectivity/Network_Type;
    .locals 1

    sget-object v0, Lcom/android/location/provider/mediatek/connectivity/Network_Type;->a:[Lcom/android/location/provider/mediatek/connectivity/Network_Type;

    invoke-virtual {v0}, [Lcom/android/location/provider/mediatek/connectivity/Network_Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/location/provider/mediatek/connectivity/Network_Type;

    return-object v0
.end method
