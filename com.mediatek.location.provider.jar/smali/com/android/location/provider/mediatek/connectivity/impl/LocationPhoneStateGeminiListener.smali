.class public Lcom/android/location/provider/mediatek/connectivity/impl/LocationPhoneStateGeminiListener;
.super Lcom/android/location/provider/mediatek/connectivity/LocationPhoneStateListenerInterface;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/location/provider/mediatek/connectivity/LocationPhoneStateListenerInterface;-><init>(Landroid/content/Context;Landroid/os/Handler;I)V

    return-void
.end method


# virtual methods
.method public getCellInfo()Lcom/android/location/provider/mediatek/utility/CellInfo;
    .locals 6

    const/4 v3, 0x5

    const/4 v5, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/location/provider/mediatek/connectivity/impl/LocationPhoneStateGeminiListener;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    iget v2, p0, Lcom/android/location/provider/mediatek/connectivity/impl/LocationPhoneStateGeminiListener;->mSimID:I

    invoke-virtual {v0, v2}, Landroid/telephony/TelephonyManager;->getSimStateGemini(I)I

    move-result v0

    if-ne v0, v3, :cond_6

    iget-object v0, p0, Lcom/android/location/provider/mediatek/connectivity/impl/LocationPhoneStateGeminiListener;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    iget v2, p0, Lcom/android/location/provider/mediatek/connectivity/impl/LocationPhoneStateGeminiListener;->mSimID:I

    invoke-virtual {v0, v2}, Landroid/telephony/TelephonyManager;->getNetworkOperatorGemini(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x0

    const/4 v3, 0x3

    :try_start_0
    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x3

    const/4 v4, 0x5

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    iget-object v0, p0, Lcom/android/location/provider/mediatek/connectivity/impl/LocationPhoneStateGeminiListener;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    iget v4, p0, Lcom/android/location/provider/mediatek/connectivity/impl/LocationPhoneStateGeminiListener;->mSimID:I

    invoke-virtual {v0, v4}, Landroid/telephony/TelephonyManager;->getCellLocationGemini(I)Landroid/telephony/CellLocation;

    move-result-object v0

    check-cast v0, Landroid/telephony/gsm/GsmCellLocation;

    if-eqz v0, :cond_6

    new-instance v1, Lcom/android/location/provider/mediatek/utility/CellInfo;

    invoke-direct {v1}, Lcom/android/location/provider/mediatek/utility/CellInfo;-><init>()V

    iget-object v4, v1, Lcom/android/location/provider/mediatek/utility/CellInfo;->mMainCell:Lcom/android/location/provider/mediatek/utility/MainCell;

    iput v2, v4, Lcom/android/location/provider/mediatek/utility/MainCell;->mMobileCountryCode:I

    iget-object v2, v1, Lcom/android/location/provider/mediatek/utility/CellInfo;->mMainCell:Lcom/android/location/provider/mediatek/utility/MainCell;

    iput v3, v2, Lcom/android/location/provider/mediatek/utility/MainCell;->mMobileNetworkCode:I

    iget-object v2, v1, Lcom/android/location/provider/mediatek/utility/CellInfo;->mMainCell:Lcom/android/location/provider/mediatek/utility/MainCell;

    invoke-virtual {v0}, Landroid/telephony/gsm/GsmCellLocation;->getCid()I

    move-result v3

    iput v3, v2, Lcom/android/location/provider/mediatek/utility/MainCell;->mCellID:I

    iget-object v2, p0, Lcom/android/location/provider/mediatek/connectivity/impl/LocationPhoneStateGeminiListener;->mNetworkType:Lcom/android/location/provider/mediatek/connectivity/Network_Type;

    sget-object v3, Lcom/android/location/provider/mediatek/connectivity/Network_Type;->Network_2G:Lcom/android/location/provider/mediatek/connectivity/Network_Type;

    if-ne v2, v3, :cond_1

    iget-object v2, v1, Lcom/android/location/provider/mediatek/utility/CellInfo;->mMainCell:Lcom/android/location/provider/mediatek/utility/MainCell;

    invoke-virtual {v0}, Landroid/telephony/gsm/GsmCellLocation;->getLac()I

    move-result v0

    iput v0, v2, Lcom/android/location/provider/mediatek/utility/MainCell;->mLocalAreaCode:I

    :goto_0
    iget-object v0, v1, Lcom/android/location/provider/mediatek/utility/CellInfo;->mMainCell:Lcom/android/location/provider/mediatek/utility/MainCell;

    iget v2, p0, Lcom/android/location/provider/mediatek/connectivity/impl/LocationPhoneStateGeminiListener;->mSingalStrength:I

    iput v2, v0, Lcom/android/location/provider/mediatek/utility/MainCell;->mSignalStrength:I

    iget-object v0, v1, Lcom/android/location/provider/mediatek/utility/CellInfo;->mMainCell:Lcom/android/location/provider/mediatek/utility/MainCell;

    iput v5, v0, Lcom/android/location/provider/mediatek/utility/MainCell;->mAge:I

    iget-object v0, v1, Lcom/android/location/provider/mediatek/utility/CellInfo;->mMainCell:Lcom/android/location/provider/mediatek/utility/MainCell;

    iget-object v2, p0, Lcom/android/location/provider/mediatek/connectivity/impl/LocationPhoneStateGeminiListener;->mRadioType:Ljava/lang/String;

    iput-object v2, v0, Lcom/android/location/provider/mediatek/utility/MainCell;->mRadioType:Ljava/lang/String;

    iget-object v0, v1, Lcom/android/location/provider/mediatek/utility/CellInfo;->mMainCell:Lcom/android/location/provider/mediatek/utility/MainCell;

    iget-object v2, p0, Lcom/android/location/provider/mediatek/connectivity/impl/LocationPhoneStateGeminiListener;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    iget v3, p0, Lcom/android/location/provider/mediatek/connectivity/impl/LocationPhoneStateGeminiListener;->mSimID:I

    invoke-virtual {v2, v3}, Landroid/telephony/TelephonyManager;->getNetworkOperatorNameGemini(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/android/location/provider/mediatek/utility/MainCell;->mCarrier:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/location/provider/mediatek/connectivity/impl/LocationPhoneStateGeminiListener;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    iget v2, p0, Lcom/android/location/provider/mediatek/connectivity/impl/LocationPhoneStateGeminiListener;->mSimID:I

    invoke-virtual {v0, v2}, Landroid/telephony/TelephonyManager;->getNeighboringCellInfoGemini(I)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/NeighboringCellInfo;

    new-instance v3, Lcom/android/location/provider/mediatek/utility/NeighborCell;

    invoke-direct {v3}, Lcom/android/location/provider/mediatek/utility/NeighborCell;-><init>()V

    :try_start_1
    invoke-virtual {v0}, Landroid/telephony/NeighboringCellInfo;->getCid()I

    move-result v4

    iput v4, v3, Lcom/android/location/provider/mediatek/utility/NeighborCell;->mCid:I

    iget-object v4, p0, Lcom/android/location/provider/mediatek/connectivity/impl/LocationPhoneStateGeminiListener;->mNetworkType:Lcom/android/location/provider/mediatek/connectivity/Network_Type;

    sget-object v5, Lcom/android/location/provider/mediatek/connectivity/Network_Type;->Network_2G:Lcom/android/location/provider/mediatek/connectivity/Network_Type;

    if-ne v4, v5, :cond_2

    invoke-virtual {v0}, Landroid/telephony/NeighboringCellInfo;->getLac()I

    move-result v4

    iput v4, v3, Lcom/android/location/provider/mediatek/utility/NeighborCell;->mLac:I

    :goto_2
    invoke-virtual {v0}, Landroid/telephony/NeighboringCellInfo;->getPsc()I

    move-result v4

    iput v4, v3, Lcom/android/location/provider/mediatek/utility/NeighborCell;->mPsc:I

    invoke-virtual {v0}, Landroid/telephony/NeighboringCellInfo;->getRssi()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, -0x71

    iput v0, v3, Lcom/android/location/provider/mediatek/utility/NeighborCell;->mRssi:I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    iget-object v0, v1, Lcom/android/location/provider/mediatek/utility/CellInfo;->mNeighborCells:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :catch_0
    move-exception v0

    move-object v2, v0

    const-string v3, "LocationPhoneStateGeminiListener"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SimID: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v0, p0, Lcom/android/location/provider/mediatek/connectivity/impl/LocationPhoneStateGeminiListener;->mSimID:I

    if-nez v0, :cond_0

    const-string v0, "SIM1"

    :goto_3
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "MCCMNC NumberFormatException: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_4
    return-object v1

    :cond_0
    const-string v0, "SIM2"

    goto :goto_3

    :cond_1
    iget-object v0, v1, Lcom/android/location/provider/mediatek/utility/CellInfo;->mMainCell:Lcom/android/location/provider/mediatek/utility/MainCell;

    iput v5, v0, Lcom/android/location/provider/mediatek/utility/MainCell;->mLocalAreaCode:I

    goto/16 :goto_0

    :cond_2
    const/4 v4, 0x0

    :try_start_2
    iput v4, v3, Lcom/android/location/provider/mediatek/utility/NeighborCell;->mLac:I
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    :catch_1
    move-exception v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    :goto_5
    const-string v2, "LocationPhoneStateGeminiListener"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SimID: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v1, p0, Lcom/android/location/provider/mediatek/connectivity/impl/LocationPhoneStateGeminiListener;->mSimID:I

    if-nez v1, :cond_4

    const-string v1, "SIM1"

    :goto_6
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "Network_Type:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, p0, Lcom/android/location/provider/mediatek/connectivity/impl/LocationPhoneStateGeminiListener;->mNetworkType:Lcom/android/location/provider/mediatek/connectivity/Network_Type;

    sget-object v4, Lcom/android/location/provider/mediatek/connectivity/Network_Type;->Network_2G:Lcom/android/location/provider/mediatek/connectivity/Network_Type;

    if-ne v1, v4, :cond_5

    const-string v1, "2G"

    :goto_7
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "Cell Info: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v0

    goto :goto_4

    :cond_4
    const-string v1, "SIM2"

    goto :goto_6

    :cond_5
    const-string v1, "3G"

    goto :goto_7

    :cond_6
    move-object v0, v1

    goto :goto_5
.end method
