.class public Lcom/android/location/provider/mediatek/connectivity/impl/LocationTelephonyGeminiUtility;
.super Lcom/android/location/provider/mediatek/connectivity/LocationTelephonyUtilityInterface;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/location/provider/mediatek/connectivity/LocationTelephonyUtilityInterface;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method a(ILandroid/os/Handler;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/location/provider/mediatek/connectivity/impl/LocationTelephonyGeminiUtility;->mLocationPhoneStateListenerList:Ljava/util/List;

    monitor-enter v1

    if-nez p2, :cond_0

    :try_start_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v2, "null == handler"

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    new-instance v0, Lcom/android/location/provider/mediatek/connectivity/impl/LocationPhoneStateGeminiListener;

    iget-object v2, p0, Lcom/android/location/provider/mediatek/connectivity/impl/LocationTelephonyGeminiUtility;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2, p2, p1}, Lcom/android/location/provider/mediatek/connectivity/impl/LocationPhoneStateGeminiListener;-><init>(Landroid/content/Context;Landroid/os/Handler;I)V

    iget-object v2, p0, Lcom/android/location/provider/mediatek/connectivity/impl/LocationTelephonyGeminiUtility;->mLocationPhoneStateListenerList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public addLocationPhoneStateListener(Landroid/os/Handler;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/android/location/provider/mediatek/connectivity/impl/LocationTelephonyGeminiUtility;->a(ILandroid/os/Handler;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1}, Lcom/android/location/provider/mediatek/connectivity/impl/LocationTelephonyGeminiUtility;->a(ILandroid/os/Handler;)V

    return-void
.end method

.method public registerListener()V
    .locals 5

    iget-object v0, p0, Lcom/android/location/provider/mediatek/connectivity/impl/LocationTelephonyGeminiUtility;->mLocationPhoneStateListenerList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/location/provider/mediatek/connectivity/LocationPhoneStateListenerInterface;

    iget-object v2, p0, Lcom/android/location/provider/mediatek/connectivity/impl/LocationTelephonyGeminiUtility;->mTelephonyMgr:Landroid/telephony/TelephonyManager;

    const/16 v3, 0x111

    invoke-virtual {v0}, Lcom/android/location/provider/mediatek/connectivity/LocationPhoneStateListenerInterface;->getSimID()I

    move-result v4

    invoke-virtual {v2, v0, v3, v4}, Landroid/telephony/TelephonyManager;->listenGemini(Landroid/telephony/PhoneStateListener;II)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public unRegisterListener()V
    .locals 5

    iget-object v0, p0, Lcom/android/location/provider/mediatek/connectivity/impl/LocationTelephonyGeminiUtility;->mLocationPhoneStateListenerList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/location/provider/mediatek/connectivity/LocationPhoneStateListenerInterface;

    iget-object v2, p0, Lcom/android/location/provider/mediatek/connectivity/impl/LocationTelephonyGeminiUtility;->mTelephonyMgr:Landroid/telephony/TelephonyManager;

    const/4 v3, 0x0

    invoke-virtual {v0}, Lcom/android/location/provider/mediatek/connectivity/LocationPhoneStateListenerInterface;->getSimID()I

    move-result v4

    invoke-virtual {v2, v0, v3, v4}, Landroid/telephony/TelephonyManager;->listenGemini(Landroid/telephony/PhoneStateListener;II)V

    goto :goto_0

    :cond_0
    return-void
.end method
