.class public Lcom/android/location/provider/mediatek/connectivity/impl/LocationWifiUtility;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lcom/android/location/provider/mediatek/connectivity/impl/LocationWifiBroadcastReceiver;

.field private b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/location/provider/mediatek/connectivity/impl/LocationWifiUtility;->a:Lcom/android/location/provider/mediatek/connectivity/impl/LocationWifiBroadcastReceiver;

    iput-object v0, p0, Lcom/android/location/provider/mediatek/connectivity/impl/LocationWifiUtility;->b:Landroid/content/Context;

    iput-object p1, p0, Lcom/android/location/provider/mediatek/connectivity/impl/LocationWifiUtility;->b:Landroid/content/Context;

    new-instance v0, Lcom/android/location/provider/mediatek/connectivity/impl/LocationWifiBroadcastReceiver;

    invoke-direct {v0, p1, p2}, Lcom/android/location/provider/mediatek/connectivity/impl/LocationWifiBroadcastReceiver;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/location/provider/mediatek/connectivity/impl/LocationWifiUtility;->a:Lcom/android/location/provider/mediatek/connectivity/impl/LocationWifiBroadcastReceiver;

    return-void
.end method


# virtual methods
.method public getWifiMacInfo()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/location/provider/mediatek/utility/WifiInfo;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/location/provider/mediatek/connectivity/impl/LocationWifiUtility;->a:Lcom/android/location/provider/mediatek/connectivity/impl/LocationWifiBroadcastReceiver;

    invoke-virtual {v0}, Lcom/android/location/provider/mediatek/connectivity/impl/LocationWifiBroadcastReceiver;->getWifiInfo()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public registerWifiMacListerner()V
    .locals 3

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.net.wifi.SCAN_RESULTS"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/location/provider/mediatek/connectivity/impl/LocationWifiUtility;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/location/provider/mediatek/connectivity/impl/LocationWifiUtility;->a:Lcom/android/location/provider/mediatek/connectivity/impl/LocationWifiBroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public unregisterWifiMacListerner()V
    .locals 2

    iget-object v0, p0, Lcom/android/location/provider/mediatek/connectivity/impl/LocationWifiUtility;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/location/provider/mediatek/connectivity/impl/LocationWifiUtility;->a:Lcom/android/location/provider/mediatek/connectivity/impl/LocationWifiBroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method
