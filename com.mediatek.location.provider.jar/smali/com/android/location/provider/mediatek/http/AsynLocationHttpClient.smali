.class public Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Landroid/os/Handler;

.field private b:Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;

.field private c:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;ILcom/android/location/provider/mediatek/http/HTTP_TYPE;)V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;->a:Landroid/os/Handler;

    iput-object v0, p0, Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;->b:Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;

    new-instance v0, Lcom/android/location/provider/mediatek/http/a;

    invoke-direct {v0, p0}, Lcom/android/location/provider/mediatek/http/a;-><init>(Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;)V

    iput-object v0, p0, Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;->c:Ljava/lang/Runnable;

    const-string v0, "AsynLocationHttpClient"

    const-string v1, "AsynLocationHttpClient() is called"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;->a:Landroid/os/Handler;

    new-instance v0, Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;

    invoke-direct {v0, p2, p3, p4, p5}, Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;-><init>(Ljava/lang/String;Ljava/lang/String;ILcom/android/location/provider/mediatek/http/HTTP_TYPE;)V

    iput-object v0, p0, Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;->b:Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;

    return-void
.end method

.method static synthetic a(Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;)Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;
    .locals 1

    iget-object v0, p0, Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;->b:Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;

    return-object v0
.end method

.method private a()V
    .locals 2

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x4

    iput v1, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;->b:Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;

    invoke-virtual {v1}, Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;->getReceiveContent()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v1, p0, Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;->a:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method private b()V
    .locals 2

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x5

    iput v1, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;->b:Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;

    invoke-virtual {v1}, Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;->getErrorCode()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v1, p0, Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;->a:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method static synthetic b(Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;->b()V

    return-void
.end method

.method static synthetic c(Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;->a()V

    return-void
.end method


# virtual methods
.method public getErrorCode()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;->b:Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;

    invoke-virtual {v0}, Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;->getErrorCode()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getInformationfromServer()Z
    .locals 2

    iget-object v0, p0, Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;->c:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    const-string v0, "AsynLocationHttpClient"

    const-string v1, "getInformationfromServer() is called"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;->c:Ljava/lang/Runnable;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public getReceiveContent()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;->b:Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;

    invoke-virtual {v0}, Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;->getReceiveContent()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public printNoLocation()V
    .locals 2

    iget-object v0, p0, Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;->b:Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;

    const-string v1, "Returnning null with no exception"

    invoke-virtual {v0, v1}, Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;->setErrorCode(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;->b()V

    return-void
.end method

.method public printStackTrace(Ljava/lang/Exception;)V
    .locals 5

    invoke-virtual {p1}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v0, 0x0

    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const-string v0, "AsynLocationHttpClient"

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
