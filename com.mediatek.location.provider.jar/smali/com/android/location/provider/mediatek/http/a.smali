.class Lcom/android/location/provider/mediatek/http/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;


# direct methods
.method constructor <init>(Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;)V
    .locals 0

    iput-object p1, p0, Lcom/android/location/provider/mediatek/http/a;->a:Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    const-string v0, "AsynLocationHttpClient"

    const-string v1, "Running is called"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/location/provider/mediatek/http/a;->a:Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;

    invoke-static {v0}, Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;->a(Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;)Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;->a()V

    iget-object v0, p0, Lcom/android/location/provider/mediatek/http/a;->a:Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;

    invoke-static {v0}, Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;->a(Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;)Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;->getHttpType()Lcom/android/location/provider/mediatek/http/HTTP_TYPE;

    move-result-object v0

    sget-object v1, Lcom/android/location/provider/mediatek/http/HTTP_TYPE;->HTTP_NONE:Lcom/android/location/provider/mediatek/http/HTTP_TYPE;

    if-eq v0, v1, :cond_1

    const-string v1, "AsynLocationHttpClient"

    const-string v2, "mLocationHttpClient.initializeAHHTPRequest()"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/location/provider/mediatek/http/a;->a:Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;

    invoke-static {v1}, Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;->a(Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;)Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;->a()V

    const/4 v2, 0x0

    :try_start_0
    sget-object v1, Lcom/android/location/provider/mediatek/http/HTTP_TYPE;->HTTP_POST:Lcom/android/location/provider/mediatek/http/HTTP_TYPE;

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/android/location/provider/mediatek/http/a;->a:Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;

    invoke-static {v0}, Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;->a(Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;)Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;->getReceiveContent()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AsynLocationHttpClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "XXXXReceiveContent: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz v0, :cond_2

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/location/provider/mediatek/http/a;->a:Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;

    invoke-static {v0}, Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;->a(Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;)Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;->b()V

    iget-object v0, p0, Lcom/android/location/provider/mediatek/http/a;->a:Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;

    invoke-static {v0}, Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;->a(Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;)Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;->getReceiveContent()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AsynLocationHttpClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "XXXXReceiveContent: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz v0, :cond_3

    :goto_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_2
    iget-object v0, p0, Lcom/android/location/provider/mediatek/http/a;->a:Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;

    invoke-static {v0}, Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;->a(Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;)Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;->getReceiveContent()Ljava/lang/String;

    move-result-object v1

    const-string v3, "AsynLocationHttpClient"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Ex:"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz v2, :cond_e

    move-object v0, v2

    :goto_3
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "ReceiveContent: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz v1, :cond_f

    move-object v0, v1

    :goto_4
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v2, :cond_10

    const-string v0, "AsynLocationHttpClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Http Request with exception: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/location/provider/mediatek/http/a;->a:Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;

    invoke-virtual {v0, v2}, Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;->printStackTrace(Ljava/lang/Exception;)V

    iget-object v0, p0, Lcom/android/location/provider/mediatek/http/a;->a:Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;

    :goto_5
    invoke-static {v0}, Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;->b(Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;)V

    :cond_1
    :goto_6
    return-void

    :cond_2
    :try_start_1
    const-string v0, "null"

    goto/16 :goto_0

    :cond_3
    const-string v0, "null"

    goto :goto_1

    :cond_4
    sget-object v1, Lcom/android/location/provider/mediatek/http/HTTP_TYPE;->HTTP_GET:Lcom/android/location/provider/mediatek/http/HTTP_TYPE;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/location/provider/mediatek/http/a;->a:Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;

    invoke-static {v0}, Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;->a(Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;)Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;->c()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catch_0
    move-exception v0

    :try_start_2
    iget-object v1, p0, Lcom/android/location/provider/mediatek/http/a;->a:Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;

    invoke-static {v1}, Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;->a(Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;)Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;->setErrorCode(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v1, p0, Lcom/android/location/provider/mediatek/http/a;->a:Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;

    invoke-static {v1}, Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;->a(Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;)Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;->getReceiveContent()Ljava/lang/String;

    move-result-object v2

    const-string v3, "AsynLocationHttpClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Ex:"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz v0, :cond_8

    move-object v1, v0

    :goto_7
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "ReceiveContent: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz v2, :cond_9

    move-object v1, v2

    :goto_8
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_a

    const-string v1, "AsynLocationHttpClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Http Request with exception: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/location/provider/mediatek/http/a;->a:Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;

    invoke-virtual {v1, v0}, Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;->printStackTrace(Ljava/lang/Exception;)V

    iget-object v0, p0, Lcom/android/location/provider/mediatek/http/a;->a:Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;

    goto :goto_5

    :catch_1
    move-exception v0

    :try_start_3
    iget-object v1, p0, Lcom/android/location/provider/mediatek/http/a;->a:Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;

    invoke-static {v1}, Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;->a(Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;)Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;

    move-result-object v1

    invoke-virtual {v0}, Ljava/net/URISyntaxException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;->setErrorCode(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    iget-object v1, p0, Lcom/android/location/provider/mediatek/http/a;->a:Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;

    invoke-static {v1}, Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;->a(Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;)Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;->getReceiveContent()Ljava/lang/String;

    move-result-object v2

    const-string v3, "AsynLocationHttpClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Ex:"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz v0, :cond_b

    move-object v1, v0

    :goto_9
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "ReceiveContent: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz v2, :cond_c

    move-object v1, v2

    :goto_a
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_d

    const-string v1, "AsynLocationHttpClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Http Request with exception: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/location/provider/mediatek/http/a;->a:Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;

    invoke-virtual {v1, v0}, Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;->printStackTrace(Ljava/lang/Exception;)V

    iget-object v0, p0, Lcom/android/location/provider/mediatek/http/a;->a:Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;

    goto/16 :goto_5

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/android/location/provider/mediatek/http/a;->a:Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;

    invoke-static {v1}, Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;->a(Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;)Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;->getReceiveContent()Ljava/lang/String;

    move-result-object v3

    const-string v4, "AsynLocationHttpClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Ex:"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-eqz v2, :cond_5

    move-object v1, v2

    :goto_b
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "ReceiveContent: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-eqz v3, :cond_6

    move-object v1, v3

    :goto_c
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v2, :cond_7

    const-string v1, "AsynLocationHttpClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Http Request with exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/location/provider/mediatek/http/a;->a:Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;

    invoke-virtual {v1, v2}, Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;->printStackTrace(Ljava/lang/Exception;)V

    iget-object v1, p0, Lcom/android/location/provider/mediatek/http/a;->a:Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;

    invoke-static {v1}, Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;->b(Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;)V

    :goto_d
    throw v0

    :cond_5
    const-string v1, "null"

    goto :goto_b

    :cond_6
    const-string v1, "null"

    goto :goto_c

    :cond_7
    const-string v1, "AsynLocationHttpClient"

    const-string v2, "handleSendSucessfullyMessage is called"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/location/provider/mediatek/http/a;->a:Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;

    invoke-static {v1}, Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;->c(Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;)V

    goto :goto_d

    :cond_8
    const-string v1, "null"

    goto/16 :goto_7

    :cond_9
    const-string v1, "null"

    goto/16 :goto_8

    :cond_a
    const-string v0, "AsynLocationHttpClient"

    const-string v1, "handleSendSucessfullyMessage is called"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/location/provider/mediatek/http/a;->a:Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;

    :goto_e
    invoke-static {v0}, Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;->c(Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;)V

    goto/16 :goto_6

    :cond_b
    const-string v1, "null"

    goto/16 :goto_9

    :cond_c
    const-string v1, "null"

    goto/16 :goto_a

    :cond_d
    const-string v0, "AsynLocationHttpClient"

    const-string v1, "handleSendSucessfullyMessage is called"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/location/provider/mediatek/http/a;->a:Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;

    goto :goto_e

    :cond_e
    const-string v0, "null"

    goto/16 :goto_3

    :cond_f
    const-string v0, "null"

    goto/16 :goto_4

    :cond_10
    const-string v0, "AsynLocationHttpClient"

    const-string v1, "handleSendSucessfullyMessage is called"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/location/provider/mediatek/http/a;->a:Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;

    goto :goto_e
.end method
