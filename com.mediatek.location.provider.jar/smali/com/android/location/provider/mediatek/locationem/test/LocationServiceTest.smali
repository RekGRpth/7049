.class public Lcom/android/location/provider/mediatek/locationem/test/LocationServiceTest;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Lcom/android/location/provider/mediatek/locationem/test/b;

.field b:Lcom/android/location/provider/mediatek/locationem/test/LocationTestNetworkStateThread;

.field c:Lcom/android/location/provider/mediatek/locationem/test/LocationTestGeocoderThread;

.field private d:Z

.field private e:Landroid/os/Handler;

.field private f:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationServiceTest;->a:Lcom/android/location/provider/mediatek/locationem/test/b;

    iput-object v1, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationServiceTest;->b:Lcom/android/location/provider/mediatek/locationem/test/LocationTestNetworkStateThread;

    iput-object v1, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationServiceTest;->c:Lcom/android/location/provider/mediatek/locationem/test/LocationTestGeocoderThread;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationServiceTest;->d:Z

    iput-object v1, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationServiceTest;->e:Landroid/os/Handler;

    iput-object v1, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationServiceTest;->f:Landroid/content/Context;

    iput-object p1, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationServiceTest;->f:Landroid/content/Context;

    new-instance v0, Lcom/android/location/provider/mediatek/locationem/test/b;

    invoke-direct {v0}, Lcom/android/location/provider/mediatek/locationem/test/b;-><init>()V

    iput-object v0, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationServiceTest;->a:Lcom/android/location/provider/mediatek/locationem/test/b;

    return-void
.end method


# virtual methods
.method public getGeocoderHander()Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationServiceTest;->e:Landroid/os/Handler;

    return-object v0
.end method

.method public setGeocoderHander(Landroid/os/Handler;)V
    .locals 1

    iput-object p1, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationServiceTest;->e:Landroid/os/Handler;

    iget-object v0, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationServiceTest;->a:Lcom/android/location/provider/mediatek/locationem/test/b;

    invoke-virtual {v0, p1}, Lcom/android/location/provider/mediatek/locationem/test/b;->a(Landroid/os/Handler;)V

    return-void
.end method

.method public startOrStopThread(Z)V
    .locals 6

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_LOCATION_EM:Z

    if-eqz v0, :cond_0

    const-string v0, "LocationServiceTest"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "startOrStopThread() is called isstart: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    if-eqz p1, :cond_3

    iget-boolean v0, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationServiceTest;->d:Z

    if-nez v0, :cond_3

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_LOCATION_EM:Z

    if-eqz v0, :cond_1

    const-string v0, "LocationServiceTest"

    const-string v1, "startThread() is called"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationServiceTest;->d:Z

    iget-object v0, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationServiceTest;->f:Landroid/content/Context;

    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    const-string v1, "network"

    iget-object v5, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationServiceTest;->a:Lcom/android/location/provider/mediatek/locationem/test/b;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    const-string v1, "gps"

    iget-object v5, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationServiceTest;->a:Lcom/android/location/provider/mediatek/locationem/test/b;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    new-instance v0, Lcom/android/location/provider/mediatek/locationem/test/LocationTestGeocoderThread;

    iget-object v1, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationServiceTest;->f:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Lcom/android/location/provider/mediatek/locationem/test/LocationTestGeocoderThread;-><init>(Landroid/content/Context;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationServiceTest;->c:Lcom/android/location/provider/mediatek/locationem/test/LocationTestGeocoderThread;

    iget-object v0, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationServiceTest;->c:Lcom/android/location/provider/mediatek/locationem/test/LocationTestGeocoderThread;

    invoke-virtual {v0}, Lcom/android/location/provider/mediatek/locationem/test/LocationTestGeocoderThread;->start()V

    new-instance v0, Lcom/android/location/provider/mediatek/locationem/test/LocationTestNetworkStateThread;

    iget-object v1, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationServiceTest;->f:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/location/provider/mediatek/locationem/test/LocationTestNetworkStateThread;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationServiceTest;->b:Lcom/android/location/provider/mediatek/locationem/test/LocationTestNetworkStateThread;

    iget-object v0, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationServiceTest;->b:Lcom/android/location/provider/mediatek/locationem/test/LocationTestNetworkStateThread;

    invoke-virtual {v0}, Lcom/android/location/provider/mediatek/locationem/test/LocationTestNetworkStateThread;->start()V

    :cond_2
    :goto_0
    return-void

    :cond_3
    if-nez p1, :cond_2

    iget-boolean v0, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationServiceTest;->d:Z

    if-eqz v0, :cond_2

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_LOCATION_EM:Z

    if-eqz v0, :cond_4

    const-string v0, "LocationServiceTest"

    const-string v1, "stopThread() is called"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iget-object v0, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationServiceTest;->f:Landroid/content/Context;

    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationServiceTest;->a:Lcom/android/location/provider/mediatek/locationem/test/b;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    iget-object v0, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationServiceTest;->b:Lcom/android/location/provider/mediatek/locationem/test/LocationTestNetworkStateThread;

    invoke-virtual {v0}, Lcom/android/location/provider/mediatek/locationem/test/LocationTestNetworkStateThread;->stopThread()V

    iget-object v0, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationServiceTest;->c:Lcom/android/location/provider/mediatek/locationem/test/LocationTestGeocoderThread;

    invoke-virtual {v0}, Lcom/android/location/provider/mediatek/locationem/test/LocationTestGeocoderThread;->stopThread()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationServiceTest;->d:Z

    goto :goto_0
.end method
