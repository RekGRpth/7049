.class public Lcom/android/location/provider/mediatek/locationem/test/LocationTestGeocoderThread;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field private a:Landroid/content/Context;

.field private b:Ljava/lang/Object;

.field private c:Lcom/android/location/provider/mediatek/locationem/test/a;

.field private d:Landroid/os/Looper;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput-object v0, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationTestGeocoderThread;->b:Ljava/lang/Object;

    iput-object v0, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationTestGeocoderThread;->d:Landroid/os/Looper;

    iput-object p1, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationTestGeocoderThread;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationTestGeocoderThread;->b:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    invoke-static {}, Landroid/os/Looper;->prepare()V

    new-instance v0, Lcom/android/location/provider/mediatek/locationem/test/a;

    iget-object v1, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationTestGeocoderThread;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/location/provider/mediatek/locationem/test/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationTestGeocoderThread;->c:Lcom/android/location/provider/mediatek/locationem/test/a;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationTestGeocoderThread;->d:Landroid/os/Looper;

    iget-object v0, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationTestGeocoderThread;->b:Ljava/lang/Object;

    check-cast v0, Lcom/android/location/provider/mediatek/locationem/test/LocationServiceTest;

    iget-object v1, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationTestGeocoderThread;->c:Lcom/android/location/provider/mediatek/locationem/test/a;

    invoke-virtual {v0, v1}, Lcom/android/location/provider/mediatek/locationem/test/LocationServiceTest;->setGeocoderHander(Landroid/os/Handler;)V

    iget-object v0, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationTestGeocoderThread;->d:Landroid/os/Looper;

    invoke-static {}, Landroid/os/Looper;->loop()V

    return-void
.end method

.method public stopThread()V
    .locals 1

    iget-object v0, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationTestGeocoderThread;->d:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    return-void
.end method
