.class public Lcom/android/location/provider/mediatek/locationem/test/LocationTestNetworkStateThread;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:Landroid/content/Context;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/provider/Telephony$SIMInfo;",
            ">;"
        }
    .end annotation
.end field

.field private d:Landroid/net/wifi/WifiManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationTestNetworkStateThread;->a:Z

    iput-object v1, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationTestNetworkStateThread;->c:Ljava/util/List;

    iput-object v1, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationTestNetworkStateThread;->d:Landroid/net/wifi/WifiManager;

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_LOCATION_EM:Z

    if-eqz v0, :cond_0

    const-string v0, "LocationTestNetworkStateThread"

    const-string v1, "LocationTestNetworkStateThread() is called"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iput-object p1, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationTestNetworkStateThread;->b:Landroid/content/Context;

    iget-object v0, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationTestNetworkStateThread;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/provider/Telephony$SIMInfo;->getInsertedSIMList(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationTestNetworkStateThread;->c:Ljava/util/List;

    iget-object v0, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationTestNetworkStateThread;->b:Landroid/content/Context;

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationTestNetworkStateThread;->d:Landroid/net/wifi/WifiManager;

    return-void
.end method

.method private a()V
    .locals 0

    return-void
.end method

.method private b()V
    .locals 0

    return-void
.end method

.method private c()V
    .locals 0

    return-void
.end method

.method private d()V
    .locals 0

    return-void
.end method


# virtual methods
.method public run()V
    .locals 1

    :goto_0
    iget-boolean v0, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationTestNetworkStateThread;->a:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/location/provider/mediatek/locationem/test/LocationTestNetworkStateThread;->a()V

    invoke-direct {p0}, Lcom/android/location/provider/mediatek/locationem/test/LocationTestNetworkStateThread;->b()V

    invoke-direct {p0}, Lcom/android/location/provider/mediatek/locationem/test/LocationTestNetworkStateThread;->c()V

    invoke-direct {p0}, Lcom/android/location/provider/mediatek/locationem/test/LocationTestNetworkStateThread;->d()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public stopThread()V
    .locals 3

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_LOCATION_EM:Z

    if-eqz v0, :cond_0

    const-string v0, "LocationTestNetworkStateThread"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stopThread() is called m_bThreadRuningConmmand: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationTestNetworkStateThread;->a:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/location/provider/mediatek/locationem/test/LocationTestNetworkStateThread;->a:Z

    invoke-virtual {p0}, Lcom/android/location/provider/mediatek/locationem/test/LocationTestNetworkStateThread;->interrupt()V

    return-void
.end method
