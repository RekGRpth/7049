.class public abstract Lcom/android/location/provider/mediatek/serviceaccessor/GeocoderContentAccessorInterface;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public mGeocoderProtocolInterface:Lcom/android/location/provider/mediatek/serviceaccessor/GeocoderProtocolInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract geocoding(Ljava/lang/String;DDDDILjava/util/Locale;Ljava/lang/Boolean;Ljava/util/List;Ljava/util/List;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "DDDDI",
            "Ljava/util/Locale;",
            "Ljava/lang/Boolean;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/location/Address;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation
.end method

.method public abstract reverseGeocoding(DDILjava/util/Locale;ZLjava/util/List;Ljava/util/List;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(DDI",
            "Ljava/util/Locale;",
            "Z",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/location/Address;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation
.end method
