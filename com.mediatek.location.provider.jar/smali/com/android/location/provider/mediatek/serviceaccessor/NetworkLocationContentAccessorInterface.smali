.class public abstract Lcom/android/location/provider/mediatek/serviceaccessor/NetworkLocationContentAccessorInterface;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public mNetworkLocationProtocolInterface:Lcom/android/location/provider/mediatek/serviceaccessor/NetworkLocationProtocolInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract finishGetLocationByCellIdAndWifi(Ljava/lang/String;)Landroid/location/Location;
.end method

.method public abstract startGetLocationByCellIdAndWifi(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/location/provider/mediatek/utility/WifiInfo;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/location/provider/mediatek/utility/CellInfo;",
            ">;)V"
        }
    .end annotation
.end method
