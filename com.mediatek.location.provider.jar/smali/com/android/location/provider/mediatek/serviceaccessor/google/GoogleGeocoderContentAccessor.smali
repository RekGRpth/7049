.class public Lcom/android/location/provider/mediatek/serviceaccessor/google/GoogleGeocoderContentAccessor;
.super Lcom/android/location/provider/mediatek/serviceaccessor/GeocoderContentAccessorInterface;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/location/provider/mediatek/serviceaccessor/GeocoderContentAccessorInterface;-><init>()V

    new-instance v0, Lcom/android/location/provider/mediatek/serviceaccessor/google/protocol/GoogleGeocoderContentAccessorJasonProtocol;

    invoke-direct {v0}, Lcom/android/location/provider/mediatek/serviceaccessor/google/protocol/GoogleGeocoderContentAccessorJasonProtocol;-><init>()V

    iput-object v0, p0, Lcom/android/location/provider/mediatek/serviceaccessor/google/GoogleGeocoderContentAccessor;->mGeocoderProtocolInterface:Lcom/android/location/provider/mediatek/serviceaccessor/GeocoderProtocolInterface;

    return-void
.end method


# virtual methods
.method public geocoding(Ljava/lang/String;DDDDILjava/util/Locale;Ljava/lang/Boolean;Ljava/util/List;Ljava/util/List;)Ljava/lang/String;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "DDDDI",
            "Ljava/util/Locale;",
            "Ljava/lang/Boolean;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/location/Address;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    const/4 v13, 0x0

    iget-object v3, p0, Lcom/android/location/provider/mediatek/serviceaccessor/google/GoogleGeocoderContentAccessor;->mGeocoderProtocolInterface:Lcom/android/location/provider/mediatek/serviceaccessor/GeocoderProtocolInterface;

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const-wide/16 v5, 0x0

    const-wide/16 v7, 0x0

    move-object v9, p1

    move-object/from16 v10, p12

    move-object/from16 v11, p11

    move-object/from16 v12, p13

    invoke-interface/range {v3 .. v12}, Lcom/android/location/provider/mediatek/serviceaccessor/GeocoderProtocolInterface;->getGetURL(Ljava/lang/Boolean;DDLjava/lang/String;Ljava/lang/Boolean;Ljava/util/Locale;Ljava/util/List;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    new-instance v4, Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;

    const/4 v5, 0x0

    const/16 v6, 0x2710

    sget-object v7, Lcom/android/location/provider/mediatek/http/HTTP_TYPE;->HTTP_GET:Lcom/android/location/provider/mediatek/http/HTTP_TYPE;

    invoke-direct {v4, v3, v5, v6, v7}, Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;-><init>(Ljava/lang/String;Ljava/lang/String;ILcom/android/location/provider/mediatek/http/HTTP_TYPE;)V

    invoke-virtual {v4}, Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;->getInformationfromServer()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/location/provider/mediatek/serviceaccessor/google/GoogleGeocoderContentAccessor;->mGeocoderProtocolInterface:Lcom/android/location/provider/mediatek/serviceaccessor/GeocoderProtocolInterface;

    invoke-virtual {v4}, Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;->getReceiveContent()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p11

    move/from16 v1, p10

    move-object/from16 v2, p14

    invoke-interface {v3, v4, v0, v1, v2}, Lcom/android/location/provider/mediatek/serviceaccessor/GeocoderProtocolInterface;->analyzeResult(Ljava/lang/String;Ljava/util/Locale;ILjava/util/List;)Ljava/lang/String;

    move-result-object v3

    :goto_0
    return-object v3

    :cond_0
    invoke-virtual {v4}, Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;->getErrorCode()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_1
    move-object v3, v13

    goto :goto_0
.end method

.method public reverseGeocoding(DDILjava/util/Locale;ZLjava/util/List;Ljava/util/List;)Ljava/lang/String;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(DDI",
            "Ljava/util/Locale;",
            "Z",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/location/Address;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    const/4 v13, 0x0

    iget-object v3, p0, Lcom/android/location/provider/mediatek/serviceaccessor/google/GoogleGeocoderContentAccessor;->mGeocoderProtocolInterface:Lcom/android/location/provider/mediatek/serviceaccessor/GeocoderProtocolInterface;

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v9, 0x0

    invoke-static/range {p7 .. p7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    move-wide v5, p1

    move-wide/from16 v7, p3

    move-object/from16 v11, p6

    move-object/from16 v12, p8

    invoke-interface/range {v3 .. v12}, Lcom/android/location/provider/mediatek/serviceaccessor/GeocoderProtocolInterface;->getGetURL(Ljava/lang/Boolean;DDLjava/lang/String;Ljava/lang/Boolean;Ljava/util/Locale;Ljava/util/List;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    new-instance v4, Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;

    const/4 v5, 0x0

    const v6, 0x493e0

    sget-object v7, Lcom/android/location/provider/mediatek/http/HTTP_TYPE;->HTTP_GET:Lcom/android/location/provider/mediatek/http/HTTP_TYPE;

    invoke-direct {v4, v3, v5, v6, v7}, Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;-><init>(Ljava/lang/String;Ljava/lang/String;ILcom/android/location/provider/mediatek/http/HTTP_TYPE;)V

    invoke-virtual {v4}, Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;->getInformationfromServer()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/location/provider/mediatek/serviceaccessor/google/GoogleGeocoderContentAccessor;->mGeocoderProtocolInterface:Lcom/android/location/provider/mediatek/serviceaccessor/GeocoderProtocolInterface;

    invoke-virtual {v4}, Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;->getReceiveContent()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p6

    move/from16 v1, p5

    move-object/from16 v2, p9

    invoke-interface {v3, v4, v0, v1, v2}, Lcom/android/location/provider/mediatek/serviceaccessor/GeocoderProtocolInterface;->analyzeResult(Ljava/lang/String;Ljava/util/Locale;ILjava/util/List;)Ljava/lang/String;

    move-result-object v3

    :goto_0
    return-object v3

    :cond_0
    invoke-virtual {v4}, Lcom/android/location/provider/mediatek/http/SyncLocationHttpClient;->getErrorCode()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_1
    move-object v3, v13

    goto :goto_0
.end method
