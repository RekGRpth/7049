.class public Lcom/android/location/provider/mediatek/serviceaccessor/google/GoogleNetworkLocationContentAccessor;
.super Lcom/android/location/provider/mediatek/serviceaccessor/NetworkLocationContentAccessorInterface;
.source "SourceFile"


# instance fields
.field private a:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 1

    invoke-direct {p0}, Lcom/android/location/provider/mediatek/serviceaccessor/NetworkLocationContentAccessorInterface;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/location/provider/mediatek/serviceaccessor/google/GoogleNetworkLocationContentAccessor;->a:Landroid/os/Handler;

    iput-object p1, p0, Lcom/android/location/provider/mediatek/serviceaccessor/google/GoogleNetworkLocationContentAccessor;->a:Landroid/os/Handler;

    new-instance v0, Lcom/android/location/provider/mediatek/serviceaccessor/google/protocol/GoogleNetworkContentAccessorJsonProtocol;

    invoke-direct {v0}, Lcom/android/location/provider/mediatek/serviceaccessor/google/protocol/GoogleNetworkContentAccessorJsonProtocol;-><init>()V

    iput-object v0, p0, Lcom/android/location/provider/mediatek/serviceaccessor/google/GoogleNetworkLocationContentAccessor;->mNetworkLocationProtocolInterface:Lcom/android/location/provider/mediatek/serviceaccessor/NetworkLocationProtocolInterface;

    return-void
.end method


# virtual methods
.method public finishGetLocationByCellIdAndWifi(Ljava/lang/String;)Landroid/location/Location;
    .locals 2

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_NETWOKLOCATION_SERVICE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_NETWOKLOCATION_SERVICE:Z

    if-eqz v0, :cond_0

    const-string v0, "GoogleNetworkLocationContentProvider"

    const-string v1, "finishGetLocationByCellIdAndWifi parsing data which come from service"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "GoogleNetworkLocationContentProvider"

    invoke-static {v0, p1}, Lcom/android/location/provider/mediatek/utility/LocationLogUtility;->writeLargeContentLog(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/location/provider/mediatek/serviceaccessor/google/GoogleNetworkLocationContentAccessor;->mNetworkLocationProtocolInterface:Lcom/android/location/provider/mediatek/serviceaccessor/NetworkLocationProtocolInterface;

    invoke-interface {v0, p1}, Lcom/android/location/provider/mediatek/serviceaccessor/NetworkLocationProtocolInterface;->analyzeResult(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    return-object v0
.end method

.method public startGetLocationByCellIdAndWifi(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/location/provider/mediatek/utility/WifiInfo;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/location/provider/mediatek/utility/CellInfo;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/location/provider/mediatek/serviceaccessor/google/GoogleNetworkLocationContentAccessor;->mNetworkLocationProtocolInterface:Lcom/android/location/provider/mediatek/serviceaccessor/NetworkLocationProtocolInterface;

    invoke-interface {v0, p1, p2}, Lcom/android/location/provider/mediatek/serviceaccessor/NetworkLocationProtocolInterface;->composeNetworkLocationProtocal(Ljava/util/ArrayList;Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    sget-boolean v0, Lcom/android/location/provider/mediatek/utility/NetworkLocationDebugControl;->DEBUG_NETWOKLOCATION_SERVICE:Z

    if-eqz v0, :cond_0

    const-string v0, "GoogleNetworkLocationContentProvider"

    const-string v1, "startGetLocationByCellIdAndWifi sending data to the service"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "GoogleNetworkLocationContentProvider"

    invoke-static {v0, v3}, Lcom/android/location/provider/mediatek/utility/LocationLogUtility;->writeLargeContentLog(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;

    iget-object v1, p0, Lcom/android/location/provider/mediatek/serviceaccessor/google/GoogleNetworkLocationContentAccessor;->a:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/location/provider/mediatek/serviceaccessor/google/GoogleNetworkLocationContentAccessor;->mNetworkLocationProtocolInterface:Lcom/android/location/provider/mediatek/serviceaccessor/NetworkLocationProtocolInterface;

    invoke-interface {v2}, Lcom/android/location/provider/mediatek/serviceaccessor/NetworkLocationProtocolInterface;->getPOSTURL()Ljava/lang/String;

    move-result-object v2

    const/16 v4, 0x7d0

    sget-object v5, Lcom/android/location/provider/mediatek/http/HTTP_TYPE;->HTTP_POST:Lcom/android/location/provider/mediatek/http/HTTP_TYPE;

    invoke-direct/range {v0 .. v5}, Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;-><init>(Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;ILcom/android/location/provider/mediatek/http/HTTP_TYPE;)V

    invoke-virtual {v0}, Lcom/android/location/provider/mediatek/http/AsynLocationHttpClient;->getInformationfromServer()Z

    :cond_1
    return-void
.end method
