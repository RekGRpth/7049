.class public Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAKeyPairGenerator;
.super Ljava/security/KeyPairGeneratorSpi;
.source "OpenSSLDSAKeyPairGenerator.java"


# instance fields
.field private g:[B

.field private p:[B

.field private primeBits:I

.field private q:[B

.field private random:Ljava/security/SecureRandom;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/security/KeyPairGeneratorSpi;-><init>()V

    const/16 v0, 0x400

    iput v0, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAKeyPairGenerator;->primeBits:I

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAKeyPairGenerator;->random:Ljava/security/SecureRandom;

    return-void
.end method


# virtual methods
.method public generateKeyPair()Ljava/security/KeyPair;
    .locals 8

    iget-object v4, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAKeyPairGenerator;->random:Ljava/security/SecureRandom;

    if-nez v4, :cond_0

    const/4 v3, 0x0

    :goto_0
    new-instance v0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

    iget v4, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAKeyPairGenerator;->primeBits:I

    iget-object v5, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAKeyPairGenerator;->g:[B

    iget-object v6, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAKeyPairGenerator;->p:[B

    iget-object v7, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAKeyPairGenerator;->q:[B

    invoke-static {v4, v3, v5, v6, v7}, Lorg/apache/harmony/xnet/provider/jsse/NativeCrypto;->DSA_generate_key(I[B[B[B[B)I

    move-result v4

    invoke-direct {v0, v4}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;-><init>(I)V

    new-instance v1, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAPrivateKey;

    invoke-direct {v1, v0}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAPrivateKey;-><init>(Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;)V

    new-instance v2, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAPublicKey;

    invoke-direct {v2, v0}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAPublicKey;-><init>(Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;)V

    new-instance v4, Ljava/security/KeyPair;

    invoke-direct {v4, v2, v1}, Ljava/security/KeyPair;-><init>(Ljava/security/PublicKey;Ljava/security/PrivateKey;)V

    return-object v4

    :cond_0
    const/16 v4, 0x14

    new-array v3, v4, [B

    iget-object v4, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAKeyPairGenerator;->random:Ljava/security/SecureRandom;

    invoke-virtual {v4, v3}, Ljava/security/SecureRandom;->nextBytes([B)V

    goto :goto_0
.end method

.method public initialize(ILjava/security/SecureRandom;)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/security/SecureRandom;

    iput p1, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAKeyPairGenerator;->primeBits:I

    iput-object p2, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAKeyPairGenerator;->random:Ljava/security/SecureRandom;

    return-void
.end method

.method public initialize(Ljava/security/spec/AlgorithmParameterSpec;Ljava/security/SecureRandom;)V
    .locals 6
    .param p1    # Ljava/security/spec/AlgorithmParameterSpec;
    .param p2    # Ljava/security/SecureRandom;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidAlgorithmParameterException;
        }
    .end annotation

    iput-object p2, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAKeyPairGenerator;->random:Ljava/security/SecureRandom;

    instance-of v4, p1, Ljava/security/spec/DSAParameterSpec;

    if-eqz v4, :cond_3

    move-object v0, p1

    check-cast v0, Ljava/security/spec/DSAParameterSpec;

    invoke-virtual {v0}, Ljava/security/spec/DSAParameterSpec;->getG()Ljava/math/BigInteger;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v4

    iput-object v4, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAKeyPairGenerator;->g:[B

    :cond_0
    invoke-virtual {v0}, Ljava/security/spec/DSAParameterSpec;->getP()Ljava/math/BigInteger;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v4

    iput-object v4, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAKeyPairGenerator;->p:[B

    :cond_1
    invoke-virtual {v0}, Ljava/security/spec/DSAParameterSpec;->getQ()Ljava/math/BigInteger;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v4

    iput-object v4, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAKeyPairGenerator;->q:[B

    :cond_2
    return-void

    :cond_3
    if-eqz p1, :cond_2

    new-instance v4, Ljava/security/InvalidAlgorithmParameterException;

    const-string v5, "Params must be DSAParameterSpec"

    invoke-direct {v4, v5}, Ljava/security/InvalidAlgorithmParameterException;-><init>(Ljava/lang/String;)V

    throw v4
.end method
