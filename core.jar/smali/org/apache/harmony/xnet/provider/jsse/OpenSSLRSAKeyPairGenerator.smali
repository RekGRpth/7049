.class public Lorg/apache/harmony/xnet/provider/jsse/OpenSSLRSAKeyPairGenerator;
.super Ljava/security/KeyPairGeneratorSpi;
.source "OpenSSLRSAKeyPairGenerator.java"


# instance fields
.field private modulusBits:I

.field private publicExponent:[B


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/security/KeyPairGeneratorSpi;-><init>()V

    const/4 v0, 0x3

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    iput-object v0, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLRSAKeyPairGenerator;->publicExponent:[B

    const/16 v0, 0x800

    iput v0, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLRSAKeyPairGenerator;->modulusBits:I

    return-void

    :array_0
    .array-data 1
        0x1t
        0x0t
        0x1t
    .end array-data
.end method


# virtual methods
.method public generateKeyPair()Ljava/security/KeyPair;
    .locals 5

    new-instance v0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

    iget v3, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLRSAKeyPairGenerator;->modulusBits:I

    iget-object v4, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLRSAKeyPairGenerator;->publicExponent:[B

    invoke-static {v3, v4}, Lorg/apache/harmony/xnet/provider/jsse/NativeCrypto;->RSA_generate_key_ex(I[B)I

    move-result v3

    invoke-direct {v0, v3}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;-><init>(I)V

    invoke-static {v0}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLRSAPrivateKey;->getInstance(Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;)Lorg/apache/harmony/xnet/provider/jsse/OpenSSLRSAPrivateKey;

    move-result-object v1

    new-instance v2, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLRSAPublicKey;

    invoke-direct {v2, v0}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLRSAPublicKey;-><init>(Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;)V

    new-instance v3, Ljava/security/KeyPair;

    invoke-direct {v3, v2, v1}, Ljava/security/KeyPair;-><init>(Ljava/security/PublicKey;Ljava/security/PrivateKey;)V

    return-object v3
.end method

.method public initialize(ILjava/security/SecureRandom;)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/security/SecureRandom;

    iput p1, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLRSAKeyPairGenerator;->modulusBits:I

    return-void
.end method

.method public initialize(Ljava/security/spec/AlgorithmParameterSpec;Ljava/security/SecureRandom;)V
    .locals 4
    .param p1    # Ljava/security/spec/AlgorithmParameterSpec;
    .param p2    # Ljava/security/SecureRandom;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidAlgorithmParameterException;
        }
    .end annotation

    instance-of v2, p1, Ljava/security/spec/RSAKeyGenParameterSpec;

    if-nez v2, :cond_0

    new-instance v2, Ljava/security/InvalidAlgorithmParameterException;

    const-string v3, "Only RSAKeyGenParameterSpec supported"

    invoke-direct {v2, v3}, Ljava/security/InvalidAlgorithmParameterException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    move-object v1, p1

    check-cast v1, Ljava/security/spec/RSAKeyGenParameterSpec;

    invoke-virtual {v1}, Ljava/security/spec/RSAKeyGenParameterSpec;->getPublicExponent()Ljava/math/BigInteger;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v2

    iput-object v2, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLRSAKeyPairGenerator;->publicExponent:[B

    :cond_1
    invoke-virtual {v1}, Ljava/security/spec/RSAKeyGenParameterSpec;->getKeysize()I

    move-result v2

    iput v2, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLRSAKeyPairGenerator;->modulusBits:I

    return-void
.end method
