.class public Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;
.super Ljava/security/Signature;
.source "OpenSSLSignature.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature$1;,
        Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature$SHA1DSA;,
        Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature$SHA512RSA;,
        Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature$SHA384RSA;,
        Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature$SHA256RSA;,
        Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature$SHA1RSA;,
        Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature$MD5RSA;,
        Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature$EngineType;
    }
.end annotation


# instance fields
.field private ctx:I

.field private final engineType:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature$EngineType;

.field private final evpAlgorithm:Ljava/lang/String;

.field private key:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

.field private final singleByte:[B


# direct methods
.method private constructor <init>(Ljava/lang/String;Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature$EngineType;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature$EngineType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;
        }
    .end annotation

    invoke-direct {p0, p1}, Ljava/security/Signature;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x1

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->singleByte:[B

    const-string v0, "RSA-MD2"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/security/NoSuchAlgorithmException;

    invoke-direct {v0, p1}, Ljava/security/NoSuchAlgorithmException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p2, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->engineType:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature$EngineType;

    iput-object p1, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->evpAlgorithm:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature$EngineType;Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature$1;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature$EngineType;
    .param p3    # Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature$1;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;-><init>(Ljava/lang/String;Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature$EngineType;)V

    return-void
.end method

.method private destroyContextIfExists()V
    .locals 1

    iget v0, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->ctx:I

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->ctx:I

    invoke-static {v0}, Lorg/apache/harmony/xnet/provider/jsse/NativeCrypto;->EVP_MD_CTX_destroy(I)V

    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->ctx:I

    :cond_0
    return-void
.end method


# virtual methods
.method protected engineGetParameter(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidParameterException;
        }
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method protected engineInitSign(Ljava/security/PrivateKey;)V
    .locals 4
    .param p1    # Ljava/security/PrivateKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;
        }
    .end annotation

    invoke-direct {p0}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->destroyContextIfExists()V

    instance-of v2, p1, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAPrivateKey;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->engineType:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature$EngineType;

    sget-object v3, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature$EngineType;->DSA:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature$EngineType;

    if-eq v2, v3, :cond_0

    new-instance v2, Ljava/security/InvalidKeyException;

    const-string v3, "Signature not initialized as DSA"

    invoke-direct {v2, v3}, Ljava/security/InvalidKeyException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    move-object v0, p1

    check-cast v0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAPrivateKey;

    invoke-virtual {v0}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAPrivateKey;->getOpenSSLKey()Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->key:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

    :goto_0
    return-void

    :cond_1
    instance-of v2, p1, Ljava/security/interfaces/DSAPrivateKey;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->engineType:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature$EngineType;

    sget-object v3, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature$EngineType;->DSA:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature$EngineType;

    if-eq v2, v3, :cond_2

    new-instance v2, Ljava/security/InvalidKeyException;

    const-string v3, "Signature not initialized as DSA"

    invoke-direct {v2, v3}, Ljava/security/InvalidKeyException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_2
    move-object v0, p1

    check-cast v0, Ljava/security/interfaces/DSAPrivateKey;

    invoke-static {v0}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAPrivateKey;->getInstance(Ljava/security/interfaces/DSAPrivateKey;)Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->key:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

    goto :goto_0

    :cond_3
    instance-of v2, p1, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLRSAPrivateKey;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->engineType:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature$EngineType;

    sget-object v3, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature$EngineType;->RSA:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature$EngineType;

    if-eq v2, v3, :cond_4

    new-instance v2, Ljava/security/InvalidKeyException;

    const-string v3, "Signature not initialized as RSA"

    invoke-direct {v2, v3}, Ljava/security/InvalidKeyException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_4
    move-object v1, p1

    check-cast v1, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLRSAPrivateKey;

    invoke-virtual {v1}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLRSAPrivateKey;->getOpenSSLKey()Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->key:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

    goto :goto_0

    :cond_5
    instance-of v2, p1, Ljava/security/interfaces/RSAPrivateCrtKey;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->engineType:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature$EngineType;

    sget-object v3, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature$EngineType;->RSA:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature$EngineType;

    if-eq v2, v3, :cond_6

    new-instance v2, Ljava/security/InvalidKeyException;

    const-string v3, "Signature not initialized as RSA"

    invoke-direct {v2, v3}, Ljava/security/InvalidKeyException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_6
    move-object v1, p1

    check-cast v1, Ljava/security/interfaces/RSAPrivateCrtKey;

    invoke-static {v1}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLRSAPrivateCrtKey;->getInstance(Ljava/security/interfaces/RSAPrivateCrtKey;)Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->key:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

    goto :goto_0

    :cond_7
    instance-of v2, p1, Ljava/security/interfaces/RSAPrivateKey;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->engineType:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature$EngineType;

    sget-object v3, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature$EngineType;->RSA:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature$EngineType;

    if-eq v2, v3, :cond_8

    new-instance v2, Ljava/security/InvalidKeyException;

    const-string v3, "Signature not initialized as RSA"

    invoke-direct {v2, v3}, Ljava/security/InvalidKeyException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_8
    move-object v1, p1

    check-cast v1, Ljava/security/interfaces/RSAPrivateKey;

    invoke-static {v1}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLRSAPrivateKey;->getInstance(Ljava/security/interfaces/RSAPrivateKey;)Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->key:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

    goto :goto_0

    :cond_9
    new-instance v2, Ljava/security/InvalidKeyException;

    const-string v3, "Need DSA or RSA private key"

    invoke-direct {v2, v3}, Ljava/security/InvalidKeyException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method protected engineInitVerify(Ljava/security/PublicKey;)V
    .locals 4
    .param p1    # Ljava/security/PublicKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;
        }
    .end annotation

    invoke-direct {p0}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->destroyContextIfExists()V

    instance-of v2, p1, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAPublicKey;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->engineType:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature$EngineType;

    sget-object v3, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature$EngineType;->DSA:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature$EngineType;

    if-eq v2, v3, :cond_0

    new-instance v2, Ljava/security/InvalidKeyException;

    const-string v3, "Signature not initialized as DSA"

    invoke-direct {v2, v3}, Ljava/security/InvalidKeyException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    move-object v0, p1

    check-cast v0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAPublicKey;

    invoke-virtual {v0}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAPublicKey;->getOpenSSLKey()Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->key:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

    :goto_0
    return-void

    :cond_1
    instance-of v2, p1, Ljava/security/interfaces/DSAPublicKey;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->engineType:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature$EngineType;

    sget-object v3, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature$EngineType;->DSA:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature$EngineType;

    if-eq v2, v3, :cond_2

    new-instance v2, Ljava/security/InvalidKeyException;

    const-string v3, "Signature not initialized as DSA"

    invoke-direct {v2, v3}, Ljava/security/InvalidKeyException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_2
    move-object v0, p1

    check-cast v0, Ljava/security/interfaces/DSAPublicKey;

    invoke-static {v0}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAPublicKey;->getInstance(Ljava/security/interfaces/DSAPublicKey;)Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->key:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

    goto :goto_0

    :cond_3
    instance-of v2, p1, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLRSAPublicKey;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->engineType:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature$EngineType;

    sget-object v3, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature$EngineType;->RSA:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature$EngineType;

    if-eq v2, v3, :cond_4

    new-instance v2, Ljava/security/InvalidKeyException;

    const-string v3, "Signature not initialized as RSA"

    invoke-direct {v2, v3}, Ljava/security/InvalidKeyException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_4
    move-object v1, p1

    check-cast v1, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLRSAPublicKey;

    invoke-virtual {v1}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLRSAPublicKey;->getOpenSSLKey()Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->key:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

    goto :goto_0

    :cond_5
    instance-of v2, p1, Ljava/security/interfaces/RSAPublicKey;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->engineType:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature$EngineType;

    sget-object v3, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature$EngineType;->RSA:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature$EngineType;

    if-eq v2, v3, :cond_6

    new-instance v2, Ljava/security/InvalidKeyException;

    const-string v3, "Signature not initialized as RSA"

    invoke-direct {v2, v3}, Ljava/security/InvalidKeyException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_6
    move-object v1, p1

    check-cast v1, Ljava/security/interfaces/RSAPublicKey;

    invoke-static {v1}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLRSAPublicKey;->getInstance(Ljava/security/interfaces/RSAPublicKey;)Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->key:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

    goto :goto_0

    :cond_7
    new-instance v2, Ljava/security/InvalidKeyException;

    const-string v3, "Need DSA or RSA public key"

    invoke-direct {v2, v3}, Ljava/security/InvalidKeyException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method protected engineSetParameter(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidParameterException;
        }
    .end annotation

    return-void
.end method

.method protected engineSign()[B
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/SignatureException;
        }
    .end annotation

    iget-object v4, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->key:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

    if-nez v4, :cond_0

    new-instance v4, Ljava/security/SignatureException;

    const-string v5, "Need DSA or RSA private key"

    invoke-direct {v4, v5}, Ljava/security/SignatureException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_0
    :try_start_0
    iget-object v4, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->key:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

    invoke-virtual {v4}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;->getPkeyContext()I

    move-result v4

    invoke-static {v4}, Lorg/apache/harmony/xnet/provider/jsse/NativeCrypto;->EVP_PKEY_size(I)I

    move-result v4

    new-array v0, v4, [B

    iget v4, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->ctx:I

    const/4 v5, 0x0

    iget-object v6, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->key:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

    invoke-virtual {v6}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;->getPkeyContext()I

    move-result v6

    invoke-static {v4, v0, v5, v6}, Lorg/apache/harmony/xnet/provider/jsse/NativeCrypto;->EVP_SignFinal(I[BII)I

    move-result v1

    new-array v3, v1, [B

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v0, v4, v3, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->destroyContextIfExists()V

    return-object v3

    :catch_0
    move-exception v2

    :try_start_1
    new-instance v4, Ljava/security/SignatureException;

    invoke-direct {v4, v2}, Ljava/security/SignatureException;-><init>(Ljava/lang/Throwable;)V

    throw v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v4

    invoke-direct {p0}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->destroyContextIfExists()V

    throw v4
.end method

.method protected engineUpdate(B)V
    .locals 3
    .param p1    # B

    const/4 v2, 0x0

    iget-object v0, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->singleByte:[B

    aput-byte p1, v0, v2

    iget-object v0, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->singleByte:[B

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v2, v1}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->engineUpdate([BII)V

    return-void
.end method

.method protected engineUpdate([BII)V
    .locals 3
    .param p1    # [B
    .param p2    # I
    .param p3    # I

    iget v1, p0, Ljava/security/Signature;->state:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    iget v1, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->ctx:I

    if-nez v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->evpAlgorithm:Ljava/lang/String;

    invoke-static {v1}, Lorg/apache/harmony/xnet/provider/jsse/NativeCrypto;->EVP_SignInit(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->ctx:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    iget v1, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->ctx:I

    invoke-static {v1, p1, p2, p3}, Lorg/apache/harmony/xnet/provider/jsse/NativeCrypto;->EVP_SignUpdate(I[BII)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_1
    iget v1, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->ctx:I

    if-nez v1, :cond_2

    :try_start_1
    iget-object v1, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->evpAlgorithm:Ljava/lang/String;

    invoke-static {v1}, Lorg/apache/harmony/xnet/provider/jsse/NativeCrypto;->EVP_VerifyInit(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->ctx:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_2
    iget v1, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->ctx:I

    invoke-static {v1, p1, p2, p3}, Lorg/apache/harmony/xnet/provider/jsse/NativeCrypto;->EVP_VerifyUpdate(I[BII)V

    goto :goto_0

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method protected engineVerify([B)Z
    .locals 8
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/SignatureException;
        }
    .end annotation

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v4, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->key:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

    if-nez v4, :cond_0

    new-instance v2, Ljava/security/SignatureException;

    const-string v3, "Need DSA or RSA public key"

    invoke-direct {v2, v3}, Ljava/security/SignatureException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    :try_start_0
    iget v4, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->ctx:I

    const/4 v5, 0x0

    array-length v6, p1

    iget-object v7, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->key:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

    invoke-virtual {v7}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;->getPkeyContext()I

    move-result v7

    invoke-static {v4, p1, v5, v6, v7}, Lorg/apache/harmony/xnet/provider/jsse/NativeCrypto;->EVP_VerifyFinal(I[BIII)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-ne v1, v2, :cond_1

    :goto_0
    invoke-direct {p0}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->destroyContextIfExists()V

    :goto_1
    return v2

    :cond_1
    move v2, v3

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-direct {p0}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->destroyContextIfExists()V

    move v2, v3

    goto :goto_1

    :catchall_0
    move-exception v2

    invoke-direct {p0}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->destroyContextIfExists()V

    throw v2
.end method

.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    :try_start_0
    iget v0, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->ctx:I

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignature;->ctx:I

    invoke-static {v0}, Lorg/apache/harmony/xnet/provider/jsse/NativeCrypto;->EVP_MD_CTX_destroy(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    return-void

    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method
