.class Landroid/bluetooth/BluetoothSimap$1;
.super Ljava/lang/Object;
.source "BluetoothSimap.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/bluetooth/BluetoothSimap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/bluetooth/BluetoothSimap;


# direct methods
.method constructor <init>(Landroid/bluetooth/BluetoothSimap;)V
    .locals 0

    iput-object p1, p0, Landroid/bluetooth/BluetoothSimap$1;->this$0:Landroid/bluetooth/BluetoothSimap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Landroid/os/IBinder;

    const-string v0, "Proxy object connected"

    invoke-static {v0}, Landroid/bluetooth/BluetoothSimap;->access$000(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/bluetooth/BluetoothSimap$1;->this$0:Landroid/bluetooth/BluetoothSimap;

    invoke-static {p2}, Landroid/bluetooth/IBluetoothSimap$Stub;->asInterface(Landroid/os/IBinder;)Landroid/bluetooth/IBluetoothSimap;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/bluetooth/BluetoothSimap;->access$102(Landroid/bluetooth/BluetoothSimap;Landroid/bluetooth/IBluetoothSimap;)Landroid/bluetooth/IBluetoothSimap;

    iget-object v0, p0, Landroid/bluetooth/BluetoothSimap$1;->this$0:Landroid/bluetooth/BluetoothSimap;

    invoke-static {v0}, Landroid/bluetooth/BluetoothSimap;->access$200(Landroid/bluetooth/BluetoothSimap;)Landroid/bluetooth/BluetoothSimap$ServiceListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/bluetooth/BluetoothSimap$1;->this$0:Landroid/bluetooth/BluetoothSimap;

    invoke-static {v0}, Landroid/bluetooth/BluetoothSimap;->access$200(Landroid/bluetooth/BluetoothSimap;)Landroid/bluetooth/BluetoothSimap$ServiceListener;

    move-result-object v0

    invoke-interface {v0}, Landroid/bluetooth/BluetoothSimap$ServiceListener;->onServiceConnected()V

    :cond_0
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1    # Landroid/content/ComponentName;

    const-string v0, "Proxy object disconnected"

    invoke-static {v0}, Landroid/bluetooth/BluetoothSimap;->access$000(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/bluetooth/BluetoothSimap$1;->this$0:Landroid/bluetooth/BluetoothSimap;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/bluetooth/BluetoothSimap;->access$102(Landroid/bluetooth/BluetoothSimap;Landroid/bluetooth/IBluetoothSimap;)Landroid/bluetooth/IBluetoothSimap;

    iget-object v0, p0, Landroid/bluetooth/BluetoothSimap$1;->this$0:Landroid/bluetooth/BluetoothSimap;

    invoke-static {v0}, Landroid/bluetooth/BluetoothSimap;->access$200(Landroid/bluetooth/BluetoothSimap;)Landroid/bluetooth/BluetoothSimap$ServiceListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/bluetooth/BluetoothSimap$1;->this$0:Landroid/bluetooth/BluetoothSimap;

    invoke-static {v0}, Landroid/bluetooth/BluetoothSimap;->access$200(Landroid/bluetooth/BluetoothSimap;)Landroid/bluetooth/BluetoothSimap$ServiceListener;

    move-result-object v0

    invoke-interface {v0}, Landroid/bluetooth/BluetoothSimap$ServiceListener;->onServiceDisconnected()V

    :cond_0
    return-void
.end method
