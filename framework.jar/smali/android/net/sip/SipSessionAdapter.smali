.class public Landroid/net/sip/SipSessionAdapter;
.super Landroid/net/sip/ISipSessionListener$Stub;
.source "SipSessionAdapter.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/net/sip/ISipSessionListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onCallBusy(Landroid/net/sip/ISipSession;)V
    .locals 0
    .param p1    # Landroid/net/sip/ISipSession;

    return-void
.end method

.method public onCallChangeFailed(Landroid/net/sip/ISipSession;ILjava/lang/String;)V
    .locals 0
    .param p1    # Landroid/net/sip/ISipSession;
    .param p2    # I
    .param p3    # Ljava/lang/String;

    return-void
.end method

.method public onCallEnded(Landroid/net/sip/ISipSession;)V
    .locals 0
    .param p1    # Landroid/net/sip/ISipSession;

    return-void
.end method

.method public onCallEstablished(Landroid/net/sip/ISipSession;Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/net/sip/ISipSession;
    .param p2    # Ljava/lang/String;

    return-void
.end method

.method public onCallTransferring(Landroid/net/sip/ISipSession;Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/net/sip/ISipSession;
    .param p2    # Ljava/lang/String;

    return-void
.end method

.method public onCalling(Landroid/net/sip/ISipSession;)V
    .locals 0
    .param p1    # Landroid/net/sip/ISipSession;

    return-void
.end method

.method public onError(Landroid/net/sip/ISipSession;ILjava/lang/String;)V
    .locals 0
    .param p1    # Landroid/net/sip/ISipSession;
    .param p2    # I
    .param p3    # Ljava/lang/String;

    return-void
.end method

.method public onRegistering(Landroid/net/sip/ISipSession;)V
    .locals 0
    .param p1    # Landroid/net/sip/ISipSession;

    return-void
.end method

.method public onRegistrationDone(Landroid/net/sip/ISipSession;I)V
    .locals 0
    .param p1    # Landroid/net/sip/ISipSession;
    .param p2    # I

    return-void
.end method

.method public onRegistrationFailed(Landroid/net/sip/ISipSession;ILjava/lang/String;)V
    .locals 0
    .param p1    # Landroid/net/sip/ISipSession;
    .param p2    # I
    .param p3    # Ljava/lang/String;

    return-void
.end method

.method public onRegistrationTimeout(Landroid/net/sip/ISipSession;)V
    .locals 0
    .param p1    # Landroid/net/sip/ISipSession;

    return-void
.end method

.method public onRinging(Landroid/net/sip/ISipSession;Landroid/net/sip/SipProfile;Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/net/sip/ISipSession;
    .param p2    # Landroid/net/sip/SipProfile;
    .param p3    # Ljava/lang/String;

    return-void
.end method

.method public onRingingBack(Landroid/net/sip/ISipSession;)V
    .locals 0
    .param p1    # Landroid/net/sip/ISipSession;

    return-void
.end method
