.class Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;
.super Landroid/os/Handler;
.source "GeminiDataSubUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)V
    .locals 0

    iput-object p1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 11
    .param p1    # Landroid/os/Message;

    const/4 v10, 0x0

    const/4 v9, 0x1

    iget v7, p1, Landroid/os/Message;->what:I

    sparse-switch v7, :sswitch_data_0

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/os/AsyncResult;

    iget-object v7, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-nez v7, :cond_1

    const-string v7, "EVENT_PHONE_1_GPRS_ATTACHED"

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v7, "EVENT_PHONE_1_GPRS_ATTACHED:Failed"

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_1
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/os/AsyncResult;

    iget-object v7, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-nez v7, :cond_3

    const-string v7, "EVENT_PHONE_1_GPRS_DETACHED"

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    const-string v8, "Recv EVENT_PHONE_1_GPRS_DETACHED:"

    invoke-static {v7, v8}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$100(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;Ljava/lang/String;)V

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7, v10}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$200(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;I)Lcom/android/internal/telephony/PhoneProxy;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/internal/telephony/PhoneProxy;->getActivePhone()Lcom/android/internal/telephony/Phone;

    move-result-object v3

    instance-of v7, v3, Lcom/android/internal/telephony/gsm/GSMPhone;

    if-eqz v7, :cond_2

    check-cast v3, Lcom/android/internal/telephony/gsm/GSMPhone;

    iget-object v7, v3, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    check-cast v7, Lcom/android/internal/telephony/gsm/GsmDataConnectionTracker;

    move-object v6, v7

    check-cast v6, Lcom/android/internal/telephony/gsm/GsmDataConnectionTracker;

    invoke-virtual {v6}, Lcom/android/internal/telephony/gsm/GsmDataConnectionTracker;->gprsDetachResetAPN()V

    :cond_2
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    const/4 v8, 0x6

    invoke-static {v7, v10, v8}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$300(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;II)V

    goto :goto_0

    :cond_3
    const-string v7, "EVENT_PHONE_1_GPRS_DETACHED:Failed"

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    const-string v8, "Recv EVENT_PHONE_1_GPRS_DETACHED_FAIL:"

    invoke-static {v7, v8}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$100(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;Ljava/lang/String;)V

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$400(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Z

    move-result-object v7

    aget-boolean v7, v7, v10

    if-nez v7, :cond_4

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    const/16 v8, 0x59

    invoke-virtual {v7, v10, v9, v8}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->setThisSimAbortGprsAttachByDetach(IZI)V

    goto :goto_0

    :cond_4
    const-string v7, "EVENT_PHONE_1_GPRS_DETACHED: skip retry since power off."

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_2
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/os/AsyncResult;

    iget-object v7, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-nez v7, :cond_5

    const-string v7, "EVENT_PHONE_2_GPRS_ATTACHED"

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    const-string v7, "EVENT_PHONE_2_GPRS_ATTACHED:Failed"

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_3
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/os/AsyncResult;

    iget-object v7, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-nez v7, :cond_7

    const-string v7, "EVENT_PHONE_2_GPRS_DETACHED"

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    const-string v8, "Recv EVENT_PHONE_2_GPRS_DETACHED:"

    invoke-static {v7, v8}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$100(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;Ljava/lang/String;)V

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7, v9}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$200(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;I)Lcom/android/internal/telephony/PhoneProxy;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/internal/telephony/PhoneProxy;->getActivePhone()Lcom/android/internal/telephony/Phone;

    move-result-object v3

    instance-of v7, v3, Lcom/android/internal/telephony/gsm/GSMPhone;

    if-eqz v7, :cond_6

    check-cast v3, Lcom/android/internal/telephony/gsm/GSMPhone;

    iget-object v7, v3, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    check-cast v7, Lcom/android/internal/telephony/gsm/GsmDataConnectionTracker;

    move-object v6, v7

    check-cast v6, Lcom/android/internal/telephony/gsm/GsmDataConnectionTracker;

    invoke-virtual {v6}, Lcom/android/internal/telephony/gsm/GsmDataConnectionTracker;->gprsDetachResetAPN()V

    :cond_6
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    const/4 v8, 0x7

    invoke-static {v7, v9, v8}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$300(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;II)V

    goto/16 :goto_0

    :cond_7
    const-string v7, "EVENT_PHONE_2_GPRS_DETACHED:Failed"

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    const-string v8, "Recv EVENT_PHONE_2_GPRS_DETACHED_FAIL:"

    invoke-static {v7, v8}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$100(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;Ljava/lang/String;)V

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$400(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Z

    move-result-object v7

    aget-boolean v7, v7, v9

    if-nez v7, :cond_8

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    const/16 v8, 0x63

    invoke-virtual {v7, v9, v9, v8}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->setThisSimAbortGprsAttachByDetach(IZI)V

    goto/16 :goto_0

    :cond_8
    const-string v7, "EVENT_PHONE_2_GPRS_DETACHED:skip retry since power off."

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_4
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v7, Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "EVENT_MONITOR_GPRS_ATTACH Timeout:previous_reqattach_simId="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    const-string v8, "Recv MONITOR_GPRS_ATTACH Timeout Event:"

    invoke-static {v7, v8}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$100(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;Ljava/lang/String;)V

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v7

    if-ne v7, v4, :cond_0

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$600(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    aget-object v7, v7, v4

    sget-object v8, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->ATTACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-eq v7, v8, :cond_9

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$600(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    aget-object v7, v7, v4

    sget-object v8, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v7, v8, :cond_0

    :cond_9
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$700(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Ljava/lang/String;

    move-result-object v7

    aget-object v0, v7, v4

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$600(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    sget-object v8, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aput-object v8, v7, v4

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    const/4 v8, -0x1

    invoke-static {v7, v8}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$502(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;I)I

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$800(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Z

    move-result-object v7

    aput-boolean v10, v7, v4

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$900(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Z

    move-result-object v7

    aput-boolean v9, v7, v4

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1000(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Ljava/lang/Object;

    move-result-object v8

    monitor-enter v8

    :try_start_0
    sget-boolean v7, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->$assertionsDisabled:Z

    if-nez v7, :cond_a

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1100(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    move-result-object v7

    sget-object v9, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->ATTACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    if-eq v7, v9, :cond_a

    new-instance v7, Ljava/lang/AssertionError;

    invoke-direct {v7}, Ljava/lang/AssertionError;-><init>()V

    throw v7

    :catchall_0
    move-exception v7

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v7

    :cond_a
    :try_start_1
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    sget-object v9, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->KICKING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    invoke-static {v7, v9}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1102(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;)Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-string v7, "[C5]Kicking off an op and check states"

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1200(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Landroid/os/Handler;

    move-result-object v7

    const/16 v8, 0x14

    invoke-virtual {v7, v8}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    iput v4, v2, Landroid/os/Message;->arg2:I

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1200(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Landroid/os/Handler;

    move-result-object v7

    invoke-virtual {v7, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const-string/jumbo v7, "mms"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_b

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7, v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$200(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;I)Lcom/android/internal/telephony/PhoneProxy;

    move-result-object v7

    const-string/jumbo v8, "noSuchPdp"

    invoke-virtual {v7, v8, v0}, Lcom/android/internal/telephony/PhoneProxy;->notifyDataConnection(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_b
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7, v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$200(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;I)Lcom/android/internal/telephony/PhoneProxy;

    move-result-object v7

    const-string v8, "gprsAttachedTimeout"

    invoke-virtual {v7, v8, v0}, Lcom/android/internal/telephony/PhoneProxy;->notifyDataConnection(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_5
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v7, Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "EVENT_MONITOR_GPRS_DETACH Timeout:previous_reqdetach_simId="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    const-string v8, "Recv MONITOR_GPRS_DETACH Timeout Event:"

    invoke-static {v7, v8}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$100(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;Ljava/lang/String;)V

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    const/4 v8, 0x5

    invoke-static {v7, v5, v8}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$300(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;II)V

    goto/16 :goto_0

    :sswitch_6
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1000(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Ljava/lang/Object;

    move-result-object v8

    monitor-enter v8

    :try_start_2
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Recv EVENT_KICK_OFF_ONE:psOpState="

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v9, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v9}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1100(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    sget-boolean v7, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->$assertionsDisabled:Z

    if-nez v7, :cond_c

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1100(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    move-result-object v7

    sget-object v9, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->KICKING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    if-eq v7, v9, :cond_c

    new-instance v7, Ljava/lang/AssertionError;

    invoke-direct {v7}, Ljava/lang/AssertionError;-><init>()V

    throw v7

    :catchall_1
    move-exception v7

    monitor-exit v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v7

    :cond_c
    :try_start_3
    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    const-string v7, "Wait for enable procedure"

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1300(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Ljava/lang/Object;

    move-result-object v8

    monitor-enter v8

    :try_start_4
    const-string v7, "enable procedure is done"

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    monitor-exit v8
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1400(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Ljava/lang/Object;

    move-result-object v8

    monitor-enter v8

    :try_start_5
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Recv EVENT_KICK_OFF_ONE:req_ps_state1="

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v9, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v9}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v9

    const/4 v10, 0x0

    aget-object v9, v9, v10

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, ",req_ps_state2="

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v9, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v9}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v9

    const/4 v10, 0x1

    aget-object v9, v9, v10

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    const-string v9, "[KICK_OFF_OP]"

    invoke-static {v7, v9}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$100(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;Ljava/lang/String;)V

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$600(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    const/4 v9, 0x0

    aget-object v7, v7, v9

    sget-object v9, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v7, v9, :cond_16

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$600(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    const/4 v9, 0x1

    aget-object v7, v7, v9

    sget-object v9, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v7, v9, :cond_16

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    const/4 v9, 0x0

    aget-object v7, v7, v9

    sget-object v9, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-eq v7, v9, :cond_d

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    const/4 v9, 0x0

    aget-object v7, v7, v9

    if-nez v7, :cond_12

    :cond_d
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    const/4 v9, 0x1

    aget-object v7, v7, v9

    sget-object v9, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-eq v7, v9, :cond_e

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    const/4 v9, 0x1

    aget-object v7, v7, v9

    if-nez v7, :cond_12

    :cond_e
    const-string v7, "[C1]Same both Detached states,but check if detached due to monitor attach timeout"

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$900(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Z

    move-result-object v7

    const/4 v9, 0x0

    aget-boolean v7, v7, v9

    if-eqz v7, :cond_10

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1000(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Ljava/lang/Object;

    move-result-object v9

    monitor-enter v9
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :try_start_6
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    sget-object v10, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->DETACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    invoke-static {v7, v10}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1102(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;)Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    monitor-exit v9
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    :try_start_7
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    const/4 v9, 0x0

    invoke-static {v7, v9}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1600(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;I)I

    :cond_f
    :goto_1
    monitor-exit v8

    goto/16 :goto_0

    :catchall_2
    move-exception v7

    monitor-exit v8
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    throw v7

    :catchall_3
    move-exception v7

    :try_start_8
    monitor-exit v8
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    throw v7

    :catchall_4
    move-exception v7

    :try_start_9
    monitor-exit v9
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    :try_start_a
    throw v7

    :cond_10
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$900(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Z

    move-result-object v7

    const/4 v9, 0x1

    aget-boolean v7, v7, v9

    if-eqz v7, :cond_11

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1000(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Ljava/lang/Object;

    move-result-object v9

    monitor-enter v9
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    :try_start_b
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    sget-object v10, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->DETACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    invoke-static {v7, v10}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1102(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;)Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    monitor-exit v9
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_5

    :try_start_c
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    const/4 v9, 0x1

    invoke-static {v7, v9}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1600(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;I)I
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    goto :goto_1

    :catchall_5
    move-exception v7

    :try_start_d
    monitor-exit v9
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_5

    :try_start_e
    throw v7

    :cond_11
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1000(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Ljava/lang/Object;

    move-result-object v9

    monitor-enter v9
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    :try_start_f
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    sget-object v10, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->NONE:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    invoke-static {v7, v10}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1102(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;)Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    monitor-exit v9

    goto :goto_1

    :catchall_6
    move-exception v7

    monitor-exit v9
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_6

    :try_start_10
    throw v7

    :cond_12
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    const/4 v9, 0x0

    aget-object v7, v7, v9

    sget-object v9, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->ATTACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v7, v9, :cond_14

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    const/4 v9, 0x1

    aget-object v7, v7, v9

    sget-object v9, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-eq v7, v9, :cond_13

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    const/4 v9, 0x1

    aget-object v7, v7, v9

    if-nez v7, :cond_14

    :cond_13
    const-string v7, "[C2]Compare current and requested gprs states(from NONE to SIM1)"

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1000(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Ljava/lang/Object;

    move-result-object v9

    monitor-enter v9
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_2

    :try_start_11
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    sget-object v10, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->ATTACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    invoke-static {v7, v10}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1102(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;)Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    monitor-exit v9
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_7

    :try_start_12
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    const/4 v9, 0x0

    invoke-static {v7, v9}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1700(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;I)I
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_2

    goto/16 :goto_1

    :catchall_7
    move-exception v7

    :try_start_13
    monitor-exit v9
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_7

    :try_start_14
    throw v7

    :cond_14
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    const/4 v9, 0x0

    aget-object v7, v7, v9

    sget-object v9, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-eq v7, v9, :cond_15

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    const/4 v9, 0x0

    aget-object v7, v7, v9

    if-nez v7, :cond_f

    :cond_15
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    const/4 v9, 0x1

    aget-object v7, v7, v9

    sget-object v9, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->ATTACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v7, v9, :cond_f

    const-string v7, "[C3]Compare current and requested gprs states(from NONE to SIM2)"

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1000(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Ljava/lang/Object;

    move-result-object v9

    monitor-enter v9
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_2

    :try_start_15
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    sget-object v10, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->ATTACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    invoke-static {v7, v10}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1102(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;)Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    monitor-exit v9
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_8

    :try_start_16
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    const/4 v9, 0x1

    invoke-static {v7, v9}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1700(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;I)I
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_2

    goto/16 :goto_1

    :catchall_8
    move-exception v7

    :try_start_17
    monitor-exit v9
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_8

    :try_start_18
    throw v7

    :cond_16
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$600(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    const/4 v9, 0x0

    aget-object v7, v7, v9

    sget-object v9, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v7, v9, :cond_1f

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$600(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    const/4 v9, 0x1

    aget-object v7, v7, v9

    sget-object v9, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->ATTACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v7, v9, :cond_1f

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    const/4 v9, 0x0

    aget-object v7, v7, v9

    sget-object v9, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-eq v7, v9, :cond_17

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    const/4 v9, 0x0

    aget-object v7, v7, v9

    if-nez v7, :cond_18

    :cond_17
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    const/4 v9, 0x1

    aget-object v7, v7, v9

    sget-object v9, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->ATTACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v7, v9, :cond_18

    const-string v7, "[C4]Same states(SIM1:Detached;SIM2:Attached)"

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1000(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Ljava/lang/Object;

    move-result-object v9

    monitor-enter v9
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_2

    :try_start_19
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    sget-object v10, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->NONE:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    invoke-static {v7, v10}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1102(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;)Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    monitor-exit v9

    goto/16 :goto_1

    :catchall_9
    move-exception v7

    monitor-exit v9
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_9

    :try_start_1a
    throw v7

    :cond_18
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    const/4 v9, 0x0

    aget-object v7, v7, v9

    sget-object v9, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->ATTACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v7, v9, :cond_1a

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    const/4 v9, 0x1

    aget-object v7, v7, v9

    sget-object v9, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-eq v7, v9, :cond_19

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    const/4 v9, 0x1

    aget-object v7, v7, v9

    if-nez v7, :cond_1a

    :cond_19
    const-string v7, "[C5]Compare current and requested gprs states(from SIM2 to SIM1)"

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1000(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Ljava/lang/Object;

    move-result-object v9

    monitor-enter v9
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_2

    :try_start_1b
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    sget-object v10, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->DETACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    invoke-static {v7, v10}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1102(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;)Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    monitor-exit v9
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_a

    :try_start_1c
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    const/4 v9, 0x0

    invoke-static {v7, v9}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$502(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;I)I

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    const/4 v9, 0x1

    invoke-static {v7, v9}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1802(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;Z)Z

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    const/4 v9, 0x1

    invoke-static {v7, v9}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1600(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;I)I
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_2

    goto/16 :goto_1

    :catchall_a
    move-exception v7

    :try_start_1d
    monitor-exit v9
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_a

    :try_start_1e
    throw v7

    :cond_1a
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    const/4 v9, 0x0

    aget-object v7, v7, v9

    sget-object v9, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-eq v7, v9, :cond_1b

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    const/4 v9, 0x0

    aget-object v7, v7, v9

    if-nez v7, :cond_1c

    :cond_1b
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    const/4 v9, 0x1

    aget-object v7, v7, v9

    sget-object v9, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v7, v9, :cond_1c

    const-string v7, "[C6]Compare current and requested gprs states(from SIM2 to NONE)"

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1000(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Ljava/lang/Object;

    move-result-object v9

    monitor-enter v9
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_2

    :try_start_1f
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    sget-object v10, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->DETACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    invoke-static {v7, v10}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1102(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;)Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    monitor-exit v9
    :try_end_1f
    .catchall {:try_start_1f .. :try_end_1f} :catchall_b

    :try_start_20
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    const/4 v9, 0x1

    invoke-static {v7, v9}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1600(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;I)I
    :try_end_20
    .catchall {:try_start_20 .. :try_end_20} :catchall_2

    goto/16 :goto_1

    :catchall_b
    move-exception v7

    :try_start_21
    monitor-exit v9
    :try_end_21
    .catchall {:try_start_21 .. :try_end_21} :catchall_b

    :try_start_22
    throw v7

    :cond_1c
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    const/4 v9, 0x0

    aget-object v7, v7, v9

    if-nez v7, :cond_1d

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    const/4 v9, 0x1

    aget-object v7, v7, v9

    if-nez v7, :cond_1d

    const-string v7, "[C7]Power On:SIM2 attached"

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1000(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Ljava/lang/Object;

    move-result-object v9

    monitor-enter v9
    :try_end_22
    .catchall {:try_start_22 .. :try_end_22} :catchall_2

    :try_start_23
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    sget-object v10, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->NONE:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    invoke-static {v7, v10}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1102(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;)Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    monitor-exit v9

    goto/16 :goto_1

    :catchall_c
    move-exception v7

    monitor-exit v9
    :try_end_23
    .catchall {:try_start_23 .. :try_end_23} :catchall_c

    :try_start_24
    throw v7

    :cond_1d
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    const/4 v9, 0x0

    aget-object v7, v7, v9

    sget-object v9, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v7, v9, :cond_1e

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    const/4 v9, 0x1

    aget-object v7, v7, v9

    if-nez v7, :cond_1e

    const-string v7, "[C7-2]Power On:SIM2 attached,then requests to send MMS via SIM2 via check SIM1\'s detached first"

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1000(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Ljava/lang/Object;

    move-result-object v9

    monitor-enter v9
    :try_end_24
    .catchall {:try_start_24 .. :try_end_24} :catchall_2

    :try_start_25
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    sget-object v10, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->NONE:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    invoke-static {v7, v10}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1102(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;)Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    monitor-exit v9

    goto/16 :goto_1

    :catchall_d
    move-exception v7

    monitor-exit v9
    :try_end_25
    .catchall {:try_start_25 .. :try_end_25} :catchall_d

    :try_start_26
    throw v7

    :cond_1e
    const-string v7, "[C7-3]Not update gprs_op_state:still as KICKING"

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_1f
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$600(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    const/4 v9, 0x0

    aget-object v7, v7, v9

    sget-object v9, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->ATTACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v7, v9, :cond_28

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$600(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    const/4 v9, 0x1

    aget-object v7, v7, v9

    sget-object v9, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v7, v9, :cond_28

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    const/4 v9, 0x0

    aget-object v7, v7, v9

    sget-object v9, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->ATTACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v7, v9, :cond_21

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    const/4 v9, 0x1

    aget-object v7, v7, v9

    sget-object v9, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-eq v7, v9, :cond_20

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    const/4 v9, 0x1

    aget-object v7, v7, v9

    if-nez v7, :cond_21

    :cond_20
    const-string v7, "[C8]Same states(SIM1:Attached;SIM2:Detached)"

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1000(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Ljava/lang/Object;

    move-result-object v9

    monitor-enter v9
    :try_end_26
    .catchall {:try_start_26 .. :try_end_26} :catchall_2

    :try_start_27
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    sget-object v10, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->NONE:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    invoke-static {v7, v10}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1102(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;)Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    monitor-exit v9

    goto/16 :goto_1

    :catchall_e
    move-exception v7

    monitor-exit v9
    :try_end_27
    .catchall {:try_start_27 .. :try_end_27} :catchall_e

    :try_start_28
    throw v7

    :cond_21
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    const/4 v9, 0x0

    aget-object v7, v7, v9

    sget-object v9, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-eq v7, v9, :cond_22

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    const/4 v9, 0x0

    aget-object v7, v7, v9

    if-nez v7, :cond_23

    :cond_22
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    const/4 v9, 0x1

    aget-object v7, v7, v9

    sget-object v9, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->ATTACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v7, v9, :cond_23

    const-string v7, "[C9]Compare current and requested gprs states(from SIM1 to SIM2)"

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    const/4 v9, 0x1

    invoke-static {v7, v9}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$502(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;I)I

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    const/4 v9, 0x1

    invoke-static {v7, v9}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1802(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;Z)Z

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1000(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Ljava/lang/Object;

    move-result-object v9

    monitor-enter v9
    :try_end_28
    .catchall {:try_start_28 .. :try_end_28} :catchall_2

    :try_start_29
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    sget-object v10, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->DETACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    invoke-static {v7, v10}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1102(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;)Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    monitor-exit v9
    :try_end_29
    .catchall {:try_start_29 .. :try_end_29} :catchall_f

    :try_start_2a
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    const/4 v9, 0x0

    invoke-static {v7, v9}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1600(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;I)I
    :try_end_2a
    .catchall {:try_start_2a .. :try_end_2a} :catchall_2

    goto/16 :goto_1

    :catchall_f
    move-exception v7

    :try_start_2b
    monitor-exit v9
    :try_end_2b
    .catchall {:try_start_2b .. :try_end_2b} :catchall_f

    :try_start_2c
    throw v7

    :cond_23
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    const/4 v9, 0x0

    aget-object v7, v7, v9

    sget-object v9, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v7, v9, :cond_25

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    const/4 v9, 0x1

    aget-object v7, v7, v9

    sget-object v9, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-eq v7, v9, :cond_24

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    const/4 v9, 0x1

    aget-object v7, v7, v9

    if-nez v7, :cond_25

    :cond_24
    const-string v7, "[C10]Compare current and requested gprs states(from SIM1 to NONE)"

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1000(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Ljava/lang/Object;

    move-result-object v9

    monitor-enter v9
    :try_end_2c
    .catchall {:try_start_2c .. :try_end_2c} :catchall_2

    :try_start_2d
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    sget-object v10, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->DETACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    invoke-static {v7, v10}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1102(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;)Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    monitor-exit v9
    :try_end_2d
    .catchall {:try_start_2d .. :try_end_2d} :catchall_10

    :try_start_2e
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    const/4 v9, 0x0

    invoke-static {v7, v9}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1600(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;I)I
    :try_end_2e
    .catchall {:try_start_2e .. :try_end_2e} :catchall_2

    goto/16 :goto_1

    :catchall_10
    move-exception v7

    :try_start_2f
    monitor-exit v9
    :try_end_2f
    .catchall {:try_start_2f .. :try_end_2f} :catchall_10

    :try_start_30
    throw v7

    :cond_25
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    const/4 v9, 0x0

    aget-object v7, v7, v9

    if-nez v7, :cond_26

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    const/4 v9, 0x1

    aget-object v7, v7, v9

    if-nez v7, :cond_26

    const-string v7, "[C11]Power On:SIM1 attached"

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1000(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Ljava/lang/Object;

    move-result-object v9

    monitor-enter v9
    :try_end_30
    .catchall {:try_start_30 .. :try_end_30} :catchall_2

    :try_start_31
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    sget-object v10, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->NONE:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    invoke-static {v7, v10}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1102(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;)Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    monitor-exit v9

    goto/16 :goto_1

    :catchall_11
    move-exception v7

    monitor-exit v9
    :try_end_31
    .catchall {:try_start_31 .. :try_end_31} :catchall_11

    :try_start_32
    throw v7

    :cond_26
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    const/4 v9, 0x0

    aget-object v7, v7, v9

    if-nez v7, :cond_27

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    const/4 v9, 0x1

    aget-object v7, v7, v9

    sget-object v9, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v7, v9, :cond_27

    const-string v7, "[C11-2]Power On:SIM1 attached,then requests to send MMS via SIM1 via check SIM2\'s detached first"

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1000(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Ljava/lang/Object;

    move-result-object v9

    monitor-enter v9
    :try_end_32
    .catchall {:try_start_32 .. :try_end_32} :catchall_2

    :try_start_33
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    sget-object v10, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->NONE:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    invoke-static {v7, v10}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1102(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;)Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    monitor-exit v9

    goto/16 :goto_1

    :catchall_12
    move-exception v7

    monitor-exit v9
    :try_end_33
    .catchall {:try_start_33 .. :try_end_33} :catchall_12

    :try_start_34
    throw v7

    :cond_27
    const-string v7, "[C11-3]Not update gprs_op_state:still as KICKING"

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_28
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$600(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    const/4 v9, 0x0

    aget-object v7, v7, v9

    sget-object v9, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->ATTACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v7, v9, :cond_f

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$600(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    const/4 v9, 0x1

    aget-object v7, v7, v9

    sget-object v9, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->ATTACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v7, v9, :cond_f

    const-string v7, "Not expceted states both SIM1 and SIM2 attached!"

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$600(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v7

    const/4 v9, 0x0

    aget-object v7, v7, v9

    sget-object v9, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->ATTACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v7, v9, :cond_29

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    const/4 v9, 0x1

    invoke-virtual {v7, v9}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->resetGprsRelatedContext(I)V

    :goto_2
    sget-boolean v7, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->$assertionsDisabled:Z

    if-nez v7, :cond_f

    new-instance v7, Ljava/lang/AssertionError;

    invoke-direct {v7}, Ljava/lang/AssertionError;-><init>()V

    throw v7

    :cond_29
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    const/4 v9, 0x0

    invoke-virtual {v7, v9}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->resetGprsRelatedContext(I)V
    :try_end_34
    .catchall {:try_start_34 .. :try_end_34} :catchall_2

    goto :goto_2

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x4 -> :sswitch_3
        0x5 -> :sswitch_4
        0x6 -> :sswitch_5
        0x14 -> :sswitch_6
    .end sparse-switch
.end method
