.class Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;
.super Ljava/lang/Object;
.source "GeminiDataSubUtil.java"

# interfaces
.implements Lcom/android/internal/telephony/DefaultPhoneNotifier$IDataStateChangedCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)V
    .locals 0

    iput-object p1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDataStateChanged(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)V
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Z
    .param p6    # I

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[DataStateChanged]:state="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ",reason="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ",apnName="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ",apnType="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ",from sim_id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    if-eqz p2, :cond_10

    const-string v4, "dataAttached"

    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_10

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    const-string v5, "Recv GPRS_ATTACHED:"

    invoke-static {v4, v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$100(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4, p6}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$200(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;I)Lcom/android/internal/telephony/PhoneProxy;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/internal/telephony/PhoneProxy;->getActivePhone()Lcom/android/internal/telephony/Phone;

    move-result-object v4

    check-cast v4, Lcom/android/internal/telephony/PhoneBase;

    invoke-virtual {v4}, Lcom/android/internal/telephony/PhoneBase;->getServiceStateTracker()Lcom/android/internal/telephony/ServiceStateTracker;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/internal/telephony/ServiceStateTracker;->getCurrentDataConnectionState()I

    move-result v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[DataStateChanged]:gprsState="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    if-eqz v0, :cond_1

    const-string v4, "[DataStateChanged]: not expect gprs attach event"

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-virtual {v4, p6}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->stopMonitorGprsAttachTimer(I)V

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$900(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Z

    move-result-object v4

    const/4 v5, 0x0

    aput-boolean v5, v4, p6

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1900(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v4

    const/4 v5, -0x1

    if-ne v4, v5, :cond_4

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v4

    const/4 v5, -0x1

    if-ne v4, v5, :cond_4

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4, p6}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1902(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;I)I

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$600(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v4

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1900(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v5

    sget-object v6, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->ATTACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aput-object v6, v4, v5

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "From no GPRS on both sims to attached in sim_id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1900(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v6}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1900(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v6

    invoke-static {v4, v5, v6}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$2000(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;II)I

    :cond_2
    :goto_1
    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1000(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ATTATCHED Intent:psOpState="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v6}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1100(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1100(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    move-result-object v4

    sget-object v6, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->ATTACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    if-eq v4, v6, :cond_3

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1100(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    move-result-object v4

    sget-object v6, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->NONE:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    if-ne v4, v6, :cond_f

    :cond_3
    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    sget-object v6, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->KICKING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    invoke-static {v4, v6}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1102(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;)Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    const-string v4, "[C1]Kicking off an op and check states"

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1200(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Landroid/os/Handler;

    move-result-object v4

    const/16 v6, 0x14

    invoke-virtual {v4, v6}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    iput p6, v1, Landroid/os/Message;->arg2:I

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1200(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :goto_2
    monitor-exit v5

    goto/16 :goto_0

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    :cond_4
    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1900(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v4

    const/4 v5, -0x1

    if-ne v4, v5, :cond_8

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$600(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v4

    aget-object v4, v4, p6

    sget-object v5, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v4, v5, :cond_8

    if-nez p6, :cond_7

    const/4 v2, 0x1

    :goto_3
    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4, p6}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1902(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;I)I

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    const/4 v5, -0x1

    invoke-static {v4, v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$502(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;I)I

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$600(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v4

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1900(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v5

    sget-object v6, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->ATTACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aput-object v6, v4, v5

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "GPRS Attached reported by NW for the previous attach timeout simId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$600(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v4

    aget-object v4, v4, v2

    sget-object v5, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v4, v5, :cond_6

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "cancel monitor detach timer for simId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$600(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v4

    sget-object v5, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aput-object v5, v4, v2

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-virtual {v4, v2}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->stopMonitorGprsDetachTimer(I)V

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1000(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    :try_start_1
    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1100(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    move-result-object v4

    sget-object v6, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->DETACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    if-ne v4, v6, :cond_5

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    sget-object v6, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->KICKING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    invoke-static {v4, v6}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1102(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;)Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    const/4 v6, 0x0

    invoke-static {v4, v6}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1802(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;Z)Z

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    const/4 v6, 0x0

    invoke-static {v4, v6}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$2102(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;Z)Z

    const-string v4, "[C6]Kicking off an op and check states"

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1200(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Landroid/os/Handler;

    move-result-object v4

    const/16 v6, 0x14

    invoke-virtual {v4, v6}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    iput p6, v1, Landroid/os/Message;->arg2:I

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1200(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_5
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_6
    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    const/4 v5, 0x4

    iget-object v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v6}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1900(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v6

    invoke-static {v4, v5, v6}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$2000(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;II)I

    goto/16 :goto_1

    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_3

    :catchall_1
    move-exception v4

    :try_start_2
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v4

    :cond_8
    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1900(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v4

    const/4 v5, -0x1

    if-ne v4, v5, :cond_9

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$600(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v4

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v5

    aget-object v4, v4, v5

    sget-object v5, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->ATTACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v4, v5, :cond_9

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_9

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v4

    if-ne v4, p6, :cond_9

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v5

    invoke-static {v4, v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1902(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;I)I

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    const/4 v5, -0x1

    invoke-static {v4, v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$502(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;I)I

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$600(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v4

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1900(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v5

    sget-object v6, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->ATTACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aput-object v6, v4, v5

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    const/4 v5, 0x2

    iget-object v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v6}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1900(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v6

    invoke-static {v4, v5, v6}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$2000(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;II)I

    goto/16 :goto_1

    :cond_9
    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1900(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_d

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$600(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v4

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1900(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v5

    aget-object v4, v4, v5

    sget-object v5, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v4, v5, :cond_d

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1900(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v4

    if-ne v4, p6, :cond_d

    const/4 v3, -0x1

    if-nez p6, :cond_b

    const/4 v3, 0x1

    :goto_4
    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_a

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v4

    if-ne v4, p6, :cond_c

    :cond_a
    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$600(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v4

    aget-object v4, v4, v3

    sget-object v5, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v4, v5, :cond_c

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Recv NW Attached again for sim_id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$600(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v4

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1900(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v5

    sget-object v6, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->ATTACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aput-object v6, v4, v5

    goto/16 :goto_1

    :cond_b
    const/4 v3, 0x0

    goto :goto_4

    :cond_c
    const-string v4, "Recv NW Attached but invalid case!"

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    sget-boolean v4, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->$assertionsDisabled:Z

    if-nez v4, :cond_2

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    :cond_d
    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1900(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v4

    if-ne v4, p6, :cond_e

    const-string v4, "NONE"

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$700(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v6}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1900(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v6

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_e

    const-string v4, "Attached but current request enable APN type is not NONE."

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$600(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v4

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1900(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v5

    sget-object v6, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->ATTACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aput-object v6, v4, v5

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    const/4 v5, 0x5

    invoke-static {v4, v5, p6}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$2000(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;II)I

    goto/16 :goto_1

    :cond_e
    const-string v4, "Not matched any cases for Attached"

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$2200(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_f
    :try_start_3
    const-string v4, "Recv ATTACHED Intent:Fail to kick"

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_2

    :cond_10
    if-eqz p2, :cond_21

    const-string v4, "dataDetached"

    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_21

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    const-string v5, "Recv GPRS_DETACHED:"

    invoke-static {v4, v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$100(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4, p6}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$200(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;I)Lcom/android/internal/telephony/PhoneProxy;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/internal/telephony/PhoneProxy;->getActivePhone()Lcom/android/internal/telephony/Phone;

    move-result-object v4

    check-cast v4, Lcom/android/internal/telephony/PhoneBase;

    invoke-virtual {v4}, Lcom/android/internal/telephony/PhoneBase;->getServiceStateTracker()Lcom/android/internal/telephony/ServiceStateTracker;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/internal/telephony/ServiceStateTracker;->getCurrentDataConnectionState()I

    move-result v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[DataStateChanged]:gprsState="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    const/4 v4, 0x1

    if-eq v0, v4, :cond_11

    const-string v4, "[DataStateChanged]: not expect gprs attach event"

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_11
    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-virtual {v4, p6}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->stopMonitorGprsDetachTimer(I)V

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1900(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v4

    if-eq p6, v4, :cond_12

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1800(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Z

    move-result v4

    if-eqz v4, :cond_1a

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_1a

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v4

    if-eq v4, p6, :cond_1a

    :cond_12
    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1800(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Z

    move-result v4

    if-eqz v4, :cond_18

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1802(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;Z)Z

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$600(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v4

    sget-object v5, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aput-object v5, v4, p6

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v4

    if-eqz v4, :cond_13

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v4

    const/4 v5, 0x1

    if-eq v4, v5, :cond_13

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "some error happened, request_use_gprs_sim_id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$2300(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_13
    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$600(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v4

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v5

    sget-object v6, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->ATTACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aput-object v6, v4, v5

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v5

    const/4 v6, 0x3

    invoke-virtual {v4, v5, v6}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->startMonitorGprsAttachTimer(II)V

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v5

    const/4 v6, 0x0

    const/4 v7, 0x3

    invoke-virtual {v4, v5, v6, v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->setPeerSimAbortAttachbyDetachOrAsGprsWhenNeededMode(IZI)V

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    const/4 v5, -0x1

    invoke-static {v4, v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1902(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;I)I

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Recv Detached for sim_id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ",then perform Attach for sim_id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1000(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    :try_start_4
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[C1]DETATCHED Intent:psOpState="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v6}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1100(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1100(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    move-result-object v4

    sget-object v6, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->DETACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    if-ne v4, v6, :cond_17

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    sget-object v6, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->ATTACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    invoke-static {v4, v6}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1102(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;)Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    :cond_14
    monitor-exit v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$2400(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Lcom/android/internal/telephony/gemini/GeminiPhone;

    move-result-object v4

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v6}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/android/internal/telephony/gemini/GeminiPhone;->setGprsConnType(II)V

    :cond_15
    :goto_5
    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1000(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    :try_start_5
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[C3]DETATCHED Intent:psOpState="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v6}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1100(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1100(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    move-result-object v4

    sget-object v6, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->DETACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    if-ne v4, v6, :cond_20

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    sget-object v6, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->KICKING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    invoke-static {v4, v6}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1102(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;)Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    const-string v4, "[C2]Kicking off an op and check states"

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1200(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Landroid/os/Handler;

    move-result-object v4

    const/16 v6, 0x14

    invoke-virtual {v4, v6}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    iput p6, v1, Landroid/os/Message;->arg2:I

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1200(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_16
    monitor-exit v5

    goto/16 :goto_0

    :catchall_2
    move-exception v4

    monitor-exit v5
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v4

    :cond_17
    :try_start_6
    sget-boolean v4, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->$assertionsDisabled:Z

    if-nez v4, :cond_14

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    :catchall_3
    move-exception v4

    monitor-exit v5
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    throw v4

    :cond_18
    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$600(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v4

    aget-object v4, v4, p6

    sget-object v5, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-eq v4, v5, :cond_19

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$600(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v4

    sget-object v5, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aput-object v5, v4, p6

    const-string v4, "Recv NW Detached but not triggered by DATA FW"

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    goto :goto_5

    :cond_19
    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    const/4 v5, 0x0

    const/4 v6, 0x3

    invoke-virtual {v4, p6, v5, v6}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->setThisSimAbortGprsAttachByDetach(IZI)V

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$600(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v4

    sget-object v5, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aput-object v5, v4, p6

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    const/4 v5, -0x1

    invoke-static {v4, v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1902(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;I)I

    const-string v4, "Recv NW Detached and triggered by DATA FW itself"

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    goto/16 :goto_5

    :cond_1a
    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v4

    if-ne p6, v4, :cond_1c

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$600(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v4

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v5

    aget-object v4, v4, v5

    sget-object v5, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->ATTACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v4, v5, :cond_1b

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$600(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v4

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v5

    sget-object v6, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aput-object v6, v4, v5

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Recv NW Detached automatically for requested simId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    goto/16 :goto_5

    :cond_1b
    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$600(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v4

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v5

    aget-object v4, v4, v5

    sget-object v5, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v4, v5, :cond_15

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v5

    const/4 v6, 0x0

    const/4 v7, 0x4

    invoke-virtual {v4, v5, v6, v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->setThisSimAbortGprsAttachByDetach(IZI)V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Recv NW Detached caused by app for requested simId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$600(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v4

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v5

    sget-object v6, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aput-object v6, v4, v5

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    const/4 v5, -0x1

    invoke-static {v4, v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$502(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;I)I

    goto/16 :goto_5

    :cond_1c
    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$600(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v4

    aget-object v4, v4, p6

    sget-object v5, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v4, v5, :cond_1f

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$2100(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Z

    move-result v4

    if-eqz v4, :cond_1f

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1800(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Z

    move-result v4

    if-eqz v4, :cond_15

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1802(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;Z)Z

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$2102(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;Z)Z

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v5

    const/4 v6, 0x0

    const/4 v7, 0x4

    invoke-virtual {v4, v5, v6, v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->setPeerSimAbortAttachbyDetachOrAsGprsWhenNeededMode(IZI)V

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$600(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    move-result-object v4

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v5

    sget-object v6, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->ATTACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aput-object v6, v4, v5

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v5

    const/4 v6, 0x4

    invoke-virtual {v4, v5, v6}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->startMonitorGprsAttachTimer(II)V

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$2400(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Lcom/android/internal/telephony/gemini/GeminiPhone;

    move-result-object v4

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v6}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/android/internal/telephony/gemini/GeminiPhone;->setGprsConnType(II)V

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1000(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    :try_start_7
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[C2]DETATCHED Intent:psOpState="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v6}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1100(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1100(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    move-result-object v4

    sget-object v6, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->DETACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    if-ne v4, v6, :cond_1e

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    sget-object v6, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->ATTACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    invoke-static {v4, v6}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$1102(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;)Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    :cond_1d
    monitor-exit v5

    goto/16 :goto_5

    :catchall_4
    move-exception v4

    monitor-exit v5
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    throw v4

    :cond_1e
    :try_start_8
    sget-boolean v4, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->$assertionsDisabled:Z

    if-nez v4, :cond_1d

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    :cond_1f
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Recv NW Detached but not for this sim_id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$000(Ljava/lang/String;)V

    goto/16 :goto_5

    :cond_20
    :try_start_9
    sget-boolean v4, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->$assertionsDisabled:Z

    if-nez v4, :cond_16

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    :cond_21
    if-eqz p2, :cond_0

    const-string/jumbo v4, "radioTurnedOff"

    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    const-string v5, "RADIO_TURNED_OFF:"

    invoke-static {v4, v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->access$100(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
