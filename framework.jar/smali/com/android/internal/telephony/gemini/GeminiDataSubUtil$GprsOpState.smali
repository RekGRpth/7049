.class final enum Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;
.super Ljava/lang/Enum;
.source "GeminiDataSubUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "GprsOpState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

.field public static final enum ATTACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

.field public static final enum DETACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

.field public static final enum KICKING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

.field public static final enum NONE:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->NONE:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    new-instance v0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    const-string v1, "DETACHING"

    invoke-direct {v0, v1, v3}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->DETACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    new-instance v0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    const-string v1, "ATTACHING"

    invoke-direct {v0, v1, v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->ATTACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    new-instance v0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    const-string v1, "KICKING"

    invoke-direct {v0, v1, v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->KICKING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    sget-object v1, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->NONE:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->DETACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->ATTACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->KICKING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->$VALUES:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    return-object v0
.end method

.method public static values()[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;
    .locals 1

    sget-object v0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->$VALUES:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    invoke-virtual {v0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    sget-object v0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$3;->$SwitchMap$com$android$internal$telephony$gemini$GeminiDataSubUtil$GprsOpState:[I

    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const-string v0, "INVAILED"

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "NONE"

    goto :goto_0

    :pswitch_1
    const-string v0, "DETACHING"

    goto :goto_0

    :pswitch_2
    const-string v0, "ATTACHING"

    goto :goto_0

    :pswitch_3
    const-string v0, "KICKING"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
