.class final enum Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;
.super Ljava/lang/Enum;
.source "GeminiDataSubUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "GprsState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

.field public static final enum ATTACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

.field public static final enum ATTACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

.field public static final enum DETACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

.field public static final enum DETACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

.field public static final enum REACTIVE:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

.field public static final enum RECOVERY:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    const-string v1, "DETACHED"

    invoke-direct {v0, v1, v3}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    new-instance v0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    const-string v1, "ATTACHING"

    invoke-direct {v0, v1, v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->ATTACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    new-instance v0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    const-string v1, "ATTACHED"

    invoke-direct {v0, v1, v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->ATTACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    new-instance v0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    const-string v1, "DETACHING"

    invoke-direct {v0, v1, v6}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    new-instance v0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    const-string v1, "RECOVERY"

    invoke-direct {v0, v1, v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->RECOVERY:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    new-instance v0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    const-string v1, "REACTIVE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->REACTIVE:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    sget-object v1, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->ATTACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->ATTACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->RECOVERY:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->REACTIVE:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->$VALUES:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    return-object v0
.end method

.method public static values()[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;
    .locals 1

    sget-object v0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->$VALUES:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    invoke-virtual {v0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    sget-object v0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$3;->$SwitchMap$com$android$internal$telephony$gemini$GeminiDataSubUtil$GprsState:[I

    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const-string v0, "UNKNOWN"

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "DETACHED"

    goto :goto_0

    :pswitch_1
    const-string v0, "ATTACHING"

    goto :goto_0

    :pswitch_2
    const-string v0, "ATTACHED"

    goto :goto_0

    :pswitch_3
    const-string v0, "DETACHING"

    goto :goto_0

    :pswitch_4
    const-string v0, "RECOVERY"

    goto :goto_0

    :pswitch_5
    const-string v0, "REACTIVE"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
