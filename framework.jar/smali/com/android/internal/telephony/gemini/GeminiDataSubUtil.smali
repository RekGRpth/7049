.class public Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;
.super Ljava/lang/Object;
.source "GeminiDataSubUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$3;,
        Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GeminiDataStateReceiver;,
        Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;,
        Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final DATA_DIALOG_ENABLE:Z = false

.field private static final DBG:Z = true

.field private static final DELAY_EXECUTE_GPRS_ATTACH_TIME:I = 0x1388

.field private static final EVENT_DELAYED_PHONE_1_GPRS_ATTACH:I = 0x7

.field private static final EVENT_DELAYED_PHONE_2_GPRS_ATTACH:I = 0x8

.field private static final EVENT_KICK_OFF_ONE_GPRS_OP:I = 0x14

.field private static final EVENT_MONITOR_GPRS_ATTACH:I = 0x5

.field private static final EVENT_MONITOR_GPRS_DETACH:I = 0x6

.field private static final EVENT_PHONE1_ON_VOICE_CALL_END:I = 0x9

.field private static final EVENT_PHONE2_ON_VOICE_CALL_END:I = 0xa

.field private static final EVENT_PHONE_1_GPRS_ATTACHED:I = 0x1

.field private static final EVENT_PHONE_1_GPRS_DETACHED:I = 0x2

.field private static final EVENT_PHONE_2_GPRS_ATTACHED:I = 0x3

.field private static final EVENT_PHONE_2_GPRS_DETACHED:I = 0x4

.field private static final GEMINI_ALWAYS_EXECUTE_DETACH_BEFORE_ATTACH:Z = true

.field private static final GEMINI_DETACH_ONE_CMD_SOLUTION:Z = true

.field private static final GEMINI_PS_BEHAVIOR_MODEL_1:Z = false

.field private static final LOG_TAG:Ljava/lang/String; = "PHONE"

.field private static final MONITOR_GPRS_ATTACH_TIMEOUT:I = 0xea60

.field private static final MONITOR_GPRS_DETACH_TIMEOUT:I = 0x1388

.field private static final SIM_NONE:I = -0x1

.field private static mStartEnableMMSApn:Z


# instance fields
.field private current_use_gprs_sim_id:I

.field private enable_monitor_gprs_attach_timer:[Z

.field private enable_monitor_gprs_detach_timer:[Z

.field private gprs_op_state:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

.field private keep_gprs_attach_mode:[Z

.field private mActivePhoneProxy:Lcom/android/internal/telephony/Phone;

.field protected mContext:Landroid/content/Context;

.field private mDataStateChangedCallback:Lcom/android/internal/telephony/DefaultPhoneNotifier$IDataStateChangedCallback;

.field private mDetachResetMode:Z

.field private mDisableGprsStateSync:Ljava/lang/Object;

.field private mEnableApnTypeReq:[Ljava/lang/String;

.field private mExecuteAttachAfterDetach:Z

.field private mForceDetachAgain:[Z

.field private mGeminiDataRspHander:Landroid/os/Handler;

.field private mGeminiPhone:Lcom/android/internal/telephony/gemini/GeminiPhone;

.field private mGeminiStateReceiver:Landroid/content/BroadcastReceiver;

.field private mGprsOpStateSync:Ljava/lang/Object;

.field private mPhoneProxy1:Lcom/android/internal/telephony/Phone;

.field private mPhoneProxy2:Lcom/android/internal/telephony/Phone;

.field private mRequestedGprsStateSync:Ljava/lang/Object;

.field private mServicePowerOffFlag:[Z

.field private previous_current_use_gprs_sim_id:I

.field private previous_request_use_gprs_sim_id:I

.field private request_use_gprs_sim_id:I

.field private requested_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

.field private sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

.field private switch_detach_due_to_monitor_gprs_attach_timeout:[Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    const-class v0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->$assertionsDisabled:Z

    sput-boolean v1, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mStartEnableMMSApn:Z

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public constructor <init>(Lcom/android/internal/telephony/gemini/GeminiPhone;Lcom/android/internal/telephony/Phone;Lcom/android/internal/telephony/Phone;)V
    .locals 7
    .param p1    # Lcom/android/internal/telephony/gemini/GeminiPhone;
    .param p2    # Lcom/android/internal/telephony/Phone;
    .param p3    # Lcom/android/internal/telephony/Phone;

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, -0x1

    const/4 v3, 0x2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v2, v3, [Z

    fill-array-data v2, :array_0

    iput-object v2, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mServicePowerOffFlag:[Z

    iput v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->current_use_gprs_sim_id:I

    iput v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->request_use_gprs_sim_id:I

    iput v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->previous_current_use_gprs_sim_id:I

    iput v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->previous_request_use_gprs_sim_id:I

    new-array v2, v3, [Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    iput-object v2, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    new-array v2, v3, [Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    iput-object v2, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->requested_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    new-array v2, v3, [Z

    fill-array-data v2, :array_1

    iput-object v2, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->enable_monitor_gprs_attach_timer:[Z

    new-array v2, v3, [Z

    fill-array-data v2, :array_2

    iput-object v2, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->enable_monitor_gprs_detach_timer:[Z

    new-array v2, v3, [Z

    fill-array-data v2, :array_3

    iput-object v2, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->switch_detach_due_to_monitor_gprs_attach_timeout:[Z

    new-array v2, v3, [Z

    fill-array-data v2, :array_4

    iput-object v2, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->keep_gprs_attach_mode:[Z

    sget-object v2, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->NONE:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    iput-object v2, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->gprs_op_state:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    new-array v2, v3, [Ljava/lang/String;

    iput-object v2, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mEnableApnTypeReq:[Ljava/lang/String;

    iput-boolean v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mExecuteAttachAfterDetach:Z

    iput-boolean v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mDetachResetMode:Z

    new-array v2, v3, [Z

    fill-array-data v2, :array_5

    iput-object v2, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mForceDetachAgain:[Z

    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mRequestedGprsStateSync:Ljava/lang/Object;

    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mGprsOpStateSync:Ljava/lang/Object;

    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mDisableGprsStateSync:Ljava/lang/Object;

    new-instance v2, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;

    invoke-direct {v2, p0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;-><init>(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)V

    iput-object v2, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mGeminiDataRspHander:Landroid/os/Handler;

    new-instance v2, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;

    invoke-direct {v2, p0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$2;-><init>(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)V

    iput-object v2, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mDataStateChangedCallback:Lcom/android/internal/telephony/DefaultPhoneNotifier$IDataStateChangedCallback;

    iput-object p1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mGeminiPhone:Lcom/android/internal/telephony/gemini/GeminiPhone;

    move-object v2, p2

    check-cast v2, Lcom/android/internal/telephony/PhoneProxy;

    iput-object v2, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mActivePhoneProxy:Lcom/android/internal/telephony/Phone;

    check-cast p2, Lcom/android/internal/telephony/PhoneProxy;

    iput-object p2, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mPhoneProxy1:Lcom/android/internal/telephony/Phone;

    check-cast p3, Lcom/android/internal/telephony/PhoneProxy;

    iput-object p3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mPhoneProxy2:Lcom/android/internal/telephony/Phone;

    iget-object v2, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mActivePhoneProxy:Lcom/android/internal/telephony/Phone;

    invoke-interface {v2}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v2

    iput-object v2, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mContext:Landroid/content/Context;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v2, "android.intent.action.DATA_CONNECTION_FAILED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.intent.action.SERVICE_STATE"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    new-instance v2, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GeminiDataStateReceiver;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GeminiDataStateReceiver;-><init>(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$1;)V

    iput-object v2, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mGeminiStateReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mGeminiStateReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v1

    iget-object v2, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    sget-object v3, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aput-object v3, v2, v5

    iget-object v2, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    sget-object v3, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aput-object v3, v2, v6

    iget-object v2, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mEnableApnTypeReq:[Ljava/lang/String;

    const-string v3, "NONE"

    aput-object v3, v2, v5

    iget-object v2, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mEnableApnTypeReq:[Ljava/lang/String;

    const-string v3, "NONE"

    aput-object v3, v2, v6

    return-void

    nop

    :array_0
    .array-data 1
        0x1t
        0x1t
    .end array-data

    nop

    :array_1
    .array-data 1
        0x0t
        0x0t
    .end array-data

    nop

    :array_2
    .array-data 1
        0x0t
        0x0t
    .end array-data

    nop

    :array_3
    .array-data 1
        0x0t
        0x0t
    .end array-data

    nop

    :array_4
    .array-data 1
        0x0t
        0x0t
    .end array-data

    nop

    :array_5
    .array-data 1
        0x0t
        0x0t
    .end array-data
.end method

.method static synthetic access$000(Ljava/lang/String;)V
    .locals 0
    .param p0    # Ljava/lang/String;

    invoke-static {p0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->dumpSimsGprsState(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mGprsOpStateSync:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;
    .locals 1
    .param p0    # Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->gprs_op_state:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;)Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;
    .locals 0
    .param p0    # Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;
    .param p1    # Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    iput-object p1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->gprs_op_state:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mGeminiDataRspHander:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mDisableGprsStateSync:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mRequestedGprsStateSync:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;
    .locals 1
    .param p0    # Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->requested_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;I)I
    .locals 1
    .param p0    # Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->executeDisableDataConnectivity(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$1700(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;I)I
    .locals 1
    .param p0    # Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->executeEnableDataConnectivity(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$1800(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Z
    .locals 1
    .param p0    # Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    iget-boolean v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mExecuteAttachAfterDetach:Z

    return v0
.end method

.method static synthetic access$1802(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;Z)Z
    .locals 0
    .param p0    # Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mExecuteAttachAfterDetach:Z

    return p1
.end method

.method static synthetic access$1900(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I
    .locals 1
    .param p0    # Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    iget v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->current_use_gprs_sim_id:I

    return v0
.end method

.method static synthetic access$1902(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;I)I
    .locals 0
    .param p0    # Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;
    .param p1    # I

    iput p1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->current_use_gprs_sim_id:I

    return p1
.end method

.method static synthetic access$200(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;I)Lcom/android/internal/telephony/PhoneProxy;
    .locals 1
    .param p0    # Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->selectActivePhoneProxy(I)Lcom/android/internal/telephony/PhoneProxy;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2000(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;II)I
    .locals 1
    .param p0    # Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;
    .param p1    # I
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->enableApnAfterGprsAttached(II)I

    move-result v0

    return v0
.end method

.method static synthetic access$2100(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Z
    .locals 1
    .param p0    # Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    iget-boolean v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mDetachResetMode:Z

    return v0
.end method

.method static synthetic access$2102(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;Z)Z
    .locals 0
    .param p0    # Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mDetachResetMode:Z

    return p1
.end method

.method static synthetic access$2200(Ljava/lang/String;)V
    .locals 0
    .param p0    # Ljava/lang/String;

    invoke-static {p0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logw(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2300(Ljava/lang/String;)V
    .locals 0
    .param p0    # Ljava/lang/String;

    invoke-static {p0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->loge(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2400(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)Lcom/android/internal/telephony/gemini/GeminiPhone;
    .locals 1
    .param p0    # Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mGeminiPhone:Lcom/android/internal/telephony/gemini/GeminiPhone;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;II)V
    .locals 0
    .param p0    # Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;
    .param p1    # I
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->processGprsDetached(II)V

    return-void
.end method

.method static synthetic access$400(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Z
    .locals 1
    .param p0    # Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mServicePowerOffFlag:[Z

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)I
    .locals 1
    .param p0    # Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    iget v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->request_use_gprs_sim_id:I

    return v0
.end method

.method static synthetic access$502(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;I)I
    .locals 0
    .param p0    # Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;
    .param p1    # I

    iput p1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->request_use_gprs_sim_id:I

    return p1
.end method

.method static synthetic access$600(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;
    .locals 1
    .param p0    # Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    return-object v0
.end method

.method static synthetic access$700(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mEnableApnTypeReq:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Z
    .locals 1
    .param p0    # Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->enable_monitor_gprs_attach_timer:[Z

    return-object v0
.end method

.method static synthetic access$900(Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;)[Z
    .locals 1
    .param p0    # Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->switch_detach_due_to_monitor_gprs_attach_timeout:[Z

    return-object v0
.end method

.method private disableDataConnectivityDecideSimId()I
    .locals 4

    const/4 v3, -0x1

    iget v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->current_use_gprs_sim_id:I

    if-ne v0, v3, :cond_3

    iget v2, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->request_use_gprs_sim_id:I

    if-eq v2, v3, :cond_2

    iget-object v2, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    iget v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->request_use_gprs_sim_id:I

    aget-object v2, v2, v3

    sget-object v3, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v2, v3, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DDCDS:Already detaching for simId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->request_use_gprs_sim_id:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iget v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->request_use_gprs_sim_id:I

    move v1, v0

    :goto_0
    return v1

    :cond_0
    iget-object v2, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    iget v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->request_use_gprs_sim_id:I

    aget-object v2, v2, v3

    sget-object v3, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->ATTACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v2, v3, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DDCDS:Detach the previous attaching simId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->request_use_gprs_sim_id:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iget v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->request_use_gprs_sim_id:I

    move v1, v0

    goto :goto_0

    :cond_1
    const-string v2, "DDCDS:Invalid cases"

    invoke-static {v2}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logw(Ljava/lang/String;)V

    const/4 v0, -0x1

    move v1, v0

    goto :goto_0

    :cond_2
    const-string v2, "DDCDS:No GPRS is used on any sim cards"

    invoke-static {v2}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logw(Ljava/lang/String;)V

    const/4 v0, -0x1

    move v1, v0

    goto :goto_0

    :cond_3
    move v1, v0

    goto :goto_0
.end method

.method private dumpSimsGprsState(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "sim1_gprs_state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",sim2_gprs_state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aget-object v1, v1, v3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "current_gprs_sim_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->current_use_gprs_sim_id:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",req_gprs_sim_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->request_use_gprs_sim_id:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "mExecuteAttachAfterDetach="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mExecuteAttachAfterDetach:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",mDetachResetMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mDetachResetMode:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "mEnableApnTypeReq_1="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mEnableApnTypeReq:[Ljava/lang/String;

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",mEnableApnTypeReq_2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mEnableApnTypeReq:[Ljava/lang/String;

    aget-object v1, v1, v3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "mServicePowerOffFlag_1="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mServicePowerOffFlag:[Z

    aget-boolean v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",mServicePowerOffFlag_2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mServicePowerOffFlag:[Z

    aget-boolean v1, v1, v3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mGprsOpStateSync:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "gprs_op_state="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->gprs_op_state:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private enableApnAfterGprsAttached(II)I
    .locals 6
    .param p1    # I
    .param p2    # I

    const/4 v1, -0x1

    invoke-direct {p0, p2}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->selectActivePhoneProxy(I)Lcom/android/internal/telephony/PhoneProxy;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/internal/telephony/PhoneProxy;->getActivePhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    instance-of v3, v0, Lcom/android/internal/telephony/gsm/GSMPhone;

    if-eqz v3, :cond_1

    check-cast v0, Lcom/android/internal/telephony/gsm/GSMPhone;

    iget-object v3, v0, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    check-cast v3, Lcom/android/internal/telephony/gsm/GsmDataConnectionTracker;

    move-object v2, v3

    check-cast v2, Lcom/android/internal/telephony/gsm/GsmDataConnectionTracker;

    const-string v3, "default"

    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/gsm/GsmDataConnectionTracker;->isApnTypeEnabled(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-direct {p0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->getDataConnectionFromSetting()I

    move-result v3

    if-ne p2, v3, :cond_0

    iget-object v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mEnableApnTypeReq:[Ljava/lang/String;

    aget-object v3, v3, p2

    const-string v4, "NONE"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mEnableApnTypeReq:[Ljava/lang/String;

    const-string v4, "default"

    aput-object v4, v3, p2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[C"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]Attached: have to recover default data connection"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[C"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]Attached: getDataConnectionFromSetting()="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->getDataConnectionFromSetting()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    :cond_1
    iget-object v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mEnableApnTypeReq:[Ljava/lang/String;

    aget-object v3, v3, p2

    const-string v4, "NONE"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    invoke-direct {p0, p2}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->selectActivePhoneProxy(I)Lcom/android/internal/telephony/PhoneProxy;

    move-result-object v3

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mEnableApnTypeReq:[Ljava/lang/String;

    aget-object v4, v4, p2

    invoke-virtual {v3, v4}, Lcom/android/internal/telephony/PhoneProxy;->enableApnType(Ljava/lang/String;)I

    move-result v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[C"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]Attached:then enableApnType() with rc="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    if-nez v1, :cond_2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[C"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]Attached:notify DATA is CONNECTED"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    invoke-direct {p0, p2}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->selectActivePhoneProxy(I)Lcom/android/internal/telephony/PhoneProxy;

    move-result-object v3

    const-string v4, "dataEnabled"

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mEnableApnTypeReq:[Ljava/lang/String;

    aget-object v5, v5, p2

    invoke-virtual {v3, v4, v5}, Lcom/android/internal/telephony/PhoneProxy;->notifyDataConnection(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "enableApnAfterGprsAttached():reset mEnableApnTypeReq as NONE of sim_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mEnableApnTypeReq:[Ljava/lang/String;

    const-string v4, "NONE"

    aput-object v4, v3, p2

    return v1

    :cond_2
    const/4 v3, 0x1

    if-ne v1, v3, :cond_3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[C"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]Attached: wait for enableApnType() execution result"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const/16 v3, 0x63

    if-ne v1, v3, :cond_4

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[C"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]Attached but records are not loaded!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logw(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[C"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]Attached:notify DATA is DISCONNECTED"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    invoke-direct {p0, p2}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->selectActivePhoneProxy(I)Lcom/android/internal/telephony/PhoneProxy;

    move-result-object v3

    const-string/jumbo v4, "noSuchPdp"

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mEnableApnTypeReq:[Ljava/lang/String;

    aget-object v5, v5, p2

    invoke-virtual {v3, v4, v5}, Lcom/android/internal/telephony/PhoneProxy;->notifyDataConnection(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    const/4 v1, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[C"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]Attached for sim_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " successfully"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private enableDataConnectivityDecideSimId()I
    .locals 4

    const/4 v0, -0x1

    iget v1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->current_use_gprs_sim_id:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "EDCDS:simId is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  request sim id is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->request_use_gprs_sim_id:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    if-ne v1, v0, :cond_4

    iget v2, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->request_use_gprs_sim_id:I

    if-eq v2, v0, :cond_3

    iget-object v2, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    iget v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->request_use_gprs_sim_id:I

    aget-object v2, v2, v3

    sget-object v3, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->ATTACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v2, v3, :cond_1

    const-string v2, "EDCDS:enable is ongoing"

    invoke-static {v2}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iget v1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->request_use_gprs_sim_id:I

    move v0, v1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    iget v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->request_use_gprs_sim_id:I

    aget-object v2, v2, v3

    sget-object v3, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v2, v3, :cond_2

    const-string v2, "EDCDS:Can\'t enable due to previous disable is ongoing"

    invoke-static {v2}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string v2, "EDCDS:Invalid cases"

    invoke-static {v2}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logw(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->getDataConnectionFromSetting()I

    move-result v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "enableDataConnectivityDecideSimId(): both sim detached,query gprs sim setting mode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_0

    const/16 v0, 0x63

    goto :goto_0

    :cond_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "EDCDS:return simId is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    move v0, v1

    goto :goto_0
.end method

.method private enableDataConnectivityWithDualPsActive(I)I
    .locals 3
    .param p1    # I

    const/4 v2, 0x1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[DT] enableDataConnectivityWithDualPsActive(): sim="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iput p1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->request_use_gprs_sim_id:I

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    sget-object v1, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->ATTACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aput-object v1, v0, p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[DT]change state to="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->startMonitorGprsAttachTimer(II)V

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mGeminiPhone:Lcom/android/internal/telephony/gemini/GeminiPhone;

    invoke-virtual {v0, v2, p1}, Lcom/android/internal/telephony/gemini/GeminiPhone;->setGprsConnType(II)V

    return v2
.end method

.method private enableDataConnectivityWithOnePsActive(I)I
    .locals 8
    .param p1    # I

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    new-array v0, v5, [Ljava/lang/String;

    const-string v6, "AT+CGATT=1"

    aput-object v6, v0, v4

    const-string v6, ""

    aput-object v6, v0, v3

    new-array v1, v5, [Ljava/lang/String;

    const-string v6, "AT+CGATT=0"

    aput-object v6, v1, v4

    const-string v6, ""

    aput-object v6, v1, v3

    const-string v6, "WithOnePsActive:"

    invoke-direct {p0, v6}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->dumpSimsGprsState(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aget-object v6, v6, p1

    sget-object v7, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->ATTACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v6, v7, :cond_0

    const-string v3, "enableDataConnectivityWithOnePsActive-C0"

    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    move v3, v5

    :goto_0
    return v3

    :cond_0
    iget-object v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aget-object v6, v6, p1

    sget-object v7, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->ATTACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v6, v7, :cond_1

    const-string v4, "enableDataConnectivityWithOnePsActive-C1"

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iput p1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->request_use_gprs_sim_id:I

    goto :goto_0

    :cond_1
    iget-object v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aget-object v6, v6, p1

    sget-object v7, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v6, v7, :cond_2

    const-string v3, "enableDataConnectivityWithOnePsActive-C2"

    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    const/4 v3, 0x3

    goto :goto_0

    :cond_2
    iget-object v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aget-object v6, v6, p1

    sget-object v7, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v6, v7, :cond_14

    iget v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->current_use_gprs_sim_id:I

    const/4 v7, -0x1

    if-eq v6, v7, :cond_f

    iget v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->current_use_gprs_sim_id:I

    if-eq v6, p1, :cond_a

    iget-object v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    iget v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->current_use_gprs_sim_id:I

    aget-object v6, v6, v7

    sget-object v7, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v6, v7, :cond_3

    const-string v4, "enableDataConnectivityWithOnePsActive-C3"

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iput p1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->request_use_gprs_sim_id:I

    iput-boolean v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mExecuteAttachAfterDetach:Z

    goto :goto_0

    :cond_3
    iget-object v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    iget v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->current_use_gprs_sim_id:I

    aget-object v6, v6, v7

    sget-object v7, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v6, v7, :cond_9

    iget-boolean v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mExecuteAttachAfterDetach:Z

    if-nez v5, :cond_8

    const-string v5, "enableDataConnectivityWithOnePsActive-C4"

    invoke-static {v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iput p1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->request_use_gprs_sim_id:I

    invoke-direct {p0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->updateActivePhoneProxy()V

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->switch_detach_due_to_monitor_gprs_attach_timeout:[Z

    iget v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->current_use_gprs_sim_id:I

    aget-boolean v5, v5, v6

    if-nez v5, :cond_4

    :cond_4
    iput-boolean v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mExecuteAttachAfterDetach:Z

    iput-boolean v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mDetachResetMode:Z

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->switch_detach_due_to_monitor_gprs_attach_timeout:[Z

    iget v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->current_use_gprs_sim_id:I

    aput-boolean v4, v5, v6

    iget v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->request_use_gprs_sim_id:I

    if-nez v5, :cond_6

    move v2, v3

    :goto_1
    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    sget-object v6, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aput-object v6, v5, v2

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mForceDetachAgain:[Z

    aput-boolean v3, v5, v2

    iget v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->request_use_gprs_sim_id:I

    invoke-virtual {p0, v5, v3, v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->setPeerSimAbortAttachbyDetachOrAsGprsWhenNeededMode(IZI)V

    invoke-virtual {p0, v2, v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->startMonitorGprsDetachTimer(II)V

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mGprsOpStateSync:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[C1]enableDataConnectivityWithOnePsActive:psOpState="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->gprs_op_state:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->gprs_op_state:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    sget-object v6, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->ATTACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    if-ne v5, v6, :cond_7

    sget-object v5, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->DETACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    iput-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->gprs_op_state:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    :cond_5
    monitor-exit v4

    goto/16 :goto_0

    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    :cond_6
    move v2, v4

    goto :goto_1

    :cond_7
    :try_start_1
    sget-boolean v5, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->$assertionsDisabled:Z

    if-nez v5, :cond_5

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_8
    const-string v4, "enableDataConnectivityWithOnePsActive-C5"

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iput p1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->request_use_gprs_sim_id:I

    goto/16 :goto_0

    :cond_9
    const-string v4, "enableDataConnectivityWithOnePsActive-C6"

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iput p1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->request_use_gprs_sim_id:I

    iput-boolean v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mExecuteAttachAfterDetach:Z

    invoke-direct {p0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->updateActivePhoneProxy()V

    iget v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->current_use_gprs_sim_id:I

    invoke-virtual {p0, v4, v3, v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->setThisSimAbortGprsAttachByDetach(IZI)V

    goto/16 :goto_0

    :cond_a
    const-string v5, "enableDataConnectivityWithOnePsActive-C7"

    invoke-static {v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iput p1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->request_use_gprs_sim_id:I

    invoke-direct {p0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->updateActivePhoneProxy()V

    iget v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->request_use_gprs_sim_id:I

    if-nez v5, :cond_d

    move v2, v3

    :goto_2
    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->switch_detach_due_to_monitor_gprs_attach_timeout:[Z

    aget-boolean v5, v5, v2

    if-nez v5, :cond_b

    :cond_b
    iput-boolean v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mExecuteAttachAfterDetach:Z

    iput-boolean v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mDetachResetMode:Z

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->switch_detach_due_to_monitor_gprs_attach_timeout:[Z

    aput-boolean v4, v5, v2

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    sget-object v5, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aput-object v5, v4, v2

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mForceDetachAgain:[Z

    aput-boolean v3, v4, v2

    iget v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->request_use_gprs_sim_id:I

    invoke-virtual {p0, v4, v3, v3}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->setPeerSimAbortAttachbyDetachOrAsGprsWhenNeededMode(IZI)V

    invoke-virtual {p0, v2, v3}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->startMonitorGprsDetachTimer(II)V

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mGprsOpStateSync:Ljava/lang/Object;

    monitor-enter v4

    :try_start_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[C2]enableDataConnectivityWithOnePsActive:psOpState="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->gprs_op_state:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->gprs_op_state:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    sget-object v6, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->ATTACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    if-ne v5, v6, :cond_e

    sget-object v5, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->DETACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    iput-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->gprs_op_state:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    :cond_c
    monitor-exit v4

    goto/16 :goto_0

    :catchall_1
    move-exception v3

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v3

    :cond_d
    move v2, v4

    goto :goto_2

    :cond_e
    :try_start_3
    sget-boolean v5, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->$assertionsDisabled:Z

    if-nez v5, :cond_c

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :cond_f
    const-string v6, "enableDataConnectivityWithOnePsActive-C8"

    invoke-static {v6}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iput p1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->request_use_gprs_sim_id:I

    invoke-direct {p0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->updateActivePhoneProxy()V

    iget v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->request_use_gprs_sim_id:I

    if-nez v6, :cond_12

    move v2, v3

    :goto_3
    iget-object v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->switch_detach_due_to_monitor_gprs_attach_timeout:[Z

    aget-boolean v6, v6, v2

    if-nez v6, :cond_10

    :cond_10
    iput-boolean v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mExecuteAttachAfterDetach:Z

    iput-boolean v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mDetachResetMode:Z

    iget-object v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->switch_detach_due_to_monitor_gprs_attach_timeout:[Z

    aput-boolean v4, v6, v2

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    sget-object v6, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aput-object v6, v4, v2

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mForceDetachAgain:[Z

    aput-boolean v3, v4, v2

    iget v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->request_use_gprs_sim_id:I

    invoke-virtual {p0, v4, v3, v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->setPeerSimAbortAttachbyDetachOrAsGprsWhenNeededMode(IZI)V

    invoke-virtual {p0, v2, v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->startMonitorGprsDetachTimer(II)V

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mGprsOpStateSync:Ljava/lang/Object;

    monitor-enter v4

    :try_start_4
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[C3]enableDataConnectivityWithOnePsActive:psOpState="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->gprs_op_state:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->gprs_op_state:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    sget-object v6, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->ATTACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    if-ne v5, v6, :cond_13

    sget-object v5, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->DETACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    iput-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->gprs_op_state:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    :cond_11
    monitor-exit v4

    goto/16 :goto_0

    :catchall_2
    move-exception v3

    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw v3

    :cond_12
    move v2, v4

    goto :goto_3

    :cond_13
    :try_start_5
    sget-boolean v5, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->$assertionsDisabled:Z

    if-nez v5, :cond_11

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :cond_14
    const-string v3, "enableDataConnectivityWithOnePsActive-C9"

    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    const/4 v3, 0x4

    goto/16 :goto_0
.end method

.method private executeDisableDataConnectivity(I)I
    .locals 6
    .param p1    # I

    const/4 v5, 0x6

    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/String;

    const-string v1, "AT+CGATT=0"

    aput-object v1, v0, v4

    const-string v1, ""

    aput-object v1, v0, v3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "executeDisableDataConnectivity():for simId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aget-object v1, v1, p1

    sget-object v2, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v1, v2, :cond_0

    :goto_0
    return v5

    :cond_0
    iget-object v1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aget-object v1, v1, p1

    sget-object v2, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->switch_detach_due_to_monitor_gprs_attach_timeout:[Z

    aget-boolean v1, v1, p1

    if-nez v1, :cond_1

    :cond_1
    iget-object v1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->switch_detach_due_to_monitor_gprs_attach_timeout:[Z

    aput-boolean v4, v1, p1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Abort attach procedure by detach command then set as when needed on simId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    sget-object v2, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aput-object v2, v1, p1

    iget-object v1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mForceDetachAgain:[Z

    aput-boolean v3, v1, p1

    invoke-virtual {p0, p1, v3, v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->setThisSimAbortGprsAttachByDetach(IZI)V

    const/4 v1, 0x3

    invoke-virtual {p0, p1, v1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->startMonitorGprsDetachTimer(II)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    sget-object v2, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aput-object v2, v1, p1

    invoke-direct {p0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->updateActivePhoneProxy()V

    invoke-virtual {p0, p1, v3, v3}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->setThisSimAbortGprsAttachByDetach(IZI)V

    goto :goto_0
.end method

.method private executeEnableDataConnectivity(I)I
    .locals 2
    .param p1    # I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "executeEnableDataConnectivity():for simId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->enableDataConnectivityWithOnePsActive(I)I

    move-result v0

    return v0
.end method

.method private getDataConnectionFromSetting()I
    .locals 7

    const/4 v2, -0x1

    iget-object v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "gprs_connection_sim_setting"

    const-wide/16 v5, -0x5

    invoke-static {v3, v4, v5, v6}, Landroid/provider/Settings$System;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    iget-object v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mContext:Landroid/content/Context;

    invoke-static {v3, v0, v1}, Landroid/provider/Telephony$SIMInfo;->getSlotById(Landroid/content/Context;J)I

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Default Data Setting value="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    return v2
.end method

.method private isValidApnType(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "default"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "mms"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "supl"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "dun"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "hipri"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "fota"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ims"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "cbs"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "dm"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "wap"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "net"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "cmmail"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "rcse"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static logd(Ljava/lang/String;)V
    .locals 3
    .param p0    # Ljava/lang/String;

    const-string v0, "PHONE"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[GeminiDataSubUtil] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private static loge(Ljava/lang/String;)V
    .locals 3
    .param p0    # Ljava/lang/String;

    const-string v0, "PHONE"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[GeminiDataSubUtil] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private static logv(Ljava/lang/String;)V
    .locals 3
    .param p0    # Ljava/lang/String;

    const-string v0, "PHONE"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[GeminiDataSubUtil] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private static logw(Ljava/lang/String;)V
    .locals 3
    .param p0    # Ljava/lang/String;

    const-string v0, "PHONE"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[GeminiDataSubUtil] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private processGprsDetached(II)V
    .locals 9
    .param p1    # I
    .param p2    # I

    const/4 v8, -0x1

    const/4 v4, 0x1

    const/4 v5, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "processGprsDetached():req_detach_simId="

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ",inx="

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    if-nez p1, :cond_0

    move v2, v4

    :goto_0
    const/4 v1, 0x0

    iget-object v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->enable_monitor_gprs_detach_timer:[Z

    aget-boolean v3, v3, p1

    if-eqz v3, :cond_2

    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->stopMonitorGprsDetachTimer(I)V

    iget-object v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mGprsOpStateSync:Ljava/lang/Object;

    monitor-enter v6

    :try_start_0
    sget-boolean v3, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->$assertionsDisabled:Z

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->gprs_op_state:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    sget-object v7, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->DETACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    if-eq v3, v7, :cond_1

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    :catchall_0
    move-exception v3

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    :cond_0
    move v2, v5

    goto :goto_0

    :cond_1
    :try_start_1
    const-string/jumbo v3, "processGprsDetached():Reset psOpState as NONE"

    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    sget-object v3, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->NONE:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    iput-object v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->gprs_op_state:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_2
    iget-object v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mForceDetachAgain:[Z

    aget-boolean v3, v3, p1

    if-eqz v3, :cond_4

    iget v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->current_use_gprs_sim_id:I

    if-ne p1, v3, :cond_3

    const-string v3, "DETACHED sim, set current as none"

    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iput v8, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->current_use_gprs_sim_id:I

    :cond_3
    iget-object v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mForceDetachAgain:[Z

    aput-boolean v5, v3, p1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "processGprsDetached():Reset as DETACHED state for simId="

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    sget-object v6, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aput-object v6, v3, p1

    :cond_4
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->selectActivePhoneProxy(I)Lcom/android/internal/telephony/PhoneProxy;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/internal/telephony/PhoneProxy;->getActivePhone()Lcom/android/internal/telephony/Phone;

    move-result-object v3

    check-cast v3, Lcom/android/internal/telephony/PhoneBase;

    invoke-virtual {v3}, Lcom/android/internal/telephony/PhoneBase;->getServiceStateTracker()Lcom/android/internal/telephony/ServiceStateTracker;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/internal/telephony/ServiceStateTracker;->getCurrentDataConnectionState()I

    move-result v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "processGprsDetached(): gprsState="

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mDetachResetMode:Z

    if-nez v3, :cond_5

    iget-boolean v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mExecuteAttachAfterDetach:Z

    if-eqz v3, :cond_6

    if-ne v0, v4, :cond_6

    :cond_5
    iput-boolean v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mExecuteAttachAfterDetach:Z

    iput-boolean v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mDetachResetMode:Z

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "processGprsDetached():ATTACHING for simId="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->request_use_gprs_sim_id:I

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ",peer_simId_poweroff_flag="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mServicePowerOffFlag:[Z

    aget-boolean v5, v5, v2

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mServicePowerOffFlag:[Z

    aget-boolean v3, v3, v2

    if-nez v3, :cond_6

    iget v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->request_use_gprs_sim_id:I

    if-eq v3, v8, :cond_6

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mGprsOpStateSync:Ljava/lang/Object;

    monitor-enter v5

    :try_start_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "processGprsDetached():mDetachResetMode is true,psOpState="

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->gprs_op_state:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    sget-object v3, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->ATTACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    iput-object v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->gprs_op_state:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    iget-object v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    iget v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->request_use_gprs_sim_id:I

    sget-object v7, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->ATTACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aput-object v7, v3, v6

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    iget v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->request_use_gprs_sim_id:I

    const/4 v5, 0x5

    invoke-virtual {p0, v3, v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->startMonitorGprsAttachTimer(II)V

    iget-object v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mGeminiPhone:Lcom/android/internal/telephony/gemini/GeminiPhone;

    iget v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->request_use_gprs_sim_id:I

    invoke-virtual {v3, v4, v5}, Lcom/android/internal/telephony/gemini/GeminiPhone;->setGprsConnType(II)V

    :cond_6
    return-void

    :catchall_1
    move-exception v3

    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v3
.end method

.method private selectActivePhoneProxy(I)Lcom/android/internal/telephony/PhoneProxy;
    .locals 2
    .param p1    # I

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mPhoneProxy1:Lcom/android/internal/telephony/Phone;

    check-cast v0, Lcom/android/internal/telephony/PhoneProxy;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mPhoneProxy2:Lcom/android/internal/telephony/Phone;

    check-cast v0, Lcom/android/internal/telephony/PhoneProxy;

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SAPP:Invaild simId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logw(Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setDataConnectionToSetting(I)V
    .locals 5
    .param p1    # I

    iget-object v1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mContext:Landroid/content/Context;

    invoke-static {v1, p1}, Landroid/provider/Telephony$SIMInfo;->getSIMInfoBySlot(Landroid/content/Context;I)Landroid/provider/Telephony$SIMInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "gprs_connection_sim_setting"

    iget-wide v3, v0, Landroid/provider/Telephony$SIMInfo;->mSimId:J

    invoke-static {v1, v2, v3, v4}, Landroid/provider/Settings$System;->putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Set data connection setting to simSlot:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " simID:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, v0, Landroid/provider/Telephony$SIMInfo;->mSimId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private updateActivePhoneProxy()V
    .locals 4

    const/4 v3, 0x0

    const/4 v1, -0x1

    const/4 v2, 0x1

    iget v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->current_use_gprs_sim_id:I

    if-eq v0, v1, :cond_2

    iget v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->current_use_gprs_sim_id:I

    if-nez v0, :cond_0

    const-string v0, "UAPP_C1"

    invoke-static {v0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mPhoneProxy1:Lcom/android/internal/telephony/Phone;

    iput-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mActivePhoneProxy:Lcom/android/internal/telephony/Phone;

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->current_use_gprs_sim_id:I

    if-ne v0, v2, :cond_1

    const-string v0, "UAPP_C2"

    invoke-static {v0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mPhoneProxy2:Lcom/android/internal/telephony/Phone;

    iput-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mActivePhoneProxy:Lcom/android/internal/telephony/Phone;

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UAPP_C3:Invaild simId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->current_use_gprs_sim_id:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logw(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mPhoneProxy1:Lcom/android/internal/telephony/Phone;

    iput-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mActivePhoneProxy:Lcom/android/internal/telephony/Phone;

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->request_use_gprs_sim_id:I

    if-eq v0, v1, :cond_5

    iget v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->request_use_gprs_sim_id:I

    if-nez v0, :cond_3

    const-string v0, "UAPP_C4"

    invoke-static {v0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mPhoneProxy1:Lcom/android/internal/telephony/Phone;

    iput-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mActivePhoneProxy:Lcom/android/internal/telephony/Phone;

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->request_use_gprs_sim_id:I

    if-ne v0, v2, :cond_4

    const-string v0, "UAPP_C5"

    invoke-static {v0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mPhoneProxy2:Lcom/android/internal/telephony/Phone;

    iput-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mActivePhoneProxy:Lcom/android/internal/telephony/Phone;

    goto :goto_0

    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UAPP_C6:Invaild simId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->request_use_gprs_sim_id:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logw(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mPhoneProxy1:Lcom/android/internal/telephony/Phone;

    iput-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mActivePhoneProxy:Lcom/android/internal/telephony/Phone;

    goto :goto_0

    :cond_5
    iget v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->previous_current_use_gprs_sim_id:I

    if-eq v0, v1, :cond_7

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UAPP_C6-0:previous_current_use_gprs_sim_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->previous_current_use_gprs_sim_id:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iget v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->previous_current_use_gprs_sim_id:I

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mPhoneProxy1:Lcom/android/internal/telephony/Phone;

    :goto_1
    iput-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mActivePhoneProxy:Lcom/android/internal/telephony/Phone;

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mPhoneProxy2:Lcom/android/internal/telephony/Phone;

    goto :goto_1

    :cond_7
    iget v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->previous_request_use_gprs_sim_id:I

    if-eq v0, v1, :cond_9

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UAPP_C6-1:previous_request_use_gprs_sim_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->previous_request_use_gprs_sim_id:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iget v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->previous_request_use_gprs_sim_id:I

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mPhoneProxy1:Lcom/android/internal/telephony/Phone;

    :goto_2
    iput-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mActivePhoneProxy:Lcom/android/internal/telephony/Phone;

    goto/16 :goto_0

    :cond_8
    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mPhoneProxy2:Lcom/android/internal/telephony/Phone;

    goto :goto_2

    :cond_9
    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mServicePowerOffFlag:[Z

    aget-boolean v0, v0, v3

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mServicePowerOffFlag:[Z

    aget-boolean v0, v0, v2

    if-eqz v0, :cond_a

    const-string v0, "UAPP_C6-2"

    invoke-static {v0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mPhoneProxy1:Lcom/android/internal/telephony/Phone;

    iput-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mActivePhoneProxy:Lcom/android/internal/telephony/Phone;

    goto/16 :goto_0

    :cond_a
    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mServicePowerOffFlag:[Z

    aget-boolean v0, v0, v3

    if-eqz v0, :cond_b

    const-string v0, "UAPP_C6-3"

    invoke-static {v0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mPhoneProxy2:Lcom/android/internal/telephony/Phone;

    iput-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mActivePhoneProxy:Lcom/android/internal/telephony/Phone;

    goto/16 :goto_0

    :cond_b
    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mServicePowerOffFlag:[Z

    aget-boolean v0, v0, v2

    if-eqz v0, :cond_c

    const-string v0, "UAPP_C6-4"

    invoke-static {v0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mPhoneProxy1:Lcom/android/internal/telephony/Phone;

    iput-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mActivePhoneProxy:Lcom/android/internal/telephony/Phone;

    goto/16 :goto_0

    :cond_c
    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aget-object v0, v0, v3

    sget-object v1, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v0, v1, :cond_e

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aget-object v0, v0, v2

    sget-object v1, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v0, v1, :cond_e

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mGeminiPhone:Lcom/android/internal/telephony/gemini/GeminiPhone;

    invoke-virtual {v0, v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->isSimInsert(I)Z

    move-result v0

    if-eqz v0, :cond_d

    const-string v0, "UAPP_C7"

    invoke-static {v0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mPhoneProxy1:Lcom/android/internal/telephony/Phone;

    iput-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mActivePhoneProxy:Lcom/android/internal/telephony/Phone;

    goto/16 :goto_0

    :cond_d
    const-string v0, "UAPP_C8"

    invoke-static {v0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mPhoneProxy2:Lcom/android/internal/telephony/Phone;

    iput-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mActivePhoneProxy:Lcom/android/internal/telephony/Phone;

    goto/16 :goto_0

    :cond_e
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UAPP_C9:Invaild simId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->request_use_gprs_sim_id:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logw(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mActivePhoneProxy:Lcom/android/internal/telephony/Phone;

    goto/16 :goto_0
.end method


# virtual methods
.method public disableApnType(Ljava/lang/String;)I
    .locals 4
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x3

    const/4 v1, -0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "disableApnType():type="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",call Gemini version"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->disableDataConnectivityDecideSimId()I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DAT:Select simId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    invoke-virtual {p0, p1, v1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->disableApnTypeGemini(Ljava/lang/String;I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public declared-synchronized disableApnTypeGemini(Ljava/lang/String;I)I
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v3, 0x1

    const/4 v6, -0x1

    const/4 v2, 0x3

    monitor-enter p0

    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "disableApnTypeGemini():type="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ",simId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ",powerOff="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mServicePowerOffFlag:[Z

    aget-boolean v5, v5, p2

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->getDataConnectionFromSetting()I

    move-result v0

    if-eq p2, v6, :cond_0

    if-eqz p2, :cond_2

    if-eq p2, v3, :cond_2

    :cond_0
    const-string v3, "DATG:SIM_NONE or Invalid"

    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logw(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    :goto_0
    monitor-exit p0

    return v2

    :cond_2
    :try_start_1
    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mGeminiPhone:Lcom/android/internal/telephony/gemini/GeminiPhone;

    invoke-virtual {v4, p2}, Lcom/android/internal/telephony/gemini/GeminiPhone;->isSimInsert(I)Z

    move-result v4

    if-nez v4, :cond_3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "simId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "is not inserted"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logw(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    :cond_3
    :try_start_2
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->isValidApnType(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aget-object v4, v4, p2

    sget-object v5, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->ATTACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v4, v5, :cond_4

    const-string v2, "Invoke proxy disableApnType()"

    invoke-static {v2}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->updateActivePhoneProxy()V

    iget-object v2, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mActivePhoneProxy:Lcom/android/internal/telephony/Phone;

    invoke-interface {v2, p1}, Lcom/android/internal/telephony/Phone;->disableApnType(Ljava/lang/String;)I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invoke proxy disableApnType2()"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invoke proxy disableApnType3()"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    move v2, v1

    goto :goto_0

    :cond_4
    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aget-object v4, v4, p2

    sget-object v5, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->ATTACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-eq v4, v5, :cond_5

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aget-object v4, v4, p2

    sget-object v5, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->REACTIVE:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v4, v5, :cond_8

    :cond_5
    const-string v3, "Want to disable apn while attaching is ongoing"

    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mEnableApnTypeReq:[Ljava/lang/String;

    aget-object v3, v3, p2

    const-string v4, "NONE"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Attaching is due to enable apn type="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mEnableApnTypeReq:[Ljava/lang/String;

    aget-object v3, v3, p2

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    const-string v3, "Attaching is enable then disable with same apn type"

    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "disableApnTypeGemini():reset mEnableApnType as NONE for simId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mEnableApnTypeReq:[Ljava/lang/String;

    const-string v4, "NONE"

    aput-object v4, v3, p2

    goto/16 :goto_0

    :cond_6
    const-string v3, "Attaching:Not allow to have already enabled apn which can be disabled!-C1"

    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    const-string v3, "Attaching:Not allow to have already enabled apn which can be disabled!-C2"

    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    const-string v4, "Already in Detaching or Detached state"

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "disableApnTypeGemini(): selectActivePhoneProxy("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ").disableApnType("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") in GprsState.DETACHING or GprsState.DETACHED"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    invoke-direct {p0, p2}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->selectActivePhoneProxy(I)Lcom/android/internal/telephony/PhoneProxy;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/android/internal/telephony/PhoneProxy;->disableApnType(Ljava/lang/String;)I

    if-ne v2, v1, :cond_9

    const-string/jumbo v2, "mms"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    if-eq p2, v6, :cond_9

    invoke-direct {p0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->getDataConnectionFromSetting()I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_9
    move v2, v3

    goto/16 :goto_0
.end method

.method public disableDataConnectivity()Z
    .locals 6

    const/4 v1, 0x0

    const/4 v4, 0x2

    new-array v0, v4, [Ljava/lang/String;

    const-string v4, "AT+CGATT=0"

    aput-object v4, v0, v1

    const/4 v4, 0x1

    const-string v5, ""

    aput-object v5, v0, v4

    invoke-direct {p0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->disableDataConnectivityDecideSimId()I

    move-result v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DDC:Select simId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->disableDataConnectivityGemini(I)I

    move-result v2

    const/4 v1, 0x0

    const/4 v4, 0x6

    if-eq v2, v4, :cond_0

    const/4 v4, 0x7

    if-ne v2, v4, :cond_2

    :cond_0
    const/4 v1, 0x1

    :cond_1
    :goto_0
    return v1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public disableDataConnectivityGemini(I)I
    .locals 5
    .param p1    # I

    const/4 v2, 0x1

    const-string v1, "disableDataConnectivityGemini:"

    invoke-direct {p0, v1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->dumpSimsGprsState(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mServicePowerOffFlag:[Z

    aget-boolean v1, v1, p1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "disableDataConnectivityGemini():radio off on simId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    const/4 v1, 0x7

    :goto_0
    return v1

    :cond_0
    const/4 v1, -0x1

    if-eq p1, v1, :cond_1

    if-eqz p1, :cond_2

    if-eq p1, v2, :cond_2

    :cond_1
    const-string v1, "DDCG:SIM_NONE or Invalid"

    invoke-static {v1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logw(Ljava/lang/String;)V

    const/4 v1, 0x5

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mRequestedGprsStateSync:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "disableDataConnectivityGemini:req_ps_state1="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->requested_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ",req_ps_state2="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->requested_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    const/4 v4, 0x1

    aget-object v3, v3, v4

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->requested_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    sget-object v3, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aput-object v3, v1, p1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "disableDataConnectivityGemini:updated req_ps_state1="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->requested_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ",req_ps_state2="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->requested_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    const/4 v4, 0x1

    aget-object v3, v3, v4

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mGprsOpStateSync:Ljava/lang/Object;

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->gprs_op_state:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    sget-object v4, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->NONE:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    if-ne v1, v4, :cond_3

    const-string v1, "[C4]Kicking off an op and check states"

    invoke-static {v1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    sget-object v1, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->KICKING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    iput-object v1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->gprs_op_state:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    iget-object v1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mGeminiDataRspHander:Landroid/os/Handler;

    const/16 v4, 0x14

    invoke-virtual {v1, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iput p1, v0, Landroid/os/Message;->arg2:I

    iget-object v1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mGeminiDataRspHander:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :goto_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    const/4 v1, 0x6

    goto/16 :goto_0

    :cond_3
    :try_start_3
    iget-object v1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->gprs_op_state:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    sget-object v4, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->ATTACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    if-ne v1, v4, :cond_4

    iget-object v1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aget-object v1, v1, p1

    sget-object v4, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->ATTACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v1, v4, :cond_4

    const-string v1, "disableDataConnectivityGemin():current disable sim is attaching, set when needed to abort attaching"

    invoke-static {v1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->stopMonitorGprsAttachTimer(I)V

    iget-object v1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->switch_detach_due_to_monitor_gprs_attach_timeout:[Z

    const/4 v4, 0x0

    aput-boolean v4, v1, p1

    iget-object v1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    sget-object v4, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aput-object v4, v1, p1

    iget-object v1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mForceDetachAgain:[Z

    const/4 v4, 0x1

    aput-boolean v4, v1, p1

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mExecuteAttachAfterDetach:Z

    const/4 v1, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0, p1, v1, v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->setThisSimAbortGprsAttachByDetach(IZI)V

    const/4 v1, 0x4

    invoke-virtual {p0, p1, v1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->startMonitorGprsDetachTimer(II)V

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v1

    :catchall_1
    move-exception v1

    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v1

    :cond_4
    :try_start_5
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "disableDataConnectivityGemin():Fail to kick,psOpState="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->gprs_op_state:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1
.end method

.method public disableDnsCheck(Z)V
    .locals 1
    .param p1    # Z

    invoke-direct {p0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->updateActivePhoneProxy()V

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mActivePhoneProxy:Lcom/android/internal/telephony/Phone;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->disableDnsCheck(Z)V

    return-void
.end method

.method public disableDnsCheckGemini(ZI)V
    .locals 1
    .param p1    # Z
    .param p2    # I

    invoke-direct {p0, p2}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->selectActivePhoneProxy(I)Lcom/android/internal/telephony/PhoneProxy;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/PhoneProxy;->disableDnsCheck(Z)V

    return-void
.end method

.method public enableApnType(Ljava/lang/String;)I
    .locals 4
    .param p1    # Ljava/lang/String;

    const/16 v1, 0x63

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "enableApnType():type="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",call Gemini version"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->enableDataConnectivityDecideSimId()I

    move-result v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "EAT:Select simId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_2

    :cond_0
    const-string v1, "Do not update the data setting"

    invoke-static {v1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    invoke-virtual {p0, p1, v0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->enableApnTypeGemini(Ljava/lang/String;I)I

    move-result v1

    :cond_1
    :goto_0
    return v1

    :cond_2
    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    goto :goto_0
.end method

.method public declared-synchronized enableApnTypeGemini(Ljava/lang/String;I)I
    .locals 12
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v4, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x3

    monitor-enter p0

    const/4 v5, 0x2

    const/4 v9, -0x1

    if-eq p2, v9, :cond_0

    if-eqz p2, :cond_2

    if-eq p2, v8, :cond_2

    :cond_0
    :try_start_0
    const-string v8, "EATG:SIM_NONE or Invalid"

    invoke-static {v8}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logw(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    :goto_0
    monitor-exit p0

    return v7

    :cond_2
    :try_start_1
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "gemini enableApnTypeGemini():type="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ",simId="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ",powerOff="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mServicePowerOffFlag:[Z

    aget-boolean v10, v10, p2

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    if-nez p2, :cond_3

    move v4, v8

    :cond_3
    const-string v9, "default"

    invoke-virtual {v9, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_5

    invoke-direct {p0, v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->selectActivePhoneProxy(I)Lcom/android/internal/telephony/PhoneProxy;

    move-result-object v9

    invoke-virtual {v9}, Lcom/android/internal/telephony/PhoneProxy;->getActiveApnTypes()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    array-length v9, v0

    if-eqz v9, :cond_5

    const/4 v3, 0x0

    :goto_1
    array-length v9, v0

    if-ge v3, v9, :cond_5

    const-string v9, "default"

    aget-object v10, v0, v3

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_4

    invoke-direct {p0, v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->selectActivePhoneProxy(I)Lcom/android/internal/telephony/PhoneProxy;

    move-result-object v9

    aget-object v10, v0, v3

    invoke-virtual {v9, v10}, Lcom/android/internal/telephony/PhoneProxy;->getDataConnectionState(Ljava/lang/String;)Lcom/android/internal/telephony/Phone$DataState;

    move-result-object v9

    sget-object v10, Lcom/android/internal/telephony/Phone$DataState;->DISCONNECTED:Lcom/android/internal/telephony/Phone$DataState;

    if-eq v9, v10, :cond_4

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "enableApnTypeGemini():Peer SIM still have non default active APN type: activeApnTypes["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    aget-object v9, v0, v3

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v7

    monitor-exit p0

    throw v7

    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_5
    :try_start_2
    iget-object v9, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mServicePowerOffFlag:[Z

    aget-boolean v9, v9, p2

    if-eqz v9, :cond_6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "enableApnTypeGemini():radio off on simId="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    const/16 v7, 0x62

    goto/16 :goto_0

    :cond_6
    if-nez p2, :cond_7

    const-string v9, "gsm.sim.state"

    invoke-static {v9}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    :goto_2
    const-string v9, "READY"

    invoke-virtual {v6, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_8

    const-string v8, "The SIM state is not READY"

    invoke-static {v8}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    const-string v9, "gsm.sim.state.2"

    invoke-static {v9}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto :goto_2

    :cond_8
    iget-object v9, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mGeminiPhone:Lcom/android/internal/telephony/gemini/GeminiPhone;

    invoke-virtual {v9, p2}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getServiceStateGemini(I)Landroid/telephony/ServiceState;

    move-result-object v9

    invoke-virtual {v9}, Landroid/telephony/ServiceState;->getState()I

    move-result v9

    if-eqz v9, :cond_9

    const-string v9, "default"

    invoke-virtual {p1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_9

    const-string v8, "The service state is not in service"

    invoke-static {v8}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_9
    iget-object v9, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mGeminiPhone:Lcom/android/internal/telephony/gemini/GeminiPhone;

    invoke-virtual {v9, p2}, Lcom/android/internal/telephony/gemini/GeminiPhone;->isSimInsert(I)Z

    move-result v9

    if-nez v9, :cond_a

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "simId="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "is not inserted"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logw(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_a
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->isValidApnType(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "gemini Before enableApnType:It must detach for peerSimId="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ",then attach for simId="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->getDataConnectionFromSetting()I

    move-result v1

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "gemini enableApnTypeGemini():currentDataConnectionSimId is  "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " and current_use_gprs_sim_id is "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->current_use_gprs_sim_id:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iget-object v9, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mDisableGprsStateSync:Ljava/lang/Object;

    monitor-enter v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-virtual {p0, v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->disableDataConnectivityGemini(I)I

    invoke-virtual {p0, p2}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->enableDataConnectivityGemini(I)I

    move-result v5

    monitor-exit v9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    const/4 v2, 0x0

    if-eq v5, v8, :cond_b

    const/4 v9, 0x2

    if-ne v5, v9, :cond_f

    :cond_b
    const/4 v2, 0x1

    :goto_3
    :try_start_4
    iget-object v9, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->keep_gprs_attach_mode:[Z

    const/4 v10, 0x1

    const/4 v11, 0x0

    aput-boolean v11, v9, v10

    iget-object v9, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->keep_gprs_attach_mode:[Z

    const/4 v10, 0x0

    const/4 v11, 0x0

    aput-boolean v11, v9, v10

    if-eqz v2, :cond_11

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mEnableApnTypeReq:[Ljava/lang/String;

    aget-object v7, v7, p2

    if-eqz v7, :cond_c

    const-string v7, "NONE"

    iget-object v9, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mEnableApnTypeReq:[Ljava/lang/String;

    aget-object v9, v9, p2

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_c

    const-string v7, "default"

    iget-object v9, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mEnableApnTypeReq:[Ljava/lang/String;

    aget-object v9, v9, p2

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_c

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mEnableApnTypeReq:[Ljava/lang/String;

    aget-object v7, v7, p2

    invoke-virtual {v7, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_c

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "enableApnTypeGemini():notify pdp fail "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v9, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mEnableApnTypeReq:[Ljava/lang/String;

    aget-object v9, v9, p2

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    invoke-direct {p0, p2}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->selectActivePhoneProxy(I)Lcom/android/internal/telephony/PhoneProxy;

    move-result-object v7

    const-string v9, "apnFailed"

    iget-object v10, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mEnableApnTypeReq:[Ljava/lang/String;

    aget-object v10, v10, p2

    invoke-virtual {v7, v9, v10}, Lcom/android/internal/telephony/PhoneProxy;->notifyDataConnection(Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mEnableApnTypeReq:[Ljava/lang/String;

    aput-object p1, v7, p2

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aget-object v7, v7, p2

    sget-object v9, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->ATTACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v7, v9, :cond_10

    const-string/jumbo v7, "proxy enableApnType() is called due to already attached"

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    const-string/jumbo v7, "mms"

    invoke-virtual {v7, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_e

    if-eq v1, p2, :cond_e

    if-eqz p2, :cond_d

    if-ne p2, v8, :cond_e

    :cond_d
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->keep_gprs_attach_mode:[Z

    const/4 v8, 0x1

    aput-boolean v8, v7, p2

    :cond_e
    const/4 v7, 0x3

    invoke-direct {p0, v7, p2}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->enableApnAfterGprsAttached(II)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v7

    goto/16 :goto_0

    :catchall_1
    move-exception v7

    :try_start_5
    monitor-exit v9
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    throw v7

    :cond_f
    const/4 v2, 0x0

    goto/16 :goto_3

    :cond_10
    const-string v7, "enable Apn Type is ongoing"

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    move v7, v8

    goto/16 :goto_0

    :cond_11
    const-string v8, "Can\'t enable:Previous Disable APN is ongoing"

    invoke-static {v8}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_0
.end method

.method public enableDataConnectivity()Z
    .locals 8

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    new-array v0, v7, [Ljava/lang/String;

    const-string v4, "AT+CGATT=1"

    aput-object v4, v0, v1

    const-string v4, ""

    aput-object v4, v0, v6

    invoke-direct {p0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->enableDataConnectivityDecideSimId()I

    move-result v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "EDC:Select simId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    invoke-direct {p0, v3}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->setDataConnectionToSetting(I)V

    invoke-virtual {p0, v3}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->enableDataConnectivityGemini(I)I

    move-result v2

    const/4 v1, 0x0

    if-eq v2, v6, :cond_0

    if-ne v2, v7, :cond_2

    :cond_0
    const/4 v1, 0x1

    :cond_1
    :goto_0
    return v1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public enableDataConnectivityGemini(I)I
    .locals 6
    .param p1    # I

    const/4 v2, 0x0

    const/4 v1, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "enableDataConnectivityGemini via GPRS Attach:simId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",gprsState="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aget-object v4, v4, p1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",powerOff="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mServicePowerOffFlag:[Z

    aget-boolean v4, v4, p1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mServicePowerOffFlag:[Z

    aget-boolean v3, v3, p1

    if-eqz v3, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "enableDataConnectivityGemini():radio off on simId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    const/4 v1, 0x4

    :goto_0
    return v1

    :cond_0
    const/4 v3, -0x1

    if-eq p1, v3, :cond_1

    if-eqz p1, :cond_2

    if-eq p1, v1, :cond_2

    :cond_1
    const-string v1, "EDCG:SIM_NONE or Invalid"

    invoke-static {v1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logw(Ljava/lang/String;)V

    move v1, v2

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mRequestedGprsStateSync:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "enableDataConnectivityGemini:req_ps_state1="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->requested_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",req_ps_state2="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->requested_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->requested_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    sget-object v4, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->ATTACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aput-object v4, v3, p1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "enableDataConnectivityGemini:updated req_ps_state1="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->requested_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",req_ps_state2="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->requested_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mGprsOpStateSync:Ljava/lang/Object;

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->gprs_op_state:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    sget-object v5, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->NONE:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    if-eq v4, v5, :cond_3

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->gprs_op_state:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    sget-object v5, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->ATTACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    if-ne v4, v5, :cond_4

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aget-object v4, v4, p1

    sget-object v5, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v4, v5, :cond_4

    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[C3]Kicking off an op and check states, op_state: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->gprs_op_state:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    sget-object v4, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->KICKING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    iput-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->gprs_op_state:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mGeminiDataRspHander:Landroid/os/Handler;

    const/16 v5, 0x14

    invoke-virtual {v4, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iput p1, v0, Landroid/os/Message;->arg2:I

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mGeminiDataRspHander:Landroid/os/Handler;

    invoke-virtual {v4, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :goto_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    monitor-exit v2

    goto/16 :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    :cond_4
    :try_start_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "enableDataConnectivityGemin():Fail to kick,psOpState="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->gprs_op_state:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    goto :goto_1

    :catchall_1
    move-exception v1

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method public getActiveApn()Ljava/lang/String;
    .locals 1

    const-string v0, "[DT] getActiveApn"

    invoke-static {v0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->updateActivePhoneProxy()V

    const-string v0, ""

    return-object v0
.end method

.method public getActiveApnGemini(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    const-string v0, ""

    return-object v0
.end method

.method public getActiveApnType()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->updateActivePhoneProxy()V

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mActivePhoneProxy:Lcom/android/internal/telephony/Phone;

    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getActiveApnType()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getActiveApnTypeGemini(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->selectActivePhoneProxy(I)Lcom/android/internal/telephony/PhoneProxy;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneProxy;->getActiveApnType()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getActiveApnTypes()[Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->updateActivePhoneProxy()V

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mActivePhoneProxy:Lcom/android/internal/telephony/Phone;

    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getActiveApnTypes()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getActiveApnTypesGemini(I)[Ljava/lang/String;
    .locals 1
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->selectActivePhoneProxy(I)Lcom/android/internal/telephony/PhoneProxy;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneProxy;->getActiveApnTypes()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getApnForType(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->updateActivePhoneProxy()V

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mActivePhoneProxy:Lcom/android/internal/telephony/Phone;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->getApnForType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getApnForTypeGemini(Ljava/lang/String;I)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-direct {p0, p2}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->selectActivePhoneProxy(I)Lcom/android/internal/telephony/PhoneProxy;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneProxy;->getActiveApnType()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentDataConnectionList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/DataConnection;",
            ">;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->updateActivePhoneProxy()V

    const/4 v0, 0x0

    return-object v0
.end method

.method public getCurrentDataConnectionListGemini(I)Ljava/util/List;
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/DataConnection;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public getDataActivityState()Lcom/android/internal/telephony/Phone$DataActivityState;
    .locals 1

    invoke-direct {p0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->updateActivePhoneProxy()V

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mActivePhoneProxy:Lcom/android/internal/telephony/Phone;

    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getDataActivityState()Lcom/android/internal/telephony/Phone$DataActivityState;

    move-result-object v0

    return-object v0
.end method

.method public getDataActivityStateGemini(I)Lcom/android/internal/telephony/Phone$DataActivityState;
    .locals 1
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->selectActivePhoneProxy(I)Lcom/android/internal/telephony/PhoneProxy;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneProxy;->getDataActivityState()Lcom/android/internal/telephony/Phone$DataActivityState;

    move-result-object v0

    return-object v0
.end method

.method public getDataCallList(Landroid/os/Message;)V
    .locals 1
    .param p1    # Landroid/os/Message;

    const-string v0, "[DT] getDataCallList"

    invoke-static {v0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->updateActivePhoneProxy()V

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mActivePhoneProxy:Lcom/android/internal/telephony/Phone;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->getDataCallList(Landroid/os/Message;)V

    return-void
.end method

.method public getDataCallListGemini(Landroid/os/Message;I)V
    .locals 1
    .param p1    # Landroid/os/Message;
    .param p2    # I

    invoke-direct {p0, p2}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->selectActivePhoneProxy(I)Lcom/android/internal/telephony/PhoneProxy;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/PhoneProxy;->getDataCallList(Landroid/os/Message;)V

    return-void
.end method

.method public getDataConnectionState()Lcom/android/internal/telephony/Phone$DataState;
    .locals 1

    invoke-direct {p0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->updateActivePhoneProxy()V

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mActivePhoneProxy:Lcom/android/internal/telephony/Phone;

    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getDataConnectionState()Lcom/android/internal/telephony/Phone$DataState;

    move-result-object v0

    return-object v0
.end method

.method public getDataConnectionState(Ljava/lang/String;)Lcom/android/internal/telephony/Phone$DataState;
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->updateActivePhoneProxy()V

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mActivePhoneProxy:Lcom/android/internal/telephony/Phone;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->getDataConnectionState(Ljava/lang/String;)Lcom/android/internal/telephony/Phone$DataState;

    move-result-object v0

    return-object v0
.end method

.method public getDataConnectionStateGemini(I)Lcom/android/internal/telephony/Phone$DataState;
    .locals 1
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->selectActivePhoneProxy(I)Lcom/android/internal/telephony/PhoneProxy;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneProxy;->getDataConnectionState()Lcom/android/internal/telephony/Phone$DataState;

    move-result-object v0

    return-object v0
.end method

.method public getDataConnectionStateGemini(Ljava/lang/String;I)Lcom/android/internal/telephony/Phone$DataState;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-direct {p0, p2}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->selectActivePhoneProxy(I)Lcom/android/internal/telephony/PhoneProxy;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/PhoneProxy;->getDataConnectionState(Ljava/lang/String;)Lcom/android/internal/telephony/Phone$DataState;

    move-result-object v0

    return-object v0
.end method

.method public getDataRoamingEnabled()Z
    .locals 1

    invoke-direct {p0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->updateActivePhoneProxy()V

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mActivePhoneProxy:Lcom/android/internal/telephony/Phone;

    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getDataRoamingEnabled()Z

    move-result v0

    return v0
.end method

.method public getDataRoamingEnabledGemini(I)Z
    .locals 1
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->selectActivePhoneProxy(I)Lcom/android/internal/telephony/PhoneProxy;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneProxy;->getDataRoamingEnabled()Z

    move-result v0

    return v0
.end method

.method public getDataStateChangedCallback()Lcom/android/internal/telephony/DefaultPhoneNotifier$IDataStateChangedCallback;
    .locals 1

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mDataStateChangedCallback:Lcom/android/internal/telephony/DefaultPhoneNotifier$IDataStateChangedCallback;

    return-object v0
.end method

.method public getDnsServers(Ljava/lang/String;)[Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->updateActivePhoneProxy()V

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mActivePhoneProxy:Lcom/android/internal/telephony/Phone;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->getDnsServers(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDnsServersGemini(Ljava/lang/String;I)[Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-direct {p0, p2}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->selectActivePhoneProxy(I)Lcom/android/internal/telephony/PhoneProxy;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/PhoneProxy;->getDnsServers(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getGateway(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->updateActivePhoneProxy()V

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mActivePhoneProxy:Lcom/android/internal/telephony/Phone;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->getGateway(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getGatewayGeminin(Ljava/lang/String;I)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-direct {p0, p2}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->selectActivePhoneProxy(I)Lcom/android/internal/telephony/PhoneProxy;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/PhoneProxy;->getGateway(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getInterfaceName(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->updateActivePhoneProxy()V

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mActivePhoneProxy:Lcom/android/internal/telephony/Phone;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->getInterfaceName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getInterfaceNameGemini(Ljava/lang/String;I)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-direct {p0, p2}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->selectActivePhoneProxy(I)Lcom/android/internal/telephony/PhoneProxy;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/PhoneProxy;->getInterfaceName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIpAddress(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->updateActivePhoneProxy()V

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mActivePhoneProxy:Lcom/android/internal/telephony/Phone;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->getIpAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIpAddressGemini(Ljava/lang/String;I)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-direct {p0, p2}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->selectActivePhoneProxy(I)Lcom/android/internal/telephony/PhoneProxy;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/PhoneProxy;->getIpAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPdpContextList(Landroid/os/Message;)V
    .locals 1
    .param p1    # Landroid/os/Message;

    const-string v0, "[DT] getPdpContextList"

    invoke-static {v0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->updateActivePhoneProxy()V

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mActivePhoneProxy:Lcom/android/internal/telephony/Phone;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->getPdpContextList(Landroid/os/Message;)V

    return-void
.end method

.method public getPdpContextListGemini(Landroid/os/Message;I)V
    .locals 1
    .param p1    # Landroid/os/Message;
    .param p2    # I

    invoke-direct {p0, p2}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->selectActivePhoneProxy(I)Lcom/android/internal/telephony/PhoneProxy;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/PhoneProxy;->getPdpContextList(Landroid/os/Message;)V

    return-void
.end method

.method public isDataConnectivityEnabled()Z
    .locals 1

    invoke-direct {p0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->updateActivePhoneProxy()V

    const/4 v0, 0x1

    return v0
.end method

.method public isDataConnectivityEnabledGemini(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    return v0
.end method

.method public isDataConnectivityPossible()Z
    .locals 1

    invoke-direct {p0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->updateActivePhoneProxy()V

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mActivePhoneProxy:Lcom/android/internal/telephony/Phone;

    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->isDataConnectivityPossible()Z

    move-result v0

    return v0
.end method

.method public isDataConnectivityPossible(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->updateActivePhoneProxy()V

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mActivePhoneProxy:Lcom/android/internal/telephony/Phone;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->isDataConnectivityPossible(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isDataConnectivityPossibleGemini(I)Z
    .locals 1
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->selectActivePhoneProxy(I)Lcom/android/internal/telephony/PhoneProxy;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneProxy;->isDataConnectivityPossible()Z

    move-result v0

    return v0
.end method

.method public isDataConnectivityPossibleGemini(Ljava/lang/String;I)Z
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-direct {p0, p2}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->selectActivePhoneProxy(I)Lcom/android/internal/telephony/PhoneProxy;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/PhoneProxy;->isDataConnectivityPossible(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isDnsCheckDisabled()Z
    .locals 1

    invoke-direct {p0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->updateActivePhoneProxy()V

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mActivePhoneProxy:Lcom/android/internal/telephony/Phone;

    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->isDnsCheckDisabled()Z

    move-result v0

    return v0
.end method

.method public isDnsCheckDisabledGemini(I)Z
    .locals 1
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->selectActivePhoneProxy(I)Lcom/android/internal/telephony/PhoneProxy;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneProxy;->isDnsCheckDisabled()Z

    move-result v0

    return v0
.end method

.method public isGprsDetachingOrDetached(I)Z
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aget-object v0, v0, p1

    sget-object v1, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aget-object v0, v0, p1

    sget-object v1, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public notifyDataActivity()V
    .locals 1

    invoke-direct {p0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->updateActivePhoneProxy()V

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mActivePhoneProxy:Lcom/android/internal/telephony/Phone;

    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->notifyDataActivity()V

    return-void
.end method

.method public notifyDataActivityGemini(I)V
    .locals 1
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->selectActivePhoneProxy(I)Lcom/android/internal/telephony/PhoneProxy;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneProxy;->notifyDataActivity()V

    return-void
.end method

.method public resetGprsRelatedContext(I)V
    .locals 10
    .param p1    # I

    const/16 v9, 0x14

    const/4 v3, 0x1

    const/4 v8, -0x1

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "resetGprsRelatedContext():simId="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ",current_use_gprs_sim_id="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->current_use_gprs_sim_id:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ",request_use_gprs_sim_id="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->request_use_gprs_sim_id:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "resetGprsRelatedContext():simId="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ",sim_gprs_state_1="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aget-object v6, v6, v4

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ",sim_gprs_state_2="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aget-object v6, v6, v3

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "resetGprsRelatedContext():simId="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ",requested_state_1="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->requested_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aget-object v6, v6, v4

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ",requested_state_2="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->requested_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aget-object v6, v6, v3

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "resetGprsRelatedContext():simId="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ",mExecuteAttachAfterDetach="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mExecuteAttachAfterDetach:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ",mDetachResetMode="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mDetachResetMode:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "resetGprsRelatedContext():simId="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ",mEnableApnTypeReq_1="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mEnableApnTypeReq:[Ljava/lang/String;

    aget-object v6, v6, v4

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ",mEnableApnTypeReq_2="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mEnableApnTypeReq:[Ljava/lang/String;

    aget-object v6, v6, v3

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mGprsOpStateSync:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "resetGprsRelatedContext():simId="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ",gprs_op_state="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->gprs_op_state:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-nez p1, :cond_0

    move v2, v3

    :goto_0
    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aget-object v5, v5, p1

    sget-object v6, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->ATTACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v5, v6, :cond_8

    const-string/jumbo v5, "resetGprsRelatedContext:ATTACHING->DETACHED"

    invoke-static {v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    sget-object v6, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aput-object v6, v5, p1

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mGprsOpStateSync:Ljava/lang/Object;

    monitor-enter v5

    :try_start_1
    sget-boolean v6, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->$assertionsDisabled:Z

    if-nez v6, :cond_1

    iget-object v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->gprs_op_state:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    sget-object v7, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->ATTACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    if-eq v6, v7, :cond_1

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    :catchall_0
    move-exception v3

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    :catchall_1
    move-exception v3

    :try_start_2
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v3

    :cond_0
    move v2, v4

    goto :goto_0

    :cond_1
    :try_start_3
    sget-object v6, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->NONE:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    iput-object v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->gprs_op_state:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->stopMonitorGprsAttachTimer(I)V

    iget v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->request_use_gprs_sim_id:I

    iput v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->previous_request_use_gprs_sim_id:I

    iput v8, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->request_use_gprs_sim_id:I

    :cond_2
    :goto_1
    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mRequestedGprsStateSync:Ljava/lang/Object;

    monitor-enter v5

    :try_start_4
    iget-object v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->requested_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    const/4 v7, 0x0

    aput-object v7, v6, p1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "resetGprsRelatedContext():simId="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ",reset requested_gprs_state as null for simId="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    monitor-exit v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "resetGprsRelatedContext():reset mEnableApnTypeReq as NONE for simId="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    const-string v5, "NONE"

    iget-object v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mEnableApnTypeReq:[Ljava/lang/String;

    aget-object v6, v6, p1

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    const-string v5, "default"

    iget-object v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mEnableApnTypeReq:[Ljava/lang/String;

    aget-object v6, v6, p1

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "resetGprsRelatedContext():notify pdp fail "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mEnableApnTypeReq:[Ljava/lang/String;

    aget-object v6, v6, p1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->selectActivePhoneProxy(I)Lcom/android/internal/telephony/PhoneProxy;

    move-result-object v5

    const-string/jumbo v6, "noSuchPdp"

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mEnableApnTypeReq:[Ljava/lang/String;

    aget-object v7, v7, p1

    invoke-virtual {v5, v6, v7}, Lcom/android/internal/telephony/PhoneProxy;->notifyDataConnection(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mEnableApnTypeReq:[Ljava/lang/String;

    const-string v6, "NONE"

    aput-object v6, v5, p1

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->switch_detach_due_to_monitor_gprs_attach_timeout:[Z

    aput-boolean v4, v5, p1

    if-eqz p1, :cond_4

    if-ne p1, v3, :cond_5

    :cond_4
    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->keep_gprs_attach_mode:[Z

    aput-boolean v4, v5, p1

    :cond_5
    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mServicePowerOffFlag:[Z

    aget-boolean v4, v5, v4

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mServicePowerOffFlag:[Z

    aget-boolean v3, v4, v3

    if-eqz v3, :cond_7

    iget-object v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mGprsOpStateSync:Ljava/lang/Object;

    monitor-enter v4

    :try_start_5
    iget-object v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->gprs_op_state:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    sget-object v5, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->KICKING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    if-ne v3, v5, :cond_6

    iget-boolean v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mExecuteAttachAfterDetach:Z

    if-eqz v3, :cond_6

    const-string/jumbo v3, "resetGprsRelatedContext():remove a delayed kick of for attach due to radio off on both sim"

    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mGeminiDataRspHander:Landroid/os/Handler;

    const/16 v5, 0x14

    invoke-virtual {v3, v5}, Landroid/os/Handler;->removeMessages(I)V

    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mExecuteAttachAfterDetach:Z

    iget-object v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mForceDetachAgain:[Z

    const/4 v5, 0x0

    aput-boolean v5, v3, p1

    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mDetachResetMode:Z

    :cond_6
    sget-object v3, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->NONE:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    iput-object v3, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->gprs_op_state:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    const-string/jumbo v3, "resetGprsRelatedContext():reset gprs_op_state = GprsOpState.NONE"

    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_5

    iput v8, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->current_use_gprs_sim_id:I

    iput v8, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->request_use_gprs_sim_id:I

    :cond_7
    return-void

    :cond_8
    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aget-object v5, v5, p1

    sget-object v6, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->ATTACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v5, v6, :cond_9

    const-string/jumbo v5, "resetGprsRelatedContext:ATTACHED->DETACHED"

    invoke-static {v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    sget-object v6, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aput-object v6, v5, p1

    iput p1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->previous_current_use_gprs_sim_id:I

    iput v8, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->current_use_gprs_sim_id:I

    goto/16 :goto_1

    :cond_9
    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aget-object v5, v5, p1

    sget-object v6, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v5, v6, :cond_e

    const-string/jumbo v5, "resetGprsRelatedContext:DETACHING->DETACHED"

    invoke-static {v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    sget-object v6, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aput-object v6, v5, p1

    if-nez p1, :cond_a

    const/4 v1, 0x2

    :goto_2
    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mGeminiDataRspHander:Landroid/os/Handler;

    invoke-virtual {v5, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mGprsOpStateSync:Ljava/lang/Object;

    monitor-enter v5

    :try_start_6
    sget-boolean v6, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->$assertionsDisabled:Z

    if-nez v6, :cond_b

    iget-object v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->gprs_op_state:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    sget-object v7, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->DETACHING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    if-eq v6, v7, :cond_b

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    :catchall_2
    move-exception v3

    monitor-exit v5
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v3

    :cond_a
    const/4 v1, 0x4

    goto :goto_2

    :cond_b
    :try_start_7
    sget-object v6, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->NONE:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    iput-object v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->gprs_op_state:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    monitor-exit v5
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->stopMonitorGprsDetachTimer(I)V

    iget v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->current_use_gprs_sim_id:I

    if-ne v5, p1, :cond_c

    iget v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->current_use_gprs_sim_id:I

    iput v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->previous_current_use_gprs_sim_id:I

    iput v8, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->current_use_gprs_sim_id:I

    :cond_c
    iget-boolean v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mExecuteAttachAfterDetach:Z

    if-eqz v5, :cond_d

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mGprsOpStateSync:Ljava/lang/Object;

    monitor-enter v5

    :try_start_8
    sget-object v6, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;->KICKING:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    iput-object v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->gprs_op_state:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsOpState;

    monitor-exit v5
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "resetGprsRelatedContext():simId="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ",kick off a delayed op to see if this is flight mode case"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mGeminiDataRspHander:Landroid/os/Handler;

    invoke-virtual {v5, v9}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iput p1, v0, Landroid/os/Message;->arg2:I

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mGeminiDataRspHander:Landroid/os/Handler;

    const-wide/16 v6, 0x1388

    invoke-virtual {v5, v0, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_1

    :catchall_3
    move-exception v3

    :try_start_9
    monitor-exit v5
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    throw v3

    :cond_d
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "resetGprsRelatedContext():simId="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ",Not request to execute attach after detach caused by radio off"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mForceDetachAgain:[Z

    aput-boolean v4, v5, p1

    iput-boolean v4, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mDetachResetMode:Z

    goto/16 :goto_1

    :cond_e
    iget-object v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->sim_gprs_state:[Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    aget-object v5, v5, p1

    sget-object v6, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;->DETACHED:Lcom/android/internal/telephony/gemini/GeminiDataSubUtil$GprsState;

    if-ne v5, v6, :cond_2

    iget v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->current_use_gprs_sim_id:I

    if-ne v5, p1, :cond_2

    iget v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->current_use_gprs_sim_id:I

    iput v5, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->previous_current_use_gprs_sim_id:I

    iput v8, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->current_use_gprs_sim_id:I

    goto/16 :goto_1

    :catchall_4
    move-exception v3

    :try_start_a
    monitor-exit v5
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    throw v3

    :catchall_5
    move-exception v3

    :try_start_b
    monitor-exit v4
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_5

    throw v3
.end method

.method public setDataRoamingEnabled(Z)V
    .locals 1
    .param p1    # Z

    invoke-direct {p0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->updateActivePhoneProxy()V

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mActivePhoneProxy:Lcom/android/internal/telephony/Phone;

    invoke-interface {v0, p1}, Lcom/android/internal/telephony/Phone;->setDataRoamingEnabled(Z)V

    return-void
.end method

.method public setDataRoamingEnabledGemini(ZI)V
    .locals 1
    .param p1    # Z
    .param p2    # I

    invoke-direct {p0, p2}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->selectActivePhoneProxy(I)Lcom/android/internal/telephony/PhoneProxy;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/PhoneProxy;->setDataRoamingEnabled(Z)V

    return-void
.end method

.method public setPeerSimAbortAttachbyDetachOrAsGprsWhenNeededMode(IZI)V
    .locals 12
    .param p1    # I
    .param p2    # Z
    .param p3    # I

    const/4 v7, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v4, -0x1

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Set peerSimId abort attach procedure or as When Needed:before attach for this actionSimId="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ",with detachPeer="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ",index="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    if-nez p1, :cond_1

    const/4 v4, 0x1

    :goto_0
    if-nez v4, :cond_2

    move v2, v7

    :goto_1
    invoke-direct {p0, v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->selectActivePhoneProxy(I)Lcom/android/internal/telephony/PhoneProxy;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/internal/telephony/PhoneProxy;->getPhoneType()I

    move-result v8

    if-ne v8, v11, :cond_4

    new-array v1, v7, [Ljava/lang/String;

    const-string v7, "AT+EGTYPE=0,1"

    aput-object v7, v1, v10

    const-string v7, ""

    aput-object v7, v1, v11

    invoke-direct {p0, v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->selectActivePhoneProxy(I)Lcom/android/internal/telephony/PhoneProxy;

    move-result-object v7

    iget-object v8, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mGeminiDataRspHander:Landroid/os/Handler;

    invoke-virtual {v8, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v8

    invoke-virtual {v7, v1, v8}, Lcom/android/internal/telephony/PhoneProxy;->invokeOemRilRequestStrings([Ljava/lang/String;Landroid/os/Message;)V

    if-nez v4, :cond_3

    const-string v3, "gprs_connection_mode_setting_sim1"

    :goto_2
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    invoke-static {v7, v3, v10}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    invoke-direct {p0, v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->selectActivePhoneProxy(I)Lcom/android/internal/telephony/PhoneProxy;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/internal/telephony/PhoneProxy;->getActivePhone()Lcom/android/internal/telephony/Phone;

    move-result-object v7

    check-cast v7, Lcom/android/internal/telephony/gsm/GSMPhone;

    invoke-virtual {v7}, Lcom/android/internal/telephony/gsm/GSMPhone;->getServiceStateTracker()Lcom/android/internal/telephony/ServiceStateTracker;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/internal/telephony/ServiceStateTracker;->removeGprsConnTypeRetry()V

    :goto_3
    invoke-direct {p0, v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->selectActivePhoneProxy(I)Lcom/android/internal/telephony/PhoneProxy;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/internal/telephony/PhoneProxy;->getActivePhone()Lcom/android/internal/telephony/Phone;

    move-result-object v5

    instance-of v7, v5, Lcom/android/internal/telephony/gsm/GSMPhone;

    if-eqz v7, :cond_0

    check-cast v5, Lcom/android/internal/telephony/gsm/GSMPhone;

    iget-object v7, v5, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    check-cast v7, Lcom/android/internal/telephony/gsm/GsmDataConnectionTracker;

    move-object v6, v7

    check-cast v6, Lcom/android/internal/telephony/gsm/GsmDataConnectionTracker;

    invoke-virtual {v6}, Lcom/android/internal/telephony/gsm/GsmDataConnectionTracker;->abortDataConnection()V

    :cond_0
    return-void

    :cond_1
    const/4 v4, 0x0

    goto :goto_0

    :cond_2
    const/4 v2, 0x4

    goto :goto_1

    :cond_3
    const-string v3, "gprs_connection_mode_setting_sim2"

    goto :goto_2

    :cond_4
    new-array v0, v7, [Ljava/lang/String;

    const-string v7, "AT+CGATT=0"

    aput-object v7, v0, v10

    const-string v7, ""

    aput-object v7, v0, v11

    if-eqz p2, :cond_5

    invoke-direct {p0, v4}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->selectActivePhoneProxy(I)Lcom/android/internal/telephony/PhoneProxy;

    move-result-object v7

    iget-object v8, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mGeminiDataRspHander:Landroid/os/Handler;

    invoke-virtual {v8, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v8

    invoke-virtual {v7, v0, v8}, Lcom/android/internal/telephony/PhoneProxy;->invokeOemRilRequestStrings([Ljava/lang/String;Landroid/os/Message;)V

    :cond_5
    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mGeminiPhone:Lcom/android/internal/telephony/gemini/GeminiPhone;

    invoke-virtual {v7, v10, v4}, Lcom/android/internal/telephony/gemini/GeminiPhone;->setGprsConnType(II)V

    goto :goto_3
.end method

.method public setThisSimAbortGprsAttachByDetach(IZI)V
    .locals 11
    .param p1    # I
    .param p2    # Z
    .param p3    # I

    const/4 v6, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Set thisSimId="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ":abort its attach procedure by detach command,with inx="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ",detachFlag="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    if-nez p1, :cond_1

    move v2, v6

    :goto_0
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->selectActivePhoneProxy(I)Lcom/android/internal/telephony/PhoneProxy;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/internal/telephony/PhoneProxy;->getPhoneType()I

    move-result v7

    if-ne v7, v10, :cond_3

    new-array v1, v6, [Ljava/lang/String;

    const-string v6, "AT+EGTYPE=0,1"

    aput-object v6, v1, v9

    const-string v6, ""

    aput-object v6, v1, v10

    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->selectActivePhoneProxy(I)Lcom/android/internal/telephony/PhoneProxy;

    move-result-object v6

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mGeminiDataRspHander:Landroid/os/Handler;

    invoke-virtual {v7, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v7

    invoke-virtual {v6, v1, v7}, Lcom/android/internal/telephony/PhoneProxy;->invokeOemRilRequestStrings([Ljava/lang/String;Landroid/os/Message;)V

    if-nez p1, :cond_2

    const-string v3, "gprs_connection_mode_setting_sim1"

    :goto_1
    iget-object v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-static {v6, v3, v9}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->selectActivePhoneProxy(I)Lcom/android/internal/telephony/PhoneProxy;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/internal/telephony/PhoneProxy;->getActivePhone()Lcom/android/internal/telephony/Phone;

    move-result-object v6

    check-cast v6, Lcom/android/internal/telephony/gsm/GSMPhone;

    invoke-virtual {v6}, Lcom/android/internal/telephony/gsm/GSMPhone;->getServiceStateTracker()Lcom/android/internal/telephony/ServiceStateTracker;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/internal/telephony/ServiceStateTracker;->removeGprsConnTypeRetry()V

    :goto_2
    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->selectActivePhoneProxy(I)Lcom/android/internal/telephony/PhoneProxy;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/internal/telephony/PhoneProxy;->getActivePhone()Lcom/android/internal/telephony/Phone;

    move-result-object v4

    instance-of v6, v4, Lcom/android/internal/telephony/gsm/GSMPhone;

    if-eqz v6, :cond_0

    check-cast v4, Lcom/android/internal/telephony/gsm/GSMPhone;

    iget-object v6, v4, Lcom/android/internal/telephony/PhoneBase;->mDataConnectionTracker:Lcom/android/internal/telephony/DataConnectionTracker;

    check-cast v6, Lcom/android/internal/telephony/gsm/GsmDataConnectionTracker;

    move-object v5, v6

    check-cast v5, Lcom/android/internal/telephony/gsm/GsmDataConnectionTracker;

    invoke-virtual {v5}, Lcom/android/internal/telephony/gsm/GsmDataConnectionTracker;->abortDataConnection()V

    :cond_0
    return-void

    :cond_1
    const/4 v2, 0x4

    goto :goto_0

    :cond_2
    const-string v3, "gprs_connection_mode_setting_sim2"

    goto :goto_1

    :cond_3
    new-array v0, v6, [Ljava/lang/String;

    const-string v6, "AT+CGATT=0"

    aput-object v6, v0, v9

    const-string v6, ""

    aput-object v6, v0, v10

    if-eqz p2, :cond_4

    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->selectActivePhoneProxy(I)Lcom/android/internal/telephony/PhoneProxy;

    move-result-object v6

    iget-object v7, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mGeminiDataRspHander:Landroid/os/Handler;

    invoke-virtual {v7, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v7

    invoke-virtual {v6, v0, v7}, Lcom/android/internal/telephony/PhoneProxy;->invokeOemRilRequestStrings([Ljava/lang/String;Landroid/os/Message;)V

    :cond_4
    iget-object v6, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mGeminiPhone:Lcom/android/internal/telephony/gemini/GeminiPhone;

    invoke-virtual {v6, v9, p1}, Lcom/android/internal/telephony/gemini/GeminiPhone;->setGprsConnType(II)V

    goto :goto_2
.end method

.method public startMonitorGprsAttachTimer(II)V
    .locals 4
    .param p1    # I
    .param p2    # I

    const-string v1, "Before starting a new one, remove old monitor grps attach event first"

    invoke-static {v1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->stopMonitorGprsAttachTimer(I)V

    iget-object v1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->enable_monitor_gprs_attach_timer:[Z

    const/4 v2, 0x1

    aput-boolean v2, v1, p1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "startMonitorGprsAttachTimer():simId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",with index="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mGeminiDataRspHander:Landroid/os/Handler;

    const/4 v2, 0x5

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mGeminiDataRspHander:Landroid/os/Handler;

    const-wide/32 v2, 0xea60

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method

.method public startMonitorGprsDetachTimer(II)V
    .locals 4
    .param p1    # I
    .param p2    # I

    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->stopMonitorGprsDetachTimer(I)V

    iget-object v1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->enable_monitor_gprs_detach_timer:[Z

    const/4 v2, 0x1

    aput-boolean v2, v1, p1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "startMonitorGprsDetachTimer():simId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",with index="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mGeminiDataRspHander:Landroid/os/Handler;

    const/4 v2, 0x6

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iput p1, v0, Landroid/os/Message;->arg1:I

    iget-object v1, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mGeminiDataRspHander:Landroid/os/Handler;

    const-wide/16 v2, 0x1388

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method

.method public stopMonitorGprsAttachTimer(I)V
    .locals 2
    .param p1    # I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "stopMonitorGprsAttachTimer():simId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->enable_monitor_gprs_attach_timer:[Z

    aget-boolean v0, v0, p1

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "stopMonitorGprsAttachTimer():simId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " successfully"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->enable_monitor_gprs_attach_timer:[Z

    const/4 v1, 0x0

    aput-boolean v1, v0, p1

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mGeminiDataRspHander:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    :cond_0
    return-void
.end method

.method public stopMonitorGprsDetachTimer(I)V
    .locals 2
    .param p1    # I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "stopMonitorGprsDetachTimer():simId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->enable_monitor_gprs_detach_timer:[Z

    aget-boolean v0, v0, p1

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "stopMonitorGprsDetachTimer():simId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " successfully"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->logd(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->enable_monitor_gprs_detach_timer:[Z

    const/4 v1, 0x0

    aput-boolean v1, v0, p1

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiDataSubUtil;->mGeminiDataRspHander:Landroid/os/Handler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    :cond_0
    return-void
.end method
