.class Lcom/android/internal/telephony/gemini/GeminiPhone$1;
.super Ljava/lang/Object;
.source "GeminiPhone.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/gemini/GeminiPhone;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/gemini/GeminiPhone;)V
    .locals 0

    iput-object p1, p0, Lcom/android/internal/telephony/gemini/GeminiPhone$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiPhone$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    invoke-static {v0}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$000(Lcom/android/internal/telephony/gemini/GeminiPhone;)Lcom/android/internal/telephony/Phone;

    move-result-object v0

    check-cast v0, Lcom/android/internal/telephony/PhoneProxy;

    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneProxy;->isGSMRadioAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiPhone$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    invoke-static {v0}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$100(Lcom/android/internal/telephony/gemini/GeminiPhone;)Lcom/android/internal/telephony/Phone;

    move-result-object v0

    check-cast v0, Lcom/android/internal/telephony/PhoneProxy;

    invoke-virtual {v0}, Lcom/android/internal/telephony/PhoneProxy;->isGSMRadioAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiPhone$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    const-string v1, "GeminiPhone initizlization to do flight mode boot up"

    invoke-static {v0, v1}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$200(Lcom/android/internal/telephony/gemini/GeminiPhone;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiPhone$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    invoke-static {v0, v2}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$302(Lcom/android/internal/telephony/gemini/GeminiPhone;I)I

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiPhone$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    invoke-static {v0, v2}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$402(Lcom/android/internal/telephony/gemini/GeminiPhone;Z)Z

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiPhone$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    invoke-static {v0, v2}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$502(Lcom/android/internal/telephony/gemini/GeminiPhone;Z)Z

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiPhone$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    invoke-static {v0}, Lcom/android/internal/telephony/gemini/GeminiNetworkSubUtil;->flightModeBootup(Lcom/android/internal/telephony/gemini/GeminiPhone;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiPhone$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    invoke-static {v0}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$304(Lcom/android/internal/telephony/gemini/GeminiPhone;)I

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiPhone$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    invoke-static {v0}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$300(Lcom/android/internal/telephony/gemini/GeminiPhone;)I

    move-result v0

    const/16 v1, 0xa

    if-ge v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiPhone$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GeminiPhone flight mode boot up but radio not available, retry "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/internal/telephony/gemini/GeminiPhone$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    invoke-static {v2}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$300(Lcom/android/internal/telephony/gemini/GeminiPhone;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$200(Lcom/android/internal/telephony/gemini/GeminiPhone;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiPhone$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    const-wide/16 v1, 0x3e8

    invoke-virtual {v0, p0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiPhone$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    const-string v1, "GeminiPhone flight mode boot up but radio not available, retry maximun, skip boot"

    invoke-static {v0, v1}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$200(Lcom/android/internal/telephony/gemini/GeminiPhone;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/internal/telephony/gemini/GeminiPhone$1;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    invoke-static {v0, v2}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$302(Lcom/android/internal/telephony/gemini/GeminiPhone;I)I

    goto :goto_0
.end method
