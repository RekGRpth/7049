.class Lcom/android/internal/telephony/gemini/GeminiPhone$2;
.super Ljava/lang/Object;
.source "GeminiPhone.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/gemini/GeminiPhone;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;


# direct methods
.method constructor <init>(Lcom/android/internal/telephony/gemini/GeminiPhone;)V
    .locals 0

    iput-object p1, p0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public declared-synchronized run()V
    .locals 51

    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    const-string v10, "getAllIccIdsDone start"

    # invokes: Lcom/android/internal/telephony/gemini/GeminiPhone;->logd(Ljava/lang/String;)V
    invoke-static {v3, v10}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$200(Lcom/android/internal/telephony/gemini/GeminiPhone;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    # getter for: Lcom/android/internal/telephony/gemini/GeminiPhone;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$600(Lcom/android/internal/telephony/gemini/GeminiPhone;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v22

    const/16 v39, 0x0

    const/16 v40, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    # getter for: Lcom/android/internal/telephony/gemini/GeminiPhone;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$600(Lcom/android/internal/telephony/gemini/GeminiPhone;)Landroid/content/Context;

    move-result-object v3

    const/4 v10, 0x0

    invoke-static {v3, v10}, Landroid/provider/Telephony$SIMInfo;->getSIMInfoBySlot(Landroid/content/Context;I)Landroid/provider/Telephony$SIMInfo;

    move-result-object v41

    if-eqz v41, :cond_9

    move-object/from16 v0, v41

    iget-object v0, v0, Landroid/provider/Telephony$SIMInfo;->mICCId:Ljava/lang/String;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "getAllIccIdsDone old IccId In Slot0 is "

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, v39

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    # invokes: Lcom/android/internal/telephony/gemini/GeminiPhone;->logd(Ljava/lang/String;)V
    invoke-static {v3, v10}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$200(Lcom/android/internal/telephony/gemini/GeminiPhone;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    # getter for: Lcom/android/internal/telephony/gemini/GeminiPhone;->mIccId1:Ljava/lang/String;
    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$700(Lcom/android/internal/telephony/gemini/GeminiPhone;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    # getter for: Lcom/android/internal/telephony/gemini/GeminiPhone;->mIccId1:Ljava/lang/String;
    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$700(Lcom/android/internal/telephony/gemini/GeminiPhone;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v39

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    new-instance v50, Landroid/content/ContentValues;

    const/4 v3, 0x1

    move-object/from16 v0, v50

    invoke-direct {v0, v3}, Landroid/content/ContentValues;-><init>(I)V

    const-string/jumbo v3, "slot"

    const/4 v10, -0x1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    move-object/from16 v0, v50

    invoke-virtual {v0, v3, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-object v3, Landroid/provider/Telephony$SimInfo;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v41

    iget-wide v13, v0, Landroid/provider/Telephony$SIMInfo;->mSimId:J

    invoke-static {v3, v13, v14}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    const/4 v10, 0x0

    const/4 v13, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, v50

    invoke-virtual {v0, v3, v1, v10, v13}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "getAllIccIdsDone reset Slot0 to -1, mIccId1 = "

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    # getter for: Lcom/android/internal/telephony/gemini/GeminiPhone;->mIccId1:Ljava/lang/String;
    invoke-static {v13}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$700(Lcom/android/internal/telephony/gemini/GeminiPhone;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v13, " "

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    # invokes: Lcom/android/internal/telephony/gemini/GeminiPhone;->logd(Ljava/lang/String;)V
    invoke-static {v3, v10}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$200(Lcom/android/internal/telephony/gemini/GeminiPhone;Ljava/lang/String;)V

    :cond_1
    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    # getter for: Lcom/android/internal/telephony/gemini/GeminiPhone;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$600(Lcom/android/internal/telephony/gemini/GeminiPhone;)Landroid/content/Context;

    move-result-object v3

    const/4 v10, 0x1

    invoke-static {v3, v10}, Landroid/provider/Telephony$SIMInfo;->getSIMInfoBySlot(Landroid/content/Context;I)Landroid/provider/Telephony$SIMInfo;

    move-result-object v42

    if-eqz v42, :cond_a

    move-object/from16 v0, v42

    iget-object v0, v0, Landroid/provider/Telephony$SIMInfo;->mICCId:Ljava/lang/String;

    move-object/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "getAllIccIdsDone old IccId In Slot1 is "

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, v40

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    # invokes: Lcom/android/internal/telephony/gemini/GeminiPhone;->logd(Ljava/lang/String;)V
    invoke-static {v3, v10}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$200(Lcom/android/internal/telephony/gemini/GeminiPhone;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    # getter for: Lcom/android/internal/telephony/gemini/GeminiPhone;->mIccId2:Ljava/lang/String;
    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$800(Lcom/android/internal/telephony/gemini/GeminiPhone;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    # getter for: Lcom/android/internal/telephony/gemini/GeminiPhone;->mIccId2:Ljava/lang/String;
    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$800(Lcom/android/internal/telephony/gemini/GeminiPhone;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v40

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    :cond_2
    new-instance v50, Landroid/content/ContentValues;

    const/4 v3, 0x1

    move-object/from16 v0, v50

    invoke-direct {v0, v3}, Landroid/content/ContentValues;-><init>(I)V

    const-string/jumbo v3, "slot"

    const/4 v10, -0x1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    move-object/from16 v0, v50

    invoke-virtual {v0, v3, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-object v3, Landroid/provider/Telephony$SimInfo;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v42

    iget-wide v13, v0, Landroid/provider/Telephony$SIMInfo;->mSimId:J

    invoke-static {v3, v13, v14}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    const/4 v10, 0x0

    const/4 v13, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, v50

    invoke-virtual {v0, v3, v1, v10, v13}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "getAllIccIdsDone reset Slot1 to -1, mIccId2 = "

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    # getter for: Lcom/android/internal/telephony/gemini/GeminiPhone;->mIccId2:Ljava/lang/String;
    invoke-static {v13}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$800(Lcom/android/internal/telephony/gemini/GeminiPhone;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v13, " "

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    # invokes: Lcom/android/internal/telephony/gemini/GeminiPhone;->logd(Ljava/lang/String;)V
    invoke-static {v3, v10}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$200(Lcom/android/internal/telephony/gemini/GeminiPhone;Ljava/lang/String;)V

    :cond_3
    :goto_1
    const/16 v32, 0x0

    const/16 v33, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    # getter for: Lcom/android/internal/telephony/gemini/GeminiPhone;->mIccId1:Ljava/lang/String;
    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$700(Lcom/android/internal/telephony/gemini/GeminiPhone;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    # getter for: Lcom/android/internal/telephony/gemini/GeminiPhone;->mIccId1:Ljava/lang/String;
    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$700(Lcom/android/internal/telephony/gemini/GeminiPhone;)Ljava/lang/String;

    move-result-object v3

    const-string v10, ""

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    const-string v10, "getAllIccIdsDone No SIM inserted in Slot 0, set the slot for Removed SIM to NONE "

    # invokes: Lcom/android/internal/telephony/gemini/GeminiPhone;->logd(Ljava/lang/String;)V
    invoke-static {v3, v10}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$200(Lcom/android/internal/telephony/gemini/GeminiPhone;Ljava/lang/String;)V

    :cond_5
    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    # getter for: Lcom/android/internal/telephony/gemini/GeminiPhone;->mIccId2:Ljava/lang/String;
    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$800(Lcom/android/internal/telephony/gemini/GeminiPhone;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    # getter for: Lcom/android/internal/telephony/gemini/GeminiPhone;->mIccId2:Ljava/lang/String;
    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$800(Lcom/android/internal/telephony/gemini/GeminiPhone;)Ljava/lang/String;

    move-result-object v3

    const-string v10, ""

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f

    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    const-string v10, "getAllIccIdsDone No SIM inserted in Slot 1, set the slot for Removed SIM to NONE "

    # invokes: Lcom/android/internal/telephony/gemini/GeminiPhone;->logd(Ljava/lang/String;)V
    invoke-static {v3, v10}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$200(Lcom/android/internal/telephony/gemini/GeminiPhone;Ljava/lang/String;)V

    :cond_7
    :goto_3
    const-wide/16 v6, -0x3

    const-wide/16 v8, -0x3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    # getter for: Lcom/android/internal/telephony/gemini/GeminiPhone;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$600(Lcom/android/internal/telephony/gemini/GeminiPhone;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/provider/Telephony$SIMInfo;->getInsertedSIMList(Landroid/content/Context;)Ljava/util/List;

    move-result-object v47

    if-nez v47, :cond_13

    const/16 v34, 0x0

    :goto_4
    const/16 v28, 0x0

    :goto_5
    move/from16 v0, v28

    move/from16 v1, v34

    if-ge v0, v1, :cond_15

    move-object/from16 v0, v47

    move/from16 v1, v28

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v49

    check-cast v49, Landroid/provider/Telephony$SIMInfo;

    move-object/from16 v0, v49

    iget v3, v0, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    if-nez v3, :cond_14

    move-object/from16 v0, v49

    iget-wide v6, v0, Landroid/provider/Telephony$SIMInfo;->mSimId:J

    :cond_8
    :goto_6
    add-int/lit8 v28, v28, 0x1

    goto :goto_5

    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    const-string v10, "getAllIccIdsDone No sim in slot0 for last time "

    # invokes: Lcom/android/internal/telephony/gemini/GeminiPhone;->logd(Ljava/lang/String;)V
    invoke-static {v3, v10}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$200(Lcom/android/internal/telephony/gemini/GeminiPhone;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    :cond_a
    :try_start_1
    const-string v3, "PHONE"

    const-string v10, "getAllIccIdsDone No sim in slot1 for last time "

    invoke-static {v3, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    # getter for: Lcom/android/internal/telephony/gemini/GeminiPhone;->mIccId1:Ljava/lang/String;
    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$700(Lcom/android/internal/telephony/gemini/GeminiPhone;)Ljava/lang/String;

    move-result-object v3

    const-string v10, "ff"

    invoke-virtual {v3, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_c

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    # getter for: Lcom/android/internal/telephony/gemini/GeminiPhone;->mIccId1:Ljava/lang/String;
    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$700(Lcom/android/internal/telephony/gemini/GeminiPhone;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    # getter for: Lcom/android/internal/telephony/gemini/GeminiPhone;->mIccId2:Ljava/lang/String;
    invoke-static {v10}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$800(Lcom/android/internal/telephony/gemini/GeminiPhone;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    :cond_c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    # getter for: Lcom/android/internal/telephony/gemini/GeminiPhone;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$600(Lcom/android/internal/telephony/gemini/GeminiPhone;)Landroid/content/Context;

    move-result-object v3

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    # getter for: Lcom/android/internal/telephony/gemini/GeminiPhone;->mIccId1:Ljava/lang/String;
    invoke-static {v13}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$700(Lcom/android/internal/telephony/gemini/GeminiPhone;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v13, "1"

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v13, 0x0

    invoke-static {v3, v10, v13}, Landroid/provider/Telephony$SIMInfo;->insertICCId(Landroid/content/Context;Ljava/lang/String;I)Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    const-string v10, "getAllIccIdsDone special SIM with invalid ICCID is inserted in slot1"

    # invokes: Lcom/android/internal/telephony/gemini/GeminiPhone;->logd(Ljava/lang/String;)V
    invoke-static {v3, v10}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$200(Lcom/android/internal/telephony/gemini/GeminiPhone;Ljava/lang/String;)V

    :cond_d
    :goto_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    # getter for: Lcom/android/internal/telephony/gemini/GeminiPhone;->mIccId1:Ljava/lang/String;
    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$700(Lcom/android/internal/telephony/gemini/GeminiPhone;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v39

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    # getter for: Lcom/android/internal/telephony/gemini/GeminiPhone;->mIccId1:Ljava/lang/String;
    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$700(Lcom/android/internal/telephony/gemini/GeminiPhone;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v40

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    add-int/lit8 v32, v32, 0x1

    or-int/lit8 v33, v33, 0x1

    goto/16 :goto_2

    :cond_e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    # getter for: Lcom/android/internal/telephony/gemini/GeminiPhone;->mIccId1:Ljava/lang/String;
    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$700(Lcom/android/internal/telephony/gemini/GeminiPhone;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v39

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_d

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    # getter for: Lcom/android/internal/telephony/gemini/GeminiPhone;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$600(Lcom/android/internal/telephony/gemini/GeminiPhone;)Landroid/content/Context;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    # getter for: Lcom/android/internal/telephony/gemini/GeminiPhone;->mIccId1:Ljava/lang/String;
    invoke-static {v10}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$700(Lcom/android/internal/telephony/gemini/GeminiPhone;)Ljava/lang/String;

    move-result-object v10

    const/4 v13, 0x0

    invoke-static {v3, v10, v13}, Landroid/provider/Telephony$SIMInfo;->insertICCId(Landroid/content/Context;Ljava/lang/String;I)Landroid/net/Uri;

    goto :goto_7

    :cond_f
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    # getter for: Lcom/android/internal/telephony/gemini/GeminiPhone;->mIccId2:Ljava/lang/String;
    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$800(Lcom/android/internal/telephony/gemini/GeminiPhone;)Ljava/lang/String;

    move-result-object v3

    const-string v10, "ff"

    invoke-virtual {v3, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_10

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    # getter for: Lcom/android/internal/telephony/gemini/GeminiPhone;->mIccId2:Ljava/lang/String;
    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$800(Lcom/android/internal/telephony/gemini/GeminiPhone;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    # getter for: Lcom/android/internal/telephony/gemini/GeminiPhone;->mIccId1:Ljava/lang/String;
    invoke-static {v10}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$700(Lcom/android/internal/telephony/gemini/GeminiPhone;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_12

    :cond_10
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    # getter for: Lcom/android/internal/telephony/gemini/GeminiPhone;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$600(Lcom/android/internal/telephony/gemini/GeminiPhone;)Landroid/content/Context;

    move-result-object v3

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    # getter for: Lcom/android/internal/telephony/gemini/GeminiPhone;->mIccId2:Ljava/lang/String;
    invoke-static {v13}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$800(Lcom/android/internal/telephony/gemini/GeminiPhone;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v13, "2"

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v13, 0x1

    invoke-static {v3, v10, v13}, Landroid/provider/Telephony$SIMInfo;->insertICCId(Landroid/content/Context;Ljava/lang/String;I)Landroid/net/Uri;

    const-string v3, "PHONE"

    const-string v10, "getAllIccIdsDone special SIM with invalid ICCID is inserted in slot2"

    invoke-static {v3, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_11
    :goto_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    # getter for: Lcom/android/internal/telephony/gemini/GeminiPhone;->mIccId2:Ljava/lang/String;
    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$800(Lcom/android/internal/telephony/gemini/GeminiPhone;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v39

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    # getter for: Lcom/android/internal/telephony/gemini/GeminiPhone;->mIccId2:Ljava/lang/String;
    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$800(Lcom/android/internal/telephony/gemini/GeminiPhone;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v40

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    add-int/lit8 v32, v32, 0x1

    or-int/lit8 v33, v33, 0x2

    goto/16 :goto_3

    :cond_12
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    # getter for: Lcom/android/internal/telephony/gemini/GeminiPhone;->mIccId2:Ljava/lang/String;
    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$800(Lcom/android/internal/telephony/gemini/GeminiPhone;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v40

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_11

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    # getter for: Lcom/android/internal/telephony/gemini/GeminiPhone;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$600(Lcom/android/internal/telephony/gemini/GeminiPhone;)Landroid/content/Context;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    # getter for: Lcom/android/internal/telephony/gemini/GeminiPhone;->mIccId2:Ljava/lang/String;
    invoke-static {v10}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$800(Lcom/android/internal/telephony/gemini/GeminiPhone;)Ljava/lang/String;

    move-result-object v10

    const/4 v13, 0x1

    invoke-static {v3, v10, v13}, Landroid/provider/Telephony$SIMInfo;->insertICCId(Landroid/content/Context;Ljava/lang/String;I)Landroid/net/Uri;

    goto :goto_8

    :cond_13
    invoke-interface/range {v47 .. v47}, Ljava/util/List;->size()I

    move-result v34

    goto/16 :goto_4

    :cond_14
    move-object/from16 v0, v49

    iget v3, v0, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    const/4 v10, 0x1

    if-ne v3, v10, :cond_8

    move-object/from16 v0, v49

    iget-wide v8, v0, Landroid/provider/Telephony$SIMInfo;->mSimId:J

    goto/16 :goto_6

    :cond_15
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "getAllIccIdsDone simIdForSlot ["

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v13, ", "

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v13, "]"

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    # invokes: Lcom/android/internal/telephony/gemini/GeminiPhone;->logd(Ljava/lang/String;)V
    invoke-static {v3, v10}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$200(Lcom/android/internal/telephony/gemini/GeminiPhone;Ljava/lang/String;)V

    if-lez v32, :cond_16

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    const-string v10, "getAllIccIdsDone New SIM detected. "

    # invokes: Lcom/android/internal/telephony/gemini/GeminiPhone;->logd(Ljava/lang/String;)V
    invoke-static {v3, v10}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$200(Lcom/android/internal/telephony/gemini/GeminiPhone;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    move-object/from16 v0, v47

    # invokes: Lcom/android/internal/telephony/gemini/GeminiPhone;->setColorForNewSIM(Ljava/util/List;)V
    invoke-static {v3, v0}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$900(Lcom/android/internal/telephony/gemini/GeminiPhone;Ljava/util/List;)V

    const-string v3, "airplane_mode_on"

    const/4 v10, 0x0

    move-object/from16 v0, v22

    invoke-static {v0, v3, v10}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v20

    if-lez v20, :cond_24

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    move-object/from16 v0, v47

    # invokes: Lcom/android/internal/telephony/gemini/GeminiPhone;->setDefaultNameForAllNewSIM(Ljava/util/List;)V
    invoke-static {v3, v0}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$1000(Lcom/android/internal/telephony/gemini/GeminiPhone;Ljava/util/List;)V

    :cond_16
    :goto_9
    const-string/jumbo v3, "video_call_sim_setting"

    const-wide/16 v13, -0x5

    move-object/from16 v0, v22

    invoke-static {v0, v3, v13, v14}, Landroid/provider/Settings$System;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v4

    const-string/jumbo v3, "voice_call_sim_setting"

    const-wide/16 v13, -0x5

    move-object/from16 v0, v22

    invoke-static {v0, v3, v13, v14}, Landroid/provider/Settings$System;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v11

    const-string/jumbo v3, "sms_sim_setting"

    const-wide/16 v13, -0x5

    move-object/from16 v0, v22

    invoke-static {v0, v3, v13, v14}, Landroid/provider/Settings$System;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v43

    const-string v3, "gprs_connection_sim_setting"

    const-wide/16 v13, -0x5

    move-object/from16 v0, v22

    invoke-static {v0, v3, v13, v14}, Landroid/provider/Settings$System;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v37

    const/16 v27, 0x0

    const-wide/16 v35, -0x5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    invoke-virtual {v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->get3GCapabilitySIM()I

    move-result v31

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    # getter for: Lcom/android/internal/telephony/gemini/GeminiPhone;->mPhone1:Lcom/android/internal/telephony/Phone;
    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$000(Lcom/android/internal/telephony/gemini/GeminiPhone;)Lcom/android/internal/telephony/Phone;

    move-result-object v3

    check-cast v3, Lcom/android/internal/telephony/PhoneProxy;

    invoke-virtual {v3}, Lcom/android/internal/telephony/PhoneProxy;->is3GSwitchEnable()Z

    move-result v3

    if-eqz v3, :cond_26

    const/4 v3, 0x1

    move/from16 v0, v31

    if-ne v0, v3, :cond_25

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    const/4 v10, 0x1

    invoke-virtual {v3, v10}, Lcom/android/internal/telephony/gemini/GeminiPhone;->isSimInsert(I)Z

    move-result v3

    if-eqz v3, :cond_25

    move-wide/from16 v35, v8

    :cond_17
    :goto_a
    const-string/jumbo v3, "video_call_sim_setting"

    move-object/from16 v0, v22

    move-wide/from16 v1, v35

    invoke-static {v0, v3, v1, v2}, Landroid/provider/Settings$System;->putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    # invokes: Lcom/android/internal/telephony/gemini/GeminiPhone;->isSIMRemoved(JJJ)Z
    invoke-static/range {v3 .. v9}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$1200(Lcom/android/internal/telephony/gemini/GeminiPhone;JJJ)Z

    move-result v3

    if-eqz v3, :cond_18

    const/16 v27, 0x1

    :cond_18
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    # getter for: Lcom/android/internal/telephony/gemini/GeminiPhone;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$600(Lcom/android/internal/telephony/gemini/GeminiPhone;)Landroid/content/Context;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    # getter for: Lcom/android/internal/telephony/gemini/GeminiPhone;->mContext:Landroid/content/Context;
    invoke-static {v10}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$600(Lcom/android/internal/telephony/gemini/GeminiPhone;)Landroid/content/Context;

    const-string v10, "connectivity"

    invoke-virtual {v3, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Landroid/net/ConnectivityManager;

    const/4 v3, 0x1

    move/from16 v0, v34

    if-le v0, v3, :cond_29

    const-wide/16 v13, -0x5

    cmp-long v3, v11, v13

    if-nez v3, :cond_19

    const-string/jumbo v3, "voice_call_sim_setting"

    const-wide/16 v13, -0x1

    move-object/from16 v0, v22

    invoke-static {v0, v3, v13, v14}, Landroid/provider/Settings$System;->putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    :cond_19
    const-wide/16 v13, -0x5

    cmp-long v3, v43, v13

    if-nez v3, :cond_1a

    const-string/jumbo v3, "sms_sim_setting"

    const-wide/16 v13, -0x1

    move-object/from16 v0, v22

    invoke-static {v0, v3, v13, v14}, Landroid/provider/Settings$System;->putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    :cond_1a
    const-wide/16 v13, -0x5

    cmp-long v3, v37, v13

    if-nez v3, :cond_1b

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    # invokes: Lcom/android/internal/telephony/gemini/GeminiPhone;->isGPRSDefaultOn()Z
    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$1300(Lcom/android/internal/telephony/gemini/GeminiPhone;)Z

    move-result v3

    if-eqz v3, :cond_28

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    # invokes: Lcom/android/internal/telephony/gemini/GeminiPhone;->is3GSwitched()Z
    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$1400(Lcom/android/internal/telephony/gemini/GeminiPhone;)Z

    move-result v3

    if-eqz v3, :cond_27

    const/4 v3, 0x1

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->setMobileDataEnabledGemini(I)Z

    :cond_1b
    :goto_b
    const-wide/16 v23, -0x5

    const/4 v3, 0x1

    move/from16 v0, v34

    if-le v0, v3, :cond_2d

    const-wide/16 v23, -0x1

    :cond_1c
    :goto_c
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    move-wide v13, v6

    move-wide v15, v8

    # invokes: Lcom/android/internal/telephony/gemini/GeminiPhone;->isSIMRemoved(JJJ)Z
    invoke-static/range {v10 .. v16}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$1200(Lcom/android/internal/telephony/gemini/GeminiPhone;JJJ)Z

    move-result v3

    if-eqz v3, :cond_1d

    const-string/jumbo v3, "voice_call_sim_setting"

    move-object/from16 v0, v22

    move-wide/from16 v1, v23

    invoke-static {v0, v3, v1, v2}, Landroid/provider/Settings$System;->putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    const/16 v27, 0x1

    :cond_1d
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    move-wide/from16 v14, v43

    move-wide/from16 v16, v6

    move-wide/from16 v18, v8

    # invokes: Lcom/android/internal/telephony/gemini/GeminiPhone;->isSIMRemoved(JJJ)Z
    invoke-static/range {v13 .. v19}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$1200(Lcom/android/internal/telephony/gemini/GeminiPhone;JJJ)Z

    move-result v3

    if-eqz v3, :cond_1e

    const-string/jumbo v3, "sms_sim_setting"

    move-object/from16 v0, v22

    move-wide/from16 v1, v23

    invoke-static {v0, v3, v1, v2}, Landroid/provider/Settings$System;->putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    const/16 v27, 0x1

    :cond_1e
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    move-wide/from16 v14, v37

    move-wide/from16 v16, v6

    move-wide/from16 v18, v8

    # invokes: Lcom/android/internal/telephony/gemini/GeminiPhone;->isSIMRemoved(JJJ)Z
    invoke-static/range {v13 .. v19}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$1200(Lcom/android/internal/telephony/gemini/GeminiPhone;JJJ)Z

    move-result v3

    if-eqz v3, :cond_20

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    # invokes: Lcom/android/internal/telephony/gemini/GeminiPhone;->isGPRSDefaultOn()Z
    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$1300(Lcom/android/internal/telephony/gemini/GeminiPhone;)Z

    move-result v3

    if-eqz v3, :cond_30

    const/4 v3, 0x1

    move/from16 v0, v34

    if-le v0, v3, :cond_2f

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    # invokes: Lcom/android/internal/telephony/gemini/GeminiPhone;->is3GSwitched()Z
    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$1400(Lcom/android/internal/telephony/gemini/GeminiPhone;)Z

    move-result v3

    if-eqz v3, :cond_2e

    const/4 v3, 0x1

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->setMobileDataEnabledGemini(I)Z

    :cond_1f
    :goto_d
    const/16 v27, 0x1

    :cond_20
    if-lez v32, :cond_31

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    const-string v10, "getAllIccIdsDone. New SIM detected. "

    # invokes: Lcom/android/internal/telephony/gemini/GeminiPhone;->logd(Ljava/lang/String;)V
    invoke-static {v3, v10}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$200(Lcom/android/internal/telephony/gemini/GeminiPhone;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    move/from16 v0, v34

    move/from16 v1, v33

    # invokes: Lcom/android/internal/telephony/gemini/GeminiPhone;->broadCastNewSIMDetected(II)V
    invoke-static {v3, v0, v1}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$1500(Lcom/android/internal/telephony/gemini/GeminiPhone;II)V

    :cond_21
    :goto_e
    const-string v3, "gprs_connection_sim_setting"

    const-wide/16 v13, -0x5

    move-object/from16 v0, v22

    invoke-static {v0, v3, v13, v14}, Landroid/provider/Settings$System;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v25

    const-wide/16 v13, -0x5

    cmp-long v3, v25, v13

    if-eqz v3, :cond_22

    const-wide/16 v13, 0x0

    cmp-long v3, v25, v13

    if-eqz v3, :cond_22

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    # getter for: Lcom/android/internal/telephony/gemini/GeminiPhone;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$600(Lcom/android/internal/telephony/gemini/GeminiPhone;)Landroid/content/Context;

    move-result-object v3

    move-wide/from16 v0, v25

    invoke-static {v3, v0, v1}, Landroid/provider/Telephony$SIMInfo;->getSlotById(Landroid/content/Context;J)I

    move-result v48

    const/4 v3, -0x1

    move/from16 v0, v48

    if-eq v0, v3, :cond_32

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    const/4 v10, 0x1

    move/from16 v0, v48

    invoke-virtual {v3, v10, v0}, Lcom/android/internal/telephony/gemini/GeminiPhone;->setGprsConnType(II)V

    move-object/from16 v0, v21

    move/from16 v1, v48

    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->setMobileDataEnabledGemini(I)Z

    :cond_22
    :goto_f
    const-string v3, "gsm.siminfo.ready"

    const-string/jumbo v10, "true"

    invoke-static {v3, v10}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "getAllIccIdsDone PROPERTY_SIM_INFO_READY after set is "

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v13, "gsm.siminfo.ready"

    const/4 v14, 0x0

    invoke-static {v13, v14}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    # invokes: Lcom/android/internal/telephony/gemini/GeminiPhone;->logd(Ljava/lang/String;)V
    invoke-static {v3, v10}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$200(Lcom/android/internal/telephony/gemini/GeminiPhone;Ljava/lang/String;)V

    new-instance v29, Landroid/content/Intent;

    const-string v3, "android.intent.action.SIM_INFO_UPDATE"

    move-object/from16 v0, v29

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    const-string v10, "broadCast intent ACTION_SIM_INFO_UPDATE"

    # invokes: Lcom/android/internal/telephony/gemini/GeminiPhone;->logd(Ljava/lang/String;)V
    invoke-static {v3, v10}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$200(Lcom/android/internal/telephony/gemini/GeminiPhone;Ljava/lang/String;)V

    const-string v3, "android.permission.READ_PHONE_STATE"

    move-object/from16 v0, v29

    invoke-static {v0, v3}, Landroid/app/ActivityManagerNative;->broadcastStickyIntent(Landroid/content/Intent;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    # getter for: Lcom/android/internal/telephony/gemini/GeminiPhone;->mIsToSwitch3G:Z
    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$1700(Lcom/android/internal/telephony/gemini/GeminiPhone;)Z

    move-result v3

    if-eqz v3, :cond_23

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    const-string v10, "getAllIccIdsDone. broadcast 3G switch done event"

    # invokes: Lcom/android/internal/telephony/gemini/GeminiPhone;->logd(Ljava/lang/String;)V
    invoke-static {v3, v10}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$200(Lcom/android/internal/telephony/gemini/GeminiPhone;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    # invokes: Lcom/android/internal/telephony/gemini/GeminiPhone;->broadcast3GSwitchDoneEvent()V
    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$1800(Lcom/android/internal/telephony/gemini/GeminiPhone;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_23
    monitor-exit p0

    return-void

    :cond_24
    :try_start_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    move-object/from16 v0, v47

    # invokes: Lcom/android/internal/telephony/gemini/GeminiPhone;->setDefaultNameIfImsiReadyOrLocked(Ljava/util/List;)V
    invoke-static {v3, v0}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$1100(Lcom/android/internal/telephony/gemini/GeminiPhone;Ljava/util/List;)V

    goto/16 :goto_9

    :cond_25
    if-nez v31, :cond_17

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    const/4 v10, 0x0

    invoke-virtual {v3, v10}, Lcom/android/internal/telephony/gemini/GeminiPhone;->isSimInsert(I)Z

    move-result v3

    if-eqz v3, :cond_17

    move-wide/from16 v35, v6

    goto/16 :goto_a

    :cond_26
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    const/4 v10, 0x0

    invoke-virtual {v3, v10}, Lcom/android/internal/telephony/gemini/GeminiPhone;->isSimInsert(I)Z

    move-result v3

    if-eqz v3, :cond_17

    move-wide/from16 v35, v6

    goto/16 :goto_a

    :cond_27
    const/4 v3, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->setMobileDataEnabledGemini(I)Z

    goto/16 :goto_b

    :cond_28
    const/4 v3, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->setMobileDataEnabled(Z)V

    goto/16 :goto_b

    :cond_29
    const/4 v3, 0x1

    move/from16 v0, v34

    if-ne v0, v3, :cond_1b

    const/4 v3, 0x0

    move-object/from16 v0, v47

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/provider/Telephony$SIMInfo;

    iget-wide v0, v3, Landroid/provider/Telephony$SIMInfo;->mSimId:J

    move-wide/from16 v45, v0

    const-string v3, "enable_internet_call_value"

    const/4 v10, 0x0

    move-object/from16 v0, v22

    invoke-static {v0, v3, v10}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v30

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    # getter for: Lcom/android/internal/telephony/gemini/GeminiPhone;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$600(Lcom/android/internal/telephony/gemini/GeminiPhone;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/net/sip/SipManager;->isVoipSupported(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_2a

    if-eqz v30, :cond_2a

    const-wide/16 v13, -0x5

    cmp-long v3, v11, v13

    if-nez v3, :cond_2b

    :cond_2a
    const-string/jumbo v3, "voice_call_sim_setting"

    move-object/from16 v0, v22

    move-wide/from16 v1, v45

    invoke-static {v0, v3, v1, v2}, Landroid/provider/Settings$System;->putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    :cond_2b
    const-string/jumbo v3, "sms_sim_setting"

    move-object/from16 v0, v22

    move-wide/from16 v1, v45

    invoke-static {v0, v3, v1, v2}, Landroid/provider/Settings$System;->putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    const-wide/16 v13, -0x5

    cmp-long v3, v37, v13

    if-nez v3, :cond_1b

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    # invokes: Lcom/android/internal/telephony/gemini/GeminiPhone;->isGPRSDefaultOn()Z
    invoke-static {v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$1300(Lcom/android/internal/telephony/gemini/GeminiPhone;)Z

    move-result v3

    if-eqz v3, :cond_2c

    const/4 v3, 0x0

    move-object/from16 v0, v47

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/provider/Telephony$SIMInfo;

    iget v3, v3, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->setMobileDataEnabledGemini(I)Z

    goto/16 :goto_b

    :cond_2c
    const/4 v3, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->setMobileDataEnabled(Z)V

    goto/16 :goto_b

    :cond_2d
    const/4 v3, 0x1

    move/from16 v0, v34

    if-ne v0, v3, :cond_1c

    const/4 v3, 0x0

    move-object/from16 v0, v47

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/provider/Telephony$SIMInfo;

    iget-wide v0, v3, Landroid/provider/Telephony$SIMInfo;->mSimId:J

    move-wide/from16 v23, v0

    goto/16 :goto_c

    :cond_2e
    const/4 v3, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->setMobileDataEnabledGemini(I)Z

    goto/16 :goto_d

    :cond_2f
    if-lez v34, :cond_1f

    const/4 v3, 0x0

    move-object/from16 v0, v47

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/provider/Telephony$SIMInfo;

    iget v3, v3, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->setMobileDataEnabledGemini(I)Z

    goto/16 :goto_d

    :cond_30
    const/4 v3, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->setMobileDataEnabled(Z)V

    goto/16 :goto_d

    :cond_31
    if-eqz v27, :cond_21

    if-lez v34, :cond_21

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    const-string v10, "getAllIccIdsDone No new SIM detected and Default SIM for some service has been removed. "

    # invokes: Lcom/android/internal/telephony/gemini/GeminiPhone;->logd(Ljava/lang/String;)V
    invoke-static {v3, v10}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$200(Lcom/android/internal/telephony/gemini/GeminiPhone;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    move/from16 v0, v34

    # invokes: Lcom/android/internal/telephony/gemini/GeminiPhone;->broadCastDefaultSIMRemoved(I)V
    invoke-static {v3, v0}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$1600(Lcom/android/internal/telephony/gemini/GeminiPhone;I)V

    goto/16 :goto_e

    :cond_32
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/internal/telephony/gemini/GeminiPhone$2;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    const-string v10, "getAllIccIdsDone: gprsDefaultSIM does not exist in slot then skip."

    # invokes: Lcom/android/internal/telephony/gemini/GeminiPhone;->logd(Ljava/lang/String;)V
    invoke-static {v3, v10}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$200(Lcom/android/internal/telephony/gemini/GeminiPhone;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_f
.end method
