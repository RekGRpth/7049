.class Lcom/android/internal/telephony/gemini/GeminiPhone$SwitchBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "GeminiPhone.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/gemini/GeminiPhone;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SwitchBroadcastReceiver"
.end annotation


# instance fields
.field private mSimId:I

.field final synthetic this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;


# direct methods
.method private constructor <init>(Lcom/android/internal/telephony/gemini/GeminiPhone;)V
    .locals 1

    iput-object p1, p0, Lcom/android/internal/telephony/gemini/GeminiPhone$SwitchBroadcastReceiver;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/internal/telephony/gemini/GeminiPhone$SwitchBroadcastReceiver;->mSimId:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/internal/telephony/gemini/GeminiPhone;Lcom/android/internal/telephony/gemini/GeminiPhone$1;)V
    .locals 0
    .param p1    # Lcom/android/internal/telephony/gemini/GeminiPhone;
    .param p2    # Lcom/android/internal/telephony/gemini/GeminiPhone$1;

    invoke-direct {p0, p1}, Lcom/android/internal/telephony/gemini/GeminiPhone$SwitchBroadcastReceiver;-><init>(Lcom/android/internal/telephony/gemini/GeminiPhone;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v8, 0x3

    const/4 v7, 0x0

    const/16 v6, 0x9

    const/4 v5, 0x1

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v2, "PHONE"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "GeminiPhone m3GSwitchReceiver receive ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v2, Lcom/android/internal/telephony/gemini/GeminiPhone;->EVENT_PRE_3G_SWITCH:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/android/internal/telephony/gemini/GeminiPhone$SwitchBroadcastReceiver;->mSimId:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    const-string v2, "PHONE"

    const-string v3, "No need to reset modem, set preferred network mode to NT_MODE_GSM_ONLY"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v2, p0, Lcom/android/internal/telephony/gemini/GeminiPhone$SwitchBroadcastReceiver;->mSimId:I

    invoke-static {v2}, Lcom/android/internal/telephony/gsm/GSMPhone;->set3GSim(I)V

    iget-object v2, p0, Lcom/android/internal/telephony/gemini/GeminiPhone$SwitchBroadcastReceiver;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    invoke-static {v2}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$600(Lcom/android/internal/telephony/gemini/GeminiPhone;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "preferred_network_mode"

    invoke-static {v2, v3, v5}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    iget-object v2, p0, Lcom/android/internal/telephony/gemini/GeminiPhone$SwitchBroadcastReceiver;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    iget-object v3, p0, Lcom/android/internal/telephony/gemini/GeminiPhone$SwitchBroadcastReceiver;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    invoke-virtual {v3, v6}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v5, v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->setPreferredNetworkType(ILandroid/os/Message;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/android/internal/telephony/gemini/GeminiPhone$SwitchBroadcastReceiver;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    invoke-static {v2}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$1400(Lcom/android/internal/telephony/gemini/GeminiPhone;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget v2, p0, Lcom/android/internal/telephony/gemini/GeminiPhone$SwitchBroadcastReceiver;->mSimId:I

    if-eq v5, v2, :cond_3

    :cond_2
    if-nez v1, :cond_5

    iget v2, p0, Lcom/android/internal/telephony/gemini/GeminiPhone$SwitchBroadcastReceiver;->mSimId:I

    if-nez v2, :cond_5

    :cond_3
    const-string v2, "PHONE"

    const-string v3, "No need to reset modem, set preferred network mode to NT_MODE_WCDMA_PREF"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v2, p0, Lcom/android/internal/telephony/gemini/GeminiPhone$SwitchBroadcastReceiver;->mSimId:I

    invoke-static {v2}, Lcom/android/internal/telephony/gsm/GSMPhone;->set3GSim(I)V

    iget-object v2, p0, Lcom/android/internal/telephony/gemini/GeminiPhone$SwitchBroadcastReceiver;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    invoke-static {v2}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$000(Lcom/android/internal/telephony/gemini/GeminiPhone;)Lcom/android/internal/telephony/Phone;

    move-result-object v2

    check-cast v2, Lcom/android/internal/telephony/PhoneProxy;

    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneProxy;->isWCDMAPrefered()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/android/internal/telephony/gemini/GeminiPhone$SwitchBroadcastReceiver;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    invoke-static {v2}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$600(Lcom/android/internal/telephony/gemini/GeminiPhone;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "preferred_network_mode"

    invoke-static {v2, v3, v7}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    iget-object v2, p0, Lcom/android/internal/telephony/gemini/GeminiPhone$SwitchBroadcastReceiver;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    iget-object v3, p0, Lcom/android/internal/telephony/gemini/GeminiPhone$SwitchBroadcastReceiver;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    invoke-virtual {v3, v6}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v7, v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->setPreferredNetworkType(ILandroid/os/Message;)V

    goto :goto_0

    :cond_4
    const-string v2, "PHONE"

    const-string v3, "SwitchBroadcastReceiver:setPreferredNetworkType  networkType=NT_MODE_GSM_UMTS"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/internal/telephony/gemini/GeminiPhone$SwitchBroadcastReceiver;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    invoke-static {v2}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$600(Lcom/android/internal/telephony/gemini/GeminiPhone;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "preferred_network_mode"

    invoke-static {v2, v3, v8}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    iget-object v2, p0, Lcom/android/internal/telephony/gemini/GeminiPhone$SwitchBroadcastReceiver;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    iget-object v3, p0, Lcom/android/internal/telephony/gemini/GeminiPhone$SwitchBroadcastReceiver;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    invoke-virtual {v3, v6}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v8, v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->setPreferredNetworkType(ILandroid/os/Message;)V

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lcom/android/internal/telephony/gemini/GeminiPhone$SwitchBroadcastReceiver;->this$0:Lcom/android/internal/telephony/gemini/GeminiPhone;

    iget v3, p0, Lcom/android/internal/telephony/gemini/GeminiPhone$SwitchBroadcastReceiver;->mSimId:I

    invoke-static {v2, v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->access$1900(Lcom/android/internal/telephony/gemini/GeminiPhone;I)Z

    goto :goto_0
.end method

.method public setSimId(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/internal/telephony/gemini/GeminiPhone$SwitchBroadcastReceiver;->mSimId:I

    return-void
.end method
