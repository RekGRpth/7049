.class public Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;
.super Ljava/lang/Object;
.source "GeminiRadioStatusUtil.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "PHONE"

.field private static final PREF_CATEGORY_RADIO_STATUS:Ljava/lang/String; = "RADIO_STATUS"

.field private static final PREF_RADIO_OFF_SIM_ICCID:Ljava/lang/String; = "RADIO_OFF_SIM_ICCID"

.field private static mContext:Landroid/content/Context;

.field private static mIccId1:Ljava/lang/String;

.field private static mIccId2:Ljava/lang/String;

.field private static mIccidPreference:Landroid/content/SharedPreferences;

.field private static mInitialRadioStatusDone:Z

.field private static mSIMMode:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x3

    sput v0, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->mSIMMode:I

    sput-object v1, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->mIccId1:Ljava/lang/String;

    sput-object v1, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->mIccId2:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static checkRadioOffSIM(Landroid/content/Context;)V
    .locals 3
    .param p0    # Landroid/content/Context;

    sput-object p0, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->mContext:Landroid/content/Context;

    sget-object v0, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->mContext:Landroid/content/Context;

    const-string v1, "RADIO_STATUS"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    sput-object v0, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->mIccidPreference:Landroid/content/SharedPreferences;

    sget-object v0, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->mIccidPreference:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "checkRadioOffSIM no recorded radio off SIM"

    invoke-static {v0}, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->log(Ljava/lang/String;)V

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->mInitialRadioStatusDone:Z

    :goto_0
    return-void

    :cond_0
    const-string v0, "checkRadioOffSIM has radio off SIM"

    invoke-static {v0}, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static finishRadioStatusInitialization(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->mInitialRadioStatusDone:Z

    sput-object p0, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->mIccId1:Ljava/lang/String;

    sput-object p1, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->mIccId2:Ljava/lang/String;

    sget-object v0, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->mIccId1:Ljava/lang/String;

    sget-object v1, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->mIccId2:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->getSIMModeByIccidStatus(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->mSIMMode:I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Finish radio status initialization ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->mSIMMode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->mIccId1:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->mIccId2:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->log(Ljava/lang/String;)V

    return-void
.end method

.method public static getSIMMode()I
    .locals 1

    sget v0, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->mSIMMode:I

    return v0
.end method

.method private static getSIMModeByIccidStatus(Ljava/lang/String;Ljava/lang/String;)I
    .locals 4
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x3

    sget-object v3, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->mIccidPreference:Landroid/content/SharedPreferences;

    invoke-interface {v3, p0}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    sget-object v3, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->mIccidPreference:Landroid/content/SharedPreferences;

    invoke-interface {v3, p1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    const/4 v2, 0x0

    :cond_0
    :goto_0
    return v2

    :cond_1
    if-nez v0, :cond_2

    if-nez v1, :cond_2

    const/4 v2, 0x3

    goto :goto_0

    :cond_2
    if-eqz v0, :cond_3

    const/4 v2, 0x2

    goto :goto_0

    :cond_3
    if-eqz v1, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static isRadioStatusInitialized()Z
    .locals 1

    sget-boolean v0, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->mInitialRadioStatusDone:Z

    return v0
.end method

.method private static log(Ljava/lang/String;)V
    .locals 3
    .param p0    # Ljava/lang/String;

    const-string v0, "PHONE"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[GeminiRadioStatusUtil] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private static putICCIDToPreference(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)V
    .locals 2
    .param p0    # Landroid/content/SharedPreferences$Editor;
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Add radio off SIM: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->log(Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-interface {p0, p1, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    :cond_0
    return-void
.end method

.method private static removeICCIDFromPreference(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)V
    .locals 2
    .param p0    # Landroid/content/SharedPreferences$Editor;
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Remove radio off SIM: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->log(Ljava/lang/String;)V

    invoke-interface {p0, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    :cond_0
    return-void
.end method

.method public static setRadioSIMMode(Landroid/content/Context;I)V
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # I

    const/4 v4, 0x0

    sput p1, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->mSIMMode:I

    sget-object v2, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->mIccidPreference:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    sget v2, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->mSIMMode:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    sget-object v2, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->mIccId1:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->removeICCIDFromPreference(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)V

    sget-object v2, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->mIccId2:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->putICCIDToPreference(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)V

    :cond_0
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setRadioSIMMode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->mSIMMode:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->log(Ljava/lang/String;)V

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void

    :cond_1
    sget v2, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->mSIMMode:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    sget-object v2, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->mIccId1:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->putICCIDToPreference(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)V

    sget-object v2, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->mIccId2:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->removeICCIDFromPreference(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    sget v2, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->mSIMMode:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_3

    sget-object v2, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->mIccId1:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->removeICCIDFromPreference(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)V

    sget-object v2, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->mIccId2:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->removeICCIDFromPreference(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    sget v2, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->mSIMMode:I

    if-nez v2, :cond_5

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "airplane_mode_on"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_4

    sget-object v2, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->mIccId1:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->putICCIDToPreference(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)V

    sget-object v2, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->mIccId2:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->putICCIDToPreference(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const-string v2, "air plane mode, not to record radio off SIM"

    invoke-static {v2}, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->log(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "dual_sim_mode_setting"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    sput v2, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->mSIMMode:I

    goto :goto_0

    :cond_5
    sget v2, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->mSIMMode:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    sget-object v2, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->mIccId1:Ljava/lang/String;

    sget-object v3, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->mIccId2:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->getSIMModeByIccidStatus(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    sput v2, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->mSIMMode:I

    goto :goto_0
.end method

.method public static setSIMIccId(ILjava/lang/String;)V
    .locals 0
    .param p0    # I
    .param p1    # Ljava/lang/String;

    packed-switch p0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    sput-object p1, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->mIccId1:Ljava/lang/String;

    goto :goto_0

    :pswitch_1
    sput-object p1, Lcom/android/internal/telephony/gemini/GeminiRadioStatusUtil;->mIccId2:Ljava/lang/String;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
