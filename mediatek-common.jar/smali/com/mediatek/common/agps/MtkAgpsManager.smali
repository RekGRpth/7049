.class public interface abstract Lcom/mediatek/common/agps/MtkAgpsManager;
.super Ljava/lang/Object;
.source "MtkAgpsManager.java"


# static fields
.field public static final AGPS_CAUSE_BAD_PUSH_CONTENT:I = 0x2

.field public static final AGPS_CAUSE_CNT:I = 0x10

.field public static final AGPS_CAUSE_NETWORK_CREATE_FAIL:I = 0x1

.field public static final AGPS_CAUSE_NETWORK_DISCONN:I = 0x6

.field public static final AGPS_CAUSE_NONE:I = 0x0

.field public static final AGPS_CAUSE_NOT_SUPPORTED:I = 0x3

.field public static final AGPS_CAUSE_NO_POSITION:I = 0xc

.field public static final AGPS_CAUSE_NO_RESOURCE:I = 0x5

.field public static final AGPS_CAUSE_REMOTE_ABORT:I = 0x7

.field public static final AGPS_CAUSE_REMOTE_MSG_ERROR:I = 0x9

.field public static final AGPS_CAUSE_REQ_NOT_ACCEPTED:I = 0x4

.field public static final AGPS_CAUSE_TIMER_EXPIRY:I = 0x8

.field public static final AGPS_CAUSE_TLS_AUTH_FAIL:I = 0xd

.field public static final AGPS_CAUSE_USER_AGREE:I = 0xa

.field public static final AGPS_CAUSE_USER_DENY:I = 0xb

.field public static final AGPS_CP_UP_VERIFY_TIMEOUT:I = 0xe

.field public static final AGPS_DISABLE_UPDATE:Ljava/lang/String; = "com.mediatek.agps.DISABLE_UPDATED"

.field public static final AGPS_EM_CNT:I = 0x3

.field public static final AGPS_EM_NONE:I = 0x0

.field public static final AGPS_EM_POS_FIXED:I = 0x2

.field public static final AGPS_EM_RECV_SI_REQ:I = 0x1

.field public static final AGPS_IND_CLOSEGPS:I = 0x2329

.field public static final AGPS_IND_EM:I = 0x457

.field public static final AGPS_IND_ERROR:I = 0x115c

.field public static final AGPS_IND_INFO:I = 0x8ae

.field public static final AGPS_IND_NOTIFY:I = 0xd05

.field public static final AGPS_IND_OPENGPS:I = 0x2328

.field public static final AGPS_IND_RESETGPS:I = 0x232a

.field public static final AGPS_INFO_CNT:I = 0x1

.field public static final AGPS_INFO_NONE:I = 0x0

.field public static final AGPS_MODEM_RESET_HAPPEN:I = 0xf

.field public static final AGPS_MODE_MA:I = 0x0

.field public static final AGPS_MODE_MB:I = 0x1

.field public static final AGPS_MODE_STANDALONE:I = 0x2

.field public static final AGPS_NOTIFY_ALLOW_NO_ANSWER:I = 0x2

.field public static final AGPS_NOTIFY_CNT:I = 0x5

.field public static final AGPS_NOTIFY_DENY_NO_ANSWER:I = 0x3

.field public static final AGPS_NOTIFY_NONE:I = 0x0

.field public static final AGPS_NOTIFY_ONLY:I = 0x1

.field public static final AGPS_NOTIFY_PRIVACY:I = 0x4

.field public static final AGPS_OMACP_PROFILE_UPDATE:Ljava/lang/String; = "com.mediatek.agps.OMACP_UPDATED"

.field public static final AGPS_PROFILE_UPDATE:Ljava/lang/String; = "com.mediatek.agps.PROFILE_UPDATED"

.field public static final AGPS_STATUS_UPDATE:Ljava/lang/String; = "com.mediatek.agps.STATUS_UPDATED"

.field public static final ERROR_ACTION:Ljava/lang/String; = "com.mediatek.agps.ERROR_ACTION"

.field public static final NATIVE_STATUS_DISABLE:I = 0x0

.field public static final NATIVE_STATUS_ENABLE:I = 0x1

.field public static final NOTIFY_ACTION:Ljava/lang/String; = "com.mediatek.agps.NOTIFY_ACTION"

.field public static final VERIFY_ACTION:Ljava/lang/String; = "com.mediatek.agps.VERIFY_ACTION"


# virtual methods
.method public abstract disable()V
.end method

.method public abstract enable()V
.end method

.method public abstract extraCommand(Ljava/lang/String;Landroid/os/Bundle;)I
.end method

.method public abstract getConfig()Lcom/mediatek/common/agps/MtkAgpsConfig;
.end method

.method public abstract getCpStatus()Z
.end method

.method public abstract getMode()I
.end method

.method public abstract getNiStatus()Z
.end method

.method public abstract getProfile()Lcom/mediatek/common/agps/MtkAgpsProfile;
.end method

.method public abstract getRoamingStatus()Z
.end method

.method public abstract getStatus()Z
.end method

.method public abstract getUpStatus()Z
.end method

.method public abstract log2file(Z)V
.end method

.method public abstract log2uart(Z)V
.end method

.method public abstract niUserResponse(I)V
.end method

.method public abstract setConfig(Lcom/mediatek/common/agps/MtkAgpsConfig;)V
.end method

.method public abstract setCpEnable(Z)V
.end method

.method public abstract setMode(I)V
.end method

.method public abstract setNiEnable(Z)V
.end method

.method public abstract setProfile(Lcom/mediatek/common/agps/MtkAgpsProfile;)V
.end method

.method public abstract setRoamingEnable(Z)V
.end method

.method public abstract setUpEnable(Z)V
.end method

.method public abstract supl2file(Z)V
.end method
