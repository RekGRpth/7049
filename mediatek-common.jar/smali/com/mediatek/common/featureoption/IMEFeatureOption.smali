.class public final Lcom/mediatek/common/featureoption/IMEFeatureOption;
.super Ljava/lang/Object;
.source "IMEFeatureOption.java"


# static fields
.field public static final DEFAULT_INPUT_METHOD:Ljava/lang/String; = "com.sohu.inputmethod.sogou.SogouIME"

.field public static final DEFAULT_LATIN_IME_LANGUAGES:[Ljava/lang/String;

.field public static final MTK_IME_ARABIC_SUPPORT:Z = false

.field public static final MTK_IME_ENGLISH_SUPPORT:Z = true

.field public static final MTK_IME_FRENCH_SUPPORT:Z = false

.field public static final MTK_IME_GERMAN_SUPPORT:Z = false

.field public static final MTK_IME_HANDWRITING_SUPPORT:Z = true

.field public static final MTK_IME_HINDI_SUPPORT:Z = false

.field public static final MTK_IME_INDONESIAN_SUPPORT:Z = false

.field public static final MTK_IME_ITALIAN_SUPPORT:Z = false

.field public static final MTK_IME_MALAY_SUPPORT:Z = false

.field public static final MTK_IME_PINYIN_SUPPORT:Z = true

.field public static final MTK_IME_PORTUGUESE_SUPPORT:Z = false

.field public static final MTK_IME_RUSSIAN_SUPPORT:Z = false

.field public static final MTK_IME_SPANISH_SUPPORT:Z = false

.field public static final MTK_IME_STROKE_SUPPORT:Z = true

.field public static final MTK_IME_SUPPORT:Z = false

.field public static final MTK_IME_THAI_SUPPORT:Z = false

.field public static final MTK_IME_TURKISH_SUPPORT:Z = false

.field public static final MTK_IME_VIETNAM_SUPPORT:Z = false

.field public static final MTK_IME_ZHUYIN_SUPPORT:Z = true

.field public static final PRODUCT_LOCALES:[Ljava/lang/String;

.field public static final SYS_LOCALE_AF:Z = true

.field public static final SYS_LOCALE_AF_ZA:Z = true

.field public static final SYS_LOCALE_AM:Z = true

.field public static final SYS_LOCALE_AM_ET:Z = true

.field public static final SYS_LOCALE_AR:Z = true

.field public static final SYS_LOCALE_AR_EG:Z = true

.field public static final SYS_LOCALE_AR_IL:Z = true

.field public static final SYS_LOCALE_BG:Z = true

.field public static final SYS_LOCALE_BG_BG:Z = true

.field public static final SYS_LOCALE_BN:Z = true

.field public static final SYS_LOCALE_BN_IN:Z = true

.field public static final SYS_LOCALE_CA:Z = true

.field public static final SYS_LOCALE_CA_ES:Z = true

.field public static final SYS_LOCALE_CS:Z = true

.field public static final SYS_LOCALE_CS_CZ:Z = true

.field public static final SYS_LOCALE_DA:Z = true

.field public static final SYS_LOCALE_DA_DK:Z = true

.field public static final SYS_LOCALE_DE:Z = true

.field public static final SYS_LOCALE_DE_AT:Z = true

.field public static final SYS_LOCALE_DE_CH:Z = true

.field public static final SYS_LOCALE_DE_DE:Z = true

.field public static final SYS_LOCALE_DE_LI:Z = true

.field public static final SYS_LOCALE_EL:Z = true

.field public static final SYS_LOCALE_EL_GR:Z = true

.field public static final SYS_LOCALE_EN:Z = true

.field public static final SYS_LOCALE_EN_AU:Z = true

.field public static final SYS_LOCALE_EN_CA:Z = true

.field public static final SYS_LOCALE_EN_GB:Z = true

.field public static final SYS_LOCALE_EN_IE:Z = true

.field public static final SYS_LOCALE_EN_IN:Z = true

.field public static final SYS_LOCALE_EN_NZ:Z = true

.field public static final SYS_LOCALE_EN_SG:Z = true

.field public static final SYS_LOCALE_EN_US:Z = true

.field public static final SYS_LOCALE_EN_ZA:Z = true

.field public static final SYS_LOCALE_ES:Z = true

.field public static final SYS_LOCALE_ES_ES:Z = true

.field public static final SYS_LOCALE_FA:Z = true

.field public static final SYS_LOCALE_FA_IR:Z = true

.field public static final SYS_LOCALE_FI:Z = true

.field public static final SYS_LOCALE_FI_FI:Z = true

.field public static final SYS_LOCALE_FR:Z = true

.field public static final SYS_LOCALE_FR_BE:Z = true

.field public static final SYS_LOCALE_FR_CA:Z = true

.field public static final SYS_LOCALE_FR_CH:Z = true

.field public static final SYS_LOCALE_FR_FR:Z = true

.field public static final SYS_LOCALE_HI:Z = true

.field public static final SYS_LOCALE_HI_IN:Z = true

.field public static final SYS_LOCALE_HR:Z = true

.field public static final SYS_LOCALE_HR_HR:Z = true

.field public static final SYS_LOCALE_HU:Z = true

.field public static final SYS_LOCALE_HU_HU:Z = true

.field public static final SYS_LOCALE_IN:Z = true

.field public static final SYS_LOCALE_IN_ID:Z = true

.field public static final SYS_LOCALE_IT:Z = true

.field public static final SYS_LOCALE_IT_CH:Z = true

.field public static final SYS_LOCALE_IT_IT:Z = true

.field public static final SYS_LOCALE_IW:Z = true

.field public static final SYS_LOCALE_IW_IL:Z = true

.field public static final SYS_LOCALE_JA:Z = true

.field public static final SYS_LOCALE_JA_JP:Z = true

.field public static final SYS_LOCALE_KM:Z = true

.field public static final SYS_LOCALE_KM_KH:Z = true

.field public static final SYS_LOCALE_KO:Z = true

.field public static final SYS_LOCALE_KO_KR:Z = true

.field public static final SYS_LOCALE_LT:Z = true

.field public static final SYS_LOCALE_LT_LT:Z = true

.field public static final SYS_LOCALE_LV:Z = true

.field public static final SYS_LOCALE_LV_LV:Z = true

.field public static final SYS_LOCALE_MS:Z = true

.field public static final SYS_LOCALE_MS_MY:Z = true

.field public static final SYS_LOCALE_MY:Z = true

.field public static final SYS_LOCALE_MY_MM:Z = true

.field public static final SYS_LOCALE_NB:Z = true

.field public static final SYS_LOCALE_NB_NO:Z = true

.field public static final SYS_LOCALE_NL:Z = true

.field public static final SYS_LOCALE_NL_BE:Z = true

.field public static final SYS_LOCALE_NL_NL:Z = true

.field public static final SYS_LOCALE_PL:Z = true

.field public static final SYS_LOCALE_PL_PL:Z = true

.field public static final SYS_LOCALE_PT:Z = true

.field public static final SYS_LOCALE_PT_BR:Z = true

.field public static final SYS_LOCALE_PT_PT:Z = true

.field public static final SYS_LOCALE_RM:Z = true

.field public static final SYS_LOCALE_RM_CH:Z = true

.field public static final SYS_LOCALE_RO:Z = true

.field public static final SYS_LOCALE_RO_RO:Z = true

.field public static final SYS_LOCALE_RU:Z = true

.field public static final SYS_LOCALE_RU_RU:Z = true

.field public static final SYS_LOCALE_SK:Z = true

.field public static final SYS_LOCALE_SK_SK:Z = true

.field public static final SYS_LOCALE_SL:Z = true

.field public static final SYS_LOCALE_SL_SI:Z = true

.field public static final SYS_LOCALE_SR:Z = true

.field public static final SYS_LOCALE_SR_RS:Z = true

.field public static final SYS_LOCALE_SV:Z = true

.field public static final SYS_LOCALE_SV_SE:Z = true

.field public static final SYS_LOCALE_SW:Z = true

.field public static final SYS_LOCALE_SW_TZ:Z = true

.field public static final SYS_LOCALE_TH:Z = true

.field public static final SYS_LOCALE_TH_TH:Z = true

.field public static final SYS_LOCALE_TL:Z = true

.field public static final SYS_LOCALE_TL_PH:Z = true

.field public static final SYS_LOCALE_TR:Z = true

.field public static final SYS_LOCALE_TR_TR:Z = true

.field public static final SYS_LOCALE_UK:Z = true

.field public static final SYS_LOCALE_UK_UA:Z = true

.field public static final SYS_LOCALE_UR:Z = true

.field public static final SYS_LOCALE_UR_PK:Z = true

.field public static final SYS_LOCALE_VI:Z = true

.field public static final SYS_LOCALE_VI_VN:Z = true

.field public static final SYS_LOCALE_ZH:Z = true

.field public static final SYS_LOCALE_ZH_CN:Z = true

.field public static final SYS_LOCALE_ZH_TW:Z = true

.field public static final SYS_LOCALE_ZU:Z = true

.field public static final SYS_LOCALE_ZU_ZA:Z = true


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/16 v0, 0x43

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "ru_RU"

    aput-object v1, v0, v3

    const-string v1, "af_ZA"

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const-string v2, "am_ET"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "ar_EG"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "ar_IL"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "bg_BG"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "bn_IN"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "ca_ES"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "cs_CZ"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "da_DK"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "de_AT"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "de_CH"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "de_DE"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "de_LI"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "el_GR"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "en_AU"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "en_CA"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "en_GB"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "en_IE"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "en_IN"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "en_NZ"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "en_SG"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "en_ZA"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "es_ES"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "fa_IR"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "fi_FI"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "fr_BE"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "fr_CA"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "fr_CH"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "fr_FR"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "hi_IN"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "hr_HR"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "hu_HU"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "in_ID"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "it_CH"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "it_IT"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "iw_IL"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "ja_JP"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "km_KH"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "ko_KR"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "lt_LT"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "lv_LV"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "ms_MY"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, "my_MM"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, "nb_NO"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, "nl_BE"

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, "nl_NL"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, "pl_PL"

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string v2, "pt_BR"

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string v2, "pt_PT"

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string v2, "rm_CH"

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string v2, "ro_RO"

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string v2, "en_US"

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string v2, "sk_SK"

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string v2, "sl_SI"

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string v2, "sr_RS"

    aput-object v2, v0, v1

    const/16 v1, 0x38

    const-string v2, "sv_SE"

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string v2, "sw_TZ"

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    const-string v2, "th_TH"

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    const-string v2, "tl_PH"

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    const-string v2, "tr_TR"

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    const-string v2, "uk_UA"

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    const-string v2, "ur_PK"

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    const-string v2, "vi_VN"

    aput-object v2, v0, v1

    const/16 v1, 0x40

    const-string v2, "zh_CN"

    aput-object v2, v0, v1

    const/16 v1, 0x41

    const-string v2, "zh_TW"

    aput-object v2, v0, v1

    const/16 v1, 0x42

    const-string v2, "zu_ZA"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mediatek/common/featureoption/IMEFeatureOption;->PRODUCT_LOCALES:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "ru_RU"

    aput-object v1, v0, v3

    sput-object v0, Lcom/mediatek/common/featureoption/IMEFeatureOption;->DEFAULT_LATIN_IME_LANGUAGES:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
