.class public interface abstract Lcom/mediatek/common/hdmi/IHDMINative;
.super Ljava/lang/Object;
.source "IHDMINative.java"


# virtual methods
.method public abstract enableAudio(Z)Z
.end method

.method public abstract enableHDMI(Z)Z
.end method

.method public abstract enableHDMIIPO(Z)Z
.end method

.method public abstract enableVideo(Z)Z
.end method

.method public abstract hdmiPortraitEnable(Z)Z
.end method

.method public abstract hdmiPowerEnable(Z)Z
.end method

.method public abstract ishdmiForceAwake()Z
.end method

.method public abstract setAudioConfig(I)Z
.end method

.method public abstract setVideoConfig(I)Z
.end method
