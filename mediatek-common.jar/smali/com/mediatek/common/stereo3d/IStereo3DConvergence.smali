.class public interface abstract Lcom/mediatek/common/stereo3d/IStereo3DConvergence;
.super Ljava/lang/Object;
.source "IStereo3DConvergence.java"


# static fields
.field public static final EXECUTE:Ljava/lang/String; = "execute"


# virtual methods
.method public abstract getActiveFlags()[I
.end method

.method public abstract getCropImageHeight()I
.end method

.method public abstract getCropImageWidth()I
.end method

.method public abstract getCroppingIntervals(Z)[I
.end method

.method public abstract getDefaultPosition()I
.end method

.method public abstract getOffsetX(Z)I
.end method

.method public abstract getOffsetY(Z)I
.end method
