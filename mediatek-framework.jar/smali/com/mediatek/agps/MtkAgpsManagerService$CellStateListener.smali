.class Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;
.super Landroid/telephony/PhoneStateListener;
.source "MtkAgpsManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/agps/MtkAgpsManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CellStateListener"
.end annotation


# instance fields
.field private mSimID:I

.field final synthetic this$0:Lcom/mediatek/agps/MtkAgpsManagerService;


# direct methods
.method public constructor <init>(Lcom/mediatek/agps/MtkAgpsManagerService;)V
    .locals 1

    const/4 v0, 0x1

    iput-object p1, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    iput v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->mSimID:I

    iput v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->mSimID:I

    return-void
.end method

.method public constructor <init>(Lcom/mediatek/agps/MtkAgpsManagerService;I)V
    .locals 1
    .param p2    # I

    iput-object p1, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->mSimID:I

    iput p2, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->mSimID:I

    return-void
.end method

.method private getIpAddr(ZI)Ljava/lang/String;
    .locals 6
    .param p1    # Z
    .param p2    # I

    const/4 v1, 0x0

    const-string v3, "phone"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v2

    if-eqz p1, :cond_3

    if-eqz v2, :cond_0

    const/4 v3, 0x2

    if-ne p2, v3, :cond_2

    :try_start_0
    const-string v3, "default"

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, Lcom/android/internal/telephony/ITelephony;->getIpAddressGemini(Ljava/lang/String;I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :cond_0
    :goto_0
    if-nez v1, :cond_1

    const-string v1, "127.0.0.0"

    :cond_1
    iget-object v3, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "IP="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " len="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$000(Lcom/mediatek/agps/MtkAgpsManagerService;Ljava/lang/String;)V

    return-object v1

    :cond_2
    :try_start_1
    const-string v3, "default"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Lcom/android/internal/telephony/ITelephony;->getIpAddressGemini(Ljava/lang/String;I)Ljava/lang/String;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ERR: getIpAddressGemini(GEMINI_SIM_1) throw exception:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$000(Lcom/mediatek/agps/MtkAgpsManagerService;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    if-eqz v2, :cond_0

    :try_start_2
    const-string v3, "default"

    invoke-interface {v2, v3}, Lcom/android/internal/telephony/ITelephony;->getIpAddress(Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v1

    goto :goto_0

    :catch_1
    move-exception v0

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ERR: getIpAddress(Single_SIM_1) throw exception:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$000(Lcom/mediatek/agps/MtkAgpsManagerService;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getNetworkType()I
    .locals 8

    const/4 v7, 0x3

    const/4 v6, 0x1

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    invoke-static {v4}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$1200(Lcom/mediatek/agps/MtkAgpsManagerService;)Landroid/content/Context;

    move-result-object v4

    const-string v5, "phone"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v4

    if-ne v4, v6, :cond_3

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getCellLocation()Landroid/telephony/CellLocation;

    move-result-object v0

    check-cast v0, Landroid/telephony/gsm/GsmCellLocation;

    if-eqz v0, :cond_2

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v4

    if-ne v4, v6, :cond_2

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-le v4, v7, :cond_2

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v1

    if-eq v1, v7, :cond_0

    const/16 v4, 0x8

    if-eq v1, v4, :cond_0

    const/16 v4, 0x9

    if-eq v1, v4, :cond_0

    const/16 v4, 0xa

    if-ne v1, v4, :cond_1

    :cond_0
    iget-object v4, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SIM="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->mSimID:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " network type is 3G, networkType="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$000(Lcom/mediatek/agps/MtkAgpsManagerService;Ljava/lang/String;)V

    const/4 v3, 0x1

    :goto_0
    return v3

    :cond_1
    iget-object v4, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SIM="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->mSimID:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " network type is 2G, networkType="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$000(Lcom/mediatek/agps/MtkAgpsManagerService;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    const-string v5, "ERR: gsm_cell is invalid"

    invoke-static {v4, v5}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$000(Lcom/mediatek/agps/MtkAgpsManagerService;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v4, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ERR: phone type is not GSM ="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " (0:NONE 1:GSM 2:CDMA 3:SIP)"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$000(Lcom/mediatek/agps/MtkAgpsManagerService;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private isNetworkRoaming(ZI)Z
    .locals 2
    .param p1    # Z
    .param p2    # I

    if-eqz p1, :cond_1

    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    invoke-static {v0}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$2100(Lcom/mediatek/agps/MtkAgpsManagerService;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/telephony/TelephonyManager;->isNetworkRoamingGemini(I)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    invoke-static {v0}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$2100(Lcom/mediatek/agps/MtkAgpsManagerService;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/telephony/TelephonyManager;->isNetworkRoamingGemini(I)Z

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    invoke-static {v0}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$2100(Lcom/mediatek/agps/MtkAgpsManagerService;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    move-result v0

    goto :goto_0
.end method

.method private updateCallState(I)V
    .locals 2
    .param p1    # I

    iget v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->mSimID:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    const/16 v1, 0x1e

    invoke-static {v0, v1, p1}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$1600(Lcom/mediatek/agps/MtkAgpsManagerService;II)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    const/16 v1, 0x1d

    invoke-static {v0, v1, p1}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$1600(Lcom/mediatek/agps/MtkAgpsManagerService;II)V

    goto :goto_0
.end method

.method private updateConnectionState(I)V
    .locals 3
    .param p1    # I

    iget v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->mSimID:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    invoke-static {v0, p1}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$2402(Lcom/mediatek/agps/MtkAgpsManagerService;I)I

    iget-object v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    const/16 v1, 0x26

    iget-object v2, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    invoke-static {v2}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$2400(Lcom/mediatek/agps/MtkAgpsManagerService;)I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$1600(Lcom/mediatek/agps/MtkAgpsManagerService;II)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    invoke-static {v0, p1}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$2502(Lcom/mediatek/agps/MtkAgpsManagerService;I)I

    iget-object v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    const/16 v1, 0x25

    iget-object v2, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    invoke-static {v2}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$2500(Lcom/mediatek/agps/MtkAgpsManagerService;)I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$1600(Lcom/mediatek/agps/MtkAgpsManagerService;II)V

    goto :goto_0
.end method

.method private updateIPAdress()V
    .locals 7

    const/4 v3, -0x1

    iget-object v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    invoke-virtual {p0}, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->getLocalIpAddress()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$1302(Lcom/mediatek/agps/MtkAgpsManagerService;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    invoke-static {v0}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$1300(Lcom/mediatek/agps/MtkAgpsManagerService;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    const/16 v1, 0x22

    iget-object v2, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    invoke-static {v2}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$1300(Lcom/mediatek/agps/MtkAgpsManagerService;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    iget-object v4, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    invoke-static {v4}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$1300(Lcom/mediatek/agps/MtkAgpsManagerService;)Ljava/lang/String;

    move-result-object v6

    move v4, v3

    move v5, v3

    invoke-static/range {v0 .. v6}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$1500(Lcom/mediatek/agps/MtkAgpsManagerService;IIIIILjava/lang/String;)V

    :cond_0
    return-void
.end method

.method private updateNetworkType()V
    .locals 3

    iget v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->mSimID:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    invoke-direct {p0}, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->getNetworkType()I

    move-result v1

    invoke-static {v0, v1}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$2202(Lcom/mediatek/agps/MtkAgpsManagerService;I)I

    iget-object v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    const/16 v1, 0x20

    iget-object v2, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    invoke-static {v2}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$2200(Lcom/mediatek/agps/MtkAgpsManagerService;)I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$1600(Lcom/mediatek/agps/MtkAgpsManagerService;II)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    invoke-direct {p0}, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->getNetworkType()I

    move-result v1

    invoke-static {v0, v1}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$2302(Lcom/mediatek/agps/MtkAgpsManagerService;I)I

    iget-object v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    const/16 v1, 0x1f

    iget-object v2, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    invoke-static {v2}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$2300(Lcom/mediatek/agps/MtkAgpsManagerService;)I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$1600(Lcom/mediatek/agps/MtkAgpsManagerService;II)V

    goto :goto_0
.end method

.method private updateRoamingStatus()V
    .locals 4

    const/4 v3, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->mSimID:I

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    invoke-static {v0}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$2800(Lcom/mediatek/agps/MtkAgpsManagerService;)Z

    move-result v0

    invoke-direct {p0, v0, v3}, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->isNetworkRoaming(ZI)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    invoke-static {v0, v1}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$2902(Lcom/mediatek/agps/MtkAgpsManagerService;I)I

    :goto_0
    iget-object v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SIM="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->mSimID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " isNetworkRoaming="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    invoke-static {v2}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$2900(Lcom/mediatek/agps/MtkAgpsManagerService;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$000(Lcom/mediatek/agps/MtkAgpsManagerService;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    const/16 v1, 0x24

    iget-object v2, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    invoke-static {v2}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$2900(Lcom/mediatek/agps/MtkAgpsManagerService;)I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$1600(Lcom/mediatek/agps/MtkAgpsManagerService;II)V

    :goto_1
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    invoke-static {v0, v2}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$2902(Lcom/mediatek/agps/MtkAgpsManagerService;I)I

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    invoke-static {v0}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$2800(Lcom/mediatek/agps/MtkAgpsManagerService;)Z

    move-result v0

    invoke-direct {p0, v0, v1}, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->isNetworkRoaming(ZI)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    invoke-static {v0, v1}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$3002(Lcom/mediatek/agps/MtkAgpsManagerService;I)I

    :goto_2
    iget-object v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SIM="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->mSimID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " isNetworkRoaming="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    invoke-static {v2}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$3000(Lcom/mediatek/agps/MtkAgpsManagerService;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$000(Lcom/mediatek/agps/MtkAgpsManagerService;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    const/16 v1, 0x23

    iget-object v2, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    invoke-static {v2}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$3000(Lcom/mediatek/agps/MtkAgpsManagerService;)I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$1600(Lcom/mediatek/agps/MtkAgpsManagerService;II)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    invoke-static {v0, v2}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$3002(Lcom/mediatek/agps/MtkAgpsManagerService;I)I

    goto :goto_2
.end method

.method private updateSimStatus(I)V
    .locals 2
    .param p1    # I

    iget v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->mSimID:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    invoke-static {v0, p1}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$2602(Lcom/mediatek/agps/MtkAgpsManagerService;I)I

    iget-object v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    const/16 v1, 0x1c

    invoke-static {v0, v1, p1}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$1600(Lcom/mediatek/agps/MtkAgpsManagerService;II)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    invoke-static {v0, p1}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$2702(Lcom/mediatek/agps/MtkAgpsManagerService;I)I

    iget-object v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    const/16 v1, 0x1b

    invoke-static {v0, v1, p1}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$1600(Lcom/mediatek/agps/MtkAgpsManagerService;II)V

    goto :goto_0
.end method


# virtual methods
.method public getLocalIpAddress()Ljava/lang/String;
    .locals 9

    :try_start_0
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/net/NetworkInterface;

    invoke-virtual {v4}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/net/InetAddress;

    invoke-virtual {v3}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {v3}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "IP="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$000(Lcom/mediatek/agps/MtkAgpsManagerService;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-object v5

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_2
    :goto_1
    const/4 v5, 0x0

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_1
.end method

.method public onCallStateChanged(ILjava/lang/String;)V
    .locals 3
    .param p1    # I
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SIM="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->mSimID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " phone call_state:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " (0:idle 1:ringing 2:offhook)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$000(Lcom/mediatek/agps/MtkAgpsManagerService;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->updateCallState(I)V

    return-void
.end method

.method public onCellLocationChanged(Landroid/telephony/CellLocation;)V
    .locals 3
    .param p1    # Landroid/telephony/CellLocation;

    iget-object v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    iget-boolean v0, v0, Lcom/mediatek/agps/MtkAgpsManagerService;->mIsSocketOK:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SIM="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->mSimID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onCellLocationChanged"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$000(Lcom/mediatek/agps/MtkAgpsManagerService;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    invoke-static {v0}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$3100(Lcom/mediatek/agps/MtkAgpsManagerService;)I

    iget-object v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    invoke-static {v0}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$2700(Lcom/mediatek/agps/MtkAgpsManagerService;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->updateNetworkType()V

    :cond_0
    return-void
.end method

.method public onDataConnectionStateChanged(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    iget-boolean v0, v0, Lcom/mediatek/agps/MtkAgpsManagerService;->mIsSocketOK:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SIM="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->mSimID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onDataConnectionStateChanged connected"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$000(Lcom/mediatek/agps/MtkAgpsManagerService;Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->updateConnectionState(I)V

    invoke-direct {p0}, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->updateNetworkType()V

    invoke-direct {p0}, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->updateIPAdress()V

    iget-object v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    invoke-static {v0}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$3100(Lcom/mediatek/agps/MtkAgpsManagerService;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    iget-boolean v0, v0, Lcom/mediatek/agps/MtkAgpsManagerService;->mIsSocketOK:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SIM="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->mSimID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onDataConnectionStateChanged disconnected"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$000(Lcom/mediatek/agps/MtkAgpsManagerService;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->updateConnectionState(I)V

    goto :goto_0
.end method

.method public onServiceStateChanged(Landroid/telephony/ServiceState;)V
    .locals 4
    .param p1    # Landroid/telephony/ServiceState;

    const/4 v3, 0x1

    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getState()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SIM="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->mSimID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " phone status: in service"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$000(Lcom/mediatek/agps/MtkAgpsManagerService;Ljava/lang/String;)V

    invoke-direct {p0, v3}, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->updateSimStatus(I)V

    invoke-direct {p0}, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->updateRoamingStatus()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getState()I

    move-result v0

    if-ne v0, v3, :cond_0

    iget-object v0, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SIM="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->mSimID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " phone status: out of service"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/agps/MtkAgpsManagerService;->access$000(Lcom/mediatek/agps/MtkAgpsManagerService;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mediatek/agps/MtkAgpsManagerService$CellStateListener;->updateSimStatus(I)V

    goto :goto_0
.end method
