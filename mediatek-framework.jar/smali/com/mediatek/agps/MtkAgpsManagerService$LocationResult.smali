.class public Lcom/mediatek/agps/MtkAgpsManagerService$LocationResult;
.super Ljava/lang/Object;
.source "MtkAgpsManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/agps/MtkAgpsManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "LocationResult"
.end annotation


# instance fields
.field public mTTFF:I

.field public maccuracy:F

.field public maltitude:D

.field public mbearing:F

.field public mlatitude:D

.field public mlongitude:D

.field public mspeed:F

.field public mtimestamp:J

.field final synthetic this$0:Lcom/mediatek/agps/MtkAgpsManagerService;


# direct methods
.method public constructor <init>(Lcom/mediatek/agps/MtkAgpsManagerService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/agps/MtkAgpsManagerService$LocationResult;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/mediatek/agps/MtkAgpsManagerService;DDDFFFJI)V
    .locals 0
    .param p2    # D
    .param p4    # D
    .param p6    # D
    .param p8    # F
    .param p9    # F
    .param p10    # F
    .param p11    # J
    .param p13    # I

    iput-object p1, p0, Lcom/mediatek/agps/MtkAgpsManagerService$LocationResult;->this$0:Lcom/mediatek/agps/MtkAgpsManagerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p2, p0, Lcom/mediatek/agps/MtkAgpsManagerService$LocationResult;->mlatitude:D

    iput-wide p4, p0, Lcom/mediatek/agps/MtkAgpsManagerService$LocationResult;->mlongitude:D

    iput-wide p6, p0, Lcom/mediatek/agps/MtkAgpsManagerService$LocationResult;->maltitude:D

    iput p8, p0, Lcom/mediatek/agps/MtkAgpsManagerService$LocationResult;->mspeed:F

    iput p9, p0, Lcom/mediatek/agps/MtkAgpsManagerService$LocationResult;->mbearing:F

    iput p10, p0, Lcom/mediatek/agps/MtkAgpsManagerService$LocationResult;->maccuracy:F

    iput-wide p11, p0, Lcom/mediatek/agps/MtkAgpsManagerService$LocationResult;->mtimestamp:J

    iput p13, p0, Lcom/mediatek/agps/MtkAgpsManagerService$LocationResult;->mTTFF:I

    return-void
.end method
