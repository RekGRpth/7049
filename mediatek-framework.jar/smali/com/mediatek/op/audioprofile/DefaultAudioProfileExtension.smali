.class public Lcom/mediatek/op/audioprofile/DefaultAudioProfileExtension;
.super Ljava/lang/Object;
.source "DefaultAudioProfileExtension.java"

# interfaces
.implements Lcom/mediatek/common/audioprofile/IAudioProfileExtension;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/op/audioprofile/DefaultAudioProfileExtension$ActiveProfileChangeInfo;
    }
.end annotation


# instance fields
.field protected final IS_SUPPORT_OUTDOOR_EDITABLE:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/op/audioprofile/DefaultAudioProfileExtension;->IS_SUPPORT_OUTDOOR_EDITABLE:Z

    return-void
.end method


# virtual methods
.method public getActiveProfileChangeInfo(ZLjava/lang/String;Ljava/lang/String;Z)Lcom/mediatek/common/audioprofile/IAudioProfileExtension$IActiveProfileChangeInfo;
    .locals 1
    .param p1    # Z
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Z

    const/4 v0, 0x0

    return-object v0
.end method

.method public init(Lcom/mediatek/common/audioprofile/IAudioProfileService;Landroid/content/Context;)V
    .locals 0
    .param p1    # Lcom/mediatek/common/audioprofile/IAudioProfileService;
    .param p2    # Landroid/content/Context;

    return-void
.end method

.method public onActiveProfileChange(ZLjava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1    # Z
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const/4 v0, 0x1

    return v0
.end method

.method public onNotificationChange(Z)Z
    .locals 1
    .param p1    # Z

    const/4 v0, 0x0

    return v0
.end method

.method public onRingerModeChanged(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method

.method public onRingerVolumeChanged(IILjava/lang/String;)Z
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # Ljava/lang/String;

    const/4 v0, 0x0

    return v0
.end method

.method public onRingtoneChange(Z)Z
    .locals 1
    .param p1    # Z

    const/4 v0, 0x0

    return v0
.end method

.method public persistStreamVolumeToSystem(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method

.method public shouldCheckDefaultProfiles()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public shouldSyncGeneralRingtoneToOutdoor()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
