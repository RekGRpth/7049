.class public Lcom/mediatek/op/telephony/ServiceStateExt;
.super Ljava/lang/Object;
.source "ServiceStateExt.java"

# interfaces
.implements Lcom/mediatek/common/telephony/IServiceStateExt;


# static fields
.field static final TAG:Ljava/lang/String; = "ServiceStateExt"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public ignoreDomesticRoaming()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isImeiLocked()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isRegCodeRoaming(ZILjava/lang/String;)Z
    .locals 0
    .param p1    # Z
    .param p2    # I
    .param p3    # Ljava/lang/String;

    return p1
.end method

.method public log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "ServiceStateExt"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public mapGsmSignalDbm(II)I
    .locals 2
    .param p1    # I
    .param p2    # I

    mul-int/lit8 v1, p2, 0x2

    add-int/lit8 v0, v1, -0x71

    return v0
.end method

.method public mapGsmSignalLevel(I)I
    .locals 2
    .param p1    # I

    const/4 v1, 0x2

    if-le p1, v1, :cond_0

    const/16 v1, 0x63

    if-ne p1, v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/16 v1, 0xc

    if-lt p1, v1, :cond_2

    const/4 v0, 0x4

    goto :goto_0

    :cond_2
    const/16 v1, 0x8

    if-lt p1, v1, :cond_3

    const/4 v0, 0x3

    goto :goto_0

    :cond_3
    const/4 v1, 0x5

    if-lt p1, v1, :cond_4

    const/4 v0, 0x2

    goto :goto_0

    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public needBrodcastACMT(II)Z
    .locals 1
    .param p1    # I
    .param p2    # I

    const/4 v0, 0x0

    return v0
.end method

.method public needIgnoredState(III)Z
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x0

    return v0
.end method

.method public needRejectCauseNotification(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method

.method public onPollStateDone(Landroid/telephony/ServiceState;Landroid/telephony/ServiceState;II)V
    .locals 0
    .param p1    # Landroid/telephony/ServiceState;
    .param p2    # Landroid/telephony/ServiceState;
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public onUpdateSpnDisplay(Ljava/lang/String;I)Ljava/lang/String;
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # I

    return-object p1
.end method

.method public setEmergencyCallsOnly(II)I
    .locals 1
    .param p1    # I
    .param p2    # I

    const/4 v0, -0x1

    return v0
.end method
