.class public Lcom/mediatek/stereo3d/Stereo3DConvergence;
.super Ljava/lang/Object;
.source "Stereo3DConvergence.java"

# interfaces
.implements Lcom/mediatek/common/stereo3d/IStereo3DConvergence;


# static fields
.field private static final NUM_OF_INTERVALS:I = 0x9

.field private static final SUCCESS:I = 0x0

.field private static final TAG:Ljava/lang/String; = "Stereo3DConvergence"


# instance fields
.field private final mActiveFlags:[I

.field private final mBitmapL:Landroid/graphics/Bitmap;

.field private final mBitmapR:Landroid/graphics/Bitmap;

.field private final mCropImageHeight:I

.field private final mCropImageWidth:I

.field private final mCroppingIntervalL:[I

.field private final mCroppingIntervalR:[I

.field private mDefaultPosition:I

.field private mLeftOffsetX:I

.field private mLeftOffsetY:I

.field private mRightOffsetX:I

.field private mRightOffsetY:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "gia"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    const-string v0, "giajni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V
    .locals 7
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # Landroid/graphics/Bitmap;

    const-wide v5, 0x3ff199999999999aL

    const-wide/high16 v3, 0x3fe0000000000000L

    const/16 v2, 0x9

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mediatek/stereo3d/Stereo3DConvergence;->mBitmapL:Landroid/graphics/Bitmap;

    iput-object p2, p0, Lcom/mediatek/stereo3d/Stereo3DConvergence;->mBitmapR:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/mediatek/stereo3d/Stereo3DConvergence;->mBitmapL:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    int-to-float v0, v0

    float-to-double v0, v0

    div-double/2addr v0, v5

    add-double/2addr v0, v3

    double-to-int v0, v0

    div-int/lit8 v0, v0, 0xa

    mul-int/lit8 v0, v0, 0xa

    iput v0, p0, Lcom/mediatek/stereo3d/Stereo3DConvergence;->mCropImageWidth:I

    iget-object v0, p0, Lcom/mediatek/stereo3d/Stereo3DConvergence;->mBitmapL:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    int-to-float v0, v0

    float-to-double v0, v0

    div-double/2addr v0, v5

    add-double/2addr v0, v3

    double-to-int v0, v0

    div-int/lit8 v0, v0, 0xa

    mul-int/lit8 v0, v0, 0xa

    iput v0, p0, Lcom/mediatek/stereo3d/Stereo3DConvergence;->mCropImageHeight:I

    new-array v0, v2, [I

    iput-object v0, p0, Lcom/mediatek/stereo3d/Stereo3DConvergence;->mCroppingIntervalL:[I

    new-array v0, v2, [I

    iput-object v0, p0, Lcom/mediatek/stereo3d/Stereo3DConvergence;->mCroppingIntervalR:[I

    new-array v0, v2, [I

    iput-object v0, p0, Lcom/mediatek/stereo3d/Stereo3DConvergence;->mActiveFlags:[I

    return-void
.end method

.method private static native close3DConvergence()I
.end method

.method public static execute(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)Lcom/mediatek/stereo3d/Stereo3DConvergence;
    .locals 1
    .param p0    # Landroid/graphics/Bitmap;
    .param p1    # Landroid/graphics/Bitmap;

    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/mediatek/stereo3d/Stereo3DConvergence;->execute(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Z)Lcom/mediatek/stereo3d/Stereo3DConvergence;

    move-result-object v0

    return-object v0
.end method

.method public static execute(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Z)Lcom/mediatek/stereo3d/Stereo3DConvergence;
    .locals 6
    .param p0    # Landroid/graphics/Bitmap;
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # Z

    const/4 v2, 0x0

    if-eqz p0, :cond_0

    if-nez p1, :cond_2

    :cond_0
    const-string v3, "Stereo3DConvergence"

    const-string v4, "Bitmaps are null"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v2

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    if-eq v3, v4, :cond_3

    const-string v3, "Stereo3DConvergence"

    const-string v4, "Bitmaps are not valid"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v2

    goto :goto_0

    :cond_3
    new-instance v0, Lcom/mediatek/stereo3d/Stereo3DConvergence;

    invoke-direct {v0, p0, p1}, Lcom/mediatek/stereo3d/Stereo3DConvergence;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    const-string v3, "Stereo3DConvergence"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Execute convergence: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " x "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, -0x1

    const-class v3, Lcom/mediatek/stereo3d/Stereo3DConvergence;

    monitor-enter v3

    :try_start_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-static {v4, v5}, Lcom/mediatek/stereo3d/Stereo3DConvergence;->init3DConvergence(II)I

    move-result v1

    if-nez v1, :cond_4

    invoke-static {v0, p2}, Lcom/mediatek/stereo3d/Stereo3DConvergence;->process3DConvergence(Lcom/mediatek/stereo3d/Stereo3DConvergence;Z)I

    move-result v1

    invoke-static {}, Lcom/mediatek/stereo3d/Stereo3DConvergence;->close3DConvergence()I

    :cond_4
    monitor-exit v3

    if-eqz v1, :cond_1

    move-object v0, v2

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private static native init3DConvergence(II)I
.end method

.method private static native process3DConvergence(Lcom/mediatek/stereo3d/Stereo3DConvergence;Z)I
.end method


# virtual methods
.method public getActiveFlags()[I
    .locals 2

    iget-object v0, p0, Lcom/mediatek/stereo3d/Stereo3DConvergence;->mActiveFlags:[I

    const/16 v1, 0x9

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    return-object v0
.end method

.method public getCropImageHeight()I
    .locals 1

    iget v0, p0, Lcom/mediatek/stereo3d/Stereo3DConvergence;->mCropImageHeight:I

    return v0
.end method

.method public getCropImageWidth()I
    .locals 1

    iget v0, p0, Lcom/mediatek/stereo3d/Stereo3DConvergence;->mCropImageWidth:I

    return v0
.end method

.method public getCroppingIntervals(Z)[I
    .locals 2
    .param p1    # Z

    const/16 v1, 0x9

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/mediatek/stereo3d/Stereo3DConvergence;->mCroppingIntervalL:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/stereo3d/Stereo3DConvergence;->mCroppingIntervalR:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    goto :goto_0
.end method

.method public getDefaultPosition()I
    .locals 1

    iget v0, p0, Lcom/mediatek/stereo3d/Stereo3DConvergence;->mDefaultPosition:I

    return v0
.end method

.method public getOffsetX(Z)I
    .locals 1
    .param p1    # Z

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/mediatek/stereo3d/Stereo3DConvergence;->mLeftOffsetX:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/mediatek/stereo3d/Stereo3DConvergence;->mRightOffsetX:I

    goto :goto_0
.end method

.method public getOffsetY(Z)I
    .locals 1
    .param p1    # Z

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/mediatek/stereo3d/Stereo3DConvergence;->mLeftOffsetY:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/mediatek/stereo3d/Stereo3DConvergence;->mRightOffsetY:I

    goto :goto_0
.end method
