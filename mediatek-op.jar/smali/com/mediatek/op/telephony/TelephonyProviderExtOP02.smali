.class public Lcom/mediatek/op/telephony/TelephonyProviderExtOP02;
.super Lcom/mediatek/op/telephony/TelephonyProviderExt;
.source "TelephonyProviderExtOP02.java"


# instance fields
.field private final cuApnNet:Ljava/lang/String;

.field private final cuApnWap:Ljava/lang/String;

.field private final cuNum:Ljava/lang/String;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Lcom/mediatek/op/telephony/TelephonyProviderExt;-><init>()V

    const-string v0, "46001"

    iput-object v0, p0, Lcom/mediatek/op/telephony/TelephonyProviderExtOP02;->cuNum:Ljava/lang/String;

    const-string v0, "3gnet"

    iput-object v0, p0, Lcom/mediatek/op/telephony/TelephonyProviderExtOP02;->cuApnNet:Ljava/lang/String;

    const-string v0, "3gwap"

    iput-object v0, p0, Lcom/mediatek/op/telephony/TelephonyProviderExtOP02;->cuApnWap:Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/op/telephony/TelephonyProviderExtOP02;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public onLoadApns(Landroid/content/ContentValues;)I
    .locals 4
    .param p1    # Landroid/content/ContentValues;

    const/4 v0, 0x1

    if-eqz p1, :cond_2

    const-string v1, "numeric"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2

    const-string v1, "numeric"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    const-string v2, "46001"

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "apn"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2

    const-string v1, "apn"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    const-string v2, "3gnet"

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "name"

    iget-object v2, p0, Lcom/mediatek/op/telephony/TelephonyProviderExtOP02;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x20500e2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return v0

    :cond_0
    const-string v1, "apn"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    const-string v2, "3gwap"

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "type"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-ne v1, v0, :cond_1

    const-string v1, "type"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v1, "type"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    const-string v2, "mms"

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    const-string v1, "name"

    iget-object v2, p0, Lcom/mediatek/op/telephony/TelephonyProviderExtOP02;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x20500e3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
