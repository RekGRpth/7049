.class public Lcom/android/commands/monkey/MonkeyRotationEvent;
.super Lcom/android/commands/monkey/MonkeyEvent;
.source "MonkeyRotationEvent.java"


# instance fields
.field private final mPersist:Z

.field private final mRotationDegree:I


# direct methods
.method public constructor <init>(IZ)V
    .locals 1
    .param p1    # I
    .param p2    # Z

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/android/commands/monkey/MonkeyEvent;-><init>(I)V

    iput p1, p0, Lcom/android/commands/monkey/MonkeyRotationEvent;->mRotationDegree:I

    iput-boolean p2, p0, Lcom/android/commands/monkey/MonkeyRotationEvent;->mPersist:Z

    return-void
.end method


# virtual methods
.method public injectEvent(Landroid/view/IWindowManager;Landroid/app/IActivityManager;I)I
    .locals 4
    .param p1    # Landroid/view/IWindowManager;
    .param p2    # Landroid/app/IActivityManager;
    .param p3    # I

    if-lez p3, :cond_0

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ":Sending rotation degree="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/commands/monkey/MonkeyRotationEvent;->mRotationDegree:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", persist="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/commands/monkey/MonkeyRotationEvent;->mPersist:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :try_start_0
    iget v1, p0, Lcom/android/commands/monkey/MonkeyRotationEvent;->mRotationDegree:I

    invoke-interface {p1, v1}, Landroid/view/IWindowManager;->freezeRotation(I)V

    iget-boolean v1, p0, Lcom/android/commands/monkey/MonkeyRotationEvent;->mPersist:Z

    if-nez v1, :cond_1

    invoke-interface {p1}, Landroid/view/IWindowManager;->thawRotation()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    const/4 v1, 0x1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    const/4 v1, -0x1

    goto :goto_0
.end method
