.class Landroid/app/NativeActivity$InputMethodCallback;
.super Lcom/android/internal/view/IInputMethodCallback$Stub;
.source "NativeActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/NativeActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "InputMethodCallback"
.end annotation


# instance fields
.field mNa:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/NativeActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/app/NativeActivity;)V
    .locals 1
    .param p1    # Landroid/app/NativeActivity;

    invoke-direct {p0}, Lcom/android/internal/view/IInputMethodCallback$Stub;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Landroid/app/NativeActivity$InputMethodCallback;->mNa:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public finishedEvent(IZ)V
    .locals 2
    .param p1    # I
    .param p2    # Z

    iget-object v1, p0, Landroid/app/NativeActivity$InputMethodCallback;->mNa:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NativeActivity;

    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/app/NativeActivity;->access$000(Landroid/app/NativeActivity;)I

    move-result v1

    invoke-static {v0, v1, p1, p2}, Landroid/app/NativeActivity;->access$100(Landroid/app/NativeActivity;IIZ)V

    :cond_0
    return-void
.end method

.method public sessionCreated(Lcom/android/internal/view/IInputMethodSession;)V
    .locals 0
    .param p1    # Lcom/android/internal/view/IInputMethodSession;

    return-void
.end method
