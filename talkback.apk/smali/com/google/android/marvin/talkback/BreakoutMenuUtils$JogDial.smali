.class public abstract Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial;
.super Ljava/lang/Object;
.source "BreakoutMenuUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/BreakoutMenuUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "JogDial"
.end annotation


# instance fields
.field private final mJogListener:Lcom/googlecode/eyesfree/widget/RadialMenuItem$OnMenuItemSelectionListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial$1;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial$1;-><init>(Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial;->mJogListener:Lcom/googlecode/eyesfree/widget/RadialMenuItem$OnMenuItemSelectionListener;

    return-void
.end method


# virtual methods
.method protected abstract onFirstTouch()V
.end method

.method protected abstract onNext()V
.end method

.method protected abstract onPrevious()V
.end method

.method public populateMenu(Lcom/googlecode/eyesfree/widget/RadialMenu;)V
    .locals 4
    .param p1    # Lcom/googlecode/eyesfree/widget/RadialMenu;

    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenu;->clear()V

    const/4 v0, 0x0

    :goto_0
    const/16 v2, 0x10

    if-ge v0, v2, :cond_0

    const/4 v2, 0x0

    const-string v3, ""

    invoke-virtual {p1, v2, v0, v0, v3}, Lcom/googlecode/eyesfree/widget/RadialMenu;->add(IIILjava/lang/CharSequence;)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial;->mJogListener:Lcom/googlecode/eyesfree/widget/RadialMenuItem$OnMenuItemSelectionListener;

    invoke-virtual {v1, v2}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->setOnMenuItemSelectionListener(Lcom/googlecode/eyesfree/widget/RadialMenuItem$OnMenuItemSelectionListener;)Landroid/view/MenuItem;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
