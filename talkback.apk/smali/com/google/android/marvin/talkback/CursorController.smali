.class Lcom/google/android/marvin/talkback/CursorController;
.super Ljava/lang/Object;
.source "CursorController.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/CursorController$CursorControllerListener;
    }
.end annotation


# instance fields
.field private mGranularityManager:Lcom/google/android/marvin/talkback/CursorGranularityManager;

.field private mListener:Lcom/google/android/marvin/talkback/CursorController$CursorControllerListener;

.field private final mNavigateSeenNodes:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;",
            ">;"
        }
    .end annotation
.end field

.field private mReachedEdge:Z

.field private final mService:Landroid/accessibilityservice/AccessibilityService;


# direct methods
.method public constructor <init>(Landroid/accessibilityservice/AccessibilityService;)V
    .locals 1
    .param p1    # Landroid/accessibilityservice/AccessibilityService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/CursorController;->mNavigateSeenNodes:Ljava/util/HashSet;

    iput-object p1, p0, Lcom/google/android/marvin/talkback/CursorController;->mService:Landroid/accessibilityservice/AccessibilityService;

    new-instance v0, Lcom/google/android/marvin/talkback/CursorGranularityManager;

    invoke-direct {v0}, Lcom/google/android/marvin/talkback/CursorGranularityManager;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/CursorController;->mGranularityManager:Lcom/google/android/marvin/talkback/CursorGranularityManager;

    return-void
.end method

.method private adjustGranularity(I)Z
    .locals 7
    .param p1    # I

    const/4 v6, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/CursorController;->getCursor()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-nez v0, :cond_0

    new-array v3, v6, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v3, v2

    invoke-static {v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v1, v2

    :goto_0
    return v1

    :cond_0
    :try_start_1
    iget-object v3, p0, Lcom/google/android/marvin/talkback/CursorController;->mGranularityManager:Lcom/google/android/marvin/talkback/CursorGranularityManager;

    invoke-virtual {v3, v0, p1}, Lcom/google/android/marvin/talkback/CursorGranularityManager;->adjustWithin(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v3, p0, Lcom/google/android/marvin/talkback/CursorController;->mListener:Lcom/google/android/marvin/talkback/CursorController$CursorControllerListener;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/marvin/talkback/CursorController;->mListener:Lcom/google/android/marvin/talkback/CursorController$CursorControllerListener;

    iget-object v4, p0, Lcom/google/android/marvin/talkback/CursorController;->mGranularityManager:Lcom/google/android/marvin/talkback/CursorGranularityManager;

    invoke-virtual {v4}, Lcom/google/android/marvin/talkback/CursorGranularityManager;->getRequestedGranularity()Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v3, v4, v5}, Lcom/google/android/marvin/talkback/CursorController$CursorControllerListener;->onGranularityChanged(Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    new-array v3, v6, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v3, v2

    invoke-static {v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_0

    :catchall_0
    move-exception v3

    new-array v4, v6, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v4, v2

    invoke-static {v4}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    throw v3
.end method

.method private static attemptHtmlNavigation(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Z
    .locals 2
    .param p0    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p1    # I

    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    const/16 v0, 0x400

    :goto_0
    invoke-virtual {p0, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->performAction(I)Z

    move-result v1

    return v1

    :cond_0
    const/16 v0, 0x800

    goto :goto_0
.end method

.method private attemptScrollAction(I)Z
    .locals 7
    .param p1    # I

    const/4 v5, 0x2

    const/4 v6, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/CursorController;->getCursor()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-nez v0, :cond_0

    new-array v4, v5, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v4, v3

    aput-object v2, v4, v6

    invoke-static {v4}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v1, v3

    :goto_0
    return v1

    :cond_0
    :try_start_1
    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/CursorController;->getBestScrollableNode(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    if-nez v2, :cond_1

    new-array v4, v5, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v4, v3

    aput-object v2, v4, v6

    invoke-static {v4}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v1, v3

    goto :goto_0

    :cond_1
    :try_start_2
    invoke-virtual {v2, p1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->performAction(I)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v4, p0, Lcom/google/android/marvin/talkback/CursorController;->mListener:Lcom/google/android/marvin/talkback/CursorController$CursorControllerListener;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/marvin/talkback/CursorController;->mListener:Lcom/google/android/marvin/talkback/CursorController$CursorControllerListener;

    invoke-interface {v4, p1}, Lcom/google/android/marvin/talkback/CursorController$CursorControllerListener;->onActionPerformed(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_2
    new-array v4, v5, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v4, v3

    aput-object v2, v4, v6

    invoke-static {v4}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_0

    :catchall_0
    move-exception v4

    new-array v5, v5, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v5, v3

    aput-object v2, v5, v6

    invoke-static {v5}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    throw v4
.end method

.method private getBestScrollableNode(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 5
    .param p1    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    iget-object v3, p0, Lcom/google/android/marvin/talkback/CursorController;->mService:Landroid/accessibilityservice/AccessibilityService;

    sget-object v4, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->FILTER_SCROLLABLE:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;

    invoke-static {v3, p1, v4}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->getSelfOrMatchingPredecessor(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->getRoot(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/marvin/talkback/CursorController;->mService:Landroid/accessibilityservice/AccessibilityService;

    sget-object v4, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->FILTER_SCROLLABLE:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;

    invoke-static {v3, v1, v4}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->searchFromBfs(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v2

    if-eqz v2, :cond_1

    move-object v0, v2

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static getLastNodeFrom(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 2
    .param p0    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-static {p0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->obtain(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;

    move-result-object v0

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->lastDescendant()Z

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->release()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    return-object v1
.end method

.method private declared-synchronized navigate(I)Z
    .locals 5
    .param p1    # I

    const/4 v2, 0x0

    monitor-enter p0

    const/4 v0, 0x0

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/CursorController;->getCursor()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v3, 0x2

    :try_start_1
    new-array v3, v3, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v4, 0x1

    aput-object v1, v3, v4

    invoke-static {v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    monitor-exit p0

    return v2

    :cond_0
    :try_start_2
    invoke-direct {p0, v0, p1}, Lcom/google/android/marvin/talkback/CursorController;->navigateFrom(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v3, 0x2

    :try_start_3
    new-array v3, v3, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v4, 0x1

    aput-object v1, v3, v4

    invoke-static {v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    :cond_1
    :try_start_4
    invoke-virtual {p0, v1}, Lcom/google/android/marvin/talkback/CursorController;->setCursor(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result v2

    const/4 v3, 0x2

    :try_start_5
    new-array v3, v3, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v4, 0x1

    aput-object v1, v3, v4

    invoke-static {v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_0

    :catchall_1
    move-exception v2

    const/4 v3, 0x2

    new-array v3, v3, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v4, 0x1

    aput-object v1, v3, v4

    invoke-static {v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method private navigateFrom(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 6
    .param p1    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2    # I

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/marvin/talkback/CursorController;->mNavigateSeenNodes:Ljava/util/HashSet;

    invoke-static {v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes(Ljava/util/Collection;)V

    invoke-static {p1, p2}, Lcom/googlecode/eyesfree/utils/NodeFocusFinder;->focusSearch(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/marvin/talkback/CursorController;->mService:Landroid/accessibilityservice/AccessibilityService;

    invoke-static {v1, v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->shouldFocusNode(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/marvin/talkback/CursorController;->mNavigateSeenNodes:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x6

    const-string v2, "Found duplicate during traversal: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getInfo()Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {p0, v1, v2, v3}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/marvin/talkback/CursorController;->mNavigateSeenNodes:Ljava/util/HashSet;

    invoke-static {v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes(Ljava/util/Collection;)V

    goto :goto_0

    :cond_2
    const/4 v1, 0x2

    :try_start_1
    const-string v2, "Search strategy rejected node: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getInfo()Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {p0, v1, v2, v3}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/marvin/talkback/CursorController;->mNavigateSeenNodes:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-static {v0, p2}, Lcom/googlecode/eyesfree/utils/NodeFocusFinder;->focusSearch(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_1

    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/CursorController;->mNavigateSeenNodes:Ljava/util/HashSet;

    invoke-static {v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes(Ljava/util/Collection;)V

    throw v1
.end method

.method private navigateSelfOrFrom(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 1
    .param p1    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2    # I

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/CursorController;->mService:Landroid/accessibilityservice/AccessibilityService;

    invoke-static {v0, p1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->shouldFocusNode(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/google/android/marvin/talkback/CursorController;->navigateFrom(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    goto :goto_0
.end method

.method private navigateWithEdges(IZ)Z
    .locals 3
    .param p1    # I
    .param p2    # Z

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/CursorController;->navigate(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-boolean v2, p0, Lcom/google/android/marvin/talkback/CursorController;->mReachedEdge:Z

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-boolean v1, p0, Lcom/google/android/marvin/talkback/CursorController;->mReachedEdge:Z

    if-eqz v1, :cond_2

    if-eqz p2, :cond_0

    iput-boolean v2, p0, Lcom/google/android/marvin/talkback/CursorController;->mReachedEdge:Z

    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/CursorController;->navigateWrapAround(I)Z

    move-result v0

    goto :goto_0

    :cond_2
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/marvin/talkback/CursorController;->mReachedEdge:Z

    goto :goto_0
.end method

.method private navigateWithGranularity(IZ)Z
    .locals 8
    .param p1    # I
    .param p2    # Z

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/CursorController;->getCursor()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-nez v0, :cond_0

    new-array v4, v5, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v4, v6

    invoke-static {v4}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    :goto_0
    return v6

    :cond_0
    :try_start_1
    iget-object v4, p0, Lcom/google/android/marvin/talkback/CursorController;->mGranularityManager:Lcom/google/android/marvin/talkback/CursorGranularityManager;

    invoke-virtual {v4}, Lcom/google/android/marvin/talkback/CursorGranularityManager;->getRequestedGranularity()Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;

    move-result-object v4

    sget-object v7, Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;->DEFAULT:Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;

    if-eq v4, v7, :cond_3

    if-ne p1, v5, :cond_1

    const/16 v2, 0x100

    :goto_1
    iget-object v4, p0, Lcom/google/android/marvin/talkback/CursorController;->mGranularityManager:Lcom/google/android/marvin/talkback/CursorGranularityManager;

    invoke-virtual {v4, v0, v2}, Lcom/google/android/marvin/talkback/CursorGranularityManager;->navigateWithin(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    if-ne v3, v5, :cond_2

    move v4, v5

    :goto_2
    new-array v5, v5, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v5, v6

    invoke-static {v5}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v6, v4

    goto :goto_0

    :cond_1
    const/16 v2, 0x200

    goto :goto_1

    :cond_2
    move v4, v6

    goto :goto_2

    :cond_3
    :try_start_2
    invoke-static {v0}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->hasWebContent(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-static {v0, p1}, Lcom/google/android/marvin/talkback/CursorController;->attemptHtmlNavigation(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v4

    if-eqz v4, :cond_4

    new-array v4, v5, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v4, v6

    invoke-static {v4}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v6, v5

    goto :goto_0

    :cond_4
    new-array v4, v5, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v4, v6

    invoke-static {v4}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    if-ne p1, v5, :cond_5

    move v1, v5

    :goto_3
    invoke-direct {p0, v1, p2}, Lcom/google/android/marvin/talkback/CursorController;->navigateWithEdges(IZ)Z

    move-result v6

    goto :goto_0

    :catchall_0
    move-exception v4

    new-array v5, v5, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v5, v6

    invoke-static {v5}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    throw v4

    :cond_5
    const/4 v1, -0x1

    goto :goto_3
.end method

.method private navigateWrapAround(I)Z
    .locals 12
    .param p1    # I

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/marvin/talkback/CursorController;->mService:Landroid/accessibilityservice/AccessibilityService;

    invoke-virtual {v6}, Landroid/accessibilityservice/AccessibilityService;->getRootInActiveWindow()Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v2

    if-nez v2, :cond_0

    :goto_0
    return v5

    :cond_0
    const/4 v0, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    :try_start_0
    new-instance v1, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-direct {v1, v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;-><init>(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch p1, :pswitch_data_0

    :goto_1
    :pswitch_0
    if-nez v4, :cond_1

    const/4 v6, 0x6

    :try_start_1
    const-string v7, "Failed to wrap navigation"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {p0, v6, v7, v8}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    new-array v6, v11, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v1, v6, v5

    aput-object v3, v6, v9

    aput-object v4, v6, v10

    invoke-static {v6}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_0

    :pswitch_1
    :try_start_2
    invoke-direct {p0, v1, p1}, Lcom/google/android/marvin/talkback/CursorController;->navigateSelfOrFrom(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v4

    goto :goto_1

    :pswitch_2
    invoke-static {v1}, Lcom/google/android/marvin/talkback/CursorController;->getLastNodeFrom(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v3

    invoke-direct {p0, v3, p1}, Lcom/google/android/marvin/talkback/CursorController;->navigateSelfOrFrom(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v4

    goto :goto_1

    :cond_1
    invoke-virtual {p0, v4}, Lcom/google/android/marvin/talkback/CursorController;->setCursor(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v6

    new-array v7, v11, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v1, v7, v5

    aput-object v3, v7, v9

    aput-object v4, v7, v10

    invoke-static {v7}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v5, v6

    goto :goto_0

    :catchall_0
    move-exception v6

    :goto_2
    new-array v7, v11, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v7, v5

    aput-object v3, v7, v9

    aput-object v4, v7, v10

    invoke-static {v7}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    throw v6

    :catchall_1
    move-exception v6

    move-object v0, v1

    goto :goto_2

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public clearCursor()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/marvin/talkback/CursorController;->mService:Landroid/accessibilityservice/AccessibilityService;

    invoke-virtual {v2}, Landroid/accessibilityservice/AccessibilityService;->getRootInActiveWindow()Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/view/accessibility/AccessibilityNodeInfo;->findFocus(I)Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    const/16 v2, 0x80

    invoke-virtual {v0, v2}, Landroid/view/accessibility/AccessibilityNodeInfo;->performAction(I)Z

    goto :goto_0
.end method

.method public getCursor()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 4

    iget-object v3, p0, Lcom/google/android/marvin/talkback/CursorController;->mService:Landroid/accessibilityservice/AccessibilityService;

    invoke-virtual {v3}, Landroid/accessibilityservice/AccessibilityService;->getRootInActiveWindow()Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v1, 0x0

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    new-instance v1, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-direct {v1, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;-><init>(Ljava/lang/Object;)V

    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->findFocus(I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isVisibleOrLegacy(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    goto :goto_0

    :cond_2
    move-object v1, v2

    goto :goto_0
.end method

.method public less()Z
    .locals 1

    const/16 v0, 0x2000

    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/CursorController;->attemptScrollAction(I)Z

    move-result v0

    return v0
.end method

.method public more()Z
    .locals 1

    const/16 v0, 0x1000

    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/CursorController;->attemptScrollAction(I)Z

    move-result v0

    return v0
.end method

.method public next(Z)Z
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lcom/google/android/marvin/talkback/CursorController;->navigateWithGranularity(IZ)Z

    move-result v0

    return v0
.end method

.method public nextGranularity()Z
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/CursorController;->adjustGranularity(I)Z

    move-result v0

    return v0
.end method

.method public previous(Z)Z
    .locals 1
    .param p1    # Z

    const/4 v0, -0x1

    invoke-direct {p0, v0, p1}, Lcom/google/android/marvin/talkback/CursorController;->navigateWithGranularity(IZ)Z

    move-result v0

    return v0
.end method

.method public previousGranularity()Z
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/CursorController;->adjustGranularity(I)Z

    move-result v0

    return v0
.end method

.method public refocus()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/CursorController;->getCursor()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/CursorController;->clearCursor()V

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/CursorController;->setCursor(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v1

    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    goto :goto_0
.end method

.method public setCursor(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 1
    .param p1    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/16 v0, 0x40

    invoke-virtual {p1, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->performAction(I)Z

    move-result v0

    return v0
.end method

.method public setGranularity(Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;)Z
    .locals 2
    .param p1    # Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/CursorController;->mGranularityManager:Lcom/google/android/marvin/talkback/CursorGranularityManager;

    invoke-virtual {v1, p1}, Lcom/google/android/marvin/talkback/CursorGranularityManager;->requestGranularity(Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/marvin/talkback/CursorController;->mListener:Lcom/google/android/marvin/talkback/CursorController$CursorControllerListener;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/marvin/talkback/CursorController;->mListener:Lcom/google/android/marvin/talkback/CursorController$CursorControllerListener;

    invoke-interface {v1, p1, v0}, Lcom/google/android/marvin/talkback/CursorController$CursorControllerListener;->onGranularityChanged(Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;Z)V

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setListener(Lcom/google/android/marvin/talkback/CursorController$CursorControllerListener;)V
    .locals 0
    .param p1    # Lcom/google/android/marvin/talkback/CursorController$CursorControllerListener;

    iput-object p1, p0, Lcom/google/android/marvin/talkback/CursorController;->mListener:Lcom/google/android/marvin/talkback/CursorController$CursorControllerListener;

    return-void
.end method

.method public shutdown()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/marvin/talkback/CursorController;->mGranularityManager:Lcom/google/android/marvin/talkback/CursorGranularityManager;

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/CursorGranularityManager;->shutdown()V

    return-void
.end method
