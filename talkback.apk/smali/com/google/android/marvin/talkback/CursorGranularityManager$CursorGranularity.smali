.class final enum Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;
.super Ljava/lang/Enum;
.source "CursorGranularityManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/CursorGranularityManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "CursorGranularity"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;

.field public static final enum CHARACTER:Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;

.field public static final enum DEFAULT:Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;

.field public static final enum PARAGRAPH:Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;

.field public static final enum WORD:Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;


# instance fields
.field public final id:I

.field public final resId:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x3

    const/4 v6, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x1

    new-instance v0, Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;

    const-string v1, "DEFAULT"

    const/high16 v2, -0x80000000

    const v3, 0x7f0a00aa

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;->DEFAULT:Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;

    new-instance v0, Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;

    const-string v1, "CHARACTER"

    const v2, 0x7f0a00a7

    invoke-direct {v0, v1, v4, v4, v2}, Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;->CHARACTER:Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;

    new-instance v0, Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;

    const-string v1, "WORD"

    const v2, 0x7f0a00a8

    invoke-direct {v0, v1, v5, v5, v2}, Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;->WORD:Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;

    new-instance v0, Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;

    const-string v1, "PARAGRAPH"

    const/16 v2, 0x8

    const v3, 0x7f0a00a9

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;->PARAGRAPH:Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;

    sget-object v1, Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;->DEFAULT:Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;->CHARACTER:Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;->WORD:Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;->PARAGRAPH:Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;

    aput-object v1, v0, v7

    sput-object v0, Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;->$VALUES:[Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3    # I
    .param p4    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;->id:I

    iput p4, p0, Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;->resId:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;

    return-object v0
.end method

.method public static values()[Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;
    .locals 1

    sget-object v0, Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;->$VALUES:[Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;

    invoke-virtual {v0}, [Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;

    return-object v0
.end method
