.class Lcom/google/android/marvin/talkback/FullScreenReadController$1;
.super Ljava/lang/Object;
.source "FullScreenReadController.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/marvin/talkback/FullScreenReadController;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/FullScreenReadController;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/FullScreenReadController;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/marvin/talkback/FullScreenReadController$1;->this$0:Lcom/google/android/marvin/talkback/FullScreenReadController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/marvin/talkback/FullScreenReadController$1;->this$0:Lcom/google/android/marvin/talkback/FullScreenReadController;

    # getter for: Lcom/google/android/marvin/talkback/FullScreenReadController;->mCurrentState:Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/FullScreenReadController;->access$000(Lcom/google/android/marvin/talkback/FullScreenReadController;)Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

    move-result-object v0

    sget-object v1, Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;->STOPPED:Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/FullScreenReadController$1;->this$0:Lcom/google/android/marvin/talkback/FullScreenReadController;

    # getter for: Lcom/google/android/marvin/talkback/FullScreenReadController;->mCurrentState:Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/FullScreenReadController;->access$000(Lcom/google/android/marvin/talkback/FullScreenReadController;)Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

    move-result-object v0

    sget-object v1, Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;->ENTERING_WEB_CONTENT:Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/FullScreenReadController$1;->this$0:Lcom/google/android/marvin/talkback/FullScreenReadController;

    # invokes: Lcom/google/android/marvin/talkback/FullScreenReadController;->moveForward()V
    invoke-static {v0}, Lcom/google/android/marvin/talkback/FullScreenReadController;->access$100(Lcom/google/android/marvin/talkback/FullScreenReadController;)V

    :cond_0
    return-void
.end method
