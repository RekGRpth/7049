.class public Lcom/google/android/marvin/talkback/ProcessorEventQueue;
.super Ljava/lang/Object;
.source "ProcessorEventQueue.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/TalkBackService$EventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/ProcessorEventQueue$ProcessorEventHandler;
    }
.end annotation


# instance fields
.field private final mEventQueue:Lcom/google/android/marvin/talkback/EventQueue;

.field private mEventSpeechRuleProcessor:Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;

.field private final mFeedbackController:Lcom/google/android/marvin/talkback/PreferenceFeedbackController;

.field private final mHandler:Lcom/google/android/marvin/talkback/ProcessorEventQueue$ProcessorEventHandler;

.field private mLastEventType:I

.field private mLastWindowStateChanged:J

.field private final mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

.field private mTestingListener:Lcom/google/android/marvin/talkback/test/TalkBackListener;


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 2
    .param p1    # Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/marvin/talkback/ProcessorEventQueue$ProcessorEventHandler;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/ProcessorEventQueue$ProcessorEventHandler;-><init>(Lcom/google/android/marvin/talkback/ProcessorEventQueue;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mHandler:Lcom/google/android/marvin/talkback/ProcessorEventQueue$ProcessorEventHandler;

    new-instance v0, Lcom/google/android/marvin/talkback/EventQueue;

    invoke-direct {v0}, Lcom/google/android/marvin/talkback/EventQueue;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mEventQueue:Lcom/google/android/marvin/talkback/EventQueue;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mLastWindowStateChanged:J

    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getFeedbackController()Lcom/google/android/marvin/talkback/PreferenceFeedbackController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mFeedbackController:Lcom/google/android/marvin/talkback/PreferenceFeedbackController;

    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getSpeechController()Lcom/google/android/marvin/talkback/SpeechController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    new-instance v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;

    invoke-direct {v0, p1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mEventSpeechRuleProcessor:Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->loadDefaultRules()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/marvin/talkback/ProcessorEventQueue;)Lcom/google/android/marvin/talkback/EventQueue;
    .locals 1
    .param p0    # Lcom/google/android/marvin/talkback/ProcessorEventQueue;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mEventQueue:Lcom/google/android/marvin/talkback/EventQueue;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/marvin/talkback/ProcessorEventQueue;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 0
    .param p0    # Lcom/google/android/marvin/talkback/ProcessorEventQueue;
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;

    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->processAndRecycleEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    return-void
.end method

.method private computeQueuingMode(Lcom/google/android/marvin/talkback/Utterance;Landroid/view/accessibility/AccessibilityEvent;)I
    .locals 7
    .param p1    # Lcom/google/android/marvin/talkback/Utterance;
    .param p2    # Landroid/view/accessibility/AccessibilityEvent;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/Utterance;->getMetadata()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v3

    and-int/lit16 v3, v3, 0x100c

    if-eqz v3, :cond_1

    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getEventTime()J

    move-result-wide v3

    iget-wide v5, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mLastWindowStateChanged:J

    sub-long/2addr v3, v5

    const-wide/16 v5, 0x64

    cmp-long v3, v3, v5

    if-gez v3, :cond_1

    const/4 v2, 0x1

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget v3, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mLastEventType:I

    if-eq v3, v0, :cond_0

    iput v0, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mLastEventType:I

    const-string v3, "queuing"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    goto :goto_0
.end method

.method private loadDefaultRules()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mEventSpeechRuleProcessor:Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;

    const v1, 0x7f050002

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->addSpeechStrategy(I)V

    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mEventSpeechRuleProcessor:Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;

    const v1, 0x7f050007

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->addSpeechStrategy(I)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mEventSpeechRuleProcessor:Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;

    const v1, 0x7f05000a

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->addSpeechStrategy(I)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mEventSpeechRuleProcessor:Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;

    const v1, 0x7f050001

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->addSpeechStrategy(I)V

    return-void

    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mEventSpeechRuleProcessor:Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;

    const v1, 0x7f050009

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->addSpeechStrategy(I)V

    goto :goto_0

    :cond_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mEventSpeechRuleProcessor:Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;

    const v1, 0x7f050008

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->addSpeechStrategy(I)V

    goto :goto_0

    :cond_3
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_4

    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mEventSpeechRuleProcessor:Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;

    const v1, 0x7f050006

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->addSpeechStrategy(I)V

    goto :goto_0

    :cond_4
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-lt v0, v1, :cond_5

    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mEventSpeechRuleProcessor:Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;

    const v1, 0x7f050005

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->addSpeechStrategy(I)V

    goto :goto_0

    :cond_5
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x5

    if-lt v0, v1, :cond_6

    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mEventSpeechRuleProcessor:Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;

    const v1, 0x7f050004

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->addSpeechStrategy(I)V

    goto :goto_0

    :cond_6
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x4

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mEventSpeechRuleProcessor:Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;

    const v1, 0x7f050003

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->addSpeechStrategy(I)V

    goto :goto_0
.end method

.method private processAndRecycleEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 5
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;

    const/4 v4, 0x0

    const/4 v1, 0x3

    const-string v2, "Processing event: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-static {p0, v1, v2, v3}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    invoke-static {}, Lcom/google/android/marvin/talkback/Utterance;->obtain()Lcom/google/android/marvin/talkback/Utterance;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mEventSpeechRuleProcessor:Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->processEvent(Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/Utterance;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x5

    const-string v2, "Failed to process event"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {p0, v1, v2, v3}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/Utterance;->recycle()V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mTestingListener:Lcom/google/android/marvin/talkback/test/TalkBackListener;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mTestingListener:Lcom/google/android/marvin/talkback/test/TalkBackListener;

    invoke-interface {v1, v0}, Lcom/google/android/marvin/talkback/test/TalkBackListener;->onUtteranceQueued(Lcom/google/android/marvin/talkback/Utterance;)V

    :cond_1
    invoke-direct {p0, p1, v0}, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->provideFeedbackForUtterance(Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/Utterance;)V

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/Utterance;->recycle()V

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->recycle()V

    goto :goto_0
.end method

.method private provideFeedbackForUtterance(Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/Utterance;)V
    .locals 10
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2    # Lcom/google/android/marvin/talkback/Utterance;

    const/high16 v9, 0x3f800000

    invoke-virtual {p2}, Lcom/google/android/marvin/talkback/Utterance;->getMetadata()Landroid/os/Bundle;

    move-result-object v4

    const-string v8, "earcon_rate"

    invoke-virtual {v4, v8, v9}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;F)F

    move-result v0

    const-string v8, "earcon_volume"

    invoke-virtual {v4, v8, v9}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;F)F

    move-result v1

    invoke-virtual {p2}, Lcom/google/android/marvin/talkback/Utterance;->getCustomEarcons()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v8, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mFeedbackController:Lcom/google/android/marvin/talkback/PreferenceFeedbackController;

    invoke-virtual {v8, v3, v0, v1}, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;->playCustomSound(IFF)V

    goto :goto_0

    :cond_0
    invoke-virtual {p2}, Lcom/google/android/marvin/talkback/Utterance;->getCustomVibrations()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v8, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mFeedbackController:Lcom/google/android/marvin/talkback/PreferenceFeedbackController;

    invoke-virtual {v8, v3}, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;->playCustomVibration(I)V

    goto :goto_1

    :cond_1
    invoke-virtual {p2}, Lcom/google/android/marvin/talkback/Utterance;->getText()Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    if-lez v8, :cond_2

    const-string v8, "speech_params"

    invoke-virtual {v4, v8}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v5

    invoke-direct {p0, p2, p1}, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->computeQueuingMode(Lcom/google/android/marvin/talkback/Utterance;Landroid/view/accessibility/AccessibilityEvent;)I

    move-result v6

    iget-object v8, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    const/4 v9, 0x0

    invoke-virtual {v8, v7, v6, v9, v5}, Lcom/google/android/marvin/talkback/SpeechController;->speak(Ljava/lang/CharSequence;IILandroid/os/Bundle;)V

    :cond_2
    return-void
.end method


# virtual methods
.method public process(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 3
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mLastWindowStateChanged:J

    :cond_0
    iget-object v2, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mEventQueue:Lcom/google/android/marvin/talkback/EventQueue;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mEventQueue:Lcom/google/android/marvin/talkback/EventQueue;

    invoke-virtual {v1, p1}, Lcom/google/android/marvin/talkback/EventQueue;->enqueue(Landroid/view/accessibility/AccessibilityEvent;)V

    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mHandler:Lcom/google/android/marvin/talkback/ProcessorEventQueue$ProcessorEventHandler;

    invoke-virtual {v1, p1}, Lcom/google/android/marvin/talkback/ProcessorEventQueue$ProcessorEventHandler;->postSpeak(Landroid/view/accessibility/AccessibilityEvent;)V

    monitor-exit v2

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public setTestingListener(Lcom/google/android/marvin/talkback/test/TalkBackListener;)V
    .locals 0
    .param p1    # Lcom/google/android/marvin/talkback/test/TalkBackListener;

    iput-object p1, p0, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->mTestingListener:Lcom/google/android/marvin/talkback/test/TalkBackListener;

    return-void
.end method
