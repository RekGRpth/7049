.class Lcom/google/android/marvin/talkback/ProcessorFollowFocus$FollowFocusHandler;
.super Lcom/googlecode/eyesfree/utils/WeakReferenceHandler;
.source "ProcessorFollowFocus.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/ProcessorFollowFocus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FollowFocusHandler"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/googlecode/eyesfree/utils/WeakReferenceHandler",
        "<",
        "Lcom/google/android/marvin/talkback/ProcessorFollowFocus;",
        ">;"
    }
.end annotation


# instance fields
.field private mCachedContentRecord:Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

.field private mCachedScrollNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/ProcessorFollowFocus;)V
    .locals 0
    .param p1    # Lcom/google/android/marvin/talkback/ProcessorFollowFocus;

    invoke-direct {p0, p1}, Lcom/googlecode/eyesfree/utils/WeakReferenceHandler;-><init>(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public followContentChangedDelayed(Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;)V
    .locals 3
    .param p1    # Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    const/4 v2, 0x2

    invoke-virtual {p0, v2}, Lcom/google/android/marvin/talkback/ProcessorFollowFocus$FollowFocusHandler;->removeMessages(I)V

    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus$FollowFocusHandler;->mCachedContentRecord:Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus$FollowFocusHandler;->mCachedContentRecord:Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    invoke-virtual {v1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->recycle()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus$FollowFocusHandler;->mCachedContentRecord:Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    :cond_0
    invoke-static {p1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->obtain(Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;)Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus$FollowFocusHandler;->mCachedContentRecord:Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    invoke-virtual {p0, v2}, Lcom/google/android/marvin/talkback/ProcessorFollowFocus$FollowFocusHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    const-wide/16 v1, 0x1f4

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/marvin/talkback/ProcessorFollowFocus$FollowFocusHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method

.method public followScrollDelayed(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Z)V
    .locals 3
    .param p1    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2    # Z

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/google/android/marvin/talkback/ProcessorFollowFocus$FollowFocusHandler;->removeMessages(I)V

    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus$FollowFocusHandler;->mCachedScrollNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus$FollowFocusHandler;->mCachedScrollNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus$FollowFocusHandler;->mCachedScrollNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    :cond_0
    invoke-static {p1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus$FollowFocusHandler;->mCachedScrollNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v2, v1}, Lcom/google/android/marvin/talkback/ProcessorFollowFocus$FollowFocusHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    const-wide/16 v1, 0xfa

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/marvin/talkback/ProcessorFollowFocus$FollowFocusHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method

.method public handleMessage(Landroid/os/Message;Lcom/google/android/marvin/talkback/ProcessorFollowFocus;)V
    .locals 3
    .param p1    # Landroid/os/Message;
    .param p2    # Lcom/google/android/marvin/talkback/ProcessorFollowFocus;

    const/4 v2, 0x0

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus$FollowFocusHandler;->mCachedScrollNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    # invokes: Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->followScrollEvent(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Z)Z
    invoke-static {p2, v1, v0}, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->access$000(Lcom/google/android/marvin/talkback/ProcessorFollowFocus;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Z)Z

    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus$FollowFocusHandler;->mCachedScrollNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus$FollowFocusHandler;->mCachedScrollNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    iput-object v2, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus$FollowFocusHandler;->mCachedScrollNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus$FollowFocusHandler;->mCachedContentRecord:Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    # invokes: Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->followContentChangedEvent(Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;)Z
    invoke-static {p2, v0}, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->access$100(Lcom/google/android/marvin/talkback/ProcessorFollowFocus;Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;)Z

    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus$FollowFocusHandler;->mCachedContentRecord:Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus$FollowFocusHandler;->mCachedContentRecord:Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->recycle()V

    iput-object v2, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus$FollowFocusHandler;->mCachedContentRecord:Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic handleMessage(Landroid/os/Message;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Landroid/os/Message;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/marvin/talkback/ProcessorFollowFocus$FollowFocusHandler;->handleMessage(Landroid/os/Message;Lcom/google/android/marvin/talkback/ProcessorFollowFocus;)V

    return-void
.end method

.method public interruptFollowDelayed()V
    .locals 1

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/ProcessorFollowFocus$FollowFocusHandler;->removeMessages(I)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/ProcessorFollowFocus$FollowFocusHandler;->removeMessages(I)V

    return-void
.end method
