.class Lcom/google/android/marvin/talkback/ProcessorFollowFocus;
.super Ljava/lang/Object;
.source "ProcessorFollowFocus.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/TalkBackService$EventListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/ProcessorFollowFocus$FollowFocusHandler;
    }
.end annotation


# instance fields
.field private final mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

.field private final mCursorController:Lcom/google/android/marvin/talkback/CursorController;

.field private final mHandler:Lcom/google/android/marvin/talkback/ProcessorFollowFocus$FollowFocusHandler;

.field private mLastScrollAction:I

.field private mLastScrollFromIndex:I

.field private mLastScrollSource:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

.field private mLastScrollToIndex:I

.field private final mService:Lcom/google/android/marvin/talkback/TalkBackService;


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 2
    .param p1    # Lcom/google/android/marvin/talkback/TalkBackService;

    const/4 v1, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->mLastScrollAction:I

    iput v1, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->mLastScrollFromIndex:I

    iput v1, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->mLastScrollToIndex:I

    new-instance v0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus$FollowFocusHandler;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/ProcessorFollowFocus$FollowFocusHandler;-><init>(Lcom/google/android/marvin/talkback/ProcessorFollowFocus;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->mHandler:Lcom/google/android/marvin/talkback/ProcessorFollowFocus$FollowFocusHandler;

    iput-object p1, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getCursorController()Lcom/google/android/marvin/talkback/CursorController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    const-string v0, "accessibility"

    invoke-virtual {p1, v0}, Lcom/google/android/marvin/talkback/TalkBackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/marvin/talkback/ProcessorFollowFocus;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Z)Z
    .locals 1
    .param p0    # Lcom/google/android/marvin/talkback/ProcessorFollowFocus;
    .param p1    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2    # Z

    invoke-direct {p0, p1, p2}, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->followScrollEvent(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Z)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/marvin/talkback/ProcessorFollowFocus;Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;)Z
    .locals 1
    .param p0    # Lcom/google/android/marvin/talkback/ProcessorFollowFocus;
    .param p1    # Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->followContentChangedEvent(Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;)Z

    move-result v0

    return v0
.end method

.method private ensureFocusConsistency()Z
    .locals 14

    const/4 v13, 0x4

    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v6, 0x0

    const/4 v7, 0x1

    iget-object v8, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v8}, Lcom/google/android/marvin/talkback/TalkBackService;->getRootInActiveWindow()Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v5

    if-nez v5, :cond_0

    :goto_0
    return v6

    :cond_0
    const/4 v0, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v2, 0x0

    :try_start_0
    new-instance v1, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-direct {v1, v5}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;-><init>(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v8, 0x2

    :try_start_1
    invoke-virtual {v1, v8}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->findFocus(I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v3

    if-eqz v3, :cond_2

    iget-object v8, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-static {v8, v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->shouldFocusNode(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v8

    if-eqz v8, :cond_1

    new-array v8, v13, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v1, v8, v6

    aput-object v3, v8, v7

    aput-object v4, v8, v11

    aput-object v2, v8, v12

    invoke-static {v8}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v6, v7

    goto :goto_0

    :cond_1
    const/4 v8, 0x6

    :try_start_2
    const-string v9, "Clearing old focus"

    const/4 v10, 0x0

    new-array v10, v10, [Ljava/lang/Object;

    invoke-static {v8, v9, v10}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(ILjava/lang/String;[Ljava/lang/Object;)V

    const/16 v8, 0x80

    invoke-virtual {v3, v8}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->performAction(I)Z

    :cond_2
    const/4 v8, 0x1

    invoke-virtual {v1, v8}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->findFocus(I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->tryFocusing(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v8

    if-eqz v8, :cond_3

    new-array v8, v13, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v1, v8, v6

    aput-object v3, v8, v7

    aput-object v4, v8, v11

    aput-object v2, v8, v12

    invoke-static {v8}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v6, v7

    goto :goto_0

    :cond_3
    const/4 v8, 0x1

    :try_start_3
    invoke-direct {p0, v1, v8}, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->findFocusFromNode(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->tryFocusing(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v8

    if-eqz v8, :cond_4

    new-array v8, v13, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v1, v8, v6

    aput-object v3, v8, v7

    aput-object v4, v8, v11

    aput-object v2, v8, v12

    invoke-static {v8}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v6, v7

    goto :goto_0

    :cond_4
    const/4 v8, 0x6

    :try_start_4
    const-string v9, "Failed to place focus from new window"

    const/4 v10, 0x0

    new-array v10, v10, [Ljava/lang/Object;

    invoke-static {v8, v9, v10}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(ILjava/lang/String;[Ljava/lang/Object;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    new-array v8, v13, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v1, v8, v6

    aput-object v3, v8, v7

    aput-object v4, v8, v11

    aput-object v2, v8, v12

    invoke-static {v8}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_0

    :catchall_0
    move-exception v8

    :goto_1
    new-array v9, v13, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v9, v6

    aput-object v3, v9, v7

    aput-object v4, v9, v11

    aput-object v2, v9, v12

    invoke-static {v9}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    throw v8

    :catchall_1
    move-exception v8

    move-object v0, v1

    goto :goto_1
.end method

.method private findChildFromNode(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 7
    .param p1    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2    # I

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getChildCount()I

    move-result v1

    if-nez v1, :cond_1

    move-object v0, v5

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const/4 v6, 0x1

    if-ne p2, v6, :cond_2

    const/4 v3, 0x1

    const/4 v4, 0x0

    :goto_1
    move v2, v4

    :goto_2
    if-ltz v2, :cond_4

    if-ge v2, v1, :cond_4

    invoke-virtual {p1, v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getChild(I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    if-nez v0, :cond_3

    :goto_3
    add-int/2addr v2, v3

    goto :goto_2

    :cond_2
    const/4 v3, -0x1

    add-int/lit8 v4, v1, -0x1

    goto :goto_1

    :cond_3
    iget-object v6, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-static {v6, v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->shouldFocusNode(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    goto :goto_3

    :cond_4
    move-object v0, v5

    goto :goto_0
.end method

.method private findFocusFromNode(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 3
    .param p1    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2    # I

    invoke-static {p1, p2}, Lcom/googlecode/eyesfree/utils/NodeFocusFinder;->focusSearch(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-static {v2, v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->shouldFocusNode(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-static {v0, p2}, Lcom/googlecode/eyesfree/utils/NodeFocusFinder;->focusSearch(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-static {v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes(Ljava/util/Collection;)V

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :cond_1
    return-object v0
.end method

.method private followContentChangedEvent(Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;)Z
    .locals 1
    .param p1    # Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->ensureFocusConsistency()Z

    move-result v0

    return v0
.end method

.method private followScrollEvent(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Z)Z
    .locals 8
    .param p1    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2    # Z

    const/4 v7, 0x2

    const/4 v4, 0x0

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v6}, Lcom/google/android/marvin/talkback/TalkBackService;->getRootInActiveWindow()Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v3

    if-nez v3, :cond_0

    :goto_0
    return v4

    :cond_0
    const/4 v0, 0x0

    const/4 v2, 0x0

    :try_start_0
    new-instance v1, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-direct {v1, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;-><init>(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v6, 0x2

    :try_start_1
    invoke-virtual {v1, v6}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->findFocus(I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v6, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-static {v6, v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->shouldFocusNode(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v6

    if-eqz v6, :cond_1

    new-array v6, v7, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v1, v6, v4

    aput-object v2, v6, v5

    invoke-static {v6}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v4, v5

    goto :goto_0

    :cond_1
    const/16 v6, 0x80

    :try_start_2
    invoke-virtual {v2, v6}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->performAction(I)Z

    :cond_2
    invoke-direct {p0, p1, p2}, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->tryFocusingChild(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Z)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v6

    if-eqz v6, :cond_3

    new-array v6, v7, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v1, v6, v4

    aput-object v2, v6, v5

    invoke-static {v6}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v4, v5

    goto :goto_0

    :cond_3
    :try_start_3
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->tryFocusing(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v6

    if-eqz v6, :cond_4

    new-array v6, v7, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v1, v6, v4

    aput-object v2, v6, v5

    invoke-static {v6}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v4, v5

    goto :goto_0

    :cond_4
    new-array v6, v7, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v1, v6, v4

    aput-object v2, v6, v5

    invoke-static {v6}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_0

    :catchall_0
    move-exception v6

    :goto_1
    new-array v7, v7, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v7, v4

    aput-object v2, v7, v5

    invoke-static {v7}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    throw v6

    :catchall_1
    move-exception v6

    move-object v0, v1

    goto :goto_1
.end method

.method private handleContentChangedEvent(Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;)V
    .locals 2
    .param p1    # Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    invoke-virtual {p1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getSource()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->mHandler:Lcom/google/android/marvin/talkback/ProcessorFollowFocus$FollowFocusHandler;

    invoke-virtual {v1, p1}, Lcom/google/android/marvin/talkback/ProcessorFollowFocus$FollowFocusHandler;->followContentChangedDelayed(Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;)V

    goto :goto_0
.end method

.method private handleScrollEvent(Landroid/view/accessibility/AccessibilityEvent;Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;)V
    .locals 5
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2    # Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    const/4 v2, 0x0

    invoke-virtual {p2}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getSource()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v3, 0x6

    const-string v4, "Drop scroll with no source node"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0, v3, v4, v2}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    iget-object v3, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->mLastScrollSource:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v1, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget v3, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->mLastScrollAction:I

    const/16 v4, 0x1000

    if-eq v3, v4, :cond_1

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getFromIndex()I

    move-result v3

    iget v4, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->mLastScrollFromIndex:I

    if-gt v3, v4, :cond_1

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getToIndex()I

    move-result v3

    iget v4, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->mLastScrollToIndex:I

    if-le v3, v4, :cond_4

    :cond_1
    const/4 v0, 0x1

    :goto_1
    iget-object v3, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->mHandler:Lcom/google/android/marvin/talkback/ProcessorFollowFocus$FollowFocusHandler;

    invoke-virtual {v3, v1, v0}, Lcom/google/android/marvin/talkback/ProcessorFollowFocus$FollowFocusHandler;->followScrollDelayed(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Z)V

    :cond_2
    iget-object v3, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->mLastScrollSource:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->mLastScrollSource:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    :cond_3
    iput-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->mLastScrollSource:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {p2}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getFromIndex()I

    move-result v3

    iput v3, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->mLastScrollFromIndex:I

    invoke-virtual {p2}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getToIndex()I

    move-result v3

    iput v3, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->mLastScrollToIndex:I

    iput v2, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->mLastScrollAction:I

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method private setFocusFromHover(Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;)Z
    .locals 6
    .param p1    # Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    const/4 v4, 0x2

    const/4 v5, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getSource()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-nez v1, :cond_0

    new-array v3, v4, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v1, v3, v2

    aput-object v0, v3, v5

    invoke-static {v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    :goto_0
    return v2

    :cond_0
    :try_start_1
    iget-object v3, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-static {v3, v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->findFocusFromHover(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    if-nez v0, :cond_1

    new-array v3, v4, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v1, v3, v2

    aput-object v0, v3, v5

    invoke-static {v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_0

    :cond_1
    :try_start_2
    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->tryFocusing(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v3

    new-array v4, v4, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v1, v4, v2

    aput-object v0, v4, v5

    invoke-static {v4}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v2, v3

    goto :goto_0

    :catchall_0
    move-exception v3

    new-array v4, v4, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v1, v4, v2

    aput-object v0, v4, v5

    invoke-static {v4}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    throw v3
.end method

.method private setFocusFromInput(Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;)Z
    .locals 4
    .param p1    # Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getSource()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-nez v0, :cond_0

    new-array v2, v3, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v2, v1

    invoke-static {v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    :goto_0
    return v1

    :cond_0
    :try_start_1
    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->tryFocusing(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    new-array v3, v3, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v3, v1

    invoke-static {v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v1, v2

    goto :goto_0

    :catchall_0
    move-exception v2

    new-array v3, v3, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v3, v1

    invoke-static {v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    throw v2
.end method

.method private setFocusFromSelection(Landroid/view/accessibility/AccessibilityEvent;Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;)Z
    .locals 8
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2    # Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p2}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getSource()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    if-nez v2, :cond_0

    new-array v4, v7, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v2, v4, v3

    aput-object v0, v4, v6

    invoke-static {v4}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    :goto_0
    return v3

    :cond_0
    :try_start_1
    invoke-virtual {p2}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getCurrentItemIndex()I

    move-result v4

    invoke-virtual {p2}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getFromIndex()I

    move-result v5

    sub-int v1, v4, v5

    if-ltz v1, :cond_1

    invoke-virtual {v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getChildCount()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v4

    if-lt v1, v4, :cond_2

    :cond_1
    new-array v4, v7, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v2, v4, v3

    aput-object v0, v4, v6

    invoke-static {v4}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_0

    :cond_2
    :try_start_2
    invoke-virtual {v2, v1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getChild(I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    if-nez v0, :cond_3

    new-array v4, v7, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v2, v4, v3

    aput-object v0, v4, v6

    invoke-static {v4}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_0

    :cond_3
    :try_start_3
    iget-object v4, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-static {v4, v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isTopLevelScrollItem(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v4

    if-nez v4, :cond_4

    new-array v4, v7, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v2, v4, v3

    aput-object v0, v4, v6

    invoke-static {v4}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_0

    :cond_4
    :try_start_4
    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->tryFocusing(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v4

    new-array v5, v7, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v2, v5, v3

    aput-object v0, v5, v6

    invoke-static {v5}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v3, v4

    goto :goto_0

    :catchall_0
    move-exception v4

    new-array v5, v7, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v2, v5, v3

    aput-object v0, v5, v6

    invoke-static {v5}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    throw v4
.end method

.method private tryFocusing(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 2
    .param p1    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-static {v1, p1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->shouldFocusNode(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x40

    invoke-virtual {p1, v1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->performAction(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->mHandler:Lcom/google/android/marvin/talkback/ProcessorFollowFocus$FollowFocusHandler;

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/ProcessorFollowFocus$FollowFocusHandler;->interruptFollowDelayed()V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private tryFocusingChild(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Z)Z
    .locals 5
    .param p1    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2    # Z

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz p2, :cond_0

    move v1, v2

    :goto_0
    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, p1, v1}, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->findChildFromNode(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-nez v0, :cond_1

    new-array v2, v2, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v2, v3

    invoke-static {v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v2, v3

    :goto_1
    return v2

    :cond_0
    const/4 v1, -0x1

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->tryFocusing(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v4

    new-array v2, v2, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v2, v3

    invoke-static {v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v2, v4

    goto :goto_1

    :catchall_0
    move-exception v4

    new-array v2, v2, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v2, v3

    invoke-static {v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    throw v4
.end method


# virtual methods
.method public onActionPerformed(I)V
    .locals 0
    .param p1    # I

    sparse-switch p1, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    iput p1, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->mLastScrollAction:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1000 -> :sswitch_0
        0x2000 -> :sswitch_0
    .end sparse-switch
.end method

.method public process(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 3
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;

    const/4 v2, -0x1

    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    invoke-direct {v0, p1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;-><init>(Ljava/lang/Object;)V

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    invoke-direct {p0, p1, v0}, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->setFocusFromSelection(Landroid/view/accessibility/AccessibilityEvent;Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;)Z

    goto :goto_0

    :sswitch_1
    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->setFocusFromInput(Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;)Z

    goto :goto_0

    :sswitch_2
    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->mLastScrollSource:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->mLastScrollSource:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->mLastScrollSource:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    :cond_1
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->mLastScrollAction:I

    iput v2, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->mLastScrollFromIndex:I

    iput v2, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->mLastScrollToIndex:I

    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/CursorController;->clearCursor()V

    goto :goto_0

    :sswitch_3
    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->handleContentChangedEvent(Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;)V

    goto :goto_0

    :sswitch_4
    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->setFocusFromHover(Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;)Z

    goto :goto_0

    :sswitch_5
    invoke-direct {p0, p1, v0}, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->handleScrollEvent(Landroid/view/accessibility/AccessibilityEvent;Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x8 -> :sswitch_1
        0x20 -> :sswitch_2
        0x80 -> :sswitch_4
        0x800 -> :sswitch_3
        0x1000 -> :sswitch_5
    .end sparse-switch
.end method
