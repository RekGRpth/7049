.class Lcom/google/android/marvin/talkback/ProcessorGestureVibrator;
.super Ljava/lang/Object;
.source "ProcessorGestureVibrator.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/TalkBackService$EventListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x11
.end annotation


# instance fields
.field private final mCursorController:Lcom/google/android/marvin/talkback/CursorController;

.field private final mFeedbackController:Lcom/google/android/marvin/talkback/PreferenceFeedbackController;

.field private final mFeedbackRunnable:Ljava/lang/Runnable;

.field private final mHandler:Landroid/os/Handler;

.field private final mService:Lcom/google/android/marvin/talkback/TalkBackService;


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 1
    .param p1    # Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/marvin/talkback/ProcessorGestureVibrator;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getFeedbackController()Lcom/google/android/marvin/talkback/PreferenceFeedbackController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorGestureVibrator;->mFeedbackController:Lcom/google/android/marvin/talkback/PreferenceFeedbackController;

    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getCursorController()Lcom/google/android/marvin/talkback/CursorController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorGestureVibrator;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorGestureVibrator;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/marvin/talkback/ProcessorGestureVibrator$1;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/ProcessorGestureVibrator$1;-><init>(Lcom/google/android/marvin/talkback/ProcessorGestureVibrator;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorGestureVibrator;->mFeedbackRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/marvin/talkback/ProcessorGestureVibrator;)Lcom/google/android/marvin/talkback/PreferenceFeedbackController;
    .locals 1
    .param p0    # Lcom/google/android/marvin/talkback/ProcessorGestureVibrator;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorGestureVibrator;->mFeedbackController:Lcom/google/android/marvin/talkback/PreferenceFeedbackController;

    return-object v0
.end method


# virtual methods
.method public process(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 5
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorGestureVibrator;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/CursorController;->getCursor()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    invoke-static {}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->isTutorialActive()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorGestureVibrator;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const-class v2, Landroid/webkit/WebView;

    invoke-static {v1, v0, v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->nodeMatchesClassByType(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Ljava/lang/Class;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorGestureVibrator;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/TalkBackService;->interruptAllFeedback()V

    goto :goto_0

    :sswitch_1
    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorGestureVibrator;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/ProcessorGestureVibrator;->mFeedbackRunnable:Ljava/lang/Runnable;

    const-wide/16 v3, 0x46

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :sswitch_2
    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorGestureVibrator;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/ProcessorGestureVibrator;->mFeedbackRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorGestureVibrator;->mFeedbackController:Lcom/google/android/marvin/talkback/PreferenceFeedbackController;

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;->cancelVibration()V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x40000 -> :sswitch_1
        0x80000 -> :sswitch_2
        0x100000 -> :sswitch_0
    .end sparse-switch
.end method
