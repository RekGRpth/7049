.class Lcom/google/android/marvin/talkback/ProcessorScrollPosition$ScrollPositionHandler;
.super Lcom/googlecode/eyesfree/utils/WeakReferenceHandler;
.source "ProcessorScrollPosition.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/ProcessorScrollPosition;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ScrollPositionHandler"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/googlecode/eyesfree/utils/WeakReferenceHandler",
        "<",
        "Lcom/google/android/marvin/talkback/ProcessorScrollPosition;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/ProcessorScrollPosition;)V
    .locals 0
    .param p1    # Lcom/google/android/marvin/talkback/ProcessorScrollPosition;

    invoke-direct {p0, p1}, Lcom/googlecode/eyesfree/utils/WeakReferenceHandler;-><init>(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/marvin/talkback/ProcessorScrollPosition$ScrollPositionHandler;)V
    .locals 0
    .param p0    # Lcom/google/android/marvin/talkback/ProcessorScrollPosition$ScrollPositionHandler;

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/ProcessorScrollPosition$ScrollPositionHandler;->cancelSeekFeedback()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/marvin/talkback/ProcessorScrollPosition$ScrollPositionHandler;)V
    .locals 0
    .param p0    # Lcom/google/android/marvin/talkback/ProcessorScrollPosition$ScrollPositionHandler;

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/ProcessorScrollPosition$ScrollPositionHandler;->cancelScrollFeedback()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/marvin/talkback/ProcessorScrollPosition$ScrollPositionHandler;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 0
    .param p0    # Lcom/google/android/marvin/talkback/ProcessorScrollPosition$ScrollPositionHandler;
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;

    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/ProcessorScrollPosition$ScrollPositionHandler;->postScrollFeedback(Landroid/view/accessibility/AccessibilityEvent;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/marvin/talkback/ProcessorScrollPosition$ScrollPositionHandler;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 0
    .param p0    # Lcom/google/android/marvin/talkback/ProcessorScrollPosition$ScrollPositionHandler;
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;

    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/ProcessorScrollPosition$ScrollPositionHandler;->postSeekFeedback(Landroid/view/accessibility/AccessibilityEvent;)V

    return-void
.end method

.method private cancelScrollFeedback()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/ProcessorScrollPosition$ScrollPositionHandler;->removeMessages(I)V

    return-void
.end method

.method private cancelSeekFeedback()V
    .locals 1

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/ProcessorScrollPosition$ScrollPositionHandler;->removeMessages(I)V

    return-void
.end method

.method private postScrollFeedback(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 4
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;

    invoke-static {p1}, Lcom/googlecode/eyesfree/compat/view/accessibility/AccessibilityEventCompatUtils;->obtain(Landroid/view/accessibility/AccessibilityEvent;)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {p0, v2, v0}, Lcom/google/android/marvin/talkback/ProcessorScrollPosition$ScrollPositionHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x3e8

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/marvin/talkback/ProcessorScrollPosition$ScrollPositionHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method

.method private postSeekFeedback(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 4
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;

    invoke-static {p1}, Lcom/googlecode/eyesfree/compat/view/accessibility/AccessibilityEventCompatUtils;->obtain(Landroid/view/accessibility/AccessibilityEvent;)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    const/4 v2, 0x2

    invoke-virtual {p0, v2, v0}, Lcom/google/android/marvin/talkback/ProcessorScrollPosition$ScrollPositionHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x3e8

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/marvin/talkback/ProcessorScrollPosition$ScrollPositionHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;Lcom/google/android/marvin/talkback/ProcessorScrollPosition;)V
    .locals 2
    .param p1    # Landroid/os/Message;
    .param p2    # Lcom/google/android/marvin/talkback/ProcessorScrollPosition;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/view/accessibility/AccessibilityEvent;

    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->recycle()V

    return-void

    :pswitch_0
    # invokes: Lcom/google/android/marvin/talkback/ProcessorScrollPosition;->handleScrollFeedback(Landroid/view/accessibility/AccessibilityEvent;)V
    invoke-static {p2, v0}, Lcom/google/android/marvin/talkback/ProcessorScrollPosition;->access$400(Lcom/google/android/marvin/talkback/ProcessorScrollPosition;Landroid/view/accessibility/AccessibilityEvent;)V

    goto :goto_0

    :pswitch_1
    # invokes: Lcom/google/android/marvin/talkback/ProcessorScrollPosition;->handleSeekFeedback(Landroid/view/accessibility/AccessibilityEvent;)V
    invoke-static {p2, v0}, Lcom/google/android/marvin/talkback/ProcessorScrollPosition;->access$500(Lcom/google/android/marvin/talkback/ProcessorScrollPosition;Landroid/view/accessibility/AccessibilityEvent;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic handleMessage(Landroid/os/Message;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Landroid/os/Message;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Lcom/google/android/marvin/talkback/ProcessorScrollPosition;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/marvin/talkback/ProcessorScrollPosition$ScrollPositionHandler;->handleMessage(Landroid/os/Message;Lcom/google/android/marvin/talkback/ProcessorScrollPosition;)V

    return-void
.end method
