.class public Lcom/google/android/marvin/talkback/ProcessorSingleTap;
.super Ljava/lang/Object;
.source "ProcessorSingleTap.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/TalkBackService$EventListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x11
.end annotation


# static fields
.field private static final mHasFocusFilter:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mEnabled:Z

.field private mFirstFocusedItem:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

.field private final mJumpTapTimeout:J

.field private mTouchStartTime:J

.field private mTouchedItemCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/marvin/talkback/ProcessorSingleTap$1;

    invoke-direct {v0}, Lcom/google/android/marvin/talkback/ProcessorSingleTap$1;-><init>()V

    sput-object v0, Lcom/google/android/marvin/talkback/ProcessorSingleTap;->mHasFocusFilter:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/marvin/talkback/ProcessorSingleTap;->mContext:Landroid/content/Context;

    invoke-static {}, Landroid/view/ViewConfiguration;->getJumpTapTimeout()I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/android/marvin/talkback/ProcessorSingleTap;->mJumpTapTimeout:J

    return-void
.end method

.method private onHoverEnter(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 6
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;

    new-instance v3, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    invoke-direct {v3, p1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v3}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getSource()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v4, p0, Lcom/google/android/marvin/talkback/ProcessorSingleTap;->mContext:Landroid/content/Context;

    sget-object v5, Lcom/google/android/marvin/talkback/ProcessorSingleTap;->mHasFocusFilter:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;

    invoke-static {v4, v2, v5}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->getSelfOrMatchingPredecessor(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/marvin/talkback/ProcessorSingleTap;->mFirstFocusedItem:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-eqz v4, :cond_2

    if-eqz v1, :cond_4

    iget-object v4, p0, Lcom/google/android/marvin/talkback/ProcessorSingleTap;->mFirstFocusedItem:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v4, v1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    :cond_2
    const/4 v0, 0x1

    :goto_1
    iget-object v4, p0, Lcom/google/android/marvin/talkback/ProcessorSingleTap;->mFirstFocusedItem:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-nez v4, :cond_3

    iput-object v1, p0, Lcom/google/android/marvin/talkback/ProcessorSingleTap;->mFirstFocusedItem:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    :cond_3
    if-eqz v0, :cond_0

    iget v4, p0, Lcom/google/android/marvin/talkback/ProcessorSingleTap;->mTouchedItemCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/android/marvin/talkback/ProcessorSingleTap;->mTouchedItemCount:I

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private onTouchEnd(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 6
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/ProcessorSingleTap;->mFirstFocusedItem:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventTime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/marvin/talkback/ProcessorSingleTap;->mTouchStartTime:J

    sub-long v0, v2, v4

    iget v2, p0, Lcom/google/android/marvin/talkback/ProcessorSingleTap;->mTouchedItemCount:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    iget-wide v2, p0, Lcom/google/android/marvin/talkback/ProcessorSingleTap;->mJumpTapTimeout:J

    cmp-long v2, v0, v2

    if-gez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/ProcessorSingleTap;->mFirstFocusedItem:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-direct {p0, v2}, Lcom/google/android/marvin/talkback/ProcessorSingleTap;->performClick(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    :cond_1
    iget-object v2, p0, Lcom/google/android/marvin/talkback/ProcessorSingleTap;->mFirstFocusedItem:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/marvin/talkback/ProcessorSingleTap;->mFirstFocusedItem:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    goto :goto_0
.end method

.method private onTouchStart(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorSingleTap;->mFirstFocusedItem:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorSingleTap;->mFirstFocusedItem:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorSingleTap;->mFirstFocusedItem:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/marvin/talkback/ProcessorSingleTap;->mTouchedItemCount:I

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/marvin/talkback/ProcessorSingleTap;->mTouchStartTime:J

    return-void
.end method

.method private performClick(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    .locals 5
    .param p1    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/google/android/marvin/talkback/ProcessorSingleTap;->mContext:Landroid/content/Context;

    new-array v1, v4, [Ljava/lang/Class;

    const/4 v2, 0x0

    const-class v3, Landroid/widget/EditText;

    aput-object v3, v1, v2

    invoke-static {v0, p1, v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->nodeMatchesAnyClassByType(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;[Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1, v4}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->performAction(I)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p1}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->hasWebContent(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x10

    invoke-virtual {p1, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->performAction(I)Z

    goto :goto_0
.end method


# virtual methods
.method public process(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;

    iget-boolean v1, p0, Lcom/google/android/marvin/talkback/ProcessorSingleTap;->mEnabled:Z

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    const v1, 0x300080

    and-int/2addr v1, v0

    if-eqz v1, :cond_0

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/ProcessorSingleTap;->onHoverEnter(Landroid/view/accessibility/AccessibilityEvent;)V

    goto :goto_0

    :sswitch_1
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/ProcessorSingleTap;->onTouchStart(Landroid/view/accessibility/AccessibilityEvent;)V

    goto :goto_0

    :sswitch_2
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/ProcessorSingleTap;->onTouchEnd(Landroid/view/accessibility/AccessibilityEvent;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x80 -> :sswitch_0
        0x100000 -> :sswitch_1
        0x200000 -> :sswitch_2
    .end sparse-switch
.end method

.method public setEnabled(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/marvin/talkback/ProcessorSingleTap;->mEnabled:Z

    return-void
.end method
