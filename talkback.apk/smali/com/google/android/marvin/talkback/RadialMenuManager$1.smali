.class Lcom/google/android/marvin/talkback/RadialMenuManager$1;
.super Ljava/lang/Object;
.source "RadialMenuManager.java"

# interfaces
.implements Lcom/googlecode/eyesfree/widget/RadialMenuItem$OnMenuItemSelectionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/RadialMenuManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/RadialMenuManager;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$1;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuItemSelection(Lcom/googlecode/eyesfree/widget/RadialMenuItem;)Z
    .locals 7
    .param p1    # Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$1;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    # getter for: Lcom/google/android/marvin/talkback/RadialMenuManager;->mHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/google/android/marvin/talkback/RadialMenuManager;->access$100(Lcom/google/android/marvin/talkback/RadialMenuManager;)Landroid/os/Handler;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$1;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    # getter for: Lcom/google/android/marvin/talkback/RadialMenuManager;->mRadialMenuHint:Ljava/lang/Runnable;
    invoke-static {v5}, Lcom/google/android/marvin/talkback/RadialMenuManager;->access$000(Lcom/google/android/marvin/talkback/RadialMenuManager;)Ljava/lang/Runnable;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v4, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$1;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    # getter for: Lcom/google/android/marvin/talkback/RadialMenuManager;->mFeedbackController:Lcom/google/android/marvin/talkback/PreferenceFeedbackController;
    invoke-static {v4}, Lcom/google/android/marvin/talkback/RadialMenuManager;->access$200(Lcom/google/android/marvin/talkback/RadialMenuManager;)Lcom/google/android/marvin/talkback/PreferenceFeedbackController;

    move-result-object v4

    const v5, 0x7f080034

    invoke-virtual {v4, v5}, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;->playCustomVibration(I)V

    iget-object v4, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$1;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    # getter for: Lcom/google/android/marvin/talkback/RadialMenuManager;->mFeedbackController:Lcom/google/android/marvin/talkback/PreferenceFeedbackController;
    invoke-static {v4}, Lcom/google/android/marvin/talkback/RadialMenuManager;->access$200(Lcom/google/android/marvin/talkback/RadialMenuManager;)Lcom/google/android/marvin/talkback/PreferenceFeedbackController;

    move-result-object v4

    const v5, 0x7f080023

    invoke-virtual {v4, v5}, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;->playCustomSound(I)V

    iget-object v4, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$1;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    # getter for: Lcom/google/android/marvin/talkback/RadialMenuManager;->mClient:Lcom/google/android/marvin/talkback/RadialMenuManager$RadialMenuClient;
    invoke-static {v4}, Lcom/google/android/marvin/talkback/RadialMenuManager;->access$300(Lcom/google/android/marvin/talkback/RadialMenuManager;)Lcom/google/android/marvin/talkback/RadialMenuManager$RadialMenuClient;

    move-result-object v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$1;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    # getter for: Lcom/google/android/marvin/talkback/RadialMenuManager;->mClient:Lcom/google/android/marvin/talkback/RadialMenuManager$RadialMenuClient;
    invoke-static {v4}, Lcom/google/android/marvin/talkback/RadialMenuManager;->access$300(Lcom/google/android/marvin/talkback/RadialMenuManager;)Lcom/google/android/marvin/talkback/RadialMenuManager$RadialMenuClient;

    move-result-object v4

    invoke-interface {v4, p1}, Lcom/google/android/marvin/talkback/RadialMenuManager$RadialMenuClient;->onMenuItemHovered(Landroid/view/MenuItem;)Z

    move-result v4

    if-eqz v4, :cond_1

    move v0, v2

    :goto_0
    if-nez v0, :cond_0

    if-nez p1, :cond_2

    iget-object v4, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$1;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    # getter for: Lcom/google/android/marvin/talkback/RadialMenuManager;->mService:Lcom/google/android/marvin/talkback/TalkBackService;
    invoke-static {v4}, Lcom/google/android/marvin/talkback/RadialMenuManager;->access$400(Lcom/google/android/marvin/talkback/RadialMenuManager;)Lcom/google/android/marvin/talkback/TalkBackService;

    move-result-object v4

    const/high16 v5, 0x1040000

    invoke-virtual {v4, v5}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_1
    iget-object v4, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$1;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    # getter for: Lcom/google/android/marvin/talkback/RadialMenuManager;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;
    invoke-static {v4}, Lcom/google/android/marvin/talkback/RadialMenuManager;->access$500(Lcom/google/android/marvin/talkback/RadialMenuManager;)Lcom/google/android/marvin/talkback/SpeechController;

    move-result-object v4

    const/4 v5, 0x2

    const/4 v6, 0x0

    invoke-virtual {v4, v1, v3, v5, v6}, Lcom/google/android/marvin/talkback/SpeechController;->speak(Ljava/lang/CharSequence;IILandroid/os/Bundle;)V

    :cond_0
    return v2

    :cond_1
    move v0, v3

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_1
.end method
