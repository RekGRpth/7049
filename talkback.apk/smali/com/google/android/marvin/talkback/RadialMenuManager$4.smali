.class Lcom/google/android/marvin/talkback/RadialMenuManager$4;
.super Ljava/lang/Object;
.source "RadialMenuManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/RadialMenuManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/RadialMenuManager;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$4;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$4;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    # getter for: Lcom/google/android/marvin/talkback/RadialMenuManager;->mService:Lcom/google/android/marvin/talkback/TalkBackService;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/RadialMenuManager;->access$400(Lcom/google/android/marvin/talkback/RadialMenuManager;)Lcom/google/android/marvin/talkback/TalkBackService;

    move-result-object v0

    const v3, 0x7f0a0075

    invoke-virtual {v0, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$4;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    # setter for: Lcom/google/android/marvin/talkback/RadialMenuManager;->mHintSpeechPending:Z
    invoke-static {v0, v2}, Lcom/google/android/marvin/talkback/RadialMenuManager;->access$802(Lcom/google/android/marvin/talkback/RadialMenuManager;Z)Z

    iget-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$4;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    # getter for: Lcom/google/android/marvin/talkback/RadialMenuManager;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/RadialMenuManager;->access$500(Lcom/google/android/marvin/talkback/RadialMenuManager;)Lcom/google/android/marvin/talkback/SpeechController;

    move-result-object v0

    const/4 v3, 0x2

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$4;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    # getter for: Lcom/google/android/marvin/talkback/RadialMenuManager;->mHintSpeechCompleted:Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;
    invoke-static {v5}, Lcom/google/android/marvin/talkback/RadialMenuManager;->access$900(Lcom/google/android/marvin/talkback/RadialMenuManager;)Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/marvin/talkback/SpeechController;->speak(Ljava/lang/CharSequence;IILandroid/os/Bundle;Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;)V

    return-void
.end method
