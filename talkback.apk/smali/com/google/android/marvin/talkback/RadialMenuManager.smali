.class Lcom/google/android/marvin/talkback/RadialMenuManager;
.super Landroid/content/BroadcastReceiver;
.source "RadialMenuManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/RadialMenuManager$RadialMenuClient;
    }
.end annotation


# instance fields
.field private final mCachedRadialMenus:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;",
            ">;"
        }
    .end annotation
.end field

.field private mClient:Lcom/google/android/marvin/talkback/RadialMenuManager$RadialMenuClient;

.field private final mFeedbackController:Lcom/google/android/marvin/talkback/PreferenceFeedbackController;

.field private final mHandler:Landroid/os/Handler;

.field private final mHintSpeechCompleted:Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

.field private mHintSpeechPending:Z

.field private mIsRadialMenuShowing:I

.field private final mOnClick:Landroid/view/MenuItem$OnMenuItemClickListener;

.field private final mOnSelection:Lcom/googlecode/eyesfree/widget/RadialMenuItem$OnMenuItemSelectionListener;

.field private final mOverlayListener:Lcom/googlecode/eyesfree/widget/SimpleOverlay$SimpleOverlayListener;

.field private final mRadialMenuHint:Ljava/lang/Runnable;

.field private final mScreenOffFilter:Landroid/content/IntentFilter;

.field private final mService:Lcom/google/android/marvin/talkback/TalkBackService;

.field private final mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 2
    .param p1    # Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mCachedRadialMenus:Landroid/util/SparseArray;

    new-instance v0, Lcom/google/android/marvin/talkback/RadialMenuManager$1;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/RadialMenuManager$1;-><init>(Lcom/google/android/marvin/talkback/RadialMenuManager;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mOnSelection:Lcom/googlecode/eyesfree/widget/RadialMenuItem$OnMenuItemSelectionListener;

    new-instance v0, Lcom/google/android/marvin/talkback/RadialMenuManager$2;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/RadialMenuManager$2;-><init>(Lcom/google/android/marvin/talkback/RadialMenuManager;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mOnClick:Landroid/view/MenuItem$OnMenuItemClickListener;

    new-instance v0, Lcom/google/android/marvin/talkback/RadialMenuManager$3;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/RadialMenuManager$3;-><init>(Lcom/google/android/marvin/talkback/RadialMenuManager;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mOverlayListener:Lcom/googlecode/eyesfree/widget/SimpleOverlay$SimpleOverlayListener;

    new-instance v0, Lcom/google/android/marvin/talkback/RadialMenuManager$4;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/RadialMenuManager$4;-><init>(Lcom/google/android/marvin/talkback/RadialMenuManager;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mRadialMenuHint:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/marvin/talkback/RadialMenuManager$5;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/RadialMenuManager$5;-><init>(Lcom/google/android/marvin/talkback/RadialMenuManager;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mHintSpeechCompleted:Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mHandler:Landroid/os/Handler;

    iput-object p1, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getSpeechController()Lcom/google/android/marvin/talkback/SpeechController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getFeedbackController()Lcom/google/android/marvin/talkback/PreferenceFeedbackController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mFeedbackController:Lcom/google/android/marvin/talkback/PreferenceFeedbackController;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mScreenOffFilter:Landroid/content/IntentFilter;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/marvin/talkback/RadialMenuManager;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/google/android/marvin/talkback/RadialMenuManager;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mRadialMenuHint:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/marvin/talkback/RadialMenuManager;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/google/android/marvin/talkback/RadialMenuManager;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/marvin/talkback/RadialMenuManager;)Lcom/google/android/marvin/talkback/PreferenceFeedbackController;
    .locals 1
    .param p0    # Lcom/google/android/marvin/talkback/RadialMenuManager;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mFeedbackController:Lcom/google/android/marvin/talkback/PreferenceFeedbackController;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/marvin/talkback/RadialMenuManager;)Lcom/google/android/marvin/talkback/RadialMenuManager$RadialMenuClient;
    .locals 1
    .param p0    # Lcom/google/android/marvin/talkback/RadialMenuManager;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mClient:Lcom/google/android/marvin/talkback/RadialMenuManager$RadialMenuClient;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/marvin/talkback/RadialMenuManager;)Lcom/google/android/marvin/talkback/TalkBackService;
    .locals 1
    .param p0    # Lcom/google/android/marvin/talkback/RadialMenuManager;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/marvin/talkback/RadialMenuManager;)Lcom/google/android/marvin/talkback/SpeechController;
    .locals 1
    .param p0    # Lcom/google/android/marvin/talkback/RadialMenuManager;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/marvin/talkback/RadialMenuManager;Landroid/view/Menu;)V
    .locals 0
    .param p0    # Lcom/google/android/marvin/talkback/RadialMenuManager;
    .param p1    # Landroid/view/Menu;

    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/RadialMenuManager;->playScaleForMenu(Landroid/view/Menu;)V

    return-void
.end method

.method static synthetic access$708(Lcom/google/android/marvin/talkback/RadialMenuManager;)I
    .locals 2
    .param p0    # Lcom/google/android/marvin/talkback/RadialMenuManager;

    iget v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mIsRadialMenuShowing:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mIsRadialMenuShowing:I

    return v0
.end method

.method static synthetic access$710(Lcom/google/android/marvin/talkback/RadialMenuManager;)I
    .locals 2
    .param p0    # Lcom/google/android/marvin/talkback/RadialMenuManager;

    iget v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mIsRadialMenuShowing:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mIsRadialMenuShowing:I

    return v0
.end method

.method static synthetic access$800(Lcom/google/android/marvin/talkback/RadialMenuManager;)Z
    .locals 1
    .param p0    # Lcom/google/android/marvin/talkback/RadialMenuManager;

    iget-boolean v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mHintSpeechPending:Z

    return v0
.end method

.method static synthetic access$802(Lcom/google/android/marvin/talkback/RadialMenuManager;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/marvin/talkback/RadialMenuManager;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mHintSpeechPending:Z

    return p1
.end method

.method static synthetic access$900(Lcom/google/android/marvin/talkback/RadialMenuManager;)Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;
    .locals 1
    .param p0    # Lcom/google/android/marvin/talkback/RadialMenuManager;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mHintSpeechCompleted:Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    return-object v0
.end method

.method private playScaleForMenu(Landroid/view/Menu;)V
    .locals 10
    .param p1    # Landroid/view/Menu;

    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v7

    if-gtz v7, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x4

    if-gt v7, v0, :cond_1

    :goto_1
    add-int/lit8 v5, v7, 0x1

    const-wide/high16 v0, 0x4049000000000000L

    int-to-double v8, v5

    invoke-static {v8, v9}, Ljava/lang/Math;->log(D)D

    move-result-wide v8

    mul-double/2addr v0, v8

    double-to-int v0, v0

    add-int/lit8 v0, v0, 0x64

    div-int v3, v0, v5

    iget-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mFeedbackController:Lcom/google/android/marvin/talkback/PreferenceFeedbackController;

    const/16 v1, 0x73

    const/16 v2, 0x69

    const/16 v4, 0x33

    const/4 v6, 0x3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;->playMidiScale(IIIIII)Z

    goto :goto_0

    :cond_1
    div-int/lit8 v0, v7, 0x4

    add-int/lit8 v7, v0, 0x3

    goto :goto_1
.end method


# virtual methods
.method public clearCache()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mCachedRadialMenus:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    return-void
.end method

.method public dismissAll()V
    .locals 3

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mCachedRadialMenus:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mCachedRadialMenus:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;

    invoke-virtual {v1}, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;->isVisible()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;->dismiss()V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public getFilter()Landroid/content/IntentFilter;
    .locals 1

    iget-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mScreenOffFilter:Landroid/content/IntentFilter;

    return-object v0
.end method

.method public isRadialMenuShowing()Z
    .locals 1

    iget v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mIsRadialMenuShowing:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const-string v0, "android.intent.action.SCREEN_OFF"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/RadialMenuManager;->dismissAll()V

    :cond_0
    return-void
.end method

.method public setClient(Lcom/google/android/marvin/talkback/RadialMenuManager$RadialMenuClient;)V
    .locals 0
    .param p1    # Lcom/google/android/marvin/talkback/RadialMenuManager$RadialMenuClient;

    iput-object p1, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mClient:Lcom/google/android/marvin/talkback/RadialMenuManager$RadialMenuClient;

    return-void
.end method

.method public showRadialMenu(I)Z
    .locals 7
    .param p1    # I

    const/4 v4, 0x0

    invoke-static {}, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;->isTutorialActive()Z

    move-result v5

    if-eqz v5, :cond_0

    :goto_0
    return v4

    :cond_0
    iget-object v5, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mCachedRadialMenus:Landroid/util/SparseArray;

    invoke-virtual {v5, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;

    if-nez v1, :cond_2

    new-instance v1, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;

    iget-object v5, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-direct {v1, v5, v4}, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;-><init>(Landroid/content/Context;Z)V

    iget-object v5, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mOverlayListener:Lcom/googlecode/eyesfree/widget/SimpleOverlay$SimpleOverlayListener;

    invoke-virtual {v1, v5}, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;->setListener(Lcom/googlecode/eyesfree/widget/SimpleOverlay$SimpleOverlayListener;)V

    invoke-virtual {v1}, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;->getParams()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    const/16 v5, 0x7da

    iput v5, v2, Landroid/view/WindowManager$LayoutParams;->type:I

    iget v5, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit8 v5, v5, 0x8

    iput v5, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    invoke-virtual {v1, v2}, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;->setParams(Landroid/view/WindowManager$LayoutParams;)V

    invoke-virtual {v1}, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;->getMenu()Lcom/googlecode/eyesfree/widget/RadialMenu;

    move-result-object v0

    iget-object v5, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mOnSelection:Lcom/googlecode/eyesfree/widget/RadialMenuItem$OnMenuItemSelectionListener;

    invoke-virtual {v0, v5}, Lcom/googlecode/eyesfree/widget/RadialMenu;->setDefaultSelectionListener(Lcom/googlecode/eyesfree/widget/RadialMenuItem$OnMenuItemSelectionListener;)V

    iget-object v5, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mOnClick:Landroid/view/MenuItem$OnMenuItemClickListener;

    invoke-virtual {v0, v5}, Lcom/googlecode/eyesfree/widget/RadialMenu;->setDefaultListener(Landroid/view/MenuItem$OnMenuItemClickListener;)V

    invoke-virtual {v1}, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;->getView()Lcom/googlecode/eyesfree/widget/RadialMenuView;

    move-result-object v3

    sget-object v5, Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;->LIFT_TO_ACTIVATE:Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;

    invoke-virtual {v3, v5}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->setSubMenuMode(Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;)V

    iget-object v5, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mClient:Lcom/google/android/marvin/talkback/RadialMenuManager$RadialMenuClient;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mClient:Lcom/google/android/marvin/talkback/RadialMenuManager$RadialMenuClient;

    invoke-interface {v5, p1, v0}, Lcom/google/android/marvin/talkback/RadialMenuManager$RadialMenuClient;->onCreateRadialMenu(ILcom/googlecode/eyesfree/widget/RadialMenu;)V

    :cond_1
    iget-object v5, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mCachedRadialMenus:Landroid/util/SparseArray;

    invoke-virtual {v5, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_2
    iget-object v5, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mClient:Lcom/google/android/marvin/talkback/RadialMenuManager$RadialMenuClient;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mClient:Lcom/google/android/marvin/talkback/RadialMenuManager$RadialMenuClient;

    invoke-virtual {v1}, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;->getMenu()Lcom/googlecode/eyesfree/widget/RadialMenu;

    move-result-object v6

    invoke-interface {v5, p1, v6}, Lcom/google/android/marvin/talkback/RadialMenuManager$RadialMenuClient;->onPrepareRadialMenu(ILcom/googlecode/eyesfree/widget/RadialMenu;)Z

    move-result v5

    if-nez v5, :cond_3

    iget-object v5, p0, Lcom/google/android/marvin/talkback/RadialMenuManager;->mFeedbackController:Lcom/google/android/marvin/talkback/PreferenceFeedbackController;

    const v6, 0x7f08002e

    invoke-virtual {v5, v6}, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;->playCustomSound(I)V

    goto :goto_0

    :cond_3
    invoke-virtual {v1}, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;->showWithDot()V

    const/4 v4, 0x1

    goto :goto_0
.end method
