.class public Lcom/google/android/marvin/talkback/ShakeDetector;
.super Ljava/lang/Object;
.source "ShakeDetector.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# instance fields
.field private final mAccelerometer:Landroid/hardware/Sensor;

.field private final mContext:Lcom/google/android/marvin/talkback/TalkBackService;

.field private final mFullScreenRead:Lcom/google/android/marvin/talkback/FullScreenReadController;

.field private mIsActive:Z

.field private mIsFeatureEnabled:Z

.field private mLastEventValues:[F

.field private mLastSensorUpdate:J

.field private final mSensorManager:Landroid/hardware/SensorManager;


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 2
    .param p1    # Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getFullScreenReadController()Lcom/google/android/marvin/talkback/FullScreenReadController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mFullScreenRead:Lcom/google/android/marvin/talkback/FullScreenReadController;

    const-string v0, "sensor"

    invoke-virtual {p1, v0}, Lcom/google/android/marvin/talkback/TalkBackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mAccelerometer:Landroid/hardware/Sensor;

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1    # Landroid/hardware/Sensor;
    .param p2    # I

    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 11
    .param p1    # Landroid/hardware/SensorEvent;

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mLastSensorUpdate:J

    sub-long v0, v4, v6

    long-to-float v6, v0

    const/high16 v7, 0x43480000

    cmpl-float v6, v6, v7

    if-lez v6, :cond_0

    iget-object v6, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v6, v6, v8

    iget-object v7, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v7, v7, v9

    add-float/2addr v6, v7

    iget-object v7, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v7, v7, v10

    add-float/2addr v6, v7

    iget-object v7, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mLastEventValues:[F

    aget v7, v7, v8

    sub-float/2addr v6, v7

    iget-object v7, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mLastEventValues:[F

    aget v7, v7, v9

    sub-float/2addr v6, v7

    iget-object v7, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mLastEventValues:[F

    aget v7, v7, v10

    sub-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v2

    long-to-float v6, v0

    div-float v6, v2, v6

    const v7, 0x461c4000

    mul-float v3, v6, v7

    iput-wide v4, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mLastSensorUpdate:J

    iget-object v6, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {v6}, [F->clone()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [F

    iput-object v6, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mLastEventValues:[F

    const/high16 v6, 0x442f0000

    cmpl-float v6, v3, v6

    if-lez v6, :cond_0

    iget-object v6, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mFullScreenRead:Lcom/google/android/marvin/talkback/FullScreenReadController;

    invoke-virtual {v6}, Lcom/google/android/marvin/talkback/FullScreenReadController;->getReadingState()Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

    move-result-object v6

    sget-object v7, Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;->STOPPED:Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

    if-ne v6, v7, :cond_0

    iget-object v6, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mFullScreenRead:Lcom/google/android/marvin/talkback/FullScreenReadController;

    invoke-virtual {v6}, Lcom/google/android/marvin/talkback/FullScreenReadController;->startReadingFromNextNode()Z

    :cond_0
    return-void
.end method

.method public pausePolling()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mIsActive:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mIsActive:Z

    goto :goto_0
.end method

.method public resumePolling()V
    .locals 3

    const/4 v2, 0x3

    iget-boolean v0, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mIsFeatureEnabled:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mIsActive:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mLastSensorUpdate:J

    new-array v0, v2, [F

    iput-object v0, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mLastEventValues:[F

    iget-object v0, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mAccelerometer:Landroid/hardware/Sensor;

    invoke-virtual {v0, p0, v1, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mIsActive:Z

    goto :goto_0
.end method

.method public setEnabled(Z)V
    .locals 3
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mIsFeatureEnabled:Z

    if-eqz p1, :cond_1

    iget-object v1, p0, Lcom/google/android/marvin/talkback/ShakeDetector;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    const-string v2, "power"

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/ShakeDetector;->resumePolling()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/ShakeDetector;->pausePolling()V

    goto :goto_0
.end method
