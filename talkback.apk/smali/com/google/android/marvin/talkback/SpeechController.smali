.class public Lcom/google/android/marvin/talkback/SpeechController;
.super Ljava/lang/Object;
.source "SpeechController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;,
        Lcom/google/android/marvin/talkback/SpeechController$CompletionRunner;,
        Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteAction;,
        Lcom/google/android/marvin/talkback/SpeechController$SpeechControllerListener;,
        Lcom/google/android/marvin/talkback/SpeechController$SpeechItem;
    }
.end annotation


# instance fields
.field private final mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private final mAudioManager:Landroid/media/AudioManager;

.field private mCurrentSpeechItem:Lcom/google/android/marvin/talkback/SpeechController$SpeechItem;

.field private mExpectedRingerVolume:I

.field private final mFailoverTts:Lcom/google/android/marvin/utils/FailoverTextToSpeech;

.field private final mFailoverTtsListener:Lcom/google/android/marvin/utils/FailoverTextToSpeech$FailoverTtsListener;

.field private mFullScreenReadNextCallback:Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

.field private final mHandler:Landroid/os/Handler;

.field private mInjectFullScreenReadCallbacks:Z

.field private mIsRingerLowered:Z

.field private mIsSpeaking:Z

.field private mLastUtteranceSpeech:Ljava/lang/CharSequence;

.field private mNextUtteranceIndex:I

.field private final mPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field private final mProximityChangeListener:Lcom/google/android/marvin/utils/ProximitySensor$ProximityChangeListener;

.field private mProximitySensor:Lcom/google/android/marvin/utils/ProximitySensor;

.field private mRequestedProximityState:Z

.field private mRestoreRingerVolume:I

.field private mScreenIsOn:Z

.field private final mService:Lcom/google/android/marvin/talkback/TalkBackService;

.field private mSilenceOnProximity:Z

.field private mSpeechListener:Lcom/google/android/marvin/talkback/SpeechController$SpeechControllerListener;

.field private final mSpeechParametersMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mSpeechQueue:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/android/marvin/talkback/SpeechController$SpeechItem;",
            ">;"
        }
    .end annotation
.end field

.field private mSpeechVolume:I

.field private final mTelephonyManager:Landroid/telephony/TelephonyManager;

.field private mTtsOverlay:Lcom/google/android/marvin/talkback/TextToSpeechOverlay;

.field private mUseAudioFocus:Z

.field private mUseIntonation:Z

.field private final mUtteranceCompleteActions:Ljava/util/PriorityQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/PriorityQueue",
            "<",
            "Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteAction;",
            ">;"
        }
    .end annotation
.end field

.field private mVolumeMonitor:Lcom/google/android/marvin/talkback/VolumeMonitor;


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 4
    .param p1    # Lcom/google/android/marvin/talkback/TalkBackService;

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mSpeechParametersMap:Ljava/util/HashMap;

    new-instance v1, Ljava/util/PriorityQueue;

    invoke-direct {v1}, Ljava/util/PriorityQueue;-><init>()V

    iput-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mUtteranceCompleteActions:Ljava/util/PriorityQueue;

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mSpeechQueue:Ljava/util/LinkedList;

    const-string v1, ""

    iput-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mLastUtteranceSpeech:Ljava/lang/CharSequence;

    iput v3, p0, Lcom/google/android/marvin/talkback/SpeechController;->mNextUtteranceIndex:I

    new-instance v1, Lcom/google/android/marvin/talkback/SpeechController$1;

    invoke-direct {v1, p0}, Lcom/google/android/marvin/talkback/SpeechController$1;-><init>(Lcom/google/android/marvin/talkback/SpeechController;)V

    iput-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mProximityChangeListener:Lcom/google/android/marvin/utils/ProximitySensor$ProximityChangeListener;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/marvin/talkback/SpeechController$2;

    invoke-direct {v1, p0}, Lcom/google/android/marvin/talkback/SpeechController$2;-><init>(Lcom/google/android/marvin/talkback/SpeechController;)V

    iput-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    new-instance v1, Lcom/google/android/marvin/talkback/SpeechController$3;

    invoke-direct {v1, p0}, Lcom/google/android/marvin/talkback/SpeechController$3;-><init>(Lcom/google/android/marvin/talkback/SpeechController;)V

    iput-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mFailoverTtsListener:Lcom/google/android/marvin/utils/FailoverTextToSpeech$FailoverTtsListener;

    new-instance v1, Lcom/google/android/marvin/talkback/SpeechController$4;

    invoke-direct {v1, p0}, Lcom/google/android/marvin/talkback/SpeechController$4;-><init>(Lcom/google/android/marvin/talkback/SpeechController;)V

    iput-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    iput-object p1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const-string v2, "audio"

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    iput-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const-string v2, "phone"

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    iput-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    new-instance v1, Lcom/google/android/marvin/utils/FailoverTextToSpeech;

    invoke-direct {v1, p1}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mFailoverTts:Lcom/google/android/marvin/utils/FailoverTextToSpeech;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mFailoverTts:Lcom/google/android/marvin/utils/FailoverTextToSpeech;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/SpeechController;->mFailoverTtsListener:Lcom/google/android/marvin/utils/FailoverTextToSpeech$FailoverTtsListener;

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->setListener(Lcom/google/android/marvin/utils/FailoverTextToSpeech$FailoverTtsListener;)V

    iput-boolean v3, p0, Lcom/google/android/marvin/talkback/SpeechController;->mInjectFullScreenReadCallbacks:Z

    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/SpeechController;->reloadPreferences(Landroid/content/SharedPreferences;)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mScreenIsOn:Z

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/marvin/talkback/SpeechController;)Lcom/google/android/marvin/talkback/TalkBackService;
    .locals 1
    .param p0    # Lcom/google/android/marvin/talkback/SpeechController;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/marvin/talkback/SpeechController;Landroid/content/SharedPreferences;)V
    .locals 0
    .param p0    # Lcom/google/android/marvin/talkback/SpeechController;
    .param p1    # Landroid/content/SharedPreferences;

    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/SpeechController;->reloadPreferences(Landroid/content/SharedPreferences;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/marvin/talkback/SpeechController;Z)V
    .locals 0
    .param p0    # Lcom/google/android/marvin/talkback/SpeechController;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/SpeechController;->onTtsInitialized(Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/marvin/talkback/SpeechController;Ljava/lang/String;ZZ)V
    .locals 0
    .param p0    # Lcom/google/android/marvin/talkback/SpeechController;
    .param p1    # Ljava/lang/String;
    .param p2    # Z
    .param p3    # Z

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/marvin/talkback/SpeechController;->onUtteranceCompleted(Ljava/lang/String;ZZ)V

    return-void
.end method

.method private clearCurrentAndQueuedUtterances()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mSpeechQueue:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->clear()V

    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mCurrentSpeechItem:Lcom/google/android/marvin/talkback/SpeechController$SpeechItem;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mCurrentSpeechItem:Lcom/google/android/marvin/talkback/SpeechController$SpeechItem;

    iget-object v0, v1, Lcom/google/android/marvin/talkback/SpeechController$SpeechItem;->utteranceId:Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/marvin/talkback/SpeechController;->onUtteranceCompleted(Ljava/lang/String;ZZ)V

    :cond_0
    return-void
.end method

.method private clearUtteranceCompletionActions(Z)V
    .locals 4
    .param p1    # Z

    if-nez p1, :cond_1

    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mUtteranceCompleteActions:Ljava/util/PriorityQueue;

    invoke-virtual {v1}, Ljava/util/PriorityQueue;->clear()V

    :cond_0
    return-void

    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mUtteranceCompleteActions:Ljava/util/PriorityQueue;

    invoke-virtual {v1}, Ljava/util/PriorityQueue;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mUtteranceCompleteActions:Ljava/util/PriorityQueue;

    invoke-virtual {v1}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteAction;

    iget-object v0, v1, Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteAction;->runnable:Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/marvin/talkback/SpeechController$CompletionRunner;

    const/4 v3, 0x3

    invoke-direct {v2, v0, v3}, Lcom/google/android/marvin/talkback/SpeechController$CompletionRunner;-><init>(Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;I)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private getNextUtteranceId()I
    .locals 2

    iget v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mNextUtteranceIndex:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mNextUtteranceIndex:I

    return v0
.end method

.method private handleSpeechCompleted()V
    .locals 4

    const/4 v3, 0x0

    iget-boolean v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mScreenIsOn:Z

    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/SpeechController;->setProximitySensorState(Z)V

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/SpeechController;->restoreRingerVolumeIfNecessary()V

    iget-boolean v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mUseAudioFocus:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mIsSpeaking:Z

    if-nez v0, :cond_1

    const/4 v0, 0x6

    const-string v1, "Completed speech while already completed!"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {p0, v0, v1, v2}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    iput-boolean v3, p0, Lcom/google/android/marvin/talkback/SpeechController;->mIsSpeaking:Z

    return-void
.end method

.method private handleSpeechStarting()V
    .locals 4

    const/4 v2, 0x3

    const/4 v3, 0x1

    invoke-direct {p0, v3}, Lcom/google/android/marvin/talkback/SpeechController;->setProximitySensorState(Z)V

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/SpeechController;->lowerRingerVolumeIfNecessary()V

    iget-boolean v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mUseAudioFocus:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1, v2, v2}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mIsSpeaking:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x6

    const-string v1, "Started speech while already speaking!"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v1, v2}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    iput-boolean v3, p0, Lcom/google/android/marvin/talkback/SpeechController;->mIsSpeaking:Z

    return-void
.end method

.method private isInCallState(I)Z
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v0

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private lowerRingerVolumeIfNecessary()V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x2

    iget-boolean v3, p0, Lcom/google/android/marvin/talkback/SpeechController;->mIsRingerLowered:Z

    if-nez v3, :cond_0

    invoke-direct {p0, v7}, Lcom/google/android/marvin/talkback/SpeechController;->isInCallState(I)Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v3, p0, Lcom/google/android/marvin/talkback/SpeechController;->mUseAudioFocus:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/marvin/talkback/SpeechController;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v3}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v3

    if-ne v3, v5, :cond_0

    iget-object v3, p0, Lcom/google/android/marvin/talkback/SpeechController;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v3, v5}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    iget-object v3, p0, Lcom/google/android/marvin/talkback/SpeechController;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v3, v5}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v2

    div-int/lit8 v3, v2, 0x3

    div-int/lit8 v4, v0, 0x2

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mRestoreRingerVolume:I

    iput v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mExpectedRingerVolume:I

    iput-boolean v7, p0, Lcom/google/android/marvin/talkback/SpeechController;->mIsRingerLowered:Z

    iget-object v3, p0, Lcom/google/android/marvin/talkback/SpeechController;->mVolumeMonitor:Lcom/google/android/marvin/talkback/VolumeMonitor;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/marvin/talkback/SpeechController;->mVolumeMonitor:Lcom/google/android/marvin/talkback/VolumeMonitor;

    invoke-virtual {v3, v5, v1, v6}, Lcom/google/android/marvin/talkback/VolumeMonitor;->setStreamVolume(III)V

    :cond_2
    iget-object v3, p0, Lcom/google/android/marvin/talkback/SpeechController;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v3, v5, v1, v6}, Landroid/media/AudioManager;->setStreamVolume(III)V

    goto :goto_0
.end method

.method private onTtsInitialized(Z)V
    .locals 2
    .param p1    # Z

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mCurrentSpeechItem:Lcom/google/android/marvin/talkback/SpeechController$SpeechItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mCurrentSpeechItem:Lcom/google/android/marvin/talkback/SpeechController$SpeechItem;

    iget-object v0, v0, Lcom/google/android/marvin/talkback/SpeechController$SpeechItem;->utteranceId:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v1}, Lcom/google/android/marvin/talkback/SpeechController;->onUtteranceCompleted(Ljava/lang/String;ZZ)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mCurrentSpeechItem:Lcom/google/android/marvin/talkback/SpeechController$SpeechItem;

    :cond_0
    if-eqz p1, :cond_2

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/SpeechController;->speakCurrentEngine()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mSpeechQueue:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/SpeechController;->speakNextItem()Z

    goto :goto_0
.end method

.method private onUtteranceCompleted(Ljava/lang/String;ZZ)V
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # Z
    .param p3    # Z

    const/4 v8, 0x2

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-static {p1}, Lcom/google/android/marvin/talkback/SpeechController;->parseUtteranceId(Ljava/lang/String;)I

    move-result v3

    iget-object v4, p0, Lcom/google/android/marvin/talkback/SpeechController;->mCurrentSpeechItem:Lcom/google/android/marvin/talkback/SpeechController$SpeechItem;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/marvin/talkback/SpeechController;->mCurrentSpeechItem:Lcom/google/android/marvin/talkback/SpeechController$SpeechItem;

    iget-object v4, v4, Lcom/google/android/marvin/talkback/SpeechController$SpeechItem;->utteranceId:Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    move v0, v5

    :goto_0
    if-eqz v0, :cond_2

    const/4 v2, 0x3

    :cond_0
    :goto_1
    iget-object v4, p0, Lcom/google/android/marvin/talkback/SpeechController;->mUtteranceCompleteActions:Ljava/util/PriorityQueue;

    invoke-virtual {v4}, Ljava/util/PriorityQueue;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_4

    iget-object v4, p0, Lcom/google/android/marvin/talkback/SpeechController;->mUtteranceCompleteActions:Ljava/util/PriorityQueue;

    invoke-virtual {v4}, Ljava/util/PriorityQueue;->peek()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteAction;

    iget v4, v4, Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteAction;->utteranceIndex:I

    if-gt v4, v3, :cond_4

    iget-object v4, p0, Lcom/google/android/marvin/talkback/SpeechController;->mUtteranceCompleteActions:Ljava/util/PriorityQueue;

    invoke-virtual {v4}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteAction;

    iget-object v1, v4, Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteAction;->runnable:Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    if-eqz v1, :cond_0

    iget-object v4, p0, Lcom/google/android/marvin/talkback/SpeechController;->mHandler:Landroid/os/Handler;

    new-instance v7, Lcom/google/android/marvin/talkback/SpeechController$CompletionRunner;

    invoke-direct {v7, v1, v2}, Lcom/google/android/marvin/talkback/SpeechController$CompletionRunner;-><init>(Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;I)V

    invoke-virtual {v4, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    :cond_1
    move v0, v6

    goto :goto_0

    :cond_2
    if-eqz p2, :cond_3

    const/4 v2, 0x4

    goto :goto_1

    :cond_3
    const/4 v2, 0x1

    goto :goto_1

    :cond_4
    iget-object v4, p0, Lcom/google/android/marvin/talkback/SpeechController;->mSpeechListener:Lcom/google/android/marvin/talkback/SpeechController$SpeechControllerListener;

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/google/android/marvin/talkback/SpeechController;->mSpeechListener:Lcom/google/android/marvin/talkback/SpeechController$SpeechControllerListener;

    invoke-interface {v4, v3, v2}, Lcom/google/android/marvin/talkback/SpeechController$SpeechControllerListener;->onUtteranceCompleted(II)V

    :cond_5
    if-eqz v0, :cond_7

    const-string v4, "Interrupted %s with %s"

    new-array v7, v8, [Ljava/lang/Object;

    aput-object p1, v7, v6

    iget-object v6, p0, Lcom/google/android/marvin/talkback/SpeechController;->mCurrentSpeechItem:Lcom/google/android/marvin/talkback/SpeechController$SpeechItem;

    iget-object v6, v6, Lcom/google/android/marvin/talkback/SpeechController$SpeechItem;->utteranceId:Ljava/lang/String;

    aput-object v6, v7, v5

    invoke-static {p0, v8, v4, v7}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    :cond_6
    :goto_2
    return-void

    :cond_7
    if-eqz p3, :cond_6

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/SpeechController;->speakNextItem()Z

    move-result v4

    if-nez v4, :cond_6

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/SpeechController;->handleSpeechCompleted()V

    goto :goto_2
.end method

.method private static parseFloatParam(Ljava/util/HashMap;Ljava/lang/String;F)F
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "F)F"
        }
    .end annotation

    invoke-virtual {p0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    if-nez v1, :cond_0

    :goto_0
    return p2

    :cond_0
    :try_start_0
    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p2

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_0
.end method

.method static parseUtteranceId(Ljava/lang/String;)I
    .locals 7
    .param p0    # Ljava/lang/String;

    const/4 v1, -0x1

    const-string v2, "talkback_"

    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-class v2, Lcom/google/android/marvin/talkback/SpeechController;

    const/4 v3, 0x6

    const-string v4, "Bad utterance ID: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p0, v5, v6

    invoke-static {v2, v3, v4, v5}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return v1

    :cond_0
    :try_start_0
    const-string v2, "talkback_"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_0
.end method

.method private reloadPreferences(Landroid/content/SharedPreferences;)V
    .locals 4
    .param p1    # Landroid/content/SharedPreferences;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/SpeechController;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a000f

    const v3, 0x7f0c0006

    invoke-static {p1, v0, v2, v3}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->getBooleanPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)Z

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/android/marvin/talkback/SpeechController;->setOverlayEnabled(Z)V

    const v2, 0x7f0a000d

    const v3, 0x7f0c0005

    invoke-static {p1, v0, v2, v3}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->getBooleanPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/marvin/talkback/SpeechController;->mUseIntonation:Z

    const v2, 0x7f0a0005

    const v3, 0x7f0a0023

    invoke-static {p1, v0, v2, v3}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->getIntFromStringPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)I

    move-result v2

    iput v2, p0, Lcom/google/android/marvin/talkback/SpeechController;->mSpeechVolume:I

    const v2, 0x7f0a0009

    const v3, 0x7f0c0004

    invoke-static {p1, v0, v2, v3}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->getBooleanPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/marvin/talkback/SpeechController;->mUseAudioFocus:Z

    iget-boolean v2, p0, Lcom/google/android/marvin/talkback/SpeechController;->mUseAudioFocus:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/marvin/talkback/SpeechController;->mAudioManager:Landroid/media/AudioManager;

    iget-object v3, p0, Lcom/google/android/marvin/talkback/SpeechController;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    :cond_0
    return-void
.end method

.method private restoreRingerVolumeIfNecessary()V
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x2

    iget-boolean v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mIsRingerLowered:Z

    if-eqz v1, :cond_0

    invoke-direct {p0, v6}, Lcom/google/android/marvin/talkback/SpeechController;->isInCallState(I)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-boolean v7, p0, Lcom/google/android/marvin/talkback/SpeechController;->mIsRingerLowered:Z

    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1, v6}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    iget v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mExpectedRingerVolume:I

    if-eq v1, v0, :cond_2

    const/4 v1, 0x6

    const-string v2, "Current volume %d does not match expected volume %d"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    const/4 v4, 0x1

    iget v5, p0, Lcom/google/android/marvin/talkback/SpeechController;->mExpectedRingerVolume:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {p0, v1, v2, v3}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    :cond_2
    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mVolumeMonitor:Lcom/google/android/marvin/talkback/VolumeMonitor;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mVolumeMonitor:Lcom/google/android/marvin/talkback/VolumeMonitor;

    iget v2, p0, Lcom/google/android/marvin/talkback/SpeechController;->mRestoreRingerVolume:I

    invoke-virtual {v1, v6, v2, v7}, Lcom/google/android/marvin/talkback/VolumeMonitor;->setStreamVolume(III)V

    :cond_3
    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mAudioManager:Landroid/media/AudioManager;

    iget v2, p0, Lcom/google/android/marvin/talkback/SpeechController;->mRestoreRingerVolume:I

    invoke-virtual {v1, v6, v2, v7}, Landroid/media/AudioManager;->setStreamVolume(III)V

    goto :goto_0
.end method

.method private setOverlayEnabled(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mTtsOverlay:Lcom/google/android/marvin/talkback/TextToSpeechOverlay;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/marvin/talkback/TextToSpeechOverlay;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-direct {v0, v1}, Lcom/google/android/marvin/talkback/TextToSpeechOverlay;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mTtsOverlay:Lcom/google/android/marvin/talkback/TextToSpeechOverlay;

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mTtsOverlay:Lcom/google/android/marvin/talkback/TextToSpeechOverlay;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mTtsOverlay:Lcom/google/android/marvin/talkback/TextToSpeechOverlay;

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/TextToSpeechOverlay;->hide()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mTtsOverlay:Lcom/google/android/marvin/talkback/TextToSpeechOverlay;

    goto :goto_0
.end method

.method private setProximitySensorState(Z)V
    .locals 2
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mRequestedProximityState:Z

    iget-boolean v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mSilenceOnProximity:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mProximitySensor:Lcom/google/android/marvin/utils/ProximitySensor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mProximitySensor:Lcom/google/android/marvin/utils/ProximitySensor;

    invoke-virtual {v0}, Lcom/google/android/marvin/utils/ProximitySensor;->stop()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mProximitySensor:Lcom/google/android/marvin/utils/ProximitySensor;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mProximitySensor:Lcom/google/android/marvin/utils/ProximitySensor;

    if-nez v0, :cond_2

    if-eqz p1, :cond_0

    new-instance v0, Lcom/google/android/marvin/utils/ProximitySensor;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-direct {v0, v1}, Lcom/google/android/marvin/utils/ProximitySensor;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mProximitySensor:Lcom/google/android/marvin/utils/ProximitySensor;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mProximitySensor:Lcom/google/android/marvin/utils/ProximitySensor;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mProximityChangeListener:Lcom/google/android/marvin/utils/ProximitySensor$ProximityChangeListener;

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/utils/ProximitySensor;->setProximityChangeListener(Lcom/google/android/marvin/utils/ProximitySensor$ProximityChangeListener;)V

    :cond_2
    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mProximitySensor:Lcom/google/android/marvin/utils/ProximitySensor;

    invoke-virtual {v0}, Lcom/google/android/marvin/utils/ProximitySensor;->start()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mProximitySensor:Lcom/google/android/marvin/utils/ProximitySensor;

    invoke-virtual {v0}, Lcom/google/android/marvin/utils/ProximitySensor;->stop()V

    goto :goto_0
.end method

.method private speakCurrentEngine()V
    .locals 8

    const/4 v4, 0x0

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mFailoverTts:Lcom/google/android/marvin/utils/FailoverTextToSpeech;

    invoke-virtual {v0}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->getEngineLabel()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v3, 0x7f0a0048

    new-array v5, v2, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v6, v5, v7

    invoke-virtual {v0, v3, v5}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x2

    move-object v0, p0

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/marvin/talkback/SpeechController;->speak(Ljava/lang/CharSequence;IILandroid/os/Bundle;Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;)V

    goto :goto_0
.end method

.method private speakInternal(Lcom/google/android/marvin/talkback/SpeechController$SpeechItem;)V
    .locals 17
    .param p1    # Lcom/google/android/marvin/talkback/SpeechController$SpeechItem;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InlinedApi"
        }
    .end annotation

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/marvin/talkback/SpeechController;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/google/android/marvin/talkback/SpeechController$SpeechItem;->text:Ljava/lang/String;

    invoke-static {v1, v14}, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->cleanUp(Landroid/content/Context;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v12

    invoke-direct/range {p0 .. p0}, Lcom/google/android/marvin/talkback/SpeechController;->getNextUtteranceId()I

    move-result v2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "talkback_"

    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    iput-object v13, v0, Lcom/google/android/marvin/talkback/SpeechController$SpeechItem;->utteranceId:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/google/android/marvin/talkback/SpeechController$SpeechItem;->completedAction:Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    if-eqz v7, :cond_0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v7}, Lcom/google/android/marvin/talkback/SpeechController;->addUtteranceCompleteAction(ILcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;)V

    :cond_0
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/marvin/talkback/SpeechController;->mInjectFullScreenReadCallbacks:Z

    if-eqz v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/marvin/talkback/SpeechController;->mFullScreenReadNextCallback:Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v1}, Lcom/google/android/marvin/talkback/SpeechController;->addUtteranceCompleteAction(ILcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;)V

    :cond_1
    if-nez v12, :cond_3

    const/4 v3, 0x0

    :goto_0
    move-object/from16 v0, p1

    iget v1, v0, Lcom/google/android/marvin/talkback/SpeechController$SpeechItem;->flags:I

    and-int/lit8 v1, v1, 0x2

    const/4 v14, 0x2

    if-eq v1, v14, :cond_2

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/marvin/talkback/SpeechController;->mLastUtteranceSpeech:Ljava/lang/CharSequence;

    :cond_2
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/marvin/talkback/SpeechController;->mSpeechParametersMap:Ljava/util/HashMap;

    invoke-virtual {v11}, Ljava/util/HashMap;->clear()V

    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/google/android/marvin/talkback/SpeechController$SpeechItem;->params:Landroid/os/Bundle;

    invoke-virtual {v9}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-virtual {v9, v10}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11, v10, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_3
    invoke-virtual {v12}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_4
    const-string v1, "utteranceId"

    invoke-virtual {v11, v1, v13}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "streamType"

    const/4 v14, 0x3

    invoke-static {v14}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v11, v1, v14}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "volume"

    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/marvin/talkback/SpeechController;->mSpeechVolume:I

    int-to-float v14, v14

    const/high16 v15, 0x42c80000

    div-float/2addr v14, v15

    invoke-static {v14}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v11, v1, v14}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/marvin/talkback/SpeechController;->mUseIntonation:Z

    if-eqz v1, :cond_7

    const-string v1, "pitch"

    const/high16 v14, 0x3f800000

    invoke-static {v11, v1, v14}, Lcom/google/android/marvin/talkback/SpeechController;->parseFloatParam(Ljava/util/HashMap;Ljava/lang/String;F)F

    move-result v4

    :goto_2
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/marvin/talkback/SpeechController;->mUseIntonation:Z

    if-eqz v1, :cond_8

    const-string v1, "rate"

    const/high16 v14, 0x3f800000

    invoke-static {v11, v1, v14}, Lcom/google/android/marvin/talkback/SpeechController;->parseFloatParam(Ljava/util/HashMap;Ljava/lang/String;F)F

    move-result v5

    :goto_3
    const/4 v1, 0x2

    const-string v14, "Speaking \"%s\""

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object v12, v15, v16

    move-object/from16 v0, p0

    invoke-static {v0, v1, v14, v15}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/marvin/talkback/SpeechController;->mSpeechListener:Lcom/google/android/marvin/talkback/SpeechController$SpeechControllerListener;

    if-eqz v1, :cond_5

    invoke-static {v11}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/marvin/talkback/SpeechController;->mSpeechListener:Lcom/google/android/marvin/talkback/SpeechController$SpeechControllerListener;

    invoke-interface/range {v1 .. v6}, Lcom/google/android/marvin/talkback/SpeechController$SpeechControllerListener;->onUtteranceStarted(ILjava/lang/String;FFLjava/util/Map;)V

    :cond_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/marvin/talkback/SpeechController;->mFailoverTts:Lcom/google/android/marvin/utils/FailoverTextToSpeech;

    invoke-virtual {v1, v3, v4, v5, v11}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->speak(Ljava/lang/String;FFLjava/util/HashMap;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/marvin/talkback/SpeechController;->mTtsOverlay:Lcom/google/android/marvin/talkback/TextToSpeechOverlay;

    if-eqz v1, :cond_6

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/marvin/talkback/SpeechController;->mTtsOverlay:Lcom/google/android/marvin/talkback/TextToSpeechOverlay;

    invoke-virtual {v1, v3}, Lcom/google/android/marvin/talkback/TextToSpeechOverlay;->speak(Ljava/lang/String;)V

    :cond_6
    return-void

    :cond_7
    const/high16 v4, 0x3f800000

    goto :goto_2

    :cond_8
    const/high16 v5, 0x3f800000

    goto :goto_3
.end method

.method private speakNextItem()Z
    .locals 6

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mCurrentSpeechItem:Lcom/google/android/marvin/talkback/SpeechController$SpeechItem;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/SpeechController;->mSpeechQueue:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mCurrentSpeechItem:Lcom/google/android/marvin/talkback/SpeechController$SpeechItem;

    if-nez v0, :cond_1

    const/4 v2, 0x2

    const-string v4, "No next item, stopping speech queue"

    new-array v5, v3, [Ljava/lang/Object;

    invoke-static {p0, v2, v4, v5}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    move v2, v3

    :goto_1
    return v2

    :cond_0
    iget-object v2, p0, Lcom/google/android/marvin/talkback/SpeechController;->mSpeechQueue:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/marvin/talkback/SpeechController$SpeechItem;

    move-object v0, v2

    goto :goto_0

    :cond_1
    if-nez v1, :cond_2

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/SpeechController;->handleSpeechStarting()V

    :cond_2
    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/SpeechController;->speakInternal(Lcom/google/android/marvin/talkback/SpeechController$SpeechItem;)V

    const/4 v2, 0x1

    goto :goto_1
.end method


# virtual methods
.method public addUtteranceCompleteAction(ILcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;)V
    .locals 2
    .param p1    # I
    .param p2    # Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    new-instance v0, Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteAction;

    invoke-direct {v0, p1, p2}, Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteAction;-><init>(ILcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;)V

    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mUtteranceCompleteActions:Ljava/util/PriorityQueue;

    invoke-virtual {v1, v0}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public interrupt()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/SpeechController;->clearCurrentAndQueuedUtterances()V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/SpeechController;->clearUtteranceCompletionActions(Z)V

    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mFailoverTts:Lcom/google/android/marvin/utils/FailoverTextToSpeech;

    invoke-virtual {v0}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->stopAll()V

    return-void
.end method

.method public peekNextUtteranceId()I
    .locals 1

    iget v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mNextUtteranceIndex:I

    return v0
.end method

.method public removeUtteranceCompleteAction(Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;)V
    .locals 3
    .param p1    # Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/SpeechController;->mUtteranceCompleteActions:Ljava/util/PriorityQueue;

    invoke-virtual {v2}, Ljava/util/PriorityQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteAction;

    iget-object v2, v0, Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteAction;->runnable:Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    if-ne v2, p1, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public repeatLastUtterance()Z
    .locals 6

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mLastUtteranceSpeech:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mLastUtteranceSpeech:Ljava/lang/CharSequence;

    const/4 v2, 0x3

    const/4 v3, 0x2

    move-object v0, p0

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/marvin/talkback/SpeechController;->speak(Ljava/lang/CharSequence;IILandroid/os/Bundle;Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setScreenIsOn(Z)V
    .locals 1
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mScreenIsOn:Z

    iget-boolean v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mScreenIsOn:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/SpeechController;->setProximitySensorState(Z)V

    :cond_0
    return-void
.end method

.method public setShouldInjectAutoReadingCallbacks(ZLcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;)V
    .locals 1
    .param p1    # Z
    .param p2    # Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    if-eqz p1, :cond_1

    move-object v0, p2

    :goto_0
    iput-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mFullScreenReadNextCallback:Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    iput-boolean p1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mInjectFullScreenReadCallbacks:Z

    if-nez p1, :cond_0

    invoke-virtual {p0, p2}, Lcom/google/android/marvin/talkback/SpeechController;->removeUtteranceCompleteAction(Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setSilenceOnProximity(Z)V
    .locals 1
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mSilenceOnProximity:Z

    iget-boolean v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mSilenceOnProximity:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mRequestedProximityState:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/SpeechController;->setProximitySensorState(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setVolumeMonitor(Lcom/google/android/marvin/talkback/VolumeMonitor;)V
    .locals 0
    .param p1    # Lcom/google/android/marvin/talkback/VolumeMonitor;

    iput-object p1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mVolumeMonitor:Lcom/google/android/marvin/talkback/VolumeMonitor;

    return-void
.end method

.method public shutdown()V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/SpeechController;->interrupt()V

    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController;->mFailoverTts:Lcom/google/android/marvin/utils/FailoverTextToSpeech;

    invoke-virtual {v0}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->shutdown()V

    invoke-direct {p0, v1}, Lcom/google/android/marvin/talkback/SpeechController;->setOverlayEnabled(Z)V

    invoke-direct {p0, v1}, Lcom/google/android/marvin/talkback/SpeechController;->setProximitySensorState(Z)V

    return-void
.end method

.method public speak(Ljava/lang/CharSequence;IILandroid/os/Bundle;)V
    .locals 6
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # Landroid/os/Bundle;

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/marvin/talkback/SpeechController;->speak(Ljava/lang/CharSequence;IILandroid/os/Bundle;Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;)V

    return-void
.end method

.method public speak(Ljava/lang/CharSequence;IILandroid/os/Bundle;Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;)V
    .locals 6
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # Landroid/os/Bundle;
    .param p5    # Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    const/4 v1, 0x0

    const/4 v5, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    new-instance v0, Lcom/google/android/marvin/talkback/SpeechController$SpeechItem;

    invoke-direct {v0, v1}, Lcom/google/android/marvin/talkback/SpeechController$SpeechItem;-><init>(Lcom/google/android/marvin/talkback/SpeechController$1;)V

    if-nez p1, :cond_2

    :goto_0
    iput-object v1, v0, Lcom/google/android/marvin/talkback/SpeechController$SpeechItem;->text:Ljava/lang/String;

    if-ne p2, v5, :cond_3

    move v1, v2

    :goto_1
    iput-boolean v1, v0, Lcom/google/android/marvin/talkback/SpeechController$SpeechItem;->uninterruptible:Z

    iput p3, v0, Lcom/google/android/marvin/talkback/SpeechController$SpeechItem;->flags:I

    if-nez p4, :cond_0

    sget-object p4, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    :cond_0
    iput-object p4, v0, Lcom/google/android/marvin/talkback/SpeechController$SpeechItem;->params:Landroid/os/Bundle;

    iput-object p5, v0, Lcom/google/android/marvin/talkback/SpeechController$SpeechItem;->completedAction:Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    if-eq p2, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mSpeechQueue:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->clear()V

    :cond_1
    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mSpeechQueue:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mFailoverTts:Lcom/google/android/marvin/utils/FailoverTextToSpeech;

    invoke-virtual {v1}, Lcom/google/android/marvin/utils/FailoverTextToSpeech;->isReady()Z

    move-result v1

    if-nez v1, :cond_4

    const/4 v1, 0x6

    const-string v2, "Attempted to speak before TTS was initialized."

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p0, v1, v2, v3}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    :goto_2
    return-void

    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_3
    move v1, v3

    goto :goto_1

    :cond_4
    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mCurrentSpeechItem:Lcom/google/android/marvin/talkback/SpeechController$SpeechItem;

    if-eqz v1, :cond_5

    if-eq p2, v2, :cond_6

    iget-object v1, p0, Lcom/google/android/marvin/talkback/SpeechController;->mCurrentSpeechItem:Lcom/google/android/marvin/talkback/SpeechController$SpeechItem;

    iget-boolean v1, v1, Lcom/google/android/marvin/talkback/SpeechController$SpeechItem;->uninterruptible:Z

    if-nez v1, :cond_6

    :cond_5
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/SpeechController;->speakNextItem()Z

    goto :goto_2

    :cond_6
    const-string v1, "Queued speech item, waiting for \"%s\""

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/marvin/talkback/SpeechController;->mCurrentSpeechItem:Lcom/google/android/marvin/talkback/SpeechController$SpeechItem;

    iget-object v4, v4, Lcom/google/android/marvin/talkback/SpeechController$SpeechItem;->text:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {p0, v5, v1, v2}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2
.end method

.method public spellLastUtterance()Z
    .locals 9

    const/4 v4, 0x0

    const/4 v8, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/marvin/talkback/SpeechController;->mLastUtteranceSpeech:Ljava/lang/CharSequence;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v7, 0x0

    :goto_1
    iget-object v2, p0, Lcom/google/android/marvin/talkback/SpeechController;->mLastUtteranceSpeech:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-ge v7, v2, :cond_1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/SpeechController;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    iget-object v3, p0, Lcom/google/android/marvin/talkback/SpeechController;->mLastUtteranceSpeech:Ljava/lang/CharSequence;

    invoke-interface {v3, v7}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    invoke-static {v2, v3}, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->getCleanValueFor(Landroid/content/Context;C)Ljava/lang/String;

    move-result-object v6

    new-array v2, v8, [Ljava/lang/Object;

    aput-object v6, v2, v0

    invoke-static {v1, v2}, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->appendWithSeparator(Ljava/lang/StringBuilder;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_1
    const/4 v2, 0x3

    const/4 v3, 0x2

    move-object v0, p0

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/marvin/talkback/SpeechController;->speak(Ljava/lang/CharSequence;IILandroid/os/Bundle;Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;)V

    move v0, v8

    goto :goto_0
.end method
