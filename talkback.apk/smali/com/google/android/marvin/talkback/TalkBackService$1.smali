.class Lcom/google/android/marvin/talkback/TalkBackService$1;
.super Ljava/lang/Object;
.source "TalkBackService.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/marvin/talkback/TalkBackService;->confirmSuspendTalkBack()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/TalkBackService;

.field final synthetic val$confirmCheckBox:Landroid/widget/CheckBox;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;Landroid/widget/CheckBox;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/marvin/talkback/TalkBackService$1;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    iput-object p2, p0, Lcom/google/android/marvin/talkback/TalkBackService$1;->val$confirmCheckBox:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    packed-switch p2, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService$1;->val$confirmCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService$1;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    # getter for: Lcom/google/android/marvin/talkback/TalkBackService;->mPrefs:Landroid/content/SharedPreferences;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/TalkBackService;->access$000(Lcom/google/android/marvin/talkback/TalkBackService;)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService$1;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/TalkBackService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0022

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->putBooleanPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;IZ)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService$1;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    # invokes: Lcom/google/android/marvin/talkback/TalkBackService;->suspendTalkBack()V
    invoke-static {v0}, Lcom/google/android/marvin/talkback/TalkBackService;->access$100(Lcom/google/android/marvin/talkback/TalkBackService;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
    .end packed-switch
.end method
