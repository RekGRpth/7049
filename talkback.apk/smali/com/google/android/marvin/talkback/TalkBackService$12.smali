.class Lcom/google/android/marvin/talkback/TalkBackService$12;
.super Ljava/lang/Object;
.source "TalkBackService.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/RadialMenuManager$RadialMenuClient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/TalkBackService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/TalkBackService;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/marvin/talkback/TalkBackService$12;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateRadialMenu(ILcom/googlecode/eyesfree/widget/RadialMenu;)V
    .locals 5
    .param p1    # I
    .param p2    # Lcom/googlecode/eyesfree/widget/RadialMenu;

    const/high16 v4, 0x7f0f0000

    const v2, 0x7f080057

    const/16 v3, 0x10

    if-ne p1, v4, :cond_3

    const/high16 v0, 0x7f0f0000

    :goto_0
    if-lez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService$12;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    # invokes: Lcom/google/android/marvin/talkback/TalkBackService;->getMenuInflater()Landroid/view/MenuInflater;
    invoke-static {v1}, Lcom/google/android/marvin/talkback/TalkBackService;->access$1200(Lcom/google/android/marvin/talkback/TalkBackService;)Landroid/view/MenuInflater;

    move-result-object v1

    invoke-virtual {v1, v0, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    :cond_0
    if-ne p1, v4, :cond_2

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v1, v3, :cond_1

    const v1, 0x7f080053

    invoke-virtual {p2, v1}, Lcom/googlecode/eyesfree/widget/RadialMenu;->removeItem(I)V

    const v1, 0x7f080056

    invoke-virtual {p2, v1}, Lcom/googlecode/eyesfree/widget/RadialMenu;->removeItem(I)V

    :cond_1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v1, v3, :cond_4

    invoke-virtual {p2, v2}, Lcom/googlecode/eyesfree/widget/RadialMenu;->removeItem(I)V

    :goto_1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v3, :cond_2

    const v1, 0x7f080054

    invoke-virtual {p2, v1}, Lcom/googlecode/eyesfree/widget/RadialMenu;->removeItem(I)V

    :cond_2
    return-void

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService$12;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {p2, v2}, Lcom/googlecode/eyesfree/widget/RadialMenu;->findItem(I)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v2

    # invokes: Lcom/google/android/marvin/talkback/TalkBackService;->createSummaryMenu(Lcom/googlecode/eyesfree/widget/RadialMenuItem;)V
    invoke-static {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->access$1300(Lcom/google/android/marvin/talkback/TalkBackService;Lcom/googlecode/eyesfree/widget/RadialMenuItem;)V

    goto :goto_1
.end method

.method public onMenuItemClicked(Landroid/view/MenuItem;)Z
    .locals 8
    .param p1    # Landroid/view/MenuItem;

    const v7, 0x7f08002e

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v6, 0x7f080053

    if-ne v0, v6, :cond_3

    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService$12;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    # getter for: Lcom/google/android/marvin/talkback/TalkBackService;->mFullScreenReadController:Lcom/google/android/marvin/talkback/FullScreenReadController;
    invoke-static {v4}, Lcom/google/android/marvin/talkback/TalkBackService;->access$1500(Lcom/google/android/marvin/talkback/TalkBackService;)Lcom/google/android/marvin/talkback/FullScreenReadController;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/marvin/talkback/FullScreenReadController;->getReadingState()Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

    move-result-object v4

    sget-object v6, Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;->STOPPED:Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

    if-ne v4, v6, :cond_2

    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService$12;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    # getter for: Lcom/google/android/marvin/talkback/TalkBackService;->mFullScreenReadController:Lcom/google/android/marvin/talkback/FullScreenReadController;
    invoke-static {v4}, Lcom/google/android/marvin/talkback/TalkBackService;->access$1500(Lcom/google/android/marvin/talkback/TalkBackService;)Lcom/google/android/marvin/talkback/FullScreenReadController;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/marvin/talkback/FullScreenReadController;->startReadingFromBeginning()Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService$12;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    # getter for: Lcom/google/android/marvin/talkback/TalkBackService;->mFeedbackController:Lcom/google/android/marvin/talkback/PreferenceFeedbackController;
    invoke-static {v4}, Lcom/google/android/marvin/talkback/TalkBackService;->access$400(Lcom/google/android/marvin/talkback/TalkBackService;)Lcom/google/android/marvin/talkback/PreferenceFeedbackController;

    move-result-object v4

    invoke-virtual {v4, v7}, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;->playCustomSound(I)V

    :cond_2
    move v4, v5

    goto :goto_0

    :cond_3
    const v6, 0x7f080056

    if-ne v0, v6, :cond_5

    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService$12;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    # getter for: Lcom/google/android/marvin/talkback/TalkBackService;->mFullScreenReadController:Lcom/google/android/marvin/talkback/FullScreenReadController;
    invoke-static {v4}, Lcom/google/android/marvin/talkback/TalkBackService;->access$1500(Lcom/google/android/marvin/talkback/TalkBackService;)Lcom/google/android/marvin/talkback/FullScreenReadController;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/marvin/talkback/FullScreenReadController;->getReadingState()Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

    move-result-object v4

    sget-object v6, Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;->STOPPED:Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

    if-ne v4, v6, :cond_4

    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService$12;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    # getter for: Lcom/google/android/marvin/talkback/TalkBackService;->mFullScreenReadController:Lcom/google/android/marvin/talkback/FullScreenReadController;
    invoke-static {v4}, Lcom/google/android/marvin/talkback/TalkBackService;->access$1500(Lcom/google/android/marvin/talkback/TalkBackService;)Lcom/google/android/marvin/talkback/FullScreenReadController;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/marvin/talkback/FullScreenReadController;->startReadingFromNextNode()Z

    move-result v4

    if-nez v4, :cond_4

    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService$12;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    # getter for: Lcom/google/android/marvin/talkback/TalkBackService;->mFeedbackController:Lcom/google/android/marvin/talkback/PreferenceFeedbackController;
    invoke-static {v4}, Lcom/google/android/marvin/talkback/TalkBackService;->access$400(Lcom/google/android/marvin/talkback/TalkBackService;)Lcom/google/android/marvin/talkback/PreferenceFeedbackController;

    move-result-object v4

    invoke-virtual {v4, v7}, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;->playCustomSound(I)V

    :cond_4
    move v4, v5

    goto :goto_0

    :cond_5
    const v6, 0x7f080054

    if-ne v0, v6, :cond_6

    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService$12;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    # getter for: Lcom/google/android/marvin/talkback/TalkBackService;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;
    invoke-static {v4}, Lcom/google/android/marvin/talkback/TalkBackService;->access$500(Lcom/google/android/marvin/talkback/TalkBackService;)Lcom/google/android/marvin/talkback/SpeechController;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/marvin/talkback/SpeechController;->repeatLastUtterance()Z

    move v4, v5

    goto :goto_0

    :cond_6
    const v6, 0x7f080055

    if-ne v0, v6, :cond_7

    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService$12;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    # getter for: Lcom/google/android/marvin/talkback/TalkBackService;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;
    invoke-static {v4}, Lcom/google/android/marvin/talkback/TalkBackService;->access$500(Lcom/google/android/marvin/talkback/TalkBackService;)Lcom/google/android/marvin/talkback/SpeechController;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/marvin/talkback/SpeechController;->spellLastUtterance()Z

    move v4, v5

    goto :goto_0

    :cond_7
    const v6, 0x7f080058

    if-ne v0, v6, :cond_9

    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService$12;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v4}, Lcom/google/android/marvin/talkback/TalkBackService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService$12;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v4}, Lcom/google/android/marvin/talkback/TalkBackService;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0a0022

    const v7, 0x7f0c000b

    invoke-static {v1, v4, v6, v7}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->getBooleanPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)Z

    move-result v3

    if-eqz v3, :cond_8

    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService$12;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    # invokes: Lcom/google/android/marvin/talkback/TalkBackService;->confirmSuspendTalkBack()V
    invoke-static {v4}, Lcom/google/android/marvin/talkback/TalkBackService;->access$1600(Lcom/google/android/marvin/talkback/TalkBackService;)V

    :goto_1
    move v4, v5

    goto/16 :goto_0

    :cond_8
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService$12;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    # invokes: Lcom/google/android/marvin/talkback/TalkBackService;->suspendTalkBack()V
    invoke-static {v4}, Lcom/google/android/marvin/talkback/TalkBackService;->access$100(Lcom/google/android/marvin/talkback/TalkBackService;)V

    goto :goto_1

    :cond_9
    const v6, 0x7f080059

    if-ne v0, v6, :cond_0

    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService$12;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    const-class v6, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;

    invoke-direct {v2, v4, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v4, 0x10000000

    invoke-virtual {v2, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const/high16 v4, 0x4000000

    invoke-virtual {v2, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService$12;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v4, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->startActivity(Landroid/content/Intent;)V

    move v4, v5

    goto/16 :goto_0
.end method

.method public onMenuItemHovered(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1    # Landroid/view/MenuItem;

    const/4 v0, 0x0

    return v0
.end method

.method public onPrepareRadialMenu(ILcom/googlecode/eyesfree/widget/RadialMenu;)Z
    .locals 3
    .param p1    # I
    .param p2    # Lcom/googlecode/eyesfree/widget/RadialMenu;

    const v2, 0x7f0f0001

    if-ne p1, v2, :cond_1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService$12;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    # getter for: Lcom/google/android/marvin/talkback/TalkBackService;->mMenuProcessor:Lcom/google/android/marvin/talkback/speechrules/NodeMenuProcessor;
    invoke-static {v2}, Lcom/google/android/marvin/talkback/TalkBackService;->access$1400(Lcom/google/android/marvin/talkback/TalkBackService;)Lcom/google/android/marvin/talkback/speechrules/NodeMenuProcessor;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService$12;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    # getter for: Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;
    invoke-static {v2}, Lcom/google/android/marvin/talkback/TalkBackService;->access$300(Lcom/google/android/marvin/talkback/TalkBackService;)Lcom/google/android/marvin/talkback/CursorController;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/CursorController;->getCursor()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService$12;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    # getter for: Lcom/google/android/marvin/talkback/TalkBackService;->mMenuProcessor:Lcom/google/android/marvin/talkback/speechrules/NodeMenuProcessor;
    invoke-static {v2}, Lcom/google/android/marvin/talkback/TalkBackService;->access$1400(Lcom/google/android/marvin/talkback/TalkBackService;)Lcom/google/android/marvin/talkback/speechrules/NodeMenuProcessor;

    move-result-object v2

    invoke-virtual {v2, p2, v0}, Lcom/google/android/marvin/talkback/speechrules/NodeMenuProcessor;->prepareMenuForNode(Lcom/googlecode/eyesfree/widget/RadialMenu;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v1

    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    goto :goto_0

    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method
