.class Lcom/google/android/marvin/talkback/TalkBackService$6;
.super Ljava/lang/Object;
.source "TalkBackService.java"

# interfaces
.implements Lcom/googlecode/eyesfree/widget/RadialMenu$OnMenuVisibilityChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/marvin/talkback/TalkBackService;->createSummaryMenu(Lcom/googlecode/eyesfree/widget/RadialMenuItem;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/TalkBackService;

.field final synthetic val$hintRunnable:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;Ljava/lang/Runnable;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/marvin/talkback/TalkBackService$6;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    iput-object p2, p0, Lcom/google/android/marvin/talkback/TalkBackService$6;->val$hintRunnable:Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuDismissed(Lcom/googlecode/eyesfree/widget/RadialMenu;)V
    .locals 2
    .param p1    # Lcom/googlecode/eyesfree/widget/RadialMenu;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService$6;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    # getter for: Lcom/google/android/marvin/talkback/TalkBackService;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/TalkBackService;->access$600(Lcom/google/android/marvin/talkback/TalkBackService;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService$6;->val$hintRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService$6;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    # getter for: Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/TalkBackService;->access$300(Lcom/google/android/marvin/talkback/TalkBackService;)Lcom/google/android/marvin/talkback/CursorController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/CursorController;->refocus()Z

    return-void
.end method

.method public onMenuShown(Lcom/googlecode/eyesfree/widget/RadialMenu;)V
    .locals 4
    .param p1    # Lcom/googlecode/eyesfree/widget/RadialMenu;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService$6;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    # getter for: Lcom/google/android/marvin/talkback/TalkBackService;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/TalkBackService;->access$600(Lcom/google/android/marvin/talkback/TalkBackService;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService$6;->val$hintRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method
