.class public Lcom/google/android/marvin/talkback/TalkBackService;
.super Landroid/accessibilityservice/AccessibilityService;
.source "TalkBackService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/TalkBackService$EventListener;
    }
.end annotation


# static fields
.field static final SUPPORTS_TOUCH_PREF:Z

.field private static sInstance:Lcom/google/android/marvin/talkback/TalkBackService;

.field private static sIsTalkBackSuspended:Z


# instance fields
.field private mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

.field private final mActiveReceiver:Landroid/content/BroadcastReceiver;

.field private mCallStateMonitor:Lcom/google/android/marvin/talkback/CallStateMonitor;

.field private mCursorController:Lcom/google/android/marvin/talkback/CursorController;

.field private final mCursorControllerListener:Lcom/google/android/marvin/talkback/CursorController$CursorControllerListener;

.field private final mEventListeners:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/android/marvin/talkback/TalkBackService$EventListener;",
            ">;"
        }
    .end annotation
.end field

.field private mFeedbackController:Lcom/google/android/marvin/talkback/PreferenceFeedbackController;

.field private mFullScreenReadController:Lcom/google/android/marvin/talkback/FullScreenReadController;

.field private final mHandler:Landroid/os/Handler;

.field private mIsUserTouchExploring:Z

.field private mLastSpokenEvent:Landroid/view/accessibility/AccessibilityEvent;

.field private mLastWindowStateChanged:J

.field private mMenuInflater:Landroid/view/MenuInflater;

.field private mMenuProcessor:Lcom/google/android/marvin/talkback/speechrules/NodeMenuProcessor;

.field private mNodeProcessor:Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;

.field private mOrientationMonitor:Lcom/google/android/marvin/talkback/OrientationMonitor;

.field private mPrefs:Landroid/content/SharedPreferences;

.field private mProcessorEventQueue:Lcom/google/android/marvin/talkback/ProcessorEventQueue;

.field private mProcessorFollowFocus:Lcom/google/android/marvin/talkback/ProcessorFollowFocus;

.field private mProcessorSingleTap:Lcom/google/android/marvin/talkback/ProcessorSingleTap;

.field private final mRadialMenuClient:Lcom/google/android/marvin/talkback/RadialMenuManager$RadialMenuClient;

.field private mRadialMenuManager:Lcom/google/android/marvin/talkback/RadialMenuManager;

.field private mRingerModeAndScreenMonitor:Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;

.field private mShakeDetector:Lcom/google/android/marvin/talkback/ShakeDetector;

.field private final mSharedPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field private mSpeakCallerId:Z

.field private mSpeakWhenScreenOff:Z

.field private mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

.field private final mSuspendedReceiver:Landroid/content/BroadcastReceiver;

.field private mTestingListener:Lcom/google/android/marvin/talkback/test/TalkBackListener;

.field private final mTouchExploreObserver:Landroid/database/ContentObserver;

.field private mTtsOverlay:Lcom/google/android/marvin/talkback/TextToSpeechOverlay;

.field private mVolumeMonitor:Lcom/google/android/marvin/talkback/VolumeMonitor;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v1, 0x1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v0, v2, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/google/android/marvin/talkback/TalkBackService;->SUPPORTS_TOUCH_PREF:Z

    sput-boolean v1, Lcom/google/android/marvin/talkback/TalkBackService;->sIsTalkBackSuspended:Z

    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/marvin/talkback/TalkBackService;->sInstance:Lcom/google/android/marvin/talkback/TalkBackService;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/accessibilityservice/AccessibilityService;-><init>()V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mEventListeners:Ljava/util/LinkedList;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mLastWindowStateChanged:J

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/marvin/talkback/TalkBackService$7;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/TalkBackService$7;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCursorControllerListener:Lcom/google/android/marvin/talkback/CursorController$CursorControllerListener;

    new-instance v0, Lcom/google/android/marvin/talkback/TalkBackService$8;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/TalkBackService$8;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSharedPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    new-instance v0, Lcom/google/android/marvin/talkback/TalkBackService$9;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/google/android/marvin/talkback/TalkBackService$9;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mTouchExploreObserver:Landroid/database/ContentObserver;

    new-instance v0, Lcom/google/android/marvin/talkback/TalkBackService$10;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/TalkBackService$10;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mActiveReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/google/android/marvin/talkback/TalkBackService$11;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/TalkBackService$11;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSuspendedReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/google/android/marvin/talkback/TalkBackService$12;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/TalkBackService$12;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mRadialMenuClient:Lcom/google/android/marvin/talkback/RadialMenuManager$RadialMenuClient;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/marvin/talkback/TalkBackService;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0    # Lcom/google/android/marvin/talkback/TalkBackService;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mPrefs:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 0
    .param p0    # Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->suspendTalkBack()V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/marvin/talkback/TalkBackService;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/marvin/talkback/TalkBackService;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/TalkBackService;->performGesture(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 0
    .param p0    # Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->resumeTalkBack()V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/marvin/talkback/TalkBackService;)Landroid/view/MenuInflater;
    .locals 1
    .param p0    # Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/marvin/talkback/TalkBackService;Lcom/googlecode/eyesfree/widget/RadialMenuItem;)V
    .locals 0
    .param p0    # Lcom/google/android/marvin/talkback/TalkBackService;
    .param p1    # Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/TalkBackService;->createSummaryMenu(Lcom/googlecode/eyesfree/widget/RadialMenuItem;)V

    return-void
.end method

.method static synthetic access$1400(Lcom/google/android/marvin/talkback/TalkBackService;)Lcom/google/android/marvin/talkback/speechrules/NodeMenuProcessor;
    .locals 1
    .param p0    # Lcom/google/android/marvin/talkback/TalkBackService;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mMenuProcessor:Lcom/google/android/marvin/talkback/speechrules/NodeMenuProcessor;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/marvin/talkback/TalkBackService;)Lcom/google/android/marvin/talkback/FullScreenReadController;
    .locals 1
    .param p0    # Lcom/google/android/marvin/talkback/TalkBackService;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mFullScreenReadController:Lcom/google/android/marvin/talkback/FullScreenReadController;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 0
    .param p0    # Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->confirmSuspendTalkBack()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/marvin/talkback/TalkBackService;)Ljava/util/LinkedList;
    .locals 1
    .param p0    # Lcom/google/android/marvin/talkback/TalkBackService;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mEventListeners:Ljava/util/LinkedList;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/marvin/talkback/TalkBackService;)Lcom/google/android/marvin/talkback/CursorController;
    .locals 1
    .param p0    # Lcom/google/android/marvin/talkback/TalkBackService;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/marvin/talkback/TalkBackService;)Lcom/google/android/marvin/talkback/PreferenceFeedbackController;
    .locals 1
    .param p0    # Lcom/google/android/marvin/talkback/TalkBackService;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mFeedbackController:Lcom/google/android/marvin/talkback/PreferenceFeedbackController;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/marvin/talkback/TalkBackService;)Lcom/google/android/marvin/talkback/SpeechController;
    .locals 1
    .param p0    # Lcom/google/android/marvin/talkback/TalkBackService;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/marvin/talkback/TalkBackService;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/google/android/marvin/talkback/TalkBackService;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/marvin/talkback/TalkBackService;)Lcom/google/android/marvin/talkback/ProcessorFollowFocus;
    .locals 1
    .param p0    # Lcom/google/android/marvin/talkback/TalkBackService;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mProcessorFollowFocus:Lcom/google/android/marvin/talkback/ProcessorFollowFocus;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 0
    .param p0    # Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->reloadPreferences()V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 0
    .param p0    # Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->onTouchExplorationEnabled()V

    return-void
.end method

.method private cacheEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mLastSpokenEvent:Landroid/view/accessibility/AccessibilityEvent;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mLastSpokenEvent:Landroid/view/accessibility/AccessibilityEvent;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->recycle()V

    :cond_0
    invoke-static {p1}, Lcom/googlecode/eyesfree/compat/view/accessibility/AccessibilityEventCompatUtils;->obtain(Landroid/view/accessibility/AccessibilityEvent;)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mLastSpokenEvent:Landroid/view/accessibility/AccessibilityEvent;

    return-void
.end method

.method private confirmSuspendTalkBack()V
    .locals 8

    const/4 v7, 0x0

    const-string v5, "layout_inflater"

    invoke-virtual {p0, v5}, Lcom/google/android/marvin/talkback/TalkBackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    const v5, 0x7f030008

    invoke-virtual {v2, v5, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ScrollView;

    const v5, 0x7f080052

    invoke-virtual {v4, v5}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    new-instance v3, Lcom/google/android/marvin/talkback/TalkBackService$1;

    invoke-direct {v3, p0, v0}, Lcom/google/android/marvin/talkback/TalkBackService$1;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;Landroid/widget/CheckBox;)V

    new-instance v5, Landroid/app/AlertDialog$Builder;

    invoke-direct {v5, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v6, 0x7f0a00db

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    const/high16 v6, 0x1040000

    invoke-virtual {v5, v6, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    const v6, 0x104000a

    invoke-virtual {v5, v6, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v5

    const/16 v6, 0x7da

    invoke-virtual {v5, v6}, Landroid/view/Window;->setType(I)V

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method private createSummaryMenu(Lcom/googlecode/eyesfree/widget/RadialMenuItem;)V
    .locals 5
    .param p1    # Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->getSubMenu()Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    move-result-object v3

    new-instance v1, Lcom/google/android/marvin/talkback/TalkBackService$3;

    invoke-direct {v1, p0}, Lcom/google/android/marvin/talkback/TalkBackService$3;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    new-instance v0, Lcom/google/android/marvin/talkback/TalkBackService$4;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/TalkBackService$4;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    new-instance v2, Lcom/google/android/marvin/talkback/TalkBackService$5;

    invoke-direct {v2, p0, v0}, Lcom/google/android/marvin/talkback/TalkBackService$5;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;Ljava/lang/Runnable;)V

    new-instance v4, Lcom/google/android/marvin/talkback/TalkBackService$6;

    invoke-direct {v4, p0, v0}, Lcom/google/android/marvin/talkback/TalkBackService$6;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;Ljava/lang/Runnable;)V

    invoke-virtual {v1, v3}, Lcom/google/android/marvin/talkback/BreakoutMenuUtils$JogDial;->populateMenu(Lcom/googlecode/eyesfree/widget/RadialMenu;)V

    invoke-virtual {v3, v2}, Lcom/googlecode/eyesfree/widget/RadialSubMenu;->setDefaultSelectionListener(Lcom/googlecode/eyesfree/widget/RadialMenuItem$OnMenuItemSelectionListener;)V

    invoke-virtual {v3, v4}, Lcom/googlecode/eyesfree/widget/RadialSubMenu;->setOnMenuVisibilityChangedListener(Lcom/googlecode/eyesfree/widget/RadialMenu$OnMenuVisibilityChangedListener;)V

    return-void
.end method

.method private getMenuInflater()Landroid/view/MenuInflater;
    .locals 1

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mMenuInflater:Landroid/view/MenuInflater;

    if-nez v0, :cond_0

    new-instance v0, Landroid/view/MenuInflater;

    invoke-direct {v0, p0}, Landroid/view/MenuInflater;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mMenuInflater:Landroid/view/MenuInflater;

    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mMenuInflater:Landroid/view/MenuInflater;

    return-object v0
.end method

.method private initializeInfrastructure()V
    .locals 11

    const/16 v10, 0x11

    const/16 v9, 0xe

    const/4 v8, 0x1

    const/16 v7, 0x10

    const-string v5, "accessibility"

    invoke-virtual {p0, v5}, Lcom/google/android/marvin/talkback/TalkBackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/accessibility/AccessibilityManager;

    iput-object v5, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    new-instance v5, Lcom/google/android/marvin/talkback/SpeechController;

    invoke-direct {v5, p0}, Lcom/google/android/marvin/talkback/SpeechController;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    iput-object v5, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    new-instance v5, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;

    invoke-direct {v5, p0}, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mFeedbackController:Lcom/google/android/marvin/talkback/PreferenceFeedbackController;

    new-instance v5, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;

    invoke-direct {v5, p0}, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mNodeProcessor:Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;

    invoke-static {}, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->getInstance()Lcom/googlecode/eyesfree/utils/ClassLoadingManager;

    move-result-object v5

    invoke-virtual {v5, p0}, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->init(Landroid/content/Context;)V

    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v5, v7, :cond_0

    new-instance v5, Lcom/google/android/marvin/talkback/CursorController;

    invoke-direct {v5, p0}, Lcom/google/android/marvin/talkback/CursorController;-><init>(Landroid/accessibilityservice/AccessibilityService;)V

    iput-object v5, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    iget-object v5, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCursorControllerListener:Lcom/google/android/marvin/talkback/CursorController$CursorControllerListener;

    invoke-virtual {v5, v6}, Lcom/google/android/marvin/talkback/CursorController;->setListener(Lcom/google/android/marvin/talkback/CursorController$CursorControllerListener;)V

    :cond_0
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v5, v7, :cond_1

    new-instance v5, Lcom/google/android/marvin/talkback/FullScreenReadController;

    invoke-direct {v5, p0}, Lcom/google/android/marvin/talkback/FullScreenReadController;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    iput-object v5, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mFullScreenReadController:Lcom/google/android/marvin/talkback/FullScreenReadController;

    iget-object v5, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mEventListeners:Ljava/util/LinkedList;

    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mFullScreenReadController:Lcom/google/android/marvin/talkback/FullScreenReadController;

    invoke-virtual {v5, v6}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    :cond_1
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v5, v7, :cond_2

    new-instance v5, Lcom/google/android/marvin/talkback/ShakeDetector;

    invoke-direct {v5, p0}, Lcom/google/android/marvin/talkback/ShakeDetector;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    iput-object v5, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mShakeDetector:Lcom/google/android/marvin/talkback/ShakeDetector;

    :cond_2
    new-instance v5, Lcom/google/android/marvin/talkback/ProcessorEventQueue;

    invoke-direct {v5, p0}, Lcom/google/android/marvin/talkback/ProcessorEventQueue;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    iput-object v5, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mProcessorEventQueue:Lcom/google/android/marvin/talkback/ProcessorEventQueue;

    iget-object v5, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mProcessorEventQueue:Lcom/google/android/marvin/talkback/ProcessorEventQueue;

    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mTestingListener:Lcom/google/android/marvin/talkback/test/TalkBackListener;

    invoke-virtual {v5, v6}, Lcom/google/android/marvin/talkback/ProcessorEventQueue;->setTestingListener(Lcom/google/android/marvin/talkback/test/TalkBackListener;)V

    iget-object v5, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mEventListeners:Ljava/util/LinkedList;

    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mProcessorEventQueue:Lcom/google/android/marvin/talkback/ProcessorEventQueue;

    invoke-virtual {v5, v6}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    iget-object v5, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mEventListeners:Ljava/util/LinkedList;

    new-instance v6, Lcom/google/android/marvin/talkback/ProcessorScrollPosition;

    invoke-direct {v6, p0}, Lcom/google/android/marvin/talkback/ProcessorScrollPosition;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    invoke-virtual {v5, v6}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v5, v10, :cond_3

    new-instance v5, Lcom/google/android/marvin/talkback/ProcessorSingleTap;

    invoke-direct {v5, p0}, Lcom/google/android/marvin/talkback/ProcessorSingleTap;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mProcessorSingleTap:Lcom/google/android/marvin/talkback/ProcessorSingleTap;

    iget-object v5, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mEventListeners:Ljava/util/LinkedList;

    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mProcessorSingleTap:Lcom/google/android/marvin/talkback/ProcessorSingleTap;

    invoke-virtual {v5, v6}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    :cond_3
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v5, v9, :cond_4

    iget-object v5, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mEventListeners:Ljava/util/LinkedList;

    new-instance v6, Lcom/google/android/marvin/talkback/ProcessorLongHover;

    invoke-direct {v6, p0}, Lcom/google/android/marvin/talkback/ProcessorLongHover;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    invoke-virtual {v5, v6}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    :cond_4
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v5, v7, :cond_5

    new-instance v5, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;

    invoke-direct {v5, p0}, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    iput-object v5, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mProcessorFollowFocus:Lcom/google/android/marvin/talkback/ProcessorFollowFocus;

    iget-object v5, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mEventListeners:Ljava/util/LinkedList;

    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mProcessorFollowFocus:Lcom/google/android/marvin/talkback/ProcessorFollowFocus;

    invoke-virtual {v5, v6}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    :cond_5
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v5, v7, :cond_6

    new-instance v5, Lcom/google/android/marvin/talkback/VolumeMonitor;

    invoke-direct {v5, p0}, Lcom/google/android/marvin/talkback/VolumeMonitor;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    iput-object v5, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mVolumeMonitor:Lcom/google/android/marvin/talkback/VolumeMonitor;

    iget-object v5, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mVolumeMonitor:Lcom/google/android/marvin/talkback/VolumeMonitor;

    invoke-virtual {v5, v6}, Lcom/google/android/marvin/talkback/SpeechController;->setVolumeMonitor(Lcom/google/android/marvin/talkback/VolumeMonitor;)V

    :cond_6
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v5, v10, :cond_7

    iget-object v5, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mEventListeners:Ljava/util/LinkedList;

    new-instance v6, Lcom/google/android/marvin/talkback/ProcessorGestureVibrator;

    invoke-direct {v6, p0}, Lcom/google/android/marvin/talkback/ProcessorGestureVibrator;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    invoke-virtual {v5, v6}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    :cond_7
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v5, v7, :cond_8

    iget-object v5, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mEventListeners:Ljava/util/LinkedList;

    new-instance v6, Lcom/google/android/marvin/talkback/ProcessorWebContent;

    invoke-direct {v6, p0}, Lcom/google/android/marvin/talkback/ProcessorWebContent;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    invoke-virtual {v5, v6}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    :cond_8
    new-instance v5, Lcom/google/android/marvin/talkback/OrientationMonitor;

    invoke-direct {v5, p0}, Lcom/google/android/marvin/talkback/OrientationMonitor;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    iput-object v5, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mOrientationMonitor:Lcom/google/android/marvin/talkback/OrientationMonitor;

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v5, "android.hardware.telephony"

    invoke-virtual {v3, v5}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    new-instance v5, Lcom/google/android/marvin/talkback/CallStateMonitor;

    invoke-direct {v5, p0}, Lcom/google/android/marvin/talkback/CallStateMonitor;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    iput-object v5, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCallStateMonitor:Lcom/google/android/marvin/talkback/CallStateMonitor;

    :cond_9
    const-string v5, "android.hardware.touchscreen"

    invoke-virtual {v3, v5}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-nez v1, :cond_a

    if-eqz v0, :cond_b

    :cond_a
    new-instance v5, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;

    invoke-direct {v5, p0}, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    iput-object v5, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mRingerModeAndScreenMonitor:Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;

    :cond_b
    new-instance v5, Lcom/google/android/marvin/talkback/RadialMenuManager;

    invoke-direct {v5, p0}, Lcom/google/android/marvin/talkback/RadialMenuManager;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    iput-object v5, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mRadialMenuManager:Lcom/google/android/marvin/talkback/RadialMenuManager;

    iget-object v5, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mRadialMenuManager:Lcom/google/android/marvin/talkback/RadialMenuManager;

    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mRadialMenuClient:Lcom/google/android/marvin/talkback/RadialMenuManager$RadialMenuClient;

    invoke-virtual {v5, v6}, Lcom/google/android/marvin/talkback/RadialMenuManager;->setClient(Lcom/google/android/marvin/talkback/RadialMenuManager$RadialMenuClient;)V

    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v5, v9, :cond_c

    new-instance v5, Lcom/google/android/marvin/talkback/speechrules/NodeMenuProcessor;

    invoke-direct {v5, p0}, Lcom/google/android/marvin/talkback/speechrules/NodeMenuProcessor;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    iput-object v5, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mMenuProcessor:Lcom/google/android/marvin/talkback/speechrules/NodeMenuProcessor;

    :cond_c
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    const-string v5, "performCustomGesture"

    invoke-virtual {v2, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mActiveReceiver:Landroid/content/BroadcastReceiver;

    const-string v6, "com.google.android.marvin.feedback.permission.TALKBACK"

    const/4 v7, 0x0

    invoke-virtual {p0, v5, v2, v6, v7}, Lcom/google/android/marvin/talkback/TalkBackService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    new-instance v4, Landroid/content/ComponentName;

    const-class v5, Lcom/google/android/marvin/talkback/ShortcutProxyActivity;

    invoke-direct {v4, p0, v5}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v3, v4, v8, v8}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    return-void
.end method

.method public static isServiceActive()Z
    .locals 1

    sget-boolean v0, Lcom/google/android/marvin/talkback/TalkBackService;->sIsTalkBackSuspended:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private maintainExplorationState(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 3
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    const/16 v1, 0x200

    if-ne v0, v1, :cond_1

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mIsUserTouchExploring:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v1, 0x400

    if-ne v0, v1, :cond_2

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mIsUserTouchExploring:Z

    goto :goto_0

    :cond_2
    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mLastWindowStateChanged:J

    goto :goto_0
.end method

.method private onTouchExplorationEnabled()V
    .locals 5

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mPrefs:Landroid/content/SharedPreferences;

    const-string v3, "first_time_user"

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "first_time_user"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v2, v3, :cond_0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const/high16 v2, 0x4000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/google/android/marvin/talkback/TalkBackService;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private performCustomGesture(II)V
    .locals 4
    .param p1    # I
    .param p2    # I

    invoke-virtual {p0, p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p2}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v3, v1, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->performGesture(Ljava/lang/String;)V

    return-void
.end method

.method private performGesture(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const-string v0, "UNASSIGNED"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "BACK"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/googlecode/eyesfree/compat/accessibilityservice/AccessibilityServiceCompatUtils;->performGlobalAction(Landroid/accessibilityservice/AccessibilityService;I)Z

    goto :goto_0

    :cond_2
    const-string v0, "HOME"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x2

    invoke-static {p0, v0}, Lcom/googlecode/eyesfree/compat/accessibilityservice/AccessibilityServiceCompatUtils;->performGlobalAction(Landroid/accessibilityservice/AccessibilityService;I)Z

    goto :goto_0

    :cond_3
    const-string v0, "RECENTS"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x3

    invoke-static {p0, v0}, Lcom/googlecode/eyesfree/compat/accessibilityservice/AccessibilityServiceCompatUtils;->performGlobalAction(Landroid/accessibilityservice/AccessibilityService;I)Z

    goto :goto_0

    :cond_4
    const-string v0, "NOTIFICATIONS"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x4

    invoke-static {p0, v0}, Lcom/googlecode/eyesfree/compat/accessibilityservice/AccessibilityServiceCompatUtils;->performGlobalAction(Landroid/accessibilityservice/AccessibilityService;I)Z

    goto :goto_0

    :cond_5
    const-string v0, "TALKBACK_BREAKOUT"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mRadialMenuManager:Lcom/google/android/marvin/talkback/RadialMenuManager;

    const/high16 v1, 0x7f0f0000

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/RadialMenuManager;->showRadialMenu(I)Z

    goto :goto_0

    :cond_6
    const-string v0, "LOCAL_BREAKOUT"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mRadialMenuManager:Lcom/google/android/marvin/talkback/RadialMenuManager;

    const v1, 0x7f0f0001

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/RadialMenuManager;->showRadialMenu(I)Z

    goto :goto_0
.end method

.method private processEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 3
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mEventListeners:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/marvin/talkback/TalkBackService$EventListener;

    invoke-interface {v0, p1}, Lcom/google/android/marvin/talkback/TalkBackService$EventListener;->process(Landroid/view/accessibility/AccessibilityEvent;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private registerTouchSettingObserver(Landroid/content/ContentResolver;)V
    .locals 3
    .param p1    # Landroid/content/ContentResolver;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    const-string v1, "touch_exploration_enabled"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mTouchExploreObserver:Landroid/database/ContentObserver;

    invoke-virtual {p1, v0, v1, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    return-void
.end method

.method private reloadPreferences()V
    .locals 9

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mPrefs:Landroid/content/SharedPreferences;

    const v7, 0x7f0a0003

    const/high16 v8, 0x7f0c0000

    invoke-static {v6, v1, v7, v8}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->getBooleanPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSpeakWhenScreenOff:Z

    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mPrefs:Landroid/content/SharedPreferences;

    const v7, 0x7f0a0010

    const v8, 0x7f0c0007

    invoke-static {v6, v1, v7, v8}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->getBooleanPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSpeakCallerId:Z

    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mPrefs:Landroid/content/SharedPreferences;

    const v7, 0x7f0a0004

    const v8, 0x7f0c0001

    invoke-static {v6, v1, v7, v8}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->getBooleanPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)Z

    move-result v2

    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    invoke-virtual {v6, v2}, Lcom/google/android/marvin/talkback/SpeechController;->setSilenceOnProximity(Z)V

    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mProcessorSingleTap:Lcom/google/android/marvin/talkback/ProcessorSingleTap;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mPrefs:Landroid/content/SharedPreferences;

    const v7, 0x7f0a0013

    const v8, 0x7f0c0009

    invoke-static {v6, v1, v7, v8}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->getBooleanPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)Z

    move-result v5

    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mProcessorSingleTap:Lcom/google/android/marvin/talkback/ProcessorSingleTap;

    invoke-virtual {v6, v5}, Lcom/google/android/marvin/talkback/ProcessorSingleTap;->setEnabled(Z)V

    invoke-static {v5}, Lcom/google/android/marvin/talkback/speechrules/NodeHintRule$NodeHintHelper;->updateActionResId(Z)V

    :cond_0
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mShakeDetector:Lcom/google/android/marvin/talkback/ShakeDetector;

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mPrefs:Landroid/content/SharedPreferences;

    const v7, 0x7f0a0014

    const v8, 0x7f0c000a

    invoke-static {v6, v1, v7, v8}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->getBooleanPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)Z

    move-result v4

    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mShakeDetector:Lcom/google/android/marvin/talkback/ShakeDetector;

    invoke-virtual {v6, v4}, Lcom/google/android/marvin/talkback/ShakeDetector;->setEnabled(Z)V

    :cond_1
    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mPrefs:Landroid/content/SharedPreferences;

    const v7, 0x7f0a000b

    const v8, 0x7f0a0024

    invoke-static {v6, v1, v7, v8}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->getIntFromStringPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)I

    move-result v0

    invoke-static {v0}, Lcom/googlecode/eyesfree/utils/LogUtils;->setLogLevel(I)V

    sget-boolean v6, Lcom/google/android/marvin/talkback/TalkBackService;->SUPPORTS_TOUCH_PREF:Z

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mPrefs:Landroid/content/SharedPreferences;

    const v7, 0x7f0a0011

    const v8, 0x7f0c0008

    invoke-static {v6, v1, v7, v8}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->getBooleanPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)Z

    move-result v3

    invoke-direct {p0, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->requestTouchExploration(Z)V

    :cond_2
    return-void
.end method

.method private requestTouchExploration(Z)V
    .locals 5
    .param p1    # Z
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->getServiceInfo()Landroid/accessibilityservice/AccessibilityServiceInfo;

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v2, 0x6

    const-string v3, "Failed to change touch exploration request state, service info was null"

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {p0, v2, v3, v4}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v2, v1, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_2

    const/4 v0, 0x1

    :cond_2
    if-eq v0, p1, :cond_0

    if-eqz p1, :cond_3

    iget v2, v1, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    or-int/lit8 v2, v2, 0x4

    iput v2, v1, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    :goto_1
    invoke-virtual {p0, v1}, Lcom/google/android/marvin/talkback/TalkBackService;->setServiceInfo(Landroid/accessibilityservice/AccessibilityServiceInfo;)V

    goto :goto_0

    :cond_3
    iget v2, v1, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    and-int/lit8 v2, v2, -0x5

    iput v2, v1, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    goto :goto_1
.end method

.method private resumeInfrastructure()V
    .locals 5

    const/4 v3, 0x0

    sget-boolean v1, Lcom/google/android/marvin/talkback/TalkBackService;->sIsTalkBackSuspended:Z

    if-nez v1, :cond_0

    const/4 v1, 0x6

    const-string v2, "Attempted to resume while not suspended"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p0, v1, v2, v3}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    sput-boolean v3, Lcom/google/android/marvin/talkback/TalkBackService;->sIsTalkBackSuspended:Z

    new-instance v0, Landroid/accessibilityservice/AccessibilityServiceInfo;

    invoke-direct {v0}, Landroid/accessibilityservice/AccessibilityServiceInfo;-><init>()V

    const/4 v1, -0x1

    iput v1, v0, Landroid/accessibilityservice/AccessibilityServiceInfo;->eventTypes:I

    iget v1, v0, Landroid/accessibilityservice/AccessibilityServiceInfo;->feedbackType:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Landroid/accessibilityservice/AccessibilityServiceInfo;->feedbackType:I

    iget v1, v0, Landroid/accessibilityservice/AccessibilityServiceInfo;->feedbackType:I

    or-int/lit8 v1, v1, 0x4

    iput v1, v0, Landroid/accessibilityservice/AccessibilityServiceInfo;->feedbackType:I

    iget v1, v0, Landroid/accessibilityservice/AccessibilityServiceInfo;->feedbackType:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v0, Landroid/accessibilityservice/AccessibilityServiceInfo;->feedbackType:I

    iget v1, v0, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    iget v1, v0, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    or-int/lit8 v1, v1, 0x8

    iput v1, v0, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    iget v1, v0, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    or-int/lit8 v1, v1, 0x10

    iput v1, v0, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    const-wide/16 v1, 0x0

    iput-wide v1, v0, Landroid/accessibilityservice/AccessibilityServiceInfo;->notificationTimeout:J

    sget-boolean v1, Lcom/google/android/marvin/talkback/TalkBackService;->SUPPORTS_TOUCH_PREF:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mPrefs:Landroid/content/SharedPreferences;

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0011

    const v4, 0x7f0c0008

    invoke-static {v1, v2, v3, v4}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->getBooleanPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)Z

    move-result v1

    if-eqz v1, :cond_1

    iget v1, v0, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    or-int/lit8 v1, v1, 0x4

    iput v1, v0, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/TalkBackService;->setServiceInfo(Landroid/accessibilityservice/AccessibilityServiceInfo;)V

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCallStateMonitor:Lcom/google/android/marvin/talkback/CallStateMonitor;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCallStateMonitor:Lcom/google/android/marvin/talkback/CallStateMonitor;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCallStateMonitor:Lcom/google/android/marvin/talkback/CallStateMonitor;

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/CallStateMonitor;->getFilter()Landroid/content/IntentFilter;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_2
    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mRingerModeAndScreenMonitor:Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mRingerModeAndScreenMonitor:Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mRingerModeAndScreenMonitor:Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->getFilter()Landroid/content/IntentFilter;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_3
    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mRadialMenuManager:Lcom/google/android/marvin/talkback/RadialMenuManager;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mRadialMenuManager:Lcom/google/android/marvin/talkback/RadialMenuManager;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mRadialMenuManager:Lcom/google/android/marvin/talkback/RadialMenuManager;

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/RadialMenuManager;->getFilter()Landroid/content/IntentFilter;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_4
    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mVolumeMonitor:Lcom/google/android/marvin/talkback/VolumeMonitor;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mVolumeMonitor:Lcom/google/android/marvin/talkback/VolumeMonitor;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mVolumeMonitor:Lcom/google/android/marvin/talkback/VolumeMonitor;

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/VolumeMonitor;->getFilter()Landroid/content/IntentFilter;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_5
    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSharedPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->reloadPreferences()V

    goto/16 :goto_0
.end method

.method private resumeTalkBack()V
    .locals 3

    sget-boolean v0, Lcom/google/android/marvin/talkback/TalkBackService;->sIsTalkBackSuspended:Z

    if-nez v0, :cond_0

    const/4 v0, 0x6

    const-string v1, "Attempted to resume TalkBack when not suspended."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v1, v2}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSuspendedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/TalkBackService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/TalkBackService;->stopForeground(Z)V

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->resumeInfrastructure()V

    goto :goto_0
.end method

.method private shouldDropEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 13
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;

    const/4 v12, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v6

    :cond_1
    sget-boolean v8, Lcom/google/android/marvin/talkback/TalkBackService;->sIsTalkBackSuspended:Z

    if-nez v8, :cond_0

    iget-object v8, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mLastSpokenEvent:Landroid/view/accessibility/AccessibilityEvent;

    invoke-static {v8, p1}, Lcom/googlecode/eyesfree/utils/AccessibilityEventUtils;->eventEquals(Landroid/view/accessibility/AccessibilityEvent;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v8

    if-eqz v8, :cond_2

    const-string v8, "Drop duplicate event"

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {p0, v12, v8, v7}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v8

    const/16 v9, 0x800

    if-eq v8, v9, :cond_0

    iget-object v8, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-static {v8}, Landroid/support/v4/view/accessibility/AccessibilityManagerCompat;->isTouchExplorationEnabled(Landroid/view/accessibility/AccessibilityManager;)Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v8

    and-int/lit16 v8, v8, 0x100c

    if-eqz v8, :cond_3

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventTime()J

    move-result-wide v8

    iget-wide v10, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mLastWindowStateChanged:J

    sub-long/2addr v8, v10

    const-wide/16 v10, 0x64

    cmp-long v8, v8, v10

    if-gez v8, :cond_3

    const-string v8, "Drop event after window state change"

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {p0, v12, v8, v7}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v8

    const/16 v9, 0x40

    if-ne v8, v9, :cond_4

    move v1, v6

    :goto_1
    iget-object v8, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCallStateMonitor:Lcom/google/android/marvin/talkback/CallStateMonitor;

    if-eqz v8, :cond_5

    iget-object v8, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCallStateMonitor:Lcom/google/android/marvin/talkback/CallStateMonitor;

    invoke-virtual {v8}, Lcom/google/android/marvin/talkback/CallStateMonitor;->getCurrentCallState()I

    move-result v8

    if-eqz v8, :cond_5

    move v2, v6

    :goto_2
    iget-object v8, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mRingerModeAndScreenMonitor:Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;

    if-eqz v8, :cond_6

    iget-object v8, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mRingerModeAndScreenMonitor:Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;

    invoke-virtual {v8}, Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;->isScreenOff()Z

    move-result v8

    if-eqz v8, :cond_6

    move v3, v6

    :goto_3
    iget-boolean v8, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSpeakCallerId:Z

    if-eqz v8, :cond_7

    iget-object v8, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCallStateMonitor:Lcom/google/android/marvin/talkback/CallStateMonitor;

    if-eqz v8, :cond_7

    iget-object v8, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCallStateMonitor:Lcom/google/android/marvin/talkback/CallStateMonitor;

    invoke-virtual {v8}, Lcom/google/android/marvin/talkback/CallStateMonitor;->getCurrentCallState()I

    move-result v8

    if-ne v8, v6, :cond_7

    move v4, v6

    :goto_4
    if-eqz v3, :cond_9

    iget-boolean v8, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSpeakWhenScreenOff:Z

    if-nez v8, :cond_8

    if-nez v4, :cond_9

    const-string v8, "Drop event due to screen state and user pref"

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {p0, v12, v8, v7}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_4
    move v1, v7

    goto :goto_1

    :cond_5
    move v2, v7

    goto :goto_2

    :cond_6
    move v3, v7

    goto :goto_3

    :cond_7
    move v4, v7

    goto :goto_4

    :cond_8
    if-nez v1, :cond_9

    const-string v8, "Drop non-notification event due to screen state"

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {p0, v12, v8, v7}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_9
    const v8, 0x8080

    invoke-static {p1, v8}, Lcom/googlecode/eyesfree/utils/AccessibilityEventUtils;->eventMatchesAnyType(Landroid/view/accessibility/AccessibilityEvent;I)Z

    move-result v0

    iget-object v8, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mRadialMenuManager:Lcom/google/android/marvin/talkback/RadialMenuManager;

    invoke-virtual {v8}, Lcom/google/android/marvin/talkback/RadialMenuManager;->isRadialMenuShowing()Z

    move-result v8

    if-eqz v8, :cond_a

    if-nez v0, :cond_a

    move v5, v6

    :goto_5
    if-eqz v5, :cond_b

    const-string v8, "Drop event due to radial menu state"

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {p0, v12, v8, v7}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_a
    move v5, v7

    goto :goto_5

    :cond_b
    if-eqz v1, :cond_d

    iget-boolean v8, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mIsUserTouchExploring:Z

    if-nez v8, :cond_c

    if-eqz v2, :cond_d

    :cond_c
    const-string v8, "Drop notification due to touch or phone state"

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {p0, v12, v8, v7}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_d
    move v6, v7

    goto/16 :goto_0
.end method

.method private shutdownInfrastructure()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/CursorController;->shutdown()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mFullScreenReadController:Lcom/google/android/marvin/talkback/FullScreenReadController;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mFullScreenReadController:Lcom/google/android/marvin/talkback/FullScreenReadController;

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/FullScreenReadController;->shutdown()V

    :cond_1
    invoke-static {}, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->getInstance()Lcom/googlecode/eyesfree/utils/ClassLoadingManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->shutdown()V

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mFeedbackController:Lcom/google/android/marvin/talkback/PreferenceFeedbackController;

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;->shutdown()V

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/SpeechController;->shutdown()V

    return-void
.end method

.method private suspendInfrastructure()V
    .locals 7

    const/4 v6, 0x1

    sget-boolean v4, Lcom/google/android/marvin/talkback/TalkBackService;->sIsTalkBackSuspended:Z

    if-eqz v4, :cond_1

    const/4 v4, 0x6

    const-string v5, "Attempted to suspend while already suspended"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {p0, v4, v5, v6}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sput-boolean v6, Lcom/google/android/marvin/talkback/TalkBackService;->sIsTalkBackSuspended:Z

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->interruptAllFeedback()V

    new-instance v4, Landroid/accessibilityservice/AccessibilityServiceInfo;

    invoke-direct {v4}, Landroid/accessibilityservice/AccessibilityServiceInfo;-><init>()V

    invoke-virtual {p0, v4}, Lcom/google/android/marvin/talkback/TalkBackService;->setServiceInfo(Landroid/accessibilityservice/AccessibilityServiceInfo;)V

    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v5, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSharedPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v4, v5}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mActiveReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v4}, Lcom/google/android/marvin/talkback/TalkBackService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCallStateMonitor:Lcom/google/android/marvin/talkback/CallStateMonitor;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCallStateMonitor:Lcom/google/android/marvin/talkback/CallStateMonitor;

    invoke-virtual {p0, v4}, Lcom/google/android/marvin/talkback/TalkBackService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_2
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mRingerModeAndScreenMonitor:Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mRingerModeAndScreenMonitor:Lcom/google/android/marvin/talkback/RingerModeAndScreenMonitor;

    invoke-virtual {p0, v4}, Lcom/google/android/marvin/talkback/TalkBackService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_3
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mRadialMenuManager:Lcom/google/android/marvin/talkback/RadialMenuManager;

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mRadialMenuManager:Lcom/google/android/marvin/talkback/RadialMenuManager;

    invoke-virtual {p0, v4}, Lcom/google/android/marvin/talkback/TalkBackService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mRadialMenuManager:Lcom/google/android/marvin/talkback/RadialMenuManager;

    invoke-virtual {v4}, Lcom/google/android/marvin/talkback/RadialMenuManager;->clearCache()V

    :cond_4
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mVolumeMonitor:Lcom/google/android/marvin/talkback/VolumeMonitor;

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mVolumeMonitor:Lcom/google/android/marvin/talkback/VolumeMonitor;

    invoke-virtual {p0, v4}, Lcom/google/android/marvin/talkback/TalkBackService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mVolumeMonitor:Lcom/google/android/marvin/talkback/VolumeMonitor;

    invoke-virtual {v4}, Lcom/google/android/marvin/talkback/VolumeMonitor;->releaseControl()V

    :cond_5
    sget-boolean v4, Lcom/google/android/marvin/talkback/TalkBackService;->SUPPORTS_TOUCH_PREF:Z

    if-eqz v4, :cond_6

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mTouchExploreObserver:Landroid/database/ContentObserver;

    invoke-virtual {v2, v4}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    new-instance v3, Landroid/content/ComponentName;

    const-class v4, Lcom/google/android/marvin/talkback/ShortcutProxyActivity;

    invoke-direct {v3, p0, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v3, v6, v6}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    const-string v4, "notification"

    invoke-virtual {p0, v4}, Lcom/google/android/marvin/talkback/TalkBackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-virtual {v0}, Landroid/app/NotificationManager;->cancelAll()V

    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mTtsOverlay:Lcom/google/android/marvin/talkback/TextToSpeechOverlay;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mTtsOverlay:Lcom/google/android/marvin/talkback/TextToSpeechOverlay;

    invoke-virtual {v4}, Lcom/google/android/marvin/talkback/TextToSpeechOverlay;->hide()V

    goto :goto_0
.end method

.method private suspendTalkBack()V
    .locals 9

    const/4 v8, 0x1

    const/4 v7, 0x0

    sget-boolean v4, Lcom/google/android/marvin/talkback/TalkBackService;->sIsTalkBackSuspended:Z

    if-eqz v4, :cond_0

    const/4 v4, 0x6

    const-string v5, "Attempted to suspend TalkBack while already suspended."

    new-array v6, v7, [Ljava/lang/Object;

    invoke-static {p0, v4, v5, v6}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mFeedbackController:Lcom/google/android/marvin/talkback/PreferenceFeedbackController;

    const v5, 0x7f080032

    invoke-virtual {v4, v5}, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;->playCustomSound(I)V

    sput-boolean v8, Lcom/google/android/marvin/talkback/TalkBackService;->sIsTalkBackSuspended:Z

    sget-boolean v4, Lcom/google/android/marvin/talkback/TalkBackService;->SUPPORTS_TOUCH_PREF:Z

    if-eqz v4, :cond_1

    invoke-direct {p0, v7}, Lcom/google/android/marvin/talkback/TalkBackService;->requestTouchExploration(Z)V

    :cond_1
    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    invoke-virtual {v4}, Lcom/google/android/marvin/talkback/CursorController;->clearCursor()V

    :cond_2
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v4, "com.google.android.marvin.talkback.RESUME_FEEDBACK"

    invoke-virtual {v0, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v4, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSuspendedReceiver:Landroid/content/BroadcastReceiver;

    const-string v5, "com.google.android.marvin.feedback.permission.TALKBACK"

    const/4 v6, 0x0

    invoke-virtual {p0, v4, v0, v5, v6}, Lcom/google/android/marvin/talkback/TalkBackService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->suspendInfrastructure()V

    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.google.android.marvin.talkback.RESUME_FEEDBACK"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p0, v7, v3, v7}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    new-instance v4, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v4, p0}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    const v5, 0x7f0a00d9

    invoke-virtual {p0, v5}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    const v5, 0x7f0a00da

    invoke-virtual {p0, v5}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->setPriority(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    const/high16 v5, 0x7f020000

    invoke-virtual {v4, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/support/v4/app/NotificationCompat$Builder;->setOngoing(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    const-wide/16 v5, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setWhen(J)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    const v4, 0x7f08003c

    invoke-virtual {p0, v4, v1}, Lcom/google/android/marvin/talkback/TalkBackService;->startForeground(ILandroid/app/Notification;)V

    goto :goto_0
.end method


# virtual methods
.method public addEventListener(Lcom/google/android/marvin/talkback/TalkBackService$EventListener;)V
    .locals 1
    .param p1    # Lcom/google/android/marvin/talkback/TalkBackService$EventListener;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mEventListeners:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public getCursorController()Lcom/google/android/marvin/talkback/CursorController;
    .locals 2

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "mCursorController has not been initialized"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    return-object v0
.end method

.method public getFeedbackController()Lcom/google/android/marvin/talkback/PreferenceFeedbackController;
    .locals 2

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mFeedbackController:Lcom/google/android/marvin/talkback/PreferenceFeedbackController;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "mFeedbackController has not been initialized"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mFeedbackController:Lcom/google/android/marvin/talkback/PreferenceFeedbackController;

    return-object v0
.end method

.method public getFullScreenReadController()Lcom/google/android/marvin/talkback/FullScreenReadController;
    .locals 2

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mFullScreenReadController:Lcom/google/android/marvin/talkback/FullScreenReadController;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "mFullScreenReadController has not been initialized"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mFullScreenReadController:Lcom/google/android/marvin/talkback/FullScreenReadController;

    return-object v0
.end method

.method public getNodeProcessor()Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;
    .locals 2

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mNodeProcessor:Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "mNodeProcessor has not been initialized"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mNodeProcessor:Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;

    return-object v0
.end method

.method public getOrientationMonitor()Lcom/google/android/marvin/talkback/OrientationMonitor;
    .locals 2

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mOrientationMonitor:Lcom/google/android/marvin/talkback/OrientationMonitor;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "mOrientationMonitor has not been initialized"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mOrientationMonitor:Lcom/google/android/marvin/talkback/OrientationMonitor;

    return-object v0
.end method

.method public getShakeDetector()Lcom/google/android/marvin/talkback/ShakeDetector;
    .locals 1

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mShakeDetector:Lcom/google/android/marvin/talkback/ShakeDetector;

    return-object v0
.end method

.method public getSpeechController()Lcom/google/android/marvin/talkback/SpeechController;
    .locals 2

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "mSpeechController has not been initialized"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    return-object v0
.end method

.method public interruptAllFeedback()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/CursorController;->getCursor()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->hasWebContent(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->isScriptInjectionEnabled(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, -0x3

    invoke-static {v0, v1}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->performSpecialAction(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Z

    :cond_0
    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mFullScreenReadController:Lcom/google/android/marvin/talkback/FullScreenReadController;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mFullScreenReadController:Lcom/google/android/marvin/talkback/FullScreenReadController;

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/FullScreenReadController;->interrupt()V

    :cond_1
    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/SpeechController;->interrupt()V

    :cond_2
    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mFeedbackController:Lcom/google/android/marvin/talkback/PreferenceFeedbackController;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mFeedbackController:Lcom/google/android/marvin/talkback/PreferenceFeedbackController;

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;->interrupt()V

    :cond_3
    return-void
.end method

.method public onAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mTestingListener:Lcom/google/android/marvin/talkback/test/TalkBackListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mTestingListener:Lcom/google/android/marvin/talkback/test/TalkBackListener;

    invoke-interface {v0, p1}, Lcom/google/android/marvin/talkback/test/TalkBackListener;->onAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/TalkBackService;->shouldDropEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/TalkBackService;->maintainExplorationState(Landroid/view/accessibility/AccessibilityEvent;)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_2

    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/TalkBackService;->cacheEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/TalkBackService;->processEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1    # Landroid/content/res/Configuration;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mOrientationMonitor:Lcom/google/android/marvin/talkback/OrientationMonitor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mOrientationMonitor:Lcom/google/android/marvin/talkback/OrientationMonitor;

    invoke-virtual {v0, p1}, Lcom/google/android/marvin/talkback/OrientationMonitor;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    :cond_0
    return-void
.end method

.method public onCreate()V
    .locals 1

    invoke-super {p0}, Landroid/accessibilityservice/AccessibilityService;->onCreate()V

    sput-object p0, Lcom/google/android/marvin/talkback/TalkBackService;->sInstance:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mPrefs:Landroid/content/SharedPreferences;

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->initializeInfrastructure()V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/accessibilityservice/AccessibilityService;->onDestroy()V

    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/marvin/talkback/TalkBackService;->sInstance:Lcom/google/android/marvin/talkback/TalkBackService;

    sget-boolean v0, Lcom/google/android/marvin/talkback/TalkBackService;->sIsTalkBackSuspended:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->suspendInfrastructure()V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->shutdownInfrastructure()V

    return-void
.end method

.method public onGesture(I)Z
    .locals 8
    .param p1    # I

    const/4 v7, 0x0

    const/4 v6, 0x1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    if-nez v2, :cond_0

    :goto_0
    return v6

    :cond_0
    const/4 v2, 0x2

    const-string v3, "Recognized gesture %s"

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {p0, v2, v3, v4}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mFeedbackController:Lcom/google/android/marvin/talkback/PreferenceFeedbackController;

    const v3, 0x7f080030

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;->playCustomSound(I)V

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-gt v2, v3, :cond_1

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->interruptAllFeedback()V

    :cond_1
    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mRadialMenuManager:Lcom/google/android/marvin/talkback/RadialMenuManager;

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/RadialMenuManager;->dismissAll()V

    const/4 v0, 0x1

    const/4 v1, 0x0

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mFeedbackController:Lcom/google/android/marvin/talkback/PreferenceFeedbackController;

    const v3, 0x7f08002e

    invoke-virtual {v2, v1, v7, v3}, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;->playCustomSoundConditional(ZII)V

    goto :goto_0

    :pswitch_0
    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    invoke-virtual {v2, v6}, Lcom/google/android/marvin/talkback/CursorController;->previous(Z)Z

    move-result v1

    goto :goto_1

    :pswitch_1
    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    invoke-virtual {v2, v6}, Lcom/google/android/marvin/talkback/CursorController;->next(Z)Z

    move-result v1

    goto :goto_1

    :pswitch_2
    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/CursorController;->previousGranularity()Z

    move-result v1

    goto :goto_1

    :pswitch_3
    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/CursorController;->nextGranularity()Z

    move-result v1

    goto :goto_1

    :pswitch_4
    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/CursorController;->less()Z

    move-result v1

    goto :goto_1

    :pswitch_5
    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/CursorController;->more()Z

    move-result v1

    goto :goto_1

    :cond_2
    packed-switch p1, :pswitch_data_1

    goto :goto_0

    :pswitch_6
    const v2, 0x7f0a0020

    const v3, 0x7f0a002e

    invoke-direct {p0, v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->performCustomGesture(II)V

    goto :goto_0

    :pswitch_7
    const v2, 0x7f0a001b

    const v3, 0x7f0a0029

    invoke-direct {p0, v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->performCustomGesture(II)V

    goto :goto_0

    :pswitch_8
    const v2, 0x7f0a001c

    const v3, 0x7f0a002a

    invoke-direct {p0, v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->performCustomGesture(II)V

    goto/16 :goto_0

    :pswitch_9
    const v2, 0x7f0a0019

    const v3, 0x7f0a0027

    invoke-direct {p0, v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->performCustomGesture(II)V

    goto/16 :goto_0

    :pswitch_a
    const v2, 0x7f0a001a

    const v3, 0x7f0a0028

    invoke-direct {p0, v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->performCustomGesture(II)V

    goto/16 :goto_0

    :pswitch_b
    const v2, 0x7f0a001d

    const v3, 0x7f0a002b

    invoke-direct {p0, v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->performCustomGesture(II)V

    goto/16 :goto_0

    :pswitch_c
    const v2, 0x7f0a001e

    const v3, 0x7f0a002c

    invoke-direct {p0, v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->performCustomGesture(II)V

    goto/16 :goto_0

    :pswitch_d
    const v2, 0x7f0a001f

    const v3, 0x7f0a002d

    invoke-direct {p0, v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->performCustomGesture(II)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x9
        :pswitch_6
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_9
        :pswitch_a
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public onInterrupt()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->interruptAllFeedback()V

    return-void
.end method

.method protected onServiceConnected()V
    .locals 4

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->resumeInfrastructure()V

    new-instance v0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->showPendingNotifications()V

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->checkUpdate()V

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v2, v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "touch_exploration_enabled"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackService;->onTouchExplorationEnabled()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, v1}, Lcom/google/android/marvin/talkback/TalkBackService;->registerTouchSettingObserver(Landroid/content/ContentResolver;)V

    goto :goto_0
.end method

.method postRemoveEventListener(Lcom/google/android/marvin/talkback/TalkBackService$EventListener;)V
    .locals 2
    .param p1    # Lcom/google/android/marvin/talkback/TalkBackService$EventListener;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/marvin/talkback/TalkBackService$2;

    invoke-direct {v1, p0, p1}, Lcom/google/android/marvin/talkback/TalkBackService$2;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;Lcom/google/android/marvin/talkback/TalkBackService$EventListener;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
