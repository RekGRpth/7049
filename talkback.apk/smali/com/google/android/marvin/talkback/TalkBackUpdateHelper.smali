.class Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;
.super Ljava/lang/Object;
.source "TalkBackUpdateHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/TalkBackUpdateHelper$ExploreByTouchUpdateHelper;
    }
.end annotation


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field private final mNotificationManager:Landroid/app/NotificationManager;

.field private final mNotificationRunnable:Ljava/lang/Runnable;

.field private final mService:Lcom/google/android/marvin/talkback/TalkBackService;

.field private final mSharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 2
    .param p1    # Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper$1;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper$1;-><init>(Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mNotificationRunnable:Ljava/lang/Runnable;

    iput-object p1, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/TalkBackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mNotificationManager:Landroid/app/NotificationManager;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mSharedPreferences:Landroid/content/SharedPreferences;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;)V
    .locals 0
    .param p0    # Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->displayGestureChangedNotification()V

    return-void
.end method

.method private deprecateStringPreference(Landroid/content/SharedPreferences$Editor;II)V
    .locals 4
    .param p1    # Landroid/content/SharedPreferences$Editor;
    .param p2    # I
    .param p3    # I

    iget-object v3, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v3, p2}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v3, p3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v3, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    return-void
.end method

.method private displayGestureChangedNotification()V
    .locals 10

    const v9, 0x7f0a00d7

    const/4 v8, 0x0

    new-instance v3, Landroid/content/Intent;

    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const-class v7, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;

    invoke-direct {v3, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v6, 0x10000000

    invoke-virtual {v3, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const/high16 v6, 0x800000

    invoke-virtual {v3, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const/high16 v7, 0x8000000

    invoke-static {v6, v8, v3, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v6, v9}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v6, v9}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v7, 0x7f0a00d8

    invoke-virtual {v6, v7}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v6, Landroid/support/v4/app/NotificationCompat$Builder;

    iget-object v7, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-direct {v6, v7}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    const/high16 v7, 0x7f020000

    invoke-virtual {v6, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    invoke-virtual {v6, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    invoke-virtual {v6, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    const-wide/16 v7, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/support/v4/app/NotificationCompat$Builder;->setWhen(J)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v2

    iget v6, v2, Landroid/app/Notification;->defaults:I

    or-int/lit8 v6, v6, 0x1

    iput v6, v2, Landroid/app/Notification;->defaults:I

    iget v6, v2, Landroid/app/Notification;->flags:I

    or-int/lit8 v6, v6, 0x2

    iput v6, v2, Landroid/app/Notification;->flags:I

    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mNotificationManager:Landroid/app/NotificationManager;

    const/4 v7, 0x2

    invoke-virtual {v6, v7, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void
.end method

.method private displaySettingsAvailableNotification()V
    .locals 11

    const v10, 0x7f0a00d5

    const/4 v9, 0x1

    new-instance v2, Landroid/content/Intent;

    const-string v6, "android.intent.action.VIEW"

    invoke-direct {v2, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v7, 0x7f0a0033

    invoke-virtual {v6, v7}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const/4 v7, 0x0

    const/high16 v8, 0x8000000

    invoke-static {v6, v7, v2, v8}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v6, v10}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v6, v10}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v7, 0x7f0a00d6

    invoke-virtual {v6, v7}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v6, Landroid/support/v4/app/NotificationCompat$Builder;

    iget-object v7, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-direct {v6, v7}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    const/high16 v7, 0x7f020000

    invoke-virtual {v6, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    invoke-virtual {v6, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    invoke-virtual {v6, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Landroid/support/v4/app/NotificationCompat$Builder;->setWhen(J)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v3

    iget v6, v3, Landroid/app/Notification;->defaults:I

    or-int/lit8 v6, v6, 0x1

    iput v6, v3, Landroid/app/Notification;->defaults:I

    iget v6, v3, Landroid/app/Notification;->flags:I

    or-int/lit8 v6, v6, 0x12

    iput v6, v3, Landroid/app/Notification;->flags:I

    iget-object v6, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mNotificationManager:Landroid/app/NotificationManager;

    invoke-virtual {v6, v9, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void
.end method

.method private needsExploreByTouchHelper(I)Z
    .locals 3
    .param p1    # I

    const/4 v0, 0x1

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-ne v1, v2, :cond_0

    const/16 v1, 0x44

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "first_time_user"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private notifyUserOfGestureChanges()V
    .locals 5

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const v1, 0x7f0a001b

    const v2, 0x7f0a0031

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->deprecateStringPreference(Landroid/content/SharedPreferences$Editor;II)V

    const v1, 0x7f0a001c

    const v2, 0x7f0a0032

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->deprecateStringPreference(Landroid/content/SharedPreferences$Editor;II)V

    const v1, 0x7f0a0019

    const v2, 0x7f0a002f

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->deprecateStringPreference(Landroid/content/SharedPreferences$Editor;II)V

    const v1, 0x7f0a001a

    const v2, 0x7f0a0030

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->deprecateStringPreference(Landroid/content/SharedPreferences$Editor;II)V

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v2, 0x7f0a0021

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mNotificationRunnable:Ljava/lang/Runnable;

    const-wide/16 v3, 0x1388

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private remapContinuousReadingMenu()V
    .locals 11

    iget-object v9, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v9}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v8, "READ_ALL_BREAKOUT"

    const-string v7, "LOCAL_BREAKOUT"

    const/16 v9, 0x8

    new-array v2, v9, [I

    fill-array-data v2, :array_0

    move-object v0, v2

    array-length v5, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v5, :cond_1

    aget v4, v0, v3

    iget-object v9, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v9, v4}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v6

    iget-object v9, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v10, ""

    invoke-interface {v9, v6, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "READ_ALL_BREAKOUT"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    const-string v9, "LOCAL_BREAKOUT"

    invoke-interface {v1, v6, v9}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void

    :array_0
    .array-data 4
        0x7f0a001b
        0x7f0a001c
        0x7f0a001f
        0x7f0a0020
        0x7f0a001d
        0x7f0a001e
        0x7f0a0019
        0x7f0a001a
    .end array-data
.end method


# virtual methods
.method public checkUpdate()V
    .locals 12

    const/4 v11, 0x0

    iget-object v8, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v9, "app_version"

    const/4 v10, -0x1

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v7

    iget-object v8, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v8}, Lcom/google/android/marvin/talkback/TalkBackService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    :try_start_0
    iget-object v8, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v8}, Lcom/google/android/marvin/talkback/TalkBackService;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v4, v8, v9}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    iget v0, v3, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v7, v0, :cond_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    :cond_0
    iget-object v8, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v8}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v8, "app_version"

    invoke-interface {v2, v8, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const/16 v8, 0xc

    if-ge v7, v8, :cond_1

    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v9, 0xe

    if-ge v8, v9, :cond_1

    iget-object v8, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const-string v9, "com.marvin.preferences"

    invoke-static {v8, v9}, Lcom/googlecode/eyesfree/utils/PackageManagerUtils;->hasPackage(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_1

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->displaySettingsAvailableNotification()V

    :cond_1
    const/16 v8, 0x2a

    if-ge v7, v8, :cond_3

    iget-object v8, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const-string v9, "com.google.android.marvin.kickback"

    invoke-static {v8, v9}, Lcom/googlecode/eyesfree/utils/PackageManagerUtils;->hasPackage(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    iget-object v8, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v9, 0x7f0a0007

    invoke-virtual {v8, v9}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v2, v6, v11}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    :cond_2
    iget-object v8, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const-string v9, "com.google.android.marvin.soundback"

    invoke-static {v8, v9}, Lcom/googlecode/eyesfree/utils/PackageManagerUtils;->hasPackage(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    iget-object v8, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v9, 0x7f0a0008

    invoke-virtual {v8, v9}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5, v11}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    :cond_3
    invoke-direct {p0, v7}, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->needsExploreByTouchHelper(I)Z

    move-result v8

    if-eqz v8, :cond_4

    iget-object v8, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    new-instance v9, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper$ExploreByTouchUpdateHelper;

    iget-object v10, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-direct {v9, v10}, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper$ExploreByTouchUpdateHelper;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;)V

    invoke-virtual {v8, v9}, Lcom/google/android/marvin/talkback/TalkBackService;->addEventListener(Lcom/google/android/marvin/talkback/TalkBackService$EventListener;)V

    :cond_4
    const/16 v8, 0x44

    if-lt v7, v8, :cond_5

    const/16 v8, 0x4a

    if-ge v7, v8, :cond_5

    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v9, 0x10

    if-lt v8, v9, :cond_5

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->notifyUserOfGestureChanges()V

    :cond_5
    const/16 v8, 0x54

    if-ge v7, v8, :cond_6

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->remapContinuousReadingMenu()V

    :cond_6
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_0
.end method

.method public showPendingNotifications()V
    .locals 5

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mSharedPreferences:Landroid/content/SharedPreferences;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const v3, 0x7f0a0021

    invoke-virtual {v2, v3}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackUpdateHelper;->mNotificationRunnable:Ljava/lang/Runnable;

    const-wide/16 v3, 0x1388

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method
