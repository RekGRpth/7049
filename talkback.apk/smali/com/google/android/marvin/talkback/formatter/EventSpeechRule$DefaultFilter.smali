.class Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFilter;
.super Ljava/lang/Object;
.source "EventSpeechRule.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DefaultFilter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;Landroid/content/Context;Lorg/w3c/dom/Node;)V
    .locals 9
    .param p2    # Landroid/content/Context;
    .param p3    # Lorg/w3c/dom/Node;

    iput-object p1, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFilter;->this$0:Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-interface {p3}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v3

    const/4 v2, 0x0

    invoke-interface {v3}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v1

    :goto_0
    if-ge v2, v1, :cond_2

    invoke-interface {v3, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v7

    const/4 v8, 0x1

    if-eq v7, v8, :cond_1

    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    # invokes: Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getUnqualifiedNodeName(Lorg/w3c/dom/Node;)Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->access$000(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v6

    # invokes: Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getTextContent(Lorg/w3c/dom/Node;)Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->access$100(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v5

    new-instance v4, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;

    invoke-direct {v4, p2, v6, v5}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    # getter for: Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mPropertyMatchers:Ljava/util/LinkedHashMap;
    invoke-static {p1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->access$200(Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;)Ljava/util/LinkedHashMap;

    move-result-object v7

    invoke-virtual {v7, v6, v4}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v7, "packageName"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    # setter for: Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mPackageName:Ljava/lang/String;
    invoke-static {p1, v5}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->access$302(Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_1

    :cond_2
    return-void
.end method

.method private evaluatePropertyForEvent(Landroid/content/Context;Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;Landroid/view/accessibility/AccessibilityEvent;Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;)Z
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;
    .param p3    # Landroid/view/accessibility/AccessibilityEvent;
    .param p4    # Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    const/4 v7, 0x0

    # getter for: Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->mPropertyName:Ljava/lang/String;
    invoke-static {p2}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->access$400(Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;)Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFilter;->this$0:Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;

    # invokes: Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getPropertyValue(Landroid/content/Context;Ljava/lang/String;Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/Object;
    invoke-static {v5, p1, v3, p3}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->access$500(Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;Landroid/content/Context;Ljava/lang/String;Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/Object;

    move-result-object v4

    const-string v5, "className"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "classNameStrict"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    :cond_0
    invoke-virtual {p3}, Landroid/view/accessibility/AccessibilityEvent;->getPackageName()Ljava/lang/CharSequence;

    move-result-object v0

    iget-object v5, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFilter;->this$0:Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;

    # getter for: Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mPropertyMatchers:Ljava/util/LinkedHashMap;
    invoke-static {v5}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->access$200(Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;)Ljava/util/LinkedHashMap;

    move-result-object v5

    const-string v6, "packageName"

    invoke-virtual {v5, v6}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->getAcceptedValues()[Ljava/lang/Object;

    move-result-object v5

    aget-object v1, v5, v7

    check-cast v1, Ljava/lang/String;

    :goto_0
    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v0, v5, v7

    const/4 v6, 0x1

    aput-object v1, v5, v6

    invoke-virtual {p2, v4, v5}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->accept(Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v5

    :goto_1
    return v5

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    new-array v5, v7, [Ljava/lang/Object;

    invoke-virtual {p2, v4, v5}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;->accept(Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v5

    goto :goto_1
.end method


# virtual methods
.method public accept(Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/TalkBackService;)Z
    .locals 4
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2    # Lcom/google/android/marvin/talkback/TalkBackService;

    new-instance v2, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    invoke-direct {v2, p1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;-><init>(Ljava/lang/Object;)V

    iget-object v3, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFilter;->this$0:Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;

    # getter for: Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mPropertyMatchers:Ljava/util/LinkedHashMap;
    invoke-static {v3}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->access$200(Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;)Ljava/util/LinkedHashMap;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;

    invoke-direct {p0, p2, v1, p1, v2}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFilter;->evaluatePropertyForEvent(Landroid/content/Context;Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;Landroid/view/accessibility/AccessibilityEvent;Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;)Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v3, 0x0

    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method
