.class public Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;
.super Ljava/lang/Object;
.source "EventSpeechRuleProcessor.java"


# instance fields
.field private final mContext:Lcom/google/android/marvin/talkback/TalkBackService;

.field private mDocumentBuilder:Ljavax/xml/parsers/DocumentBuilder;

.field private final mPackageNameToSpeechRulesMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 1
    .param p1    # Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->mPackageNameToSpeechRulesMap:Ljava/util/Map;

    iput-object p1, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    return-void
.end method

.method private addSpeechRuleLocked(Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;)Z
    .locals 3
    .param p1    # Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;

    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->mPackageNameToSpeechRulesMap:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    if-nez v1, :cond_0

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iget-object v2, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->mPackageNameToSpeechRulesMap:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-result v2

    return v2
.end method

.method private getDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/xml/parsers/ParserConfigurationException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->mDocumentBuilder:Ljavax/xml/parsers/DocumentBuilder;

    if-nez v0, :cond_0

    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v0

    invoke-virtual {v0}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->mDocumentBuilder:Ljavax/xml/parsers/DocumentBuilder;

    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->mDocumentBuilder:Ljavax/xml/parsers/DocumentBuilder;

    return-object v0
.end method

.method private parseSpeechStrategy(Ljava/io/InputStream;)Lorg/w3c/dom/Document;
    .locals 8
    .param p1    # Ljava/io/InputStream;

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->getDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/InputStream;)Lorg/w3c/dom/Document;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_0
    return-object v2

    :catch_0
    move-exception v1

    const-class v2, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;

    const/4 v3, 0x6

    const-string v4, "Could not open speechstrategy xml file\n%s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v2, v3, v4, v5}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method private processEvent(Ljava/util/List;Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/Utterance;)Z
    .locals 12
    .param p2    # Landroid/view/accessibility/AccessibilityEvent;
    .param p3    # Lcom/google/android/marvin/talkback/Utterance;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;",
            ">;",
            "Landroid/view/accessibility/AccessibilityEvent;",
            "Lcom/google/android/marvin/talkback/Utterance;",
            ")Z"
        }
    .end annotation

    const/16 v11, 0x800

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;

    :try_start_0
    invoke-virtual {v2, p2}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->applyFilter(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v2, p2, p3}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->applyFormatter(Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/Utterance;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v5

    if-eq v5, v11, :cond_1

    const-class v5, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;

    const/4 v6, 0x2

    const-string v7, "Processed event using rule:\n%s"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v2, v8, v9

    invoke-static {v5, v6, v7, v8}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    :goto_1
    return v3

    :cond_2
    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v5

    if-eq v5, v11, :cond_3

    const-class v5, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;

    const/4 v6, 0x2

    const-string v7, "The \"%s\" filter accepted the event, but the \"%s\" formatter indicated the event should be dropped."

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getFilter()Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFilter;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getFormatter()Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v5, v6, v7, v8}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    move v3, v4

    goto :goto_1

    :catch_0
    move-exception v0

    const-class v5, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;

    const/4 v6, 0x6

    const-string v7, "Error while processing rule:\n%s"

    new-array v8, v3, [Ljava/lang/Object;

    aput-object v2, v8, v4

    invoke-static {v5, v6, v7, v8}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :cond_4
    move v3, v4

    goto :goto_1
.end method


# virtual methods
.method public addSpeechStrategy(Ljava/lang/Iterable;)I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;",
            ">;)I"
        }
    .end annotation

    const/4 v0, 0x0

    iget-object v4, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->mPackageNameToSpeechRulesMap:Ljava/util/Map;

    monitor-enter v4

    :try_start_0
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;

    invoke-direct {p0, v2}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->addSpeechRuleLocked(Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;)Z

    move-result v3

    if-eqz v3, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    monitor-exit v4

    return v0

    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public addSpeechStrategy(I)V
    .locals 12
    .param p1    # I

    iget-object v6, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v6}, Lcom/google/android/marvin/talkback/TalkBackService;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->parseSpeechStrategy(Ljava/io/InputStream;)Lorg/w3c/dom/Document;

    move-result-object v1

    iget-object v6, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-static {v6, v1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->createSpeechRules(Lcom/google/android/marvin/talkback/TalkBackService;Lorg/w3c/dom/Document;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->addSpeechStrategy(Ljava/lang/Iterable;)I

    move-result v0

    const-class v6, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;

    const/4 v7, 0x4

    const-string v8, "%d speech rules appended from: %s"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    aput-object v5, v9, v10

    invoke-static {v6, v7, v8, v9}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public processEvent(Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/Utterance;)Z
    .locals 5
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2    # Lcom/google/android/marvin/talkback/Utterance;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->mPackageNameToSpeechRulesMap:Ljava/util/Map;

    monitor-enter v2

    :try_start_0
    iget-object v3, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->mPackageNameToSpeechRulesMap:Ljava/util/Map;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getPackageName()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->processEvent(Ljava/util/List;Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/Utterance;)Z

    move-result v3

    if-eqz v3, :cond_0

    monitor-exit v2

    :goto_0
    return v1

    :cond_0
    iget-object v3, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->mPackageNameToSpeechRulesMap:Ljava/util/Map;

    const-string v4, "undefined_package_name"

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_1

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRuleProcessor;->processEvent(Ljava/util/List;Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/Utterance;)Z

    move-result v3

    if-eqz v3, :cond_1

    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v1, 0x0

    goto :goto_0
.end method
