.class public Lcom/google/android/marvin/talkback/formatter/ImageViewFormatter;
.super Ljava/lang/Object;
.source "ImageViewFormatter.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;


# instance fields
.field private ruleNonTextViews:Lcom/google/android/marvin/talkback/speechrules/RuleNonTextViews;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/marvin/talkback/speechrules/RuleNonTextViews;

    invoke-direct {v0}, Lcom/google/android/marvin/talkback/speechrules/RuleNonTextViews;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/formatter/ImageViewFormatter;->ruleNonTextViews:Lcom/google/android/marvin/talkback/speechrules/RuleNonTextViews;

    return-void
.end method


# virtual methods
.method public format(Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/TalkBackService;Lcom/google/android/marvin/talkback/Utterance;)Z
    .locals 7
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2    # Lcom/google/android/marvin/talkback/TalkBackService;
    .param p3    # Lcom/google/android/marvin/talkback/Utterance;

    const/4 v6, 0x1

    new-instance v0, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    invoke-direct {v0, p1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getSource()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/marvin/talkback/formatter/ImageViewFormatter;->ruleNonTextViews:Lcom/google/android/marvin/talkback/speechrules/RuleNonTextViews;

    invoke-virtual {v3, p2, v1, p1}, Lcom/google/android/marvin/talkback/speechrules/RuleNonTextViews;->format(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p3}, Lcom/google/android/marvin/talkback/Utterance;->getText()Ljava/lang/StringBuilder;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->appendWithSeparator(Ljava/lang/StringBuilder;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    return v6
.end method
