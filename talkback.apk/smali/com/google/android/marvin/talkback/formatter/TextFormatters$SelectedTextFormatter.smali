.class public final Lcom/google/android/marvin/talkback/formatter/TextFormatters$SelectedTextFormatter;
.super Ljava/lang/Object;
.source "TextFormatters.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/formatter/TextFormatters;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SelectedTextFormatter"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private formatPassword(Landroid/view/accessibility/AccessibilityEvent;Landroid/content/Context;Ljava/lang/StringBuilder;)Z
    .locals 9
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2    # Landroid/content/Context;
    .param p3    # Ljava/lang/StringBuilder;

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v2, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    invoke-direct {v2, p1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;-><init>(Ljava/lang/Object;)V

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getFromIndex()I

    move-result v1

    invoke-virtual {v2}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getToIndex()I

    move-result v3

    if-gt v3, v1, :cond_0

    :goto_0
    return v4

    :cond_0
    const v6, 0x7f0a005e

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v5

    invoke-virtual {p2, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-array v6, v5, [Ljava/lang/Object;

    aput-object v0, v6, v4

    invoke-static {p3, v6}, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->appendWithSeparator(Ljava/lang/StringBuilder;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move v4, v5

    goto :goto_0
.end method

.method private shouldIgnoreEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 10
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;

    const/4 v5, 0x1

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventTime()J

    move-result-wide v2

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getPackageName()Ljava/lang/CharSequence;

    move-result-object v1

    # getter for: Lcom/google/android/marvin/talkback/formatter/TextFormatters;->sChangedTimestamp:J
    invoke-static {}, Lcom/google/android/marvin/talkback/formatter/TextFormatters;->access$100()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-lez v6, :cond_1

    # getter for: Lcom/google/android/marvin/talkback/formatter/TextFormatters;->sChangedTimestamp:J
    invoke-static {}, Lcom/google/android/marvin/talkback/formatter/TextFormatters;->access$100()J

    move-result-wide v6

    sub-long v6, v2, v6

    const-wide/16 v8, 0x64

    cmp-long v6, v6, v8

    if-gez v6, :cond_1

    # getter for: Lcom/google/android/marvin/talkback/formatter/TextFormatters;->sChangedPackage:Ljava/lang/CharSequence;
    invoke-static {}, Lcom/google/android/marvin/talkback/formatter/TextFormatters;->access$200()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-static {v1, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    const-wide/16 v6, -0x1

    # setter for: Lcom/google/android/marvin/talkback/formatter/TextFormatters;->sChangedTimestamp:J
    invoke-static {v6, v7}, Lcom/google/android/marvin/talkback/formatter/TextFormatters;->access$102(J)J

    const/4 v6, 0x0

    # setter for: Lcom/google/android/marvin/talkback/formatter/TextFormatters;->sChangedPackage:Ljava/lang/CharSequence;
    invoke-static {v6}, Lcom/google/android/marvin/talkback/formatter/TextFormatters;->access$202(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    :cond_0
    :goto_0
    return v5

    :cond_1
    new-instance v0, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    invoke-direct {v0, p1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getSource()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v4}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->isAccessibilityFocused()Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {v4}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->isFocused()Z

    move-result v6

    if-eqz v6, :cond_0

    :cond_2
    const/4 v5, 0x0

    goto :goto_0
.end method


# virtual methods
.method public format(Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/TalkBackService;Lcom/google/android/marvin/talkback/Utterance;)Z
    .locals 12
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2    # Lcom/google/android/marvin/talkback/TalkBackService;
    .param p3    # Lcom/google/android/marvin/talkback/Utterance;

    const/4 v10, 0x0

    # setter for: Lcom/google/android/marvin/talkback/formatter/TextFormatters;->sAwaitingSelection:Z
    invoke-static {v10}, Lcom/google/android/marvin/talkback/formatter/TextFormatters;->access$002(Z)Z

    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/formatter/TextFormatters$SelectedTextFormatter;->shouldIgnoreEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v10

    if-eqz v10, :cond_0

    const/4 v10, 0x0

    :goto_0
    return v10

    :cond_0
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v10

    const/high16 v11, 0x20000

    if-ne v10, v11, :cond_1

    const/4 v5, 0x1

    :goto_1
    if-eqz v5, :cond_2

    invoke-static {p1}, Lcom/googlecode/eyesfree/utils/AccessibilityEventUtils;->getEventTextOrDescription(Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;

    move-result-object v8

    :goto_2
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getItemCount()I

    move-result v2

    invoke-virtual {p3}, Lcom/google/android/marvin/talkback/Utterance;->getText()Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {p2}, Lcom/googlecode/eyesfree/compat/provider/SettingsCompatUtils$SecureCompatUtils;->shouldSpeakPasswords(Landroid/content/Context;)Z

    move-result v7

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->isPassword()Z

    move-result v10

    if-eqz v10, :cond_3

    if-nez v7, :cond_3

    invoke-direct {p0, p1, p2, v9}, Lcom/google/android/marvin/talkback/formatter/TextFormatters$SelectedTextFormatter;->formatPassword(Landroid/view/accessibility/AccessibilityEvent;Landroid/content/Context;Ljava/lang/StringBuilder;)Z

    move-result v10

    goto :goto_0

    :cond_1
    const/4 v5, 0x0

    goto :goto_1

    :cond_2
    # invokes: Lcom/google/android/marvin/talkback/formatter/TextFormatters;->getEventText(Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;
    invoke-static {p1}, Lcom/google/android/marvin/talkback/formatter/TextFormatters;->access$300(Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;

    move-result-object v8

    goto :goto_2

    :cond_3
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_4

    if-nez v2, :cond_5

    :cond_4
    const/4 v10, 0x0

    goto :goto_0

    :cond_5
    if-eqz v5, :cond_6

    invoke-static {p1}, Lcom/googlecode/eyesfree/compat/view/accessibility/AccessibilityEventCompatUtils;->getMovementGranularity(Landroid/view/accessibility/AccessibilityEvent;)I

    move-result v10

    const/4 v11, 0x1

    if-ne v10, v11, :cond_6

    const/4 v4, 0x1

    :goto_3
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getToIndex()I

    move-result v3

    if-eqz v4, :cond_7

    move v0, v3

    :goto_4
    # invokes: Lcom/google/android/marvin/talkback/formatter/TextFormatters;->areValidIndices(Ljava/lang/CharSequence;II)Z
    invoke-static {v8, v0, v3}, Lcom/google/android/marvin/talkback/formatter/TextFormatters;->access$400(Ljava/lang/CharSequence;II)Z

    move-result v10

    if-nez v10, :cond_8

    const/4 v10, 0x0

    goto :goto_0

    :cond_6
    const/4 v4, 0x0

    goto :goto_3

    :cond_7
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getFromIndex()I

    move-result v0

    goto :goto_4

    :cond_8
    if-eq v0, v3, :cond_9

    invoke-interface {v8, v0, v3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v6

    :goto_5
    invoke-static {p2, v6}, Lcom/google/android/marvin/talkback/SpeechCleanupUtils;->cleanUp(Landroid/content/Context;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v1, v10, v11

    invoke-static {v9, v10}, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->appendWithSeparator(Ljava/lang/StringBuilder;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v10, 0x1

    goto :goto_0

    :cond_9
    invoke-interface {v8}, Ljava/lang/CharSequence;->length()I

    move-result v10

    if-ge v3, v10, :cond_a

    add-int/lit8 v10, v3, 0x1

    invoke-interface {v8, v0, v10}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v6

    goto :goto_5

    :cond_a
    const v10, 0x7f0a00e9

    invoke-virtual {p2, v10}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto :goto_5
.end method
