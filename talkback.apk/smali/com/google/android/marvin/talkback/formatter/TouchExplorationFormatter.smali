.class public final Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;
.super Ljava/lang/Object;
.source "TouchExplorationFormatter.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/TalkBackService$EventListener;
.implements Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;
.implements Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$ContextBasedRule;


# static fields
.field private static final SUPPORTS_A11Y_FOCUS:Z

.field private static final SUPPORTS_NODE_CACHE:Z


# instance fields
.field private mContext:Lcom/google/android/marvin/talkback/TalkBackService;

.field private mLastFocusedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

.field private mLastNodeWasScrollable:Z

.field private mNodeProcessor:Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v3, 0x10

    const/4 v1, 0x1

    const/4 v2, 0x0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v3, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;->SUPPORTS_A11Y_FOCUS:Z

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v3, :cond_1

    :goto_1
    sput-boolean v1, Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;->SUPPORTS_NODE_CACHE:Z

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private addDescription(Lcom/google/android/marvin/talkback/Utterance;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/view/accessibility/AccessibilityEvent;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 5
    .param p1    # Lcom/google/android/marvin/talkback/Utterance;
    .param p2    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p3    # Landroid/view/accessibility/AccessibilityEvent;
    .param p4    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;->mNodeProcessor:Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;

    invoke-virtual {v2, p2, p3, p4}, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->getDescriptionForTree(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/view/accessibility/AccessibilityEvent;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/Utterance;->getText()Ljava/lang/StringBuilder;

    move-result-object v2

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->appendWithSeparator(Ljava/lang/StringBuilder;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :goto_0
    return v1

    :cond_0
    invoke-direct {p0, p1, p3}, Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;->addDescriptionForEvent(Lcom/google/android/marvin/talkback/Utterance;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v1

    goto :goto_0
.end method

.method private addDescriptionForEvent(Lcom/google/android/marvin/talkback/Utterance;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 5
    .param p1    # Lcom/google/android/marvin/talkback/Utterance;
    .param p2    # Landroid/view/accessibility/AccessibilityEvent;

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-static {p2}, Lcom/googlecode/eyesfree/utils/AccessibilityEventUtils;->getEventTextOrDescription(Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    :goto_0
    return v2

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/Utterance;->getText()Ljava/lang/StringBuilder;

    move-result-object v0

    new-array v4, v3, [Ljava/lang/Object;

    aput-object v1, v4, v2

    invoke-static {v0, v4}, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->appendWithSeparator(Ljava/lang/StringBuilder;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move v2, v3

    goto :goto_0
.end method

.method private addEarcons(Lcom/google/android/marvin/talkback/Utterance;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    .locals 6
    .param p1    # Lcom/google/android/marvin/talkback/Utterance;
    .param p2    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-nez p2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v4, p0, Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    sget-object v5, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->FILTER_SCROLLABLE:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;

    invoke-static {v4, p2, v5}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->getSelfOrMatchingPredecessor(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    if-eqz v0, :cond_3

    move v1, v2

    :goto_1
    new-array v2, v2, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v2, v3

    invoke-static {v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    iget-boolean v2, p0, Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;->mLastNodeWasScrollable:Z

    if-eq v2, v1, :cond_1

    iput-boolean v1, p0, Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;->mLastNodeWasScrollable:Z

    if-eqz v1, :cond_4

    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/Utterance;->getCustomEarcons()Ljava/util/List;

    move-result-object v2

    const v3, 0x7f08002c

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_2
    sget-boolean v2, Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;->SUPPORTS_NODE_CACHE:Z

    if-eqz v2, :cond_2

    if-eqz v1, :cond_2

    iget-object v2, p0, Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-static {v2, p2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isEdgeListItem(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/Utterance;->getCustomEarcons()Ljava/util/List;

    move-result-object v2

    const v3, 0x7f08002b

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    invoke-static {p2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isActionableForAccessibility(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/Utterance;->getCustomEarcons()Ljava/util/List;

    move-result-object v2

    const v3, 0x7f080023

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/Utterance;->getCustomVibrations()Ljava/util/List;

    move-result-object v2

    const v3, 0x7f080034

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    move v1, v3

    goto :goto_1

    :cond_4
    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/Utterance;->getCustomEarcons()Ljava/util/List;

    move-result-object v2

    const v3, 0x7f08002d

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_5
    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/Utterance;->getCustomEarcons()Ljava/util/List;

    move-result-object v2

    const v3, 0x7f080022

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/Utterance;->getCustomVibrations()Ljava/util/List;

    move-result-object v2

    const v3, 0x7f080033

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method private getFocusedNode(ILandroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 4
    .param p1    # I
    .param p2    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/4 v1, 0x0

    if-nez p2, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    sget-boolean v2, Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;->SUPPORTS_A11Y_FOCUS:Z

    if-eqz v2, :cond_2

    const v2, 0x8000

    if-eq p1, v2, :cond_1

    move-object v0, v1

    goto :goto_0

    :cond_1
    invoke-static {p2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/16 v2, 0x80

    if-eq p1, v2, :cond_3

    move-object v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-static {v2, p2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->findFocusFromHover(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v2, p0, Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;->mLastFocusedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v0, v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move-object v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;->mLastFocusedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;->mLastFocusedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    :cond_5
    if-eqz v0, :cond_6

    invoke-static {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;->mLastFocusedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    :goto_1
    invoke-static {v0}, Lcom/google/android/marvin/talkback/formatter/ProgressBarFormatter;->updateRecentlyExplored(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_0

    :cond_6
    iput-object v1, p0, Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;->mLastFocusedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    goto :goto_1
.end method


# virtual methods
.method public format(Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/TalkBackService;Lcom/google/android/marvin/talkback/Utterance;)Z
    .locals 9
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2    # Lcom/google/android/marvin/talkback/TalkBackService;
    .param p3    # Lcom/google/android/marvin/talkback/Utterance;

    const/4 v8, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v1, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    invoke-direct {v1, p1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getSource()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v2

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v5

    invoke-direct {p0, v5, v2}, Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;->getFocusedNode(ILandroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    if-eqz v2, :cond_0

    if-nez v0, :cond_0

    new-array v4, v4, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v2, v4, v3

    invoke-static {v4}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    :goto_0
    return v3

    :cond_0
    const-string v5, "Announcing node: %s"

    new-array v6, v4, [Ljava/lang/Object;

    aput-object v0, v6, v3

    invoke-static {p0, v8, v5, v6}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    invoke-direct {p0, p3, v0, p1, v2}, Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;->addDescription(Lcom/google/android/marvin/talkback/Utterance;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/view/accessibility/AccessibilityEvent;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    invoke-direct {p0, p3, v0}, Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;->addEarcons(Lcom/google/android/marvin/talkback/Utterance;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    invoke-virtual {p3}, Lcom/google/android/marvin/talkback/Utterance;->getMetadata()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "queuing"

    const/4 v7, 0x3

    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    new-array v5, v8, [Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v2, v5, v3

    aput-object v0, v5, v4

    invoke-static {v5}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v3, v4

    goto :goto_0
.end method

.method public initialize(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 1
    .param p1    # Lcom/google/android/marvin/talkback/TalkBackService;

    iput-object p1, p0, Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v0, p0}, Lcom/google/android/marvin/talkback/TalkBackService;->addEventListener(Lcom/google/android/marvin/talkback/TalkBackService$EventListener;)V

    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getNodeProcessor()Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;->mNodeProcessor:Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;

    return-void
.end method

.method public process(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/marvin/talkback/formatter/TouchExplorationFormatter;->mLastNodeWasScrollable:Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x20
        :pswitch_0
    .end packed-switch
.end method
