.class Lcom/google/android/marvin/talkback/formatter/WebContentFormatter$Action;
.super Ljava/lang/Object;
.source "WebContentFormatter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/formatter/WebContentFormatter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Action"
.end annotation


# instance fields
.field private mActionCode:I

.field private mFirstArgument:I

.field private mSecondArgument:I

.field private mThirdArgument:I

.field final synthetic this$0:Lcom/google/android/marvin/talkback/formatter/WebContentFormatter;


# direct methods
.method private constructor <init>(Lcom/google/android/marvin/talkback/formatter/WebContentFormatter;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/marvin/talkback/formatter/WebContentFormatter$Action;->this$0:Lcom/google/android/marvin/talkback/formatter/WebContentFormatter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/marvin/talkback/formatter/WebContentFormatter;Lcom/google/android/marvin/talkback/formatter/WebContentFormatter$1;)V
    .locals 0
    .param p1    # Lcom/google/android/marvin/talkback/formatter/WebContentFormatter;
    .param p2    # Lcom/google/android/marvin/talkback/formatter/WebContentFormatter$1;

    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/formatter/WebContentFormatter$Action;-><init>(Lcom/google/android/marvin/talkback/formatter/WebContentFormatter;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/marvin/talkback/formatter/WebContentFormatter$Action;)I
    .locals 1
    .param p0    # Lcom/google/android/marvin/talkback/formatter/WebContentFormatter$Action;

    iget v0, p0, Lcom/google/android/marvin/talkback/formatter/WebContentFormatter$Action;->mActionCode:I

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/marvin/talkback/formatter/WebContentFormatter$Action;)I
    .locals 1
    .param p0    # Lcom/google/android/marvin/talkback/formatter/WebContentFormatter$Action;

    iget v0, p0, Lcom/google/android/marvin/talkback/formatter/WebContentFormatter$Action;->mSecondArgument:I

    return v0
.end method


# virtual methods
.method public init(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "0x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    const/high16 v2, -0x1000000

    and-int/2addr v2, v0

    shr-int/lit8 v2, v2, 0x18

    iput v2, p0, Lcom/google/android/marvin/talkback/formatter/WebContentFormatter$Action;->mActionCode:I

    const/high16 v2, 0xff0000

    and-int/2addr v2, v0

    shr-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/android/marvin/talkback/formatter/WebContentFormatter$Action;->mFirstArgument:I

    const v2, 0xff00

    and-int/2addr v2, v0

    shr-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/android/marvin/talkback/formatter/WebContentFormatter$Action;->mSecondArgument:I

    and-int/lit8 v2, v0, 0x0

    const/16 v3, 0xff

    shr-int/2addr v2, v3

    iput v2, p0, Lcom/google/android/marvin/talkback/formatter/WebContentFormatter$Action;->mThirdArgument:I

    :goto_0
    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method
