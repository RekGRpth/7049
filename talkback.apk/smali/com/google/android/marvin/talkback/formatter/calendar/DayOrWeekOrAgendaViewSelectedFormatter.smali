.class public final Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;
.super Ljava/lang/Object;
.source "DayOrWeekOrAgendaViewSelectedFormatter.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;


# static fields
.field private static CLASS_NAME_AGENDA_VIEW:Ljava/lang/String;

.field public static final CONTENT_URI_CALENDARS:Landroid/net/Uri;

.field private static final PROJECTION:[Ljava/lang/String;

.field private static final SDK_INT:I


# instance fields
.field private final mColorToDisplayNameMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mLastDateFragment:Ljava/lang/String;

.field private mLastTimeFragment:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    sput v0, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->SDK_INT:I

    const-string v0, ""

    sput-object v0, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->CLASS_NAME_AGENDA_VIEW:Ljava/lang/String;

    sget v0, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->SDK_INT:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    const-string v0, "content://com.android.calendar/calendars"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->CONTENT_URI_CALENDARS:Landroid/net/Uri;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "color"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "displayName"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->PROJECTION:[Ljava/lang/String;

    return-void

    :pswitch_0
    const-string v0, "com.android.calendar.AgendaListView"

    sput-object v0, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->CLASS_NAME_AGENDA_VIEW:Ljava/lang/String;

    goto :goto_0

    :pswitch_1
    const-string v0, "com.android.calendar.agenda.AgendaListView"

    sput-object v0, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->CLASS_NAME_AGENDA_VIEW:Ljava/lang/String;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->mColorToDisplayNameMap:Landroid/util/SparseArray;

    return-void
.end method

.method private appendDisplayName(Landroid/content/Context;Landroid/view/accessibility/AccessibilityEvent;Ljava/lang/StringBuilder;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/accessibility/AccessibilityEvent;
    .param p3    # Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getParcelableData()Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v3, "color"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iget-object v3, p0, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->mColorToDisplayNameMap:Landroid/util/SparseArray;

    invoke-virtual {v3, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-nez v0, :cond_2

    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->reloadColorToDisplayNameMap(Landroid/content/Context;)V

    :cond_2
    iget-object v3, p0, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->mColorToDisplayNameMap:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_0

    iget-object v3, p0, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->mColorToDisplayNameMap:Landroid/util/SparseArray;

    invoke-virtual {v3, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_0

    const v3, 0x7f0a0106

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p3, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v3, 0x2c

    invoke-virtual {p3, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v3, 0x20

    invoke-virtual {p3, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v3, 0x2e

    invoke-virtual {p3, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private appendEventCountAnnouncement(Landroid/content/Context;Landroid/view/accessibility/AccessibilityEvent;Ljava/lang/StringBuilder;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/accessibility/AccessibilityEvent;
    .param p3    # Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getCurrentItemIndex()I

    move-result v2

    add-int/lit8 v1, v2, 0x1

    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getItemCount()I

    move-result v0

    const v2, 0x7f0a0105

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x20

    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x7f0d0000

    invoke-virtual {v2, v3, v0}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method

.method private appendEventText(Landroid/view/accessibility/AccessibilityEvent;Ljava/lang/StringBuilder;)V
    .locals 4
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2    # Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const/16 v3, 0x20

    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_0
    return-void
.end method

.method private appendSelectedEventDetails(Landroid/content/Context;Landroid/view/accessibility/AccessibilityEvent;Ljava/lang/StringBuilder;)V
    .locals 12
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/accessibility/AccessibilityEvent;
    .param p3    # Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getParcelableData()Landroid/os/Parcelable;

    move-result-object v7

    check-cast v7, Landroid/os/Bundle;

    if-nez v7, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->appendDisplayName(Landroid/content/Context;Landroid/view/accessibility/AccessibilityEvent;Ljava/lang/StringBuilder;)V

    const-string v0, "title"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v9

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const/16 v0, 0x20

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const/16 v0, 0x2e

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_2
    const-string v0, "startMillis"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    const-wide/16 v10, 0x0

    cmp-long v0, v1, v10

    if-lez v0, :cond_4

    const-string v0, "endMillis"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    const v5, 0x81413

    invoke-static {p1}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    or-int/lit16 v5, v5, 0x80

    :cond_3
    move-object v0, p1

    invoke-static/range {v0 .. v5}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v8

    const/16 v0, 0x20

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v0, 0x2e

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_4
    const-string v0, "location"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x20

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const/16 v0, 0x2e

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private appendSelectedEventIndexAnouncement(Landroid/content/Context;Landroid/view/accessibility/AccessibilityEvent;Ljava/lang/StringBuilder;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/accessibility/AccessibilityEvent;
    .param p3    # Ljava/lang/StringBuilder;

    const/16 v2, 0x20

    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getItemCount()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->appendEventCountAnnouncement(Landroid/content/Context;Landroid/view/accessibility/AccessibilityEvent;Ljava/lang/StringBuilder;)V

    const/16 v1, 0x2e

    invoke-virtual {p3, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_0
    return-void
.end method

.method private appendSelectedRange(Landroid/content/Context;Landroid/view/accessibility/AccessibilityEvent;Ljava/lang/StringBuilder;)V
    .locals 11
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/accessibility/AccessibilityEvent;
    .param p3    # Ljava/lang/StringBuilder;

    const/16 v10, 0x20

    const/4 v9, 0x0

    const/16 v8, 0x2e

    const/16 v7, 0x2c

    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/CharSequence;

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getClassName()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v6, "com.android.calendar.DayView"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    move-object v4, v2

    sget v6, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->SDK_INT:I

    packed-switch v6, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v6, p0, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->mLastTimeFragment:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    iput-object v4, p0, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->mLastTimeFragment:Ljava/lang/String;

    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    :pswitch_1
    const/4 v1, 0x0

    invoke-virtual {v2, v7}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    const/4 v6, -0x1

    if-le v3, v6, :cond_2

    invoke-virtual {v2, v9, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v3, 0x1

    invoke-virtual {v2, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    :cond_2
    iget-object v6, p0, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->mLastTimeFragment:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    iput-object v4, p0, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->mLastTimeFragment:Ljava/lang/String;

    invoke-virtual {p3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-nez v1, :cond_4

    invoke-virtual {p3, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_3
    :goto_1
    if-eqz v1, :cond_0

    invoke-virtual {p3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_4
    invoke-virtual {p3, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_5
    invoke-virtual {v2, v7}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    invoke-virtual {v2, v9, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v3, 0x1

    invoke-virtual {v2, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    iget-object v6, p0, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->mLastDateFragment:Ljava/lang/String;

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_7

    iput-object v1, p0, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->mLastDateFragment:Ljava/lang/String;

    invoke-virtual {p3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getAddedCount()I

    move-result v5

    if-lez v5, :cond_6

    invoke-virtual {p3, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p3, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->appendEventCountAnnouncement(Landroid/content/Context;Landroid/view/accessibility/AccessibilityEvent;Ljava/lang/StringBuilder;)V

    :cond_6
    invoke-virtual {p3, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p3, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_7
    iget-object v6, p0, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->mLastTimeFragment:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    iput-object v4, p0, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->mLastTimeFragment:Ljava/lang/String;

    invoke-virtual {p3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getItemCount()I

    move-result v6

    if-lez v6, :cond_0

    invoke-virtual {p3, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p3, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private formatAgendaViewSelected(Landroid/view/accessibility/AccessibilityEvent;Landroid/content/Context;Lcom/google/android/marvin/talkback/Utterance;)V
    .locals 1
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2    # Landroid/content/Context;
    .param p3    # Lcom/google/android/marvin/talkback/Utterance;

    invoke-virtual {p3}, Lcom/google/android/marvin/talkback/Utterance;->getText()Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0, p2, p1, v0}, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->appendDisplayName(Landroid/content/Context;Landroid/view/accessibility/AccessibilityEvent;Ljava/lang/StringBuilder;)V

    invoke-direct {p0, p1, v0}, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->appendEventText(Landroid/view/accessibility/AccessibilityEvent;Ljava/lang/StringBuilder;)V

    return-void
.end method

.method private formatDayOrWeekViewSelected(Landroid/view/accessibility/AccessibilityEvent;Landroid/content/Context;Lcom/google/android/marvin/talkback/Utterance;)V
    .locals 1
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2    # Landroid/content/Context;
    .param p3    # Lcom/google/android/marvin/talkback/Utterance;

    invoke-virtual {p3}, Lcom/google/android/marvin/talkback/Utterance;->getText()Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0, p2, p1, v0}, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->appendSelectedRange(Landroid/content/Context;Landroid/view/accessibility/AccessibilityEvent;Ljava/lang/StringBuilder;)V

    invoke-direct {p0, p2, p1, v0}, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->appendSelectedEventIndexAnouncement(Landroid/content/Context;Landroid/view/accessibility/AccessibilityEvent;Ljava/lang/StringBuilder;)V

    invoke-direct {p0, p2, p1, v0}, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->appendSelectedEventDetails(Landroid/content/Context;Landroid/view/accessibility/AccessibilityEvent;Ljava/lang/StringBuilder;)V

    return-void
.end method

.method private reloadColorToDisplayNameMap(Landroid/content/Context;)V
    .locals 11
    .param p1    # Landroid/content/Context;

    const/4 v10, 0x1

    const/4 v9, 0x0

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->CONTENT_URI_CALENDARS:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->PROJECTION:[Ljava/lang/String;

    const-string v3, "selected=?"

    new-array v4, v10, [Ljava/lang/String;

    const-string v5, "1"

    aput-object v5, v4, v9

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    if-nez v7, :cond_0

    :goto_0
    return-void

    :cond_0
    :goto_1
    invoke-interface {v7}, Landroid/database/Cursor;->isLast()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    invoke-interface {v7, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-interface {v7, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    iget-object v0, p0, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->mColorToDisplayNameMap:Landroid/util/SparseArray;

    invoke-virtual {v0, v6, v8}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_1

    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method


# virtual methods
.method public format(Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/TalkBackService;Lcom/google/android/marvin/talkback/Utterance;)Z
    .locals 2
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2    # Lcom/google/android/marvin/talkback/TalkBackService;
    .param p3    # Lcom/google/android/marvin/talkback/Utterance;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getClassName()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->CLASS_NAME_AGENDA_VIEW:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->formatAgendaViewSelected(Landroid/view/accessibility/AccessibilityEvent;Landroid/content/Context;Lcom/google/android/marvin/talkback/Utterance;)V

    :goto_0
    const/4 v1, 0x1

    return v1

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/marvin/talkback/formatter/calendar/DayOrWeekOrAgendaViewSelectedFormatter;->formatDayOrWeekViewSelected(Landroid/view/accessibility/AccessibilityEvent;Landroid/content/Context;Lcom/google/android/marvin/talkback/Utterance;)V

    goto :goto_0
.end method
