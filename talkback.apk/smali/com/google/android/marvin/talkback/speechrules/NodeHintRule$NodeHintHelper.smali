.class public Lcom/google/android/marvin/talkback/speechrules/NodeHintRule$NodeHintHelper;
.super Ljava/lang/Object;
.source "NodeHintRule.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/speechrules/NodeHintRule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NodeHintHelper"
.end annotation


# static fields
.field private static mActionResId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/marvin/talkback/speechrules/NodeHintRule$NodeHintHelper;->updateActionResId(Z)V

    return-void
.end method

.method public static getHintString(Landroid/content/Context;I)Ljava/lang/CharSequence;
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # I

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    sget v2, Lcom/google/android/marvin/talkback/speechrules/NodeHintRule$NodeHintHelper;->mActionResId:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, p1, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static updateActionResId(Z)V
    .locals 2
    .param p0    # Z

    if-nez p0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_1

    :cond_0
    const v0, 0x7f0a006f

    sput v0, Lcom/google/android/marvin/talkback/speechrules/NodeHintRule$NodeHintHelper;->mActionResId:I

    :goto_0
    return-void

    :cond_1
    const v0, 0x7f0a006e

    sput v0, Lcom/google/android/marvin/talkback/speechrules/NodeHintRule$NodeHintHelper;->mActionResId:I

    goto :goto_0
.end method
