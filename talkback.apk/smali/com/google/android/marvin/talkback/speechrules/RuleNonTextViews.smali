.class public Lcom/google/android/marvin/talkback/speechrules/RuleNonTextViews;
.super Lcom/google/android/marvin/talkback/speechrules/RuleDefault;
.source "RuleNonTextViews.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/speechrules/RuleDefault;-><init>()V

    return-void
.end method


# virtual methods
.method public accept(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    const-class v2, Landroid/widget/ImageView;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-class v2, Landroid/widget/ImageButton;

    aput-object v2, v0, v1

    invoke-static {p1, p2, v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->nodeMatchesAnyClassByType(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;[Ljava/lang/Class;)Z

    move-result v0

    return v0
.end method

.method public format(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p3    # Landroid/view/accessibility/AccessibilityEvent;

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/marvin/talkback/speechrules/RuleDefault;->format(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-static {p2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isActionableForAccessibility(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v1

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_2

    move v0, v6

    :goto_0
    const/4 v3, 0x0

    const/4 v4, 0x0

    const-class v8, Landroid/widget/ImageButton;

    invoke-static {p1, p2, v8}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->nodeMatchesClassByType(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Ljava/lang/Class;)Z

    move-result v8

    if-eqz v8, :cond_3

    const v3, 0x7f0a0052

    const v4, 0x7f0a0054

    :cond_0
    :goto_1
    if-eqz v1, :cond_4

    if-nez v0, :cond_4

    invoke-virtual {p2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->hashCode()I

    move-result v8

    rem-int/lit8 v2, v8, 0x64

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {p1, v4, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    :cond_1
    :goto_2
    return-object v5

    :cond_2
    move v0, v7

    goto :goto_0

    :cond_3
    const-class v8, Landroid/widget/ImageView;

    invoke-static {p1, p2, v8}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->nodeMatchesClassByType(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Ljava/lang/Class;)Z

    move-result v8

    if-eqz v8, :cond_0

    const v3, 0x7f0a004f

    const v4, 0x7f0a0053

    goto :goto_1

    :cond_4
    if-nez v1, :cond_1

    if-eqz v0, :cond_1

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v5, v6, v7

    invoke-virtual {p1, v3, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto :goto_2
.end method

.method public bridge synthetic getHintText(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-super {p0, p1, p2}, Lcom/google/android/marvin/talkback/speechrules/RuleDefault;->getHintText(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method
