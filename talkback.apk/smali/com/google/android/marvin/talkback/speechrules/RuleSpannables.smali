.class public Lcom/google/android/marvin/talkback/speechrules/RuleSpannables;
.super Ljava/lang/Object;
.source "RuleSpannables.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/speechrules/NodeMenuRule;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/speechrules/RuleSpannables$SpannableMenuClickListener;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public accept(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/4 v3, 0x0

    invoke-virtual {p2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    instance-of v4, v1, Landroid/text/SpannableString;

    if-eqz v4, :cond_0

    invoke-virtual {p2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/SpannableString;

    invoke-virtual {v0}, Landroid/text/SpannableString;->length()I

    move-result v4

    const-class v5, Landroid/text/style/URLSpan;

    invoke-virtual {v0, v3, v4, v5}, Landroid/text/SpannableString;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Landroid/text/style/URLSpan;

    array-length v4, v2

    if-lez v4, :cond_0

    const/4 v3, 0x1

    :cond_0
    return v3
.end method

.method public getMenuItemsForNode(Lcom/google/android/marvin/talkback/TalkBackService;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/util/List;
    .locals 14
    .param p1    # Lcom/google/android/marvin/talkback/TalkBackService;
    .param p2    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/marvin/talkback/TalkBackService;",
            "Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/googlecode/eyesfree/widget/RadialMenuItem;",
            ">;"
        }
    .end annotation

    invoke-virtual/range {p2 .. p2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getText()Ljava/lang/CharSequence;

    move-result-object v9

    check-cast v9, Landroid/text/SpannableString;

    const/4 v1, 0x0

    invoke-virtual {v9}, Landroid/text/SpannableString;->length()I

    move-result v2

    const-class v4, Landroid/text/style/URLSpan;

    invoke-virtual {v9, v1, v2, v4}, Landroid/text/SpannableString;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v13

    check-cast v13, [Landroid/text/style/URLSpan;

    new-instance v8, Ljava/util/LinkedList;

    invoke-direct {v8}, Ljava/util/LinkedList;-><init>()V

    if-eqz v13, :cond_0

    array-length v1, v13

    if-nez v1, :cond_1

    :cond_0
    return-object v8

    :cond_1
    const/4 v7, 0x0

    const/4 v3, 0x0

    :goto_0
    array-length v1, v13

    if-ge v3, v1, :cond_0

    aget-object v12, v13, v3

    invoke-virtual {v9, v12}, Landroid/text/SpannableString;->getSpanStart(Ljava/lang/Object;)I

    move-result v10

    invoke-virtual {v9, v12}, Landroid/text/SpannableString;->getSpanEnd(Ljava/lang/Object;)I

    move-result v6

    invoke-virtual {v9, v10, v6}, Landroid/text/SpannableString;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v12}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v11

    new-instance v0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    const/4 v2, 0x0

    const/4 v4, 0x0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;-><init>(Landroid/content/Context;IIILjava/lang/CharSequence;)V

    new-instance v1, Lcom/google/android/marvin/talkback/speechrules/RuleSpannables$SpannableMenuClickListener;

    invoke-direct {v1, p1, v11}, Lcom/google/android/marvin/talkback/speechrules/RuleSpannables$SpannableMenuClickListener;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    invoke-virtual {v8, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public getUserFriendlyMenuName(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0a00e8

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
