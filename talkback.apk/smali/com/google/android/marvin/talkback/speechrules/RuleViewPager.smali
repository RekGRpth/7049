.class public Lcom/google/android/marvin/talkback/speechrules/RuleViewPager;
.super Ljava/lang/Object;
.source "RuleViewPager.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/speechrules/NodeMenuRule;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/speechrules/RuleViewPager$ViewPagerItemClickListener;
    }
.end annotation


# static fields
.field private static final FILTER_VIEWPAGER:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/marvin/talkback/speechrules/RuleViewPager$1;

    invoke-direct {v0}, Lcom/google/android/marvin/talkback/speechrules/RuleViewPager$1;-><init>()V

    sput-object v0, Lcom/google/android/marvin/talkback/speechrules/RuleViewPager;->FILTER_VIEWPAGER:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public accept(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    sget-object v1, Lcom/google/android/marvin/talkback/speechrules/RuleViewPager;->FILTER_VIEWPAGER:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;

    invoke-static {p1, p2, v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->getSelfOrMatchingPredecessor(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getMenuItemsForNode(Lcom/google/android/marvin/talkback/TalkBackService;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/util/List;
    .locals 13
    .param p1    # Lcom/google/android/marvin/talkback/TalkBackService;
    .param p2    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/marvin/talkback/TalkBackService;",
            "Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/googlecode/eyesfree/widget/RadialMenuItem;",
            ">;"
        }
    .end annotation

    const/4 v6, 0x1

    const/4 v2, 0x0

    sget-object v1, Lcom/google/android/marvin/talkback/speechrules/RuleViewPager;->FILTER_VIEWPAGER:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;

    invoke-static {p1, p2, v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->getSelfOrMatchingPredecessor(Landroid/content/Context;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v12

    new-instance v11, Ljava/util/LinkedList;

    invoke-direct {v11}, Ljava/util/LinkedList;-><init>()V

    new-array v1, v6, [I

    const/16 v4, 0x2000

    aput v4, v1, v2

    invoke-static {v12, v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->supportsAnyAction(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;[I)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    const v3, 0x7f08003d

    const v1, 0x7f0a00c4

    invoke-virtual {p1, v1}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object v1, p1

    move v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;-><init>(Landroid/content/Context;IIILjava/lang/CharSequence;)V

    invoke-virtual {v11, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    :cond_0
    new-array v1, v6, [I

    const/16 v4, 0x1000

    aput v4, v1, v2

    invoke-static {v12, v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->supportsAnyAction(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;[I)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v3, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    const v6, 0x7f08003e

    const v1, 0x7f0a00c3

    invoke-virtual {p1, v1}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v8

    move-object v4, p1

    move v5, v2

    move v7, v2

    invoke-direct/range {v3 .. v8}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;-><init>(Landroid/content/Context;IIILjava/lang/CharSequence;)V

    invoke-virtual {v11, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-virtual {v11}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    new-instance v1, Lcom/google/android/marvin/talkback/speechrules/RuleViewPager$ViewPagerItemClickListener;

    invoke-direct {v1, v12}, Lcom/google/android/marvin/talkback/speechrules/RuleViewPager$ViewPagerItemClickListener;-><init>(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    invoke-interface {v10, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_0

    :cond_2
    return-object v11
.end method

.method public getUserFriendlyMenuName(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0a00c2

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
