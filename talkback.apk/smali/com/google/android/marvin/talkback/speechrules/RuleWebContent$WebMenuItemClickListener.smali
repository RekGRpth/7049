.class Lcom/google/android/marvin/talkback/speechrules/RuleWebContent$WebMenuItemClickListener;
.super Ljava/lang/Object;
.source "RuleWebContent.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/speechrules/RuleWebContent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "WebMenuItemClickListener"
.end annotation


# instance fields
.field private final mNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;


# direct methods
.method public constructor <init>(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    .locals 0
    .param p1    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/marvin/talkback/speechrules/RuleWebContent$WebMenuItemClickListener;->mNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 5
    .param p1    # Landroid/view/MenuItem;

    const/4 v4, -0x1

    const/4 v1, 0x1

    if-nez p1, :cond_0

    iget-object v2, p0, Lcom/google/android/marvin/talkback/speechrules/RuleWebContent$WebMenuItemClickListener;->mNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    :goto_0
    return v1

    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v2, 0x7f080065

    if-ne v0, v2, :cond_1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/speechrules/RuleWebContent$WebMenuItemClickListener;->mNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const-string v3, "SECTION"

    invoke-static {v2, v1, v3}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->performNavigationToHtmlElementAction(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;ILjava/lang/String;)Z

    :goto_1
    iget-object v2, p0, Lcom/google/android/marvin/talkback/speechrules/RuleWebContent$WebMenuItemClickListener;->mNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    goto :goto_0

    :cond_1
    const v2, 0x7f080064

    if-ne v0, v2, :cond_2

    iget-object v2, p0, Lcom/google/android/marvin/talkback/speechrules/RuleWebContent$WebMenuItemClickListener;->mNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const-string v3, "LIST"

    invoke-static {v2, v1, v3}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->performNavigationToHtmlElementAction(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;ILjava/lang/String;)Z

    goto :goto_1

    :cond_2
    const v2, 0x7f080063

    if-ne v0, v2, :cond_3

    iget-object v2, p0, Lcom/google/android/marvin/talkback/speechrules/RuleWebContent$WebMenuItemClickListener;->mNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const-string v3, "CONTROL"

    invoke-static {v2, v1, v3}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->performNavigationToHtmlElementAction(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;ILjava/lang/String;)Z

    goto :goto_1

    :cond_3
    const v2, 0x7f080066

    if-ne v0, v2, :cond_4

    iget-object v2, p0, Lcom/google/android/marvin/talkback/speechrules/RuleWebContent$WebMenuItemClickListener;->mNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const-string v3, "SECTION"

    invoke-static {v2, v4, v3}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->performNavigationToHtmlElementAction(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;ILjava/lang/String;)Z

    goto :goto_1

    :cond_4
    const v2, 0x7f080061

    if-ne v0, v2, :cond_5

    iget-object v2, p0, Lcom/google/android/marvin/talkback/speechrules/RuleWebContent$WebMenuItemClickListener;->mNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const-string v3, "LIST"

    invoke-static {v2, v4, v3}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->performNavigationToHtmlElementAction(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;ILjava/lang/String;)Z

    goto :goto_1

    :cond_5
    const v2, 0x7f080062

    if-ne v0, v2, :cond_6

    iget-object v2, p0, Lcom/google/android/marvin/talkback/speechrules/RuleWebContent$WebMenuItemClickListener;->mNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const-string v3, "CONTROL"

    invoke-static {v2, v4, v3}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->performNavigationToHtmlElementAction(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;ILjava/lang/String;)Z

    goto :goto_1

    :cond_6
    const/4 v1, 0x0

    goto :goto_0
.end method
