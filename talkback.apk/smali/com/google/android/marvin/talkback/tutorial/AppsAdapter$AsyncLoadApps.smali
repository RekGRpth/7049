.class Lcom/google/android/marvin/talkback/tutorial/AppsAdapter$AsyncLoadApps;
.super Landroid/os/AsyncTask;
.source "AppsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AsyncLoadApps"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Landroid/content/Intent;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Landroid/content/pm/ResolveInfo;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;


# direct methods
.method private constructor <init>(Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/marvin/talkback/tutorial/AppsAdapter$AsyncLoadApps;->this$0:Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;Lcom/google/android/marvin/talkback/tutorial/AppsAdapter$1;)V
    .locals 0
    .param p1    # Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;
    .param p2    # Lcom/google/android/marvin/talkback/tutorial/AppsAdapter$1;

    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/tutorial/AppsAdapter$AsyncLoadApps;-><init>(Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Landroid/content/Intent;

    invoke-virtual {p0, p1}, Lcom/google/android/marvin/talkback/tutorial/AppsAdapter$AsyncLoadApps;->doInBackground([Landroid/content/Intent;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Landroid/content/Intent;)Ljava/util/List;
    .locals 7
    .param p1    # [Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move-object v0, p1

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    iget-object v5, p0, Lcom/google/android/marvin/talkback/tutorial/AppsAdapter$AsyncLoadApps;->this$0:Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;

    # getter for: Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;->mPackageManager:Landroid/content/pm/PackageManager;
    invoke-static {v5}, Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;->access$100(Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;)Landroid/content/pm/PackageManager;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v3, v6}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v4
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/google/android/marvin/talkback/tutorial/AppsAdapter$AsyncLoadApps;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/AppsAdapter$AsyncLoadApps;->this$0:Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;->addAll(Ljava/util/Collection;)V

    return-void
.end method
