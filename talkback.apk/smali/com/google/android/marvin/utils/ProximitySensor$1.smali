.class Lcom/google/android/marvin/utils/ProximitySensor$1;
.super Ljava/lang/Object;
.source "ProximitySensor.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/utils/ProximitySensor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/utils/ProximitySensor;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/utils/ProximitySensor;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/marvin/utils/ProximitySensor$1;->this$0:Lcom/google/android/marvin/utils/ProximitySensor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1    # Landroid/hardware/Sensor;
    .param p2    # I

    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 4
    .param p1    # Landroid/hardware/SensorEvent;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/marvin/utils/ProximitySensor$1;->this$0:Lcom/google/android/marvin/utils/ProximitySensor;

    # getter for: Lcom/google/android/marvin/utils/ProximitySensor;->mIgnoreNextEvent:Z
    invoke-static {v1}, Lcom/google/android/marvin/utils/ProximitySensor;->access$000(Lcom/google/android/marvin/utils/ProximitySensor;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/marvin/utils/ProximitySensor$1;->this$0:Lcom/google/android/marvin/utils/ProximitySensor;

    # setter for: Lcom/google/android/marvin/utils/ProximitySensor;->mIgnoreNextEvent:Z
    invoke-static {v1, v0}, Lcom/google/android/marvin/utils/ProximitySensor;->access$002(Lcom/google/android/marvin/utils/ProximitySensor;Z)Z

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/marvin/utils/ProximitySensor$1;->this$0:Lcom/google/android/marvin/utils/ProximitySensor;

    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v2, v2, v0

    iget-object v3, p0, Lcom/google/android/marvin/utils/ProximitySensor$1;->this$0:Lcom/google/android/marvin/utils/ProximitySensor;

    # getter for: Lcom/google/android/marvin/utils/ProximitySensor;->mFarValue:F
    invoke-static {v3}, Lcom/google/android/marvin/utils/ProximitySensor;->access$200(Lcom/google/android/marvin/utils/ProximitySensor;)F

    move-result v3

    cmpg-float v2, v2, v3

    if-gez v2, :cond_1

    const/4 v0, 0x1

    :cond_1
    # setter for: Lcom/google/android/marvin/utils/ProximitySensor;->mIsClose:Z
    invoke-static {v1, v0}, Lcom/google/android/marvin/utils/ProximitySensor;->access$102(Lcom/google/android/marvin/utils/ProximitySensor;Z)Z

    iget-object v0, p0, Lcom/google/android/marvin/utils/ProximitySensor$1;->this$0:Lcom/google/android/marvin/utils/ProximitySensor;

    # getter for: Lcom/google/android/marvin/utils/ProximitySensor;->mCallback:Lcom/google/android/marvin/utils/ProximitySensor$ProximityChangeListener;
    invoke-static {v0}, Lcom/google/android/marvin/utils/ProximitySensor;->access$300(Lcom/google/android/marvin/utils/ProximitySensor;)Lcom/google/android/marvin/utils/ProximitySensor$ProximityChangeListener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/marvin/utils/ProximitySensor$1;->this$0:Lcom/google/android/marvin/utils/ProximitySensor;

    # getter for: Lcom/google/android/marvin/utils/ProximitySensor;->mIsClose:Z
    invoke-static {v1}, Lcom/google/android/marvin/utils/ProximitySensor;->access$100(Lcom/google/android/marvin/utils/ProximitySensor;)Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/marvin/utils/ProximitySensor$ProximityChangeListener;->onProximityChanged(Z)V

    goto :goto_0
.end method
