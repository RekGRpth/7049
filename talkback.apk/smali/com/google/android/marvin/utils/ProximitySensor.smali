.class public Lcom/google/android/marvin/utils/ProximitySensor;
.super Ljava/lang/Object;
.source "ProximitySensor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/utils/ProximitySensor$ProximityChangeListener;
    }
.end annotation


# instance fields
.field private mCallback:Lcom/google/android/marvin/utils/ProximitySensor$ProximityChangeListener;

.field private final mFarValue:F

.field private mIgnoreNextEvent:Z

.field private mIsActive:Z

.field private mIsClose:Z

.field private final mListener:Landroid/hardware/SensorEventListener;

.field private final mProxSensor:Landroid/hardware/Sensor;

.field private final mSensorManager:Landroid/hardware/SensorManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/marvin/utils/ProximitySensor$1;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/utils/ProximitySensor$1;-><init>(Lcom/google/android/marvin/utils/ProximitySensor;)V

    iput-object v0, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mListener:Landroid/hardware/SensorEventListener;

    const-string v0, "sensor"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v0, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mSensorManager:Landroid/hardware/SensorManager;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mProxSensor:Landroid/hardware/Sensor;

    iget-object v0, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mProxSensor:Landroid/hardware/Sensor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mProxSensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getMaximumRange()F

    move-result v0

    :goto_0
    iput v0, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mFarValue:F

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/marvin/utils/ProximitySensor;)Z
    .locals 1
    .param p0    # Lcom/google/android/marvin/utils/ProximitySensor;

    iget-boolean v0, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mIgnoreNextEvent:Z

    return v0
.end method

.method static synthetic access$002(Lcom/google/android/marvin/utils/ProximitySensor;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/marvin/utils/ProximitySensor;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mIgnoreNextEvent:Z

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/marvin/utils/ProximitySensor;)Z
    .locals 1
    .param p0    # Lcom/google/android/marvin/utils/ProximitySensor;

    iget-boolean v0, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mIsClose:Z

    return v0
.end method

.method static synthetic access$102(Lcom/google/android/marvin/utils/ProximitySensor;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/marvin/utils/ProximitySensor;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mIsClose:Z

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/marvin/utils/ProximitySensor;)F
    .locals 1
    .param p0    # Lcom/google/android/marvin/utils/ProximitySensor;

    iget v0, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mFarValue:F

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/marvin/utils/ProximitySensor;)Lcom/google/android/marvin/utils/ProximitySensor$ProximityChangeListener;
    .locals 1
    .param p0    # Lcom/google/android/marvin/utils/ProximitySensor;

    iget-object v0, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mCallback:Lcom/google/android/marvin/utils/ProximitySensor$ProximityChangeListener;

    return-object v0
.end method


# virtual methods
.method public setProximityChangeListener(Lcom/google/android/marvin/utils/ProximitySensor$ProximityChangeListener;)V
    .locals 0
    .param p1    # Lcom/google/android/marvin/utils/ProximitySensor$ProximityChangeListener;

    iput-object p1, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mCallback:Lcom/google/android/marvin/utils/ProximitySensor$ProximityChangeListener;

    return-void
.end method

.method public start()V
    .locals 4

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mProxSensor:Landroid/hardware/Sensor;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mIsActive:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_2

    iput-boolean v2, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mIgnoreNextEvent:Z

    :cond_2
    iput-boolean v2, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mIsActive:Z

    iget-object v0, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mListener:Landroid/hardware/SensorEventListener;

    iget-object v2, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mProxSensor:Landroid/hardware/Sensor;

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    goto :goto_0
.end method

.method public stop()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mProxSensor:Landroid/hardware/Sensor;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mIsActive:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mIsActive:Z

    iget-object v0, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/google/android/marvin/utils/ProximitySensor;->mListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    goto :goto_0
.end method
