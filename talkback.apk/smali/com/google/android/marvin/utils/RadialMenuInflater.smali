.class public Lcom/google/android/marvin/utils/RadialMenuInflater;
.super Landroid/view/MenuInflater;
.source "RadialMenuInflater.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/utils/RadialMenuInflater$DummyRadialMenu;
    }
.end annotation


# instance fields
.field private final mHolderMenu:Lcom/google/android/marvin/utils/RadialMenuInflater$DummyRadialMenu;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/view/MenuInflater;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/google/android/marvin/utils/RadialMenuInflater$DummyRadialMenu;

    invoke-direct {v0, p1}, Lcom/google/android/marvin/utils/RadialMenuInflater$DummyRadialMenu;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/marvin/utils/RadialMenuInflater;->mHolderMenu:Lcom/google/android/marvin/utils/RadialMenuInflater$DummyRadialMenu;

    return-void
.end method


# virtual methods
.method public inflate(I)Ljava/util/List;
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/googlecode/eyesfree/widget/RadialMenuItem;",
            ">;"
        }
    .end annotation

    iget-object v2, p0, Lcom/google/android/marvin/utils/RadialMenuInflater;->mHolderMenu:Lcom/google/android/marvin/utils/RadialMenuInflater$DummyRadialMenu;

    invoke-virtual {v2}, Lcom/google/android/marvin/utils/RadialMenuInflater$DummyRadialMenu;->clear()V

    iget-object v2, p0, Lcom/google/android/marvin/utils/RadialMenuInflater;->mHolderMenu:Lcom/google/android/marvin/utils/RadialMenuInflater$DummyRadialMenu;

    invoke-virtual {p0, p1, v2}, Lcom/google/android/marvin/utils/RadialMenuInflater;->inflate(ILandroid/view/Menu;)V

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/marvin/utils/RadialMenuInflater;->mHolderMenu:Lcom/google/android/marvin/utils/RadialMenuInflater$DummyRadialMenu;

    invoke-virtual {v2}, Lcom/google/android/marvin/utils/RadialMenuInflater$DummyRadialMenu;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/marvin/utils/RadialMenuInflater;->mHolderMenu:Lcom/google/android/marvin/utils/RadialMenuInflater$DummyRadialMenu;

    invoke-virtual {v2}, Lcom/google/android/marvin/utils/RadialMenuInflater$DummyRadialMenu;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/marvin/utils/RadialMenuInflater;->mHolderMenu:Lcom/google/android/marvin/utils/RadialMenuInflater$DummyRadialMenu;

    invoke-virtual {v2, v0}, Lcom/google/android/marvin/utils/RadialMenuInflater$DummyRadialMenu;->getItem(I)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method
