.class public Lcom/googlecode/eyesfree/compat/media/AudioManagerCompatUtils;
.super Ljava/lang/Object;
.source "AudioManagerCompatUtils.java"


# static fields
.field private static final CLASS_OnAudioFocusChangeListener:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final METHOD_abandonAudioFocus:Ljava/lang/reflect/Method;

.field private static final METHOD_forceVolumeControlStream:Ljava/lang/reflect/Method;

.field private static final METHOD_requestAudioFocus:Ljava/lang/reflect/Method;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    const-string v0, "android.media.AudioManager$OnAudioFocusChangeListener"

    invoke-static {v0}, Lcom/googlecode/eyesfree/compat/CompatUtils;->getClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lcom/googlecode/eyesfree/compat/media/AudioManagerCompatUtils;->CLASS_OnAudioFocusChangeListener:Ljava/lang/Class;

    const-class v0, Landroid/media/AudioManager;

    const-string v1, "requestAudioFocus"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Class;

    sget-object v3, Lcom/googlecode/eyesfree/compat/media/AudioManagerCompatUtils;->CLASS_OnAudioFocusChangeListener:Ljava/lang/Class;

    aput-object v3, v2, v5

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v3, v2, v6

    const/4 v3, 0x2

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/googlecode/eyesfree/compat/CompatUtils;->getMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/googlecode/eyesfree/compat/media/AudioManagerCompatUtils;->METHOD_requestAudioFocus:Ljava/lang/reflect/Method;

    const-class v0, Landroid/media/AudioManager;

    const-string v1, "abandonAudioFocus"

    new-array v2, v6, [Ljava/lang/Class;

    sget-object v3, Lcom/googlecode/eyesfree/compat/media/AudioManagerCompatUtils;->CLASS_OnAudioFocusChangeListener:Ljava/lang/Class;

    aput-object v3, v2, v5

    invoke-static {v0, v1, v2}, Lcom/googlecode/eyesfree/compat/CompatUtils;->getMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/googlecode/eyesfree/compat/media/AudioManagerCompatUtils;->METHOD_abandonAudioFocus:Ljava/lang/reflect/Method;

    const-class v0, Landroid/media/AudioManager;

    const-string v1, "forceVolumeControlStream"

    new-array v2, v6, [Ljava/lang/Class;

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v3, v2, v5

    invoke-static {v0, v1, v2}, Lcom/googlecode/eyesfree/compat/CompatUtils;->getMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/googlecode/eyesfree/compat/media/AudioManagerCompatUtils;->METHOD_forceVolumeControlStream:Ljava/lang/reflect/Method;

    return-void
.end method

.method public static forceVolumeControlStream(Landroid/media/AudioManager;I)V
    .locals 5
    .param p0    # Landroid/media/AudioManager;
    .param p1    # I

    const/4 v0, 0x0

    sget-object v1, Lcom/googlecode/eyesfree/compat/media/AudioManagerCompatUtils;->METHOD_forceVolumeControlStream:Ljava/lang/reflect/Method;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {p0, v0, v1, v2}, Lcom/googlecode/eyesfree/compat/CompatUtils;->invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
