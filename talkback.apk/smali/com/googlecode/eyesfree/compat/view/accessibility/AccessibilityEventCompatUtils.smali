.class public Lcom/googlecode/eyesfree/compat/view/accessibility/AccessibilityEventCompatUtils;
.super Ljava/lang/Object;
.source "AccessibilityEventCompatUtils.java"


# static fields
.field private static final CLASS_AccessibilityEvent:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final CLASS_AccessibilityRecord:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final METHOD_getMovementGranularity:Ljava/lang/reflect/Method;

.field private static final METHOD_getToIndex:Ljava/lang/reflect/Method;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x0

    const-class v0, Landroid/view/accessibility/AccessibilityEvent;

    sput-object v0, Lcom/googlecode/eyesfree/compat/view/accessibility/AccessibilityEventCompatUtils;->CLASS_AccessibilityEvent:Ljava/lang/Class;

    const-string v0, "android.view.accessibility.AccessibilityRecord"

    invoke-static {v0}, Lcom/googlecode/eyesfree/compat/CompatUtils;->getClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lcom/googlecode/eyesfree/compat/view/accessibility/AccessibilityEventCompatUtils;->CLASS_AccessibilityRecord:Ljava/lang/Class;

    sget-object v0, Lcom/googlecode/eyesfree/compat/view/accessibility/AccessibilityEventCompatUtils;->CLASS_AccessibilityRecord:Ljava/lang/Class;

    const-string v1, "getToIndex"

    new-array v2, v3, [Ljava/lang/Class;

    invoke-static {v0, v1, v2}, Lcom/googlecode/eyesfree/compat/CompatUtils;->getMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/googlecode/eyesfree/compat/view/accessibility/AccessibilityEventCompatUtils;->METHOD_getToIndex:Ljava/lang/reflect/Method;

    sget-object v0, Lcom/googlecode/eyesfree/compat/view/accessibility/AccessibilityEventCompatUtils;->CLASS_AccessibilityEvent:Ljava/lang/Class;

    const-string v1, "getMovementGranularity"

    new-array v2, v3, [Ljava/lang/Class;

    invoke-static {v0, v1, v2}, Lcom/googlecode/eyesfree/compat/CompatUtils;->getMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/googlecode/eyesfree/compat/view/accessibility/AccessibilityEventCompatUtils;->METHOD_getMovementGranularity:Ljava/lang/reflect/Method;

    return-void
.end method

.method public static getMovementGranularity(Landroid/view/accessibility/AccessibilityEvent;)I
    .locals 3
    .param p0    # Landroid/view/accessibility/AccessibilityEvent;

    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v1, Lcom/googlecode/eyesfree/compat/view/accessibility/AccessibilityEventCompatUtils;->METHOD_getMovementGranularity:Ljava/lang/reflect/Method;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v1, v2}, Lcom/googlecode/eyesfree/compat/CompatUtils;->invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public static getToIndex(Landroid/view/accessibility/AccessibilityEvent;)I
    .locals 3
    .param p0    # Landroid/view/accessibility/AccessibilityEvent;

    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v1, Lcom/googlecode/eyesfree/compat/view/accessibility/AccessibilityEventCompatUtils;->METHOD_getToIndex:Ljava/lang/reflect/Method;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v1, v2}, Lcom/googlecode/eyesfree/compat/CompatUtils;->invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public static obtain(Landroid/view/accessibility/AccessibilityEvent;)Landroid/view/accessibility/AccessibilityEvent;
    .locals 3
    .param p0    # Landroid/view/accessibility/AccessibilityEvent;

    const/4 v2, 0x0

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    invoke-virtual {p0, v1, v2}, Landroid/view/accessibility/AccessibilityEvent;->writeToParcel(Landroid/os/Parcel;I)V

    invoke-virtual {v1, v2}, Landroid/os/Parcel;->setDataPosition(I)V

    sget-object v2, Landroid/view/accessibility/AccessibilityEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v2, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityEvent;

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    return-object v0
.end method
