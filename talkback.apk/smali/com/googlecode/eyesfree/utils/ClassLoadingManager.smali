.class public Lcom/googlecode/eyesfree/utils/ClassLoadingManager;
.super Ljava/lang/Object;
.source "ClassLoadingManager.java"


# static fields
.field private static sInstance:Lcom/googlecode/eyesfree/utils/ClassLoadingManager;


# instance fields
.field private final mClassNameToClassMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final mInstalledPackagesSet:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mNotFoundClassesMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mPackageMonitor:Lcom/googlecode/eyesfree/utils/BasePackageMonitor;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->mClassNameToClassMap:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->mNotFoundClassesMap:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->mInstalledPackagesSet:Ljava/util/HashSet;

    new-instance v0, Lcom/googlecode/eyesfree/utils/ClassLoadingManager$1;

    invoke-direct {v0, p0}, Lcom/googlecode/eyesfree/utils/ClassLoadingManager$1;-><init>(Lcom/googlecode/eyesfree/utils/ClassLoadingManager;)V

    iput-object v0, p0, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->mPackageMonitor:Lcom/googlecode/eyesfree/utils/BasePackageMonitor;

    return-void
.end method

.method static synthetic access$000(Lcom/googlecode/eyesfree/utils/ClassLoadingManager;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/googlecode/eyesfree/utils/ClassLoadingManager;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->addInstalledPackageToCache(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/googlecode/eyesfree/utils/ClassLoadingManager;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/googlecode/eyesfree/utils/ClassLoadingManager;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->removeInstalledPackageFromCache(Ljava/lang/String;)V

    return-void
.end method

.method private addInstalledPackageToCache(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->mInstalledPackagesSet:Ljava/util/HashSet;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->mInstalledPackagesSet:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->mNotFoundClassesMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private buildInstalledPackagesCache(Landroid/content/Context;)V
    .locals 5
    .param p1    # Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/PackageInfo;

    iget-object v3, v1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->addInstalledPackageToCache(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private clearInstalledPackagesCache()V
    .locals 2

    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->mInstalledPackagesSet:Ljava/util/HashSet;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->mInstalledPackagesSet:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getInstance()Lcom/googlecode/eyesfree/utils/ClassLoadingManager;
    .locals 1

    sget-object v0, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->sInstance:Lcom/googlecode/eyesfree/utils/ClassLoadingManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;

    invoke-direct {v0}, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;-><init>()V

    sput-object v0, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->sInstance:Lcom/googlecode/eyesfree/utils/ClassLoadingManager;

    :cond_0
    sget-object v0, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->sInstance:Lcom/googlecode/eyesfree/utils/ClassLoadingManager;

    return-object v0
.end method

.method private removeInstalledPackageFromCache(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->mInstalledPackagesSet:Ljava/util/HashSet;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->mInstalledPackagesSet:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public checkInstanceOf(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/CharSequence;
    .param p3    # Ljava/lang/CharSequence;
    .param p4    # Ljava/lang/CharSequence;

    const/4 v1, 0x0

    if-eqz p2, :cond_0

    if-nez p4, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-static {p2, p4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {p0, p1, p4, p3}, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->loadOrGetCachedClass(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Class;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->checkInstanceOf(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/Class;)Z

    move-result v1

    goto :goto_0
.end method

.method public checkInstanceOf(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/Class;)Z
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/CharSequence;
    .param p3    # Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/Class",
            "<*>;)Z"
        }
    .end annotation

    const/4 v1, 0x0

    if-eqz p2, :cond_0

    if-nez p4, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->loadOrGetCachedClass(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Class;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p4, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    goto :goto_0
.end method

.method public init(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->buildInstalledPackagesCache(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->mPackageMonitor:Lcom/googlecode/eyesfree/utils/BasePackageMonitor;

    invoke-virtual {v0, p1}, Lcom/googlecode/eyesfree/utils/BasePackageMonitor;->register(Landroid/content/Context;)V

    return-void
.end method

.method public loadOrGetCachedClass(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Class;
    .locals 15
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/CharSequence;
    .param p3    # Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/CharSequence;",
            ")",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_1

    const/4 v11, 0x3

    const-string v12, "Missing class name. Failed to load class."

    const/4 v13, 0x0

    new-array v13, v13, [Ljava/lang/Object;

    invoke-static {p0, v11, v12, v13}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    const/4 v2, 0x0

    :cond_0
    :goto_0
    return-object v2

    :cond_1
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_3

    const/16 v11, 0x2e

    move-object/from16 v0, p2

    invoke-static {v0, v11}, Landroid/text/TextUtils;->lastIndexOf(Ljava/lang/CharSequence;C)I

    move-result v6

    if-gez v6, :cond_2

    const/4 v11, 0x3

    const-string v12, "Missing package name. Failed to load class: %s"

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object p2, v13, v14

    invoke-static {p0, v11, v12, v13}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    const/4 v2, 0x0

    goto :goto_0

    :cond_2
    const/4 v11, 0x0

    move-object/from16 v0, p2

    invoke-static {v0, v11, v6}, Landroid/text/TextUtils;->substring(Ljava/lang/CharSequence;II)Ljava/lang/String;

    move-result-object p3

    :cond_3
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual/range {p3 .. p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v7, 0x0

    iget-object v12, p0, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->mInstalledPackagesSet:Ljava/util/HashSet;

    monitor-enter v12

    :try_start_0
    iget-object v11, p0, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->mNotFoundClassesMap:Ljava/util/HashMap;

    invoke-virtual {v11, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    move-object v0, v11

    check-cast v0, Ljava/util/HashSet;

    move-object v7, v0

    if-eqz v7, :cond_4

    invoke-virtual {v7, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    const/4 v2, 0x0

    monitor-exit v12

    goto :goto_0

    :catchall_0
    move-exception v11

    monitor-exit v12
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v11

    :cond_4
    :try_start_1
    monitor-exit v12
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v11, p0, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->mClassNameToClassMap:Ljava/util/HashMap;

    invoke-virtual {v11, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Class;

    if-nez v2, :cond_0

    :try_start_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    if-eqz v5, :cond_5

    iget-object v11, p0, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->mClassNameToClassMap:Ljava/util/HashMap;

    invoke-virtual {v11, v1, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/ClassNotFoundException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v2, v5

    goto :goto_0

    :catch_0
    move-exception v11

    :cond_5
    if-nez p1, :cond_6

    const/4 v2, 0x0

    goto :goto_0

    :cond_6
    const/4 v4, 0x3

    const/4 v11, 0x3

    :try_start_3
    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v11}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v8

    if-eqz v8, :cond_7

    iget-object v11, p0, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->mClassNameToClassMap:Ljava/util/HashMap;

    invoke-virtual {v11, v1, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    move-object v2, v8

    goto/16 :goto_0

    :catch_1
    move-exception v3

    const/4 v11, 0x6

    const-string v12, "Error encountered. Failed to load outside class: %s"

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v1, v13, v14

    invoke-static {p0, v11, v12, v13}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    :cond_7
    if-nez v7, :cond_8

    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    iget-object v11, p0, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->mNotFoundClassesMap:Ljava/util/HashMap;

    invoke-virtual {v11, v10, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_8
    invoke-virtual {v7, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const/4 v11, 0x3

    const-string v12, "Failed to load class: %s"

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v1, v13, v14

    invoke-static {v11, v12, v13}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(ILjava/lang/String;[Ljava/lang/Object;)V

    const/4 v2, 0x0

    goto/16 :goto_0
.end method

.method public shutdown()V
    .locals 1

    invoke-direct {p0}, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->clearInstalledPackagesCache()V

    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->mClassNameToClassMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->mPackageMonitor:Lcom/googlecode/eyesfree/utils/BasePackageMonitor;

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/utils/BasePackageMonitor;->unregister()V

    return-void
.end method
