.class public Lcom/googlecode/eyesfree/utils/LogUtils;
.super Ljava/lang/Object;
.source "LogUtils.java"


# static fields
.field public static LOG_LEVEL:I

.field public static TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "LogUtils"

    sput-object v0, Lcom/googlecode/eyesfree/utils/LogUtils;->TAG:Ljava/lang/String;

    const/4 v0, 0x6

    sput v0, Lcom/googlecode/eyesfree/utils/LogUtils;->LOG_LEVEL:I

    return-void
.end method

.method public static varargs log(ILjava/lang/String;[Ljava/lang/Object;)V
    .locals 1
    .param p0    # I
    .param p1    # Ljava/lang/String;
    .param p2    # [Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-static {v0, p0, p1, p2}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public static varargs log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V
    .locals 5
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/Object;

    sget v2, Lcom/googlecode/eyesfree/utils/LogUtils;->LOG_LEVEL:I

    if-ge p1, v2, :cond_0

    :goto_0
    return-void

    :cond_0
    if-nez p0, :cond_1

    sget-object v1, Lcom/googlecode/eyesfree/utils/LogUtils;->TAG:Ljava/lang/String;

    :goto_1
    :try_start_0
    invoke-static {p2, p3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v1, v2}, Landroid/util/Log;->println(ILjava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/util/IllegalFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v2, Lcom/googlecode/eyesfree/utils/LogUtils;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Bad formatting string: \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_1
    instance-of v2, p0, Ljava/lang/Class;

    if-eqz v2, :cond_2

    check-cast p0, Ljava/lang/Class;

    invoke-virtual {p0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method public static setLogLevel(I)V
    .locals 0
    .param p0    # I

    sput p0, Lcom/googlecode/eyesfree/utils/LogUtils;->LOG_LEVEL:I

    return-void
.end method
