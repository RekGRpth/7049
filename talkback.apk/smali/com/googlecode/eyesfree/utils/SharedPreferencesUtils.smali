.class public Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;
.super Ljava/lang/Object;
.source "SharedPreferencesUtils.java"


# direct methods
.method public static getBooleanPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)Z
    .locals 2
    .param p0    # Landroid/content/SharedPreferences;
    .param p1    # Landroid/content/res/Resources;
    .param p2    # I
    .param p3    # I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, p3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static getIntFromStringPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)I
    .locals 3
    .param p0    # Landroid/content/SharedPreferences;
    .param p1    # Landroid/content/res/Resources;
    .param p2    # I
    .param p3    # I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    return v1
.end method

.method public static putBooleanPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;IZ)V
    .locals 2
    .param p0    # Landroid/content/SharedPreferences;
    .param p1    # Landroid/content/res/Resources;
    .param p2    # I
    .param p3    # Z

    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method
