.class Lcom/googlecode/eyesfree/utils/SparseIterableArray$SparseIterator;
.super Ljava/lang/Object;
.source "SparseIterableArray.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/googlecode/eyesfree/utils/SparseIterableArray;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SparseIterator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private mIndex:I

.field final synthetic this$0:Lcom/googlecode/eyesfree/utils/SparseIterableArray;


# direct methods
.method private constructor <init>(Lcom/googlecode/eyesfree/utils/SparseIterableArray;)V
    .locals 1

    iput-object p1, p0, Lcom/googlecode/eyesfree/utils/SparseIterableArray$SparseIterator;->this$0:Lcom/googlecode/eyesfree/utils/SparseIterableArray;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/googlecode/eyesfree/utils/SparseIterableArray$SparseIterator;->mIndex:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/googlecode/eyesfree/utils/SparseIterableArray;Lcom/googlecode/eyesfree/utils/SparseIterableArray$1;)V
    .locals 0
    .param p1    # Lcom/googlecode/eyesfree/utils/SparseIterableArray;
    .param p2    # Lcom/googlecode/eyesfree/utils/SparseIterableArray$1;

    invoke-direct {p0, p1}, Lcom/googlecode/eyesfree/utils/SparseIterableArray$SparseIterator;-><init>(Lcom/googlecode/eyesfree/utils/SparseIterableArray;)V

    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 2

    iget v0, p0, Lcom/googlecode/eyesfree/utils/SparseIterableArray$SparseIterator;->mIndex:I

    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/SparseIterableArray$SparseIterator;->this$0:Lcom/googlecode/eyesfree/utils/SparseIterableArray;

    invoke-virtual {v1}, Lcom/googlecode/eyesfree/utils/SparseIterableArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    iget v0, p0, Lcom/googlecode/eyesfree/utils/SparseIterableArray$SparseIterator;->mIndex:I

    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/SparseIterableArray$SparseIterator;->this$0:Lcom/googlecode/eyesfree/utils/SparseIterableArray;

    invoke-virtual {v1}, Lcom/googlecode/eyesfree/utils/SparseIterableArray;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/SparseIterableArray$SparseIterator;->this$0:Lcom/googlecode/eyesfree/utils/SparseIterableArray;

    iget v1, p0, Lcom/googlecode/eyesfree/utils/SparseIterableArray$SparseIterator;->mIndex:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/googlecode/eyesfree/utils/SparseIterableArray$SparseIterator;->mIndex:I

    invoke-virtual {v0, v1}, Lcom/googlecode/eyesfree/utils/SparseIterableArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 3

    iget v0, p0, Lcom/googlecode/eyesfree/utils/SparseIterableArray$SparseIterator;->mIndex:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/googlecode/eyesfree/utils/SparseIterableArray$SparseIterator;->mIndex:I

    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/SparseIterableArray$SparseIterator;->this$0:Lcom/googlecode/eyesfree/utils/SparseIterableArray;

    invoke-virtual {v1}, Lcom/googlecode/eyesfree/utils/SparseIterableArray;->size()I

    move-result v1

    if-lt v0, v1, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/SparseIterableArray$SparseIterator;->this$0:Lcom/googlecode/eyesfree/utils/SparseIterableArray;

    iget v1, p0, Lcom/googlecode/eyesfree/utils/SparseIterableArray$SparseIterator;->mIndex:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lcom/googlecode/eyesfree/utils/SparseIterableArray$SparseIterator;->mIndex:I

    invoke-virtual {v0, v1}, Lcom/googlecode/eyesfree/utils/SparseIterableArray;->removeAt(I)V

    return-void
.end method
