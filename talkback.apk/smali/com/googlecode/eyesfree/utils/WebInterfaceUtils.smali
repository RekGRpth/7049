.class public Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;
.super Ljava/lang/Object;
.source "WebInterfaceUtils.java"


# direct methods
.method public static hasWebContent(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 1
    .param p0    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {p0, v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->supportsAnyAction(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;[I)Z

    move-result v0

    return v0

    nop

    :array_0
    .array-data 4
        0x400
        0x800
    .end array-data
.end method

.method public static isScriptInjectionEnabled(Landroid/content/Context;)Z
    .locals 5
    .param p0    # Landroid/content/Context;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "accessibility_script_injection"

    invoke-static {v3, v4, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v1, :cond_0

    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0
.end method

.method public static performNavigationAtGranularityAction(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;II)Z
    .locals 3
    .param p0    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p1    # I
    .param p2    # I

    const/4 v2, 0x1

    if-ne p1, v2, :cond_0

    const/16 v0, 0x100

    :goto_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "ACTION_ARGUMENT_MOVEMENT_GRANULARITY_INT"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->performAction(ILandroid/os/Bundle;)Z

    move-result v2

    return v2

    :cond_0
    const/16 v0, 0x200

    goto :goto_0
.end method

.method public static performNavigationToHtmlElementAction(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;ILjava/lang/String;)Z
    .locals 3
    .param p0    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p1    # I
    .param p2    # Ljava/lang/String;

    const/4 v2, 0x1

    if-ne p1, v2, :cond_0

    const/16 v0, 0x400

    :goto_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "ACTION_ARGUMENT_HTML_ELEMENT_STRING"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->performAction(ILandroid/os/Bundle;)Z

    move-result v2

    return v2

    :cond_0
    const/16 v0, 0x800

    goto :goto_0
.end method

.method public static performSpecialAction(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Z
    .locals 1
    .param p0    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p1    # I

    const/4 v0, 0x1

    invoke-static {p0, v0, p1}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->performNavigationAtGranularityAction(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;II)Z

    move-result v0

    return v0
.end method
