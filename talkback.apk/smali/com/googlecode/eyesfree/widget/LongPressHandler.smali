.class Lcom/googlecode/eyesfree/widget/LongPressHandler;
.super Landroid/os/Handler;
.source "LongPressHandler.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/googlecode/eyesfree/widget/LongPressHandler$LongPressListener;
    }
.end annotation


# static fields
.field private static final LONG_PRESS_TIMEOUT:J


# instance fields
.field private final TOUCH_SLOP_SQUARED:I

.field private mListener:Lcom/googlecode/eyesfree/widget/LongPressHandler$LongPressListener;

.field private mMoved:F

.field private mPreviousEvent:Landroid/view/MotionEvent;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v0

    int-to-long v0, v0

    sput-wide v0, Lcom/googlecode/eyesfree/widget/LongPressHandler;->LONG_PRESS_TIMEOUT:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {p0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    mul-int v1, v0, v0

    iput v1, p0, Lcom/googlecode/eyesfree/widget/LongPressHandler;->TOUCH_SLOP_SQUARED:I

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1    # Landroid/os/Message;

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/LongPressHandler;->mListener:Lcom/googlecode/eyesfree/widget/LongPressHandler$LongPressListener;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/LongPressHandler;->mListener:Lcom/googlecode/eyesfree/widget/LongPressHandler$LongPressListener;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/view/MotionEvent;

    invoke-interface {v1, v0}, Lcom/googlecode/eyesfree/widget/LongPressHandler$LongPressListener;->onLongPress(Landroid/view/MotionEvent;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/MotionEvent;

    const/4 v7, 0x0

    const/4 v6, 0x1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    const/4 v4, 0x0

    return v4

    :pswitch_1
    iput v7, p0, Lcom/googlecode/eyesfree/widget/LongPressHandler;->mMoved:F

    :pswitch_2
    iget-object v4, p0, Lcom/googlecode/eyesfree/widget/LongPressHandler;->mPreviousEvent:Landroid/view/MotionEvent;

    if-eqz v4, :cond_1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    iget-object v5, p0, Lcom/googlecode/eyesfree/widget/LongPressHandler;->mPreviousEvent:Landroid/view/MotionEvent;

    invoke-virtual {v5}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    sub-float v0, v4, v5

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    iget-object v5, p0, Lcom/googlecode/eyesfree/widget/LongPressHandler;->mPreviousEvent:Landroid/view/MotionEvent;

    invoke-virtual {v5}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    sub-float v1, v4, v5

    mul-float v4, v0, v0

    mul-float v5, v1, v1

    add-float v2, v4, v5

    iget v4, p0, Lcom/googlecode/eyesfree/widget/LongPressHandler;->mMoved:F

    add-float/2addr v4, v2

    iput v4, p0, Lcom/googlecode/eyesfree/widget/LongPressHandler;->mMoved:F

    :cond_1
    iget v4, p0, Lcom/googlecode/eyesfree/widget/LongPressHandler;->mMoved:F

    iget v5, p0, Lcom/googlecode/eyesfree/widget/LongPressHandler;->TOUCH_SLOP_SQUARED:I

    int-to-float v5, v5

    cmpl-float v4, v4, v5

    if-lez v4, :cond_2

    iput v7, p0, Lcom/googlecode/eyesfree/widget/LongPressHandler;->mMoved:F

    invoke-virtual {p0, v6}, Lcom/googlecode/eyesfree/widget/LongPressHandler;->removeMessages(I)V

    iget-object v4, p0, Lcom/googlecode/eyesfree/widget/LongPressHandler;->mPreviousEvent:Landroid/view/MotionEvent;

    invoke-virtual {p0, v6, v4}, Lcom/googlecode/eyesfree/widget/LongPressHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    sget-wide v4, Lcom/googlecode/eyesfree/widget/LongPressHandler;->LONG_PRESS_TIMEOUT:J

    invoke-virtual {p0, v3, v4, v5}, Lcom/googlecode/eyesfree/widget/LongPressHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_2
    invoke-static {p2}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v4

    iput-object v4, p0, Lcom/googlecode/eyesfree/widget/LongPressHandler;->mPreviousEvent:Landroid/view/MotionEvent;

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0, v6}, Lcom/googlecode/eyesfree/widget/LongPressHandler;->removeMessages(I)V

    iget-object v4, p0, Lcom/googlecode/eyesfree/widget/LongPressHandler;->mPreviousEvent:Landroid/view/MotionEvent;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/googlecode/eyesfree/widget/LongPressHandler;->mPreviousEvent:Landroid/view/MotionEvent;

    invoke-virtual {v4}, Landroid/view/MotionEvent;->recycle()V

    const/4 v4, 0x0

    iput-object v4, p0, Lcom/googlecode/eyesfree/widget/LongPressHandler;->mPreviousEvent:Landroid/view/MotionEvent;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public setListener(Lcom/googlecode/eyesfree/widget/LongPressHandler$LongPressListener;)V
    .locals 0
    .param p1    # Lcom/googlecode/eyesfree/widget/LongPressHandler$LongPressListener;

    iput-object p1, p0, Lcom/googlecode/eyesfree/widget/LongPressHandler;->mListener:Lcom/googlecode/eyesfree/widget/LongPressHandler$LongPressListener;

    return-void
.end method
