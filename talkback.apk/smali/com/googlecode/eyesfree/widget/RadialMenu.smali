.class public Lcom/googlecode/eyesfree/widget/RadialMenu;
.super Ljava/lang/Object;
.source "RadialMenu.java"

# interfaces
.implements Landroid/view/Menu;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/googlecode/eyesfree/widget/RadialMenu$OnMenuVisibilityChangedListener;,
        Lcom/googlecode/eyesfree/widget/RadialMenu$MenuLayoutListener;
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mCorners:Lcom/googlecode/eyesfree/utils/SparseIterableArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/googlecode/eyesfree/utils/SparseIterableArray",
            "<",
            "Lcom/googlecode/eyesfree/widget/RadialMenuItem;",
            ">;"
        }
    .end annotation
.end field

.field private final mItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/googlecode/eyesfree/widget/RadialMenuItem;",
            ">;"
        }
    .end annotation
.end field

.field private mLayoutListener:Lcom/googlecode/eyesfree/widget/RadialMenu$MenuLayoutListener;

.field private mListener:Landroid/view/MenuItem$OnMenuItemClickListener;

.field private final mParent:Landroid/content/DialogInterface;

.field private mQwertyMode:Z

.field private mSelectionListener:Lcom/googlecode/eyesfree/widget/RadialMenuItem$OnMenuItemSelectionListener;

.field private mVisibilityListener:Lcom/googlecode/eyesfree/widget/RadialMenu$OnMenuVisibilityChangedListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/DialogInterface;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/DialogInterface;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mParent:Landroid/content/DialogInterface;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mItems:Ljava/util/List;

    new-instance v0, Lcom/googlecode/eyesfree/utils/SparseIterableArray;

    invoke-direct {v0}, Lcom/googlecode/eyesfree/utils/SparseIterableArray;-><init>()V

    iput-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mCorners:Lcom/googlecode/eyesfree/utils/SparseIterableArray;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mQwertyMode:Z

    return-void
.end method

.method static getCornerLocation(I)Landroid/graphics/PointF;
    .locals 3
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :pswitch_0
    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_1
    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    goto :goto_0

    :pswitch_1
    const/high16 v0, 0x3f800000

    const/4 v1, 0x0

    goto :goto_1

    :pswitch_2
    const/high16 v0, 0x3f800000

    const/high16 v1, 0x3f800000

    goto :goto_1

    :pswitch_3
    const/4 v0, 0x0

    const/high16 v1, 0x3f800000

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method static getCornerRotation(I)F
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    const/high16 v0, 0x43070000

    goto :goto_0

    :pswitch_1
    const/high16 v0, -0x3cf90000

    goto :goto_0

    :pswitch_2
    const/high16 v0, -0x3dcc0000

    goto :goto_0

    :pswitch_3
    const/high16 v0, 0x42340000

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method private getItemForShortcut(ILandroid/view/KeyEvent;)Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    .locals 3
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    invoke-virtual {v1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->getAlphabeticShortcut()C

    move-result v2

    if-eq v2, p1, :cond_1

    invoke-virtual {v1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->getNumericShortcut()C

    move-result v2

    if-ne v2, p1, :cond_0

    :cond_1
    :goto_0
    return-object v1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic add(I)Landroid/view/MenuItem;
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/widget/RadialMenu;->add(I)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic add(IIII)Landroid/view/MenuItem;
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/googlecode/eyesfree/widget/RadialMenu;->add(IIII)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/CharSequence;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/googlecode/eyesfree/widget/RadialMenu;->add(IIILjava/lang/CharSequence;)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/widget/RadialMenu;->add(Ljava/lang/CharSequence;)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v0

    return-object v0
.end method

.method public add(I)Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v0, v0, p1}, Lcom/googlecode/eyesfree/widget/RadialMenu;->add(IIII)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v0

    return-object v0
.end method

.method public add(IIII)Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mContext:Landroid/content/Context;

    invoke-virtual {v1, p4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/googlecode/eyesfree/widget/RadialMenu;->add(IIILjava/lang/CharSequence;)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v1

    return-object v1
.end method

.method public add(IIILjava/lang/CharSequence;)Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    .locals 6
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/CharSequence;

    new-instance v0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mContext:Landroid/content/Context;

    move v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;-><init>(Landroid/content/Context;IIILjava/lang/CharSequence;)V

    invoke-virtual {p0, v0}, Lcom/googlecode/eyesfree/widget/RadialMenu;->add(Lcom/googlecode/eyesfree/widget/RadialMenuItem;)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v1

    return-object v1
.end method

.method public add(Lcom/googlecode/eyesfree/widget/RadialMenuItem;)Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    .locals 2
    .param p1    # Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->hasSubMenu()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mSelectionListener:Lcom/googlecode/eyesfree/widget/RadialMenuItem$OnMenuItemSelectionListener;

    invoke-virtual {p1, v0}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->setOnMenuItemSelectionListener(Lcom/googlecode/eyesfree/widget/RadialMenuItem$OnMenuItemSelectionListener;)Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mListener:Landroid/view/MenuItem$OnMenuItemClickListener;

    invoke-virtual {p1, v0}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    :cond_0
    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->getGroupId()I

    move-result v0

    sget v1, Lcom/googlecode/eyesfree/widget/R$id;->group_corners:I

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->setCorner(Z)Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mCorners:Lcom/googlecode/eyesfree/utils/SparseIterableArray;

    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->getOrder()I

    move-result v1

    invoke-virtual {v0, v1, p1}, Lcom/googlecode/eyesfree/utils/SparseIterableArray;->put(ILjava/lang/Object;)V

    :goto_0
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/RadialMenu;->onLayoutChanged()V

    return-object p1

    :cond_1
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public add(Ljava/lang/CharSequence;)Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v0, v0, p1}, Lcom/googlecode/eyesfree/widget/RadialMenu;->add(IIILjava/lang/CharSequence;)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v0

    return-object v0
.end method

.method public addIntentOptions(IIILandroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I[Landroid/view/MenuItem;)I
    .locals 13
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Landroid/content/ComponentName;
    .param p5    # [Landroid/content/Intent;
    .param p6    # Landroid/content/Intent;
    .param p7    # I
    .param p8    # [Landroid/view/MenuItem;

    iget-object v12, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mContext:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v10

    const/4 v12, 0x0

    move-object/from16 v0, p4

    move-object/from16 v1, p5

    move-object/from16 v2, p6

    invoke-virtual {v10, v0, v1, v2, v12}, Landroid/content/pm/PackageManager;->queryIntentActivityOptions(Landroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v8

    and-int/lit8 v12, p7, 0x1

    if-nez v12, :cond_0

    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/widget/RadialMenu;->removeGroup(I)V

    :cond_0
    const/4 v3, 0x0

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/pm/ResolveInfo;

    invoke-virtual {v7, v10}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v7, v10}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v11

    move/from16 v0, p3

    invoke-virtual {p0, p1, p2, v0, v11}, Lcom/googlecode/eyesfree/widget/RadialMenu;->add(IIILjava/lang/CharSequence;)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v9

    invoke-interface {v9, v6}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    move-object/from16 v0, p8

    array-length v12, v0

    if-ge v3, v12, :cond_1

    add-int/lit8 v4, v3, 0x1

    aput-object v9, p8, v3

    move v3, v4

    goto :goto_0

    :cond_1
    new-instance v12, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v12}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v12

    :cond_2
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v12

    return v12
.end method

.method public bridge synthetic addSubMenu(I)Landroid/view/SubMenu;
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/widget/RadialMenu;->addSubMenu(I)Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic addSubMenu(IIII)Landroid/view/SubMenu;
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/googlecode/eyesfree/widget/RadialMenu;->addSubMenu(IIII)Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/CharSequence;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/googlecode/eyesfree/widget/RadialMenu;->addSubMenu(IIILjava/lang/CharSequence;)Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic addSubMenu(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/widget/RadialMenu;->addSubMenu(Ljava/lang/CharSequence;)Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    move-result-object v0

    return-object v0
.end method

.method public addSubMenu(I)Lcom/googlecode/eyesfree/widget/RadialSubMenu;
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v0, v0, p1}, Lcom/googlecode/eyesfree/widget/RadialMenu;->addSubMenu(IIII)Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    move-result-object v0

    return-object v0
.end method

.method public addSubMenu(IIII)Lcom/googlecode/eyesfree/widget/RadialSubMenu;
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mContext:Landroid/content/Context;

    invoke-virtual {v1, p4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/googlecode/eyesfree/widget/RadialMenu;->addSubMenu(IIILjava/lang/CharSequence;)Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    move-result-object v1

    return-object v1
.end method

.method public addSubMenu(IIILjava/lang/CharSequence;)Lcom/googlecode/eyesfree/widget/RadialSubMenu;
    .locals 8
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/CharSequence;

    new-instance v0, Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mParent:Landroid/content/DialogInterface;

    move-object v3, p0

    move v4, p1

    move v5, p2

    move v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/googlecode/eyesfree/widget/RadialSubMenu;-><init>(Landroid/content/Context;Landroid/content/DialogInterface;Lcom/googlecode/eyesfree/widget/RadialMenu;IIILjava/lang/CharSequence;)V

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/widget/RadialSubMenu;->getItem()Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/googlecode/eyesfree/widget/RadialMenu;->add(Lcom/googlecode/eyesfree/widget/RadialMenuItem;)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    return-object v0
.end method

.method public addSubMenu(Ljava/lang/CharSequence;)Lcom/googlecode/eyesfree/widget/RadialSubMenu;
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v0, v0, p1}, Lcom/googlecode/eyesfree/widget/RadialMenu;->addSubMenu(IIILjava/lang/CharSequence;)Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    move-result-object v0

    return-object v0
.end method

.method public clear()V
    .locals 1

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method public clearSelection(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/googlecode/eyesfree/widget/RadialMenu;->selectMenuItem(Lcom/googlecode/eyesfree/widget/RadialMenuItem;I)Z

    move-result v0

    return v0
.end method

.method public close()V
    .locals 1

    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/RadialMenu;->onDismiss()V

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mParent:Landroid/content/DialogInterface;

    invoke-interface {v0}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method

.method public bridge synthetic findItem(I)Landroid/view/MenuItem;
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/widget/RadialMenu;->findItem(I)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v0

    return-object v0
.end method

.method public findItem(I)Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    .locals 4
    .param p1    # I

    const/4 v2, 0x0

    const/4 v3, -0x1

    if-ne p1, v3, :cond_0

    move-object v1, v2

    :goto_0
    return-object v1

    :cond_0
    iget-object v3, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mItems:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    invoke-virtual {v1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->getItemId()I

    move-result v3

    if-ne v3, p1, :cond_1

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mCorners:Lcom/googlecode/eyesfree/utils/SparseIterableArray;

    invoke-virtual {v3}, Lcom/googlecode/eyesfree/utils/SparseIterableArray;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    invoke-virtual {v1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->getItemId()I

    move-result v3

    if-ne v3, p1, :cond_3

    goto :goto_0

    :cond_4
    move-object v1, v2

    goto :goto_0
.end method

.method public getCorner(I)Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mCorners:Lcom/googlecode/eyesfree/utils/SparseIterableArray;

    invoke-virtual {v0, p1}, Lcom/googlecode/eyesfree/utils/SparseIterableArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Landroid/view/MenuItem;
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/widget/RadialMenu;->getItem(I)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v0

    return-object v0
.end method

.method public getItem(I)Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    return-object v0
.end method

.method public hasVisibleItems()Z
    .locals 3

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    invoke-interface {v1}, Landroid/view/MenuItem;->isVisible()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public indexOf(Lcom/googlecode/eyesfree/widget/RadialMenuItem;)I
    .locals 1
    .param p1    # Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isShortcutKey(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    invoke-direct {p0, p1, p2}, Lcom/googlecode/eyesfree/widget/RadialMenu;->getItemForShortcut(ILandroid/view/KeyEvent;)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method onDismiss()V
    .locals 1

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mVisibilityListener:Lcom/googlecode/eyesfree/widget/RadialMenu$OnMenuVisibilityChangedListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mVisibilityListener:Lcom/googlecode/eyesfree/widget/RadialMenu$OnMenuVisibilityChangedListener;

    invoke-interface {v0, p0}, Lcom/googlecode/eyesfree/widget/RadialMenu$OnMenuVisibilityChangedListener;->onMenuDismissed(Lcom/googlecode/eyesfree/widget/RadialMenu;)V

    :cond_0
    return-void
.end method

.method protected onLayoutChanged()V
    .locals 1

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mLayoutListener:Lcom/googlecode/eyesfree/widget/RadialMenu$MenuLayoutListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mLayoutListener:Lcom/googlecode/eyesfree/widget/RadialMenu$MenuLayoutListener;

    invoke-interface {v0}, Lcom/googlecode/eyesfree/widget/RadialMenu$MenuLayoutListener;->onLayoutChanged()V

    :cond_0
    return-void
.end method

.method onShow()V
    .locals 1

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mVisibilityListener:Lcom/googlecode/eyesfree/widget/RadialMenu$OnMenuVisibilityChangedListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mVisibilityListener:Lcom/googlecode/eyesfree/widget/RadialMenu$OnMenuVisibilityChangedListener;

    invoke-interface {v0, p0}, Lcom/googlecode/eyesfree/widget/RadialMenu$OnMenuVisibilityChangedListener;->onMenuShown(Lcom/googlecode/eyesfree/widget/RadialMenu;)V

    :cond_0
    return-void
.end method

.method public performIdentifierAction(II)Z
    .locals 2
    .param p1    # I
    .param p2    # I

    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/widget/RadialMenu;->findItem(I)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/googlecode/eyesfree/widget/RadialMenu;->performMenuItem(Lcom/googlecode/eyesfree/widget/RadialMenuItem;I)Z

    move-result v1

    return v1
.end method

.method public performMenuItem(Lcom/googlecode/eyesfree/widget/RadialMenuItem;I)Z
    .locals 2
    .param p1    # Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    .param p2    # I

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->onClickPerformed()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mListener:Landroid/view/MenuItem$OnMenuItemClickListener;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mListener:Landroid/view/MenuItem$OnMenuItemClickListener;

    invoke-interface {v1, p1}, Landroid/view/MenuItem$OnMenuItemClickListener;->onMenuItemClick(Landroid/view/MenuItem;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz p1, :cond_1

    and-int/lit8 v1, p2, 0x1

    if-eqz v1, :cond_1

    and-int/lit8 v1, p2, 0x2

    if-eqz v1, :cond_2

    :cond_1
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/RadialMenu;->close()V

    :cond_2
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public performShortcut(ILandroid/view/KeyEvent;I)Z
    .locals 2
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;
    .param p3    # I

    invoke-direct {p0, p1, p2}, Lcom/googlecode/eyesfree/widget/RadialMenu;->getItemForShortcut(ILandroid/view/KeyEvent;)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v0

    invoke-virtual {p0, v0, p3}, Lcom/googlecode/eyesfree/widget/RadialMenu;->performMenuItem(Lcom/googlecode/eyesfree/widget/RadialMenuItem;I)Z

    move-result v1

    return v1
.end method

.method public removeGroup(I)V
    .locals 4
    .param p1    # I

    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iget-object v3, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mItems:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    invoke-interface {v1}, Landroid/view/MenuItem;->getGroupId()I

    move-result v3

    if-ne v3, p1, :cond_0

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mItems:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_2

    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/RadialMenu;->onLayoutChanged()V

    :cond_2
    return-void
.end method

.method public removeItem(I)V
    .locals 2
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/widget/RadialMenu;->findItem(I)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mItems:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/RadialMenu;->onLayoutChanged()V

    goto :goto_0
.end method

.method public selectMenuItem(Lcom/googlecode/eyesfree/widget/RadialMenuItem;I)Z
    .locals 2
    .param p1    # Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    .param p2    # I

    const/4 v0, 0x1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->onSelectionPerformed()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mSelectionListener:Lcom/googlecode/eyesfree/widget/RadialMenuItem$OnMenuItemSelectionListener;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mSelectionListener:Lcom/googlecode/eyesfree/widget/RadialMenuItem$OnMenuItemSelectionListener;

    invoke-interface {v1, p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem$OnMenuItemSelectionListener;->onMenuItemSelection(Lcom/googlecode/eyesfree/widget/RadialMenuItem;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setDefaultListener(Landroid/view/MenuItem$OnMenuItemClickListener;)V
    .locals 0
    .param p1    # Landroid/view/MenuItem$OnMenuItemClickListener;

    iput-object p1, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mListener:Landroid/view/MenuItem$OnMenuItemClickListener;

    return-void
.end method

.method public setDefaultSelectionListener(Lcom/googlecode/eyesfree/widget/RadialMenuItem$OnMenuItemSelectionListener;)V
    .locals 0
    .param p1    # Lcom/googlecode/eyesfree/widget/RadialMenuItem$OnMenuItemSelectionListener;

    iput-object p1, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mSelectionListener:Lcom/googlecode/eyesfree/widget/RadialMenuItem$OnMenuItemSelectionListener;

    return-void
.end method

.method public setGroupCheckable(IZZ)V
    .locals 4
    .param p1    # I
    .param p2    # Z
    .param p3    # Z

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mItems:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    invoke-interface {v1}, Landroid/view/MenuItem;->getGroupId()I

    move-result v3

    if-ne v3, p1, :cond_0

    invoke-interface {v1, p2}, Landroid/view/MenuItem;->setCheckable(Z)Landroid/view/MenuItem;

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/RadialMenu;->onLayoutChanged()V

    :cond_2
    return-void
.end method

.method public setGroupEnabled(IZ)V
    .locals 4
    .param p1    # I
    .param p2    # Z

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mItems:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    invoke-interface {v1}, Landroid/view/MenuItem;->getGroupId()I

    move-result v3

    if-ne v3, p1, :cond_0

    invoke-interface {v1, p2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/RadialMenu;->onLayoutChanged()V

    :cond_2
    return-void
.end method

.method public setGroupVisible(IZ)V
    .locals 4
    .param p1    # I
    .param p2    # Z

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mItems:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    invoke-interface {v1}, Landroid/view/MenuItem;->getGroupId()I

    move-result v3

    if-ne v3, p1, :cond_0

    invoke-interface {v1, p2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/RadialMenu;->onLayoutChanged()V

    :cond_2
    return-void
.end method

.method public setLayoutListener(Lcom/googlecode/eyesfree/widget/RadialMenu$MenuLayoutListener;)V
    .locals 0
    .param p1    # Lcom/googlecode/eyesfree/widget/RadialMenu$MenuLayoutListener;

    iput-object p1, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mLayoutListener:Lcom/googlecode/eyesfree/widget/RadialMenu$MenuLayoutListener;

    return-void
.end method

.method public setOnMenuVisibilityChangedListener(Lcom/googlecode/eyesfree/widget/RadialMenu$OnMenuVisibilityChangedListener;)V
    .locals 0
    .param p1    # Lcom/googlecode/eyesfree/widget/RadialMenu$OnMenuVisibilityChangedListener;

    iput-object p1, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mVisibilityListener:Lcom/googlecode/eyesfree/widget/RadialMenu$OnMenuVisibilityChangedListener;

    return-void
.end method

.method public setQwertyMode(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mQwertyMode:Z

    return-void
.end method

.method public size()I
    .locals 1

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenu;->mItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
