.class public Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;
.super Lcom/googlecode/eyesfree/widget/SimpleOverlay;
.source "RadialMenuOverlay.java"

# interfaces
.implements Landroid/content/DialogInterface;


# instance fields
.field private final mMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;

.field private final mMenuView:Lcom/googlecode/eyesfree/widget/RadialMenuView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Z

    invoke-direct {p0, p1}, Lcom/googlecode/eyesfree/widget/SimpleOverlay;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/googlecode/eyesfree/widget/RadialMenu;

    invoke-direct {v0, p1, p0}, Lcom/googlecode/eyesfree/widget/RadialMenu;-><init>(Landroid/content/Context;Landroid/content/DialogInterface;)V

    iput-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;->mMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;

    new-instance v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;

    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;->mMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;

    invoke-direct {v0, p1, v1, p2}, Lcom/googlecode/eyesfree/widget/RadialMenuView;-><init>(Landroid/content/Context;Lcom/googlecode/eyesfree/widget/RadialMenu;Z)V

    iput-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;->mMenuView:Lcom/googlecode/eyesfree/widget/RadialMenuView;

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;->mMenuView:Lcom/googlecode/eyesfree/widget/RadialMenuView;

    invoke-virtual {p0, v0}, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;->setContentView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 0

    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;->dismiss()V

    return-void
.end method

.method public dismiss()V
    .locals 1

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;->mMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/widget/RadialMenu;->onDismiss()V

    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;->hide()V

    return-void
.end method

.method public getMenu()Lcom/googlecode/eyesfree/widget/RadialMenu;
    .locals 1

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;->mMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;

    return-object v0
.end method

.method public getView()Lcom/googlecode/eyesfree/widget/RadialMenuView;
    .locals 1

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;->mMenuView:Lcom/googlecode/eyesfree/widget/RadialMenuView;

    return-object v0
.end method

.method public showWithDot()V
    .locals 1

    invoke-super {p0}, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->show()V

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;->mMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/widget/RadialMenu;->onShow()V

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;->mMenuView:Lcom/googlecode/eyesfree/widget/RadialMenuView;

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->displayDot()V

    return-void
.end method
