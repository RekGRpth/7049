.class Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;
.super Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;
.source "RadialMenuView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/googlecode/eyesfree/widget/RadialMenuView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RadialMenuHelper"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/googlecode/eyesfree/utils/TouchExplorationHelper",
        "<",
        "Lcom/googlecode/eyesfree/widget/RadialMenuItem;",
        ">;"
    }
.end annotation


# instance fields
.field private final mTempRect:Landroid/graphics/Rect;

.field final synthetic this$0:Lcom/googlecode/eyesfree/widget/RadialMenuView;


# direct methods
.method public constructor <init>(Lcom/googlecode/eyesfree/widget/RadialMenuView;Landroid/content/Context;)V
    .locals 1
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;->this$0:Lcom/googlecode/eyesfree/widget/RadialMenuView;

    invoke-direct {p0, p2}, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;->mTempRect:Landroid/graphics/Rect;

    return-void
.end method


# virtual methods
.method protected getIdForItem(Lcom/googlecode/eyesfree/widget/RadialMenuItem;)I
    .locals 1
    .param p1    # Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;->this$0:Lcom/googlecode/eyesfree/widget/RadialMenuView;

    # getter for: Lcom/googlecode/eyesfree/widget/RadialMenuView;->mRootMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;
    invoke-static {v0}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->access$400(Lcom/googlecode/eyesfree/widget/RadialMenuView;)Lcom/googlecode/eyesfree/widget/RadialMenu;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/googlecode/eyesfree/widget/RadialMenu;->indexOf(Lcom/googlecode/eyesfree/widget/RadialMenuItem;)I

    move-result v0

    return v0
.end method

.method protected bridge synthetic getIdForItem(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;->getIdForItem(Lcom/googlecode/eyesfree/widget/RadialMenuItem;)I

    move-result v0

    return v0
.end method

.method protected getItemAt(FF)Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    .locals 1
    .param p1    # F
    .param p2    # F

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;->this$0:Lcom/googlecode/eyesfree/widget/RadialMenuView;

    # invokes: Lcom/googlecode/eyesfree/widget/RadialMenuView;->computeTouchedMenuItem(FF)Landroid/util/Pair;
    invoke-static {v0, p1, p2}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->access$500(Lcom/googlecode/eyesfree/widget/RadialMenuView;FF)Landroid/util/Pair;

    move-result-object v0

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    return-object v0
.end method

.method protected bridge synthetic getItemAt(FF)Ljava/lang/Object;
    .locals 1
    .param p1    # F
    .param p2    # F

    invoke-virtual {p0, p1, p2}, Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;->getItemAt(FF)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v0

    return-object v0
.end method

.method protected getItemForId(I)Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;->this$0:Lcom/googlecode/eyesfree/widget/RadialMenuView;

    # getter for: Lcom/googlecode/eyesfree/widget/RadialMenuView;->mRootMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;
    invoke-static {v0}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->access$400(Lcom/googlecode/eyesfree/widget/RadialMenuView;)Lcom/googlecode/eyesfree/widget/RadialMenu;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/googlecode/eyesfree/widget/RadialMenu;->getItem(I)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic getItemForId(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;->getItemForId(I)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v0

    return-object v0
.end method

.method protected getVisibleItems(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/googlecode/eyesfree/widget/RadialMenuItem;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;->this$0:Lcom/googlecode/eyesfree/widget/RadialMenuView;

    # getter for: Lcom/googlecode/eyesfree/widget/RadialMenuView;->mRootMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;
    invoke-static {v2}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->access$400(Lcom/googlecode/eyesfree/widget/RadialMenuView;)Lcom/googlecode/eyesfree/widget/RadialMenu;

    move-result-object v2

    invoke-virtual {v2}, Lcom/googlecode/eyesfree/widget/RadialMenu;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;->this$0:Lcom/googlecode/eyesfree/widget/RadialMenuView;

    # getter for: Lcom/googlecode/eyesfree/widget/RadialMenuView;->mRootMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;
    invoke-static {v2}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->access$400(Lcom/googlecode/eyesfree/widget/RadialMenuView;)Lcom/googlecode/eyesfree/widget/RadialMenu;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/googlecode/eyesfree/widget/RadialMenu;->getItem(I)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected performActionForItem(Lcom/googlecode/eyesfree/widget/RadialMenuItem;ILandroid/os/Bundle;)Z
    .locals 1
    .param p1    # Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    .param p2    # I
    .param p3    # Landroid/os/Bundle;

    packed-switch p2, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->onClickPerformed()Z

    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x10
        :pswitch_0
    .end packed-switch
.end method

.method protected bridge synthetic performActionForItem(Ljava/lang/Object;ILandroid/os/Bundle;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # I
    .param p3    # Landroid/os/Bundle;

    check-cast p1, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    invoke-virtual {p0, p1, p2, p3}, Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;->performActionForItem(Lcom/googlecode/eyesfree/widget/RadialMenuItem;ILandroid/os/Bundle;)Z

    move-result v0

    return v0
.end method

.method protected populateEventForItem(Lcom/googlecode/eyesfree/widget/RadialMenuItem;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1
    .param p1    # Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    .param p2    # Landroid/view/accessibility/AccessibilityEvent;

    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->isChecked()Z

    move-result v0

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setChecked(Z)V

    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->isEnabled()Z

    move-result v0

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setEnabled(Z)V

    return-void
.end method

.method protected bridge synthetic populateEventForItem(Ljava/lang/Object;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Landroid/view/accessibility/AccessibilityEvent;

    check-cast p1, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    invoke-virtual {p0, p1, p2}, Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;->populateEventForItem(Lcom/googlecode/eyesfree/widget/RadialMenuItem;Landroid/view/accessibility/AccessibilityEvent;)V

    return-void
.end method

.method protected populateNodeForItem(Lcom/googlecode/eyesfree/widget/RadialMenuItem;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    .locals 2
    .param p1    # Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    .param p2    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->isVisible()Z

    move-result v0

    invoke-virtual {p2, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setVisibleToUser(Z)V

    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->isCheckable()Z

    move-result v0

    invoke-virtual {p2, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setCheckable(Z)V

    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->isChecked()Z

    move-result v0

    invoke-virtual {p2, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setChecked(Z)V

    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->isEnabled()Z

    move-result v0

    invoke-virtual {p2, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setEnabled(Z)V

    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setClickable(Z)V

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;->this$0:Lcom/googlecode/eyesfree/widget/RadialMenuView;

    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {p2, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setBoundsInParent(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;->this$0:Lcom/googlecode/eyesfree/widget/RadialMenuView;

    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {p2, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setBoundsInScreen(Landroid/graphics/Rect;)V

    return-void
.end method

.method protected bridge synthetic populateNodeForItem(Ljava/lang/Object;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    check-cast p1, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    invoke-virtual {p0, p1, p2}, Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;->populateNodeForItem(Lcom/googlecode/eyesfree/widget/RadialMenuItem;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    return-void
.end method
