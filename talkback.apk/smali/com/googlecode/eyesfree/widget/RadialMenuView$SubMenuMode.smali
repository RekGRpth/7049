.class public final enum Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;
.super Ljava/lang/Enum;
.source "RadialMenuView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/googlecode/eyesfree/widget/RadialMenuView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SubMenuMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;

.field public static final enum LIFT_TO_ACTIVATE:Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;

.field public static final enum LONG_PRESS:Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;

    const-string v1, "LONG_PRESS"

    invoke-direct {v0, v1, v2}, Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;->LONG_PRESS:Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;

    new-instance v0, Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;

    const-string v1, "LIFT_TO_ACTIVATE"

    invoke-direct {v0, v1, v3}, Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;->LIFT_TO_ACTIVATE:Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;

    sget-object v1, Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;->LONG_PRESS:Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;->LIFT_TO_ACTIVATE:Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;

    aput-object v1, v0, v3

    sput-object v0, Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;->$VALUES:[Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;

    return-object v0
.end method

.method public static values()[Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;
    .locals 1

    sget-object v0, Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;->$VALUES:[Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;

    invoke-virtual {v0}, [Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;

    return-object v0
.end method
