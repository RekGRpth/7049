.class public Lcom/leff/midi/MidiFile;
.super Ljava/lang/Object;
.source "MidiFile.java"


# static fields
.field public static final IDENTIFIER:[B


# instance fields
.field private mResolution:I

.field private mTrackCount:I

.field private mTracks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/leff/midi/MidiTrack;",
            ">;"
        }
    .end annotation
.end field

.field private mType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/leff/midi/MidiFile;->IDENTIFIER:[B

    return-void

    nop

    :array_0
    .array-data 1
        0x4dt
        0x54t
        0x68t
        0x64t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    const/16 v0, 0x1e0

    invoke-direct {p0, v0}, Lcom/leff/midi/MidiFile;-><init>(I)V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1    # I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {p0, p1, v0}, Lcom/leff/midi/MidiFile;-><init>(ILjava/util/ArrayList;)V

    return-void
.end method

.method public constructor <init>(ILjava/util/ArrayList;)V
    .locals 2
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/leff/midi/MidiTrack;",
            ">;)V"
        }
    .end annotation

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-ltz p1, :cond_0

    :goto_0
    iput p1, p0, Lcom/leff/midi/MidiFile;->mResolution:I

    if-eqz p2, :cond_1

    move-object v0, p2

    :goto_1
    iput-object v0, p0, Lcom/leff/midi/MidiFile;->mTracks:Ljava/util/ArrayList;

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/leff/midi/MidiFile;->mTrackCount:I

    iget v0, p0, Lcom/leff/midi/MidiFile;->mTrackCount:I

    if-le v0, v1, :cond_2

    move v0, v1

    :goto_2
    iput v0, p0, Lcom/leff/midi/MidiFile;->mType:I

    return-void

    :cond_0
    const/16 p1, 0x1e0

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method


# virtual methods
.method public writeToFile(Ljava/io/File;)V
    .locals 6
    .param p1    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v5, 0x2

    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    sget-object v3, Lcom/leff/midi/MidiFile;->IDENTIFIER:[B

    invoke-virtual {v1, v3}, Ljava/io/FileOutputStream;->write([B)V

    const/4 v3, 0x6

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/leff/midi/util/MidiUtil;->intToBytes(II)[B

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/io/FileOutputStream;->write([B)V

    iget v3, p0, Lcom/leff/midi/MidiFile;->mType:I

    invoke-static {v3, v5}, Lcom/leff/midi/util/MidiUtil;->intToBytes(II)[B

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/io/FileOutputStream;->write([B)V

    iget v3, p0, Lcom/leff/midi/MidiFile;->mTrackCount:I

    invoke-static {v3, v5}, Lcom/leff/midi/util/MidiUtil;->intToBytes(II)[B

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/io/FileOutputStream;->write([B)V

    iget v3, p0, Lcom/leff/midi/MidiFile;->mResolution:I

    invoke-static {v3, v5}, Lcom/leff/midi/util/MidiUtil;->intToBytes(II)[B

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/io/FileOutputStream;->write([B)V

    iget-object v3, p0, Lcom/leff/midi/MidiFile;->mTracks:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/leff/midi/MidiTrack;

    invoke-virtual {v0, v1}, Lcom/leff/midi/MidiTrack;->writeToFile(Ljava/io/OutputStream;)V

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->flush()V

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    return-void
.end method
