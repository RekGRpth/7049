.class public Lcom/leff/midi/MidiTrack;
.super Ljava/lang/Object;
.source "MidiTrack.java"


# static fields
.field public static final IDENTIFIER:[B


# instance fields
.field private mClosed:Z

.field private mEvents:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet",
            "<",
            "Lcom/leff/midi/event/MidiEvent;",
            ">;"
        }
    .end annotation
.end field

.field private mSize:I

.field private mSizeNeedsRecalculating:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/leff/midi/MidiTrack;->IDENTIFIER:[B

    return-void

    nop

    :array_0
    .array-data 1
        0x4dt
        0x54t
        0x72t
        0x6bt
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    iput-object v0, p0, Lcom/leff/midi/MidiTrack;->mEvents:Ljava/util/TreeSet;

    iput v1, p0, Lcom/leff/midi/MidiTrack;->mSize:I

    iput-boolean v1, p0, Lcom/leff/midi/MidiTrack;->mSizeNeedsRecalculating:Z

    iput-boolean v1, p0, Lcom/leff/midi/MidiTrack;->mClosed:Z

    return-void
.end method

.method private recalculateSize()V
    .locals 6

    const/4 v5, 0x0

    iput v5, p0, Lcom/leff/midi/MidiTrack;->mSize:I

    iget-object v3, p0, Lcom/leff/midi/MidiTrack;->mEvents:Ljava/util/TreeSet;

    invoke-virtual {v3}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/leff/midi/event/MidiEvent;

    iget v3, p0, Lcom/leff/midi/MidiTrack;->mSize:I

    invoke-virtual {v0}, Lcom/leff/midi/event/MidiEvent;->getSize()I

    move-result v4

    add-int/2addr v3, v4

    iput v3, p0, Lcom/leff/midi/MidiTrack;->mSize:I

    if-eqz v2, :cond_0

    invoke-virtual {v0, v2}, Lcom/leff/midi/event/MidiEvent;->requiresStatusByte(Lcom/leff/midi/event/MidiEvent;)Z

    move-result v3

    if-nez v3, :cond_0

    iget v3, p0, Lcom/leff/midi/MidiTrack;->mSize:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/leff/midi/MidiTrack;->mSize:I

    :cond_0
    move-object v2, v0

    goto :goto_0

    :cond_1
    iput-boolean v5, p0, Lcom/leff/midi/MidiTrack;->mSizeNeedsRecalculating:Z

    return-void
.end method


# virtual methods
.method public closeTrack()V
    .locals 8

    const-wide/16 v2, 0x0

    iget-object v4, p0, Lcom/leff/midi/MidiTrack;->mEvents:Ljava/util/TreeSet;

    invoke-virtual {v4}, Ljava/util/TreeSet;->size()I

    move-result v4

    if-lez v4, :cond_0

    iget-object v4, p0, Lcom/leff/midi/MidiTrack;->mEvents:Ljava/util/TreeSet;

    invoke-virtual {v4}, Ljava/util/TreeSet;->last()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/leff/midi/event/MidiEvent;

    invoke-virtual {v1}, Lcom/leff/midi/event/MidiEvent;->getTick()J

    move-result-wide v4

    const-wide/16 v6, 0x1

    add-long v2, v4, v6

    :cond_0
    new-instance v0, Lcom/leff/midi/event/meta/EndOfTrack;

    const-wide/16 v4, 0x0

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/leff/midi/event/meta/EndOfTrack;-><init>(JJ)V

    invoke-virtual {p0, v0}, Lcom/leff/midi/MidiTrack;->insertEvent(Lcom/leff/midi/event/MidiEvent;)V

    return-void
.end method

.method public insertEvent(Lcom/leff/midi/event/MidiEvent;)V
    .locals 10
    .param p1    # Lcom/leff/midi/event/MidiEvent;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    const/4 v9, 0x1

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v5, p0, Lcom/leff/midi/MidiTrack;->mClosed:Z

    if-eqz v5, :cond_2

    sget-object v5, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v6, "Error: Cannot add an event to a closed track."

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_0
    const-string v5, "java.util.TreeSet"

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    const-string v5, "floor"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Ljava/lang/Object;

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    const-string v5, "ceiling"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Ljava/lang/Object;

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v2, 0x1

    :goto_1
    if-eqz v2, :cond_5

    iget-object v5, p0, Lcom/leff/midi/MidiTrack;->mEvents:Ljava/util/TreeSet;

    invoke-virtual {v5, p1}, Ljava/util/TreeSet;->floor(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/leff/midi/event/MidiEvent;

    iget-object v5, p0, Lcom/leff/midi/MidiTrack;->mEvents:Ljava/util/TreeSet;

    invoke-virtual {v5, p1}, Ljava/util/TreeSet;->ceiling(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/leff/midi/event/MidiEvent;

    :cond_3
    iget-object v5, p0, Lcom/leff/midi/MidiTrack;->mEvents:Ljava/util/TreeSet;

    invoke-virtual {v5, p1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    iput-boolean v9, p0, Lcom/leff/midi/MidiTrack;->mSizeNeedsRecalculating:Z

    if-eqz v3, :cond_6

    invoke-virtual {p1}, Lcom/leff/midi/event/MidiEvent;->getTick()J

    move-result-wide v5

    invoke-virtual {v3}, Lcom/leff/midi/event/MidiEvent;->getTick()J

    move-result-wide v7

    sub-long/2addr v5, v7

    invoke-virtual {p1, v5, v6}, Lcom/leff/midi/event/MidiEvent;->setDelta(J)V

    :goto_2
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/leff/midi/event/MidiEvent;->getTick()J

    move-result-wide v5

    invoke-virtual {p1}, Lcom/leff/midi/event/MidiEvent;->getTick()J

    move-result-wide v7

    sub-long/2addr v5, v7

    invoke-virtual {v1, v5, v6}, Lcom/leff/midi/event/MidiEvent;->setDelta(J)V

    :cond_4
    iget v5, p0, Lcom/leff/midi/MidiTrack;->mSize:I

    invoke-virtual {p1}, Lcom/leff/midi/event/MidiEvent;->getSize()I

    move-result v6

    add-int/2addr v5, v6

    iput v5, p0, Lcom/leff/midi/MidiTrack;->mSize:I

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    const-class v6, Lcom/leff/midi/event/meta/EndOfTrack;

    invoke-virtual {v5, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    if-eqz v1, :cond_7

    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "Attempting to insert EndOfTrack before an existing event. Use closeTrack() when finished with MidiTrack."

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_5
    iget-object v5, p0, Lcom/leff/midi/MidiTrack;->mEvents:Ljava/util/TreeSet;

    invoke-virtual {v5}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/leff/midi/event/MidiEvent;

    invoke-virtual {v1}, Lcom/leff/midi/event/MidiEvent;->getTick()J

    move-result-wide v5

    invoke-virtual {p1}, Lcom/leff/midi/event/MidiEvent;->getTick()J

    move-result-wide v7

    cmp-long v5, v5, v7

    if-gtz v5, :cond_3

    move-object v3, v1

    const/4 v1, 0x0

    goto :goto_3

    :cond_6
    invoke-virtual {p1}, Lcom/leff/midi/event/MidiEvent;->getTick()J

    move-result-wide v5

    invoke-virtual {p1, v5, v6}, Lcom/leff/midi/event/MidiEvent;->setDelta(J)V

    goto :goto_2

    :cond_7
    iput-boolean v9, p0, Lcom/leff/midi/MidiTrack;->mClosed:Z

    goto/16 :goto_0

    :catch_0
    move-exception v5

    goto/16 :goto_1
.end method

.method public insertNote(IIIJJ)V
    .locals 6
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # J
    .param p6    # J

    new-instance v0, Lcom/leff/midi/event/NoteOn;

    move-wide v1, p4

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/leff/midi/event/NoteOn;-><init>(JIII)V

    invoke-virtual {p0, v0}, Lcom/leff/midi/MidiTrack;->insertEvent(Lcom/leff/midi/event/MidiEvent;)V

    new-instance v0, Lcom/leff/midi/event/NoteOff;

    add-long v1, p4, p6

    const/4 v5, 0x0

    move v3, p1

    move v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/leff/midi/event/NoteOff;-><init>(JIII)V

    invoke-virtual {p0, v0}, Lcom/leff/midi/MidiTrack;->insertEvent(Lcom/leff/midi/event/MidiEvent;)V

    return-void
.end method

.method public writeToFile(Ljava/io/OutputStream;)V
    .locals 5
    .param p1    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-boolean v3, p0, Lcom/leff/midi/MidiTrack;->mClosed:Z

    if-nez v3, :cond_0

    invoke-virtual {p0}, Lcom/leff/midi/MidiTrack;->closeTrack()V

    :cond_0
    iget-boolean v3, p0, Lcom/leff/midi/MidiTrack;->mSizeNeedsRecalculating:Z

    if-eqz v3, :cond_1

    invoke-direct {p0}, Lcom/leff/midi/MidiTrack;->recalculateSize()V

    :cond_1
    sget-object v3, Lcom/leff/midi/MidiTrack;->IDENTIFIER:[B

    invoke-virtual {p1, v3}, Ljava/io/OutputStream;->write([B)V

    iget v3, p0, Lcom/leff/midi/MidiTrack;->mSize:I

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/leff/midi/util/MidiUtil;->intToBytes(II)[B

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/io/OutputStream;->write([B)V

    iget-object v3, p0, Lcom/leff/midi/MidiTrack;->mEvents:Ljava/util/TreeSet;

    invoke-virtual {v3}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/leff/midi/event/MidiEvent;

    invoke-virtual {v0, v2}, Lcom/leff/midi/event/MidiEvent;->requiresStatusByte(Lcom/leff/midi/event/MidiEvent;)Z

    move-result v3

    invoke-virtual {v0, p1, v3}, Lcom/leff/midi/event/MidiEvent;->writeToFile(Ljava/io/OutputStream;Z)V

    move-object v2, v0

    goto :goto_0

    :cond_2
    return-void
.end method
