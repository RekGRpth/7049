.class public Lcom/leff/midi/event/ChannelEvent;
.super Lcom/leff/midi/event/MidiEvent;
.source "ChannelEvent.java"


# static fields
.field private static mOrderMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field protected mChannel:I

.field protected mType:I

.field protected mValue1:I

.field protected mValue2:I


# direct methods
.method protected constructor <init>(JIIII)V
    .locals 9
    .param p1    # J
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I

    const-wide/16 v3, 0x0

    move-object v0, p0

    move-wide v1, p1

    move v5, p3

    move v6, p4

    move v7, p5

    move v8, p6

    invoke-direct/range {v0 .. v8}, Lcom/leff/midi/event/ChannelEvent;-><init>(JJIIII)V

    return-void
.end method

.method protected constructor <init>(JJIIII)V
    .locals 1
    .param p1    # J
    .param p3    # J
    .param p5    # I
    .param p6    # I
    .param p7    # I
    .param p8    # I

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/leff/midi/event/MidiEvent;-><init>(JJ)V

    and-int/lit8 v0, p5, 0xf

    iput v0, p0, Lcom/leff/midi/event/ChannelEvent;->mType:I

    and-int/lit8 v0, p6, 0xf

    iput v0, p0, Lcom/leff/midi/event/ChannelEvent;->mChannel:I

    and-int/lit16 v0, p7, 0xff

    iput v0, p0, Lcom/leff/midi/event/ChannelEvent;->mValue1:I

    and-int/lit16 v0, p8, 0xff

    iput v0, p0, Lcom/leff/midi/event/ChannelEvent;->mValue2:I

    return-void
.end method

.method private static buildOrderMap()V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/leff/midi/event/ChannelEvent;->mOrderMap:Ljava/util/HashMap;

    sget-object v0, Lcom/leff/midi/event/ChannelEvent;->mOrderMap:Ljava/util/HashMap;

    const/16 v1, 0xc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/leff/midi/event/ChannelEvent;->mOrderMap:Ljava/util/HashMap;

    const/16 v1, 0xb

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/leff/midi/event/ChannelEvent;->mOrderMap:Ljava/util/HashMap;

    const/16 v1, 0x9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/leff/midi/event/ChannelEvent;->mOrderMap:Ljava/util/HashMap;

    const/16 v1, 0x8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/leff/midi/event/ChannelEvent;->mOrderMap:Ljava/util/HashMap;

    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/leff/midi/event/ChannelEvent;->mOrderMap:Ljava/util/HashMap;

    const/16 v1, 0xd

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/leff/midi/event/ChannelEvent;->mOrderMap:Ljava/util/HashMap;

    const/16 v1, 0xe

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public compareTo(Lcom/leff/midi/event/MidiEvent;)I
    .locals 10
    .param p1    # Lcom/leff/midi/event/MidiEvent;

    const/4 v4, -0x1

    const/4 v5, 0x1

    iget-wide v6, p0, Lcom/leff/midi/event/ChannelEvent;->mTick:J

    invoke-virtual {p1}, Lcom/leff/midi/event/MidiEvent;->getTick()J

    move-result-wide v8

    cmp-long v3, v6, v8

    if-eqz v3, :cond_2

    iget-wide v6, p0, Lcom/leff/midi/event/ChannelEvent;->mTick:J

    invoke-virtual {p1}, Lcom/leff/midi/event/MidiEvent;->getTick()J

    move-result-wide v8

    cmp-long v3, v6, v8

    if-gez v3, :cond_1

    move v3, v4

    :goto_0
    move v4, v3

    :cond_0
    :goto_1
    return v4

    :cond_1
    move v3, v5

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/leff/midi/event/ChannelEvent;->mDelta:Lcom/leff/midi/util/VariableLengthInt;

    invoke-virtual {v3}, Lcom/leff/midi/util/VariableLengthInt;->getValue()I

    move-result v3

    iget-object v6, p1, Lcom/leff/midi/event/MidiEvent;->mDelta:Lcom/leff/midi/util/VariableLengthInt;

    invoke-virtual {v6}, Lcom/leff/midi/util/VariableLengthInt;->getValue()I

    move-result v6

    if-eq v3, v6, :cond_4

    iget-object v3, p0, Lcom/leff/midi/event/ChannelEvent;->mDelta:Lcom/leff/midi/util/VariableLengthInt;

    invoke-virtual {v3}, Lcom/leff/midi/util/VariableLengthInt;->getValue()I

    move-result v3

    iget-object v6, p1, Lcom/leff/midi/event/MidiEvent;->mDelta:Lcom/leff/midi/util/VariableLengthInt;

    invoke-virtual {v6}, Lcom/leff/midi/util/VariableLengthInt;->getValue()I

    move-result v6

    if-ge v3, v6, :cond_3

    :goto_2
    move v4, v5

    goto :goto_1

    :cond_3
    move v5, v4

    goto :goto_2

    :cond_4
    instance-of v3, p1, Lcom/leff/midi/event/ChannelEvent;

    if-nez v3, :cond_5

    move v4, v5

    goto :goto_1

    :cond_5
    move-object v0, p1

    check-cast v0, Lcom/leff/midi/event/ChannelEvent;

    iget v3, p0, Lcom/leff/midi/event/ChannelEvent;->mType:I

    invoke-virtual {v0}, Lcom/leff/midi/event/ChannelEvent;->getType()I

    move-result v6

    if-eq v3, v6, :cond_7

    sget-object v3, Lcom/leff/midi/event/ChannelEvent;->mOrderMap:Ljava/util/HashMap;

    if-nez v3, :cond_6

    invoke-static {}, Lcom/leff/midi/event/ChannelEvent;->buildOrderMap()V

    :cond_6
    sget-object v3, Lcom/leff/midi/event/ChannelEvent;->mOrderMap:Ljava/util/HashMap;

    iget v6, p0, Lcom/leff/midi/event/ChannelEvent;->mType:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sget-object v3, Lcom/leff/midi/event/ChannelEvent;->mOrderMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Lcom/leff/midi/event/ChannelEvent;->getType()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-lt v1, v2, :cond_0

    move v4, v5

    goto :goto_1

    :cond_7
    iget v3, p0, Lcom/leff/midi/event/ChannelEvent;->mValue1:I

    iget v6, v0, Lcom/leff/midi/event/ChannelEvent;->mValue1:I

    if-eq v3, v6, :cond_8

    iget v3, p0, Lcom/leff/midi/event/ChannelEvent;->mValue1:I

    iget v6, v0, Lcom/leff/midi/event/ChannelEvent;->mValue1:I

    if-lt v3, v6, :cond_0

    move v4, v5

    goto :goto_1

    :cond_8
    iget v3, p0, Lcom/leff/midi/event/ChannelEvent;->mValue2:I

    iget v6, v0, Lcom/leff/midi/event/ChannelEvent;->mValue2:I

    if-eq v3, v6, :cond_9

    iget v3, p0, Lcom/leff/midi/event/ChannelEvent;->mValue2:I

    iget v6, v0, Lcom/leff/midi/event/ChannelEvent;->mValue2:I

    if-lt v3, v6, :cond_0

    move v4, v5

    goto :goto_1

    :cond_9
    iget v3, p0, Lcom/leff/midi/event/ChannelEvent;->mChannel:I

    invoke-virtual {v0}, Lcom/leff/midi/event/ChannelEvent;->getChannel()I

    move-result v6

    if-eq v3, v6, :cond_a

    iget v3, p0, Lcom/leff/midi/event/ChannelEvent;->mChannel:I

    invoke-virtual {v0}, Lcom/leff/midi/event/ChannelEvent;->getChannel()I

    move-result v6

    if-lt v3, v6, :cond_0

    move v4, v5

    goto/16 :goto_1

    :cond_a
    const/4 v4, 0x0

    goto/16 :goto_1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/leff/midi/event/MidiEvent;

    invoke-virtual {p0, p1}, Lcom/leff/midi/event/ChannelEvent;->compareTo(Lcom/leff/midi/event/MidiEvent;)I

    move-result v0

    return v0
.end method

.method public getChannel()I
    .locals 1

    iget v0, p0, Lcom/leff/midi/event/ChannelEvent;->mChannel:I

    return v0
.end method

.method protected getEventSize()I
    .locals 1

    iget v0, p0, Lcom/leff/midi/event/ChannelEvent;->mType:I

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x3

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x2

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0xc
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public getType()I
    .locals 1

    iget v0, p0, Lcom/leff/midi/event/ChannelEvent;->mType:I

    return v0
.end method

.method public requiresStatusByte(Lcom/leff/midi/event/MidiEvent;)Z
    .locals 4
    .param p1    # Lcom/leff/midi/event/MidiEvent;

    const/4 v1, 0x1

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    instance-of v2, p1, Lcom/leff/midi/event/ChannelEvent;

    if-eqz v2, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/leff/midi/event/ChannelEvent;

    iget v2, p0, Lcom/leff/midi/event/ChannelEvent;->mType:I

    invoke-virtual {v0}, Lcom/leff/midi/event/ChannelEvent;->getType()I

    move-result v3

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/leff/midi/event/ChannelEvent;->mChannel:I

    invoke-virtual {v0}, Lcom/leff/midi/event/ChannelEvent;->getChannel()I

    move-result v3

    if-ne v2, v3, :cond_0

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public writeToFile(Ljava/io/OutputStream;Z)V
    .locals 3
    .param p1    # Ljava/io/OutputStream;
    .param p2    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-super {p0, p1, p2}, Lcom/leff/midi/event/MidiEvent;->writeToFile(Ljava/io/OutputStream;Z)V

    if-eqz p2, :cond_0

    iget v1, p0, Lcom/leff/midi/event/ChannelEvent;->mType:I

    shl-int/lit8 v1, v1, 0x4

    iget v2, p0, Lcom/leff/midi/event/ChannelEvent;->mChannel:I

    add-int v0, v1, v2

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    :cond_0
    iget v1, p0, Lcom/leff/midi/event/ChannelEvent;->mValue1:I

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write(I)V

    iget v1, p0, Lcom/leff/midi/event/ChannelEvent;->mType:I

    const/16 v2, 0xc

    if-eq v1, v2, :cond_1

    iget v1, p0, Lcom/leff/midi/event/ChannelEvent;->mType:I

    const/16 v2, 0xd

    if-eq v1, v2, :cond_1

    iget v1, p0, Lcom/leff/midi/event/ChannelEvent;->mValue2:I

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write(I)V

    :cond_1
    return-void
.end method
