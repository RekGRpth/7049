.class public Lcom/leff/midi/event/meta/Tempo;
.super Lcom/leff/midi/event/meta/MetaEvent;
.source "Tempo.java"


# instance fields
.field private mBPM:F

.field private mMPQN:I


# direct methods
.method public constructor <init>()V
    .locals 6

    const-wide/16 v1, 0x0

    const v5, 0x7a120

    move-object v0, p0

    move-wide v3, v1

    invoke-direct/range {v0 .. v5}, Lcom/leff/midi/event/meta/Tempo;-><init>(JJI)V

    return-void
.end method

.method public constructor <init>(JJI)V
    .locals 7
    .param p1    # J
    .param p3    # J
    .param p5    # I

    const/16 v5, 0x51

    new-instance v6, Lcom/leff/midi/util/VariableLengthInt;

    const/4 v0, 0x3

    invoke-direct {v6, v0}, Lcom/leff/midi/util/VariableLengthInt;-><init>(I)V

    move-object v0, p0

    move-wide v1, p1

    move-wide v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/leff/midi/event/meta/MetaEvent;-><init>(JJILcom/leff/midi/util/VariableLengthInt;)V

    invoke-virtual {p0, p5}, Lcom/leff/midi/event/meta/Tempo;->setMpqn(I)V

    return-void
.end method


# virtual methods
.method public compareTo(Lcom/leff/midi/event/MidiEvent;)I
    .locals 7
    .param p1    # Lcom/leff/midi/event/MidiEvent;

    const/4 v1, -0x1

    const/4 v2, 0x1

    iget-wide v3, p0, Lcom/leff/midi/event/meta/Tempo;->mTick:J

    invoke-virtual {p1}, Lcom/leff/midi/event/MidiEvent;->getTick()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-eqz v3, :cond_2

    iget-wide v3, p0, Lcom/leff/midi/event/meta/Tempo;->mTick:J

    invoke-virtual {p1}, Lcom/leff/midi/event/MidiEvent;->getTick()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-gez v3, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    move v1, v2

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/leff/midi/event/meta/Tempo;->mDelta:Lcom/leff/midi/util/VariableLengthInt;

    invoke-virtual {v3}, Lcom/leff/midi/util/VariableLengthInt;->getValue()I

    move-result v3

    int-to-long v3, v3

    invoke-virtual {p1}, Lcom/leff/midi/event/MidiEvent;->getDelta()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/leff/midi/event/meta/Tempo;->mDelta:Lcom/leff/midi/util/VariableLengthInt;

    invoke-virtual {v3}, Lcom/leff/midi/util/VariableLengthInt;->getValue()I

    move-result v3

    int-to-long v3, v3

    invoke-virtual {p1}, Lcom/leff/midi/event/MidiEvent;->getDelta()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-gez v3, :cond_3

    :goto_1
    move v1, v2

    goto :goto_0

    :cond_3
    move v2, v1

    goto :goto_1

    :cond_4
    instance-of v3, p1, Lcom/leff/midi/event/meta/Tempo;

    if-nez v3, :cond_5

    move v1, v2

    goto :goto_0

    :cond_5
    move-object v0, p1

    check-cast v0, Lcom/leff/midi/event/meta/Tempo;

    iget v3, p0, Lcom/leff/midi/event/meta/Tempo;->mMPQN:I

    iget v4, v0, Lcom/leff/midi/event/meta/Tempo;->mMPQN:I

    if-eq v3, v4, :cond_6

    iget v3, p0, Lcom/leff/midi/event/meta/Tempo;->mMPQN:I

    iget v4, v0, Lcom/leff/midi/event/meta/Tempo;->mMPQN:I

    if-lt v3, v4, :cond_0

    move v1, v2

    goto :goto_0

    :cond_6
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/leff/midi/event/MidiEvent;

    invoke-virtual {p0, p1}, Lcom/leff/midi/event/meta/Tempo;->compareTo(Lcom/leff/midi/event/MidiEvent;)I

    move-result v0

    return v0
.end method

.method protected getEventSize()I
    .locals 1

    const/4 v0, 0x6

    return v0
.end method

.method public setBpm(F)V
    .locals 2
    .param p1    # F

    iput p1, p0, Lcom/leff/midi/event/meta/Tempo;->mBPM:F

    const v0, 0x4c64e1c0

    iget v1, p0, Lcom/leff/midi/event/meta/Tempo;->mBPM:F

    div-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/leff/midi/event/meta/Tempo;->mMPQN:I

    return-void
.end method

.method public setMpqn(I)V
    .locals 2
    .param p1    # I

    iput p1, p0, Lcom/leff/midi/event/meta/Tempo;->mMPQN:I

    const v0, 0x4c64e1c0

    iget v1, p0, Lcom/leff/midi/event/meta/Tempo;->mMPQN:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/leff/midi/event/meta/Tempo;->mBPM:F

    return-void
.end method

.method public writeToFile(Ljava/io/OutputStream;)V
    .locals 2
    .param p1    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x3

    invoke-super {p0, p1}, Lcom/leff/midi/event/meta/MetaEvent;->writeToFile(Ljava/io/OutputStream;)V

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write(I)V

    iget v0, p0, Lcom/leff/midi/event/meta/Tempo;->mMPQN:I

    invoke-static {v0, v1}, Lcom/leff/midi/util/MidiUtil;->intToBytes(II)[B

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    return-void
.end method
