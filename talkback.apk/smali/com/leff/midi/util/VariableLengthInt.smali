.class public Lcom/leff/midi/util/VariableLengthInt;
.super Ljava/lang/Object;
.source "VariableLengthInt.java"


# instance fields
.field private mBytes:[B

.field private mSizeInBytes:I

.field private mValue:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0, p1}, Lcom/leff/midi/util/VariableLengthInt;->setValue(I)V

    return-void
.end method

.method private buildBytes()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget v3, p0, Lcom/leff/midi/util/VariableLengthInt;->mValue:I

    if-nez v3, :cond_1

    new-array v3, v5, [B

    iput-object v3, p0, Lcom/leff/midi/util/VariableLengthInt;->mBytes:[B

    iget-object v3, p0, Lcom/leff/midi/util/VariableLengthInt;->mBytes:[B

    aput-byte v4, v3, v4

    iput v5, p0, Lcom/leff/midi/util/VariableLengthInt;->mSizeInBytes:I

    :cond_0
    return-void

    :cond_1
    iput v4, p0, Lcom/leff/midi/util/VariableLengthInt;->mSizeInBytes:I

    new-array v2, v6, [I

    iget v1, p0, Lcom/leff/midi/util/VariableLengthInt;->mValue:I

    :goto_0
    iget v3, p0, Lcom/leff/midi/util/VariableLengthInt;->mSizeInBytes:I

    if-ge v3, v6, :cond_2

    if-lez v1, :cond_2

    iget v3, p0, Lcom/leff/midi/util/VariableLengthInt;->mSizeInBytes:I

    and-int/lit8 v4, v1, 0x7f

    aput v4, v2, v3

    iget v3, p0, Lcom/leff/midi/util/VariableLengthInt;->mSizeInBytes:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/leff/midi/util/VariableLengthInt;->mSizeInBytes:I

    shr-int/lit8 v1, v1, 0x7

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    :goto_1
    iget v3, p0, Lcom/leff/midi/util/VariableLengthInt;->mSizeInBytes:I

    if-ge v0, v3, :cond_3

    aget v3, v2, v0

    or-int/lit16 v3, v3, 0x80

    aput v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iget v3, p0, Lcom/leff/midi/util/VariableLengthInt;->mSizeInBytes:I

    new-array v3, v3, [B

    iput-object v3, p0, Lcom/leff/midi/util/VariableLengthInt;->mBytes:[B

    const/4 v0, 0x0

    :goto_2
    iget v3, p0, Lcom/leff/midi/util/VariableLengthInt;->mSizeInBytes:I

    if-ge v0, v3, :cond_0

    iget-object v3, p0, Lcom/leff/midi/util/VariableLengthInt;->mBytes:[B

    iget v4, p0, Lcom/leff/midi/util/VariableLengthInt;->mSizeInBytes:I

    sub-int/2addr v4, v0

    add-int/lit8 v4, v4, -0x1

    aget v4, v2, v4

    int-to-byte v4, v4

    aput-byte v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method


# virtual methods
.method public getByteCount()I
    .locals 1

    iget v0, p0, Lcom/leff/midi/util/VariableLengthInt;->mSizeInBytes:I

    return v0
.end method

.method public getBytes()[B
    .locals 1

    iget-object v0, p0, Lcom/leff/midi/util/VariableLengthInt;->mBytes:[B

    return-object v0
.end method

.method public getValue()I
    .locals 1

    iget v0, p0, Lcom/leff/midi/util/VariableLengthInt;->mValue:I

    return v0
.end method

.method public setValue(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/leff/midi/util/VariableLengthInt;->mValue:I

    invoke-direct {p0}, Lcom/leff/midi/util/VariableLengthInt;->buildBytes()V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/leff/midi/util/VariableLengthInt;->mBytes:[B

    invoke-static {v1}, Lcom/leff/midi/util/MidiUtil;->bytesToHex([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/leff/midi/util/VariableLengthInt;->mValue:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
