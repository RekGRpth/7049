.class Lcom/android/uiautomator/core/UiAutomatorBridge;
.super Landroid/accessibilityservice/UiTestAutomationBridge;
.source "UiAutomatorBridge.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/uiautomator/core/UiAutomatorBridge$AccessibilityEventListener;
    }
.end annotation


# static fields
.field private static final BUSY_STATE_POLL_TIME:J = 0x32L

.field private static final LOGTAG:Ljava/lang/String;

.field private static final QUIET_TIME_TO_BE_CONSIDERD_IDLE_STATE:J = 0x1f4L

.field public static final TIMEOUT_ASYNC_PROCESSING:J = 0x1388L

.field private static final TOTAL_TIME_TO_WAIT_FOR_IDLE_STATE:J = 0x2710L

.field private static final WAIT_TIME_FROM_IDLE_TO_BUSY_STATE:J = 0x1f4L


# instance fields
.field private final mEventQueue:Ljava/util/concurrent/LinkedBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingQueue",
            "<",
            "Landroid/view/accessibility/AccessibilityEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final mInteractionController:Lcom/android/uiautomator/core/InteractionController;

.field private mLastEventTime:J

.field private mLastOperationTime:J

.field private final mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/android/uiautomator/core/UiAutomatorBridge$AccessibilityEventListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mLock:Ljava/lang/Object;

.field private final mQueryController:Lcom/android/uiautomator/core/QueryController;

.field private volatile mWaitingForEventDelivery:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/uiautomator/core/UiAutomatorBridge;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/uiautomator/core/UiAutomatorBridge;->LOGTAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>()V
    .locals 3

    const-wide/16 v1, 0x0

    invoke-direct {p0}, Landroid/accessibilityservice/UiTestAutomationBridge;-><init>()V

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/uiautomator/core/UiAutomatorBridge;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/uiautomator/core/UiAutomatorBridge;->mLock:Ljava/lang/Object;

    iput-wide v1, p0, Lcom/android/uiautomator/core/UiAutomatorBridge;->mLastEventTime:J

    iput-wide v1, p0, Lcom/android/uiautomator/core/UiAutomatorBridge;->mLastOperationTime:J

    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>(I)V

    iput-object v0, p0, Lcom/android/uiautomator/core/UiAutomatorBridge;->mEventQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    new-instance v0, Lcom/android/uiautomator/core/InteractionController;

    invoke-direct {v0, p0}, Lcom/android/uiautomator/core/InteractionController;-><init>(Lcom/android/uiautomator/core/UiAutomatorBridge;)V

    iput-object v0, p0, Lcom/android/uiautomator/core/UiAutomatorBridge;->mInteractionController:Lcom/android/uiautomator/core/InteractionController;

    new-instance v0, Lcom/android/uiautomator/core/QueryController;

    invoke-direct {v0, p0}, Lcom/android/uiautomator/core/QueryController;-><init>(Lcom/android/uiautomator/core/UiAutomatorBridge;)V

    iput-object v0, p0, Lcom/android/uiautomator/core/UiAutomatorBridge;->mQueryController:Lcom/android/uiautomator/core/QueryController;

    invoke-virtual {p0}, Landroid/accessibilityservice/UiTestAutomationBridge;->connect()V

    return-void
.end method

.method private getLastEventTime()J
    .locals 4

    iget-object v1, p0, Lcom/android/uiautomator/core/UiAutomatorBridge;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-wide v2, p0, Lcom/android/uiautomator/core/UiAutomatorBridge;->mLastEventTime:J

    monitor-exit v1

    return-wide v2

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private getLastOperationTime()J
    .locals 4

    iget-object v1, p0, Lcom/android/uiautomator/core/UiAutomatorBridge;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-wide v2, p0, Lcom/android/uiautomator/core/UiAutomatorBridge;->mLastOperationTime:J

    monitor-exit v1

    return-wide v2

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private notifyListeners(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 3
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;

    iget-object v2, p0, Lcom/android/uiautomator/core/UiAutomatorBridge;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/uiautomator/core/UiAutomatorBridge$AccessibilityEventListener;

    invoke-interface {v1, p1}, Lcom/android/uiautomator/core/UiAutomatorBridge$AccessibilityEventListener;->onAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method addAccessibilityEventListener(Lcom/android/uiautomator/core/UiAutomatorBridge$AccessibilityEventListener;)V
    .locals 1
    .param p1    # Lcom/android/uiautomator/core/UiAutomatorBridge$AccessibilityEventListener;

    iget-object v0, p0, Lcom/android/uiautomator/core/UiAutomatorBridge;->mListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public executeCommandAndWaitForAccessibilityEvent(Ljava/lang/Runnable;Lcom/android/internal/util/Predicate;J)Landroid/view/accessibility/AccessibilityEvent;
    .locals 11
    .param p1    # Ljava/lang/Runnable;
    .param p3    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            "Lcom/android/internal/util/Predicate",
            "<",
            "Landroid/view/accessibility/AccessibilityEvent;",
            ">;J)",
            "Landroid/view/accessibility/AccessibilityEvent;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/concurrent/TimeoutException;,
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v10, 0x0

    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/android/uiautomator/core/UiAutomatorBridge;->mWaitingForEventDelivery:Z

    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    :cond_0
    :goto_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    sub-long v1, v8, v6

    sub-long v4, p3, v1

    const-wide/16 v8, 0x0

    cmp-long v8, v4, v8

    if-gtz v8, :cond_1

    iput-boolean v10, p0, Lcom/android/uiautomator/core/UiAutomatorBridge;->mWaitingForEventDelivery:Z

    iget-object v8, p0, Lcom/android/uiautomator/core/UiAutomatorBridge;->mEventQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v8}, Ljava/util/concurrent/LinkedBlockingQueue;->clear()V

    new-instance v8, Ljava/util/concurrent/TimeoutException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Expected event not received within: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " ms."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw v8

    :cond_1
    const/4 v3, 0x0

    :try_start_0
    iget-object v8, p0, Lcom/android/uiautomator/core/UiAutomatorBridge;->mEventQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    sget-object v9, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v8, v4, v5, v9}, Ljava/util/concurrent/LinkedBlockingQueue;->poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v8

    move-object v0, v8

    check-cast v0, Landroid/view/accessibility/AccessibilityEvent;

    move-object v3, v0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    if-eqz v3, :cond_0

    invoke-interface {p2, v3}, Lcom/android/internal/util/Predicate;->apply(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    iput-boolean v10, p0, Lcom/android/uiautomator/core/UiAutomatorBridge;->mWaitingForEventDelivery:Z

    iget-object v8, p0, Lcom/android/uiautomator/core/UiAutomatorBridge;->mEventQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v8}, Ljava/util/concurrent/LinkedBlockingQueue;->clear()V

    return-object v3

    :cond_2
    invoke-virtual {v3}, Landroid/view/accessibility/AccessibilityEvent;->recycle()V

    goto :goto_0

    :catch_0
    move-exception v8

    goto :goto_1
.end method

.method getInteractionController()Lcom/android/uiautomator/core/InteractionController;
    .locals 1

    iget-object v0, p0, Lcom/android/uiautomator/core/UiAutomatorBridge;->mInteractionController:Lcom/android/uiautomator/core/InteractionController;

    return-object v0
.end method

.method getQueryController()Lcom/android/uiautomator/core/QueryController;
    .locals 1

    iget-object v0, p0, Lcom/android/uiautomator/core/UiAutomatorBridge;->mQueryController:Lcom/android/uiautomator/core/QueryController;

    return-object v0
.end method

.method public onAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 5
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;

    invoke-super {p0, p1}, Landroid/accessibilityservice/UiTestAutomationBridge;->onAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    sget-object v1, Lcom/android/uiautomator/core/UiAutomatorBridge;->LOGTAG:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v1, p0, Lcom/android/uiautomator/core/UiAutomatorBridge;->mWaitingForEventDelivery:Z

    if-eqz v1, :cond_0

    :try_start_0
    invoke-static {p1}, Landroid/view/accessibility/AccessibilityEvent;->obtain(Landroid/view/accessibility/AccessibilityEvent;)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    iget-object v1, p0, Lcom/android/uiautomator/core/UiAutomatorBridge;->mEventQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    const-wide/16 v2, 0x1388

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v0, v2, v3, v4}, Ljava/util/concurrent/LinkedBlockingQueue;->offer(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-boolean v1, p0, Lcom/android/uiautomator/core/UiAutomatorBridge;->mWaitingForEventDelivery:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/uiautomator/core/UiAutomatorBridge;->mEventQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/LinkedBlockingQueue;->clear()V

    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/uiautomator/core/UiAutomatorBridge;->mLastEventTime:J

    invoke-direct {p0, p1}, Lcom/android/uiautomator/core/UiAutomatorBridge;->notifyListeners(Landroid/view/accessibility/AccessibilityEvent;)V

    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method setOperationTime()V
    .locals 4

    iget-object v1, p0, Lcom/android/uiautomator/core/UiAutomatorBridge;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/uiautomator/core/UiAutomatorBridge;->mLastOperationTime:J

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method updateEventTime()V
    .locals 4

    iget-object v1, p0, Lcom/android/uiautomator/core/UiAutomatorBridge;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/uiautomator/core/UiAutomatorBridge;->mLastEventTime:J

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public waitForIdle()V
    .locals 2

    const-wide/16 v0, 0x2710

    invoke-virtual {p0, v0, v1}, Lcom/android/uiautomator/core/UiAutomatorBridge;->waitForIdle(J)V

    return-void
.end method

.method public waitForIdle(J)V
    .locals 2
    .param p1    # J

    const-wide/16 v0, 0x1f4

    invoke-virtual {p0, v0, v1, p1, p2}, Lcom/android/uiautomator/core/UiAutomatorBridge;->waitForIdle(JJ)V

    return-void
.end method

.method public waitForIdle(JJ)V
    .locals 6
    .param p1    # J
    .param p3    # J

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    :goto_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v0

    const-wide/16 v4, 0x1f4

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    invoke-direct {p0}, Lcom/android/uiautomator/core/UiAutomatorBridge;->getLastOperationTime()J

    move-result-wide v2

    invoke-direct {p0}, Lcom/android/uiautomator/core/UiAutomatorBridge;->getLastEventTime()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    const-wide/16 v2, 0x32

    invoke-static {v2, v3}, Landroid/os/SystemClock;->sleep(J)V

    goto :goto_0

    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/accessibilityservice/UiTestAutomationBridge;->waitForIdle(JJ)V

    return-void
.end method
